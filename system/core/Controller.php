<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		EllisLab Dev Team
 * @copyright		Copyright (c) 2008 - 2014, EllisLab, Inc.
 * @copyright		Copyright (c) 2014 - 2015, British Columbia Institute of Technology (http://bcit.ca/)
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter Application Controller Class
 *
 * This class object is the super class that every library in
 * CodeIgniter will be assigned to.
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		EllisLab Dev Team
 * @link		http://codeigniter.com/user_guide/general/controllers.html
 */
class CI_Controller {

	private static $instance;
        
        public $user        = NULL;
        public $dirFoto     = "images/teamjll/";
        public $galeriaComp = "gallery/comp/";
        public $myData      = NULL;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		self::$instance =& $this;

		// Assign all the class objects that were instantiated by the
		// bootstrap file (CodeIgniter.php) to local class variables
		// so that CI can run as one big super object.
		foreach (is_loaded() as $var => $class)
		{
			$this->$var =& load_class($class);
		}

		$this->load =& load_class('Loader', 'core');

		$this->load->initialize();
		
		log_message('debug', "Controller Class Initialized");
	}

	public static function &get_instance()
	{
		return self::$instance;
	}
        
        public function validaSesion($ajax,$admin)
	{
		$this->user = $this -> session -> userdata('datos_sesion');		
												
		if( $this->user == NULL )
                    {
                            $data['titulos']  = array("navegador" => "JLL - HERMES", 
                                                      "ventana"   => "HERMES",                                                      
                                                      "titulo"    => "Ingreso al Portal HERMES");
                            $msj              = "Su sesión ha expirado, favor de ingresar al portal nuevamente";
                            $data['usuario']  = "";
                            $data['iconuser'] = "";
                            $data['foto']     = "";
                            $data['sesion']   = $msj;
                            
                            if($ajax=== NULL)
                                {    
                                $this->load->view('login',$data);
                                die($this->output->get_output());
                                }
                            else
                                { return array("session" => "E","msj"=>$msj); }
                    }
                else{
                    $this->user = $this -> session -> userdata('datos_sesion');                		
                    if ($this->user['0']['tipo']=="2" & $admin===TRUE)
                    {
                            $msj              = "Usted no cuenta con los privilegios necesarios para accesar a la sección solicitada";
                            $data['usuario']  = "";
                            $data['iconuser'] = "";
                            $data['foto']     = "";
                            $data['sesion']   = $msj;
                            $data['titulos']  = array("navegador" => "JLL - HERMES", 
                                                      "ventana"   => "HERMES",                                                      
                                                      "titulo"    => "Ingreso al Portal HERMES");

                            if($ajax=== NULL)
                            {    
                                $this->load->view('login',$data);
                                die($this->output->get_output());
                            }
                            else
                                { return array("session" => "P","msj"=>$msj); }
                    }
                    else {
                            $this->session->sess_update();        
                            $this->session->userdata['last_activity'] = time();
                            $datos_sesion = $this -> session -> userdata('datos_sesion');
                            $this->session->set_userdata('datos_sesion', $datos_sesion);
                            
                            $this->myData['usuario']  = element('nombre', $this->user['0'])." ".element('apellidos', $this->user['0']);
                            $this->myData['tipo']     = element('tipo'  , $this->user['0']);
                            $this->myData['iconuser'] = '<img src="'.base_url().'images/user.png"/>';
                            $this->myData['foto']     = '<img width="35" heigth="35" class="img-circle" src="'.base_url().$this->dirFoto.element('foto', $this->user['0']).'"/>';
                            return TRUE;
                         }
                    }
        }
}
// END Controller class

/* End of file Controller.php */
/* Location: ./system/core/Controller.php */