<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Utils
{     
    public function traduceMeses($fecha) 
    {
        $meses = array("Enero", "Febrero", "Marzo","Abri","Mayo","Junio","Julio","Agosto","Septiembre","Octubre",
                       "Noviembre","Diciembre");
        $months = array("January","February","March","April","May","June","July","August","September","October",
                        "November","December");
        
        $fechaTraducida = str_replace($months,$meses,$fecha);
        $fechaTraducida = str_replace("-"," de ", $fechaTraducida);     
    
        return $fechaTraducida;
    }
        
        public function hermesDateFormat($paramDate) 
    {
            if ( empty($paramDate) )
                { return NULL; }                    
            else
                {   $paramDateStr = DateTime::createFromFormat('M j Y', $paramDate);
                    return $paramDateStr ->format('Y/m/j');                         
                }                    
    }
        
    public function hermesDateFormatEspanol($paramDate,$formatDateOrigin,$formatDateDestino) 
    {
            if ( empty($paramDate) )
                { return ""; }                    
            else
                {  
                     $paramDateStr = DateTime::createFromFormat($formatDateOrigin, $paramDate);
                    return $paramDateStr->format($formatDateDestino);                         
                }                    
    }
        
        public function gethermesDate($paramDate,$idiom,$formatDateOrigin,$formatDateDestino)
        {
            /* echo "paramDate:" . $paramDate;
            echo "formatDateOrigin:" . $formatDateOrigin;
            echo "formatDateDestino:" . $formatDateDestino; */

          if ($idiom ===  "ENG")
          { 
            //return $paramDate; 
            //'M dS Y'
 
            $formatDateDestino = "F d, Y";
       
            return $this->hermesDateFormatEspanol($paramDate,$formatDateOrigin,$formatDateDestino);
          } 
          else
          { return $this->hermesDateFormatEspanol($paramDate,$formatDateOrigin,$formatDateDestino); }
        } 
        
         public function validaDateFormat($paramDate) 
    {
        try{                 
               if (empty($paramDate))
                   { return '';}
               else    
                {   $dateStr = DateTime::createFromFormat('M j Y', $paramDate);
                    return $dateStr->format('Y-m-d');                  
                }
         }  catch (Exception $e) {echo 'validaDateFormat Utils Excepción: ',  $e, "\n";}   
       }
    
    public function generaNombreFile($fileName) 
    {
        $newCaracter = array("n", "N", "a","e","i","o","u","A","E","I","O","U");
        $oldCaracter = array("ñ","Ñ","á","é","í","ó","ú","Á","É","Í","Ó","Ú");
        
        $sinAcentos = str_replace($oldCaracter,$newCaracter,$fileName);
        $sinAcentos = str_replace(" ","_", $sinAcentos);
                $sinAcentos = str_replace(")","_", $sinAcentos);
                $sinAcentos = str_replace("(","_", $sinAcentos);
                $sinAcentos = str_replace("&","_", $sinAcentos);
                $sinAcentos = str_replace("%","_", $sinAcentos);
        
        if (strlen($sinAcentos) > 10 )
            $sinAcentos = substr($sinAcentos, 0, 10);
            
        return $sinAcentos;
    }
        
        
        public function adHtmlJustificado($texto)
    {
            $contenidoHtml   = "";
            $textoAntesTag   = strstr($texto, '<ul>', true);
            if ($textoAntesTag === FALSE)
            {
                $textoAntesTag   = strstr($texto, '<ol>', true);
                if ($textoAntesTag === FALSE)
                     { $contenidoHtml   ='<p style="text-align: justify;">'.$texto.'</p>'; }
                else { $inter           = strstr($texto, '<ol>');  
                       $textoDespuesTag = strstr($inter, '</ol>', true);
                       $textoUltimoTag  = strstr($texto, '</ol>');
                       $contenidoHtml   ='<p style="text-align: justify;">'.$textoAntesTag.'</p>'.$textoDespuesTag.'</ol><p style="text-align: justify;">'.str_replace("</ol>","", $textoUltimoTag).'</p>';               
                     }
            }
            else{
                $inter           = strstr($texto, '<ul>');
                $textoDespuesTag = strstr($inter, '</ul>', true);
                $textoUltimoTag  = strstr($texto, '</ul>');
                $contenidoHtml   ='<p style="text-align: justify;">'.$textoAntesTag.'</p>'.$textoDespuesTag.'</ul><p style="text-align: justify;">'.str_replace("</ul>","", $textoUltimoTag).'</p>';               
            }
            
            return $contenidoHtml;
        }
        
        public function generaNombreImagen($fileName) 
    {
        $newCaracter = array("n", "N", "a","e","i","o","u","A","E","I","O","U");
        $oldCaracter = array("ñ","Ñ","á","é","í","ó","ú","Á","É","Í","Ó","Ú");
        
        $sinAcentos = str_replace($oldCaracter,$newCaracter,$fileName);
        $sinAcentos = str_replace(" ","_", $sinAcentos);
                $sinAcentos = str_replace(")","_", $sinAcentos);
                $sinAcentos = str_replace("(","_", $sinAcentos);
                $sinAcentos = str_replace("&","_", $sinAcentos);
                $sinAcentos = str_replace("%","_", $sinAcentos);
                $sinAcentos = str_replace("|","_", $sinAcentos);
                $sinAcentos = str_replace("°","_", $sinAcentos);
                $sinAcentos = str_replace("!","_", $sinAcentos);
                $sinAcentos = str_replace("@","_", $sinAcentos);
                $sinAcentos = str_replace("#","_", $sinAcentos);
                $sinAcentos = str_replace("$","_", $sinAcentos);
                $sinAcentos = str_replace("/","_", $sinAcentos);
                $sinAcentos = str_replace("\\","_", $sinAcentos);
                $sinAcentos = str_replace("^","_", $sinAcentos);
                $sinAcentos = str_replace("*","_", $sinAcentos);                
                $sinAcentos = str_replace("?","_", $sinAcentos);
                $sinAcentos = str_replace("[","_", $sinAcentos);
                $sinAcentos = str_replace("]","_", $sinAcentos);
                $sinAcentos = str_replace("{","_", $sinAcentos);
                $sinAcentos = str_replace("}","_", $sinAcentos);
                $sinAcentos = str_replace("[","_", $sinAcentos);
                $sinAcentos = str_replace("'","", $sinAcentos);
                $sinAcentos = str_replace("\"","", $sinAcentos);                

        return $sinAcentos;
    }
    
    public function pdf_create($html, $filename='', $stream=TRUE) 
    {                       
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->render();
        
        if ($stream) 
            $dompdf->stream($filename.".pdf");
        else 
            return $dompdf->output();
    }
        
        
        public function borraArchivoPorExtension($dir,$tipo) 
        {  
            $extension = '';
            
            if($tipo=='P')
                    { $extension = 'pptx'; }
                elseif ($tipo=='W') 
                    { $extension = 'docx'; }
                    else
                        { $extension = 'xlsx'; }
                        
            $cdir = scandir($dir); 
            foreach ($cdir as $key => $value) 
            { 
                if( substr($value,-4) == $extension)
                    { unlink($dir.$value);  }
            }              
         } 
         
         private function seccionReorte($listaSecciones, $seccion)
         {
             $incluirSeccion = FALSE;
             
             foreach ($listaSecciones as $s)
                {
                    if($s==$seccion)
                        {$incluirSeccion = TRUE; }
                }
                
            return $incluirSeccion; 
         }
         
         private function addFooter($section, $withLogo)
         {
            $footer = $section->addFooter();
            $tableF  = $footer ->addTable();
            $tableF->addRow();
            if($withLogo === TRUE)
                { $tableF->addCell(7000)->addImage('images/logoJLL_Small.png',array('marginTop' => 0,'marginLeft' => 0,'align'=>'left') ); }
           else
                { $tableF->addCell(7000)->addText('','ArialNarrowBlack9','paragraphLeft');  }
                
            $tableF->addCell(7000)->addPreserveText(utf8_decode('Page {PAGE} of {NUMPAGES}.'),'ArialNarrowGray11', 'paragraphRight');

         }
         
         private function addHeader($section, $appraisalNo, $nameSecction, $fotoWatermark)
         {             
             $header  = $section -> addHeader();
             $tableH  = $header  -> addTable();
             $tableH->addRow();                     
             $tableH->addCell(6000)->addText($appraisalNo,'ArialNarrowBlack9','paragraphLeft');
             $tableH->addCell(4000)->addText($nameSecction,'ArialNarrowBlack9','paragraphRight');
             $header->addWatermark($fotoWatermark, array('marginTop'=>200, 'marginLeft'=>55));             
         }

         private function addFooterLandscape($section)
         {
            $footer = $section->addFooter();
            $tableF  = $footer ->addTable();
            $tableF->addRow();            
            //$tableF->addCell(14000)->addImage('images/logoJLL_Small.jpg',array('marginTop' => 0,'marginLeft' => 0,'align'=>'left') );
            $this->addmyImageTBL($tableF,14000,NULL,'images/logoJLL_Small.jpg',NULL,NULL,'left');
            $tableF->addCell(14000)->addPreserveText(utf8_decode('Page {PAGE} of {NUMPAGES}.'),'ArialNarrowGray11', 'paragraphRight');

         }
         
         private function addHeaderLandscape($section, $appraisalNo, $nameSecction, $fotoWatermark)
         {             
             $header  = $section -> addHeader();
             $tableH  = $header  -> addTable();
             $tableH->addRow();                     
             $tableH->addCell(7500)->addText($appraisalNo,'ArialNarrowBlack9','paragraphLeft');
             $tableH->addCell(7500)->addText($nameSecction,'ArialNarrowBlack9','paragraphRight');
             $header->addWatermark($fotoWatermark, array('marginTop'=>200, 'marginLeft'=>55));             
         }
         
         private function addmyImage($section, $nombreConRuta, $widthImg, $heightImg,$align)
         {             
             try { $section->addImage($nombreConRuta,array('width' => $widthImg,'height' => $heightImg,'marginTop' => 0,'marginLeft' => 0,'align'=>$align) );} 
             catch (Exception $e) {  }
         }
         
         private function addmyImageTBL($tabla,$anchoCelda,$estiloCelda, $nombreConRuta, $widthImg, $heightImg,$align)
         {             
             try {$param = ($widthImg==NULL && $heightImg==NULL)?array('marginTop' => 0,'marginLeft' => 0,'align'=>$align):array('width' => $widthImg,'height' => $heightImg,'marginTop' => 0,'marginLeft' => 0,'marginBottom' => 0,'align'=>$align);
                  if($estiloCelda==NULL)
                    { $tabla->addCell($anchoCelda)->addImage($nombreConRuta,$param ); }
                  else
                    { $tabla->addCell($anchoCelda,$estiloCelda)->addImage($nombreConRuta,$param );  }
                 } 
             catch (Exception $e) {  }
         }
                  
         
         public function exportaWord($infoFile,$inmueble,$dirFoto,$galeriaComp,$md_gallery,$reportSections,$annSections,$apSections,$md_inmuebles)
        {
         try{
                require_once APPPATH.'third_party/PhpOffice/PhpWord/Autoloader.php';
                require_once APPPATH.'libraries/UtilLanguage.php';
                \PhpOffice\PhpWord\Autoloader::register();             
                
                $formatoWord = $infoFile['idFile'].'.docx';
                $numAnexo    = 0;
                $appraisalNo = 'Appraisal No:'.$infoFile['idFile'];

        $word = new \PhpOffice\PhpWord\PhpWord();
                $utilLanguage = new UtilLanguage();

                $fontStyle = array('spaceAfter' => 0, 'size' => 10);
                $word->addFontStyle('ArialNarrowBlack7',      array('name'=>'Arial Narrow','size'=>7));
                $word->addFontStyle('ArialNarrowBlack9',      array('name'=>'Arial Narrow','size'=>8));
                $word->addFontStyle('ArialNarrowBlack9Italic',array('name'=>'Arial Narrow','size'=>8,'italic'=>true));
                $word->addFontStyle('ArialNarrowBlack8',      array('name'=>'Arial Narrow','size'=>8));
                $word->addFontStyle('ArialNarrowBlackBold9',  array('name'=>'Arial Narrow','size'=>9,'bold'=>true));
                $word->addFontStyle('ArialNarrowBlack12',     array('name'=>'Arial Narrow','size'=>12));
                $word->addFontStyle('ArialNarrowBlack12Italic',array('name'=>'Arial Narrow','size'=>12,'italic'=>true));
                $word->addFontStyle('ArialNarrowBlackBold12', array('name'=>'Arial Narrow','size'=>12,'bold'=>true));
                $word->addFontStyle('ArialNarrowBlackBold14', array('name'=>'Arial Narrow','size'=>14,'bold'=>true));
                $word->addFontStyle('ArialNarrowBlack10',     array('name'=>'Arial Narrow','size'=>10));
                $word->addFontStyle('ArialNarrowBlackCur10',  array('name'=>'Arial Narrow','size'=>10,'underline' => \PhpOffice\PhpWord\Style\Font::UNDERLINE_SINGLE));
                $word->addFontStyle('ArialNarrowGray11',      array('name'=>'Arial Narrow','size'=>11,'color'=>'#5A5A5A'));
                $word->addFontStyle('ArialNarrowGray16',      array('name'=>'Arial Narrow','size'=>16,'color'=>'#5A5A5A'));
                $word->addFontStyle('ArialNarrowBlack11',     array('name'=>'Arial Narrow','size'=>11,'color'=>'#000000'));
                $word->addFontStyle('ArialNarrowBlack11Italic',array('name'=>'Arial Narrow','size'=>10,'bold'=>true,'italic'=>true));
                $word->addFontStyle('ArialNarrowBlackBold11', array('name'=>'Arial Narrow','size'=>11,'color'=>'#000000','bold'=>true));
                $word->addFontStyle('ArialNarrowRed11',       array('name'=>'Arial Narrow','size'=>11,'color'=>'#8C0F13'));
                $word->addFontStyle('ArialNarrowRedBold11',   array('name'=>'Arial Narrow','size'=>11,'color'=>'#BC141A','bold'=>true));
                $word->addFontStyle('ArialNarrowRedBold10',   array('name'=>'Arial Narrow','size'=>10,'color'=>'#BC141A','bold'=>true));
                $word->addFontStyle('ArialNarrowRedBold12',   array('name'=>'Arial Narrow','size'=>12,'color'=>'#BC141A','bold'=>true));
                $word->addFontStyle('ArialRedBold14',         array('name'=>'Arial Narrow','size'=>14,'color'=>'#BC141A','bold'=>true));
                $word->addFontStyle('ArialNarrowGrayBold11',  array('name'=>'Arial Narrow','size'=>11,'color'=>'#5A5A5A','bold'=>true));
                $word->addFontStyle('ArialNGrayBold11Italic', array('name'=>'Arial Narrow','size'=>11,'color'=>'#5A5A5A','bold'=>true,'italic'=>true));
                $word->addFontStyle('ArialNarrowGrayBold14',  array('name'=>'Arial Narrow','size'=>14,'color'=>'#5A5A5A','bold'=>true));
                $word->addFontStyle('ArialNarrowBlackBold11', array('name'=>'Arial Narrow','size'=>10,'color'=>'#000000','bold'=>true));
                $word->addFontStyle('ArialNarrowBlackBold10', array('name'=>'Arial Narrow','size'=>10,'color'=>'#000000','bold'=>true));
                $word->addFontStyle('ArialNarrowBlackBold14', array('name'=>'Arial Narrow','size'=>14,'color'=>'#000000','bold'=>true));
                $word->addFontStyle('ArialNarrowBlackBold16', array('name'=>'Arial Narrow','size'=>16,'color'=>'#000000','bold'=>true));
                /*PORTADA*/
                $word->addFontStyle('ArialNarrowBlackBold18'    , array('name'=>'Arial Narrow','size'=>18,'color'=>'#000000','bold'=>true));
                $word->addFontStyle('TimesNewRomanBlackItalic36', array('name'=>'Times New Roman','size'=>36,'color'=>'#000000','italic'=>true));
                $word->addFontStyle('ArialNarrowRedBold20'      , array('name'=>'Arial Narrow','size'=>20,'color'=>'#BC141A','bold'=>true));
                $word->addFontStyle('ArialNarrowBlack16',         array('name'=>'Arial Narrow','size'=>16));
                /*PORTADA*/     
                $word->addFontStyle('TimesNewRomanRedBold16', array('color'=>'#BC141A','name'=>'Times New Roman','size'=>16,'bold'=>true));
                $word->addFontStyle('ArialNarrowRedWhiteBold11', array('name'=>'Arial Narrow','size'=>11,'color'=>'#FFFFFF','bgColor' => '#963634','bold'=>true));
                $word->addFontStyle('ArialNarrowRedWhiteBold12', array('name'=>'Arial Narrow','size'=>12,'color'=>'#FFFFFF','bgColor' => '#963634','bold'=>true));
                $word->addFontStyle('ArialNarrowRedWhiteBold10', array('name'=>'Arial Narrow','size'=>10,'color'=>'#FFFFFF','bgColor' => '#963634','bold'=>true));
                $word->addFontStyle('ArialNarrowRedWhiteBold14', array('name'=>'Arial Narrow','size'=>14,'color'=>'#FFFFFF','bgColor' => '#963634','bold'=>true));
                $word->addTitleStyle(1, array('size' => 14, 'color' => '#BC141A','name'=>'Arial Narrow','bold'=>true,'align'=>'center'));
                $word->addTitleStyle(2, array('size' => 14, 'color' => '#BC141A','name'=>'Arial Narrow','bold'=>true));
                $word->addTitleStyle(3, array('size' => 14, 'color' => '#BC141A','name'=>'Arial Narrow','bold'=>true));
                $word->addTitleStyle(4, array('size' => 1,  'color' => '#FFFFFF','name'=>'Arial Narrow','bold'=>true));
                                
                $word->addParagraphStyle('paragraphCenter', array('align'=>'center' ));
        $word->addParagraphStyle('paragraphRight' , array('align'=>'right'  ));
        $word->addParagraphStyle('paragraphLeft'  , array('align'=>'left'   ));
                $word->addParagraphStyle('paragraphJust'  , array('align'=>'justify'));
                $word->addParagraphStyle('paraCenterT'    , array('align'=>'center' ,'spaceBefore' => 0, 'spaceAfter' => 0));
                $word->addParagraphStyle('paraLeftT'      , array('align'=>'left'   ,'spaceBefore' => 0, 'spaceAfter' => 0));
                $word->addParagraphStyle('paraRightT'     , array('align'=>'right'  ,'spaceBefore' => 0, 'spaceAfter' => 0));
                $word->addParagraphStyle('paraJust'       , array('align'=>'justify','spaceBefore' => 0, 'spaceAfter' => 0));
                
                $word->setDefaultFontName('Arial Narrow');
                $word->setDefaultFontSize(12);
                
                $sCellLineTop    = array('valign' => 'center','borderTopSize'    => 15,'borderTopColor'   =>'#CB0B0B');
                $sCellLineSinL   = array('valign' => 'center','borderTopSize'    => 1 ,'borderBottomSize' => 1,'borderBottomColor'=>'#FFFFFF','borderTopColor'=>'#FFFFFF');
                $sCellLineBottom = array('valign' => 'center','borderBottomSize' => 15,'borderBottomColor'=>'#CB0B0B','spaceAfter' => 0,'spaceBefore' => 0);
                $sCellLineBtSpace= array('valign' => 'center','borderBottomSize' => 15,'borderBottomColor'=>'#CB0B0B','spaceAfter' => 2,'spaceBefore' => 2);
                $sCellLineBot    = array('valign' => 'center','borderBottomSize' => 15,'borderBottomColor'=>'#CB0B0B','borderTopSize' => 6,'borderTopColor'=>'#CB0B0B');
                $cellLineBotThin = array('valign' => 'center','borderBottomSize' => 10,'borderBottomColor'=>'#CB0B0B','spaceAfter' => 0,'spaceBefore' => 0);
                
                $fotoWatermark=(($inmueble['frontPage'][0]['id_status'] == "1") | ($inmueble['frontPage'][0]['id_status'] == "2") ) ? 'images/draft.jpg' : (($inmueble['frontPage'][0]['id_status'] == "3") ? 'images/rejected.jpg' : 'images/ap.jpg');
                                
                $totGBA    = empty($inmueble['totArea'])?" ":number_format($inmueble['totArea'][0]['totGBA'], 2, '.', ',')." m2";
                $totTRA    = empty($inmueble['totArea'])?0:$inmueble['totArea'][0]['totTRA'];
                $totGBAFT  = empty($inmueble['totArea'])?" ":number_format($inmueble['totArea'][0]['totGBA']*$inmueble['dc']['ft'], 2, '.', ',')." ft2";
                $totTRAFT  = empty($inmueble['totArea'])?" ":number_format($inmueble['totArea'][0]['totTRA']*$inmueble['dc']['ft'], 2, '.', ',')." ft2";
                $totLand   = empty($inmueble['land'])?" ":number_format($inmueble['land'][0]['deed'], 2, '.', ',')." m2";
                $totLandFT = empty($inmueble['land'])?" ":number_format($inmueble['land'][0]['deed']*$inmueble['dc']['ft'], 2, '.', ',')." ft2";
                $plans     = empty($inmueble['land'])?0:$inmueble['land'][0]['plans'];
                $plansFT   = empty($inmueble['land'])?0:$inmueble['land'][0]['plans']*$inmueble['dc']['ft'];
                $difLand   = empty($inmueble['land'])?" ":number_format($inmueble['land'][0]['deed']-$inmueble['land'][0]['plans'], 2, '.', ',')." m2";
                $difLandFT = empty($inmueble['land'])?" ":number_format(($inmueble['land'][0]['deed']-$inmueble['land'][0]['plans'])*$inmueble['dc']['ft'], 2, '.', ',')." ft2";
                $area      = array("land"=>$plans,"tra"=>$totTRA);                
                
                $AXmarketVal     = empty($inmueble['APVII'][0]['AXmarketVal'])?0:$inmueble['APVII'][0]['AXmarketVal'];
                $AXmarketValM2   = $totTRA==0?0:$AXmarketVal / $totTRA;
                
                $AXmarketValft2  = $inmueble['dc']['ft']===0?0:$AXmarketValM2 / $inmueble['dc']['ft'];

                $AX_VU_Approach  = empty($inmueble['mv'][0]['AX_VU_Approach'])?0:$inmueble['mv'][0]['AX_VU_Approach'];
                $AXVUApproachM2  = $totTRA==0?0:$AX_VU_Approach / $totTRA;
                $AXVUApproachft2 = $inmueble['dc']['ft']===0?0:$AXVUApproachM2 / $inmueble['dc']['ft']; 

                $AXIliquidVal    = empty($inmueble['APVIII'][0]['AXIliquidVal'])?0:$inmueble['APVIII'][0]['AXIliquidVal'];
                $AXIliquidValM2  = $totTRA==0?0:$AXIliquidVal / $totTRA;
                $AXIliquidValft2 = $inmueble['dc']['ft']===0?0:$AXIliquidValM2 / $inmueble['dc']['ft'];

                $section = $word->addSection();

                $location = $this->creaLocation($inmueble['frontPage'][0]['calle'],$inmueble['frontPage'][0]['num'],$inmueble['frontPage'][0]['col'],$inmueble['frontPage'][0]['mun'],$inmueble['frontPage'][0]['edo'],$inmueble['frontPage'][0]['cp']); 
                $param     = $this->leyendasAPV();//borrar
                
                //******** FRON PAGE INI ****************/
                $lables = $utilLanguage->getLabel("FP", $infoFile['idiom']);
                if ($this->seccionReorte($reportSections, "FP") === TRUE)
                {   $this->addHeader($section, '', '', $fotoWatermark);
                    $this->addFooter($section, FALSE);
                    
                    $tblPortada  = $section ->addTable();
                    $tblPortada->addRow();
                    $cell = $tblPortada->addCell(10000);
                    $textrun = $cell->addTextRun(array('align' => 'left','spaceBefore' => 0, 'spaceAfter' => 0));
                    $textrun->addText($lables['FP1'].$this->gethermesDate($inmueble['frontPage'][0]['effectiveDate'],$infoFile['idiom'],'M dS Y','d  M  Y'),'ArialNarrowBlackBold18','paragraphLeft');
                    $textrun->addTextBreak();
                    $textrun->addTextBreak();
                    $textrun->addText($lables['FP2'].$inmueble['frontPage'][0]['preparedFor'],'TimesNewRomanBlackItalic36','paragraphLeft');
                    $textrun->addTextBreak();
                    $textrun->addTextBreak();
                    $textrun->addText($lables['FP3'].$inmueble['frontPage'][0]['propertyOf'],'ArialNarrowRedBold20','paragraphLeft');
                    $textrun->addTextBreak();
                    $textrun->addText($location,'ArialNarrowGray16','paragraphLeft');
                    $textrun->addTextBreak();
                    $textrun->addText($lables['FP4'].$inmueble['frontPage'][0]['appraisal_num'],'ArialNarrowGray16','paragraphLeft');
                    $textrun->addTextBreak();
                    $tblPortada->addCell(1000)->addImage('images/logoJLLPortada.png',array('width' => '109','height' => '109','marginTop' => 0,'marginLeft' => 0,'marginBottom' => 0,'align'=>'right') );         

                    $fotoPortada=($inmueble['frontPage'][0]['fotoPortada'] == "logoJLL.jpg") ? $dirFoto.'/'.$inmueble['frontPage'][0]['fotoPortada'] : $dirFoto.'/'.$inmueble['frontPage'][0]['id_in'].'/'.$inmueble['frontPage'][0]['fotoPortada'];
                    $this->addmyImage($section, $fotoPortada, 615, 520,"center");
                    $section->addPageBreak();
                }
                //******** FRON PAGE FIN ****************/                 
                
                $sectionCont = $word->addSection();

                $this->addHeader($sectionCont, $appraisalNo, $lables['FP0'], $fotoWatermark);
                $this->addFooter($sectionCont, TRUE);

                $toc = $sectionCont->addTOC($fontStyle);
                $toc->setMinDepth(2);
                $toc->setMaxDepth(3);

                //******** LETTER INI ****************/
                $lables = $utilLanguage->getLabel("LT", $infoFile['idiom']);
                if ($this->seccionReorte($reportSections, "LT") === TRUE && !empty($inmueble['letter']))
                {
                    $sectionLT = $word->addSection();
                    $this->addHeader($sectionLT, $appraisalNo, $lables["LT0"], $fotoWatermark);
                    $this->addFooter($sectionLT, FALSE);
                    $sectionLT->addTitle(htmlspecialchars($lables["LT0"]), 2);
                    
                    $tableLT  = $sectionLT ->addTable();
                    $tableLT->addRow();                                                         
                    $this->addmyImageTBL($tableLT,7000,NULL,'images/logoJLL_Medium.jpg',NULL,NULL,'left');
                    if($inmueble['letter']['client_logo']!='No Logo')
                        { $this->addmyImageTBL($tableLT,7000,NULL,$dirFoto.'/'.$infoFile['id_in'].'/'.$inmueble['letter']['client_logo'],132,57,'right'); }
                    
                    $sectionLT->addText($lables["LT1"].$this->gethermesDate($infoFile['fechaReporte'],$infoFile['idiom']," l, F j, Y ","d  M  Y"),'ArialNarrowBlack10','paragraphRight');                    
                    $sectionLT->addText(htmlspecialchars($inmueble['letter']['client']),'ArialNarrowBlackBold14','paraLeftT');
                    $sectionLT->addText(htmlspecialchars($inmueble['letter']['client_addres']),'ArialNarrowBlack10','paraLeftT');                    
                    $sectionLT->addText(htmlspecialchars($inmueble['letter']['client_addres_2']),'ArialNarrowBlack10','paraLeftT');
                    
                    $sectionLT->addText(htmlspecialchars($inmueble['letter']['atn_officer']),'ArialNarrowBlackBold14','paraRightT');
                    $sectionLT->addText(htmlspecialchars($inmueble['letter']['jd_atn_officer']),'ArialNarrowBlack10','paraRightT');
                    $sectionLT->addText(htmlspecialchars($inmueble['letter']['jd_atn_officer_2']),'ArialNarrowBlack10','paraRightT');
                    
                    $sectionLT->addTextBreak(1);                    
                    $sectionLT->addText(htmlspecialchars($inmueble['letter']['parrafo1'].' '.$location),'ArialNarrowBlack14','paragraphJust');                    
                    
                    $sectionLT->addText($lables["LT2"].$this->gethermesDate($inmueble['letter']['effectiveDate'],$infoFile['idiom'],'M dS Y','d  M  Y'),'ArialNarrowBlack12','paragraphLeft');
                    $sectionLT->addTextBreak(1);
                    $flagPageBreak = FALSE;
                    
                    $sectionLT->addText(htmlspecialchars($inmueble['letter']['texto1']),'ArialNarrowBlackBold12','paragraphLeft');
                    $tblResumen = $sectionLT->addTable(array('cellMargin' => 0, 'cellMarginRight' => 0, 'cellMarginBottom' => 0, 'cellMarginLeft' => 0));
                    if($inmueble['letter']['LTchbox1']=="1")
                    {
                        $tblResumen->addRow();
                        $tblResumen->addCell(3000, $sCellLineBot)->addText('$ '.number_format($this->redondearHermes($AXmarketVal), 2, '.', ',').' '.$param['currency'],'ArialNarrowBlackBold12','paraCenterT'); 
                        $tblResumen->addCell(3000, $sCellLineBot)->addText('$ '.number_format($AXmarketValM2, 2, '.', ',').' '.$param['currency'].' / m2','ArialNarrowBlackBold12','paraCenterT'); 
                        $tblResumen->addCell(3000, $sCellLineBot)->addText('$ '.number_format($AXmarketValft2, 2, '.', ',').' '.$param['currency'].' / ft2','ArialNarrowBlackBold12','paraCenterT');
                        $sectionLT->addTextBreak(1);
                    }
                    
                    if($inmueble['letter']['LTchbox2']=="1")
                    {
                        $sectionLT->addText($inmueble['letter']['texto2'],'ArialNarrowBlackBold12','paragraphLeft');
                        $tblResumen = $sectionLT->addTable(array('cellMargin' => 0, 'cellMarginRight' => 0, 'cellMarginBottom' => 0, 'cellMarginLeft' => 0));
                        $tblResumen->addRow();
                        $tblResumen->addCell(3000, $sCellLineBot)->addText('$ '.number_format($this->redondearHermes($AX_VU_Approach), 2, '.', ',').' '.$param['currency'],'ArialNarrowBlackBold12','paraCenterT');
                        $tblResumen->addCell(3000, $sCellLineBot)->addText('$ '.number_format($AXVUApproachM2, 2, '.', ',').' '.$param['currency'].' / m2','ArialNarrowBlackBold12','paraCenterT');
                        $tblResumen->addCell(3000, $sCellLineBot)->addText('$ '.number_format($AXVUApproachft2, 2, '.', ',').' '.$param['currency'].' / ft2','ArialNarrowBlackBold12','paraCenterT');
                        $sectionLT->addTextBreak(1);
                        $flagPageBreak = TRUE;
                    }
                    
                    if($inmueble['letter']['LTchbox3']=="1")
                    {
                        $sectionLT->addText($inmueble['letter']['texto3'],'ArialNarrowBlackBold12','paragraphLeft');
                        $tblResumen = $sectionLT->addTable(array('cellMargin' => 0, 'cellMarginRight' => 0, 'cellMarginBottom' => 0, 'cellMarginLeft' => 0));
                        $tblResumen->addRow();
                        $tblResumen->addCell(3000, $sCellLineBot)->addText('$ '.number_format($this->redondearHermes($AXIliquidVal), 2, '.', ',').' '.$param['currency'],'ArialNarrowBlackBold12','paraCenterT'); 
                        $tblResumen->addCell(3000, $sCellLineBot)->addText('$ '.number_format($AXIliquidValM2, 2, '.', ',').' '.$param['currency'].' / m2','ArialNarrowBlackBold12','paraCenterT'); 
                        $tblResumen->addCell(3000, $sCellLineBot)->addText('$ '.number_format($AXIliquidValft2, 2, '.', ',').' '.$param['currency'].' / ft2','ArialNarrowBlackBold12','paraCenterT');                    
                        $sectionLT->addTextBreak(1);
                        $flagPageBreak = TRUE;
                    }
                    
                    if(!empty($inmueble['letter']['parrafo2']))
                        { \PhpOffice\PhpWord\Shared\Html::addHtml($sectionLT, $inmueble['letter']['parrafo2']); }
                    
                    if($flagPageBreak === FALSE)
                        { $sectionLT->addPageBreak(); 
                    
                        $sectionLT_II = $word->addSection();
                        $this->addHeader($sectionLT_II, $appraisalNo, $lables["LT0"], $fotoWatermark);
                        $this->addFooter($sectionLT_II, TRUE);
                        
                        if(!empty($inmueble['letter']['parrafo3']))
                        {
                         $sectionLT_II->addTextBreak(2); 
                         \PhpOffice\PhpWord\Shared\Html::addHtml($sectionLT_II, $inmueble['letter']['parrafo3']);
                         $sectionLT_II->addTextBreak(7);
                        }
                        $table = $sectionLT_II->addTable(array('align' => 'center','cellMargin' => 0, 'cellMarginRight' => 0, 'cellMarginBottom' => 0, 'cellMarginLeft' => 0));
                        $table->addRow(350);
                        $table->addCell(4500, array('align' => 'center'))->addShape('line',array('points'  => '0,0 250,0','outline' => array('color' => '#000000','line' => 'thickThin','weight' => 1)));
                        $table->addCell(4500, array('align' => 'center'))->addShape('line',array('points'  => '0,0 250,0','outline' => array('color' => '#000000','line' => 'thickThin','weight' => 1)));
                        $table->addRow(350);
                        $table->addCell(4500)->addText($inmueble['letter']['nombreFP'].', '.$inmueble['letter']['tituloFP'],'ArialNarrowRedBold11','paraCenterT');
                        $table->addCell(4500)->addText($inmueble['letter']['nombreFS'].(empty($inmueble['letter']['tituloFS'])?' ':', '.$inmueble['letter']['tituloFS']),'ArialNarrowRedBold11','paraCenterT');
                        $table->addRow(350);
                        $table->addCell(4500)->addText($inmueble['letter']['puestoFP'],'ArialNarrowBlackBold11','paraCenterT');
                        $table->addCell(4500)->addText(empty($inmueble['letter']['puestoFS'])?' ':$inmueble['letter']['puestoFS'],'ArialNarrowBlackBold11','paraCenterT');
                        $table->addRow(350);
                        $table->addCell(4500)->addText($lables["LT3"],'ArialNarrowBlackBold11','paraCenterT');
                        $table->addCell(4500)->addText($lables["LT3"],'ArialNarrowBlackBold11','paraCenterT');
                        $table->addRow(350);
                        $table->addCell(4500)->addText($lables["LT4"],'ArialNarrowBlackBold11','paraCenterT');
                        $table->addCell(4500)->addText($lables["LT4"],'ArialNarrowBlackBold11','paraCenterT');                  

                        $sectionLT_II->addPageBreak();
                        }
                    else{
                        $sectionLT->addTextBreak(2);
                        \PhpOffice\PhpWord\Shared\Html::addHtml($sectionLT, $inmueble['letter']['parrafo3']);
                        $sectionLT->addTextBreak(7);

                        $table = $sectionLT->addTable(array('align' => 'center','cellMargin' => 0, 'cellMarginRight' => 0, 'cellMarginBottom' => 0, 'cellMarginLeft' => 0));
                        $table->addRow(350);
                        $table->addCell(4500, array('align' => 'center'))->addShape('line',array('points'  => '0,0 250,0','outline' => array('color' => '#000000','line' => 'thickThin','weight' => 1)));
                        $table->addCell(4500, array('align' => 'center'))->addShape('line',array('points'  => '0,0 250,0','outline' => array('color' => '#000000','line' => 'thickThin','weight' => 1)));
                        $table->addRow(350);
                        $table->addCell(4500)->addText(htmlspecialchars($inmueble['letter']['nombreFP'].', '.$inmueble['letter']['tituloFP']),'ArialNarrowRedBold11','paraCenterT');
                        $table->addCell(4500)->addText(htmlspecialchars($inmueble['letter']['nombreFS'].(empty($inmueble['letter']['tituloFS'])?' ':', '.$inmueble['letter']['tituloFS'])),'ArialNarrowRedBold11','paraCenterT');
                        $table->addRow(350);
                        $table->addCell(4500)->addText(htmlspecialchars($inmueble['letter']['puestoFP']),'ArialNarrowBlackBold11','paraCenterT');
                        $table->addCell(4500)->addText(empty($inmueble['letter']['puestoFS'])?' ':htmlspecialchars($inmueble['letter']['puestoFS']),'ArialNarrowBlackBold11','paraCenterT');
                        $table->addRow(350);
                        $table->addCell(4500)->addText($lables["LT3"],'ArialNarrowBlackBold11','paraCenterT');
                        $table->addCell(4500)->addText($lables["LT3"],'ArialNarrowBlackBold11','paraCenterT');
                        $table->addRow(350);
                        $table->addCell(4500)->addText($lables["LT4"],'ArialNarrowBlackBold11','paraCenterT');
                        $table->addCell(4500)->addText($lables["LT4"],'ArialNarrowBlackBold11','paraCenterT');

                        $sectionLT->addPageBreak();
                        }
                    $sectionDef = $word->addSection();
                    $this->addHeader($sectionDef, $appraisalNo, $lables["LT5"], $fotoWatermark);
                    $this->addFooter($sectionDef, TRUE);
                    
                    $sectionDef->addTitle(htmlspecialchars($lables["LT5"]), 2);
                    $sectionDef->addText($lables["LT6"],'ArialNarrowBlack12','paragraphJust');                    
                   
                    $y=-1;                                        
                    foreach($inmueble['def'] as $def)
                    {
                       $y++;
                       $sectionDef->addText($def['concepto'.$infoFile['idiom']],'ArialNarrowRedBold12','paraJust');                   
                       \PhpOffice\PhpWord\Shared\Html::addHtml($sectionDef, $this->adHtmlJustificado($def['descripcion'.$infoFile['idiom']]) );
                    }

                    $sectionDef->addPageBreak();
                }
                //******** LETTER FIN ****************/


                
                 //******** SUMMARY INI ****************/
                if ($this->seccionReorte($reportSections, "SM") === TRUE)
                {
                    $sectionSM = $word->addSection();                

                    $this->addHeader($sectionSM, $appraisalNo, 'SUMMARY', $fotoWatermark);
                    $this->addFooter($sectionSM, TRUE);

                    $sectionSM->addTitle(htmlspecialchars('SUMMARY OF SALIENT FACTS AND CONCLUSIONS'), 2);
                    $sectionSM->addTextBreak(1);
                    $sectionSM->addText(htmlspecialchars($inmueble['summary']['SMTit1']),'ArialNarrowRedBold12','paraLeftT');
                    $sectionSM->addText(htmlspecialchars($inmueble['summary']['SMText1']),'ArialNarrowBlack12','paraJust');
                    
                    $sectionSM->addTextBreak(1);
                    $sectionSM->addText('Subject\'s address: ','ArialNarrowRedBold12','paraLeftT');
                    $sectionSM->addText(htmlspecialchars($inmueble['summary']['SM_sa']),'ArialNarrowBlack12','paraJust');
                    
                    $sectionSM->addTextBreak(1);
                    $sectionSM->addText('Client: ','ArialNarrowRedBold12','paraLeftT');
                    $sectionSM->addText(htmlspecialchars($inmueble['letter']['client']),'ArialNarrowBlack12','paraJust');
                    
                    $sectionSM->addTextBreak(1);
                    $sectionSM->addText('Property owner: ','ArialNarrowRedBold12','paraLeftT');
                    $sectionSM->addText(htmlspecialchars($inmueble['summary']['SM_po']),'ArialNarrowBlack12','paraJust');                    
                    
                    $sectionSM->addTextBreak(1);
                    if(isset( $inmueble['summary']['SMTit2'])){
                    $sectionSM->addText(htmlspecialchars($inmueble['summary']['SMTit2']),'ArialNarrowRedBold12','paraLeftT');}
                    $sectionSM->addText(htmlspecialchars($inmueble['summary']['SMText2']),'ArialNarrowBlack12','paraJust');
                    
                    $sectionSM->addTextBreak(1);
                    $sectionSM->addText('Areas','ArialNarrowRedBold12','paragraphJust');                    
                    
                    $tblResumen = $sectionSM->addTable(array('cellMargin' => 0, 'cellMarginRight' => 0, 'cellMarginBottom' => 0, 'cellMarginLeft' => 0));
                    $tblResumen->addRow(650);
                    $tblResumen->addCell(3000, $sCellLineTop)->addText('Land Surface','ArialNarrowBlackBold12','paraCenterT'); 
                    $tblResumen->addCell(3000, $sCellLineTop)->addText('Gross Building Area','ArialNarrowBlackBold12','paraCenterT'); 
                    $tblResumen->addCell(3000, $sCellLineTop)->addText('Rentable Area','ArialNarrowBlackBold12','paraCenterT');                                        
                    
                    $tblResumen->addRow(650);
                    $tblResumen->addCell(3000, $sCellLineSinL)->addText($totLand,'ArialNarrowBlackBold12','paraCenterT'); 
                    $tblResumen->addCell(3000, $sCellLineSinL)->addText($totGBA,'ArialNarrowBlackBold12','paraCenterT'); 
                    $tblResumen->addCell(3000, $sCellLineSinL)->addText(number_format($totTRA, 2, '.', ',')." m2",'ArialNarrowBlackBold12','paraCenterT');
                                        
                    $tblResumen->addRow(650);
                    $tblResumen->addCell(3000, $sCellLineBottom)->addText($totLandFT,'ArialNarrowBlack12Italic','paraCenterT'); 
                    $tblResumen->addCell(3000, $sCellLineBottom)->addText($totGBAFT,'ArialNarrowBlack12Italic','paraCenterT'); 
                    $tblResumen->addCell(3000, $sCellLineBottom)->addText($totTRAFT,'ArialNarrowBlack12Italic','paraCenterT');
                    
                    $sectionSM->addTextBreak(1);
                    if(isset($inmueble['summary']['SMTit3'])){
                    $sectionSM->addText(htmlspecialchars($inmueble['summary']['SMTit3']),'ArialNarrowRedBold12','paraLeftT');
                    }
                    $sectionSM->addText(htmlspecialchars($inmueble['summary']['SMText3']),'ArialNarrowBlack12','paraJust');
                    
                    $sectionSM->addTextBreak(1);
                    $sectionSM->addText(htmlspecialchars($inmueble['summary']['SMTit4']),'ArialNarrowRedBold12','paraLeftT');
                    $sectionSM->addText(htmlspecialchars($inmueble['summary']['SMText4']),'ArialNarrowBlack12','paraJust');
                    
                    $sectionSM->addTextBreak(1);
                    $sectionSM->addText('Intended user the report:','ArialNarrowRedBold12','paraLeftT');
                    $sectionSM->addText(htmlspecialchars($inmueble['API'][0]['AIText4']),'ArialNarrowBlack12','paraJust');
                                        
                    $sectionSM->addTextBreak(1);
                    $sectionSM->addText(htmlspecialchars($inmueble['summary']['SMTit6']),'ArialNarrowRedBold12','paraLeftT');
                    $sectionSM->addText(htmlspecialchars($inmueble['summary']['SMText6']),'ArialNarrowBlack12','paraJust');
                    
                    $sectionSM->addTextBreak(1);
                    $sectionSM->addText(htmlspecialchars($inmueble['summary']['SMTit7']),'ArialNarrowRedBold12','paraLeftT');
                    $sectionSM->addText(htmlspecialchars($inmueble['summary']['SMText7']),'ArialNarrowBlack12','paraJust');
                    
                    $sectionSM->addTextBreak(1);
                    $sectionSM->addText('Exchange rate:','ArialNarrowRedBold12','paraLeftT');
                    $sectionSM->addText('$ '.$inmueble['summary']['SM_er']. ' MXN/USD','ArialNarrowBlack12','paraJust');
                    
                    $sectionSM->addTextBreak(1);
                    $sectionSM->addText('Effective date of the report (date of value):','ArialNarrowRedBold12','paraLeftT');
                    $sectionSM->addText($this->gethermesDate(htmlspecialchars($inmueble['summary']['SM_ed']),$infoFile['idiom'],"M dS Y","d  M  Y"),'ArialNarrowBlack12','paraJust');
                    
                    $sectionSM->addTextBreak(1);
                    $sectionSM->addText('Date of the report (date of delivery): ','ArialNarrowRedBold12','paraLeftT');


                    //$this->gethermesDate($infoFile['fechaReporte'],$infoFile['idiom'],'M dS Y','d  M  Y'),'ArialNarrowBlackBold18','paragraphLeft');
                    //$sectionSM->addText($infoFile['fechaReporte'],'ArialNarrowBlackBold18','paragraphLeft');
                    
                   // Sunday, September 9, 2018

                 $sectionSM->addText($this->gethermesDate(trim($infoFile['fechaReporte']),$infoFile['idiom'],"l, F j, Y","d  M  Y"),'ArialNarrowBlack12','paraJust'); 

                   // $sectionSM->addText("hola".$this->gethermesDate($infoFile['fechaReporte'],$infoFile['idiom']," l, F j, Y ","d  M  Y"),'ArialNarrowBlack10','paragraphRight');    
                    
                    $sectionSM->addTextBreak(1);                    
                    $sectionSM->addText(htmlspecialchars($inmueble['scopeofwork']['SWTit6']),'ArialNarrowRedBold12','paraLeftT');
                    $sectionSM->addText(htmlspecialchars($inmueble['scopeofwork']['SWText6']),'ArialNarrowBlack12','paraJust');
                    
                    $sectionSM->addTextBreak(1);                    
                    $sectionSM->addText(htmlspecialchars($inmueble['scopeofwork']['SWTit7']),'ArialNarrowRedBold12','paraLeftT');
                    $sectionSM->addText(htmlspecialchars($inmueble['scopeofwork']['SWText7']),'ArialNarrowBlack12','paraJust');                    
                    
                    $sectionSM->addTextBreak(1);
                    $sectionSM->addText('Value Conclusion:','ArialNarrowRedBold12','paraLeftT');
                    $sectionSM->addTextBreak(1);
                    $tblVC = $sectionSM->addTable(array('cellMargin' => 0, 'cellMarginRight' => 0, 'cellMarginBottom' => 0, 'cellMarginLeft' => 0));
                    $tblVC->addRow(650);
                    $tblVC->addCell(3000, $sCellLineTop)->addText('Appraisal Premise','ArialNarrowBlackBold12','paraLeftT'); 
                    $tblVC->addCell(3000, $sCellLineTop)->addText('Interest Appraised','ArialNarrowBlackBold12','paraLeftT'); 
                    $columnaDate= FALSE;
                    if( empty($inmueble['summary']['SMvc1_3']) & empty($inmueble['summary']['SMvc2_3']) & empty($inmueble['summary']['SMvc3_3']) & empty($inmueble['summary']['SMvc4_3']) & empty($inmueble['summary']['SMvc5_3']) ) { $columnaDate= FALSE; }
                    else { $columnaDate= TRUE; 
                           $tblVC->addCell(3000, $sCellLineTop)->addText('Date of Value','ArialNarrowBlackBold12','paraCenterT'); }
                    $tblVC->addCell(3000, $sCellLineTop)->addText('Value Conclusion','ArialNarrowBlackBold12','paraCenterT');
                    
                    if( $inmueble['summary']['LTchbox1']=="1" )
                    {
                        $tblVC->addRow(650);
                        $tblVC->addCell(3000, $sCellLineSinL)->addText($inmueble['summary']['SMvc1_1'],'ArialNarrowBlack12','paraLeftT'); 
                        $tblVC->addCell(3000, $sCellLineSinL)->addText($inmueble['summary']['SMvc1_2'],'ArialNarrowBlack12','paraLeftT'); 
                        if( !empty($inmueble['summary']['SMvc1_3']) )
                            { $tblVC->addCell(3000, $sCellLineSinL)->addText($inmueble['summary']['SMvc1_3'],'ArialNarrowBlack12','paraCenterT'); }
                        $tblVC->addCell(3000, $sCellLineSinL)->addText('$ '.number_format($this->redondearHermes($AXmarketVal), 2, '.', ',').' '.$param['currency'],'ArialNarrowBlack12','paraCenterT');
                    }
                     
                    if( $inmueble['summary']['LTchbox2']=="1" )
                    {
                        $tblVC->addRow(650);
                        $tblVC->addCell(3000, $sCellLineSinL)->addText($inmueble['summary']['SMvc2_1'],'ArialNarrowBlack12','paraLeftT'); 
                        $tblVC->addCell(3000, $sCellLineSinL)->addText($inmueble['summary']['SMvc2_2'],'ArialNarrowBlack12','paraLeftT'); 
                        if( !empty($inmueble['summary']['SMvc2_3']) )
                            { $tblVC->addCell(3000, $sCellLineSinL)->addText($inmueble['summary']['SMvc2_3'],'ArialNarrowBlack12','paraCenterT'); }
                        $tblVC->addCell(3000, $sCellLineSinL)->addText('$ '.number_format($this->redondearHermes($AX_VU_Approach), 2, '.', ',').' '.$param['currency'],'ArialNarrowBlack12','paraCenterT');
                    }
                    
                    if( $inmueble['summary']['LTchbox3']=="1" )
                    {
                        $tblVC->addRow(650);
                        $tblVC->addCell(3000, $sCellLineSinL)->addText($inmueble['summary']['SMvc3_1'],'ArialNarrowBlack12','paraLeftT'); 
                        $tblVC->addCell(3000, $sCellLineSinL)->addText($inmueble['summary']['SMvc3_2'],'ArialNarrowBlack12','paraLeftT'); 
                        if( !empty($inmueble['summary']['SMvc3_3']) )
                            { $tblVC->addCell(3000, $sCellLineSinL)->addText($inmueble['summary']['SMvc3_3'],'ArialNarrowBlack12','paraCenterT'); }
                        $tblVC->addCell(3000, $sCellLineSinL)->addText('$ '.number_format($this->redondearHermes($AXIliquidVal), 2, '.', ',').' '.$param['currency'],'ArialNarrowBlack12','paraCenterT');
                    }
                    if( !empty($inmueble['summary']['SMvc4_1']) | !empty($inmueble['summary']['SMvc4_2']) | !empty($inmueble['summary']['SMvc4_3']) | !empty($inmueble['summary']['SMvc4_4']) )
                    {
                        $tblVC->addRow(650);
                        $tblVC->addCell(3000, $sCellLineSinL)->addText($inmueble['summary']['SMvc4_1'],'ArialNarrowBlack12','paraLeftT'); 
                        $tblVC->addCell(3000, $sCellLineSinL)->addText($inmueble['summary']['SMvc4_2'],'ArialNarrowBlack12','paraLeftT'); 
                        if( !empty($inmueble['summary']['SMvc4_3']) )
                            { $tblVC->addCell(3000, $sCellLineSinL)->addText($inmueble['summary']['SMvc4_3'],'ArialNarrowBlack12','paraCenterT'); }
                        $tblVC->addCell(3000, $sCellLineSinL)->addText($inmueble['summary']['SMvc4_4'],'ArialNarrowBlack12','paraCenterT');
                    }
                    if( !empty($inmueble['summary']['SMvc5_1']) | !empty($inmueble['summary']['SMvc5_2']) | !empty($inmueble['summary']['SMvc5_3']) | !empty($inmueble['summary']['SMvc5_4']) )
                    {
                        $tblVC->addRow(650);
                        $tblVC->addCell(3000, $sCellLineSinL)->addText($inmueble['summary']['SMvc5_1'],'ArialNarrowBlack12','paraLeftT'); 
                        $tblVC->addCell(3000, $sCellLineSinL)->addText($inmueble['summary']['SMvc5_2'],'ArialNarrowBlack12','paraLeftT'); 
                        if( !empty($inmueble['summary']['SMvc5_3']) )
                            { $tblVC->addCell(3000, $sCellLineSinL)->addText($inmueble['summary']['SMvc5_3'],'ArialNarrowBlack12','paraCenterT'); }
                        $tblVC->addCell(3000, $sCellLineSinL)->addText($inmueble['summary']['SMvc5_4'],'ArialNarrowBlack12','paraCenterT');
                    }
                    $tblVC->addRow(300);
                    $tblVC->addCell(3000, $sCellLineBottom)->addText('','ArialNarrowBlack12','paraCenterT'); 
                    $tblVC->addCell(3000, $sCellLineBottom)->addText('','ArialNarrowBlack12','paraCenterT'); 
                    $tblVC->addCell(3000, $sCellLineBottom)->addText('','ArialNarrowBlack12','paraCenterT');
                    if ($columnaDate === TRUE)
                        { $tblVC->addCell(3000, $sCellLineBottom)->addText('','ArialNarrowBlack12','paraCenterT'); }

                    
                    $sectionSM->addPageBreak();
                }    
                //******** SUMMARY FIN ****************/


                                //******** SCOPE OF WORK INI ****************/
                $lables = $utilLanguage->getLabel("SW", $infoFile['idiom']);
                if ($this->seccionReorte($reportSections, "SW") === TRUE)
                {
                    $sectionSW = $word->addSection();
                    $this->addHeader($sectionSW, $appraisalNo, $lables["SW0"], $fotoWatermark);
                    $this->addFooter($sectionSW, TRUE);
                    
                    $sectionSW->addTitle(htmlspecialchars($lables["SW0"].':'), 2);
                    \PhpOffice\PhpWord\Shared\Html::addHtml($sectionSW, $inmueble['scopeofwork']['SWParrafo1']);
                    $sectionSW->addTextBreak(1);

                    $sectionSW->addText(htmlspecialchars($inmueble['scopeofwork']['SWTit1']),'ArialNarrowRedBold12','paraLeftT');
                    $sectionSW->addText(htmlspecialchars($inmueble['scopeofwork']['SWText1']." ".$inmueble['scopeofwork']['col']),'ArialNarrowBlack12','paraJust');
                    $sectionSW->addTextBreak(1);
                    
                    $sectionSW->addText(htmlspecialchars($inmueble['scopeofwork']['SWTit2']),'ArialNarrowRedBold12','paraLeftT');
                    $sectionSW->addText(htmlspecialchars($inmueble['scopeofwork']['SWText2_1']." ".$inmueble['scopeofwork']['loc'].$inmueble['scopeofwork']['SWText2_2'].$inmueble['scopeofwork']['providedBy']. $inmueble['scopeofwork']['SWText2_3']),'ArialNarrowBlack12','paraJust');
                    $sectionSW->addTextBreak(1);
                     
                    $sectionSW->addText(htmlspecialchars($inmueble['scopeofwork']['SWTit3']),'ArialNarrowRedBold12','paraLeftT');
                    $sectionSW->addText(htmlspecialchars($inmueble['scopeofwork']['SWText3_1'].$this->gethermesDate(htmlspecialchars($inmueble['scopeofwork']['dateInspection']),$infoFile['idiom'],"M dS Y","d  M  Y").", by ".$inmueble['scopeofwork']['inspector']." ". $inmueble['scopeofwork']['SWText3_2']),'ArialNarrowBlack12','paraJust');
                    $sectionSW->addTextBreak(1);
 
                    
                    $sectionSW->addText(htmlspecialchars($inmueble['scopeofwork']['SWTit4']),'ArialNarrowRedBold12','paraLeftT');
                    $sectionSW->addText(htmlspecialchars($inmueble['scopeofwork']['SWText4_1']." ".$inmueble['scopeofwork']['SWText4_2']." ".$inmueble['scopeofwork']['SWText4_3']),'ArialNarrowBlack12','paraJust');
                    $sectionSW->addTextBreak(1);
                    
                    $sectionSW->addText(htmlspecialchars($inmueble['scopeofwork']['SWTit5']),'ArialNarrowRedBold12','paraLeftT');
                    \PhpOffice\PhpWord\Shared\Html::addHtml($sectionSW, $inmueble['scopeofwork']['SWText5']);
                    
                    $sectionSW->addPageBreak();
                }  
                //******** SCOPE OF WORK FIN ****************/
                
                
                 
                 //**************** PHOTOGRAPHS INI ****************/
                 if( ($this->seccionReorte($reportSections, "PH") === TRUE) & (sizeof($inmueble['fotos']) > 0) )
                 {
                   $sectionPH = $word->addSection();                
                   
                   $this->addHeader($sectionPH, $appraisalNo, 'PHOTOGRAPHS', $fotoWatermark);
                   $this->addFooter($sectionPH, TRUE);
                    
                   $sectionPH->addTitle(htmlspecialchars('PHOTOGRAPHS'), 2);
                   $this->creaTablaPhotosWord($inmueble['fotos'],$sectionPH);
                 }
                //**************** PHOTOGRAPHS FIN ****************/
                 
                 //******** APPRAISAL INI ****************/
                 $numAp = 0;                 
                 
                 if( ($this->seccionReorte($apSections, "API") === TRUE) )
                 { $numAp++;
                   $numAP_Romano = $this->converToRoman($numAp);
                   $sectionAP = $word->addSection();

                   $this->addHeader($sectionAP, $appraisalNo, $numAP_Romano.' General Information', $fotoWatermark);
                   $this->addFooter($sectionAP, TRUE);

                   $sectionAP->addTitle(htmlspecialchars('Report'), 2);
                   $sectionAP->addTextBreak(1);
                   $sectionAP->addTitle(htmlspecialchars($numAP_Romano.' General Information'), 3);
                   $sectionAP->addTextBreak(1);
                   
                   $sectionAP->addText("Introduction",'ArialNarrowRedBold12','paraLeftT');
                   
                   $sectionAP->addText($inmueble['API'][0]['AIText1_1'].' '.$location.$inmueble['API'][0]['AIText1_2'],'ArialNarrowBlack12','paraJust');
                   
                   $sectionAP->addTextBreak(1);
                   $sectionAP->addText($inmueble['API'][0]['AITit2'],'ArialNarrowRedBold12','paraLeftT');
                   $sectionAP->addText($inmueble['API'][0]['AIText2'],'ArialNarrowBlack12','paraJust');
                  
                   $sectionAP->addTextBreak(1);
                   $sectionAP->addText($inmueble['API'][0]['AITit3'],'ArialNarrowRedBold12','paraLeftT');
                   $sectionAP->addText($inmueble['API'][0]['AIText3'],'ArialNarrowBlack12','paraJust');
                   
                   $sectionAP->addTextBreak(1);
                   $sectionAP->addText($inmueble['API'][0]['AITit4'],'ArialNarrowRedBold12','paraLeftT');
                   $sectionAP->addText($inmueble['API'][0]['AIText4'],'ArialNarrowBlack12','paraJust');
                   $sectionAP->addText($inmueble['letter']['client_addres'].' '.$inmueble['letter']['client_addres_2'],'ArialNarrowBlack12','paraJust');
               
                   $sectionAP->addTextBreak(1);
                   $sectionAP->addText($inmueble['API'][0]['AITit5'],'ArialNarrowRedBold12','paraLeftT');
                   //$sectionAP->addText($inmueble['frontPage'][0]['effectiveDate'],'ArialNarrowBlack12','paraJust');

                   $sectionAP->addText($this->gethermesDate(htmlspecialchars($inmueble['frontPage'][0]['effectiveDate']),$infoFile['idiom'],"M dS Y","d  M  Y"),'ArialNarrowBlack12','paraJust');
                  
                   $sectionAP->addTextBreak(1);
                   $sectionAP->addText($inmueble['API'][0]['AITit6'],'ArialNarrowRedBold12','paraLeftT');
                   $sectionAP->addText(date("F d, Y "),'ArialNarrowBlack12','paraJust');
                   
                   $sectionAP->addTextBreak(1);
                   $sectionAP->addText($inmueble['API'][0]['AITit7'],'ArialNarrowRedBold12','paraLeftT');
                   $sectionAP->addText($inmueble['API'][0]['AIText7'],'ArialNarrowBlack12','paraJust');
                   
                   $sectionAP->addTextBreak(1);
                   $sectionAP->addText($inmueble['API'][0]['AITit8'],'ArialNarrowRedBold12','paraLeftT');
                   $sectionAP->addText($location,'ArialNarrowBlack12','paraJust');
                   
                   $sectionAP->addTextBreak(1);
                   $sectionAP->addText($inmueble['API'][0]['AITit9'],'ArialNarrowRedBold12','paraLeftT');
                   $sectionAP->addText($inmueble['API'][0]['AIText9'],'ArialNarrowBlack12','paraJust');
                   
                   $sectionAP->addTextBreak(1);
                   $sectionAP->addText($inmueble['API'][0]['AITit10'],'ArialNarrowRedBold12','paraLeftT');
                   $sectionAP->addText($inmueble['frontPage'][0]['propertyOf'],'ArialNarrowBlack12','paraJust');
                   $sectionAP->addPageBreak();
                   
                   $sectionAP->addTextBreak(1);
                   $sectionAP->addText($inmueble['API'][0]['AITit11'],'ArialNarrowRedBold12','paraLeftT');
                   $sectionAP->addText($inmueble['API'][0]['AIText11'],'ArialNarrowBlack12','paraJust');
                   
                   $sectionAP->addTextBreak(1);
                   $sectionAP->addText($inmueble['API'][0]['AITit12'],'ArialNarrowRedBold12','paraLeftT');
                   $sectionAP->addText($inmueble['API'][0]['AIText12_1'].$inmueble['API'][0]['AIText12_2'].$inmueble['API'][0]['AIText12_3'].$inmueble['API'][0]['AIText12_4'],'ArialNarrowBlack12','paraJust');
                   $sectionAP->addTextBreak(1);
                   if( !empty($inmueble['API'][0]['imgAPI']) )
                   {  $this->addmyImage($sectionAP, 'gallery/'.$infoFile['id_in'].'/'.$inmueble['API'][0]['imgAPI'], 450, 300,"center");
                      $sectionAP->addText("Surrounding Uses",'ArialNarrowRedBold12','paraCenterT');
                   }
                      
                   $sectionAP->addPageBreak();

                 }
                 
                 if( ($this->seccionReorte($apSections, "APII") === TRUE) )
                 { $numAp++;
                   $numAP_Romano = $this->converToRoman($numAp);
                   $sectionAP = $word->addSection();
                   
                   $this->addHeader($sectionAP, $appraisalNo, $numAP_Romano.' Subject Property', $fotoWatermark);
                   $this->addFooter($sectionAP, TRUE);
                   
                   $sectionAP->addTitle(htmlspecialchars($numAP_Romano.' Subject Property '), 3);
                   $sectionAP->addTextBreak(1);
                   
                   $sectionAP->addText($inmueble['APII'][0]['AIITit1'],'ArialNarrowRedBold12','paraLeftT');
                   $sectionAP->addText($location,'ArialNarrowBlack12','paraJust');
                   $imagen1 = FALSE;
                   $imagen2 = FALSE;
                   if( empty($inmueble['APII'][0]['imgApII_1']) )
                   {$sectionAP->addTextBreak(1);}
                   else
                   {  $sectionAP->addTextBreak(1);
                      $this->addmyImage($sectionAP, 'gallery/'.$infoFile['id_in'].'/'.$inmueble['APII'][0]['imgApII_1'], 450, 300,"center");
                      $sectionAP->addText("Subject's Location",'ArialNarrowRedBold12','paraCenterT');
                      $sectionAP->addTextBreak(1);
                      $imagen1 = TRUE;
                   }
                   
                   if( empty($inmueble['APII'][0]['AIIText2_1']) & empty($inmueble['APII'][0]['AIIText2_2']) )
                   {   }
                   else
                   {      
                       $sectionAP->addText($inmueble['APII'][0]['AIITit2'],'ArialNarrowRedBold12','paraLeftT');
                       $sectionAP->addText('Latitude: '.$inmueble['APII'][0]['AIIText2_1'].'                                Longitude: '.$inmueble['APII'][0]['AIIText2_2'],'ArialNarrowBlackBold12','paraCenterT');
                       $sectionAP->addTextBreak(1);
                   }
                   
                   if( empty($inmueble['APII'][0]['imgApII_2']) )
                   {   $sectionAP->addTextBreak(1); }
                   else
                   {  
                      $this->addmyImage($sectionAP, 'gallery/'.$infoFile['id_in'].'/'.$inmueble['APII'][0]['imgApII_2'], 450, 280,"center");
                      $sectionAP->addText("Subject's Location",'ArialNarrowRedBold12','paraCenterT');
                      $imagen2 = TRUE;                      
                   }
                  
                   if($imagen1 == TRUE & $imagen2 == TRUE)
                       { $sectionAP->addPageBreak(1); }
                   else
                       { $sectionAP->addTextBreak(1); }                   
                       
                   $sectionAP->addText($inmueble['APII'][0]['AIITit3'],'ArialNarrowRedBold12','paraLeftT');
                   $sectionAP->addTextBreak(1);
                   $sCellLineBtSpace= array('valign' => 'center','borderBottomSize' => 15,'borderBottomColor'=>'#CB0B0B','spaceAfter' => 0,'spaceBefore' => 0);
                   $tablaAPII_CA = $sectionAP->addTable(array('width' => 4000, 'unit' => 'pct', 'align' => 'center','cellMargin' => 0, 'cellMarginRight' => 0, 'cellMarginBottom' => 0, 'cellMarginLeft' => 0));
                   $tablaAPII_CA->addRow(650);
                   $tablaAPII_CA->addCell(3000, $sCellLineBottom)->addText($inmueble['land'][0]['deedTxt'],'ArialNarrowBlack12','paraLeftT'); 
                   $tablaAPII_CA->addCell(3000, $sCellLineBottom)->addText($totLand,'ArialNarrowBlackBold12','paraCenterT');
                   $tablaAPII_CA->addCell(3000, $sCellLineBottom)->addText($totLandFT,'ArialNarrowBlack12','paraCenterT');
                   $tablaAPII_CA->addRow(650);
                   $tablaAPII_CA->addCell(3000, $sCellLineBtSpace)->addText($inmueble['land'][0]['plansTxt'],'ArialNarrowBlack12','paraLeftT'); 
                   $tablaAPII_CA->addCell(3000, $sCellLineBtSpace)->addText(number_format($plans  , 2, '.', ',').' m2','ArialNarrowBlackBold12','paraCenterT');
                   $tablaAPII_CA->addCell(3000, $sCellLineBtSpace)->addText(number_format($plansFT, 2, '.', ',').' ft2','ArialNarrowBlack12','paraCenterT');
                   if($difLand == 0)
                   {
                    $tablaAPII_CA->addRow(650);
                    $tablaAPII_CA->addCell(3000, $sCellLineSinL)->addText("",'ArialNarrowBlack12','paraLeftT'); 
                    $tablaAPII_CA->addCell(3000, $sCellLineSinL)->addText("",'ArialNarrowBlackBold12','paraCenterT');
                    $tablaAPII_CA->addCell(3000, $sCellLineSinL)->addText("",'ArialNarrowBlack12','paraCenterT');
                   }else
                   {
                    $tablaAPII_CA->addRow(650);
                    $tablaAPII_CA->addCell(3000, $sCellLineSinL)->addText("Difference: ",'ArialNarrowBlack12','paraLeftT'); 
                    $tablaAPII_CA->addCell(3000, $sCellLineSinL)->addText($difLand,'ArialNarrowBlackBold12','paraCenterT');
                    $tablaAPII_CA->addCell(3000, $sCellLineSinL)->addText($difLandFT,'ArialNarrowBlack12','paraCenterT');
                   }
                   $sectionAP->addTextBreak(1);
                   $sectionAP->addText($inmueble['APII'][0]['AIIText3'],'ArialNarrowBlack12','paraJust');
                   $sectionAP->addTextBreak(1);
                   $sectionAP->addText($inmueble['APII'][0]['AIITit4'],'ArialNarrowRedBold12','paraLeftT');
                   $sectionAP->addText($inmueble['APII'][0]['AIIText4'],'ArialNarrowBlack12','paraJust');
                   
                   if($imagen1 == TRUE & $imagen2 == TRUE)
                       { $sectionAP->addPageBreak(1); }
                   else
                       { $sectionAP->addTextBreak(1); } 
                   $sectionAP->addText($inmueble['APII'][0]['AIITit5'],'ArialNarrowRedBold12','paraLeftT');
                   $sectionAP->addText($inmueble['APII'][0]['AIIText5'],'ArialNarrowBlack12','paraJust');
                   $sectionAP->addTextBreak(1);

                   $styleCellLine = array('valign' => 'center','borderSize'       => 10,'borderColor'      =>'#CB0B0B');
                   $sCellLineBot  = array('valign' => 'center','borderBottomSize' => 15,'borderTopSize'    => 10,'borderRightSize' => 10,'borderLeftSize'   => 10,'borderBottomColor'=>'#CB0B0B','borderTopColor'=>'#CB0B0B','borderLeftColor'=>'#CB0B0B','borderRightColor'=>'#CB0B0B');
                   $sCellLineRi   = array('valign' => 'center','borderRightSize'  => 15,'borderTopSize'    => 10,'borderLeftSize'  => 10,'borderBottomSize' => 10,'borderBottomColor'=>'#CB0B0B','borderTopColor'=>'#CB0B0B','borderLeftColor'=>'#CB0B0B','borderRightColor'=>'#CB0B0B');
                   $sCellLineRiBot= array('valign' => 'center','borderRightSize'  => 15,'borderTopSize'    => 10,'borderLeftSize'  => 10,'borderBottomSize' => 15,'borderBottomColor'=>'#CB0B0B','borderTopColor'=>'#CB0B0B','borderLeftColor'=>'#CB0B0B','borderRightColor'=>'#CB0B0B');
                   
                   $tablaAPII = $sectionAP->addTable(array('cellMargin' => 0, 'cellMarginRight' => 0, 'cellMarginBottom' => 0, 'cellMarginLeft' => 0));
                   $tablaAPII->addRow(650);
                   $tablaAPII->addCell(1000, $sCellLineRiBot)->addText("No",'ArialNarrowBlackBold10','paraCenterT'); 
                   $tablaAPII->addCell(3000, $sCellLineBot)  ->addText("Area",'ArialNarrowBlackBold10','paraCenterT');
                   foreach($inmueble['ACA']['columns'] as $c)
                   { $tablaAPII->addCell(100, $sCellLineBot)->addText($c['type_area'].' '.$c['name_area'],'ArialNarrowBlack10','paraCenterT'); }
                   $tablaAPII->addCell(3000, $sCellLineBot)->addText("Total Area (sqm)",'ArialNarrowBlackBold10','paraCenterT');                   
                   
                   $rowId = 0;
                   foreach($inmueble['ACA']['rows'] as $r)
                   {   $rowId++;
                       $subArea  = 0;
                       $columnId = 0;
                       $tablaAPII->addRow(650);
                       $tablaAPII->addCell(1000, $sCellLineRi)->addText($rowId,'ArialNarrowBlackBold10','paraCenterT'); 
                       $tablaAPII->addCell(3000, $styleCellLine)->addText($r['zone'],'ArialNarrowBlack10','paraLeftT');
                       foreach ($inmueble['ACA']['columns'] as $c)
                        {  $columnId++;
                           $dato      = $md_inmuebles->traCA($inmueble['ACA']['id_in'],$columnId.$rowId);
                           $subArea = $subArea + $dato;
                           $celda     = empty($dato)?"":number_format($dato, 2, '.', ',');
                           $tablaAPII->addCell(3000, $styleCellLine)->addText($celda,'ArialNarrowBlack10','paraCenterT');      
                        }                   
                      $tablaAPII->addCell(3000, $styleCellLine)->addText(number_format($subArea, 2, '.', ','),'ArialNarrowBlackBold10','paraCenterT');  
                   }
                   $tablaAPII->addRow(650);
                   $tablaAPII->addCell(1000, $sCellLineRi)->addText('','ArialNarrowBlackBold9','paraCenterT'); 
                   $tablaAPII->addCell(3000, $styleCellLine)->addText("Sum",'ArialNarrowBlackBold10','paraLeftT');
                   $columnId = 0;
                   $subTotal = 0;
                   foreach ($inmueble['ACA']['columns'] as $c)
                    {   $columnId++; 
                        $st = $md_inmuebles->traSumCAXColumn($inmueble['ACA']['id_in'],$columnId);
                        $subTotal = $subTotal + $st;
                        $tablaAPII->addCell(3000, $styleCellLine)->addText(number_format($st, 2, '.', ','),'ArialNarrowBlackBold10','paraCenterT');
                    }
                    $tablaAPII->addCell(3000, $styleCellLine)->addText(number_format($subTotal, 2, '.', ','),'ArialNarrowBlackBold10','paraCenterT');
                    
                    $sectionAP->addTextBreak(1);
                    $gfa = empty($inmueble['totArea'][0])?0:$inmueble['totArea'][0]['totCA'];
                    $gba = empty($inmueble['totArea'][0])?0:$inmueble['totArea'][0]['totGBA'];
                    $ra  = empty($inmueble['totArea'][0])?0:$inmueble['totArea'][0]['totTRA'];                    
                    $tablaAPII_to = $sectionAP->addTable(array('width' => 4500, 'unit' => 'pct', 'align' => 'center','cellMargin' => 0, 'cellMarginRight' => 0, 'cellMarginBottom' => 0, 'cellMarginLeft' => 0));
                    $tablaAPII_to->addRow(650);
                    $tablaAPII_to->addCell(3000, $sCellLineBottom)->addText("Ground Floor Area ",'ArialNarrowBlackBold12','paraLeftT'); 
                    $tablaAPII_to->addCell(3000, $sCellLineBottom)->addText(number_format($gfa, 2, '.', ',')."m2",'ArialNarrowBlackBold12','paraCenterT');
                    $tablaAPII_to->addCell(3000, $sCellLineBottom)->addText(number_format($gfa*$inmueble['dc']['ft'], 2, '.', ',')."ft2",'ArialNarrowBlack12','paraCenterT');
                    $tablaAPII_to->addRow(650);
                    $tablaAPII_to->addCell(3000, $sCellLineBtSpace)->addText("Gross Building Area ",'ArialNarrowBlackBold12','paraLeftT'); 
                    $tablaAPII_to->addCell(3000, $sCellLineBottom)->addText(number_format($gba, 2, '.', ',')."m2",'ArialNarrowBlackBold12','paraCenterT');
                    $tablaAPII_to->addCell(3000, $sCellLineBottom)->addText(number_format($gba*$inmueble['dc']['ft'], 2, '.', ',')."ft2",'ArialNarrowBlack12','paraCenterT');
                    $tablaAPII_to->addRow(650); 
                    $tablaAPII_to->addCell(3000, $sCellLineBtSpace)->addText("Rentable Area ",'ArialNarrowBlackBold12','paraLeftT'); 
                    $tablaAPII_to->addCell(3000, $sCellLineBottom)->addText(number_format($ra, 2, '.', ',')."m2",'ArialNarrowBlackBold12','paraCenterT');
                    $tablaAPII_to->addCell(3000, $sCellLineBottom)->addText(number_format($ra*$inmueble['dc']['ft'], 2, '.', ',')."ft2",'ArialNarrowBlack12','paraCenterT');
                    
                   $sectionAP->addTextBreak(1);
                   $sectionAP->addText($inmueble['APII'][0]['AIITit6'],'ArialNarrowRedBold12','paraLeftT');
                   $sectionAP->addText($inmueble['APII'][0]['AIIText6'],'ArialNarrowBlack12','paraJust');
                   
                   $sectionAP->addPageBreak();
                   $sectionAP->addText($inmueble['APII'][0]['AIITit7'],'ArialNarrowRedBold12','paraLeftT');
                   $sectionAP->addText($inmueble['APII'][0]['AIIText7_1'],'ArialNarrowBlack12','paraJust');
                   if( empty($inmueble['APII'][0]['imgApII_3']) )
                   {$sectionAP->addTextBreak(1);}
                   else
                   {  $this->addmyImage($sectionAP, 'gallery/'.$infoFile['id_in'].'/'.$inmueble['APII'][0]['imgApII_3'], 450, 300,"center");
                      $sectionAP->addText("Property Plans",'ArialNarrowRedBold12','paraCenterT');
                      $sectionAP->addTextBreak(1);
                   }
                   
                   if(sizeof($inmueble['ACA']['ae']) > 0 | sizeof($inmueble['ACA']['cw']) > 0)
                   {
                        $sectionAP->addText($inmueble['APII'][0]['AIIText7_2'],'ArialNarrowBlack12','paraJust');
                        $sectionAP->addTextBreak(1);

                        $sCellLineBottomSpan3 = array('valign' => 'center','borderBottomSize' => 15,'borderBottomColor'=>'#CB0B0B','spaceAfter' => 0,'spaceBefore' => 0,'gridSpan' => 3);
                        $sCellLinetopBot      = array('valign' => 'center','borderTopSize'    => 15,'borderBottomSize' => 15,'borderBottomColor'=>'#CB0B0B','borderTopColor'=>'#CB0B0B');
                        $sCellLineBottomThin  = array('valign' => 'center','borderBottomSize' => 10,'borderBottomColor'=>'#D0D0D0','spaceAfter' => 0,'spaceBefore' => 0);
                        $tablaAPII_AE = $sectionAP->addTable(array('cellMargin' => 0, 'cellMarginRight' => 0, 'cellMarginBottom' => 0, 'cellMarginLeft' => 0));
                        $tablaAPII_AE->addRow();
                        $tablaAPII_AE->addCell(1000, $sCellLineSinL)->addText(" ",'ArialNarrowBlackBold10','paraLeftT'); 
                        $tablaAPII_AE->addCell(9000, $sCellLineBottomSpan3)->addText("Equipment",'ArialNarrowBlackBold10','paraLeftT'); 
                        $tablaAPII_AE->addRow();
                        $tablaAPII_AE->addCell(1000, $sCellLinetopBot)->addText(" ",'ArialNarrowBlackBold10','paraLeftT'); 
                        $tablaAPII_AE->addCell(5000, $sCellLinetopBot)->addText("Item",'ArialNarrowBlackBold10','paraCenterT'); 
                        $tablaAPII_AE->addCell(2000, $sCellLinetopBot)->addText("Unit",'ArialNarrowBlackBold10','paraCenterT'); 
                        $tablaAPII_AE->addCell(2000, $sCellLinetopBot)->addText("Quantity",'ArialNarrowBlackBold10','paraCenterT'); 

                        $x=0;                   
                        foreach ($inmueble['ACA']['ae'] as $ae) {
                            $x++;
                            $tablaAPII_AE->addRow();
                            $tablaAPII_AE->addCell(1000, $sCellLineBottomThin)->addText($x,'ArialNarrowBlack10','paraLeftT'); 
                            $tablaAPII_AE->addCell(5000, $sCellLineBottomThin)->addText($ae['leyenda'],'ArialNarrowBlack10','paraLeftT');
                            $tablaAPII_AE->addCell(2000, $sCellLineBottomThin)->addText($ae['unit'],'ArialNarrowBlack10','paraCenterT');
                            $tablaAPII_AE->addCell(2000, $sCellLineBottomThin)->addText(number_format($ae['amount'], 0, '.', ','),'ArialNarrowBlack9','paraCenterT');
                        }                   
                        $tablaAPII_AE->addRow(650);
                        $tablaAPII_AE->addCell(1000, $sCellLineSinL)->addText(" ",'ArialNarrowBlackBold10','paraLeftT'); 
                        $tablaAPII_AE->addCell(5000, $sCellLineSinL)->addText(" ",'ArialNarrowBlackBold10','paraLeftT'); 
                        $tablaAPII_AE->addCell(2000, $sCellLineSinL)->addText(" ",'ArialNarrowBlackBold10','paraLeftT'); 
                        $tablaAPII_AE->addCell(2000, $sCellLineSinL)->addText(" ",'ArialNarrowBlackBold10','paraLeftT'); 
                        $tablaAPII_AE->addRow();
                        $tablaAPII_AE->addCell(1000, $sCellLineSinL)->addText(" ",'ArialNarrowBlackBold10','paraLeftT'); 
                        $tablaAPII_AE->addCell(9000, $sCellLineBottomSpan3)->addText("Complementary works and special amenities",'ArialNarrowBlackBold9','paraLeftT'); 
                        $tablaAPII_AE->addRow();
                        $tablaAPII_AE->addCell(1000, $sCellLinetopBot)->addText(" ",'ArialNarrowBlackBold10','paraLeftT'); 
                        $tablaAPII_AE->addCell(5000, $sCellLinetopBot)->addText("Item",'ArialNarrowBlackBold10','paraCenterT'); 
                        $tablaAPII_AE->addCell(2000, $sCellLinetopBot)->addText("Unit",'ArialNarrowBlackBold10','paraCenterT'); 
                        $tablaAPII_AE->addCell(2000, $sCellLinetopBot)->addText("Quantity",'ArialNarrowBlackBold10','paraCenterT'); 

                        $x=0;
                        foreach ($inmueble['ACA']['cw'] as $cw) {
                            $x++;
                            $tablaAPII_AE->addRow();
                            $tablaAPII_AE->addCell(1000, $sCellLineBottomThin)->addText($x,'ArialNarrowBlack10','paraLeftT'); 
                            $tablaAPII_AE->addCell(5000, $sCellLineBottomThin)->addText($cw['leyenda'],'ArialNarrowBlack10','paraLeftT');
                            $tablaAPII_AE->addCell(2000, $sCellLineBottomThin)->addText($cw['unit'],'ArialNarrowBlack10','paraCenterT'); 
                            $tablaAPII_AE->addCell(2000, $sCellLineBottomThin)->addText(number_format($cw['amount'], 0, '.', ','),'ArialNarrowBlack10','paraCenterT');
                        }
                   }
                   
                   $sectionAP->addPageBreak();
                   
                 }
                 
                 if( ($this->seccionReorte($apSections, "APIII") === TRUE) )
                 { $numAp++;
                   $numAP_Romano = $this->converToRoman($numAp);
                   $sectionAP    = $word->addSection();

                   $this->addHeader($sectionAP, $appraisalNo, $numAP_Romano.' Appraisal Process and Methodology', $fotoWatermark);
                   $this->addFooter($sectionAP, TRUE);

                   $sectionAP->addTitle(htmlspecialchars($numAP_Romano.' Appraisal Process and Methodology '), 3);
                   $sectionAP->addTextBreak(2);
                                      
                   $sectionAP->addText("",'ArialNarrowBlack12','paraJust');
                   \PhpOffice\PhpWord\Shared\Html::addHtml($sectionAP, $inmueble['APIII'][0]['AIIIText1']);
                   
                   $sectionAP->addPageBreak();
                 }
                 
                 if( ($this->seccionReorte($apSections, "APIV") === TRUE) )
                 { $numAp++;
                   $numAP_Romano = $this->converToRoman($numAp);
                   $sectionAP    = $word->addSection();                   

                   $this->addHeader($sectionAP, $appraisalNo, $numAP_Romano.' Highest and best use', $fotoWatermark);
                   $this->addFooter($sectionAP, TRUE);

                   $sectionAP->addTitle(htmlspecialchars($numAP_Romano.' Highest and best use '), 3);
                   
                   $sectionAP->addTextBreak(1);                   
                   $sectionAP->addText($inmueble['APIV'][0]['AIVTit1'],'ArialNarrowRedBold12','paraLeftT');
                   $sectionAP->addText($inmueble['APIV'][0]['AIVText1'],'ArialNarrowBlack12','paraJust');
                   
                   $sectionAP->addTextBreak(1);
                   $sectionAP->addText($inmueble['APIV'][0]['AIVTit2'],'ArialNarrowRedBold12','paraLeftT');                   
                   \PhpOffice\PhpWord\Shared\Html::addHtml($sectionAP, $inmueble['APIV'][0]['AIVText2']);
                   
                   $sectionAP->addTextBreak(1);
                   $sectionAP->addText($inmueble['APIV'][0]['AIVTit3'],'ArialNarrowRedBold12','paraLeftT');
                   $sectionAP->addText($inmueble['APIV'][0]['AIVText3'],'ArialNarrowBlack12','paraJust');
                   
                   $sectionAP->addTextBreak(1);
                   $sectionAP->addText($inmueble['APIV'][0]['AIVTit4'],'ArialNarrowRedBold12','paraLeftT');
                   $sectionAP->addText($inmueble['APIV'][0]['AIVText4'],'ArialNarrowBlack12','paraJust');
                   
                   $sectionAP->addPageBreak();
                 }
                 
                 
                 if( ($this->seccionReorte($apSections, "APV") === TRUE) )
                 { $numAp++;
                   $numAP_Romano = $this->converToRoman($numAp);
                   $nombreSecc   = $numAP_Romano.' Cost Approach';
                   $sectionAP    = $word->addSection();
                   
                   $this->addHeader($sectionAP, $appraisalNo, $nombreSecc, $fotoWatermark);
                   $this->addFooter($sectionAP, TRUE);
                   
                   $sectionAP->addTitle(htmlspecialchars($nombreSecc), 3);                   
                   $numAp = $this->creaCoreAPVWORD($sectionAP,$inmueble,$md_inmuebles,$numAp,$plans,$plansFT,  empty($inmueble['totArea'])?0:$inmueble['totArea'][0]['totTRA']);
                   
                   $sectionAP->addPageBreak();
                 }
                 
                 if( ($this->seccionReorte($apSections, "APVI") === TRUE) )
                 { $numAp++;
                   $numAP_Romano = $this->converToRoman($numAp);
                   $nombreSecc   = $numAP_Romano.' Income Capitalization Approach';
                   $sectionAP    = $word->addSection();
                   
                   $this->addHeader($sectionAP, $appraisalNo, $nombreSecc, $fotoWatermark);
                   $this->addFooter($sectionAP, TRUE);
                   
                   $sectionAP->addTitle(htmlspecialchars($nombreSecc), 3);
                   $this->creaCoreAPVIWORD($sectionAP,$inmueble);
                   
                   $sectionAP->addPageBreak();
                 }
                 
                 $nombreSecc3 = "";
                 $nombreSecc1 = "";
                 $nombreSecc2 = "";                 
                 $nuevaSec    = FALSE;
                 
                 if( ($this->seccionReorte($apSections, "APVII") === TRUE) )
                 { $numAp++;
                   $numAP_Romano = $this->converToRoman($numAp);
                   $nombreSecc1   = $numAP_Romano.' Sales Comparison Approach';
                 }
                 
                if( ($this->seccionReorte($apSections, "APVIII") === TRUE) )
                 {  $numAp++;
                    $numAP_Romano = $this->converToRoman($numAp);                    
                    $nombreSecc2 = $numAP_Romano.' Value in Use';                    
                  }                
                
                if( ($this->seccionReorte($apSections, "APIX") === TRUE) )  
                 {  $numAp++;
                    $numAP_Romano = $this->converToRoman($numAp);                    
                    $nombreSecc3 = $numAP_Romano.' Summary of Values';
                    $nombreSecc  = $nombreSecc1.' and '.$numAP_Romano.' Value in Use and '.$numAP_Romano.' Summary of Values';
                  }
                  
                $nombreSecc  = $nombreSecc1." ".$nombreSecc2." ".$nombreSecc3;
                 
                 if( ($this->seccionReorte($apSections, "APVII") === TRUE) )
                 { 
                   $sectionAP    = $word->addSection();
                   $nuevaSec     = TRUE;
                   $this->addHeader($sectionAP, $appraisalNo, $nombreSecc, $fotoWatermark);
                   $this->addFooter($sectionAP, TRUE);
                   
                   $sectionAP->addTitle(htmlspecialchars($nombreSecc1), 3);
                                      
                   \PhpOffice\PhpWord\Shared\Html::addHtml($sectionAP, $inmueble['APVII'][0]['AVIIText1']);                   
                   \PhpOffice\PhpWord\Shared\Html::addHtml($sectionAP, $inmueble['APVII'][0]['AVIIText2']);
                   
                   $ra           = empty($inmueble['totArea'][0])?0:$inmueble['totArea'][0]['totTRA'];
                   $uav          = empty($inmueble['iv']['indicated_value_crs'])?0:$inmueble['iv']['indicated_value_crs'];
                   $ivusca       = $ra * $uav;
                   $sectionAP->addTextBreak(1);
                   $tblResumen = $sectionAP->addTable(array('cellMargin' => 0, 'cellMarginRight' => 0, 'cellMarginBottom' => 0, 'cellMarginLeft' => 0));                                       
                   $tblResumen->addRow();
                   $tblResumen->addCell(8000, $cellLineBotThin)->addText($param['saop'],'ArialNarrowBlack12','paragraphLeft'); 
                   $tblResumen->addCell(4000, $cellLineBotThin)->addText(number_format($ra, 2, '.', ',').' m2','ArialNarrowBlack12','paragraphCenter'); 
                   $tblResumen->addRow();
                   $tblResumen->addCell(8000, $cellLineBotThin)->addText($param['uav'],'ArialNarrowBlack12','paragraphLeft'); 
                   $tblResumen->addCell(4000, $cellLineBotThin)->addText('$ '.number_format($uav, 2, '.', ',').' usd','ArialNarrowBlack12','paragraphCenter'); 
                   $tblResumen->addRow();
                   $tblResumen->addCell(8000, $cellLineBotThin)->addText($param['ivusca'],'ArialNarrowBlack12','paragraphLeft'); 
                   $tblResumen->addCell(4000, $cellLineBotThin)->addText('$ '.number_format($ivusca, 2, '.', ',').' usd','ArialNarrowBlack12','paragraphCenter'); 
                   $tblResumen->addRow(750);
                   $tblResumen->addCell(8000, $sCellLineBtSpace)->addText($param['roun'],'ArialNarrowBlackBold12','paragraphLeft'); 
                   $tblResumen->addCell(4000, $sCellLineBtSpace)->addText('$ '.number_format($this->redondearHermes($ivusca), 2, '.', ',').' usd','ArialNarrowBlackBold12','paragraphCenter'); 
                   $sectionAP->addTextBreak(2);
                 }

                 if( ($this->seccionReorte($apSections, "APVIII") === TRUE) )
                 { 
                   if($nuevaSec===FALSE)
                   {
                    $sectionAP    = $word->addSection();
                    $nuevaSec     = TRUE;
                    $this->addHeader($sectionAP, $appraisalNo, $nombreSecc, $fotoWatermark);
                    $this->addFooter($sectionAP, TRUE);
                   }
                   
                   $sectionAP->addTitle(htmlspecialchars($nombreSecc2), 3);                   
                   \PhpOffice\PhpWord\Shared\Html::addHtml($sectionAP, $inmueble['APVII'][0]['AVIIIText1']);
                   $sectionAP->addTextBreak(1);
                   $cavweo = $inmueble['APV']['datos'][0]['AV_vica'] + $inmueble['APV']['datos'][0]['AV_oe'];
                   $tblResumen = $sectionAP->addTable(array('cellMargin' => 0, 'cellMarginRight' => 0, 'cellMarginBottom' => 0, 'cellMarginLeft' => 0));
                   $tblResumen->addRow();
                   $tblResumen->addCell(8000, $cellLineBotThin)->addText($param['cavweo'],'ArialNarrowBlack12','paragraphLeft'); 
                   $tblResumen->addCell(4000, $cellLineBotThin)->addText('$ '.number_format(empty($cavweo)?0:$cavweo, 2, '.', ',').' usd','ArialNarrowBlack12','paragraphCenter'); 
                   $tblResumen->addRow(750);
                   $tblResumen->addCell(8000, $sCellLineBtSpace)->addText($param['roun'],'ArialNarrowBlackBold12','paragraphLeft'); 
                   $tblResumen->addCell(4000, $sCellLineBtSpace)->addText('$ '.number_format($this->redondearHermes(empty($cavweo)?0:$cavweo), 2, '.', ',').' usd','ArialNarrowBlackBold12','paragraphCenter');                    
                   $sectionAP->addTextBreak(2);
                 }

                $totTRA = empty($totTRA)?1:$totTRA;
                $ca        = empty($inmueble['APV']['datos'][0]['AV_vica'])?0:$inmueble['APV']['datos'][0]['AV_vica'];
                $caUV      = $ca / $totTRA;
                $ivudicael = empty($inmueble['APVI'][0]['AVIivudicael'])?0:$inmueble['APVI'][0]['AVIivudicael'];                   
                $incomeUV  = $ivudicael / $totTRA;
                $uav       = empty($inmueble['iv']['indicated_value_crs'])?0:$inmueble['iv']['indicated_value_crs'];                
                $cavweo    = $inmueble['APV']['datos'][0]['AV_vica'] + $inmueble['APV']['datos'][0]['AV_oe'];
                $cavweoUV  = $cavweo    / $totTRA;
                $dcf       = empty($inmueble['APVII'][0]['AXnum3'])?0:$inmueble['APVII'][0]['AXnum3'];
                $dcfUV     = $dcf / $totTRA;
                $totTRA = empty($totTRA)?0:$totTRA;
                $ivusca    = $uav * $totTRA;
                $ivuscaUV  = $uav;
                   
                 if( ($this->seccionReorte($apSections, "APIX") === TRUE) )
                 { 
                   if($nuevaSec===FALSE)
                   {
                    $sectionAP    = $word->addSection();
                    $nuevaSec     = TRUE;
                    $this->addHeader($sectionAP, $appraisalNo, $nombreSecc, $fotoWatermark);
                    $this->addFooter($sectionAP, TRUE);
                   }
                   
                   $sectionAP->addTitle(htmlspecialchars($nombreSecc3), 3);                                      
                   \PhpOffice\PhpWord\Shared\Html::addHtml($sectionAP, $inmueble['APVII'][0]['AIXText1']);
                   

                   $tblResumen = $sectionAP->addTable(array('cellMargin' => 0, 'cellMarginRight' => 0, 'cellMarginBottom' => 0, 'cellMarginLeft' => 0));
                   $tblResumen->addRow();
                   $tblResumen->addCell(4000, $sCellLineBtSpace)->addText($param['valAp'],'ArialNarrowBlackBold12','paragraphCenter'); 
                   $tblResumen->addCell(4000, $sCellLineBtSpace)->addText(strtoupper($param['currency']),'ArialNarrowBlackBold12','paragraphCenter'); 
                   $tblResumen->addCell(4000, $sCellLineBtSpace)->addText($param['tUV'],'ArialNarrowBlackBold12','paragraphCenter');
                   if( ($this->seccionReorte($apSections, "APV") === TRUE) & $ca!=0 )
                   {
                   $tblResumen->addRow();
                   $tblResumen->addCell(4000, $sCellLineBtSpace)->addText($param['costAp'],'ArialNarrowBlackBold12','paragraphLeft');
                   $tblResumen->addCell(4000, $sCellLineBtSpace)->addText('$ '.number_format($ca, 2, '.', ',').' '.$param['currency'],'ArialNarrowBlackBold12','paragraphCenter');
                   $tblResumen->addCell(4000, $sCellLineBtSpace)->addText('$ '.number_format($caUV, 2, '.', ',').' '.$param['currency'].'/sqm','ArialNarrowBlackBold12','paragraphCenter');
                   }
                   if( ($this->seccionReorte($apSections, "APVI") === TRUE) & $ivudicael!=0 )
                   {
                   $tblResumen->addRow();
                   $tblResumen->addCell(4000, $sCellLineBtSpace)->addText($param['ica'],'ArialNarrowBlackBold12','paragraphLeft');
                   $tblResumen->addCell(4000, $sCellLineBtSpace)->addText('$ '.number_format($ivudicael, 2, '.', ',').' '.$param['currency'],'ArialNarrowBlackBold12','paragraphCenter');
                   $tblResumen->addCell(4000, $sCellLineBtSpace)->addText('$ '.number_format($incomeUV, 2, '.', ',').' '.$param['currency'].'/sqm','ArialNarrowBlackBold12','paragraphCenter');                   
                   }
                   if( $inmueble['APVII'][0]['AXTit3Chkbx']=="1" & $dcf !=0 )
                   {
                   $tblResumen->addRow();
                   $tblResumen->addCell(4000, $sCellLineBtSpace)->addText($inmueble['APVII'][0]['AXTit3'],'ArialNarrowBlackBold12','paragraphLeft');
                   $tblResumen->addCell(4000, $sCellLineBtSpace)->addText('$ '.number_format($dcf, 2, '.', ',').' '.$param['currency'],'ArialNarrowBlackBold12','paragraphCenter');
                   $tblResumen->addCell(4000, $sCellLineBtSpace)->addText('$ '.number_format($dcfUV, 2, '.', ',').' '.$param['currency'].'/sqm','ArialNarrowBlackBold12','paragraphCenter');
                   }
                   if( ($this->seccionReorte($apSections, "APVII") === TRUE) & $ivusca!=0 )
                   {
                   $tblResumen->addRow();
                   $tblResumen->addCell(4000, $sCellLineBtSpace)->addText($param['sca'],'ArialNarrowBlackBold12','paragraphLeft');
                   $tblResumen->addCell(4000, $sCellLineBtSpace)->addText('$ '.number_format($ivusca, 2, '.', ',').' '.$param['currency'],'ArialNarrowBlackBold12','paragraphCenter');
                   $tblResumen->addCell(4000, $sCellLineBtSpace)->addText('$ '.number_format($ivuscaUV, 2, '.', ',').' '.$param['currency'].'/sqm','ArialNarrowBlackBold12','paragraphCenter');
                   }
                   if( ($this->seccionReorte($apSections, "APVIII") === TRUE) & $cavweo!=0 )
                   {
                   $tblResumen->addRow();
                   $tblResumen->addCell(4000, $sCellLineBtSpace)->addText($param['vis'],'ArialNarrowBlackBold12','paragraphLeft');
                   $tblResumen->addCell(4000, $sCellLineBtSpace)->addText('$ '.number_format($cavweo, 2, '.', ',').' '.$param['currency'],'ArialNarrowBlackBold12','paragraphCenter');
                   $tblResumen->addCell(4000, $sCellLineBtSpace)->addText('$ '.number_format($cavweoUV, 2, '.', ',').' '.$param['currency'].'/sqm','ArialNarrowBlackBold12','paragraphCenter');
                   }
                   $sectionAP->addPageBreak();
                 }
                 
                 if( ($this->seccionReorte($apSections, "APX") === TRUE) )
                 { $numAp++;
                   $numAP_Romano = $this->converToRoman($numAp);
                   $sectionAP = $word->addSection();
                   
                   $this->addHeader($sectionAP, $appraisalNo, $numAP_Romano.' Reconciliation of Values (Market Value) ', $fotoWatermark);
                   $this->addFooter($sectionAP, TRUE);
                   
                   $sectionAP->addTitle(htmlspecialchars($numAP_Romano.' Reconciliation of Values (Market Value) '), 3);
                   
                   \PhpOffice\PhpWord\Shared\Html::addHtml($sectionAP, $inmueble['APVII'][0]['AXText1']);
                   \PhpOffice\PhpWord\Shared\Html::addHtml($sectionAP, $inmueble['APVII'][0]['AXText2']);    
                   $sectionAP->addTextBreak(1);
                   $ponderacionMV1 = ($inmueble['APVII'][0]['AXpor1']/100) * $ca;
                   $ponderacionMV2 = ($inmueble['APVII'][0]['AXpor2']/100) * $ivudicael;
                   $ponderacionMV3 = ($inmueble['APVII'][0]['AXpor3']/100) * $inmueble['APVII'][0]['AXnum3'];
                   $ponderacionMV5 = ($inmueble['APVII'][0]['AXpor5']/100) * $ivusca;
                   $AXmarketVal    = $ponderacionMV1 + $ponderacionMV2 + $ponderacionMV3  + $ponderacionMV5;
                   $ponderacionMV4 = ($inmueble['APVII'][0]['AXpor4']/100) * $cavweo;
                   
                   $cellLineWhite4Space = array('gridSpan' => 4,'valign' => 'center','borderBottomSize' => 10,'borderBottomColor'=>'#FFFFFF','spaceAfter' => 2,'spaceBefore' => 2);
                   $sCellLineBt3Space   = array('gridSpan' => 3,'valign' => 'center','borderBottomSize' => 15,'borderBottomColor'=>'#CB0B0B','spaceAfter' => 2,'spaceBefore' => 2);

                   $tblResumen = $sectionAP->addTable(array('cellMargin' => 0, 'cellMarginRight' => 0, 'cellMarginBottom' => 0, 'cellMarginLeft' => 0));
                   $tblResumen->addRow();
                   $tblResumen->addCell(12000, $cellLineWhite4Space)->addText($param['marval'],'ArialNarrowBlackBold12','paragraphLeft'); 
                   if( $inmueble['APVII'][0]['AXTit1Chkbx']=="1" & $ca !=0 )
                   {
                        $tblResumen->addRow(300);
                        $tblResumen->addCell(5000, $sCellLineBtSpace)->addText($inmueble['APVII'][0]['AXTit1'],'ArialNarrowBlack12','paragraphLeft');
                        $tblResumen->addCell(3000, $sCellLineBtSpace)->addText('$ '.number_format($ca, 2, '.', ','),'ArialNarrowBlack12','paragraphCenter'); 
                        $tblResumen->addCell(1000, $sCellLineBtSpace)->addText(number_format($inmueble['APVII'][0]['AXpor1'], 2, '.', ',').' %','ArialNarrowBlack12','paragraphCenter'); 
                        $tblResumen->addCell(3000, $sCellLineBtSpace)->addText('$ '.number_format($ponderacionMV1, 2, '.', ',').' '.$param['currency'],'ArialNarrowBlack12','paragraphRight'); 
                   }
                   if( $inmueble['APVII'][0]['AXTit2Chkbx']=="1" & $ivudicael !=0 )
                   {
                        $tblResumen->addRow(300);
                        $tblResumen->addCell(5000, $sCellLineBtSpace)->addText($inmueble['APVII'][0]['AXTit2'],'ArialNarrowBlack12','paragraphLeft');
                        $tblResumen->addCell(3000, $sCellLineBtSpace)->addText('$ '.number_format($ivudicael, 2, '.', ','),'ArialNarrowBlack12','paragraphCenter'); 
                        $tblResumen->addCell(1000, $sCellLineBtSpace)->addText(number_format($inmueble['APVII'][0]['AXpor2'], 2, '.', ',').' %','ArialNarrowBlack12','paragraphCenter'); 
                        $tblResumen->addCell(3000, $sCellLineBtSpace)->addText('$ '.number_format($ponderacionMV2, 2, '.', ',').' '.$param['currency'],'ArialNarrowBlack12','paragraphRight'); 
                   }
                   if( $inmueble['APVII'][0]['AXTit3Chkbx']=="1" & $inmueble['APVII'][0]['AXnum3'] !=0 )
                   {
                        $tblResumen->addRow(300);
                        $tblResumen->addCell(5000, $sCellLineBtSpace)->addText($inmueble['APVII'][0]['AXTit3'],'ArialNarrowBlack12','paragraphLeft');
                        $tblResumen->addCell(3000, $sCellLineBtSpace)->addText('$ '.number_format($inmueble['APVII'][0]['AXnum3'], 2, '.', ','),'ArialNarrowBlack12','paragraphCenter'); 
                        $tblResumen->addCell(1000, $sCellLineBtSpace)->addText(number_format($inmueble['APVII'][0]['AXpor3'], 2, '.', ',').' %','ArialNarrowBlack12','paragraphCenter'); 
                        $tblResumen->addCell(3000, $sCellLineBtSpace)->addText('$ '.number_format($ponderacionMV3, 2, '.', ',').' '.$param['currency'],'ArialNarrowBlack12','paragraphRight');
                   }
                   if( $inmueble['APVII'][0]['AXTit4Chkbx']=="1" & $ivusca !=0 )
                   {
                        $tblResumen->addRow(300);
                        $tblResumen->addCell(5000, $sCellLineBtSpace)->addText($inmueble['APVII'][0]['AXTit4'],'ArialNarrowBlack12','paragraphLeft');
                        $tblResumen->addCell(3000, $sCellLineBtSpace)->addText('$ '.number_format($ivusca, 2, '.', ','),'ArialNarrowBlack12','paragraphCenter'); 
                        $tblResumen->addCell(1000, $sCellLineBtSpace)->addText(number_format($inmueble['APVII'][0]['AXpor5'], 2, '.', ',').' %','ArialNarrowBlack12','paragraphCenter'); 
                        $tblResumen->addCell(3000, $sCellLineBtSpace)->addText('$ '.number_format($ponderacionMV5, 2, '.', ',').' '.$param['currency'],'ArialNarrowBlack12','paragraphRight');
                   }     
                   $tblResumen->addRow();
                   $tblResumen->addCell(5000, $sCellLineBt3Space)->addText($param['marval'].'of the property','ArialNarrowBlackBold12','paragraphLeft');                   
                   $tblResumen->addCell(3000, $sCellLineBtSpace)->addText('$ '.number_format($AXmarketVal, 2, '.', ',').' '.$param['currency'],'ArialNarrowBlack12','paragraphRight');
                   $tblResumen->addRow(750);
                   $tblResumen->addCell(5000, $sCellLineBt3Space)->addText($param['roun'],'ArialNarrowBlackBold12','paragraphLeft');                   
                   $tblResumen->addCell(3000, $sCellLineBtSpace)->addText('$ '.number_format($this->redondearHermes(empty($AXmarketVal)?0:$AXmarketVal), 2, '.', ',').' '.$param['currency'],'ArialNarrowBlackBold12','paragraphRight');
                   $tblResumen->addRow(750);
                   $tblResumen->addCell(12000, $cellLineWhite4Space)->addText('','ArialNarrowBlackBold12','paragraphLeft');
                   
                   if( ($this->seccionReorte($apSections, "APVIII") === TRUE) & $cavweo != 0 )
                   {
                    $tblResumen->addRow();
                    $tblResumen->addCell(5000, $sCellLineBtSpace)->addText($param['vis'].': ','ArialNarrowBlackBold12','paragraphLeft');                                      
                    $tblResumen->addCell(3000, $sCellLineBtSpace)->addText('$ '.number_format(empty($cavweo)?0:$cavweo, 2, '.', ','),'ArialNarrowBlack12','paragraphCenter'); 
                    $tblResumen->addCell(1000, $sCellLineBtSpace)->addText(number_format($inmueble['APVII'][0]['AXpor4'], 2, '.', ',').' %','ArialNarrowBlack12','paragraphCenter'); 
                    $tblResumen->addCell(3000, $sCellLineBtSpace)->addText('$ '.number_format($ponderacionMV4, 2, '.', ',').' '.$param['currency'],'ArialNarrowBlack12','paragraphRight');
                    $tblResumen->addRow(750);
                    $tblResumen->addCell(5000, $sCellLineBt3Space)->addText($param['roun'],'ArialNarrowBlackBold12','paragraphLeft');                   
                    $tblResumen->addCell(3000, $sCellLineBtSpace)->addText('$ '.number_format($this->redondearHermes(empty($ponderacionMV4)?0:$ponderacionMV4), 2, '.', ',').' '.$param['currency'],'ArialNarrowBlackBold12','paragraphRight');
                   }
                   $sectionAP->addPageBreak();
                 }
                                  
                   $marketVal   = empty($inmueble['APVII'])?0:$inmueble['APVII'][0]['AXmarketVal'];
                   $totRate     = $inmueble['APVIII'][0]['AXIRate1'] + $inmueble['APVIII'][0]['AXIRate2'] + $inmueble['APVIII'][0]['AXIRate3'] + $inmueble['APVIII'][0]['AXIRate4'] + $inmueble['APVIII'][0]['AXIRate5'];
                   $difDR       = round($totRate, 0) - $inmueble['APVIII'][0]['AXIRate1'];
                   $ocdr        = $difDR + $inmueble['APVIII'][0]['AXIRate9'];
                   $ocdrMonth   = $ocdr / 12 ;
                   $AXISaleTerm = empty($inmueble['APVIII'][0]['AXISaleTerm'])?0:$inmueble['APVIII'][0]['AXISaleTerm'];
                   $mt          = (empty($inmueble['frontPage'][0]['mt'])?0:$inmueble['frontPage'][0]['mt']);
                   $restaMT     = ($mt- $AXISaleTerm); 
                   $restaMT     = $restaMT<0?($restaMT*-1):$restaMT;
                   $oppCos      = $marketVal - ( $marketVal / pow((1 + ( $ocdrMonth / 100)), $restaMT ) );
                   $oppCos      = empty($restaMT)?0:$marketVal - ( $marketVal / pow((1 + ($ocdrMonth / 100)),$restaMT) );
                   $rate6Month  = $inmueble['APVIII'][0]['AXIRate6'] / 12;
                   $manCost     = ($rate6Month/100) * $AXISaleTerm * $marketVal;
                   $saleCost    = ($inmueble['APVIII'][0]['AXIRate7']/100) * $marketVal;
                   $rate8Month  = $inmueble['APVIII'][0]['AXIRate8'] / 12;
                   $properTax   = ($rate8Month/100) * $AXISaleTerm * $marketVal;
                   $liquidVal   = $marketVal - $oppCos - $manCost - $saleCost - $properTax;
                   $liquidFactor= empty($marketVal)?0:($marketVal - $liquidVal) / $marketVal;
                 
                 if( ($this->seccionReorte($apSections, "APXI") === TRUE) )
                 { $numAp++;
                   $numAP_Romano = $this->converToRoman($numAp);
                   $nombreSecc   = $numAP_Romano.' Liquidation Value';
                   $sectionAP    = $word->addSection();
                   
                   $this->addHeader($sectionAP, $appraisalNo, $nombreSecc, $fotoWatermark);
                   $this->addFooter($sectionAP, TRUE);
                   
                   $sectionAP->addTitle(htmlspecialchars($nombreSecc), 3);                   
                   $param       = $this->leyendasAPV();
                   
                   $cellLineWhite4Space = array('gridSpan' => 4,'valign' => 'center','borderBottomSize' => 10,'borderBottomColor'=>'#FFFFFF','spaceAfter' => 0,'spaceBefore' => 0);
                   $cellLineWhite3Space = array('gridSpan' => 2,'valign' => 'center','borderBottomSize' => 10,'borderBottomColor'=>'#FFFFFF','spaceAfter' => 0,'spaceBefore' => 0);
                   $cellLineWhite2Space = array('gridSpan' => 3,'valign' => 'center','borderBottomSize' => 10,'borderBottomColor'=>'#FFFFFF','spaceAfter' => 0,'spaceBefore' => 0);
                   $cellLineWhite       = array(                'valign' => 'center','borderBottomSize' => 10,'borderBottomColor'=>'#FFFFFF','spaceAfter' => 0,'spaceBefore' => 0);
                   $sCellLineBt3Space   = array('gridSpan' => 3,'valign' => 'center','borderBottomSize' => 15,'borderBottomColor'=>'#CB0B0B','spaceAfter' => 0,'spaceBefore' => 0);
                   $cellColSquare1_1    = array(                 'borderBottomSize' => 15,'borderBottomColor'=>'#000000', 'borderTopSize' => 15,'borderTopColor'=>'#000000', 'borderLeftSize' => 15,'borderLeftColor'=>'#000000', 'borderRightSize' => 15,'borderRightColor'=>'#000000','valign' => 'center','spaceAfter' => 0,'spaceBefore' => 0);
                   $cellColSquareAllThin= array(                 'borderBottomSize' => 10,'borderBottomColor'=>'#000000', 'borderTopSize' => 10,'borderTopColor'=>'#000000', 'borderLeftSize' => 10,'borderLeftColor'=>'#000000', 'borderRightSize' => 10,'borderRightColor'=>'#000000','valign' => 'center','spaceAfter' => 0,'spaceBefore' => 0);
                   $cellColSquareSp2Thin= array('gridSpan' => 2 ,'borderBottomSize' => 10,'borderBottomColor'=>'#000000', 'borderTopSize' => 10,'borderTopColor'=>'#000000', 'borderLeftSize' => 10,'borderLeftColor'=>'#000000', 'borderRightSize' => 10,'borderRightColor'=>'#000000','valign' => 'center','spaceAfter' => 0,'spaceBefore' => 0);
                   $cellColSquareSp3Thin= array('gridSpan' => 3 ,'borderBottomSize' => 10,'borderBottomColor'=>'#000000', 'borderTopSize' => 10,'borderTopColor'=>'#000000', 'borderLeftSize' => 10,'borderLeftColor'=>'#000000', 'borderRightSize' => 10,'borderRightColor'=>'#000000','valign' => 'center','spaceAfter' => 0,'spaceBefore' => 0);
                   $cLineBottom         = array(                'valign' => 'center','borderBottomSize' => 15,'borderBottomColor'=>'#CB0B0B','spaceAfter' => 0,'spaceBefore' => 0);
                   $cLineBottomS2       = array('gridSpan' => 2,'valign' => 'center','borderBottomSize' => 15,'borderBottomColor'=>'#CB0B0B','spaceAfter' => 0,'spaceBefore' => 0);
                   
                   \PhpOffice\PhpWord\Shared\Html::addHtml($sectionAP, $inmueble['APVIII'][0]['AXIText1']);
                                                         
                   $tblResumen = $sectionAP->addTable(array('cellMargin' => 0, 'cellMarginRight' => 0, 'cellMarginBottom' => 0, 'cellMarginLeft' => 0));
                   $tblResumen->addRow();
                   $tblResumen->addCell(12000, $cellLineWhite3Space)->addText($inmueble['APVIII'][0]['AXIText2'],'ArialNarrowRedBold12','paraLeftT');                   
                   $tblResumen->addRow();
                   $tblResumen->addCell(8000, $cellLineWhite2Space)->addText($inmueble['APVIII'][0]['AXIText3'],'ArialNarrowBlackBold12','paraLeftT'); 
                   $tblResumen->addCell(4000, $cellLineWhite      )->addText('$ '.number_format($marketVal, 2, '.', ',').' '.$param['currency'],'ArialNarrowBlackBold12','paraLeftT'); 
                   $tblResumen->addRow();
                   $tblResumen->addCell(12000, $cellLineWhite3Space)->addText('','ArialNarrowRedBold12','paraLeftT');
                   $tblResumen->addRow();
                   $tblResumen->addCell(12000, $cellLineWhite3Space)->addText($inmueble['APVIII'][0]['AXIText4'],'ArialNarrowRedBold12','paraLeftT'); 
                   $tblResumen->addRow();                   
                   $tblResumen->addCell(12000, $cellLineWhite3Space)->addText($inmueble['APVIII'][0]['AXIText5'],'$ArialNarrowBlack11','paraLeftT'); 
                   $tblResumen->addRow();                   
                   $tblResumen->addCell(12000, $cellLineWhite3Space)->addText($inmueble['APVIII'][0]['AXIText6'],'$ArialNarrowBlack11','paraLeftT'); 
                   $tblResumen->addRow();                   
                   $tblResumen->addCell(12000, $cellLineWhite3Space)->addText($inmueble['APVIII'][0]['AXIText7'],'$ArialNarrowBlack11','paraLeftT');
                   $tblResumen->addRow();
                   $tblResumen->addCell(9000, $cellLineWhite2Space)->addText($param['sugRate'].'  ','$ArialNarrowBlack11','paraRightT'); 
                   $tblResumen->addCell(3000, $cellLineWhite      )->addText(number_format(empty($inmueble['APVIII'][0]['AXIRate1'])?0:$inmueble['APVIII'][0]['AXIRate1'], 2, '.', ',').' %','$ArialNarrowBlack11','paraLeftT'); 
                   $tblResumen->addRow();                   
                   $tblResumen->addCell(12000, $cellLineWhite3Space)->addText($inmueble['APVIII'][0]['AXIText8'],'$ArialNarrowBlack11','paraLeftT'); 
                   $tblResumen->addRow();                   
                   $tblResumen->addCell(18000, $cellLineWhite3Space)->addText($inmueble['APVIII'][0]['AXIText9'],'$ArialNarrowBlack11','paraJust'); 
                   $tblResumen->addRow();                   
                   $tblResumen->addCell(18000, $cellLineWhite3Space)->addText($inmueble['APVIII'][0]['AXIText10'],'$ArialNarrowBlack11','paraJust');
                   $tblResumen->addRow();
                   $tblResumen->addCell(9000, $cellLineWhite2Space)->addText($param['sugRate'].'  ','$ArialNarrowBlack11','paraRightT'); 
                   $tblResumen->addCell(3000, $cellLineWhite      )->addText(number_format(empty($inmueble['APVIII'][0]['AXIRate2'])?0:$inmueble['APVIII'][0]['AXIRate2'], 2, '.', ',').' %','$ArialNarrowBlack11','paraLeftT'); 
                   $tblResumen->addRow();                   
                   $tblResumen->addCell(12000, $cellLineWhite3Space)->addText($inmueble['APVIII'][0]['AXIText11'],'$ArialNarrowBlack11','paraLeftT'); 
                   $tblResumen->addRow();                   
                   $tblResumen->addCell(18000, $cellLineWhite3Space)->addText($inmueble['APVIII'][0]['AXIText12'],'$ArialNarrowBlack11','paraJust'); 
                   $tblResumen->addRow();                   
                   $tblResumen->addCell(18000, $cellLineWhite3Space)->addText($inmueble['APVIII'][0]['AXIText13'],'$ArialNarrowBlack11','paraJust');
                   $tblResumen->addRow();
                   $tblResumen->addCell(9000, $cellLineWhite2Space)->addText($param['sugRate'].'  ','$ArialNarrowBlack11','paraRightT'); 
                   $tblResumen->addCell(3000, $cellLineWhite      )->addText(number_format(empty($inmueble['APVIII'][0]['AXIRate3'])?0:$inmueble['APVIII'][0]['AXIRate3'], 2, '.', ',').' %','$ArialNarrowBlack11','paraLeftT');                    
                   $tblResumen->addRow();
                   $tblResumen->addCell(12000, $cellLineWhite3Space)->addText($inmueble['APVIII'][0]['AXIText14'],'$ArialNarrowBlack11','paraLeftT'); 
                   $tblResumen->addRow();                   
                   $tblResumen->addCell(18000, $cellLineWhite3Space)->addText($inmueble['APVIII'][0]['AXIText15'],'$ArialNarrowBlack11','paraJust');
                   $tblResumen->addRow();
                   $tblResumen->addCell(9000, $cellLineWhite2Space)->addText($param['sugRate'].'  ','$ArialNarrowBlack11','paraRightT'); 
                   $tblResumen->addCell(3000, $cellLineWhite      )->addText(number_format(empty($inmueble['APVIII'][0]['AXIRate4'])?0:$inmueble['APVIII'][0]['AXIRate4'], 2, '.', ',').' %','$ArialNarrowBlack11','paraLeftT'); 
                   $tblResumen->addRow();                   
                   $tblResumen->addCell(12000, $cellLineWhite3Space)->addText($inmueble['APVIII'][0]['AXIText17'],'$ArialNarrowBlack11','paraJust');                    
                   $tblResumen->addRow();                   
                   $tblResumen->addCell(3000, $cellLineWhite      )->addText('','ArialNarrowBlack12','paraJust'); 
                   $tblResumen->addCell(3000, $cellLineWhite      )->addText($param['totRiskP'],'ArialNarrowBlack12','paraLeftT'); 
                   $tblResumen->addCell(3000, $cellLineWhite      )->addText(number_format(empty($totRate)?0:$totRate, 2, '.', ',').' %','ArialNarrowBlack12','paraLeftT');                    
                   $tblResumen->addRow();
                   $tblResumen->addCell(3000, $cellLineWhite      )->addText('','ArialNarrowBlack12','paraRightT'); 
                   $tblResumen->addCell(3000, $cellColSquare1_1   )->addText($param['discRate'],'ArialNarrowBlack12','paraLeftT'); 
                   $tblResumen->addCell(3000, $cellColSquare1_1   )->addText(number_format(empty($totRate)?0:$totRate, 2, '.', ',').' %','ArialNarrowBlack12','paraLeftT'); 
                   $tblResumen->addRow();
                   $tblResumen->addCell(3000, $cellLineWhite      )->addText('','ArialNarrowBlackBold12','paraRightT'); 
                   $tblResumen->addCell(3000, $cellColSquare1_1   )->addText($param['roun'],'ArialNarrowBlackBold12','paraLeftT'); 
                   $tblResumen->addCell(3000, $cellColSquare1_1   )->addText(number_format(empty($totRate)?0:round($totRate, 0), 2, '.', ',').' %','ArialNarrowBlackBold12','paraLeftT');                    
                   $tblResumen->addRow(250);                   
                   $tblResumen->addCell(12000, $cellLineWhite3Space)->addText('','ArialNarrowRedBold12','paraLeftT'); 
                   $tblResumen->addRow();                   
                   $tblResumen->addCell(12000, $cellLineWhite3Space)->addText($inmueble['APVIII'][0]['AXIText18'],'ArialNarrowRedBold12','paraLeftT'); 
                   $tblResumen->addRow();
                   $tblResumen->addCell(9000, $cellLineWhite2Space)->addText($inmueble['APVIII'][0]['AXIText19'],'ArialNarrowBlack12','paraRightT'); 
                   $tblResumen->addCell(3000, $cellLineWhite      )->addText(number_format(empty($inmueble['APVIII'][0]['AXIRate6'])?0:$inmueble['APVIII'][0]['AXIRate6'], 2, '.', ',').' %','ArialNarrowBlack12','paraLeftT'); 
                   $tblResumen->addRow();
                   $tblResumen->addCell(9000, $cellLineWhite2Space)->addText($inmueble['APVIII'][0]['AXIText20'],'ArialNarrowBlack12','paraRightT'); 
                   $tblResumen->addCell(3000, $cellLineWhite      )->addText(number_format(empty($inmueble['APVIII'][0]['AXIRate7'])?0:$inmueble['APVIII'][0]['AXIRate7'], 2, '.', ',').' %','ArialNarrowBlack12','paraLeftT'); 
                   $tblResumen->addRow();
                   $tblResumen->addCell(9000, $cellLineWhite2Space)->addText($inmueble['APVIII'][0]['AXIText21'],'ArialNarrowBlack12','paraRightT'); 
                   $tblResumen->addCell(3000, $cellLineWhite      )->addText(number_format(empty($inmueble['APVIII'][0]['AXIRate8'])?0:$inmueble['APVIII'][0]['AXIRate8'], 2, '.', ',').' %','ArialNarrowBlack12','paraLeftT'); 
                   $tblResumen->addRow(250);                   
                   $tblResumen->addCell(12000, $cellLineWhite3Space)->addText('','ArialNarrowRedBold12','paraLeftT'); 
                   $tblResumen->addRow();                   
                   $tblResumen->addCell(9000, $cellLineWhite2Space)->addText($inmueble['APVIII'][0]['AXIText22'],'ArialNarrowRedBold12','paraLeftT'); 
                   $tblResumen->addCell(3000, $cellLineWhite      )->addText(number_format(empty($AXISaleTerm)?0:$AXISaleTerm, 2, '.', ',').' months','ArialNarrowBlack12','paraLeftT');                    
                   $tblResumen->addRow(100);                   
                   $tblResumen->addCell(12000, $cellLineWhite3Space)->addText('','ArialNarrowRedBold12','paraLeftT'); 
                                      
                   $tblResumenII = $sectionAP->addTable(array('cellMargin' => 0, 'cellMarginRight' => 0, 'cellMarginBottom' => 0, 'cellMarginLeft' => 0));
                   $tblResumenII->addRow();
                   $tblResumenII->addCell(12000, $cellLineWhite3Space)->addText($param['liqVal'],'ArialNarrowRedBold12','paraLeftT'); 
                   $tblResumenII->addRow();
                   $tblResumenII->addCell(3000, $cellColSquareAllThin)->addText('1.- '.$param['basVal'].' '.$param['currency'],'ArialNarrowBlackBold12','paraLeftT'); 
                   $tblResumenII->addCell(3000, $cellColSquareAllThin)->addText('$ '.number_format($marketVal, 2, '.', ','),'ArialNarrowBlack12','paraLeftT'); 
                   $tblResumenII->addCell(6000, $cellColSquareAllThin)->addText($param['rfir'],'ArialNarrowBlack12','paraLeftT'); 
                   $tblResumenII->addRow();
                   $tblResumenII->addCell(9000, $cellColSquareSp2Thin)->addText('','ArialNarrowBlack12','paraLeftT'); 
                   $tblResumenII->addCell(3000, $cellColSquareAllThin)->addText($inmueble['APVIII'][0]['AXIFecha'],'ArialNarrowBlack12','paraLeftT'); 
                   $tblResumenII->addRow();
                   $tblResumenII->addCell(9000, $cellColSquareSp2Thin)->addText($param['adPoin'],'ArialNarrowBlack12','paraLeftT'); 
                   $tblResumenII->addCell(3000, $cellColSquareAllThin)->addText(number_format(empty($inmueble['APVIII'][0]['AXIRate9'])?0:$inmueble['APVIII'][0]['AXIRate9'], 2, '.', ',').' %','ArialNarrowBlack12','paraLeftT'); 
                   $tblResumenII->addRow();
                   $tblResumenII->addCell(12000, $cellColSquareSp3Thin)->addText('2.- '.$param['oppCos'],'ArialNarrowBlackBold12','paraLeftT'); 
                   $tblResumenII->addRow();
                   $tblResumenII->addCell(3000, $cellColSquareAllThin)->addText('Marketing Time','ArialNarrowBlack12','paraLeftT'); 
                   $tblResumenII->addCell(3000, $cellColSquareAllThin)->addText($mt.' months','ArialNarrowBlack12','paraCenterT'); 
                   $tblResumenII->addCell(6000, $cellColSquareAllThin)->addText($param['addRisk'],'ArialNarrowBlack12','paraCenterT'); 
                   $tblResumenII->addRow(); 
                   $tblResumenII->addCell(3000, $cellColSquareAllThin)->addText($param['estMonS'],'ArialNarrowBlack12','paraLeftT'); 
                   $tblResumenII->addCell(3000, $cellColSquareAllThin)->addText($inmueble['APVIII'][0]['AXISaleTerm'].' months','ArialNarrowBlack12','paraLeftT'); 
                   $tblResumenII->addCell(6000, $cellColSquareAllThin)->addText('','ArialNarrowBlack12','paraLeftT'); 
                   $tblResumenII->addRow(); 
                   $tblResumenII->addCell(3000, $cellColSquareAllThin)->addText('','ArialNarrowBlack12','paraLeftT'); 
                   $tblResumenII->addCell(3000, $cellColSquareAllThin)->addText(($mt - $inmueble['APVIII'][0]['AXISaleTerm']).' months','ArialNarrowRedBold12','paraLeftT'); 
                   $tblResumenII->addCell(6000, $cellColSquareAllThin)->addText('','ArialNarrowBlack12','paraLeftT'); 
                   $tblResumenII->addRow();
                   $tblResumenII->addCell(3000, $cellColSquareAllThin)->addText($param['discRate'],'ArialNarrowBlack12','paraLeftT'); 
                   $tblResumenII->addCell(3000, $cellColSquareAllThin)->addText(number_format($ocdr, 2, '.', ',').' %','ArialNarrowBlack12','paraCenterT'); 
                   $tblResumenII->addCell(6000, $cellColSquareAllThin)->addText($param['difRfir'].' '.number_format($difDR, 2, '.', ',').' %','ArialNarrowBlack12','paraCenterT');
                   $tblResumenII->addRow(); 
                   $tblResumenII->addCell(3000, $cellColSquareAllThin)->addText('Monthly risk free interest rate','ArialNarrowBlack12','paraLeftT'); 
                   $tblResumenII->addCell(3000, $cellColSquareAllThin)->addText(number_format($ocdrMonth, 2, '.', ',').' %','ArialNarrowBlack12','paraCenterT'); 
                   $tblResumenII->addCell(6000, $cellColSquareAllThin)->addText('','ArialNarrowBlack12','paraLeftT'); 
                   $tblResumenII->addRow();
                   $tblResumenII->addCell(9000, $cellColSquareSp2Thin)->addText($param['oppCos'],'ArialNarrowBlackBold12','paraLeftT'); 
                   $tblResumenII->addCell(3000, $cellColSquareAllThin)->addText('$ '.number_format($oppCos, 2, '.', ','),'ArialNarrowBlackBold12','paraLeftT'); 
                   $tblResumenII->addRow();
                   $tblResumenII->addCell(12000, $cellColSquareSp3Thin)->addText('3.- '.$param['manCos'],'ArialNarrowBlackBold12','paraLeftT'); 
                   $tblResumenII->addRow();
                   $tblResumenII->addCell(9000, $cellColSquareSp2Thin)->addText($param['perYear'],'ArialNarrowBlack12','paraLeftT'); 
                   $tblResumenII->addCell(3000, $cellColSquareAllThin)->addText(number_format(empty($inmueble['APVIII'][0]['AXIRate6'])?0:$inmueble['APVIII'][0]['AXIRate6'], 2, '.', ',').' %','ArialNarrowBlack12','paraLeftT'); 
                   $tblResumenII->addRow();
                   $tblResumenII->addCell(9000, $cellColSquareSp2Thin)->addText($param['perMonth'],'ArialNarrowBlack12','paraLeftT'); 
                   $tblResumenII->addCell(3000, $cellColSquareAllThin)->addText(number_format($rate6Month, 2, '.', ',').' %','ArialNarrowBlack12','paraLeftT'); 
                   $tblResumenII->addRow();
                   $tblResumenII->addCell(9000, $cellColSquareSp2Thin)->addText($param['manCos'],'ArialNarrowBlack12','paraLeftT'); 
                   $tblResumenII->addCell(3000, $cellColSquareAllThin)->addText('$ '.number_format($manCost, 2, '.', ','),'ArialNarrowBlack12','paraLeftT'); 
                   $tblResumenII->addRow();
                   $tblResumenII->addCell(12000, $cellColSquareSp3Thin)->addText('4.- '.$param['saleCos'],'ArialNarrowBlackBold12','paraLeftT'); 
                   $tblResumenII->addRow();
                   $tblResumenII->addCell(9000, $cellColSquareSp2Thin)->addText($param['commSale'],'ArialNarrowBlack12','paraLeftT'); 
                   $tblResumenII->addCell(3000, $cellColSquareAllThin)->addText(number_format(empty($inmueble['APVIII'][0]['AXIRate7'])?0:$inmueble['APVIII'][0]['AXIRate7'], 2, '.', ',').' %','ArialNarrowBlack12','paraLeftT');
                   $tblResumenII->addRow();
                   $tblResumenII->addCell(9000, $cellColSquareSp2Thin)->addText($param['saleCos'],'ArialNarrowBlackBold12','paraLeftT'); 
                   $tblResumenII->addCell(3000, $cellColSquareAllThin)->addText('$ '.number_format($saleCost, 2, '.', ','),'ArialNarrowBlackBold12','paraLeftT'); 
                   $tblResumenII->addRow();
                   $tblResumenII->addCell(12000, $cellColSquareSp3Thin)->addText('5.- '.$param['properTax'],'ArialNarrowBlackBold12','paraLeftT'); 
                   $tblResumenII->addRow();
                   $tblResumenII->addCell(9000, $cellColSquareSp2Thin)->addText($param['perYear'],'ArialNarrowBlack12','paraLeftT'); 
                   $tblResumenII->addCell(3000, $cellColSquareAllThin)->addText(number_format(empty($inmueble['APVIII'][0]['AXIRate8'])?0:$inmueble['APVIII'][0]['AXIRate8'], 2, '.', ',').' %','ArialNarrowBlack12','paraLeftT'); 
                   $tblResumenII->addRow();
                   $tblResumenII->addCell(9000, $cellColSquareSp2Thin)->addText($param['perMonth'],'ArialNarrowBlack12','paraLeftT'); 
                   $tblResumenII->addCell(3000, $cellColSquareAllThin)->addText(number_format($rate8Month, 2, '.', ',').' %','ArialNarrowBlack12','paraLeftT'); 
                   $tblResumenII->addRow();
                   $tblResumenII->addCell(9000, $cellColSquareSp2Thin)->addText($param['properTax'],'ArialNarrowBlackBold12','paraLeftT'); 
                   $tblResumenII->addCell(3000, $cellColSquareAllThin)->addText('$ '.number_format($properTax, 2, '.', ','),'ArialNarrowBlackBold12','paraLeftT'); 
                   $tblResumenII->addRow();
                   $tblResumenII->addCell(12000, $cellColSquareSp3Thin)->addText('6.- '.$param['liqVal'],'ArialNarrowBlackBold12','paraLeftT'); 
                   $tblResumenII->addRow();
                   $tblResumenII->addCell(9000, $cellColSquareSp2Thin)->addText($param['liqVal'],'ArialNarrowBlackBold12','paraLeftT'); 
                   $tblResumenII->addCell(3000, $cellColSquareAllThin)->addText('$ '.number_format($liquidVal, 2, '.', ','),'ArialNarrowBlackBold12','paraLeftT'); 
                   $tblResumenII->addRow();
                   $tblResumenII->addCell(9000, $cellColSquareSp2Thin)->addText($param['liqFactor'],'ArialNarrowBlackBold12','paraLeftT'); 
                   $tblResumenII->addCell(3000, $cellColSquareAllThin)->addText(number_format($liquidFactor*100, 2, '.', ',').' %','ArialNarrowBlackBold12','paraLeftT'); 
                   $tblResumenII->addRow(550);
                   $tblResumenII->addCell(12000, $cellLineWhite)->addText('','ArialNarrowBlackBold12','paraLeftT'); 
                   $tblResumenII->addRow();
                   $tblResumenII->addCell(3000, $cLineBottom)->addText($param['liqVal'],'ArialNarrowBlack12','paraLeftT'); 
                   $tblResumenII->addCell(9000, $cLineBottomS2)->addText('$ '.number_format($liquidVal, 2, '.', ',').' '.$param['currency'],'ArialNarrowBlack12','paraRightT'); 
                   $tblResumenII->addRow();
                   $tblResumenII->addCell(3000, $cLineBottom)->addText($param['roun'],'ArialNarrowBlackBold12','paraLeftT'); 
                   $tblResumenII->addCell(9000, $cLineBottomS2)->addText('$ '.number_format($this->redondearHermes($liquidVal), 2, '.', ',').' '.$param['currency'],'ArialNarrowBlackBold12','paraRightT'); 
                   
                   
                 }
                 
                 if( ($this->seccionReorte($apSections, "APXII") === TRUE) )
                 { $numAp++;
                   $numAP_Romano = $this->converToRoman($numAp);
                   $nombreSecc   = $numAP_Romano.' Conclusion and Certification';
                   $sectionAP    = $word->addSection();
                   
                   $this->addHeader($sectionAP, $appraisalNo, $nombreSecc, $fotoWatermark);
                   $this->addFooter($sectionAP, TRUE);
                   
                   $sectionAP->addTitle(htmlspecialchars($nombreSecc), 3);
                   
                   $cellLineWhite       = array(                'valign' => 'center','borderBottomSize' => 10,'borderBottomColor'=>'#FFFFFF','spaceAfter' => 0,'spaceBefore' => 0);
                   
                   $param = $this->leyendasAPV();
                   
                   \PhpOffice\PhpWord\Shared\Html::addHtml($sectionAP, $inmueble['APVIII'][0]['AXIIText1']);
                   
                   $sectionAP->addTextBreak(1);
                   
                   $AXmarketVal      = empty($inmueble['APVII'][0]['AXmarketVal'])?0:$inmueble['APVII'][0]['AXmarketVal'];
                   $AXmarketValMXM   = $AXmarketVal * $inmueble['dc']['exchange_rate'];
                   $AXmarketValM2    = $AXmarketVal / $totTRA;
                   $AXmarketValMXMM2 = $AXmarketValMXM / $totTRA;
                   $AXmarketValft2   = $AXmarketValM2 / $inmueble['dc']['ft'];

                   $AX_VU_Approach    = empty($inmueble['mv'][0]['AX_VU_Approach'])?0:$inmueble['mv'][0]['AX_VU_Approach'];
                   $AX_VU_ApproachMXM = $AX_VU_Approach * $inmueble['dc']['exchange_rate'];
                   $AXVUApproachM2    = $AX_VU_Approach / $totTRA;
                   $AXVUApproachMXMM2 = $AX_VU_ApproachMXM / $totTRA;
                   $AXVUApproachft2   = $AXVUApproachM2 / $inmueble['dc']['ft']; 

                   $AXIliquidVal      = empty($liquidVal)?0:$liquidVal;
                   $AXIliquidValMXM   = $AXIliquidVal    * $inmueble['dc']['exchange_rate'];
                   $AXIliquidValM2    = $AXIliquidVal    / $totTRA;
                   $AXIliquidValMXMM2 = $AXIliquidValMXM / $totTRA;
                   $AXIliquidValft2   = $AXIliquidValM2  / $inmueble['dc']['ft'];
                   
                   $tblResumen = $sectionAP->addTable(array('cellMargin' => 0, 'cellMarginRight' => 0, 'cellMarginBottom' => 0, 'cellMarginLeft' => 0));
                   $this->columnAPXTbl($tblResumen, $sCellLineBottom, $param['marval'].' of the Property',$this->redondearHermes($AXmarketVal),$AXmarketValM2,$AXmarketValft2,$param['currency'],$this->redondearHermes($AXmarketValMXM),$AXmarketValMXMM2,$param['currencyMX']);
                   if( ($this->seccionReorte($apSections, "APVIII") === TRUE) & $AX_VU_Approach != 0 )
                    { $this->columnAPXTbl($tblResumen, $sCellLineBottom, $param['vis'],$this->redondearHermes($AX_VU_Approach),$AXVUApproachM2,$AXVUApproachft2,$param['currency'],$this->redondearHermes($AX_VU_ApproachMXM),$AXVUApproachMXMM2,$param['currencyMX']); }
                   $this->columnAPXTbl($tblResumen, $sCellLineBottom, $param['liqVal'],$this->redondearHermes($AXIliquidVal),$AXIliquidValM2,$AXIliquidValft2,$param['currency'],$this->redondearHermes($AXIliquidValMXM),$AXIliquidValMXMM2,$param['currencyMX']);
                   
                   $sectionAP->addTextBreak(3);
                   
                   $tblResumenI = $sectionAP->addTable(array('cellMargin' => 0, 'cellMarginRight' => 0, 'cellMarginBottom' => 0, 'cellMarginLeft' => 0));
                   $tblResumenI->addRow();
                   $tblResumenI->addCell(12000, $cellLineWhite)->addText('Exposure Time: '.(empty($inmueble['frontPage'][0]['et'])?0:$inmueble['frontPage'][0]['et']).' months','ArialNarrowBlackBold12','paragraphRight'); 
                   $tblResumenI->addRow();
                   $tblResumenI->addCell(12000, $cellLineWhite)->addText('Marketing Time: '.(empty($inmueble['frontPage'][0]['mt'])?0:$inmueble['frontPage'][0]['mt']).' months','ArialNarrowBlackBold12','paragraphRight'); 
                   
                   $sectionAP->addTextBreak(5);
                   
                   $table = $sectionAP->addTable(array('align' => 'center','cellMargin' => 0, 'cellMarginRight' => 0, 'cellMarginBottom' => 0, 'cellMarginLeft' => 0));
                   $table->addRow(350);
                   $table->addCell(4500, array('align' => 'center'))->addShape('line',array('points'  => '0,0 250,0','outline' => array('color' => '#000000','line' => 'thickThin','weight' => 1)));
                   $table->addCell(4500, array('align' => 'center'))->addShape('line',array('points'  => '0,0 250,0','outline' => array('color' => '#000000','line' => 'thickThin','weight' => 1)));
                   $table->addRow(350);
                    $table->addCell(4500)->addText($inmueble['letter']['nombreFP'].', '.$inmueble['letter']['tituloFP'],'ArialNarrowRedBold11','paraCenterT');
                    $table->addCell(4500)->addText($inmueble['letter']['nombreFS'].(empty($inmueble['letter']['tituloFS'])?' ':', '.$inmueble['letter']['tituloFS']),'ArialNarrowRedBold11','paraCenterT');
                    $table->addRow(350);
                    $table->addCell(4500)->addText($inmueble['letter']['puestoFP'],'ArialNarrowBlackBold11','paraCenterT');
                    $table->addCell(4500)->addText(empty($inmueble['letter']['puestoFS'])?' ':$inmueble['letter']['puestoFS'],'ArialNarrowBlackBold11','paraCenterT');
                    $table->addRow(350);
                    $table->addCell(4500)->addText('Valuation and Consulting Services','ArialNarrowBlackBold11','paraCenterT');
                    $table->addCell(4500)->addText('Valuation and Consulting Services','ArialNarrowBlackBold11','paraCenterT');
                    $table->addRow(350);
                    $table->addCell(4500)->addText('JLL Mexico','ArialNarrowBlackBold11','paraCenterT');
                    $table->addCell(4500)->addText('JLL Mexico','ArialNarrowBlackBold11','paraCenterT');

                 }

                //******** APPRAISAL FIN ****************/                                  
                
                 $word->addTableStyle('aa1TableStyle' , array('borderSize' => 3, 'borderColor'=>'000000', 'cellMargin'=>0,'spacing' => 0));
                 $word->addTableStyle('rca1TableStyle', array('borderSize' => 0, 'borderColor'=>'FFFFFF', 'cellMargin'=>0));
                 
                //******************************************* ANNEXES A1 INI *******************************************/
                 if ($this->seccionReorte($annSections, "AA1") === TRUE)
                 {  $numAnexo++; 
                    $paramA1 = array("leyendas"    => array(strtoupper("Annex $numAnexo. - Market Research Sales Schedules"),"Land for Sale No.","Area of Comparable","Asking Price (USD)","Unit Value (USD/sqm)","Time on market"),
                                       "ft"          => $inmueble['dc']['ft'],
                                       "ftyear"      => $inmueble['dc']['ftyear'],
                                       "er"          => $inmueble['dc']['exchange_rate'],
                                       "galeriaComp" => $galeriaComp,
                                       "tipoComp"    => "CLS"
                                      );
                    $sectionAN = $word->addSection();

                    $this->addHeader($sectionAN, $appraisalNo, $paramA1['leyendas'][0], $fotoWatermark);
                    $this->addFooter($sectionAN, TRUE);
                    $sectionAN->addTitle(htmlspecialchars('ANNEXS'), 2);
                    $this->creaAnexoCompWord($paramA1,$inmueble['AA12'],$sectionAN);
                 } 
                //******************************************* ANNEXES A1 FIN *******************************************/
                 
                //******************************************* ANNEXES A2 INI *******************************************/
                if ($this->seccionReorte($annSections, "AA2") === TRUE & !empty($inmueble['AA12']))
                 {  $numAnexo++;
                 
                    $paramA2   = array("leyendas"    => array(strtoupper("Annex $numAnexo. - Land Sales Comparison Data and Adjustment Grid"),"Site size","Price","Adjusted Price","Weight","Composition of Indicated Value",""),
                                       "ft"          => $inmueble['dc']['ft'],
                                       "ftyear"      => $inmueble['dc']['ftyear'],
                                       "er"          => $inmueble['dc']['exchange_rate'],
                                       "ce"          => $infoFile['ComparableElements'],
                                       "landSunject" => empty($area['land'])?0:$area['land'],
                                       "limiteRC8"   => 22,
                                       "tipoComp"    => "CLS",
                                       "tipo"        => "AA2",
                                       "menosRC"     => 0,
                                       "limiteRC7"   => 16,
                                       "quitarRC"    => $md_inmuebles->traeRCVacios($infoFile['id_in'],"CLS")
                                       );

                    $sectionA2 = $word->createSection(array('orientation'=>'landscape'));

                    $this->addHeaderLandscape($sectionA2, $appraisalNo, $paramA2['leyendas'][0], $fotoWatermark);
                    $this->addFooterLandscape($sectionA2);

                    $this->creaGridCompWord($paramA2,$inmueble['AA12'],$inmueble['rcAA2'],$sectionA2);

                    $sectionA2->addPageBreak();
                 }   
                //******************************************* ANNEXES A2 FIN *******************************************/                                
                                
                 //******************************************* ANNEXES A3 INI *******************************************/
                if ($this->seccionReorte($annSections, "AA3") === TRUE & !empty($inmueble['AA34']))
                 {  $numAnexo++;
                    $paramA3   = array("leyendas"    => array(strtoupper("Annex $numAnexo. - Market Research Lease Schedules"),"PROPERTY FOR LEASE No. ","Comparable area","Monthly Rent (USD)","Monthly Rent (USD/M2)","Closing Date"),
                                       "ft"          => $inmueble['dc']['ft'],
                                       "ftyear"      => $inmueble['dc']['ftyear'],
                                       "er"          => $inmueble['dc']['exchange_rate'],
                                       "galeriaComp" => $galeriaComp,
                                       "tipoComp"    => "CRL"
                                       );
                    $sectionII = $word->addSection();                    

                    $this->addHeader($sectionII, $appraisalNo, $paramA3['leyendas'][0], $fotoWatermark);
                    $this->addFooter($sectionII, TRUE);

                    $this->creaAnexoCompWord($paramA3,$inmueble['AA34'],$sectionII);
                 }
                //******************************************* ANNEXES A3 FIN *******************************************/
                 
                 //******************************************* ANNEXES A4 INI *******************************************/
                if ($this->seccionReorte($annSections, "AA4") === TRUE & !empty($inmueble['AA34']))
                 {  $numAnexo++;
                    $paramA4   = array("leyendas"    => array(strtoupper("Annex $numAnexo. - Lease Comparison Data and Adjustment Grid"),"Rentable area","Price","Adjusted Price","Weight","Composition of Indicated Value",""),
                                       "ft"          => $inmueble['dc']['ft'],
                                       "ftyear"      => $inmueble['dc']['ftyear'],
                                       "er"          => $inmueble['dc']['exchange_rate'],
                                       "ce"          => $infoFile['ComparableElements'],
                                       "landSunject" => empty($area['tra'])?0:$area['tra'],
                                       "limiteRC8"   => 24,
                                       "tipoComp"    => "CRL",
                                       "tipo"        => "AA4",
                                       "menosRC"     => 3,
                                       "limiteRC7"   => 13,//14,
                                       "quitarRC"    => $md_inmuebles->traeRCVacios($infoFile['id_in'],"CRL")
                                       );

                    $sectionA4 = $word->createSection(array('orientation'=>'landscape'));

                    $this->addHeaderLandscape($sectionA4, $appraisalNo, $paramA4['leyendas'][0], $fotoWatermark);
                    $this->addFooterLandscape($sectionA4);

                    $this->creaGridCompWord($paramA4,$inmueble['AA34'],$inmueble['rcAA4'],$sectionA4);
                    $sectionA4->addPageBreak();
                 }
                //******************************************* ANNEXES A4 FIN *******************************************/
                                
                
                //******************************************* ANNEXES A5 INI *******************************************/
                if ($this->seccionReorte($annSections, "AA5") === TRUE & !empty($inmueble['AA56']))
                 {  $numAnexo++;
                    $paramA5   = array("leyendas"    => array(strtoupper("Annex $numAnexo. - Market Research - For Sale"),"PROPERTY FOR SALE No. ","Comparable area","Total Sale Price","Unit Sale Price","Lease Date"),
                                       "ft"          => $inmueble['dc']['ft'],
                                       "ftyear"      => $inmueble['dc']['ftyear'],
                                       "er"          => $inmueble['dc']['exchange_rate'],
                                       "galeriaComp" => $galeriaComp,
                                       "tipoComp"    => "CRS"
                                       );
                    $sectionIII = $word->addSection();   

                    $this->addHeader($sectionIII, $appraisalNo, $paramA5['leyendas'][0], $fotoWatermark);
                    $this->addFooter($sectionIII, TRUE);

                     $this->creaAnexoCompWord($paramA5,$inmueble['AA56'],$sectionIII);
                 }
                //******************************************* ANNEXES A5 FIN *******************************************/
                 
                 //******************************************* ANNEXES A6 INI *******************************************/
                if ($this->seccionReorte($annSections, "AA6") === TRUE & !empty($inmueble['AA56']))
                 {  $numAnexo++;
                    $paramA6   = array("leyendas"    => array(strtoupper("Annex $numAnexo. - Sale Comparison Data and Adjustment Grid"),"Site size","Price (usd/m2) (usd/ft2)","Adjusted Price","Weight","Composition of Indicated Value",""),
                                       "ft"          => $inmueble['dc']['ft'],
                                       "ftyear"      => $inmueble['dc']['ftyear'],
                                       "er"          => $inmueble['dc']['exchange_rate'],
                                       "ce"          => $infoFile['ComparableElements'],
                                       "landSunject" => empty($area['tra'])?0:$area['tra'],
                                       "limiteRC8"   => 26,
                                       "tipoComp"    => "CRS",
                                       "tipo"        => "AA6",
                                       "menosRC"     => 3,
                                       "limiteRC7"   => 13,//14,
                                       "quitarRC"    => $md_inmuebles->traeRCVacios($infoFile['id_in'],"CRS")                      
                                       );
                    
                    $sectionA6 = $word->createSection(array('orientation'=>'landscape'));

                    $this->addHeaderLandscape($sectionA6, $appraisalNo, $paramA6['leyendas'][0], $fotoWatermark);
                    $this->addFooterLandscape($sectionA6);

                    $this->creaGridCompWord($paramA6,$inmueble['AA56'],$inmueble['rcAA6'],$sectionA6);
                    $sectionA6->addPageBreak();
                 }   
                //******************************************* ANNEXES A6 FIN *******************************************/                
                                
                
                 //****************************** ANNEXES AAIMG INI ****************/
                 if( ($this->seccionReorte($annSections, "AAA") === TRUE) & (sizeof($inmueble['aaimg']) > 0) )
                     { $numAnexo = $this->creaTablaAAIMGWord($inmueble['frontPage'][0]['id_in'],$inmueble['aaimg'],$numAnexo,$word,$md_gallery,$fotoWatermark,$appraisalNo); }
                //****************************** ANNEXES AAIMG FIN ****************/ 

                 //****************************** ANNEXES Assumption & Certifications INI ****************/
                 if( ($this->seccionReorte($annSections, "LIM") === TRUE) & (!empty($inmueble['AnnexsLC'][0]['AnnexsLimTxt'])) )
                 {   
                    $numAnexo++;
                    $nombreSecc =  strtoupper("Annex $numAnexo. - Assumptions and Limiting Conditions");
                    $sectionLM    = $word->addSection();
                   
                    $this->addHeader($sectionLM, $appraisalNo, $nombreSecc, $fotoWatermark);
                    $this->addFooter($sectionLM, TRUE);
                   
                    $sectionLM->addTitle(htmlspecialchars($nombreSecc), 3);
                    \PhpOffice\PhpWord\Shared\Html::addHtml($sectionLM, $inmueble['AnnexsLC'][0]['AnnexsLimTxt']);                    
                    $sectionLM->addPageBreak();                                        
                    
                 }//****************************** ANNEXES Assumption  fIN ****************/
                 
                 //****************************** ANNEXES  & Certifications INI ****************/
                 if( ($this->seccionReorte($annSections, "CERT") === TRUE) & (!empty($inmueble['AnnexsLC'][0]['AnnexsCertTxt'])) )
                 {   
                    $numAnexo++;
                    $nombreSecc =  strtoupper("Annex $numAnexo. - Certifications");
                    $sectionCR    = $word->addSection();
                   
                    $this->addHeader($sectionCR, $appraisalNo, $nombreSecc, $fotoWatermark);
                    $this->addFooter($sectionCR, TRUE);
                   
                    $sectionCR->addTitle(htmlspecialchars($nombreSecc), 3);
                    \PhpOffice\PhpWord\Shared\Html::addHtml($sectionCR, $inmueble['AnnexsLC'][0]['AnnexsCertTxt']);
                    $sectionCR->addTextBreak(2);
                    $l = $this->leyendasFirmas();
                    $table = $sectionCR->addTable(array('align' => 'center','cellMargin' => 0, 'cellMarginRight' => 0, 'cellMarginBottom' => 0, 'cellMarginLeft' => 0));
                    $table->addRow(350);
                    $table->addCell(4500, array('align' => 'center'))->addShape('line',array('points'  => '0,0 250,0','outline' => array('color' => '#000000','line' => 'thickThin','weight' => 1)));
                    $table->addCell(4500, array('align' => 'center'))->addShape('line',array('points'  => '0,0 250,0','outline' => array('color' => '#000000','line' => 'thickThin','weight' => 1)));
                    $table->addRow(350);
                    $table->addCell(4500)->addText(htmlspecialchars($inmueble['letter']['nombreFP'].', '.$inmueble['letter']['tituloFP']),'ArialNarrowRedBold11','paraCenterT');
                    $table->addCell(4500)->addText(htmlspecialchars($inmueble['letter']['nombreFS'].(empty($inmueble['letter']['tituloFS'])?' ':', '.$inmueble['letter']['tituloFS'])),'ArialNarrowRedBold11','paraCenterT');
                    $table->addRow(350);
                    $table->addCell(4500)->addText(htmlspecialchars($inmueble['letter']['puestoFP']),'ArialNarrowBlackBold11','paraCenterT');
                    $table->addCell(4500)->addText(empty($inmueble['letter']['puestoFS'])?' ':htmlspecialchars($inmueble['letter']['puestoFS']),'ArialNarrowBlackBold11','paraCenterT');
                    $table->addRow(350);
                    $table->addCell(4500)->addText($l['vcs'],'ArialNarrowBlackBold11','paraCenterT');
                    $table->addCell(4500)->addText($l['vcs'],'ArialNarrowBlackBold11','paraCenterT');
                    $table->addRow(350);
                    $table->addCell(4500)->addText('JLL Mexico','ArialNarrowBlackBold11','paraCenterT');
                    $table->addCell(4500)->addText('JLL Mexico','ArialNarrowBlackBold11','paraCenterT');

                 }
                //****************************** ANNEXES   Certifications FIN ****************/
                 
                //******************************************* SAVE FILE INI *******************************************/
                 $properties = $word->getDocInfo();
                 $properties->setCreator('Valuation Mexico');
                 $properties->setCompany('JLL Mexico');
                 $properties->setTitle('Valuation Report');
                 $properties->setDescription('Valuation Report');
                 $properties->setCategory('Valuation Report');
                 $properties->setCreator('JLL MEXICO Strategic Valuation Services');
                 $properties->setSubject('JLL MEXICO Strategic Valuation Services');
                 //$properties->setCreated(mktime(0, 0, 0, 3, 12, 2014));
                 //$properties->setModified(mktime(0, 0, 0, 3, 14, 2014));                 
                 //$properties->setKeywords('my, key, word');
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($word, 'Word2007');
        $objWriter->save($infoFile['dir'].$infoFile['id_in'].'/'.$formatoWord);
        //******************************************* SAVE FILE FIN *******************************************/

        return $formatoWord;        

            } catch (Exception $e) {echo 'ERROR exportaWord: ',  $e, "\n";} 
        }
        
       
        
        
         public function creaGridCompWord($param,$comparables,$rc,$section)
        {
         try{
             require_once APPPATH.'third_party/PhpOffice/PhpWord/Autoloader.php';
             \PhpOffice\PhpWord\Autoloader::register();            
             
             $landStr        = array();
             $land           = array();
             $priceStr       = array();
             $priceUSD       = array();
             $priceMX        = array();
             $priceMXAcum    = array();// PRECIO EN DOLARES
             $adPrice        = array();
             $adPriceValue   = array();
             $weight         = array();
             $weightPor      = array();
             $weightPorTot   = array();
             $composition    = array();
             $styleCellAA1   = array('valign' => 'center');
             $styleCellAA1RW = array('valign' => 'center', 'bgColor'=>'#963634');
             $ft             = $param['ft'];
             $ftyear         = $param['ftyear'];
             $er             = $param['er'];
             $x              = 0;
             $headTable      = array("ELEMENT","SUBJECT");             
                          
             $section->addTitle(htmlspecialchars($param['leyendas'][0]), 3);             

             $tableGrid = $section->addTable('aa1TableStyle');
             $tableGrid->addRow(null, array('tblHeader' => true, 'cantSplit' => true));             
             $tableGrid->addCell(3800, $styleCellAA1RW)->addText($headTable[0],'ArialNarrowRedWhiteBold10','paraCenterT');
             $tableGrid->addCell(3000, $styleCellAA1RW)->addText($headTable[1],'ArialNarrowRedWhiteBold10','paraCenterT'); 
             foreach ($comparables as $cAA1)
                {   $x++;
                    $landStr [] = number_format((($param['tipo'] === "AA2")?$cAA1['land_m2']:$cAA1['construction']), 2, '.', ',').' m2\n'.number_format((($param['tipo'] === "AA2")?$cAA1['land_ft2']:($cAA1['construction']*$ft)), 2, '.', ',').' ft2';
                    $land    [] = ($param['tipo'] === "AA2")?$cAA1['land_m2']:$cAA1['construction'];                    
                    $priceStr[] = $this->creaAPWord($param['tipoComp'], $cAA1['unit_value_mx'], $er, $ft, $ftyear);
                    $priceMX [] = $cAA1['unit_value_mx'];
                    $priceUSD[] = $cAA1['unit_value_mx'] / $er;//PRECIO EN DOLARES
                    $tableGrid->addCell(3000, $styleCellAA1RW)->addText('COMPARABLE '.$x,'ArialNarrowRedWhiteBold10','paraCenterT'); 
                }
             $maxColumns     = sizeof($landStr);   
             $maxP           = count($priceUSD)==0?0:max($priceUSD);
             $minP           = count($priceUSD)==0?0:min($priceUSD);
             $avP            = count($priceUSD)==0?0:array_sum($priceUSD) / count($priceUSD);
             $std            = count($priceUSD)==0?0:$this->stats_standard_deviation($priceUSD);
             $variationCoeff = count($priceUSD)==0?0:($std / $avP) * 100 ;
             
             $tableGrid->addRow(null, array('tblHeader' => true, 'cantSplit' => true)); //SIZE
             $tableGrid->addCell(3800, $styleCellAA1)->addText($param['leyendas'][1],'ArialNarrowBlack9','paraLeftT');
             $cellLandSub = $tableGrid->addCell(3000, $styleCellAA1); //SUBJECT
             $trLS        = $cellLandSub->addTextRun(array('align' => 'center','spaceBefore' => 0, 'spaceAfter' => 0));
             $trLS->addText(number_format( $param['landSunject'], 2, '.', ',').' m2','ArialNarrowBlack9','paraCenterT');
             $trLS->addTextBreak();
             $trLS->addText(number_format( $param['landSunject']*$ft, 2, '.', ',').' ft2','ArialNarrowBlack9','paraCenterT');
             for($i = 0; $i < $maxColumns;$i++)
               { $textlines = explode('\n', $landStr[$i]);
                 $cell      = $tableGrid->addCell(3000, $styleCellAA1);                 
                 $textrun   = $cell->addTextRun(array('align' => 'center','spaceBefore' => 0, 'spaceAfter' => 0));
                 $textrun->addText($textlines[0],'ArialNarrowBlack9','paraCenterT');
                 $textrun->addTextBreak();
                 $textrun->addText($textlines[1],'ArialNarrowBlack9','paraCenterT');
               }
                
             $tableGrid->addRow(null, array('tblHeader' => true, 'cantSplit' => true)); //PRICE
             $tableGrid->addCell(3800, $styleCellAA1)->addText($param['leyendas'][2],'ArialNarrowBlack9','paraLeftT');
             $tableGrid->addCell(3000, $styleCellAA1)->addText(' '                  ,'ArialNarrowBlack9','paraCenterT'); //SUBJECT
             for($i = 0; $i < $maxColumns;$i++)                
                 { $textlines = explode('\n', $priceStr[$i]);
                   $cell      = $tableGrid->addCell(3000, $styleCellAA1);
                   $textrun   = $cell->addTextRun(array('align' => 'center','spaceBefore' => 0, 'spaceAfter' => 0));
                   $textrun->addText($textlines[0],'ArialNarrowBlack9Italic','paraCenterT');
                   $textrun->addTextBreak();
                   $textrun->addText($textlines[1],'ArialNarrowBlack9Italic','paraCenterT');
                 }
                
             /**********  RELEVANT CHARACTERISTICS OF THE COMPARABLE INI*********************/
            $x         = 0;
            $ap        = 0;
            $espacio   = "";
            $leyendaRC = "";
            $menosRC   = $param['menosRC'];
            $limiteRC7 = $param['limiteRC7'];
            $limiteRC  = 18-$menosRC;
            $totalPrice= array();
            //print_r($rc);
            foreach ($rc as $r)
            {   $x++;
                $rcValue   = array();
                $y         = -1;             
                $weightPor = array(); 
                
                foreach ($comparables as $cAA1)
                    {   $y++;
                        $leyendaRC    = $this->traeLeyendaRC($x,$cAA1,$r['campo'],$param['tipo']);
                        //print_r("\r\n".$leyendaRC . ' '. ($leyendaRC !== NULL));
                        if($leyendaRC !== NULL)
                         {
                            //print_r(" - Entra");
                            $price      = ($x===1)?$priceMX[$y]:$priceMXAcum[$y];
                            $porcentaje = $this->traeRCAdjustmentCriteria($r['tipo'],$cAA1,$r,$param['limiteRC8']);
                            if($x===6)
                                { $porcentaje = empty($param['landSunject'])?0:($porcentaje+((1-$porcentaje)*($land[$y]/$param['landSunject']))-1); }
                            $ap               = $price*(1+($porcentaje));
                            $rcValue     []   = $this->traeAdjustmentCriteriaPorDesc($r['tipo'],$x,$cAA1,$porcentaje,$param['tipoComp'],'');
                            $sumPor           = abs(($porcentaje*100));                        
                            $celdaAP          = $this->creaAPWord($param['tipoComp'],$ap,$er,$ft,$ftyear);
                            $adPrice     []   = $celdaAP;
                            $adPriceValue[]   = round($ap,2);
                            $priceMXAcum [$y] = $ap;
                            $weightPor   [$y] = $sumPor;
                            if ($x === $limiteRC)
                            { $totalPrice  [$y]   = round($ap/$er,4); }
                         }
                         else
                         {
                            $rcValue     []   = "";
                            $adPrice     []   = "";
                         }
                    }
                    
                if(!empty($leyendaRC))                
                {   
                    //var_dump($limiteRC7,$param);
                    $limiteX        = $limiteRC7-$param['quitarRC'];
                    $weightPorTot[] = $weightPor; 
                    $espacio        = ( $x>5 & $x<=$limiteX)?"      ":"";
                    $fl             = ( $x === $limiteRC   )?"Final ":"";
                    
                    if($x === 6)
                        {   
                            $tableGrid->addRow(null, array('tblHeader' => true, 'cantSplit' => true));
                            $cell    = $tableGrid->addCell(3800, $styleCellAA1);
                            $textrun = $cell->addTextRun(array('align' => 'left','spaceBefore' => 0, 'spaceAfter' => 0));
                            $textrun->addText("6)Physical characteristics",'ArialNarrowBlack9','paraLeftT');
                            $textrun->addTextBreak();
                            $textrun->addText("      ".$leyendaRC,'ArialNarrowBlack9','paraLeftT');
                        }
                   
                    else
                        { $tableGrid->addRow(null, array('tblHeader' => true, 'cantSplit' => true));
                          $tableGrid->addCell(3800, $styleCellAA1)->addText($espacio.$leyendaRC,'ArialNarrowBlack9','paraLeftT'); }
                        
                    $tableGrid->addCell(3000, $styleCellAA1)->addText(' '                  ,'ArialNarrowBlack9','paraCenterT'); //SUBJECT                    
                    for($i = 0; $i < $maxColumns;$i++){ 
                        if(isset($rcValue[$i]) && $rcValue[$i]!=='')
                        {
                            $tableGrid->addCell(3000, $styleCellAA1)->addText($rcValue[$i],'ArialNarrowBlack9','paraCenterT'); 
                        }
                        else
                        {
                            $tableGrid->addCell(3000, $styleCellAA1);
                        }
                    }
                       
                    //var_dump($limiteX);

                    if( $x>5 & $x< $limiteX)//15
                    {}
                    else{
                        
                        //var_dump($leyendaRC, $x, $param['leyendas'][3]);
                        
                        $tableGrid->addRow(null, array('tblHeader' => true, 'cantSplit' => true));
                              $tableGrid->addCell(3800, $styleCellAA1)->addText($fl.$param['leyendas'][3],'ArialNarrowBlack9Italic','paraLeftT');
                              $tableGrid->addCell(3000, $styleCellAA1)->addText(' '                  ,'ArialNarrowBlack9','paraCenterT'); //SUBJECT
                              for($i = 0; $i < $maxColumns;$i++)
                                { 
                                    if(isset($adPrice[$i]) && $adPrice[$i]!==''){
                                        $textlines = explode('\n', $adPrice[$i]);
                                        $cell      = $tableGrid->addCell(3000, $styleCellAA1);
                                        $textrun   = $cell->addTextRun(array('align' => 'center','spaceBefore' => 0, 'spaceAfter' => 0));
                                        $textrun->addText($textlines[0],'ArialNarrowBlack9Italic','paraCenterT');
                                        $textrun->addTextBreak();
                                        $textrun->addText($textlines[1],'ArialNarrowBlack9Italic','paraCenterT');
                                    }
                                    else
                                    {
                                        $tableGrid->addCell(3000, $styleCellAA1);
                                    }
                                }
                            }
                }

                unset($rcValue);
                unset($adPrice);
                unset($weightPor);
                if( $x !== count($rc))
                 { unset($adPriceValue); }
            }
            /**********  RELEVANT CHARACTERISTICS OF THE COMPARABLE FIN*********************/        
            $y           = -1;
            $totalIVM    = 0;
            $totalIVFT   = 0;
            $weightTotal = 0;
            
            foreach ($comparables as $cAA1)
            {   $y++;
                $weightTotal   = $weightTotal + $cAA1['weight'];
                $weight     [] = number_format($cAA1['weight'], 2, '.', ',').' %';
                $fc            = $totalPrice[$y] * ($cAA1['weight'] / 100);
                $fcFT          = ($param['tipo'] === "AA2")?($totalPrice[$y] / $ft) * $fc:($fc*$ftyear);
                $composition[] = $this->compositionWORD($fc,$fcFT,($param['tipo'] === "AA4")?"usd/ft2/year":"usd/ft2");
                $totalIVM      = $totalIVM + $fc;
                $totalIVFT     = ($param['tipo'] === "AA2")?$totalIVM / $ft:($totalIVM / $ft)*12;
            }
            
            $tableGrid->addRow(null, array('tblHeader' => true, 'cantSplit' => true));// WEIGHT
            $tableGrid->addCell(3800, $styleCellAA1)->addText($param['leyendas'][4],'ArialNarrowBlack9','paraLeftT');
            $tableGrid->addCell(3000, $styleCellAA1)->addText(' '                  ,'ArialNarrowBlack9','paraCenterT'); //SUBJECT
            for($i = 0; $i < $maxColumns;$i++)
              { $tableGrid->addCell(3000, $styleCellAA1)->addText($weight[$i],'ArialNarrowBlack9','paraCenterT'); }
              
            $tableGrid->addRow(null, array('tblHeader' => true, 'cantSplit' => true));//COMPOSITION
            $tableGrid->addCell(3800, $styleCellAA1)->addText($param['leyendas'][5].' '.$param['leyendas'][6],'ArialNarrowBlack9','paraLeftT');
            $tableGrid->addCell(3000, $styleCellAA1)->addText(' '                  ,'ArialNarrowBlack9','paraCenterT'); //SUBJECT
            for($i = 0; $i < $maxColumns;$i++)
                { $textlines = explode('\n', $composition[$i]);
                  $cell      = $tableGrid->addCell(3000, $styleCellAA1);
                  $textrun   = $cell->addTextRun(array('align' => 'center','spaceBefore' => 0, 'spaceAfter' => 0));
                  $textrun->addText($textlines[0],'ArialNarrowBlack9Italic','paraCenterT');
                  $textrun->addTextBreak();
                  $textrun->addText($textlines[1],'ArialNarrowBlack9Italic','paraCenterT');
                }
            
            $year   = "";//($param['tipo']==="AA4")?"/year":"";
            $ft2AA4 = ($param['tipo'] === "AA4")?" usd/sqf/year":" usd/ft2";
            $fc2AA4 = ($param['tipo'] === "AA4")?" usd/sqm/mo." :" usd/m2";
            $section->addTextBreak(2);
            $tableResume = $section->addTable('rca1TableStyle');
            $tableResume->addRow();            
            $tableResume->addCell(3800, $styleCellAA1RW)->addText("Indicated value per m2" ,'ArialNarrowRedWhiteBold10','paraCenterT');
            $tableResume->addCell(3800, $styleCellAA1RW)->addText("Indicated value per ft2",'ArialNarrowRedWhiteBold10','paraCenterT'); 
            $tableResume->addCell(2500, $styleCellAA1RW)->addText("Maximum"                ,'ArialNarrowRedWhiteBold10','paraCenterT'); 
            $tableResume->addCell(2500, $styleCellAA1RW)->addText("Minimum"                ,'ArialNarrowRedWhiteBold10','paraCenterT');
            $tableResume->addCell(3000, $styleCellAA1RW)->addText("Average"                ,'ArialNarrowRedWhiteBold10','paraCenterT'); 
            $tableResume->addCell(3000, $styleCellAA1RW)->addText("Std. Deviation"         ,'ArialNarrowRedWhiteBold10','paraCenterT'); 
            $tableResume->addCell(3000, $styleCellAA1RW)->addText("Variation Coeff."       ,'ArialNarrowRedWhiteBold10','paraCenterT');
            $tableResume->addRow();
            //$tableResume->addCell(3800, $styleCellAA1)->addText("$ ".number_format($totalIVM, 2, '.', ',').$ft2AA4,'ArialNarrowBlack9','paraCenterT');
            //$tableResume->addCell(3000, $styleCellAA1)->addText("$ ".number_format($totalIVFT, 2, '.', ',').$fc2AA4.$year,'ArialNarrowBlack9','paraCenterT'); 
            $tableResume->addCell(3800, $styleCellAA1)->addText("$ ".number_format($totalIVM, 2, '.', ',').$fc2AA4,'ArialNarrowBlack9','paraCenterT');
            $tableResume->addCell(3000, $styleCellAA1)->addText("$ ".number_format($totalIVFT, 2, '.', ',').$ft2AA4.$year,'ArialNarrowBlack9','paraCenterT'); 
            $tableResume->addCell(3000, $styleCellAA1)->addText('$ '.number_format($maxP, 2, '.', ','),'ArialNarrowBlack9','paraCenterT'); 
            $tableResume->addCell(3000, $styleCellAA1)->addText('$ '.number_format($minP, 2, '.', ','),'ArialNarrowBlack9','paraCenterT');
            $tableResume->addCell(3000, $styleCellAA1)->addText('$ '.number_format($avP, 2, '.', ','),'ArialNarrowBlack9','paraCenterT'); 
            $tableResume->addCell(3000, $styleCellAA1)->addText('$ '.number_format($std, 2, '.', ','),'ArialNarrowBlack9','paraCenterT'); 
            $tableResume->addCell(3000, $styleCellAA1)->addText(number_format($variationCoeff, 2, '.', ',')."%",'ArialNarrowBlack9','paraCenterT'); 
            
            $section->addTextBreak(1);            
            $section->addText("Comparable elements:",'ArialNarrowBlack9','paragraphJust');            
            $section->addText("1) Property rights conveyed: Adjustments considered due to the existence of rents below market or rents not on stabilization levels in income-producing properties.",'ArialNarrowBlack9','paragraphJust');            
            $section->addText("2) Financing terms: Adjustments considered due to the existence of sales financed through atypical credit conditions.",'ArialNarrowBlack9','paragraphJust');            
            $section->addText("3) Conditions of sale: Adjustments considered due to atypical motivation of buyer or seller.",'ArialNarrowBlack9','paragraphJust');            
            $section->addText("4) Market Conditions: Adjustments considered due to the price differences in the various cycles that make up the cycle of real estate; that is, effects of inflationary reasons or decreases in prices.",'ArialNarrowBlack9','paragraphJust');            
            $section->addText("5) Location: Adjustments considered due to differences such as access, visibility, market reputation and traffic intensity.",'ArialNarrowBlack9','paragraphJust');            
            $section->addText("6) Physical characteristics: Adjustments considered due to differences in physical characteristics such as size, conditions, amenities, and property type.",'ArialNarrowBlack9','paragraphJust');
            $section->addText("7) Use (zoning): Adjustments considered due to differences in the uses and densities permitted.",'ArialNarrowBlack9','paragraphJust');            
            $section->addText("8) Listing / Sale:Adjustments considered due to differences between the listing price and the real sale price.",'ArialNarrowBlack9','paragraphJust');            
            
            return $section;
            } catch (Exception $e) {echo 'creaGridCompWord Excepción: ',  $e, "\n";}    
        }
        
        public function newLineCell($text,$cell,$estilo,$para)
         {  $textlines = explode('\n', $text);         
            $textrun = $cell->addTextRun(array('align' => 'center','spaceBefore' => 0, 'spaceAfter' => 0));
            $textrun->addText($textlines[0],$estilo,$para);
            $textrun->addTextBreak();
            $textrun->addText($textlines[1],$estilo,$para);
            if (sizeof($textlines)>2)
            {
                $textrun->addTextBreak();
                $textrun->addText($textlines[2],$estilo,$para);                
            }
         }
        
        public function creaAnexoCompWord($param,$comparables,$section)
        {
         try{
              require_once APPPATH.'third_party/PhpOffice/PhpWord/Autoloader.php';
                \PhpOffice\PhpWord\Autoloader::register();
                
                $styleCellAA1   = array('valign'     => 'center');                 
                $cellColSpan2   = array('gridSpan'   => 2, 'valign' => 'center');
                $cellColSpan3   = array('gridSpan'   => 3, 'valign' => 'center','spaceAfter' => 0);
                $styleCellAA1RW = array('valign'     => 'center', 'bgColor'=>'#963634');
                $cellColSpan2RW = array('gridSpan'   => 2, 'valign' => 'center', 'bgColor'=>'#963634');
                $cellColSpan3RW = array('gridSpan'   => 3, 'valign' => 'center','spaceAfter' => 0, 'bgColor'=>'#963634');
                $cellColSpan3WB = array('gridSpan'   => 3, 'valign' => 'center','spaceAfter' => 0, 'bgColor'=>'#FFFFFF');

                $ft             = $param['ft'];
                $ftyear         = $param['ftyear'];
                $er             = $param['er'];
                $x              = 0;
                $rc8            = $this->optionsRC8($param['tipoComp']);
                $leyendaRC8     = $param['tipoComp'] === "CRL"?"Listing / Real Rent":"Listing / Sale";
                
                $section->addTitle(htmlspecialchars($param['leyendas'][0]), 3);
                
                foreach ($comparables as $cAA1)
                    {   $x++;
                        $tableAA1 = $section->addTable('aa1TableStyle');
                        $tableAA1->addRow();
                        $tableAA1->addCell(9000, $cellColSpan3RW)->addText($param['leyendas'][1].' '.$x,'ArialNarrowRedWhiteBold14','paraCenterT');
                        
                        $tableAA1->addRow();                         
                        $this->addmyImageTBL($tableAA1,9000,$cellColSpan3,$param['galeriaComp'].$cAA1['foto'],300,200,'center');
                        $tableAA1->addRow();
                        $tableAA1->addCell(3000, $styleCellAA1RW)->addText('Type of Property','ArialNarrowRedWhiteBold11','paraCenterT');
                        $tableAA1->addCell(6000, $cellColSpan2RW)->addText('Location','ArialNarrowRedWhiteBold11','paraCenterT');
                        $tableAA1->addRow(400);
                        $tableAA1->addCell(3000, $styleCellAA1)->addText($cAA1['type_property'],'ArialNarrowBlack10','paraCenterT');
                        $tableAA1->addCell(6000, $cellColSpan2)->addText($this->creaLocation($cAA1['calle'],$cAA1['num'],$cAA1['col'],$cAA1['mun'],$cAA1['edo'],$cAA1['cp']),'ArialNarrowBlack10','paraCenterT');
                        $tableAA1->addRow();
                        $tableAA1->addCell(3000, $styleCellAA1RW)->addText('Source of Information','ArialNarrowRedWhiteBold11','paraCenterT');
                        $tableAA1->addCell(6000, $cellColSpan2RW)->addText('Telephone','ArialNarrowRedWhiteBold11','paraCenterT');
                        $lada = empty($cAA1['lada'])?"":'('.$cAA1['lada'].') ';
                        $tableAA1->addRow(350);
                        $tableAA1->addCell(3000, $styleCellAA1)->addText($cAA1['source_information'],'ArialNarrowBlack10','paraCenterT');
                        $tableAA1->addCell(6000, $cellColSpan2)->addText($lada.$cAA1['phone'],'ArialNarrowBlack10','paraCenterT');
                        $tableAA1->addRow();
                        $tableAA1->addCell(3000, $styleCellAA1RW)->addText($param['leyendas'][2],'ArialNarrowRedWhiteBold11','paraCenterT');
                        $tableAA1->addCell(3000, $styleCellAA1RW)->addText($param['leyendas'][3],'ArialNarrowRedWhiteBold11','paraCenterT');
                        $tableAA1->addCell(3000, $styleCellAA1RW)->addText($param['leyendas'][4],'ArialNarrowRedWhiteBold11','paraCenterT');
                        $tableAA1->addRow();
                        $cell = $tableAA1->addCell(3000, $styleCellAA1);
                         $this->newLineCell(number_format($cAA1['land_m2'], 2, '.', ',').' m2\n'.number_format($cAA1['land_ft2'], 2, '.', ',').' ft2', $cell, 'ArialNarrowBlack10','paraCenterT');                        
                        $cell = $tableAA1->addCell(3000, $styleCellAA1);
                         $this->newLineCell('$ '.number_format($cAA1['price_mx']/$er, 2, '.', ',').' \n ', $cell, 'ArialNarrowBlack10','paraCenterT');                         
                        $uv   = $this->creaUnidadesWord($param['tipoComp'],$cAA1['unit_value_mx'],$er,$ft,$ftyear);
                        $cell = $tableAA1->addCell(3000, $styleCellAA1);
                         $this->newLineCell($uv, $cell, 'ArialNarrowBlack10','paraCenterT');
                        $tableAA1->addRow();
                        $tableAA1->addCell(3000, $styleCellAA1RW)->addText('Construction','ArialNarrowRedWhiteBold11','paraCenterT');
                        $tableAA1->addCell(6000, $cellColSpan2RW)->addText($param['leyendas'][5],'ArialNarrowRedWhiteBold11','paraCenterT');
                        $tableAA1->addRow();
                        $cell = $tableAA1->addCell(3000, $styleCellAA1);
                         $this->newLineCell(number_format($cAA1['construction'], 2, '.', ',').' m2\n'.number_format(sprintf("%.3f", $cAA1['construction']*$ft), 2, '.', ',').' ft2', $cell, 'ArialNarrowBlack10','paraCenterT');                        
                        $tableAA1->addCell(6000, $cellColSpan2)->addText($cAA1['time_market'],'ArialNarrowBlack10','paraCenterT');
                                     
                        $tablecRCA1 = $section->addTable('rca1TableStyle');
                        $tablecRCA1->addRow();
                        $tablecRCA1->addCell(9000, $cellColSpan3WB)->addText('RELEVANT CHARACTERISTICS OF THE COMPARABLE '.$x,'ArialNarrowBlackBold11','paraCenterT');
                        $tablecRCA1->addRow();
                        $tablecRCA1->addCell(3000, $styleCellAA1)->addText('1) Property rights conveyed','ArialNarrowBlack10','paraLeftT');
                        $tablecRCA1->addCell(6000, $cellColSpan2)->addText($cAA1['descr1'],'ArialNarrowBlack10','paraLeftT');
                        $tablecRCA1->addRow();
                        $tablecRCA1->addCell(3000, $styleCellAA1)->addText('2) Financing terms','ArialNarrowBlack10','paraLeftT');
                        $tablecRCA1->addCell(6000, $cellColSpan2)->addText($cAA1['descr2'],'ArialNarrowBlack10','paraLeftT');
                        $tablecRCA1->addRow();
                        $tablecRCA1->addCell(3000, $styleCellAA1)->addText('3) Conditions of sale','ArialNarrowBlack10','paraLeftT');
                        $tablecRCA1->addCell(6000, $cellColSpan2)->addText($cAA1['descr3'],'ArialNarrowBlack10','paraLeftT');
                        $tablecRCA1->addRow();
                        $tablecRCA1->addCell(3000, $styleCellAA1)->addText('4) Market Conditions','ArialNarrowBlack10','paraLeftT');
                        $tablecRCA1->addCell(6000, $cellColSpan2)->addText($cAA1['descr4'],'ArialNarrowBlack10','paraLeftT');
                        $tablecRCA1->addRow();
                        $tablecRCA1->addCell(3000, $styleCellAA1)->addText('5) Location','ArialNarrowBlack10','paraLeftT');
                        $tablecRCA1->addCell(6000, $cellColSpan2)->addText($cAA1['descr5'],'ArialNarrowBlack10','paraLeftT');
                        $tablecRCA1->addRow(250);
                        $tablecRCA1->addCell(9000, $cellColSpan3)->addText('6) Physical characteristics','ArialNarrowBlack10','paraLeftT');
                        
                        if($param['tipoComp'] === "CLS")
                        {
                            $size   = number_format($cAA1['land_m2'], 2, '.', ',');
                            $sizeFt = number_format($cAA1['land_ft2'], 2, '.', ',');
                            
                            $tablecRCA1->addRow();
                            $tablecRCA1->addCell(1000, $styleCellAA1)->addText(' ','ArialNarrowBlack10','paraLeftT');
                            $tablecRCA1->addCell(4000, $styleCellAA1)->addText('Size (m2) (ft2)','ArialNarrowBlack10','paraLeftT');
                            $tablecRCA1->addCell(5000, $styleCellAA1)->addText($size.' m2.           '.$sizeFt.' ft2','ArialNarrowBlack10','paraLeftT');
                            $tablecRCA1->addRow();
                            $tablecRCA1->addCell(1000, $styleCellAA1)->addText(' ','ArialNarrowBlack10','paraLeftT');
                            $tablecRCA1->addCell(4000, $styleCellAA1)->addText('Frontage','ArialNarrowBlack10','paraLeftT');
                            $tablecRCA1->addCell(5000, $styleCellAA1)->addText($cAA1['descr7'],'ArialNarrowBlack10','paraLeftT');
                            $tablecRCA1->addRow();
                            $tablecRCA1->addCell(1000, $styleCellAA1)->addText(' ','ArialNarrowBlack10','paraLeftT');
                            $tablecRCA1->addCell(4000, $styleCellAA1)->addText('Access','ArialNarrowBlack10','paraLeftT');
                            $tablecRCA1->addCell(5000, $styleCellAA1)->addText($cAA1['descr8'],'ArialNarrowBlack10','paraLeftT');
                            $tablecRCA1->addRow();
                            $tablecRCA1->addCell(1000, $styleCellAA1)->addText(' ','ArialNarrowBlack10','paraLeftT');
                            $tablecRCA1->addCell(4000, $styleCellAA1)->addText('Shape','ArialNarrowBlack10','paraLeftT');
                            $tablecRCA1->addCell(5000, $styleCellAA1)->addText($cAA1['descr9'],'ArialNarrowBlack10','paraLeftT');
                            $tablecRCA1->addRow();
                            $tablecRCA1->addCell(1000, $styleCellAA1)->addText(' ','ArialNarrowBlack10','paraLeftT');
                            $tablecRCA1->addCell(4000, $styleCellAA1)->addText('Utilities','ArialNarrowBlack10','paraLeftT');
                            $tablecRCA1->addCell(5000, $styleCellAA1)->addText($cAA1['descr10'],'ArialNarrowBlack10','paraLeftT');
                            $tablecRCA1->addRow();
                            $tablecRCA1->addCell(1000, $styleCellAA1)->addText(' ','ArialNarrowBlack10','paraLeftT');
                            $tablecRCA1->addCell(4000, $styleCellAA1)->addText('Topography','ArialNarrowBlack10','paraLeftT');
                            $tablecRCA1->addCell(5000, $styleCellAA1)->addText($cAA1['descr11'],'ArialNarrowBlack10','paraLeftT');
                            $tablecRCA1->addRow();
                            $tablecRCA1->addCell(1000, $styleCellAA1)->addText(' ','ArialNarrowBlack10','paraLeftT');
                            $tablecRCA1->addCell(4000, $styleCellAA1)->addText('Depth radio','ArialNarrowBlack10','paraLeftT');
                            $tablecRCA1->addCell(5000, $styleCellAA1)->addText($cAA1['descr12'],'ArialNarrowBlack10','paraLeftT');
                            $tablecRCA1->addRow();
                            $tablecRCA1->addCell(1000, $styleCellAA1)->addText(' ','ArialNarrowBlack10','paraLeftT');
                            $tablecRCA1->addCell(4000, $styleCellAA1)->addText('Flood hazard','ArialNarrowBlack10','paraLeftT');
                            $tablecRCA1->addCell(5000, $styleCellAA1)->addText($cAA1['descr13'],'ArialNarrowBlack10','paraLeftT');
                            $tablecRCA1->addRow();
                            $tablecRCA1->addCell(1000, $styleCellAA1)->addText(' ','ArialNarrowBlack10','paraLeftT');
                            $tablecRCA1->addCell(4000, $styleCellAA1)->addText('Easements','ArialNarrowBlack10','paraLeftT');
                            $tablecRCA1->addCell(5000, $styleCellAA1)->addText($cAA1['descr14'],'ArialNarrowBlack10','paraLeftT');
                        }
                        else
                        {
                            $size   = number_format($cAA1['construction'], 2, '.', ',');
                            $sizeFt = number_format(($cAA1['construction']*$ft), 2, '.', ',');
                            
                            $tablecRCA1->addRow();
                            $tablecRCA1->addCell(1000, $styleCellAA1)->addText(' ','ArialNarrowBlack10','paraLeftT');
                            $tablecRCA1->addCell(4000, $styleCellAA1)->addText('Size (m2) (ft2)','ArialNarrowBlack10','paraLeftT');
                            $tablecRCA1->addCell(5000, $styleCellAA1)->addText($size.' m2.           '.$sizeFt.' ft2','ArialNarrowBlack10','paraLeftT');
                            $tablecRCA1->addRow();
                            $tablecRCA1->addCell(1000, $styleCellAA1)->addText(' ','ArialNarrowBlack10','paraLeftT');
                            $tablecRCA1->addCell(4000, $styleCellAA1)->addText('Condition','ArialNarrowBlack10','paraLeftT');
                            $tablecRCA1->addCell(5000, $styleCellAA1)->addText($cAA1['descr7'],'ArialNarrowBlack10','paraLeftT');
                            $tablecRCA1->addRow();
                            $tablecRCA1->addCell(1000, $styleCellAA1)->addText(' ','ArialNarrowBlack10','paraLeftT');
                            $tablecRCA1->addCell(4000, $styleCellAA1)->addText('Construction Quality','ArialNarrowBlack10','paraLeftT');
                            $tablecRCA1->addCell(5000, $styleCellAA1)->addText($cAA1['descr8'],'ArialNarrowBlack10','paraLeftT');
                            $tablecRCA1->addRow();
                            $tablecRCA1->addCell(1000, $styleCellAA1)->addText(' ','ArialNarrowBlack10','paraLeftT');
                            $tablecRCA1->addCell(4000, $styleCellAA1)->addText('Functional Utility','ArialNarrowBlack10','paraLeftT');
                            $tablecRCA1->addCell(5000, $styleCellAA1)->addText($cAA1['descr9'],'ArialNarrowBlack10','paraLeftT');
                            $tablecRCA1->addRow();
                            $tablecRCA1->addCell(1000, $styleCellAA1)->addText(' ','ArialNarrowBlack10','paraLeftT');
                            $tablecRCA1->addCell(4000, $styleCellAA1)->addText('Obsolescence','ArialNarrowBlack10','paraLeftT');
                            $tablecRCA1->addCell(5000, $styleCellAA1)->addText($cAA1['descr10'],'ArialNarrowBlack10','paraLeftT');
                            $tablecRCA1->addRow();
                            $tablecRCA1->addCell(1000, $styleCellAA1)->addText(' ','ArialNarrowBlack10','paraLeftT');
                            $tablecRCA1->addCell(4000, $styleCellAA1)->addText('Age and Maintenance','ArialNarrowBlack10','paraLeftT');
                            $tablecRCA1->addCell(5000, $styleCellAA1)->addText($cAA1['descr11'],'ArialNarrowBlack10','paraLeftT');                            
                        }
                        
                        if (!empty($cAA1['rc6_9_titulo']))
                        {
                            $tablecRCA1->addRow();
                            $tablecRCA1->addCell(1000, $styleCellAA1)->addText(' ','ArialNarrowBlack10','paraLeftT');
                            $tablecRCA1->addCell(4000, $styleCellAA1)->addText($cAA1['rc6_9_titulo'],'ArialNarrowBlack10','paraLeftT');
                            $tablecRCA1->addCell(5000, $styleCellAA1)->addText($cAA1['descr15'],'ArialNarrowBlack10','paraLeftT');                            
                        }
                        if (!empty($cAA1['rc6_10_titulo']))
                        {
                            $tablecRCA1->addRow();
                            $tablecRCA1->addCell(1000, $styleCellAA1)->addText(' ','ArialNarrowBlack10','paraLeftT');
                            $tablecRCA1->addCell(4000, $styleCellAA1)->addText($cAA1['rc6_10_titulo'],'ArialNarrowBlack10','paraLeftT');
                            $tablecRCA1->addCell(5000, $styleCellAA1)->addText($cAA1['descr16'],'ArialNarrowBlack10','paraLeftT');                            
                        }                        
                        $tablecRCA1->addRow();
                        $tablecRCA1->addCell(5000, $styleCellAA1)->addText('7) Use zoning '.$this->revisaValorVacio($cAA1['rc7_adjus']),'ArialNarrowBlack10','paraLeftT');
                        $tablecRCA1->addCell(1000, $styleCellAA1)->addText(' ','ArialNarrowBlack10','paraLeftT');
                        $tablecRCA1->addCell(4000, $styleCellAA1)->addText($cAA1['descr17'],'ArialNarrowBlack10','paraLeftT');
                        $tablecRCA1->addRow();
                        $rc8  = $this->optionsRC8($param['tipoComp']);
                        
                        $tablecRCA1->addCell(3000, $styleCellAA1)->addText('8) '.$leyendaRC8,'ArialNarrowBlack10','paraLeftT');
                        $tablecRCA1->addCell(2000, $styleCellAA1)->addText(' ','ArialNarrowBlack10','paraLeftT');
                        $tablecRCA1->addCell(5000, $styleCellAA1)->addText($rc8[$cAA1['rc8']],'ArialNarrowBlack10','paraLeftT');
                        $tablecRCA1->addRow();
                        $tablecRCA1->addCell(3000, $styleCellAA1)->addText('Additional comments: ','ArialNarrowBlackCur10','paraLeftT');                       
                        $tablecRCA1->addCell(8000, $cellColSpan2)->addText($cAA1['comments'],'ArialNarrowBlack10','paraLeftT');                        

                        $section->addPageBreak(); 
                    }
                
                return $section;
             
            } catch (Exception $e) {echo 'creaAnexoCompWord Excepción: ',  $e, "\n";}   
        }
                    

        public function exportaExcel($infoFile,$comparables)
        {
         try{
             
            $objPHPExcel = new Excel();                                                 
             
            $objPHPExcel->getProperties()
                         ->setCreator("Hermes")
                         ->setLastModifiedBy("Hermes") 
                         ->setTitle("Reporte Excel Comparables")
                         ->setSubject("Hermes")
                         ->setDescription("Comparables")
                         ->setKeywords("Comparables, valuation report")
                         ->setCategory("Reporte excel Comparables");
                         
            if($infoFile['tipo_c']=="CLS") {                    
                    $tituloReporte = " LAND FOR SALE Comparables JLL México";
                    $titulosColumnas = array('Type of property','Street/Av','No','Neighborhood','City / Municipality','State','Zipcode','Latitude','Longitude','Source of Information','Phone Code','Telephone','Area of Comparable', 'Price', 'Unit Value','Construction', 'Time on market','Listing / Sale','Closing / Listing Date','Exchange Rate','Additional comments' );
                } elseif ($infoFile['tipo_c']=="CRL") {
                    $tituloReporte = " PROPERTY FOR LEASE Comparables JLL México";
                    $titulosColumnas = array('Type of property','Street/Av','No','Neighborhood','City / Municipality','State','Zipcode','Latitude','Longitude','Source of Information','Phone Code','Telephone','Comparable area', 'Monthly Rent MXN', 'Monthly Rent','Construction','Closing / Listing Date','Listing / Real Rent', 'Closing / Listing Date','Exchange Rate','Additional comments' );
                }  else { 
                    $tituloReporte = " PROPERTY FOR SALE Comparables JLL México";               
                    $titulosColumnas = array('Type of property','Street/Av','No','Neighborhood','City / Municipality','State','Zipcode','Latitude','Longitude','Source of Information','Phone Code','Telephone','Comparable area', 'Total Sale Price MXN', 'Unit Sale Price','Construction', 'Lease Date','Listing / Sale','Closing / Listing Date','Exchange Rate','Additional comments' );
                }
            $estiloTituloReporte = array('font' => array('name'      => 'Verdana',
                                                        'bold'      => true,
                                                        'italic'    => false,
                                                        'strike'    => false,
                                                        'size'      =>16,
                                                        'color'     => array('rgb' => 'FFFFFF')
                                                    ),
                                        'fill' => array('type'  => PHPExcel_Style_Fill::FILL_SOLID,
                                                        'color' => array('argb'  => 'FF220835')
                                                       ),
                                        'borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_NONE)
                                                          ),
                                        'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                                                             'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                                                             'rotation'   => 0,
                                                             'wrap'       => TRUE
                                                            )
                                    );

            $estiloTituloColumnas = array('font' => array('name'  => 'Arial',
                                                          'bold'  => true,
                                                          'color' => array('rgb' => 'FFFFFF')
                                        ),
                                        'fill' => array('type'        => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
                                                         'rotation'   => 90,
                                                         'startcolor' => array('rgb' => 'CB0B0B'),
                                                         'endcolor'   => array('rgb' => 'F5B093')
                                                        ),
                                        'borders' => array('top' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM ,
                                                                          'color' => array('rgb' => 'EE7A48')
                                                                         ),
                                                        'bottom' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM ,
                                                                          'color' => array('rgb' => 'EE7A48')
                                                                         )
                                                        ),
                                        'alignment' =>  array(
                                            'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                                            'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                                            'wrap'      => TRUE
                                        )
                                    );

            $estiloInformacion = new PHPExcel_Style();
            $estiloInformacion->applyFromArray( array('font'  => array(
                                                        'name'  => 'Arial',
                                                        'color' => array('rgb' => '000000')
                                                     ),
                                        'fill'    => array('type'    => PHPExcel_Style_Fill::FILL_SOLID,
                                                            'color'   => array('rgb' => 'FFFFFF')),
                                        'borders' => array('left'  => array('style' => PHPExcel_Style_Border::BORDER_THIN ,
                                                                            'color' => array('rgb' => 'EE7A48')
                                                                             )
                                                          )                                        
                                    ));
            $objPHPExcel->getActiveSheet()->setTitle('Hermes Comparables '.$infoFile['tipo_c']);
            $objPHPExcel->setActiveSheetIndex(0)
                        ->mergeCells('A1:T1');
 
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A1',$tituloReporte) 
                        ->setCellValue('A3',  "           ")
                        ->setCellValue('B3',  $titulosColumnas[0])
                        ->setCellValue('C3',  $titulosColumnas[1])
                        ->setCellValue('D3',  $titulosColumnas[2])
                        ->setCellValue('E3',  $titulosColumnas[3])
                        ->setCellValue('F3',  $titulosColumnas[4])
                        ->setCellValue('G3',  $titulosColumnas[5])
                        ->setCellValue('H3',  $titulosColumnas[6])
                        ->setCellValue('I3',  $titulosColumnas[7])
                        ->setCellValue('J3',  $titulosColumnas[8])
                        ->setCellValue('K3',  $titulosColumnas[9])
                        ->setCellValue('L3',  $titulosColumnas[10])
                        ->setCellValue('M3',  $titulosColumnas[11])
                        ->setCellValue('N3',  $titulosColumnas[12])
                        ->setCellValue('O3',  $titulosColumnas[13])
                        ->setCellValue('P3',  $titulosColumnas[14])
                        ->setCellValue('Q3',  $titulosColumnas[15])
                        ->setCellValue('R3',  $titulosColumnas[16])
                        ->setCellValue('S3',  $titulosColumnas[17])
                        ->setCellValue('T3',  $titulosColumnas[18])
                        ->setCellValue('U3',  $titulosColumnas[19])
                        ->setCellValue('V3',  $titulosColumnas[20])
                        ;
            $numCelda = 3;
            foreach ($comparables as $c)
            {  $numCelda++;
               $objDrawing = new PHPExcel_Worksheet_Drawing();
               $objDrawing->setName('Photo Comparable');
               $objDrawing->setDescription('Photo Comparable');
               $objDrawing->setPath($infoFile['galeriaComp'].$c['foto']);
               $objDrawing->setCoordinates('A'.$numCelda);
               $objDrawing->setResizeProportional(false);
               $objDrawing->setWidth(60);
               $objDrawing->setHeight(60);
               $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
              
               $cons_ft2        = $c['construction'] * $infoFile['dc']['ft'];
               $uv              = $c['unit_value_mx'];
               $uv_usd          = ($infoFile['tipo_c']==="CRL")?(($uv/$c['exchange_rate'])*$infoFile['dc']['ftyear']):($infoFile['tipo_c']==="CRS")?( ($uv/$c['exchange_rate'])/$infoFile['dc']['ft']):($uv/$c['exchange_rate']);
               $constructionTxt = number_format($c['construction'], 2, '.', ',')." m2".PHP_EOL.number_format($cons_ft2, 2, '.', ',')." ft2";
               $priceTxt        = '$ '.number_format($c['price_mx'], 2, '.', ',')." MXN".PHP_EOL.'$ '.number_format($c['price_mx']/$c['exchange_rate'], 2, '.', ',')." USD";
               $landTxt         = number_format($c['land_m2'], 2, '.', ',')." m2".PHP_EOL.number_format($c['land_ft2'], 2, '.', ',')." ft2";                
               
               if($infoFile['tipo_c']=="CLS"){ $unitValueTxt = "$".number_format($uv ,2, '.', ',' )." MXN / m2".PHP_EOL."$".number_format($uv_usd ,2, '.', ',' )." USD / m2".PHP_EOL."$".number_format($uv_usd/$infoFile['dc']['ft'] ,2, '.', ',' )." USD / ft2"; }
                elseif($infoFile['tipo_c']=="CRS"){ $unitValueTxt = "$".number_format($uv ,2, '.', ',' )." MXN / m2".PHP_EOL."$".number_format($uv_usd ,2, '.', ',' )." USD / ft2"; }
                   else{ $unitValueTxt = "$".number_format($uv ,2, '.', ',' )." MXN / m2 / mo".PHP_EOL."$".number_format($uv/$c['exchange_rate'] ,2, '.', ',' )." USD / m2 / mo".PHP_EOL."$".number_format($uv_usd ,2, '.', ',' )." USD / ft2 / year"; }

              $objPHPExcel->getActiveSheet()->getRowDimension($numCelda)->setRowHeight(60);
              $objPHPExcel->setActiveSheetIndex(0)                          
                          ->setCellValue('B'.$numCelda, $c['type_property'])
                          ->setCellValue('C'.$numCelda, $c['calle'])
                          ->setCellValue('D'.$numCelda, $c['num'])
                          ->setCellValue('E'.$numCelda, $c['col'])
                          ->setCellValue('F'.$numCelda, $c['mun'])
                          ->setCellValue('G'.$numCelda, $c['edo'])
                          ->setCellValue('H'.$numCelda, $c['cp'])
                          ->setCellValue('I'.$numCelda, $c['latitud'])
                          ->setCellValue('J'.$numCelda, $c['longitud'])
                          ->setCellValue('K'.$numCelda, $c['source_information'])
                          ->setCellValue('L'.$numCelda, $c['lada'])
                          ->setCellValue('M'.$numCelda, $c['phone'])
                          ->setCellValue('N'.$numCelda, $landTxt)
                          ->setCellValue('O'.$numCelda, $priceTxt)
                          ->setCellValue('P'.$numCelda, $unitValueTxt)
                          ->setCellValue('Q'.$numCelda, $constructionTxt)
                          ->setCellValue('R'.$numCelda, $c['time_market'])
                          ->setCellValue('S'.$numCelda, $c['rc8'])
                          ->setCellValue('T'.$numCelda, $c['closing_listing_date'])
                          ->setCellValue('U'.$numCelda, "$".$c['exchange_rate'].PHP_EOL.$c['date_exchange_rate'])
                          ->setCellValue('V'.$numCelda, $c['comments'])
                          ;
            }
            
            $objPHPExcel->getActiveSheet()->getStyle('A1:W1')->applyFromArray($estiloTituloReporte);
            $objPHPExcel->getActiveSheet()->getStyle('A3:W3')->applyFromArray($estiloTituloColumnas);
            $objPHPExcel->getActiveSheet()->setSharedStyle($estiloInformacion, "A4:U".$numCelda);
            $objPHPExcel->getActiveSheet()->getStyle("A4:W".$numCelda)->applyFromArray(array('alignment' =>  array('horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY,
                                                                                            'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                                                                                            'wrap'      => TRUE
                                                                                           )));
            
            for($i = 'A'; $i <= 'V'; $i++){ $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension($i)->setAutoSize(TRUE); }
            // Inmovilizar paneles
            $objPHPExcel->getActiveSheet(0)->freezePaneByColumnAndRow(0,4);
            
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');            
            $objWriter->save($infoFile['dir'].$infoFile['name']);
                        
            return $infoFile['name'];
            
            } catch (Exception $e) {echo 'exportaExce Excepción: ',  $e, "\n";} 
        }

                
    
    public function revisaValorVacio($v_entrada)
    {
        $v_salida = "";
        if (!empty($v_entrada))
                $v_salida = $v_entrada;
                
        return $v_salida;
    }
        

        public function compositionWORD($fc,$fcFT,$unidades)
        {
            return '$ '.number_format($fc, 2, '.', ',').' usd/m2\n$'.number_format($fcFT, 2, '.', ',').' '.$unidades;
        }
        
        public function traeRCAdjustmentCriteria($tipo,$cAA1,$rc,$limiteRC8)
        { 
            $valor = $cAA1[$tipo];
            $valorAC = NULL;
            
            switch ($tipo){
            case "rc1";
            case "rc2"; 
            case "rc3";
            case "rc4";
                $valorAC = $cAA1[$tipo."_por"] / 100;
            break;
            case "rc6_0";
               $valorAC = $rc['v_1'] / 100;
            break;
            //id del catalogo 13=Very Superior==>v_1, 14=Superior==>v_2, 15=Similar==>v_3, 16=Inferior==>v_4, 17=Very Inferior==>v_5
            case "rc5";
            case "rc6_1";
            case "rc6_2";
            case "rc6_3";
            case "rc6_4";
            case "rc6_5";
            case "rc6_6";
            case "rc6_7";
            case "rc6_8";
            case "rc6_9";
            case "rc6_10";
            case "rc7";                
                 $campoBD = $valor - 12;                 
                 $valorAC = ($campoBD<=0)?0:$rc["v_$campoBD"]/100;                 
            break;
            case "rc8":
                 $valor   = $cAA1[$tipo];                
                 $valorAC = $valor==0?0:$rc["v_$valor"]/100;
            break;
            }
            return $valorAC;
        }
        
        public function traeAdjustmentCriteriaPorDesc($tipo,$x,$cAA1,$porcentaje,$tipoComp,$saltoPagina)
        {
            switch ($tipo){
            case "rc1";
            case "rc2"; 
            case "rc3";
            case "rc4";
               $renglon =round(($porcentaje*100),2) .'% '.(empty($cAA1[$tipo."_adjus"])?$cAA1["descr".$x]:$cAA1[$tipo."_adjus"]);
            break;
            case "rc6_0";               
            case "rc5";
            case "rc6_1";
            case "rc6_2";
            case "rc6_3";
            case "rc6_4";
            case "rc6_5";
            case "rc6_6";
            case "rc6_7";
            case "rc6_8";                        
            case "rc7";
                 $valor = $cAA1[$tipo];
                 $renglon =round(($porcentaje*100),2) .'% '.$cAA1["descr".$x];
            break;
            case "rc8":
                 $valor =$cAA1["rc8"];
                 $rc8  = $this->optionsRC8($tipoComp);
                 $renglon =round(($porcentaje*100),2) .'% '.$rc8[$valor];
            break;
            case "rc6_9";
                $titulo  = empty($cAA1["rc6_9_titulo"])?"":$cAA1["rc6_9_titulo"];
                //$renglon = $titulo.": ".$saltoPagina.round(($porcentaje*100),2) .'% '.$cAA1["descr".$x];                
                $renglon = $saltoPagina.round(($porcentaje*100),2) .'% '.$cAA1["descr".$x];                
            break;
            case "rc6_10";
                $titulo  = empty($cAA1["rc6_10_titulo"])?"":$cAA1["rc6_10_titulo"];
                //$renglon = $titulo.": ".$saltoPagina.round(($porcentaje*100),2) .'% '.$cAA1["descr".$x];
                $renglon = $saltoPagina.round(($porcentaje*100),2) .'% '.$cAA1["descr".$x];                
            break;
            }
            return $renglon;
        }
        
        public function traeLeyendaRC($x,$cAA1,$rc,$tipo)
        {   
            $menosRC = ($tipo==="AA2")?0:3;
            
            switch ($x){
                case (15-$menosRC);
                    $leyenda =  empty($cAA1["rc6_9_titulo"])?NULL:$cAA1["rc6_9_titulo"];
                    break;
                /*case (16-$menosRC);
                    $leyenda =  "";
                    break;*/
                case (16-$menosRC);
                    $leyenda = empty($cAA1["rc6_10_titulo"])?NULL:$cAA1["rc6_10_titulo"];
                    break;
                case (17-$menosRC);
                    $leyenda =  empty($cAA1["rc7_adjus"])?$rc:$rc." ".$cAA1["rc7_adjus"];
                    break;  
                default :
                    $leyenda = $rc;
                    break;               
            }
            return $leyenda;
        }
        
        
        public function traeElementRC($campo,$relevantCH)
        {
            foreach($relevantCH as $rc)
            {
                if ($rc['tipo']==$campo)
                { return $rc; }
            }
        }               
                
                
        /**
     * This user-land implementation follows the implementation quite strictly;
     * it does not attempt to improve the code or algorithm in any way. It will
     * raise a warning if you have fewer than 2 values in your array, just like
     * the extension does (although as an E_USER_WARNING, not E_WARNING).
     * 
     * @param array $a 
     * @param bool $sample [optional] Defaults to false
     * @return float|bool The standard deviation or false on error.
     */
     public function stats_standard_deviation(array $a) {
        $n = count($a);
        if ($n === 0) {
            trigger_error("The array has zero elements", E_USER_WARNING);
            return false;
        }
        
        $mean = array_sum($a) / $n;        
        $carry = 0.0;
        foreach ($a as $val) {
            $d = ((double) $val) - $mean;         
            $carry += $d * $d;
        }        
        return sqrt($carry / $n);
    }
    
    
    public function creaLocation($calle,$num,$col,$mun,$edo,$cp)
    {     
        $strCalle = empty($calle)?'':$calle;
        $strNum   = empty($num)?'':' '.$num;
        $strCol   = empty($col)?'':', '.$col;
        $strMun   = empty($mun)?'':', '.$mun;
        $strEdo   = empty($edo)?'':', '.$edo;
        $strCP    = empty($cp)?'':', CP '.$cp;
        return $strCalle.$strNum.$strCol.$strMun.$strEdo.$strCP;
    }
    
    public function creaCol($col,$mun,$edo,$cp)
    {             
        $strCol   = empty($col)?'':$col;
        $strMun   = empty($mun)?'':', '.$mun;
        $strEdo   = empty($edo)?'':', '.$edo;
        $strCP    = empty($cp)?'':', CP '.$cp;
        return $strCol.$strMun.$strEdo.$strCP;
    }        
    
    public function creaUnidadesWord($tipoComp,$ap,$er,$ft,$ftyear)
    {
         if ($tipoComp==="CRL")
                { $unitValueUSD='$ '.number_format($ap/$er, 2, '.', ',').' usd/m2/mo.\n'.'$ '.number_format(($ap/$er)*$ftyear, 2, '.', ',').' usd/ft2/yr'; }
             else
                { $unitValueUSD='$ '.number_format($ap/$er, 2, '.', ','). ' usd/m2\n'.'$ '.number_format(($ap/$er)/$ft, 2, '.', ',').' usd/ft2'; }
        
        return $unitValueUSD;
    }
  
    
    public function creaAPWord($tipoComp,$ap,$er,$ft,$ftyear)
    {
        if ($tipoComp==="CRL")
                { $AP='$ '.number_format($ap/$er, 2, '.', ',').' usd/m2/mo.\n$'.number_format(($ap/$er)*$ftyear, 2, '.', ',').' usd/ft2/yr'; }
             else
                { $AP='$ '.number_format($ap/$er, 2, '.', ',').' usd/m2\n$'.number_format(($ap/$er)/$ft, 2, '.', ',').' usd/ft2'; }
        
        return $AP;
    }
    
    
    
    public function creaTablaPhotosWord($fotos,$section)
    {
        $x           = -1;
        $tablePhotos = $section->addTable('rca1TableStyle');
        $tablePhotos->addRow();
        
        foreach ($fotos as $f) 
        {  $x++;

        if( $x === 2 )
           { 
             $tablePhotos->addRow();
             $x = 0;
           }
           
        if( $x === 1 )
           { $tablePhotos->addCell(2000, array('valign' => 'center','spaceBefore' => 0, 'spaceAfter' => 0))->addText('   ','ArialNarrowBlack10','paraLeftT'); }
           
        $cell    = $tablePhotos-> addCell(4800, array('valign' => 'center','spaceBefore' => 0, 'spaceAfter' => 0));
        $textrun = $cell       -> addTextRun(   array('align'  => 'center','spaceBefore' => 0, 'spaceAfter' => 0));           
        $this   ->addmyImage($textrun, $f['nombre'], 281, 224,"center");//ANCHO: 7.9CM Y ALTO: 5.9CM 
        $textrun->addText($f['titulo'],'ArialNarrowBlackBold11','paraCenterT');
        $textrun->addTextBreak(2);
        }
        
       return $section;
    }
    
    public function creaTablaAAIMGWord($id_in,$aaimg,$numAnexo,$word,$md_gallery,$fotoWatermark,$appraisal_num)
    {
    try{     
     foreach ($aaimg as $aa)
             {
                $numAnexo++;
                $fotosAAIMG  = $md_gallery -> traeFotosAAIMG($id_in,"AAIMG",$aa['titulo']);
                $ta          = str_replace("A.- X", "Annex ".$numAnexo.". - ", $aa['anexo']);
                $taFinal     = ($fotosAAIMG[0]['titulo']==="34")?$fotosAAIMG[0]['extraAA']:$ta;
                $tituloAnexo = strtoupper(str_replace("A.- X", "Annex ".$numAnexo.". - ", $taFinal));
                
                if( ($aa['titulo']== "29") || ($aa['titulo']== "30") )
                    { $width   = 850;
                      $height  = 460;
                      $section = $word->createSection(array('orientation'=>'landscape')); 
                      $this->addHeaderLandscape($section, 'Appraisal No. :'.$appraisal_num, $tituloAnexo, $fotoWatermark);
                      $this->addFooterLandscape($section);
                    }
                else    
                    { $width   = 560;
                      $height  = 760;
                      $section = $word->addSection(); 
                      $this->addHeader($section, 'Appraisal No. :'.$appraisal_num, $tituloAnexo, $fotoWatermark);
                      $this->addFooter($section, TRUE);
                    }

                $section->addTitle(htmlspecialchars($tituloAnexo), 3);
                $section->addTextBreak();                
                $tableAAIMG = $section->addTable('rca1TableStyle');
                $limAA8     = sizeof($fotosAAIMG);
                $x          = 0;
                foreach ($fotosAAIMG as $f)
                { $x++;
                  $tableAAIMG->addRow(); 
                  $cell    = $tableAAIMG-> addCell(9000, array('valign' => 'center','spaceBefore' => 0, 'spaceAfter' => 0));
                  $textrun = $cell      -> addTextRun(array('align' => 'center','spaceBefore' => 0, 'spaceAfter' => 0));
                  $this->addmyImage($textrun, $f['nombre'], $width, $height,"center");
                }
             }
    } catch (Exception $e) {echo 'creaTablaAAIMGWord Excepción: ',  $e, "\n";}

   return $numAnexo;
   }
    
    
    public function leyendasFirmas()
    {
      return  array("vcs"      => "Valuation and Consulting Services"
                   );
    }        
    
    public function leyendasAPV()
    {
      return  array("titulo"      => "PROPERTY APPRAISED",
                    "l1"          => "Description",
                    "t1"          => "In the Cost Approach a property is valued based on a comparison with the cost to build a new or substitute property. The cost estimate is adjusted for all types of depreciation of in the property. To obtain the final value indication the land value is added to the depreciated value of the improvements.",
                    "l2"          => "Land Value",
                    "t2"          => "The area where the land lot is located is a transition zone  surrounded by industrial, commercial, residential  and storage buildings with an average age of twenty years, vacant lots in this area are scarce, so for the analysis of the subject, comparables with industrial and commercial uses will be used.",
                    "t2_1"        => "Annex 1 contains market schedules  market information for different comparables that were found in the area and Annex 2 includes the adjustments that were made in order to arrive to the following conclusion of the land's value ",
                    "t2_2"        => "In Annex 6 there are maps with the location of the subject's land and comparables.",
                    "tTot"        => "Land Surface",
                    "tUV"         => "Unit Value",
                    "un"          => "Unit",
                    "tToV"        => "Total Value of Land",
                    "roun"        => "Rounded",
                    "l3"          => "Value of Improvements:",
                    "t3"          => "In order to obtain replacement cost we use costs manuals..... In additional, the entrepreneurial profit and obsolescence derived from propertys age are included, considering the average date estimated for each built area.",
                    "l4"          => "In the following table we present the cost in USD currency",
                    "fbh"         => "Factor for double height",
                    "ffc"         => "Factor for city",
                    "uc"          => "Unit Cost",
                    "rcnFC"       => "Replacement Cost New",
                    "rcn"         => "Replacement Cost New (USD)",
                    "rrc"         => "Reproduction or replacement costs:",
                    "mb"          => "Main buildings",
                    "qt"          => "Quantity",
                    "tc"          => "Total construction",
                    "ae"          => "Equipment",
                    "Tae"         => "Total Equipment Replacement Cost New ",
                    "cw"          => "Complementary Works",
                    "Tcw"         => "Total Complementary Works",
                    "tf"          => "Total Facilities",
                    "TaeTcw"      => "Total Equipment + Complementary Works",
                    "trc"         => "Total Replacement Cost New",                    
                    "TaeTcwC"     => "Total Replacement Cost New (construction, equipment and complementary works)",
                    "trcC"        => "Total Cost construction and Accessory and Complementary works",                    
                    "l12"         => "No.",
                    "l13"         => "Item",
                    "l14"         => "Age/years",
                    "l15"         => "Useful life",
                    "l16"         => "Age Factor",
                    "l17"         => "Depreciation(USD)",
                    "dep"         => "Depreciation",
                    "l18"         => "Replacement cost",
                    "l19"         => "Total cost",
                    "cpdTit"      => "Curable Physical Deterioration (deferred maintenance)",
                    "cpdTot"      => "Total curable physical deterioration",
                    "cpdTbl"      => "Deferred Maintenance (USD)",
                    "mbTot"       => "Total depreciation of constructions",
                    "depTotCW"    => "Total depreciation complementary works",
                    "depTotAE"    => "Total depreciation of equipment",
                    "depTot"      => "Total depreciation",
                    "groundFloor" => "Ground Floor Area",
                    "gba"         => "Gross Building Area",
                    "tra"         => "Total Rentable Area",
                    "ra"          => "Rentable Area",
                    "amount"      => "Amount",
                    "ealm"        => "Economic Age - Life Method",
                    "aa"          => "Area Analysis",
                    "cap"         => "Construction Area of the Property",
                    "tax"         => "Tax Bill",
                    "deed"        => "Deed",
                    "land"        => "Land Area",
                    "pp"          => "Provided Plans",
                    "ld"          => "Land Difference",
                    "ad"          => "Depreciation",
                    "ipd"         => "Incurable physical deterioration",
                    "tpd"         => "Total physical deterioration",
                    "cfo"         => "Curable functional obsolescence :",
                    "dfA"         => "Deficiency (addition)",
                    "defS"        => "Deficiency (substitution) :",
                    "oa"          => "Superadequacy :",
                    "tcfo"        => "Total curable functional obsolescence :",
                    "tcfoIn"      => "Total incurable functional obsolescence :",
                    "ifo"         => "Incurable functional obsolescence:",
                    "defy"        => "Deficiency:",
                    "oAd"         => "Superadequacy :",                    
                    "EO"          => "External obsolescence :",                    
                    "dvc"         => "Depreciated value of constructions:",
                    "dvc2"        => "Depreciated value of other constructions:",
                    "lv"          => "Land value :",
                    "vica"        => "Value indication by cost approach",
                    "eo1"         => "Total investment (without external obsolescence) (Land + constructions)",
                    "eo2"         => "Capitalization rate: (Minimum feasibility rate)",
                    "eo3"         => "Net rent:",
                    "eo4"         => "Expenses",
                    "eo5"         => "Effective gross income:",
                    "eo6"         => "Vacancy and collection losses:",
                    "eo7"         => "Required annual gross income:",
                    "eo8"         => "Required annual unit rent :",
                    "eo9"         => "Required unit monthly rent :",
                    "eo10"        => "Unit market rent:",
                    "eo11"        => "(Lease adjustment grid)",
                    "eo12"        => "Differences in rent :",
                    "eo13"        => "Vacancy:",
                    "eo14"        => "Effective rent:",
                    "eo15"        => "Operating Expense:",
                    "eo16"        => "NOI (unit cost):",
                    "eo17"        => "NOI (annual cost):",
                    "eo18"        => "Capitalization Rate:",
                    "eo19"        => "Capitalized loss:",
                    "ugmi"        => "Unit Gross Monthly Income",
                    "ugai"        => "Unit Gross Annual Income",
                    "pgi"         => "Potential Gross Income",
                    "rent"        => "Rent (per m2 pear year) ",
                    "anIn"        => "Annual Income (USD)",
                    "err"         => "Expense reimbursement revenue",   
                    "main"        => "[Ej. Maintenance]",
                    "sec"         => "[Ej. Security]",
                    "tpgip"       => "Total potential gross income 100% occupancy",
                    "mvcl"        => "Minus vacancy and collection losses",
                    "egi"         => "Effective gross income:",
                    "st"          => "Short term",
                    "lt"          => "Long term",
                    "serr"        => "Subtotal expense reimbursement revenue",
                    "tpgio"       => "Total potential gross income @ 100% occupancy",                    
                    "oe"          => "Operating expense",                    
                    "se"          => "Subtotal Expenses",
                    "unf"         => "Unforeseen",
                    "totExp"      => "Total expenses",
                    "noi"         => "Net Operating Income",
                    "inEx"        => "Income and Expenses",
                    "source"      => "Source",
                    "cr"          => "Capitalization Rate",
                    "ran"         => "Range",
                    "av"          => "*Average (not necessarily arithmetic mean)",
                    "mir"         => "Mortgage Interest Rate",
                    "mt"          => "Mortgage Term (Amortization Period)",
                    "lvr"         => "Loan-to-Value Ratio",
                    "morCo"       => "Mortgage Constant",
                    "edr"         => "Equity Dividend Rate (EDR)",
                    "morRe"       => "Mortgage Requirement",
                    "eqRe"        => "Equity Requirement",
                    "icr"         => "Indicated Capitalization Rate",
                    "roFor"       => "Ro = DCR x Rm x M",
                    "whereDCR"    => "Where DCR = Debt Coverage Ratio",
                    "rmLbl"       => "Rm  = Mortgage Capitalization Rate",
                    "mLbl"        => "M   = Loan to Value Ratio",
                    "dcrTit"      => "Considering a typical DCR",
                    "dcrLbl"      => "and similar financing conditions",
                    "ir"          => "Interest rate (i)",
                    "amoPe"       => "Amortization period (n)",
                    "lvLbl"       => "Loan-to-Value:  (M)",
                    "mcLbl"       => "Mortgage Constant (Rm)",
                    "cLbl1"       => "Considering a weighted average from the previous results we have",
                    "cLbl2"       => "With the Net Operating Income and the Capitalization Rate the value under Direct Income Capitalization is obtained as follows",
                    "ica"         => "Income Capitalization Approach",
                    "ivudica"     => "Indicated value under direct income capitalization approach:",
                    "far"         => "Floor Area Ratio :",
                    "mf"          => "Market Factor:",
                    "ls"          => "Land surface:",
                    "fArea"       => "Footing area:",
                    "tls"         => "Typical land surface",
                    "exl"         => "Excess land",
                    "uvel"        => "Unit value of excess land",
                    "vel"         => "Value of excess land",
                    "ivudicael"   => "(including excess land):  ",
                    "saop"        => "Saleable area of the property",
                    "uav"         => "Unit adjusted value",
                    "ivusca"      => "Indicated value under Sales Comparison Approach",
                    "cavweo"      => "Cost approach value without external obsolescence",
                    "valAp"       => "Valuation Approach",
                    "currency"    => "usd",
                    "currencyApp" => "Total Value (USD)",
                    "currencyMX"  => "mxn",
                    "costAp"      => "Cost Approach",
                    "sca"         => "Sales Comparison Approach",
                    "vis"         => "Value in Use",
                    "marval"      => "Market Value ",
                    "sugRate"     => "Suggested rate:",
                    "totRiskP"    => "Total risk premium",
                    "discRate"    => "Discount rate",
                    "liqVal"      => "Liquidation Value",
                    "basVal"      => "Base Value",
                    "rfir"        => "Risk-Free interest rate",
                    "adPoin"      => "Additional Points",
                    "oppCos"      => "Opportunity Cost",
                    "estMonS"     => "Estimated Months for Sale",
                    "addRisk"     => "Additional Risk",
                    "difRfir"     => "Dif to RFIR",
                    "mrfir"       => "Monthly Risk free interest Rate",                    
                    "manCos"      => "Management Costs",
                    "perYear"     => "Per Year",
                    "perMonth"    => "Per Month",
                    "month"       => "Monthly",
                    "saleCos"     => "Sale Cost",
                    "commSale"    => "Commission per Sale",
                    "liqFactor"   => "Liquidation Factor",
                    "properTax"   => "Property Tax",
                   );
    }        
    
    public function creaCoreAPVIWORD($section,$datos)
    {
    try{$param            = $this->leyendasAPV();
    
        $cNoLineBot       = array(                'valign' => 'center','borderBottomSize' => 10,'borderBottomColor'=>'#FFFFFF','spaceAfter' => 0,'spaceBefore' => 0);
        $cNoLineBotS2     = array('gridSpan' => 2,'valign' => 'center','borderBottomSize' => 10,'borderBottomColor'=>'#FFFFFF','spaceAfter' => 0,'spaceBefore' => 0);    
        $cLineBotThin     = array(                'valign' => 'center','borderBottomSize' => 10,'borderBottomColor'=>'#000000','spaceAfter' => 0,'spaceBefore' => 0);
        $cLineBotThinS2   = array('gridSpan' => 2,'valign' => 'center','borderBottomSize' => 10,'borderBottomColor'=>'#000000','spaceAfter' => 0,'spaceBefore' => 0);
        $cLineBotThinS3   = array('gridSpan' => 3,'valign' => 'center','borderBottomSize' => 10,'borderBottomColor'=>'#000000','spaceAfter' => 0,'spaceBefore' => 0);
        $cLineBottom      = array(                'valign' => 'center','borderBottomSize' => 15,'borderBottomColor'=>'#CB0B0B','spaceAfter' => 0,'spaceBefore' => 0);
        $cLineBottomS3    = array('gridSpan' => 3,'valign' => 'center','borderBottomSize' => 15,'borderBottomColor'=>'#CB0B0B','spaceAfter' => 0,'spaceBefore' => 0);
        $cLineBottomS2    = array('gridSpan' => 2,'valign' => 'center','borderBottomSize' => 15,'borderBottomColor'=>'#CB0B0B','spaceAfter' => 0,'spaceBefore' => 0);
        $cellColSquare1   = array('borderBottomSize' => 10,'borderBottomColor'=>'#000000', 'borderTopSize' => 10,'borderTopColor'=>'#000000', 'borderLeftSize' => 10,'borderLeftColor'=>'#000000', 'borderRightSize' => 10,'borderRightColor'=>'#000000','valign' => 'center','spaceAfter' => 0,'spaceBefore' => 0);
        $cellColSquare1Bt = array('borderBottomSize' => 10,'borderBottomColor'=>'#FFFFFF', 'borderTopSize' => 10,'borderTopColor'=>'#FFFFFF', 'borderLeftSize' => 10,'borderLeftColor'=>'#000000', 'borderRightSize' => 10,'borderRightColor'=>'#000000','valign' => 'center','spaceAfter' => 0,'spaceBefore' => 0);
        $cellColSquare1BT = array('borderBottomSize' => 10,'borderBottomColor'=>'#FFFFFF', 'borderTopSize' => 10,'borderTopColor'=>'#000000', 'borderLeftSize' => 10,'borderLeftColor'=>'#000000', 'borderRightSize' => 10,'borderRightColor'=>'#000000','valign' => 'center','spaceAfter' => 0,'spaceBefore' => 0);
        $cellColSquare1Wt = array('borderBottomSize' => 10,'borderBottomColor'=>'#FFFFFF', 'borderTopSize' => 10,'borderTopColor'=>'#FFFFFF', 'borderLeftSize' => 10,'borderLeftColor'=>'#FFFFFF', 'borderRightSize' => 10,'borderRightColor'=>'#FFFFFF','valign' => 'center','spaceAfter' => 0,'spaceBefore' => 0);
        $cellColSquareS2Wt= array('gridSpan' => 2,'borderBottomSize' => 10,'borderBottomColor'=>'#FFFFFF', 'borderTopSize' => 10,'borderTopColor'=>'#FFFFFF', 'borderLeftSize' => 10,'borderLeftColor'=>'#FFFFFF', 'borderRightSize' => 10,'borderRightColor'=>'#FFFFFF','valign' => 'center','spaceAfter' => 0,'spaceBefore' => 0);
        $cellLB  = array('valign' => 'center','spaceAfter' => 0,'spaceBefore' => 0,'borderBottomSize' => 15,'borderBottomColor'=>'#CB0B0B'); 
        $ra               = empty($datos['totArea'][0])?0:$datos['totArea'][0]['totTRA'];
    
        $section->addTextBreak(1);
        $section->addText($datos['APVI'][0]['AVITit1'],'ArialNarrowRedBold12','paraLeftT');        
        \PhpOffice\PhpWord\Shared\Html::addHtml($section,$datos['APVI'][0]['AVIText1']);
        $section->addTextBreak(1);
        $section->addText($datos['APVI'][0]['AVITit2'],'ArialNarrowRedBold12','paraLeftT');
        $section->addText($datos['APVI'][0]['AVIText2'],'ArialNarrowBlack12','paraJust');
        $section->addTextBreak(1);
        $section->addText($datos['APVI'][0]['AVITit3'],'ArialNarrowRedBold12','paraLeftT');
        $section->addText($datos['APVI'][0]['AVIText3'],'ArialNarrowBlack12','paraJust');
                
        $section->addTextBreak(2);
        $tblR = $section->addTable(array('cellMargin' => 0, 'cellMarginRight' => 0, 'cellMarginBottom' => 0, 'cellMarginLeft' => 0));                                       
        $tblR->addRow();
        $tblR->addCell(3000, $cellLB)->addText($param['ra'],'ArialNarrowBlackBold12','paragraphLeft'); 
        $tblR->addCell(3000, $cellLB)->addText(number_format($ra, 2, '.', ',').' m2','ArialNarrowBlackBold12','paragraphCenter'); 
        $tblR->addCell(3000, $cellLB)->addText(number_format($ra*$datos['dc']['ft'], 2, '.', ',').' ft2','ArialNarrowBlackBold12','paragraphCenter');         
        $tblR->addRow();
        $tblR->addCell(3000, $cellLB)->addText($param['ugmi'],'ArialNarrowBlackBold12','paragraphLeft'); 
        $tblR->addCell(3000, $cellLB)->addText('$ '.number_format($datos['iv']['indicated_value_crl'], 2, '.', ',').' usd/Month/m2','ArialNarrowBlackBold12','paragraphCenter'); 
        $tblR->addCell(3000, $cellLB)->addText('$ '.number_format(($datos['iv']['indicated_value_crl']/$datos['dc']['ft'] * 12), 2, '.', ',').' usd/ft2/year','ArialNarrowBlackBold12','paragraphCenter');         
        $tblR->addRow();
        $tblR->addCell(3000, $cellLB)->addText($param['ugai'],'ArialNarrowBlackBold12','paragraphLeft'); 
        $tblR->addCell(3000, $cellLB)->addText('$ '.number_format($datos['iv']['indicated_value_crl']*12, 2, '.', ',').' usd/year/m2','ArialNarrowBlackBold12','paragraphCenter'); 
        $tblR->addCell(3000, $cellLB)->addText('$ '.number_format(($datos['iv']['indicated_value_crl']/$datos['dc']['ft'] * 12), 2, '.', ',').' usd/ft2/year','ArialNarrowBlackBold12','paragraphCenter');
        
        $section->addPageBreak();
        $annualIncome = (($datos['iv']['indicated_value_crl']*12) * $ra);
        $serr  = $datos['APVI'][0]['AVIpgi_1_Year_mxn'] + $datos['APVI'][0]['AVIpgi_2_Year_mxn'] + $datos['APVI'][0]['AVIpgi_3_Year_mxn'] + $datos['APVI'][0]['AVIpgi_4_Year_mxn'];
        $tpgio = $serr + ($annualIncome * $datos['dc']['exchange_rate']);
        $mvcl  = ($datos['APVI'][0]['AVIpgi_mvcl_por'] /100) * $tpgio;
        $egi   = $tpgio - $mvcl;
        
        if(!empty($datos['APVI'][0]['AVIpgi_1_Txt']) | !empty($datos['APVI'][0]['AVIpgi_2_Txt']) | !empty($datos['APVI'][0]['AVIpgi_3_Txt']) | !empty($datos['APVI'][0]['AVIpgi_4_Txt']))
        {            
            $section->addText($datos['APVI'][0]['AVITit4'],'ArialNarrowRedBold12','paraLeftT');
            $section->addTextBreak(1);            
            $tblIE = $section->addTable(array('cellMargin' => 0, 'cellMarginRight' => 0, 'cellMarginBottom' => 0, 'cellMarginLeft' => 0));
            $tblIE->addRow();
            $tblIE->addCell(3000, $cellLB)->addText('','ArialNarrowBlackBold12','paraLeftT'); 
            $tblIE->addCell(3000, $cellLB)->addText($param['ra'].' (m2)','ArialNarrowBlackBold12','paraCenterT'); 
            $tblIE->addCell(3000, $cellLB)->addText($param['rent'],'ArialNarrowBlackBold12','paraCenterT'); 
            $tblIE->addCell(3000, $cellLB)->addText($param['anIn'],'ArialNarrowBlackBold12','paraCenterT');         
            $tblIE->addRow();
            $tblIE->addCell(3000, $cellLB)->addText($param['pgi'],'ArialNarrowBlackBold12','paraLeftT'); 
            $tblIE->addCell(3000, $cellLB)->addText(number_format($ra, 2, '.', ','),'ArialNarrowBlackBold12','paraCenterT'); 
            $tblIE->addCell(3000, $cellLB)->addText('$ '.number_format($datos['iv']['indicated_value_crl']*12, 2, '.', ',').' /m2/year','ArialNarrowBlackBold12','paraCenterT');
            $tblIE->addCell(3000, $cellLB)->addText('$ '.number_format($annualIncome, 2, '.', ','),'ArialNarrowBlackBold12','paraCenterT');         
            $tblIE->addRow(500);
            $tblIE->addCell(3000, $cNoLineBot)->addText($param['err'],'ArialNarrowBlackBold12','paraLeftT'); 
            $tblIE->addCell(3000, $cNoLineBot)->addText('','ArialNarrowBlackBold12','paraCenterT'); 
            $tblIE->addCell(3000, $cNoLineBot)->addText('','ArialNarrowBlackBold12','paraCenterT');
            $tblIE->addCell(3000, $cNoLineBot)->addText('','ArialNarrowBlackBold12','paraCenterT');
            
            if(!empty($datos['APVI'][0]['AVIpgi_1_Txt']))
            {
                $tblIE->addRow();
                $tblIE->addCell(6000, $cLineBotThinS2)->addText($datos['APVI'][0]['AVIpgi_1_Txt'],'ArialNarrowBlack12','paraLeftT');                 
                $tblIE->addCell(3000, $cLineBotThin  )->addText('$ '.number_format(empty($datos['APVI'][0]['AVIpgi_1_Mon_mxn'] )?0:$datos['APVI'][0]['AVIpgi_1_Mon_mxn'] /$datos['dc']['exchange_rate'], 2, '.', ','),'ArialNarrowBlack12','paraCenterT');
                $tblIE->addCell(3000, $cLineBotThin  )->addText('$ '.number_format(empty($datos['APVI'][0]['AVIpgi_1_Year_mxn'])?0:$datos['APVI'][0]['AVIpgi_1_Year_mxn']/$datos['dc']['exchange_rate'], 2, '.', ','),'ArialNarrowBlack12','paraCenterT');
            }
            if(!empty($datos['APVI'][0]['AVIpgi_2_Txt']))
            {
                $tblIE->addRow();
                $tblIE->addCell(6000, $cLineBotThinS2)->addText($datos['APVI'][0]['AVIpgi_2_Txt'],'ArialNarrowBlack12','paraLeftT');                 
                $tblIE->addCell(3000, $cLineBotThin  )->addText('$ '.number_format(empty($datos['APVI'][0]['AVIpgi_1_Mon_mxn'] )?0:$datos['APVI'][0]['AVIpgi_2_Mon_mxn'] /$datos['dc']['exchange_rate'], 2, '.', ','),'ArialNarrowBlack12','paraCenterT');
                $tblIE->addCell(3000, $cLineBotThin  )->addText('$ '.number_format(empty($datos['APVI'][0]['AVIpgi_1_Year_mxn'])?0:$datos['APVI'][0]['AVIpgi_2_Year_mxn']/$datos['dc']['exchange_rate'], 2, '.', ','),'ArialNarrowBlack12','paraCenterT');
            }
            if(!empty($datos['APVI'][0]['AVIpgi_3_Txt']))
            {
                $tblIE->addRow();
                $tblIE->addCell(6000, $cLineBotThinS2)->addText($datos['APVI'][0]['AVIpgi_3_Txt'],'ArialNarrowBlack12','paraLeftT');                 
                $tblIE->addCell(3000, $cLineBotThin  )->addText('$ '.number_format(empty($datos['APVI'][0]['AVIpgi_1_Mon_mxn'] )?0:$datos['APVI'][0]['AVIpgi_3_Mon_mxn'] /$datos['dc']['exchange_rate'], 2, '.', ','),'ArialNarrowBlack12','paraCenterT');
                $tblIE->addCell(3000, $cLineBotThin  )->addText('$ '.number_format(empty($datos['APVI'][0]['AVIpgi_1_Year_mxn'])?0:$datos['APVI'][0]['AVIpgi_3_Year_mxn']/$datos['dc']['exchange_rate'], 2, '.', ','),'ArialNarrowBlack12','paraCenterT');
            }
            if(!empty($datos['APVI'][0]['AVIpgi_4_Txt']))
            {
                $tblIE->addRow();
                $tblIE->addCell(6000, $cLineBotThinS2)->addText($datos['APVI'][0]['AVIpgi_4_Txt'],'ArialNarrowBlack12','paraLeftT');                 
                $tblIE->addCell(3000, $cLineBotThin  )->addText('$ '.number_format(empty($datos['APVI'][0]['AVIpgi_1_Mon_mxn'] )?0:$datos['APVI'][0]['AVIpgi_4_Mon_mxn'] /$datos['dc']['exchange_rate'], 2, '.', ','),'ArialNarrowBlack12','paraCenterT');
                $tblIE->addCell(3000, $cLineBotThin  )->addText('$ '.number_format(empty($datos['APVI'][0]['AVIpgi_1_Year_mxn'])?0:$datos['APVI'][0]['AVIpgi_4_Year_mxn']/$datos['dc']['exchange_rate'], 2, '.', ','),'ArialNarrowBlack12','paraCenterT');
            }            
            
            $tblIE->addRow();
            $tblIE->addCell(9000, $cLineBotThinS3)->addText($param['serr'],'ArialNarrowBlack12','paraLeftT');                             
            $tblIE->addCell(3000, $cLineBotThin)->addText('$ '.number_format(empty($serr)?0:$serr/$datos['dc']['exchange_rate'], 2, '.', ','),'ArialNarrowBlack12','paraCenterT');
            $tblIE->addRow();
            $tblIE->addCell(9000, $cLineBotThinS3)->addText($param['tpgio'],'ArialNarrowBlack12','paraLeftT');                             
            $tblIE->addCell(3000, $cLineBotThin)->addText('$ '.number_format(empty($tpgio)?0:$tpgio/$datos['dc']['exchange_rate'], 2, '.', ','),'ArialNarrowBlack12','paraCenterT');
            $tblIE->addRow();
            $tblIE->addCell(6000, $cLineBotThinS2)->addText($param['mvcl'],'ArialNarrowBlack12','paraLeftT');
            $tblIE->addCell(3000, $cLineBotThin  )->addText(number_format(empty($datos['APVI'][0]['AVIpgi_mvcl_por'])?0:$datos['APVI'][0]['AVIpgi_mvcl_por'], 2, '.', ',').' %','ArialNarrowBlack12','paraCenterT');
            $tblIE->addCell(3000, $cLineBotThin  )->addText('$ '.number_format(empty($mvcl)?0:$mvcl/$datos['dc']['exchange_rate'], 2, '.', ','),'ArialNarrowBlack12','paraCenterT');
            $tblIE->addRow();
            $tblIE->addCell(9000, $cLineBottom )->addText('','ArialNarrowBlackBold12','paraLeftT');                             
            $tblIE->addCell(3000, $cLineBottomS3)->addText('','ArialNarrowBlackBold12','paraLeftT');
            $tblIE->addRow();
            $tblIE->addCell(6000, $cLineBottomS2)->addText($param['egi'],'ArialNarrowBlackBold12','paraLeftT');
            $tblIE->addCell(3000, $cLineBottom  )->addText('','ArialNarrowBlackBold12','paraLeftT');
            $tblIE->addCell(3000, $cLineBottom  )->addText('$ '.number_format(empty($egi)?0:$egi/$datos['dc']['exchange_rate'], 2, '.', ','),'ArialNarrowBlackBold12','paraCenterT');
        }
        
        $section->addTextBreak(2);
        $eo1   = $datos['APVI'][0]['AVIoe_1_num2'];
        $eo2   = $datos['APVI'][0]['AVIoe_2_num1'] ;
        $eo3   = $datos['APVI'][0]['AVIoe_3_num1'] ;
        $eo4   = $datos['APVI'][0]['AVIoe_4_num1'] ;
        $eo5   = $datos['APVI'][0]['AVIoe_5_num1'] ;
        $eo6   = $datos['APVI'][0]['AVIoe_6_num1'] ;
        $eo7   = $datos['APVI'][0]['AVIoe_7_num1'] ;
        $eo8   = $datos['APVI'][0]['AVIoe_8_num1'] ;
        $eo9   = $datos['APVI'][0]['AVIoe_9_num1'] ;
        $eo10  = $datos['APVI'][0]['AVIoe_10_num2'];
        $eo11  = $datos['APVI'][0]['AVIoe_11_num2'];
        $egi   = empty($egi)?1:$egi;        
        $sePor = ($eo1/$egi) + $eo2 + $eo3 + $eo4 + $eo5 + $eo6 + $eo7 + $eo8 + $eo9 + ($eo10/$egi) +($eo11/$egi);
        $seNum = $eo1 + $datos['APVI'][0]['AVIoe_2_num2'] + $datos['APVI'][0]['AVIoe_3_num2'] + $datos['APVI'][0]['AVIoe_4_num2'] + $datos['APVI'][0]['AVIoe_5_num2'] + $datos['APVI'][0]['AVIoe_6_num2'] + $datos['APVI'][0]['AVIoe_7_num2'] + $datos['APVI'][0]['AVIoe_8_num2'] + $datos['APVI'][0]['AVIoe_9_num2'] + $eo10 + $eo11;
        $unf   = ($datos['APVI'][0]['AVIoe_unf']/100) * $seNum;

        $totExpPor = $sePor + ( ($unf/$egi)*100 );
        $totExpNum = $seNum + $unf;
        $noi       = $egi - $totExpNum;        
        
        if(!empty($datos['APVI'][0]['AVIoe_1_Txt']) | !empty($datos['APVI'][0]['AVIoe_2_Txt']) | !empty($datos['APVI'][0]['AVIoe_3_Txt']) | !empty($datos['APVI'][0]['AVIoe_4_Txt']) | !empty($datos['APVI'][0]['AVIoe_5_Txt']) | !empty($datos['APVI'][0]['AVIoe_6_Txt']) | !empty($datos['APVI'][0]['AVIoe_7_Txt']) | !empty($datos['APVI'][0]['AVIoe_8_Txt']) | !empty($datos['APVI'][0]['AVIoe_9_Txt']) | !empty($datos['APVI'][0]['AVIoe_10_Txt']) | !empty($datos['APVI'][0]['AVIoe_11_Txt']) )
        {
            $section->addText($param['oe'],'ArialNarrowRedBold12','paraLeftT');
            $section->addTextBreak(1);
            $tblOE = $section->addTable(array('cellMargin' => 0, 'cellMarginRight' => 0, 'cellMarginBottom' => 0, 'cellMarginLeft' => 0));

            if($datos['APVI'][0]['AVIoe_1_num1']!=0)
                { $this->columnaOE($tblOE,$cLineBotThin,$datos['APVI'][0]['AVIoe_1_Txt'], $datos['APVI'][0]['AVIoe_1_num1'], $eo1, $datos['dc']['exchange_rate']); }
            if($eo2!=0)
                { $this->columnaOE($tblOE,$cLineBotThin,$datos['APVI'][0]['AVIoe_2_Txt'], $eo2, $datos['APVI'][0]['AVIoe_2_num2'], $datos['dc']['exchange_rate']); }
            if($eo3!=0)
                { $this->columnaOE($tblOE,$cLineBotThin,$datos['APVI'][0]['AVIoe_3_Txt'], $eo3, $datos['APVI'][0]['AVIoe_3_num2'], $datos['dc']['exchange_rate']); }
            if($eo4!=0)
                { $this->columnaOE($tblOE,$cLineBotThin,$datos['APVI'][0]['AVIoe_4_Txt'], $eo4, $datos['APVI'][0]['AVIoe_4_num2'], $datos['dc']['exchange_rate']); }
            if($eo5!=0)
                { $this->columnaOE($tblOE,$cLineBotThin,$datos['APVI'][0]['AVIoe_5_Txt'], $eo5, $datos['APVI'][0]['AVIoe_5_num2'], $datos['dc']['exchange_rate']); }
            if($eo6!=0)
                { $this->columnaOE($tblOE,$cLineBotThin,$datos['APVI'][0]['AVIoe_6_Txt'], $eo6, $datos['APVI'][0]['AVIoe_6_num2'], $datos['dc']['exchange_rate']); }
            if($eo7!=0)
                { $this->columnaOE($tblOE,$cLineBotThin,$datos['APVI'][0]['AVIoe_7_Txt'], $eo7, $datos['APVI'][0]['AVIoe_7_num2'], $datos['dc']['exchange_rate']); }
            if($eo8!=0)
                { $this->columnaOE($tblOE,$cLineBotThin,$datos['APVI'][0]['AVIoe_8_Txt'], $eo8, $datos['APVI'][0]['AVIoe_8_num2'], $datos['dc']['exchange_rate']); }
            if($eo9!=0)
                { $this->columnaOE($tblOE,$cLineBotThin,$datos['APVI'][0]['AVIoe_9_Txt'], $eo9, $datos['APVI'][0]['AVIoe_9_num2'], $datos['dc']['exchange_rate']); }
            if($datos['APVI'][0]['AVIoe_10_num1']!=0)
                { $this->columnaOE($tblOE,$cLineBotThin,$datos['APVI'][0]['AVIoe_10_Txt'], $datos['APVI'][0]['AVIoe_10_num1'], $eo10, $datos['dc']['exchange_rate']); }
            if($datos['APVI'][0]['AVIoe_11_num1']!=0)
                { $this->columnaOE($tblOE,$cLineBotThin,$datos['APVI'][0]['AVIoe_11_Txt'], $datos['APVI'][0]['AVIoe_11_num1'], $eo11, $datos['dc']['exchange_rate']); }
            
            $this->columnaOE($tblOE,$cLineBotThin,$param['se']    , $sePor                        , $seNum, $datos['dc']['exchange_rate']);
            $this->columnaOE($tblOE,$cLineBotThin,$param['unf']   , $datos['APVI'][0]['AVIoe_unf'], $unf       , $datos['dc']['exchange_rate']);
            $this->columnaOE($tblOE,$cLineBotThin,$param['totExp'], $totExpPor                    , $totExpNum , $datos['dc']['exchange_rate']);
            $tblOE->addRow();
            $tblOE->addCell(9000, $cLineBottom  )->addText('','ArialNarrowBlackBold12','paraLeftT');                             
            $tblOE->addCell(3000, $cLineBottomS2)->addText('','ArialNarrowBlackBold12','paraLeftT');
            $tblOE->addRow();
            $tblOE->addCell(4000, $cLineBottom)->addText($param['noi'],'ArialNarrowBlackBold12','paraLeftT');
            $tblOE->addCell(4000, $cLineBottom)->addText('','ArialNarrowBlackBold12','paragraphLeft');                             
            $tblOE->addCell(4000, $cLineBottom)->addText('$ '.number_format(empty($noi)?0:$noi/$datos['dc']['exchange_rate'], 2, '.', ',').' usd','ArialNarrowBlackBold12','paraCenterT');
            $section->addTextBreak(1);
        }
        
        $section->addText($datos['APVI'][0]['AVITit5'],'ArialNarrowRedBold12','paraLeftT');
        $section->addTextBreak(1);
        $section->addText($datos['APVI'][0]['AVIText5_1'],'ArialNarrowBlack12','paraJust');
        
        $section->addPageBreak();
        $section->addText($datos['APVI'][0]['AVIText5_2'],'ArialNarrowRedBold12','paraLeftT');
        $section->addTextBreak(1);
        $section->addText($datos['APVI'][0]['AVIText5_3'],'ArialNarrowBlack12','paraJust');
        $section->addTextBreak(1);
        
        $values = array($datos['APVI'][0]['AVIsource_1_ran1'],$datos['APVI'][0]['AVIsource_1_ran2']);
        $avg1   = array_sum($values) / count($values);
        $values = array($datos['APVI'][0]['AVIsource_2_ran1'],$datos['APVI'][0]['AVIsource_2_ran2']);
        $avg2   = array_sum($values) / count($values);
        $values = array($datos['APVI'][0]['AVIsource_3_ran1'],$datos['APVI'][0]['AVIsource_3_ran2']);
        $avg3   = array_sum($values) / count($values);
        $values = array($datos['APVI'][0]['AVIsource_4_ran1'],$datos['APVI'][0]['AVIsource_4_ran2']);
        $avg4   = array_sum($values) / count($values);
        
        $valuesTot = array();
        $valuesTot = $this->capitalizationRateNumAverage($avg1,$valuesTot); 
        $valuesTot = $this->capitalizationRateNumAverage($avg2,$valuesTot); 
        $valuesTot = $this->capitalizationRateNumAverage($avg3,$valuesTot); 
        $valuesTot = $this->capitalizationRateNumAverage($avg4,$valuesTot);  


        //$avgTot    = array_sum($valuesTot) / count($valuesTot);

        if(count($valuesTot)>0){
              $avgTot = array_sum($valuesTot) / count($valuesTot);
        }else{
              $avgTot = 0;
        }
        
        $tblPFD = $section->addTable(array('cellMargin' => 0, 'cellMarginRight' => 0, 'cellMarginBottom' => 0, 'cellMarginLeft' => 0));        
        $tblStyle         = array('vMerge'   =>'restart','valign' => 'center','borderBottomSize' => 10,'borderBottomColor'=>'#000000', 'borderTopSize' => 10,'borderTopColor'=>'#000000', 'borderLeftSize' => 10,'borderLeftColor'=>'#000000', 'borderRightSize' => 10,'borderRightColor'=>'#000000');
        $cellColSquareSp2 = array('gridSpan' => 2,'borderBottomSize' => 10,'borderBottomColor'=>'#000000', 'borderTopSize' => 10,'borderTopColor'=>'#000000', 'borderLeftSize' => 10,'borderLeftColor'=>'#000000', 'borderRightSize' => 10,'borderRightColor'=>'#000000','valign' => 'center');
        $cellColSquareSp3 = array('gridSpan' => 3,'borderBottomSize' => 10,'borderBottomColor'=>'#000000', 'borderTopSize' => 10,'borderTopColor'=>'#000000', 'borderLeftSize' => 10,'borderLeftColor'=>'#000000', 'borderRightSize' => 10,'borderRightColor'=>'#000000','valign' => 'center');
        $cellRowContinue  = array('vMerge'   => 'continue','borderBottomSize' => 10,'borderBottomColor'=>'#000000', 'borderTopSize' => 10,'borderTopColor'=>'#000000', 'borderLeftSize' => 10,'borderLeftColor'=>'#000000', 'borderRightSize' => 10,'borderRightColor'=>'#000000');
        
        $tblPFD->addRow();
        $tblPFD->addCell(6000, $tblStyle        )->addText($param['source'],'ArialNarrowBlack12','paraCenterT');
        $tblPFD->addCell(6000, $cellColSquareSp3)->addText($param['cr'],'ArialNarrowBlack12','paraCenterT');        
        $tblPFD->addRow();
        $tblPFD->addCell(null, $cellRowContinue);
        $tblPFD->addCell(3000, $cellColSquareSp2)->addText($param['ran'],'ArialNarrowBlack12','paraCenterT');
        $tblPFD->addCell(3000, $cellColSquare1  )->addText($param['av'],'ArialNarrowBlack12','paraCenterT');
        $this->columnaPFD($tblPFD,$cellColSquare1,$datos['APVI'][0]['AVIsource_1_Txt'], $datos['APVI'][0]['AVIsource_1_ran1'], $datos['APVI'][0]['AVIsource_1_ran2'],$avg1);
        $this->columnaPFD($tblPFD,$cellColSquare1,$datos['APVI'][0]['AVIsource_2_Txt'], $datos['APVI'][0]['AVIsource_2_ran1'], $datos['APVI'][0]['AVIsource_2_ran2'],$avg2);
        $this->columnaPFD($tblPFD,$cellColSquare1,$datos['APVI'][0]['AVIsource_3_Txt'], $datos['APVI'][0]['AVIsource_3_ran1'], $datos['APVI'][0]['AVIsource_3_ran2'],$avg3);
        $this->columnaPFD($tblPFD,$cellColSquare1,$datos['APVI'][0]['AVIsource_4_Txt'], $datos['APVI'][0]['AVIsource_4_ran1'], $datos['APVI'][0]['AVIsource_4_ran2'],$avg4);
        $tblPFD->addRow();
        $tblPFD->addCell(3000, $cellColSquare1Wt)->addText('','ArialNarrowBlack12','paraLeftT');
        $tblPFD->addCell(3000, $cellColSquare1Wt)->addText('','ArialNarrowBlack12','paraLeftT');
        $tblPFD->addCell(3000, $cellColSquare1Wt)->addText('','ArialNarrowBlack12','paraCenterT');
        $tblPFD->addCell(3000, $cellColSquare1Wt)->addText('','ArialNarrowBlack12','paraCenterT');
        $tblPFD->addRow();
        $tblPFD->addCell(3000, $cellColSquare1Wt)->addText('','ArialNarrowBlack12','paraLeftT');
        $tblPFD->addCell(3000, $cellColSquare1  )->addText($param['av'],'ArialNarrowBlackBold12','paraCenterT');
        $tblPFD->addCell(3000, $cellColSquare1  )->addText('Ro','ArialNarrowBlackBold12','paraCenterT');
        $tblPFD->addCell(3000, $cellColSquare1  )->addText(number_format($avgTot, 2, '.', ',').' %','ArialNarrowBlackBold12','paraCenterT');
        
        $section->addTextBreak(2);
        $section->addText($datos['APVI'][0]['AVIText5_4'],'ArialNarrowRedBold12','paraLeftT');
        $section->addTextBreak(1);
        $section->addText($datos['APVI'][0]['AVIText5_5'],'ArialNarrowBlack12','paraJust');
        $section->addTextBreak(1);
        
        $AVImir   = empty($datos['APVI'][0]['AVImir'])?0:$datos['APVI'][0]['AVImir'];
        $AVImt    = $datos['APVI'][0]['AVImt'];
        $AVIlvr   = $datos['APVI'][0]['AVIlvr']/100;
        $AVIedr   = $datos['APVI'][0]['AVIedr'];
        $AVIicr   = $datos['APVI'][0]['AVIicr']*100;
        $AVImorCo = $datos['APVI'][0]['AVImorCo']*100;
        $morCoTot = $AVIlvr * $AVImorCo;
        $edr      = 1 - $AVIlvr;
        $edrTot   = $AVIedr * $edr;
        
        $tblBI = $section->addTable(array('cellMargin' => 0, 'cellMarginRight' => 0, 'cellMarginBottom' => 0, 'cellMarginLeft' => 0));        
        $this->columnaBI($tblBI,$cellColSquare1BT,$param['mir']  , $AVImir, '%','','','','',FALSE);
        $this->columnaBI($tblBI,$cellColSquare1Bt,$param['mt']   , $AVImt, '','','','','',FALSE);
        $this->columnaBI($tblBI,$cellColSquare1Bt,$param['lvr']  , $datos['APVI'][0]['AVIlvr'], '%','','','','',FALSE);
        $this->columnaBI($tblBI,$cellColSquare1Bt,$param['morCo'], $AVImorCo/100, '%','','','','',FALSE);
        $this->columnaBI($tblBI,$cellColSquare1Bt,$param['edr']  , $AVIedr, '%','','','','',FALSE);
        $this->columnaBI($tblBI,$cellColSquare1Bt,$param['morRe'], $datos['APVI'][0]['AVIlvr'], '%',' X ',$AVImorCo/100,' = ',$morCoTot/100,FALSE);
        $this->columnaBI($tblBI,$cellColSquare1Bt,$param['eqRe'] , $edr*100, '%',' X ',$AVIedr,' = ',$edrTot,FALSE);
        $this->columnaBI($tblBI,$cellColSquare1  ,$param['icr'] , '', '','','','= ',$AVIicr/100,TRUE);
        $section->addTextBreak(3);
        $section->addText($datos['APVI'][0]['AVIText5_6'],'ArialNarrowRedBold12','paraLeftT');
        $section->addTextBreak(1);
        $section->addText($datos['APVI'][0]['AVIText5_7'],'ArialNarrowBlack12','paraJust');
        
        $AVIdcrTit = $datos['APVI'][0]['AVIdcrTit'];
        $dcr       = $AVIdcrTit * $AVIlvr * $AVImorCo/100;
        
        $tblC = $section->addTable(array('cellMargin' => 0, 'cellMarginRight' => 0, 'cellMarginBottom' => 0, 'cellMarginLeft' => 0));
        $this->columnaTblC($tblC,$cellColSquare1Wt,TRUE ,$param['roFor'], null,null, 'paraLeftT',FALSE);
        $this->columnaTblC($tblC,$cellColSquare1Wt,TRUE ,$param['whereDCR'], null,null, 'paraLeftT',FALSE);
        $this->columnaTblC($tblC,$cellColSquare1Wt,TRUE ,'      '.$param['rmLbl'], null,null, 'paraLeftT',FALSE);
        $this->columnaTblC($tblC,$cellColSquare1Wt,TRUE ,'      '.$param['mLbl'], null,null, 'paraLeftT',FALSE);
        $this->columnaTblC($tblC,$cellColSquare1Wt,TRUE ,'', null,null, 'paraLeftT',FALSE);
        $this->columnaTblC($tblC,$cellColSquare1Wt,FALSE,'      '.$param['dcrTit'], $AVIdcrTit,'', 'paraLeftT',FALSE);
        $this->columnaTblC($tblC,$cellColSquare1Wt,TRUE ,'      '.$param['dcrLbl'], null,null, 'paraLeftT',FALSE);
        $this->columnaTblC($tblC,$cellColSquare1Wt,FALSE,'      '.$param['ir'], $AVImir,' %', 'paraRightT',FALSE);
        $this->columnaTblC($tblC,$cellColSquare1Wt,FALSE,'      '.$param['amoPe'], $AVImt,' years', 'paraRightT',FALSE);
        $this->columnaTblC($tblC,$cellColSquare1Wt,FALSE,'      '.$param['lvLbl'], $datos['APVI'][0]['AVIlvr'],' %', 'paraRightT',FALSE);
        $this->columnaTblC($tblC,$cellColSquare1Wt,FALSE,'      '.$param['mcLbl'], $AVImorCo/100,' %', 'paraRightT',FALSE);
        $this->columnaTblC($tblC,$cellColSquare1Wt,TRUE ,'', null,null, 'paraLeftT',FALSE);
        $this->columnaTblC($tblC,$cellColSquare1Wt,FALSE,'Ro = ', $dcr,' %', 'paraRightT',TRUE);
        
        $AVIcapR_por1 = $datos['APVI'][0]['AVIcapR_por1']/100;
        $AVIcapR_por2 = $datos['APVI'][0]['AVIcapR_por2']/100;
        $AVIcapR_por3 = $datos['APVI'][0]['AVIcapR_por3']/100;
        $icr = $AVIicr/100;
        $capitalizationRate = ($AVIcapR_por1 * $avgTot) + ($AVIcapR_por2 * $icr) + ($AVIcapR_por3 * $dcr);        
        $section->addTextBreak(3);
        $section->addText($datos['APVI'][0]['AVITit6']   ,'ArialNarrowRedBold12','paraLeftT');        
        $section->addTextBreak(1);
        $section->addText($datos['APVI'][0]['AVIText6_1'],'ArialNarrowBlack12','paraJust');
        $section->addTextBreak(1);
        $tblResumenCR = $section->addTable(array('cellMargin' => 0, 'cellMarginRight' => 0, 'cellMarginBottom' => 0, 'cellMarginLeft' => 0));
        $tblResumenCR->addRow();
        $tblResumenCR->addCell(6000, $cNoLineBot)->addText($datos['APVI'][0]['AVIText5_2'],'ArialNarrowBlack12','paraLeftT'); 
        $tblResumenCR->addCell(3000, $cNoLineBot)->addText(number_format($avgTot, 2, '.', ',').' %','ArialNarrowBlack12','paraCenterT'); 
        $tblResumenCR->addCell(3000, $cNoLineBot)->addText(number_format(empty($datos['APVI'][0]['AVIcapR_por1'])?0:$datos['APVI'][0]['AVIcapR_por1'], 2, '.', ',').' %','ArialNarrowBlack12','paraCenterT'); 
        $tblResumenCR->addRow();
        $tblResumenCR->addCell(6000, $cNoLineBot)->addText($datos['APVI'][0]['AVIText5_4'],'ArialNarrowBlack12','paraLeftT'); 
        $tblResumenCR->addCell(3000, $cNoLineBot)->addText(number_format($icr, 2, '.', ',').' %','ArialNarrowBlack12','paraCenterT'); 
        $tblResumenCR->addCell(3000, $cNoLineBot)->addText(number_format(empty($datos['APVI'][0]['AVIcapR_por2'])?0:$datos['APVI'][0]['AVIcapR_por2'], 2, '.', ',').' %','ArialNarrowBlack12','paraCenterT'); 
        $tblResumenCR->addRow();
        $tblResumenCR->addCell(6000, $cNoLineBot)->addText($datos['APVI'][0]['AVIText5_6'],'ArialNarrowBlack12','paraLeftT'); 
        $tblResumenCR->addCell(3000, $cNoLineBot)->addText(number_format($dcr, 2, '.', ',').' %','ArialNarrowBlack12','paraCenterT'); 
        $tblResumenCR->addCell(3000, $cNoLineBot)->addText(number_format(empty($datos['APVI'][0]['AVIcapR_por3'])?0:$datos['APVI'][0]['AVIcapR_por3'], 2, '.', ',').' %','ArialNarrowBlack12','paraCenterT'); 
        $tblResumenCR->addRow(750);
        $tblResumenCR->addCell(6000, $cellLB)->addText("Ro = ",'ArialNarrowBlackBold12','paraRightT'); 
        $tblResumenCR->addCell(3000, $cellLB)->addText(number_format(empty($capitalizationRate)?0:$capitalizationRate, 2, '.', ',').' %','ArialNarrowBlackBold12','paraCenterT'); 
        $tblResumenCR->addCell(3000, $cellLB)->addText('','ArialNarrowBlackBold12','paraCenterT'); 
        $section->addTextBreak(3);        
        $section->addText($datos['APVI'][0]['AVIText6_2'],'ArialNarrowBlack12','paraJust');
        $section->addTextBreak(1);
        $ivudica = empty($capitalizationRate)?0:(($noi/$datos['dc']['exchange_rate']) / ($capitalizationRate/100));        
        $tblCR = $section->addTable(array('cellMargin' => 0, 'cellMarginRight' => 0, 'cellMarginBottom' => 0, 'cellMarginLeft' => 0));
        $tblCR->addRow();
        $tblCR->addCell(4000, $cLineBottom)->addText($param['ica'],'ArialNarrowBlackBold12','paraCenterT');
        $tblCR->addCell(4000, $cLineBottom)->addText($param['noi'],'ArialNarrowBlackBold12','paraCenterT');
        $tblCR->addCell(4000, $cLineBottom)->addText($param['cr'],'ArialNarrowBlackBold12','paraCenterT');
        $tblCR->addRow(750);
        $tblCR->addCell(4000, $cNoLineBot)->addText('','ArialNarrowBlackBold12','paraCenterT');
        $tblCR->addCell(4000, $cNoLineBot)->addText('$ '.number_format($noi/$datos['dc']['exchange_rate'], 2, '.', ',').' '.$param['currency'],'ArialNarrowBlackBold12','paraCenterT');
        $tblCR->addCell(4000, $cNoLineBot)->addText(number_format($capitalizationRate, 2, '.', ',').' %','ArialNarrowBlackBold12','paraCenterT');
        $tblCR->addRow(750);
        $tblCR->addCell(4000, $cNoLineBotS2)->addText($param['ivudica'],'ArialNarrowBlack12','paraCenterT');
        $tblCR->addCell(4000, $cNoLineBot  )->addText('$ '.number_format($ivudica, 2, '.', ',').' '.$param['currency'],'ArialNarrowBlack12','paraCenterT');
        $tblCR->addRow(750);
        $tblCR->addCell(4000, $cLineBottomS2)->addText($param['roun'],'ArialNarrowBlackBold12','paraCenterT');
        $tblCR->addCell(4000, $cLineBottom  )->addText('$ '.number_format($this->redondearHermes($ivudica), 2, '.', ',').' '.$param['currency'],'ArialNarrowBlackBold12','paraCenterT');        
        
        $section->addPageBreak();
        $section->addText($datos['APVI'][0]['AVITit7']   ,'ArialNarrowRedBold12','paraLeftT');        
        $section->addTextBreak(1);
        $section->addText($datos['APVI'][0]['AVIText7'],'ArialNarrowBlack12','paraJust');
        $section->addTextBreak(1);
        $plans        = empty($datos['land'])?0:$datos['land'][0]['plans'];
        $plansFT      = empty($datos['land'])?0:$datos['land'][0]['plans']*$datos['dc']['ft'];
        $far          = $datos['APVI'][0]['AVIfar'];
        $mf           = $datos['APVI'][0]['AVImf'];
        $fArea        = $datos['APVI'][0]['AVIfArea'];
        $tls          = $far * $fArea;
        $exl          = ($plans-$tls)>0?($plans-$tls):0;
        $uvel         = $datos['iv']['indicated_value_cls'] * $mf;
        $vel          = $uvel * $exl;
        $AVIivudicael = $ivudica + $vel;
        
        $tblEL = $section->addTable(array('cellMargin' => 0, 'cellMarginRight' => 0, 'cellMarginBottom' => 0, 'cellMarginLeft' => 0));
        $tblEL->addRow();
        $tblEL->addCell(4000, $cellColSquare1Wt )->addText($param['far'],'ArialNarrowBlack12','paraLeftT');
        $tblEL->addCell(8000, $cellColSquareS2Wt)->addText(number_format(empty($datos['APVI'][0]['AVIfar'])?0:$datos['APVI'][0]['AVIfar'], 2, '.', ','),'ArialNarrowBlack12','paraLeftT');
        $tblEL->addRow();
        $tblEL->addCell(4000, $cellColSquare1Wt )->addText($param['mf'],'ArialNarrowBlack12','paraLeftT');
        $tblEL->addCell(8000, $cellColSquareS2Wt)->addText(number_format(empty($datos['APVI'][0]['AVImf'])?0:$datos['APVI'][0]['AVImf'], 2, '.', ','),'ArialNarrowBlack12','paraLeftT');
        $tblEL->addRow();
        $tblEL->addCell(4000, $cellColSquare1Wt )->addText('','ArialNarrowBlack12','paraLeftT');
        $tblEL->addCell(4000, $cellColSquare1Wt )->addText('sqm','ArialNarrowBlackBold12','paraCenterT');
        $tblEL->addCell(4000, $cellColSquare1Wt )->addText('sqf','ArialNarrowBlackBold12','paraCenterT');
        $this->columnaTblEL($tblEL,$cellColSquare1Wt,$param['ls']   , $plans, $plansFT,'','','');
        $this->columnaTblEL($tblEL,$cellColSquare1Wt,$param['fArea'], $fArea, $fArea*$datos['dc']['ft'],'','','');
        $this->columnaTblEL($tblEL,$cellColSquare1Wt,$param['tls']  , $tls  , $tls*$datos['dc']['ft'],'','','');
        $this->columnaTblEL($tblEL,$cellColSquare1Wt,$param['exl']  , $exl  , $exl*$datos['dc']['ft'],'','','');
        $this->columnaTblEL($tblEL,$cellColSquare1Wt,$param['uvel'] , $uvel , $uvel*$datos['dc']['ft'],'$ ',' /sqm',' /sqf');
        $this->columnaTblEL($tblEL,$cellColSquare1Wt,$param['vel']  , $vel  , $vel*$datos['dc']['ft'],'$ ','','');
        $tblEL->addRow(1000);
        $tblEL->addCell(8000, $cellColSquareS2Wt)->addText($param['ivudica'].' '.$param['ivudicael'],'ArialNarrowBlack12','paraLeftT');
        $tblEL->addCell(4000, $cellColSquare1Wt )->addText('$ '.number_format(empty($AVIivudicael)?0:$AVIivudicael, 2, '.', ',').' '.$param['currency'],'ArialNarrowBlack12','paraCenterT');
        $tblEL->addRow(750);
        $tblEL->addCell(8000, $cLineBottomS2)->addText($param['roun'],'ArialNarrowBlackBold12','paraLeftT');
        $tblEL->addCell(4000, $cLineBottom  )->addText('$ '.number_format(empty($AVIivudicael)?0:$this->redondearHermes($AVIivudicael), 2, '.', ',').' '.$param['currency'],'ArialNarrowBlackBold12','paraCenterT');
        
        } catch (Exception $e) {echo 'creaCoreAPVIWORD Excepción: ',  $e, "\n";}    
    }
    
    
    private function columnAPXTbl($tblResumen, $estilo, $tit1, $num1, $num2, $num3, $currency, $num4, $num5, $currency2)
    {
        try{
            
            $tblResumen->addRow();                   
            $tblResumen->addCell(4000, $estilo)->addText($tit1,'ArialNarrowBlack12','paragraphLeft');             
            $cell    = $tblResumen->addCell(4000, $estilo);                 
            $textrun = $cell->addTextRun(array('align' => 'right','spaceBefore' => 0, 'spaceAfter' => 0));
            $textrun->addText('$ '.number_format($num1, 2, '.', ',').' '.$currency,'ArialNarrowBlack12','paragraphRight');
            $textrun->addTextBreak();
            $textrun->addText('$ '.number_format($num2, 2, '.', ',').' '.$currency.' / m2','ArialNarrowBlack12','paragraphRight');
            $textrun->addTextBreak();
            $textrun->addText('$ '.number_format($num3, 2, '.', ',').' '.$currency.' / ft2','ArialNarrowBlack12','paragraphRight');
            
            $cell2    = $tblResumen->addCell(4000, $estilo);                 
            $textrun2 = $cell2->addTextRun(array('align' => 'right','spaceBefore' => 0, 'spaceAfter' => 0));
            $textrun2->addText('$ '.number_format($num4, 2, '.', ',').' '.$currency2,'ArialNarrowBlack12','paragraphRight');
            $textrun2->addTextBreak();
            $textrun2->addText('$ '.number_format($num5, 2, '.', ',').' '.$currency2.' / m2','ArialNarrowBlack12','paragraphRight');            
            $textrun->addTextBreak();
            $textrun->addText('','ArialNarrowBlack12','paragraphRight');
            
        } catch (Exception $e) {echo 'columnAPXTbl Excepción: ',  $e, "\n";}        
    }


    private function columnaTblEL($tblEL,$estilo,$texto, $num1, $num2,$signo1,$signo2,$signo3)
        {
        try{    $tipoLetra = 'ArialNarrowBlack12';
                $tblEL->addRow();
                $tblEL->addCell(4000, $estilo)->addText($texto,$tipoLetra,'paraLeftT');
                $tblEL->addCell(4000, $estilo)->addText($signo1.number_format(empty($num1)?0:$num1    , 2, '.', ',').$signo2,$tipoLetra,'paraCenterT');
                $tblEL->addCell(4000, $estilo)->addText($signo1.number_format(empty($num2)?0:$num2    , 2, '.', ',').$signo3,$tipoLetra,'paraCenterT');
                
        } catch (Exception $e) {echo 'columnaOEWORD Excepción: ',  $e, "\n";}       
    }
    
    private function columnaTblC($tblC,$estilo,$twoColumns,$texto, $num1, $signo, $estiloCelda,$lastColumn)
        {
        try{    $tipoLetra = ($lastColumn===TRUE)? 'ArialNarrowBlackBold12' : 'ArialNarrowBlack12';
                $tblC->addRow();
                if($twoColumns === TRUE)
                    { $tblC->addCell(12000, $estilo)->addText($texto,$tipoLetra,$estiloCelda); }
                else
                    { $tblC->addCell(6000, $estilo)->addText($texto,$tipoLetra,$estiloCelda);
                      $tblC->addCell(6000, $estilo)->addText(number_format(empty($num1)?0:$num1    , 2, '.', ',').$signo,$tipoLetra,'paraCenterT');
                    }
        } catch (Exception $e) {echo 'columnaOEWORD Excepción: ',  $e, "\n";}       
    }
    
    private function columnaOE($tblOE,$cLineBotThin,$texto, $num1, $num2,$er)
    {
        try{
        if(!empty($texto) )
            {
                $tblOE->addRow();
                $tblOE->addCell(4000, $cLineBotThin)->addText($texto,'ArialNarrowBlack12','paraLeftT');                 
                $tblOE->addCell(4000, $cLineBotThin)->addText(     number_format(empty($num1)?0:$num1    , 2, '.', ',').' %','ArialNarrowBlack12','paraCenterT');
                $tblOE->addCell(4000, $cLineBotThin)->addText('$ '.number_format(empty($num2)?0:$num2/$er, 2, '.', ',').' usd','ArialNarrowBlack12','paraCenterT');
            }
        } catch (Exception $e) {echo 'columnaOEWORD Excepción: ',  $e, "\n";}       
    }
    
    private function columnaPFD($tblPFD,$cLineBotThin,$texto, $r1, $r2,$av)
    {
        try{
        if(!empty($texto) )
            {
                $tblPFD->addRow();
                $tblPFD->addCell(3000, $cLineBotThin)->addText($texto,'ArialNarrowBlack12','paraLeftT');                 
                $tblPFD->addCell(3000, $cLineBotThin)->addText(number_format(empty($r1)?0:$r1, 2, '.', ',').' %','ArialNarrowBlack12','paraCenterT');
                $tblPFD->addCell(3000, $cLineBotThin)->addText(number_format(empty($r2)?0:$r2, 2, '.', ',').' %','ArialNarrowBlack12','paraCenterT');
                $tblPFD->addCell(3000, $cLineBotThin)->addText(number_format(empty($av)?0:$av, 2, '.', ',').' %','ArialNarrowBlack12','paraCenterT');
            }
        } catch (Exception $e) {echo 'columnaPFDWORD Excepción: ',  $e, "\n";}      
    }
    
    private function columnaBI($tblBI,$estilo,$texto, $num, $signo,$colum3,$colum4,$colum5,$colum6,$lastColumn)
    {
        try{
        if(!empty($texto) )
            {
                $estiloTexto = ($lastColumn===TRUE)? 'ArialNarrowBlackBold12' : 'ArialNarrowBlack12';
                $ro          = ($lastColumn===TRUE)? 'Ro ' : '';
                $tblBI->addRow();
                $tblBI->addCell(5000, $estilo)->addText($texto,$estiloTexto,'paraLeftT');
                $tblBI->addCell(2000, $estilo)->addText(empty($num)?'':number_format($num, 2, '.', ',').' '.$signo,'ArialNarrowBlack12','paraCenterT');
                $tblBI->addCell(750,  $estilo)->addText($colum3,'ArialNarrowBlack12','paraCenterT');
                $tblBI->addCell(1500, $estilo)->addText(empty($colum4)?'':number_format($colum4, 2, '.', ',').' %','ArialNarrowBlack12','paraCenterT');
                $tblBI->addCell(750,  $estilo)->addText($ro.$colum5,$estiloTexto,'paraCenterT');
                $tblBI->addCell(2000, $estilo)->addText(empty($colum6)?'':number_format($colum6, 2, '.', ',').' %',$estiloTexto,'paraCenterT');
            }
        } catch (Exception $e) {echo 'columnaPFDWORD Excepción: ',  $e, "\n";}      
    }

    
    public function creaCoreAPVWORD($section,$datos,$md_inmuebles,$numAP,$plans,$plansFT,$totTRA)
    {
    try{$param            = $this->leyendasAPV();
        $styleCell        = array('valign' => 'center');
        $styleCellLine    = array('valign' => 'center','borderBottomSize' => 15,'borderBottomColor'=>'#CB0B0B');
        $sCellLineBottom  = array('valign' => 'center','borderBottomSize' => 15,'borderBottomColor'=>'#CB0B0B','spaceAfter' => 0,'spaceBefore' => 0);
        $sCellLineTopBot  = array('valign' => 'center','borderTopSize'     => 15,'borderBottomSize' => 15,'borderTopColor'=>'#CB0B0B','borderBottomColor'=>'#CB0B0B','spaceAfter' => 0,'spaceBefore' => 0);        
        $sCellLineBtThin  = array('valign' => 'center','borderBottomSize' => 10,'borderBottomColor'=>'#CB0B0B','spaceAfter' => 0,'spaceBefore' => 0);
        $sCellLineSinLine = array('valign' => 'center','borderBottomSize' => 15,'borderBottomColor'=>'#FFFFFF','spaceAfter' => 0,'spaceBefore' => 0);
        $sCellSinLine7    = array('gridSpan' => 7,'valign' => 'center','borderBottomSize' => 15,'borderBottomColor'=>'#FFFFFF','spaceAfter' => 0,'spaceBefore' => 0);
        $sCellLineBot     = array('valign' => 'center','borderBottomSize' => 15,'borderBottomColor'=>'#CB0B0B');
        $sCellLineBotThin = array('valign' => 'center','borderBottomSize' => 10,'borderBottomColor'=>'#CB0B0B');
        $cellColSpan3     = array('gridSpan' => 3     ,'borderBottomSize' => 15,'borderBottomColor'=>'#CB0B0B','valign' => 'center');
        $cellColSpan5     = array('gridSpan' => 5     ,'borderBottomSize' => 15,'borderBottomColor'=>'#CB0B0B','valign' => 'center');
        $cellColSpan5TopBot= array('gridSpan' => 5     ,'borderTopSize'     => 15,'borderBottomSize' => 15,'borderTopColor'=>'#CB0B0B','borderBottomColor'=>'#CB0B0B','spaceAfter' => 0,'spaceBefore' => 0,'valign' => 'center');
        $cellColSpan6     = array('gridSpan' => 6     ,'borderBottomSize' => 15,'borderBottomColor'=>'#CB0B0B','valign' => 'center');
        $cellColSpan7     = array('gridSpan' => 7     ,'borderBottomSize' => 15,'borderBottomColor'=>'#CB0B0B','valign' => 'center');        
        $cellColSpan2     = array('gridSpan' => 2     ,'borderBottomSize' => 15,'borderBottomColor'=>'#CB0B0B','valign' => 'center');
        $cellColSpan2Thin = array('gridSpan' => 2     ,'borderBottomSize' => 10,'borderBottomColor'=>'#CB0B0B','valign' => 'center');
        
        $cellColSpan5Square1 = array('gridSpan' => 5, 'borderBottomSize' => 10,'borderBottomColor'=>'#CB0B0B', 'borderTopSize' => 15,'borderTopColor'=>'#CB0B0B', 'borderLeftSize' => 15,'borderLeftColor'=>'#CB0B0B', 'borderRightSize' => 10,'borderRightColor'=>'#FFFFFF','valign' => 'center');
        $cellColSquare1_1    = array(                 'borderBottomSize' => 10,'borderBottomColor'=>'#CB0B0B', 'borderTopSize' => 15,'borderTopColor'=>'#CB0B0B', 'borderLeftSize' => 10,'borderLeftColor'=>'#FFFFFF', 'borderRightSize' => 15,'borderRightColor'=>'#CB0B0B','valign' => 'center');
        $cellColSpan5Square2 = array('gridSpan' => 5, 'borderBottomSize' => 15,'borderBottomColor'=>'#CB0B0B', 'borderTopSize' => 10,'borderTopColor'=>'#CB0B0B', 'borderLeftSize' => 15,'borderLeftColor'=>'#CB0B0B', 'borderRightSize' => 10,'borderRightColor'=>'#FFFFFF','valign' => 'center');
        $cellColSquare2_1    = array(                 'borderBottomSize' => 15,'borderBottomColor'=>'#CB0B0B', 'borderTopSize' => 10,'borderTopColor'=>'#CB0B0B', 'borderLeftSize' => 10,'borderLeftColor'=>'#FFFFFF', 'borderRightSize' => 15,'borderRightColor'=>'#CB0B0B','valign' => 'center');
                        
        $section->addText($datos['APV']['datos'][0]['AVTit1'],'ArialNarrowRedBold12','paraLeftT');
        $section->addText($datos['APV']['datos'][0]['AVText1'],'ArialNarrowBlack12','paraJust');
        $section->addTextBreak(1);
        
        $section->addText($datos['APV']['datos'][0]['AVTit2'],'ArialNarrowRedBold12','paraLeftT');
        $section->addTextBreak(1);
        $uvUSD           = $datos['iv']['indicated_value_cls'];
        $totaValueLand   = $plans  * $uvUSD;
        $totaValueLandFt = $plansFT* ($uvUSD/$datos['dc']['ft']);
                
        $tblResumen = $section->addTable(array('cellMargin' => 0, 'cellMarginRight' => 0, 'cellMarginBottom' => 0, 'cellMarginLeft' => 0));                                       
        $tblResumen->addRow();
        $tblResumen->addCell(3000, $sCellLineBottom)->addText($param['tUV'],'ArialNarrowBlackBold12','paraLeftT'); 
        $tblResumen->addCell(3000, $sCellLineBottom)->addText('$ '.number_format($uvUSD, 2, '.', ',').' USD/m2','ArialNarrowBlackBold12','paraCenterT'); 
        $tblResumen->addCell(3000, $sCellLineBottom)->addText('$ '.number_format($uvUSD/$datos['dc']['ft'], 2, '.', ',').' USD/ft2 ','ArialNarrowBlackBold12','paraCenterT');
        $tblResumen->addRow();
        $tblResumen->addCell(3000, $sCellLineBottom)->addText($param['tTot'],'ArialNarrowBlackBold12','paraLeftT'); 
        $tblResumen->addCell(3000, $sCellLineBottom)->addText(number_format($plans  , 2, '.', ',').' m2','ArialNarrowBlackBold12','paraCenterT'); 
        $tblResumen->addCell(3000, $sCellLineBottom)->addText(number_format($plansFT, 2, '.', ',').' ft2','ArialNarrowBlackBold12','paraCenterT');
        $tblResumen->addRow();
        $tblResumen->addCell(3000, $sCellLineBottom)->addText($param['tToV'],'ArialNarrowBlackBold12','paraLeftT'); 
        $tblResumen->addCell(3000, $sCellLineBottom)->addText('$ '.number_format($totaValueLand  , 2, '.', ','),'ArialNarrowBlackBold12','paraCenterT'); 
        $tblResumen->addCell(3000, $sCellLineBottom)->addText('$ '.number_format($totaValueLandFt, 2, '.', ','),'ArialNarrowBlackBold12','paraCenterT');
        $tblResumen->addRow();
        $tblResumen->addCell(3000, $sCellLineBottom)->addText($param['roun'],'ArialNarrowBlackBold12','paraLeftT'); 
        $tblResumen->addCell(3000, $sCellLineBottom)->addText('$ '.number_format($this->redondearHermes($totaValueLand), 2, '.', ','),'ArialNarrowBlackBold12','paraCenterT');
        $tblResumen->addCell(3000, $sCellLineSinLine)->addText('','ArialNarrowBlackBold12','paraCenterT');
            
        $section->addTextBreak(1);
        $section->addText($datos['APV']['datos'][0]['AVText2_2'],'ArialNarrowBlack12','paraJust');        
        $section->addTextBreak(1);
        $section->addText($datos['APV']['datos'][0]['AVTit3'],'ArialNarrowRedBold12','paraLeftT');
        $section->addText($datos['APV']['datos'][0]['AVText3'],'ArialNarrowBlack12','paraJust');
        /// TABLA AREAS
        $section->addText($datos['APV']['datos'][0]['AVTit4'],'ArialNarrowBlackBold12','paraJust');        
        $section->addTextBreak(1);
        foreach ($datos['ACA']['columns'] as $c)
         {
            $tablaAPV = $section->addTable(array('cellMargin' => 0, 'cellMarginRight' => 0, 'cellMarginBottom' => 0, 'cellMarginLeft' => 0));
            $tablaAPV->addRow();
            $tablaAPV->addCell(9000, $cellColSpan3)->addText($c['type_area']." ".$c['name_area'],'ArialNarrowBlackBold10','paraLeftT'); 
            $tablaAPV->addCell(3000, $sCellLineBot)->addText("Progress",'ArialNarrowBlack10','paraCenterT'); 
            $tablaAPV->addCell(3000, $sCellLineBot)->addText($param['uc'],'ArialNarrowBlack10','paraCenterT');
            $tablaAPV->addRow();
            $tablaAPV->addCell(1000, $styleCell    )->addText(' ','ArialNarrowBlack10','paraLeftT'); 
            $tablaAPV->addCell(4000, $sCellLineBotThin)->addText($c['def_rcn'],'ArialNarrowBlack10','paraLeftT'); 
            $tablaAPV->addCell(2000, $sCellLineBotThin)->addText('$ '.number_format($c['def_value_rcn'], 2, '.', ','),'ArialNarrowBlack10','paraCenterT'); 
            $tablaAPV->addCell(6000, $cellColSpan2Thin )->addText(" ",'ArialNarrowBlack10','paraCenterT'); 
            $tablaAPV->addRow();
            $tablaAPV->addCell(1000, $styleCell    )->addText(' ','ArialNarrowBlack10','paraLeftT'); 
            $tablaAPV->addCell(4000, $sCellLineBotThin)->addText($param['fbh'],'ArialNarrowBlack10','paraLeftT'); 
            $tablaAPV->addCell(2000, $sCellLineBotThin)->addText($c['fdh_rcn'],'ArialNarrowBlack10','paraCenterT'); 
            $tablaAPV->addCell(6000, $cellColSpan2Thin )->addText(" ",'ArialNarrowBlack10','paraCenterT');
            $tablaAPV->addRow();
            $tablaAPV->addCell(1000, $styleCell    )->addText(' ','ArialNarrowBlack10','paraLeftT'); 
            $tablaAPV->addCell(4000, $sCellLineBotThin)->addText($param['ffc'],'ArialNarrowBlack10','paraLeftT'); 
            $tablaAPV->addCell(2000, $sCellLineBotThin)->addText($c['fc_rcn'],'ArialNarrowBlack10','paraCenterT'); 
            $tablaAPV->addCell(6000, $cellColSpan2Thin )->addText(" ",'ArialNarrowBlack10','paraCenterT'); 
            $tablaAPV->addRow();
            $tablaAPV->addCell(1000, $styleCell    )->addText(' ','ArialNarrowBlack10','paraLeftT'); 
            $tablaAPV->addCell(4000, $sCellLineBotThin)->addText($param['uc'],'ArialNarrowBlack10','paraLeftT'); 
            $tablaAPV->addCell(2000, $sCellLineBotThin)->addText('$ '.number_format($c['uc_rcn'], 2, '.', ','),'ArialNarrowBlack10','paraCenterT'); 
            $tablaAPV->addCell(2000, $sCellLineBotThin)->addText(number_format($c['por_rcn'], 2, '.', ',').'%','ArialNarrowBlack10','paraCenterT'); 
            $ucAd = ($c['uc_rcn'] * ($c['por_rcn']/100)) ; 
            $tablaAPV->addCell(2000, $sCellLineBotThin)->addText('$ '.number_format($ucAd, 2, '.', ','),'ArialNarrowBlack10','paraCenterT'); 
            $section->addTextBreak();
         }
         $section->addPageBreak();
        /// rplacemtn new cost
         
         $section->addText($datos['APV']['datos'][0]['AVTit5'],'ArialNarrowRedBold12','paragraphLeft');
            
         $tablaAPVRCN = $section->addTable(array('cellMargin' => 0, 'cellMarginRight' => 0, 'cellMarginBottom' => 0, 'cellMarginLeft' => 0));
         $tablaAPVRCN->addRow();
         $tablaAPVRCN->addCell(10000, $cellColSpan6)->addText($param['mb'],'ArialNarrowBlackBold10','paraLeftT');
         $tablaAPVRCN->addRow();
         $tablaAPVRCN->addCell(2000, $sCellLineBot)->addText($param['l12'],'ArialNarrowBlackBold10','paraCenterT'); 
         $tablaAPVRCN->addCell(2000, $sCellLineBot)->addText($param['l13'],'ArialNarrowBlack10','paraCenterT');
         $tablaAPVRCN->addCell(2000, $sCellLineBot)->addText("Unit",'ArialNarrowBlack10','paraCenterT');
         $tablaAPVRCN->addCell(2000, $sCellLineBot)->addText($param['qt'],'ArialNarrowBlack10','paraCenterT');
         $cellH1 = $tablaAPVRCN->addCell(2000, $sCellLineBot);
         $this->newLineCell($param['uc'].'\n usd/m2\n usd/ft2', $cellH1, 'ArialNarrowBlack10','paraCenterT');                  
         $tablaAPVRCN->addCell(2500, $sCellLineBot)->addText($param['rcn'],'ArialNarrowBlackBold10','paraCenterT');
         
         $x       = 0;
         $totArea = 0;
         $totRCN  = 0;
         foreach ($datos['ACA']['columns'] as $c)
          {   $x++;
              $ucUSD        = $c['uc_rcn']/$datos['dc']['exchange_rate'];
              $subTotColumn = $md_inmuebles->traSumCAXColumn($datos['ACA']['id_in'],$c['column_Id']);
              $totArea      = $totArea + $subTotColumn;
              $totRCN       = $totRCN + $c['rcn'];
              
              $tablaAPVRCN->addRow();
              $tablaAPVRCN->addCell(2000, $sCellLineBtThin)->addText($x,'ArialNarrowBlack10','paraCenterT'); 
              $tablaAPVRCN->addCell(2000, $sCellLineBtThin)->addText($c['type_area']." ".$c['name_area'],'ArialNarrowBlack10','paraLeftT'); 
              $cell0 = $tablaAPVRCN->addCell(2000, $sCellLineBtThin);
              $this->newLineCell('  sqm \n sqf', $cell0, 'ArialNarrowBlack10','paraCenterT');
              $cell1 = $tablaAPVRCN->addCell(2000, $sCellLineBtThin);
              $this->newLineCell(number_format($subTotColumn, 2, '.', ',').' m2\n'.number_format($subTotColumn*$datos['dc']['ft'], 2, '.', ',').' ft2', $cell1, 'ArialNarrowBlack10','paraCenterT');
              $cell2 = $tablaAPVRCN->addCell(2000, $sCellLineBtThin);
              $this->newLineCell('$ '.number_format($ucUSD, 2, '.', ',').'\n$'.number_format($ucUSD/$datos['dc']['ft'], 2, '.', ','), $cell2, 'ArialNarrowBlack10','paraCenterT');
              $tablaAPVRCN->addCell(2000, $sCellLineBtThin)->addText("$ ".number_format($c['rcn']/$datos['dc']['exchange_rate'], 2, '.', ','),'ArialNarrowBlack10','paraCenterT');               
          }
         
         $tablaAPVRCN->addRow();
         $tablaAPVRCN->addCell(2000, $cellColSpan2)->addText($param['trc'],'ArialNarrowBlackBold10','paraCenterT'); 
         $cell0 = $tablaAPVRCN->addCell(2000, $sCellLineBot);
              $this->newLineCell('  sqm \n sqf', $cell0, 'ArialNarrowBlack10','paraCenterT');
         $cell1 = $tablaAPVRCN->addCell(2000, $sCellLineBot);
         $this->newLineCell(number_format($totArea, 2, '.', ',').'  m2\n'.number_format($totArea*$datos['dc']['ft'], 2, '.', ',').' ft2', $cell1, 'ArialNarrowBlackBold10','paraCenterT');         
         $tablaAPVRCN->addCell(2000, $sCellLineBot)->addText(" ",'ArialNarrowBlackBold10','paraCenterT'); 
         $tablaAPVRCN->addCell(2000, $sCellLineBot)->addText("$ ".number_format($totRCN/$datos['dc']['exchange_rate'], 2, '.', ','),'ArialNarrowBlackBold10','paraCenterT');
        
        $section->addTextBreak();
        $tablaAPVAECW = $section->addTable(array('cellMargin' => 0, 'cellMarginRight' => 0, 'cellMarginBottom' => 0, 'cellMarginLeft' => 0));
        $x     = 0;
        $totAE = 0;
        if(sizeof($datos['ACA']['ae']) > 0) 
        {   /// equipment
             $section->addTextBreak(2);                  
             $tablaAPVAECW->addRow();
             $tablaAPVAECW->addCell(12000, $cellColSpan6)->addText($param['ae'],'ArialNarrowBlackBold10','paraLeftT');
             $tablaAPVAECW->addRow();
             $tablaAPVAECW->addCell(2000, $sCellLineBot)->addText($param['l12'],'ArialNarrowBlackBold10','paraCenterT'); 
             $tablaAPVAECW->addCell(2000, $sCellLineBot)->addText($param['l13'],'ArialNarrowBlack10','paraCenterT');
             $tablaAPVAECW->addCell(2000, $sCellLineBot)->addText("Unit",'ArialNarrowBlack10','paraCenterT');
             $tablaAPVAECW->addCell(2000, $sCellLineBot)->addText($param['qt'],'ArialNarrowBlack10','paraCenterT');
             $cellAE1 = $tablaAPVAECW->addCell(2000, $sCellLineBot);
             $this->newLineCell($param['uc'].'\n usd', $cellAE1, 'ArialNarrowBlack10','paraCenterT');                  
             $tablaAPVAECW->addCell(2000, $sCellLineBot)->addText($param['rcn'],'ArialNarrowBlackBold10','paraCenterT');                     
             foreach ($datos['ACA']['ae'] as $c)
              {   $x++;              
                  $totAE       = $totAE + $c['rcn'];
                  $tablaAPVAECW->addRow();
                  $tablaAPVAECW->addCell(2000,  $sCellLineBtThin)->addText($x,'ArialNarrowBlack10','paraCenterT'); 
                  $tablaAPVAECW->addCell(2000, $sCellLineBtThin)->addText($c['leyenda'],'ArialNarrowBlack10','paraLeftT'); 
                  $tablaAPVAECW->addCell(2000, $sCellLineBtThin)->addText($c['unit'],'ArialNarrowBlack10','paraCenterT'); 
                  $tablaAPVAECW->addCell(2000, $sCellLineBtThin)->addText(number_format($c['amount'], 2, '.', ','),'ArialNarrowBlack10','paraCenterT');               
                  $tablaAPVAECW->addCell(2000, $sCellLineBtThin)->addText(number_format($c['unit_value'], 2, '.', ','),'ArialNarrowBlack10','paraCenterT');               
                  $tablaAPVAECW->addCell(2000, $sCellLineBtThin)->addText("$ ".number_format($c['rcn']/$datos['dc']['exchange_rate'], 2, '.', ','),'ArialNarrowBlack10','paraCenterT'); 
              }

              $tablaAPVAECW->addRow();          
              $tablaAPVAECW->addCell(10000,$cellColSpan5)->addText($param['Tae'],'ArialNarrowBlackBold10','paraLeftT'); 
              $tablaAPVAECW->addCell(2000,$sCellLineBot)->addText("$ ".number_format($totAE/$datos['dc']['exchange_rate'], 2, '.', ','),'ArialNarrowBlackBold10','paraCenterT');
        }
        $section->addPageBreak();
        $tablaAPVCW = $section->addTable(array('cellMargin' => 0, 'cellMarginRight' => 0, 'cellMarginBottom' => 0, 'cellMarginLeft' => 0));        
        $x        = 0;
        $totCW    = 0;        
        if(sizeof($datos['ACA']['cw']) > 0)
        {        
            $tablaAPVCW->addRow();
            $tablaAPVCW->addCell(12000, $cellColSpan6)->addText($param['cw'],'ArialNarrowBlackBold10','paraLeftT');
            $tablaAPVCW->addRow();
            $tablaAPVCW->addCell(2000, $sCellLineBot)->addText($param['l12'],'ArialNarrowBlackBold10','paraCenterT'); 
            $tablaAPVCW->addCell(2000, $sCellLineBot)->addText($param['l13'],'ArialNarrowBlack10','paraCenterT');
            $tablaAPVCW->addCell(2000, $sCellLineBot)->addText("Unit",'ArialNarrowBlack10','paraCenterT');
            $tablaAPVCW->addCell(2000, $sCellLineBot)->addText($param['qt'],'ArialNarrowBlack10','paraCenterT');
            $cellCW1 = $tablaAPVCW->addCell(2000, $sCellLineBot);
            $this->newLineCell($param['uc'].'\n usd', $cellCW1, 'ArialNarrowBlack10','paraCenterT');                  
            $tablaAPVCW->addCell(2000, $sCellLineBot)->addText($param['rcn'],'ArialNarrowBlackBold10','paraCenterT');         
             
            foreach ($datos['ACA']['cw'] as $c)
              {   $x++;              
                  $totCW = $totCW + $c['rcn'];
                  $tablaAPVCW->addRow();
                  $tablaAPVCW->addCell(2000,  $sCellLineBtThin)->addText($x,'ArialNarrowBlack10','paraCenterT'); 
                  $tablaAPVCW->addCell(2000, $sCellLineBtThin)->addText($c['leyenda'],'ArialNarrowBlack10','paraLeftT'); 
                  $tablaAPVCW->addCell(2000, $sCellLineBtThin)->addText($c['unit'],'ArialNarrowBlack10','paraCenterT'); 
                  $tablaAPVCW->addCell(2000, $sCellLineBtThin)->addText(number_format($c['amount'], 2, '.', ','),'ArialNarrowBlack10','paraCenterT'); 
                  $tablaAPVCW->addCell(2000, $sCellLineBtThin)->addText(number_format($c['unit_value'], 2, '.', ','),'ArialNarrowBlack10','paraCenterT');
                  $tablaAPVCW->addCell(2000, $sCellLineBtThin)->addText("$ ".number_format($c['rcn']/$datos['dc']['exchange_rate'], 2, '.', ','),'ArialNarrowBlack10','paraRightT'); 
              }             
             $tablaAPVCW->addRow();          
             $tablaAPVCW->addCell(10000,$cellColSpan5)->addText($param['Tcw'],'ArialNarrowBlackBold10','paraLeftT'); 
             $tablaAPVCW->addCell(2000,$sCellLineBot)->addText("$ ".number_format($totCW/$datos['dc']['exchange_rate'], 2, '.', ','),'ArialNarrowBlackBold10','paraRightT');
             $tablaAPVCW->addRow(300);
             $tablaAPVCW->addCell(12000  ,array('gridSpan' => 6,'valign' => 'center'))->addTextBreak();
          }
          $TOTALRCN =  ($totAE+$totCW+$totRCN);
          $tablaAPVCW->addRow();          
          $tablaAPVCW->addCell(8000,$cellColSpan5Square1)->addText($param['TaeTcw'],'ArialNarrowBlackBold10','paraLeftT'); 
          $tablaAPVCW->addCell(2000,$cellColSquare1_1)->addText("$ ".number_format(($totAE+$totCW)/$datos['dc']['exchange_rate'], 2, '.', ','),'ArialNarrowBlackBold10','paraRightT');
          $tablaAPVCW->addRow();          
          $tablaAPVCW->addCell(8000,$cellColSpan5Square2)->addText($param['TaeTcwC'],'ArialNarrowBlackBold10','paraLeftT'); 
          $tablaAPVCW->addCell(2000,$cellColSquare2_1)->addText("$ ".number_format($TOTALRCN/$datos['dc']['exchange_rate'], 2, '.', ','),'ArialNarrowBlackBold10','paraRightT');
          
          /****************************** DEPRECIATION  ***********************************/
          
          /***************************** CURABLE PHYSICAL DEPECIATION ***/
         $totalCPD = 0;
         if(sizeof($datos['ACA']['cpd']) > 0)
            { $section->addTextBreak(1);   
              $section->addText($datos['APV']['datos'][0]['AVTit6'],'ArialNarrowRedBold12','paragraphLeft');
              $section->addTextBreak(1);

              $tablaCPD = $section->addTable(array('cellMargin' => 0, 'cellMarginRight' => 0, 'cellMarginBottom' => 0, 'cellMarginLeft' => 0));
              $tablaCPD->addRow();
              $tablaCPD->addCell(10000, $cellColSpan6)->addText($param['cpdTit'],'ArialNarrowBlackBold10','paraLeftT');
              $tablaCPD->addRow();
              $tablaCPD->addCell(500, $sCellLineBot)->addText($param['l12'],'ArialNarrowBlackBold10','paraCenterT'); 
              $tablaCPD->addCell(2000, $sCellLineBot)->addText($param['l13'],'ArialNarrowBlack10','paraCenterT');
              $tablaCPD->addCell(2000, $sCellLineBot)->addText($param['un'],'ArialNarrowBlack10','paraCenterT');
              $tablaCPD->addCell(2000, $sCellLineBot)->addText($param['qt'],'ArialNarrowBlack10','paraCenterT');         
              $cellCPD = $tablaCPD->addCell(2000, $sCellLineBot);
              $this->newLineCell($param['uc'].'\n usd', $cellCPD, 'ArialNarrowBlack10','paraCenterT');                  
              $tablaCPD->addCell(2500, $sCellLineBot)->addText($param['cpdTbl'],'ArialNarrowBlackBold10','paraCenterT'); 
              $cpdId       = 0;
              
               foreach ($datos['ACA']['cpd'] as $cpd)
               {  $cpdId++;
                  $totalCPD = $totalCPD + $cpd['rcn'];
                  $tablaCPD->addRow();
                  $tablaCPD->addCell(500,  $sCellLineBtThin)->addText($cpdId,'ArialNarrowBlack10','paraCenterT'); 
                  $tablaCPD->addCell(5500, $sCellLineBtThin)->addText($cpd['leyenda'],'ArialNarrowBlack10','paraLeftT'); 
                  $tablaCPD->addCell(1000, $sCellLineBtThin)->addText($cpd['unit'],'ArialNarrowBlack10','paraCenterT'); 
                  $tablaCPD->addCell(1000, $sCellLineBtThin)->addText(number_format($cpd['amount'], 2, '.', ','),'ArialNarrowBlack10','paraCenterT'); 
                  $tablaCPD->addCell(1000, $sCellLineBtThin)->addText("$ ".number_format($cpd['unit_value']/$datos['dc']['exchange_rate'], 2, '.', ','),'ArialNarrowBlack10','paraCenterT');
                  $tablaCPD->addCell(2000, $sCellLineBtThin)->addText("$ ".number_format($cpd['rcn']/$datos['dc']['exchange_rate'], 2, '.', ','),'ArialNarrowBlack10','paraRightT'); 
               }  
               $tablaCPD->addRow();          
               $tablaCPD->addCell(8000,$cellColSpan5)->addText($param['cpdTot'],'ArialNarrowBlackBold10','paraLeftT'); 
               $tablaCPD->addCell(2000,$sCellLineBot)->addText("$ ".number_format($totalCPD/$datos['dc']['exchange_rate'], 2, '.', ','),'ArialNarrowBlackBold10','paraRightT');
            }
          /**** CPD ***/
            
          $section->addTextBreak(1);
          $section->addText($datos['APV']['datos'][0]['AVTit7'],'ArialNarrowRedBold12','paraLeftT');
          $section->addTextBreak(1);
          $section->addText($datos['APV']['datos'][0]['AVText7_1'],'ArialNarrowBlack12','paraJust');
          $section->addTextBreak(1);
          $section->addText($datos['APV']['datos'][0]['AVText7_2'],'ArialNarrowBlackBold12','paraCenterT');
          $section->addTextBreak(1);
          $section->addText($datos['APV']['datos'][0]['AVText7_3'],'ArialNarrowBlack12','paraJust');
                     
          $section->addTextBreak(1);       
          
          $tablaAPVDEP= $section->addTable(array('cellMargin' => 0, 'cellMarginRight' => 0, 'cellMarginBottom' => 0, 'cellMarginLeft' => 0));
          $tablaAPVDEP->addRow();
          $tablaAPVDEP->addCell(10000, $cellColSpan7)->addText($param['mb'],'ArialNarrowBlackBold10','paraLeftT');
          $tablaAPVDEP->addRow();
          $tablaAPVDEP->addCell(1000, $sCellLineBot)->addText($param['l12'],'ArialNarrowBlackBold10','paraCenterT'); 
          $tablaAPVDEP->addCell(3500, $sCellLineBot)->addText($param['l13'],'ArialNarrowBlackBold10','paraCenterT');
          $tablaAPVDEP->addCell(2000, $sCellLineBot)->addText($param['rcn'],'ArialNarrowBlackBold10','paraCenterT');
          $tablaAPVDEP->addCell(1000, $sCellLineBot)->addText($param['l14'],'ArialNarrowBlackBold10','paraCenterT');
          $tablaAPVDEP->addCell(1000, $sCellLineBot)->addText($param['l15'],'ArialNarrowBlackBold10','paraCenterT');          
          $tablaAPVDEP->addCell(1000, $sCellLineBot)->addText($param['l16'],'ArialNarrowBlackBold10','paraCenterT'); 
          $tablaAPVDEP->addCell(3000, $sCellLineBot)->addText($param['l17'],'ArialNarrowBlackBold10','paraCenterT');           
          
          $x      = 0;
          $totDEP = 0;
          foreach ($datos['ACA']['columns'] as $c)
           {   $x++;              
               $totDEP = $totDEP + $c['dvc_dep'];
               $tablaAPVDEP->addRow();
               $tablaAPVDEP->addCell(1000, $sCellLineBotThin)->addText($x,'ArialNarrowBlack10','paraCenterT'); 
               $tablaAPVDEP->addCell(3500, $sCellLineBotThin)->addText($c['type_area']." ".$c['name_area'],'ArialNarrowBlack10','paraCenterT'); 
               $tablaAPVDEP->addCell(2000, $sCellLineBotThin)->addText("$ ".number_format($c['rcn']/$datos['dc']['exchange_rate'], 2, '.', ','),'ArialNarrowBlack10','paraCenterT'); 
               $tablaAPVDEP->addCell(1000, $sCellLineBotThin)->addText($c['efage_dep'],'ArialNarrowBlack10','paraCenterT'); 
               $tablaAPVDEP->addCell(1000, $sCellLineBotThin)->addText($c['ul_dep'],'ArialNarrowBlack10','paraCenterT'); 
               $tablaAPVDEP->addCell(1000, $sCellLineBotThin)->addText(number_format($c['af_dep']*100, 2, '.', ',')." %",'ArialNarrowBlack10','paraCenterT'); 
               $tablaAPVDEP->addCell(3000, $sCellLineBotThin)->addText("$ ".number_format($c['dvc_dep']/$datos['dc']['exchange_rate'], 2, '.', ','),'ArialNarrowBlack10','paraCenterT'); 
           }
           
          $tablaAPVDEP->addRow();
          $tablaAPVDEP->addCell(1000 ,$sCellLineBot)->addText(" ",'ArialNarrowBlack10','paraCenterT'); 
          $tablaAPVDEP->addCell(8000,$cellColSpan5)->addText($param['mbTot']." (".$param['lt'].")",'ArialNarrowBlackBold10','paraLeftT'); 
          $tablaAPVDEP->addCell(3000,$sCellLineBot)->addText("$ ".number_format($totDEP/$datos['dc']['exchange_rate'], 2, '.', ','),'ArialNarrowBlackBold10','paraCenterT');
          
          if($x >=5)
            { $section->addPageBreak(); 
              $tablaAPVDEP= $section->addTable(array('cellMargin' => 0, 'cellMarginRight' => 0, 'cellMarginBottom' => 0, 'cellMarginLeft' => 0));
            }
            
          $x        = 0;
          $totCWDEP = 0;
          if(sizeof($datos['ACA']['cw']) > 0)
          {
            $tablaAPVDEP->addRow(350);         
            $tablaAPVDEP->addCell(12000, $sCellSinLine7)->addText('','ArialNarrowBlackBold10','paraLeftT');
            $tablaAPVDEP->addRow();         
            $tablaAPVDEP->addCell(12000, $cellColSpan7)->addText($param['cw'],'ArialNarrowBlackBold10','paraLeftT');
            $tablaAPVDEP->addRow();
            $tablaAPVDEP->addCell(1000 ,$styleCellLine)->addText($param['l12'],'ArialNarrowBlackBold10','paraCenterT'); 
            $tablaAPVDEP->addCell(2500 ,$styleCellLine)->addText($param['l13'],'ArialNarrowBlackBold10','paraCenterT');
            $tablaAPVDEP->addCell(2000 ,$styleCellLine)->addText($param['rcn'],'ArialNarrowBlackBold10','paraCenterT');
            $tablaAPVDEP->addCell(1000 ,$styleCellLine)->addText($param['l14'],'ArialNarrowBlackBold10','paraCenterT'); 
            $tablaAPVDEP->addCell(1000 ,$styleCellLine)->addText($param['l15'],'ArialNarrowBlackBold10','paraCenterT'); 
            $tablaAPVDEP->addCell(1000 ,$styleCellLine)->addText($param['l16'],'ArialNarrowBlackBold10','paraCenterT'); 
            $tablaAPVDEP->addCell(3000 ,$styleCellLine)->addText($param['l17'],'ArialNarrowBlackBold10','paraCenterT');

            foreach ($datos['ACA']['cw'] as $c)
             {   $x++;              
                 $totCWDEP = $totCWDEP + $c['dvc_dep'];
                 $tablaAPVDEP->addRow();
                 $tablaAPVDEP->addCell(1000 ,$sCellLineBotThin)->addText($x,'ArialNarrowBlack10','paraCenterT'); 
                 $tablaAPVDEP->addCell(3500 ,$sCellLineBotThin)->addText($c['leyenda'],'ArialNarrowBlack10','paraLeftT');
                 $tablaAPVDEP->addCell(2000 ,$sCellLineBotThin)->addText("$ ".number_format($c['rcn']/$datos['dc']['exchange_rate'], 2, '.', ','),'ArialNarrowBlack10','paraCenterT');
                 $tablaAPVDEP->addCell(1000 ,$sCellLineBotThin)->addText($c['efage_dep'],'ArialNarrowBlack10','paraCenterT'); 
                 $tablaAPVDEP->addCell(1000 ,$sCellLineBotThin)->addText($c['ul_dep'],'ArialNarrowBlack10','paraCenterT'); 
                 $tablaAPVDEP->addCell(1000 ,$sCellLineBotThin)->addText(number_format($c['af_dep']*100, 2, '.', ',')." %",'ArialNarrowBlack10','paraCenterT'); 
                 $tablaAPVDEP->addCell(3000 ,$sCellLineBotThin)->addText("$ ".number_format($c['dvc_dep']/$datos['dc']['exchange_rate'], 2, '.', ','),'ArialNarrowBlack10','paraCenterT');
             }            
            $tablaAPVDEP->addRow();
            $tablaAPVDEP->addCell(1000 ,$sCellLineBot)->addText(" ",'ArialNarrowBlack10','paraCenterT'); 
            $tablaAPVDEP->addCell(8000 ,$cellColSpan5)->addText($param['depTotCW']." (".$param['lt'].")",'ArialNarrowBlackBold10','paraLeftT');          
            $tablaAPVDEP->addCell(3000 ,$sCellLineBot)->addText("$ ".number_format($totCWDEP/$datos['dc']['exchange_rate'], 2, '.', ','),'ArialNarrowBlackBold10','paraCenterT');
            $tablaAPVDEP->addRow(350);         
            $tablaAPVDEP->addCell(12000, $sCellSinLine7)->addText('','ArialNarrowBlackBold10','paraLeftT');
          }
          
          $tablaAPVDEP->addRow(400);
          $tablaAPVDEP->addCell(1000 ,$sCellLineTopBot   )->addText(" ",'ArialNarrowBlack10','paraCenterT'); 
          $tablaAPVDEP->addCell(8000 ,$cellColSpan5TopBot)->addText($param['depTot']." (".$param['lt'].")",'ArialNarrowBlackBold10','paraLeftT');
          $tablaAPVDEP->addCell(3000 ,$sCellLineTopBot   )->addText("$ ".number_format(($totCWDEP+$totDEP)/$datos['dc']['exchange_rate'], 2, '.', ','),'ArialNarrowBlackBold10','paraCenterT');
          
          $x        = 0;
          $totAEDEP = 0;
          if(sizeof($datos['ACA']['ae']) > 0)
          {
            $tablaAPVDEP->addRow(350);
            $tablaAPVDEP->addCell(12000,$sCellSinLine7)->addText('','ArialNarrowBlackBold10','paraLeftT');           

            $tablaAPVDEP->addRow();
            $tablaAPVDEP->addCell(12000,$cellColSpan7)->addText($param['ae'],'ArialNarrowBlackBold10','paraLeftT');           

            $tablaAPVDEP->addRow();
            $tablaAPVDEP->addCell(1000 ,$styleCellLine)->addText($param['l12'],'ArialNarrowBlackBold10','paraCenterT'); 
            $tablaAPVDEP->addCell(3500 ,$styleCellLine)->addText($param['l13'],'ArialNarrowBlackBold10','paraCenterT');
            $tablaAPVDEP->addCell(2000 ,$styleCellLine)->addText($param['rcn'],'ArialNarrowBlackBold10','paraCenterT');
            $tablaAPVDEP->addCell(1000 ,$styleCellLine)->addText($param['l14'],'ArialNarrowBlackBold10','paraCenterT'); 
            $tablaAPVDEP->addCell(1000 ,$styleCellLine)->addText($param['l15'],'ArialNarrowBlackBold10','paraCenterT'); 
            $tablaAPVDEP->addCell(1000 ,$styleCellLine)->addText($param['l16'],'ArialNarrowBlackBold10','paraCenterT'); 
            $tablaAPVDEP->addCell(3000 ,$styleCellLine)->addText($param['l17'],'ArialNarrowBlackBold10','paraCenterT'); 

            foreach ($datos['ACA']['ae'] as $c)
             {   $x++;              
                 $totAEDEP = $totAEDEP + $c['dvc_dep'];
                 $tablaAPVDEP->addRow();
                 $tablaAPVDEP->addCell(1000 ,$sCellLineBotThin)->addText($x,'ArialNarrowBlack10','paraCenterT'); 
                 $tablaAPVDEP->addCell(3500 ,$sCellLineBotThin)->addText($c['leyenda'],'ArialNarrowBlack10','paraLeftT');
                 $tablaAPVDEP->addCell(2000 ,$sCellLineBotThin)->addText("$ ".number_format($c['rcn']/$datos['dc']['exchange_rate'], 2, '.', ','),'ArialNarrowBlack10','paraCenterT');
                 $tablaAPVDEP->addCell(1000 ,$sCellLineBotThin)->addText($c['efage_dep'],'ArialNarrowBlack10','paraCenterT'); 
                 $tablaAPVDEP->addCell(1000 ,$sCellLineBotThin)->addText($c['ul_dep'],'ArialNarrowBlack10','paraCenterT'); 
                 $tablaAPVDEP->addCell(1000 ,$sCellLineBotThin)->addText(number_format($c['af_dep']*100, 2, '.', ',')." %",'ArialNarrowBlack10','paraCenterT'); 
                 $tablaAPVDEP->addCell(3000 ,$sCellLineBotThin)->addText("$ ".number_format($c['dvc_dep']/$datos['dc']['exchange_rate'], 2, '.', ','),'ArialNarrowBlack10','paraCenterT');
             }
            $tablaAPVDEP->addRow();
            $tablaAPVDEP->addCell(8000,$cellColSpan6)->addText($param['depTotAE']." (".$param['st'].")",'ArialNarrowBlackBold10','paraLeftT'); 
            $tablaAPVDEP->addCell(3000,$sCellLineBot)->addText("$ ".number_format($totAEDEP/$datos['dc']['exchange_rate'], 2, '.', ','),'ArialNarrowBlackBold10','paraCenterT');
          }          
          
          /****************************** DEPRECIATION FIN  ***********************************/

          $section->addPageBreak();
          $section->addText($datos['APV']['datos'][0]['AVTit8'],'ArialNarrowRedBold12','paraLeftT');
          $section->addTextBreak(1); 
          \PhpOffice\PhpWord\Shared\Html::addHtml($section,$datos['APV']['datos'][0]['AVText8_1']);
          $section->addTextBreak(1);
          
          /****************************** TABLA FINAL COST APPROACH  ***********************************/
          $span3SquareTop1 =  array('gridSpan' => 3, 'borderBottomSize' => 10,'borderBottomColor'=>'#000000', 'borderTopSize' => 15,'borderTopColor'=>'#000000', 'borderLeftSize' => 15,'borderLeftColor'=>'#000000', 'borderRightSize' => 10,'borderRightColor'=>'#FFFFFF','valign' => 'center');
          $squareTop2      =  array(                 'borderBottomSize' => 10,'borderBottomColor'=>'#000000', 'borderTopSize' => 15,'borderTopColor'=>'#000000', 'borderLeftSize' => 10,'borderLeftColor'=>'#FFFFFF', 'borderRightSize' => 15,'borderRightColor'=>'#000000','valign' => 'center');          
          $span2SquareBot1 =  array('gridSpan' => 2, 'borderBottomSize' => 15,'borderBottomColor'=>'#000000', 'borderTopSize' => 10,'borderTopColor'=>'#000000', 'borderLeftSize' => 15,'borderLeftColor'=>'#000000', 'borderRightSize' => 10,'borderRightColor'=>'#FFFFFF','valign' => 'center');
          $squareBot2      =  array(                 'borderBottomSize' => 15,'borderBottomColor'=>'#000000', 'borderTopSize' => 10,'borderTopColor'=>'#000000', 'borderLeftSize' => 10,'borderLeftColor'=>'#FFFFFF', 'borderRightSize' => 15,'borderRightColor'=>'#000000','valign' => 'center');
          $squareBot       =  array(                 'borderBottomSize' => 15,'borderBottomColor'=>'#000000', 'borderTopSize' => 10,'borderTopColor'=>'#000000', 'borderLeftSize' => 10,'borderLeftColor'=>'#FFFFFF', 'borderRightSize' => 15,'borderRightColor'=>'#FFFFFF','valign' => 'center');
          $span2Tbl1       =  array('gridSpan' => 2, 'borderBottomSize' => 10,'borderBottomColor'=>'#000000', 'borderLeftSize'=> 15,'borderLeftColor'=>'#000000','borderRightSize' => 10,'borderRightColor'=>'#FFFFFF','valign' => 'center');
          $span4Tbl1       =  array('gridSpan' => 4, 'borderBottomSize' => 10,'borderBottomColor'=>'#000000', 'borderLeftSize'=> 15,'borderLeftColor'=>'#000000','borderRightSize' => 15,'borderRightColor'=>'#000000','valign' => 'center');
          $tbl2            =  array(                 'borderBottomSize' => 10,'borderBottomColor'=>'#000000', 'borderRightSize'=> 15,'borderRightColor'=>'#000000','borderLeftSize' => 10,'borderLeftColor'=>'#FFFFFF','valign' => 'center');
          $span1ThinTbl    = array(                 'borderBottomSize' => 10,'borderBottomColor'=>'#000000','valign' => 'center');
          
          $tablaEN = $section->addTable(array('width' => 500,'cellMargin' => 0, 'cellMarginRight' => 0, 'cellMarginBottom' => 0, 'cellMarginLeft' => 0));
          $tablaEN ->addRow();
          $tablaEN ->addCell(10000, $sCellLineTopBot)->addText($datos['APV']['datos'][0]['AVTit8'],'ArialNarrowBlackBold12','paraCenterT');
          $tablaEN ->addCell(10000, $sCellLineTopBot)->addText(number_format(empty($datos['APV']['datos'][0]['AVText8_2'])?0:$datos['APV']['datos'][0]['AVText8_2'], 2, '.', ',').' %','ArialNarrowBlackBold12','paraLeftT');
          $section ->addTextBreak(1);
          
          $por_Ent      = $TOTALRCN * ($datos['APV']['datos'][0]['AVText8_2']/100);
          $tblFinalRCN  = ($TOTALRCN + $por_Ent)/$datos['dc']['exchange_rate'];
          $tpd          = ($totAEDEP + $totCWDEP + $totDEP + $totalCPD);
          $cpdTot       = ($totalCPD+$tpd);
          
          $tablaFinCost = $section->addTable(array('cellMargin' => 0, 'cellMarginRight' => 0, 'cellMarginBottom' => 0, 'cellMarginLeft' => 0));
          $tablaFinCost ->addRow();
          $tablaFinCost ->addCell(8000, $span3SquareTop1)->addText($param['trc'],'ArialNarrowBlackBold12','paraLeftT');
          $tablaFinCost ->addCell(2000, $squareTop2     )->addText('$ '.number_format($TOTALRCN/$datos['dc']['exchange_rate'], 2, '.', ','),'ArialNarrowBlackBold12','paraLeftT');
          $tablaFinCost ->addRow();
          $tablaFinCost ->addCell(8000, $span2Tbl1      )->addText($datos['APV']['datos'][0]['AVTit8'],'ArialNarrowBlackBold12','paraLeftT');
          $tablaFinCost ->addCell(2000, $span1ThinTbl   )->addText(number_format(empty($datos['APV']['datos'][0]['AVText8_2'])?0:$datos['APV']['datos'][0]['AVText8_2'], 2, '.', ',').' %','ArialNarrowBlackBold12','paraCenterT');
          $tablaFinCost ->addCell(2000, $tbl2           )->addText('$ '.number_format($por_Ent/$datos['dc']['exchange_rate'], 2, '.', ','),'ArialNarrowBlackBold12','paraLeftT');
          $tablaFinCost ->addRow();
          $tablaFinCost ->addCell(8000, $span2Tbl1      )->addText($param['rcnFC'],'ArialNarrowBlackBold12','paraRightT');
          $tablaFinCost ->addCell(2000, $span1ThinTbl   )->addText('','ArialNarrowBlack12','paraCenterT');
          $tablaFinCost ->addCell(2000, $tbl2           )->addText('$ '.number_format($tblFinalRCN, 2, '.', ','),'ArialNarrowBlackBold12','paraLeftT');
          $tablaFinCost ->addRow();
          $tablaFinCost ->addCell(8000, $span4Tbl1      )->addText($param['ad'],'ArialNarrowBlackBold12','paraLeftT');
          $tablaFinCost ->addRow();          
          $tablaFinCost ->addCell(8000, $span2Tbl1      )->addText($param['cpdTit'],'ArialNarrowBlackBold12','paraRightT');
          $cpdTitCant = $totalCPD/$datos['dc']['exchange_rate'];
          $cpdTitPor  = empty($tblFinalRCN)?0: $cpdTitCant / $tblFinalRCN;
          $tablaFinCost ->addCell(2000, $span1ThinTbl   )->addText(number_format($cpdTitPor*100 , 2, '.', ',').' %','ArialNarrowBlack12','paraCenterT');
          $tablaFinCost ->addCell(2000, $tbl2           )->addText('$ '.number_format($cpdTitCant, 2, '.', ','),'ArialNarrowBlack12','paraLeftT');
          $tablaFinCost ->addRow();
          $tablaFinCost ->addCell(8000, $span4Tbl1      )->addText($param['ipd'],'ArialNarrowBlackBold12','paraLeftT');
          $tablaFinCost ->addRow();
          $stCant = $totAEDEP/$datos['dc']['exchange_rate'];
          $stPor  = empty($tblFinalRCN)?0: $stCant / $tblFinalRCN;
          $tablaFinCost ->addCell(8000, $span2Tbl1      )->addText($param['st'],'ArialNarrowBlack12','paraRightT');
          $tablaFinCost ->addCell(2000, $span1ThinTbl   )->addText(     number_format( $stPor*100, 2, '.', ',').' %','ArialNarrowBlack12','paraCenterT');
          $tablaFinCost ->addCell(2000, $tbl2           )->addText('$ '.number_format( $stCant, 2, '.', ','),'ArialNarrowBlack12','paraLeftT');
          $tablaFinCost ->addRow();
          $ltCant = ($totCWDEP+$totDEP)/$datos['dc']['exchange_rate'];
          $ltPor  = empty($tblFinalRCN)?0: $ltCant / $tblFinalRCN;
          $tablaFinCost ->addCell(8000, $span2Tbl1      )->addText($param['lt'],'ArialNarrowBlack12','paraRightT');
          $tablaFinCost ->addCell(2000, $span1ThinTbl   )->addText(     number_format( $ltPor*100, 2, '.', ',').' %','ArialNarrowBlack12','paraCenterT');
          $tablaFinCost ->addCell(2000, $tbl2           )->addText('$ '.number_format( $ltCant, 2, '.', ','),'ArialNarrowBlack12','paraLeftT');
          $tablaFinCost ->addRow();          
          $tpdCant = $tpd/$datos['dc']['exchange_rate'];
          $tpdPor  = empty($tblFinalRCN)?0: $tpdCant / $tblFinalRCN;
          $tablaFinCost ->addCell(8000, $span2Tbl1      )->addText($param['tpd'],'ArialNarrowBlack12','paraRightT');
          $tablaFinCost ->addCell(2000, $span1ThinTbl   )->addText(     number_format( $tpdPor*100, 2, '.', ',').' %','ArialNarrowBlack12','paraCenterT');
          $tablaFinCost ->addCell(2000, $tbl2           )->addText('$ '.number_format( $tpdCant, 2, '.', ','),'ArialNarrowBlack12','paraLeftT');
          
          $tablaFinCost ->addRow();          
          $tablaFinCost ->addCell(8000, $span2Tbl1      )->addText($param['cfo'],'ArialNarrowBlackBold12','paraRightT');
          $tablaFinCost ->addCell(2000, $span1ThinTbl   )->addText('','ArialNarrowBlack12','paraCenterT');
          $tablaFinCost ->addCell(2000, $tbl2           )->addText('','ArialNarrowBlack12','paraLeftT');
          $tablaFinCost ->addRow();
          $AVdfaCant = $datos['APV']['datos'][0]['AVdfaNum'] / $datos['dc']['exchange_rate'];
          $AVdfaPor  = empty($tblFinalRCN)?0: $AVdfaCant/$tblFinalRCN;
          $tablaFinCost ->addCell(8000, $span2Tbl1      )->addText($param['dfA'],'ArialNarrowBlack12','paraRightT');
          $tablaFinCost ->addCell(2000, $span1ThinTbl   )->addText(     number_format( $AVdfaPor , 2, '.', ',').' %','ArialNarrowBlack12','paraCenterT');
          $tablaFinCost ->addCell(2000, $tbl2           )->addText('$ '.number_format( $AVdfaCant, 2, '.', ','),'ArialNarrowBlack12','paraLeftT');
          $tablaFinCost ->addRow();
          $AVdefSCant = $datos['APV']['datos'][0]['AVdefSNum'] / $datos['dc']['exchange_rate'];
          $AVdefSPor  = empty($tblFinalRCN)?0:$AVdefSCant/$tblFinalRCN;
          $tablaFinCost ->addCell(8000, $span2Tbl1      )->addText($param['defS'],'ArialNarrowBlack12','paraRightT');
          $tablaFinCost ->addCell(2000, $span1ThinTbl   )->addText(     number_format( $AVdefSPor, 2, '.', ',').' %','ArialNarrowBlack12','paraCenterT');
          $tablaFinCost ->addCell(2000, $tbl2           )->addText('$ '.number_format( $AVdefSCant, 2, '.', ','),'ArialNarrowBlack12','paraLeftT');
          $tablaFinCost ->addRow();
          $AVoaCant = $datos['APV']['datos'][0]['AVoaNum']/$datos['dc']['exchange_rate'];
          $AVoaPor  = empty($tblFinalRCN)?0:$AVoaCant/$tblFinalRCN;
          $tablaFinCost ->addCell(8000, $span2Tbl1      )->addText($param['oa'],'ArialNarrowBlack12','paraRightT');
          $tablaFinCost ->addCell(2000, $span1ThinTbl   )->addText(     number_format( $AVoaPor, 2, '.', ',').' %','ArialNarrowBlack12','paraCenterT');
          $tablaFinCost ->addCell(2000, $tbl2           )->addText('$ '.number_format( $AVoaCant, 2, '.', ','),'ArialNarrowBlack12','paraLeftT');
          $tablaFinCost ->addRow();
          $totCFO      = ($datos['APV']['datos'][0]['AVdfaNum'] + $datos['APV']['datos'][0]['AVdefSNum'] + $datos['APV']['datos'][0]['AVoaNum']) /$datos['dc']['exchange_rate'];
          $AVtotCfoPor = empty($tblFinalRCN)?0:$totCFO/$tblFinalRCN;
          $tablaFinCost ->addCell(8000, $span2Tbl1      )->addText($param['tcfo'],'ArialNarrowBlackBold12','paraRightT');
          $tablaFinCost ->addCell(2000, $span1ThinTbl   )->addText(number_format( $AVtotCfoPor, 2, '.', ',').' %','ArialNarrowBlack12','paraCenterT');
          $tablaFinCost ->addCell(2000, $tbl2           )->addText('$ '.number_format( $totCFO, 2, '.', ','),'ArialNarrowBlack12','paraLeftT');
          $tablaFinCost ->addRow();          
          $tablaFinCost ->addCell(8000, $span2Tbl1      )->addText($param['ifo'],'ArialNarrowBlackBold12','paraRightT');
          $tablaFinCost ->addCell(2000, $span1ThinTbl   )->addText('','ArialNarrowBlack12','paraCenterT');
          $tablaFinCost ->addCell(2000, $tbl2           )->addText('','ArialNarrowBlack12','paraLeftT');
          
          $AVdefyCant        = $datos['APV']['datos'][0]['AVdefyNum']/$datos['dc']['exchange_rate'];
          $AVdefyPor         = empty($tblFinalRCN)?0:$AVdefyCant/$tblFinalRCN;
          
          $AVoAdCant         = $datos['APV']['datos'][0]['AVoAdNum']/$datos['dc']['exchange_rate'];
          $AVoAdPor          = empty($tblFinalRCN)?0:$AVoAdCant/$tblFinalRCN;
          
          $totCFOIn          = ($AVdefyCant + $AVoAdCant) ;
          $AVtotCfoPorIn     = empty($tblFinalRCN)?0:$totCFOIn/$tblFinalRCN;                    
          
          $externObs         = $this->calculaEO(array('tvl'     => $totaValueLand,
                                                      'rcn'     => $tblFinalRCN,
                                                      'tpd'     => $tpdCant,
                                              
                                                      'cr'      => empty($datos['APVI'][0]['AVIcap_rate'])?0:$datos['APVI'][0]['AVIcap_rate'],//9.21,
                                                      'ex'      => empty($datos['APVI'][0]['AVI_totExp'] )?0:$datos['APVI'][0]['AVI_totExp'],//11.685,
                                                      'vcl'     => empty($datos['APVI'][0]['AVIoe_unf']  )?0:$datos['APVI'][0]['AVIoe_unf'],//5,
                                                      'ivLease' => empty($datos['iv']['indicated_value_crl'])?0:$datos['iv']['indicated_value_crl'],
                                                      'tra'     => $totTRA,
                                                ));
          
          $totalDepreciation = $tpdCant + $totCFO + $totCFOIn + $externObs['eo19'];
          $vica              = ($tblFinalRCN-$totalDepreciation) + $totaValueLand;
          
          $tablaFinCost ->addRow();
          $tablaFinCost ->addCell(8000, $span2Tbl1      )->addText($param['defy'],'ArialNarrowBlack12','paraRightT');
          $tablaFinCost ->addCell(2000, $span1ThinTbl   )->addText(number_format( $AVdefyPor*100, 2, '.', ',').' %','ArialNarrowBlack12','paraCenterT');
          $tablaFinCost ->addCell(2000, $tbl2           )->addText('$ '.number_format( $AVdefyCant, 2, '.', ','),'ArialNarrowBlack12','paraLeftT');
          $tablaFinCost ->addRow();          
          $tablaFinCost ->addCell(8000, $span2Tbl1      )->addText($param['oAd'],'ArialNarrowBlack12','paraRightT');
          $tablaFinCost ->addCell(2000, $span1ThinTbl   )->addText(number_format( $AVoAdPor*100, 2, '.', ',').' %','ArialNarrowBlack12','paraCenterT');
          $tablaFinCost ->addCell(2000, $tbl2           )->addText('$ '.number_format( $AVoAdCant, 2, '.', ','),'ArialNarrowBlack12','paraLeftT');
          $tablaFinCost ->addRow();          
          $tablaFinCost ->addCell(8000, $span2Tbl1      )->addText($param['tcfoIn'],'ArialNarrowBlackBold12','paraRightT');
          $tablaFinCost ->addCell(2000, $span1ThinTbl   )->addText(number_format( $AVtotCfoPorIn*100, 2, '.', ',').' %','ArialNarrowBlack12','paraCenterT');
          $tablaFinCost ->addCell(2000, $tbl2           )->addText('$ '.number_format( $totCFOIn, 2, '.', ','),'ArialNarrowBlack12','paraLeftT');
          $tablaFinCost ->addRow();          
          $tablaFinCost ->addCell(8000, $span2Tbl1      )->addText($datos['APV']['datos'][0]['AVTit10'],'ArialNarrowBlackBold12','paraRightT');
          $tablaFinCost ->addCell(2000, $span1ThinTbl   )->addText(number_format(empty($tblFinalRCN)?0:($externObs['eo19']/$tblFinalRCN)*100, 2, '.', ',').' %','ArialNarrowBlack12','paraCenterT');
          $tablaFinCost ->addCell(2000, $tbl2           )->addText('$ '.number_format($externObs['eo19'], 2, '.', ','),'ArialNarrowBlack12','paraLeftT');
          $tablaFinCost ->addRow();          
          $tablaFinCost ->addCell(8000, $span2Tbl1      )->addText($param['depTot'],'ArialNarrowBlackBold12','paraRightT');
          $tablaFinCost ->addCell(2000, $span1ThinTbl   )->addText(number_format(empty($tblFinalRCN)?0:($totalDepreciation/$tblFinalRCN)*100, 2, '.', ',').' %','ArialNarrowBlack12','paraCenterT');
          $tablaFinCost ->addCell(2000, $tbl2           )->addText('$ '.number_format($totalDepreciation, 2, '.', ','),'ArialNarrowBlack12','paraLeftT');
          $tablaFinCost ->addRow();          
          $tablaFinCost ->addCell(8000, $span2Tbl1      )->addText($param['dvc'],'ArialNarrowBlackBold12','paraRightT');
          $tablaFinCost ->addCell(2000, $span1ThinTbl   )->addText(number_format(empty($tblFinalRCN)?0:(($tblFinalRCN-$totalDepreciation)/$tblFinalRCN)*100, 2, '.', ',').' %','ArialNarrowBlack12','paraCenterT');
          $tablaFinCost ->addCell(2000, $tbl2           )->addText('$ '.number_format($tblFinalRCN-$totalDepreciation, 2, '.', ','),'ArialNarrowBlack12','paraLeftT');
          $tablaFinCost ->addRow();          
          $tablaFinCost ->addCell(8000, $span2Tbl1      )->addText($param['lv'],'ArialNarrowBlackBold12','paraRightT');
          $tablaFinCost ->addCell(2000, $span1ThinTbl   )->addText('','ArialNarrowBlack12','paraCenterT');
          $tablaFinCost ->addCell(2000, $tbl2           )->addText('$ '.number_format($totaValueLand, 2, '.', ','),'ArialNarrowBlack12','paraLeftT');
          $tablaFinCost ->addRow();          
          $tablaFinCost ->addCell(8000, $span2SquareBot1)->addText($param['vica'],'ArialNarrowBlackBold12','paraRightT');
          $tablaFinCost ->addCell(2000, $squareBot      )->addText('','ArialNarrowBlack12','paraCenterT');
          $tablaFinCost ->addCell(2000, $squareBot2     )->addText('$ '.number_format($vica, 2, '.', ','),'ArialNarrowBlack12','paraLeftT');
          
          $section->addTextBreak(1); 
          $tablaVICA = $section->addTable(array('width' => 500,'cellMargin' => 0, 'cellMarginRight' => 0, 'cellMarginBottom' => 0, 'cellMarginLeft' => 0));
          $tablaVICA ->addRow();
          $tablaVICA ->addCell(7000, $sCellLineBot)->addText($datos['APV']['datos'][0]['AVTit9'],'ArialNarrowBlackBold12','paraLeftTparaRightT');
          $tablaVICA ->addCell(4000, $sCellLineBot)->addText('$ '.number_format($this->redondearHermes($vica), 2, '.', ','),'ArialNarrowBlackBold12','paraRightT');
          
          /****************************** TABLA FINAL COST APPROACH  FIN***********************************/
          
          /****************************** EXTERNAL OBSOLESCEN  INI***********************************/
          $squareTopLeft  =  array('borderBottomSize' => 10,'borderBottomColor'=>'#000000', 'borderTopSize' => 15,'borderTopColor'=>'#000000', 'borderLeftSize' => 15,'borderLeftColor'=>'#000000', 'borderRightSize' => 10,'borderRightColor'=>'#FFFFFF','valign' => 'center');
          $squareTopRight =  array('borderBottomSize' => 10,'borderBottomColor'=>'#000000', 'borderTopSize' => 15,'borderTopColor'=>'#000000', 'borderLeftSize' => 10,'borderLeftColor'=>'#FFFFFF', 'borderRightSize' => 15,'borderRightColor'=>'#000000','valign' => 'center');          
          $squareBotLeft  =  array('borderBottomSize' => 15,'borderBottomColor'=>'#000000', 'borderTopSize' => 10,'borderTopColor'=>'#000000', 'borderLeftSize' => 15,'borderLeftColor'=>'#000000', 'borderRightSize' => 10,'borderRightColor'=>'#FFFFFF','valign' => 'center');
          $squareBotRight =  array('borderBottomSize' => 15,'borderBottomColor'=>'#000000', 'borderTopSize' => 10,'borderTopColor'=>'#000000', 'borderLeftSize' => 10,'borderLeftColor'=>'#FFFFFF', 'borderRightSize' => 15,'borderRightColor'=>'#000000','valign' => 'center');
          $tblLeft        =  array('borderBottomSize' => 10,'borderBottomColor'=>'#000000', 'borderLeftSize'=> 15,'borderLeftColor'=>'#000000','borderRightSize' => 10,'borderRightColor'=>'#FFFFFF','valign' => 'center');
          $tblRight       =  array('borderBottomSize' => 10,'borderBottomColor'=>'#000000', 'borderLeftSize'=> 10,'borderLeftColor'=>'FFFFFF','borderRightSize' => 15,'borderRightColor'=>'##000000','valign' => 'center');                    

          $section->addPageBreak();
          $section->addText($datos['APV']['datos'][0]['AVTit10'],'ArialNarrowRedBold12','paraLeftT');
          $section->addTextBreak(1);
          \PhpOffice\PhpWord\Shared\Html::addHtml($section,$datos['APV']['datos'][0]['AVText10']);
          $section->addTextBreak(2);
          
          $tablaEO = $section->addTable(array('cellMargin' => 0, 'cellMarginRight' => 0, 'cellMarginBottom' => 0, 'cellMarginLeft' => 0));
          $tablaEO ->addRow();
          $tablaEO ->addCell(10000, $squareTopLeft )->addText($param['eo1'],'ArialNarrowBlackBold12','paraLeftT');
          $tablaEO ->addCell(2000, $squareTopRight)->addText('$ '.number_format($externObs['eo1'], 2, '.', ','),'ArialNarrowBlackBold12','paraLeftT');
          $tablaEO ->addRow();
          $tablaEO ->addCell(10000, $tblLeft )->addText($param['eo2'],'ArialNarrowBlackBold12','paraLeftT');
          $tablaEO ->addCell(2000, $tblRight)->addText(number_format($externObs['eo2'], 2, '.', ',').' %','ArialNarrowBlackBold12','paraLeftT');
          $tablaEO ->addRow();
          $tablaEO ->addCell(10000, $tblLeft)->addText($param['eo3'],'ArialNarrowBlackBold12','paraLeftT');
          $tablaEO ->addCell(2000, $tblRight)->addText('$ '.number_format($externObs['eo3'], 2, '.', ','),'ArialNarrowBlackBold12','paraLeftT');          
          $tablaEO ->addRow();
          $tablaEO ->addCell(10000, $tblLeft)->addText($param['eo4'],'ArialNarrowBlackBold12','paraLeftT');
          $tablaEO ->addCell(2000, $tblRight)->addText(number_format($externObs['eo4'], 2, '.', ',').' %','ArialNarrowBlackBold12','paraLeftT');
          $tablaEO ->addRow();
          $tablaEO ->addCell(10000, $tblLeft)->addText($param['eo5'],'ArialNarrowBlackBold12','paraLeftT');
          $tablaEO ->addCell(2000, $tblRight)->addText('$ '.number_format($externObs['eo5'], 2, '.', ','),'ArialNarrowBlackBold12','paraLeftT');
          $tablaEO ->addRow();
          $tablaEO ->addCell(10000, $tblLeft)->addText($param['eo6'],'ArialNarrowBlackBold12','paraLeftT');
          $tablaEO ->addCell(2000, $tblRight)->addText(number_format($externObs['eo6'], 2, '.', ',').' %','ArialNarrowBlackBold12','paraLeftT');
          $tablaEO ->addRow();
          $tablaEO ->addCell(10000, $tblLeft)->addText($param['eo7'],'ArialNarrowBlackBold12','paraLeftT');
          $tablaEO ->addCell(2000, $tblRight)->addText('$ '.number_format($externObs['eo7'], 2, '.', ','),'ArialNarrowBlackBold12','paraLeftT');
          $tablaEO ->addRow();
          $tablaEO ->addCell(10000, $tblLeft)->addText($param['eo8'],'ArialNarrowBlackBold12','paraLeftT');
          $tablaEO ->addCell(2000, $tblRight)->addText('$ '.number_format($externObs['eo8'], 2, '.', ','),'ArialNarrowBlackBold12','paraLeftT');
          $tablaEO ->addRow();
          $tablaEO ->addCell(10000, $tblLeft)->addText($param['eo9'],'ArialNarrowBlackBold12','paraLeftT');
          $tablaEO ->addCell(2000, $tblRight)->addText('$ '.number_format($externObs['eo9'], 2, '.', ','),'ArialNarrowBlackBold12','paraLeftT');
          $tablaEO ->addRow();
          $tablaEO ->addCell(10000, $tblLeft)->addText($param['eo10'],'ArialNarrowBlackBold12','paraLeftT');
          $tablaEO ->addCell(2000, $tblRight)->addText('$ '.number_format($externObs['eo10'], 2, '.', ','),'ArialNarrowBlackBold12','paraLeftT');
          $tablaEO ->addRow();
          $tablaEO ->addCell(10000, $tblLeft)->addText($param['eo11'],'ArialNarrowBlackBold12','paraLeftT');
          $tablaEO ->addCell(2000, $tblRight)->addText(' ','ArialNarrowBlackBold12','paraLeftT');          
          $tablaEO ->addRow();
          $tablaEO ->addCell(10000, $tblLeft)->addText($param['eo12'],'ArialNarrowBlackBold12','paraLeftT');
          $tablaEO ->addCell(2000, $tblRight)->addText('$ '.number_format($externObs['eo12'], 2, '.', ','),'ArialNarrowBlackBold12','paraLeftT');
          $tablaEO ->addRow();
          $tablaEO ->addCell(10000, $tblLeft)->addText($param['eo13'],'ArialNarrowBlackBold12','paraLeftT');
          $tablaEO ->addCell(2000, $tblRight)->addText('$ '.number_format($externObs['eo13'], 2, '.', ','),'ArialNarrowBlackBold12','paraLeftT');
          $tablaEO ->addRow();
          $tablaEO ->addCell(10000, $tblLeft)->addText($param['eo14'],'ArialNarrowBlackBold12','paraLeftT');
          $tablaEO ->addCell(2000, $tblRight)->addText('$ '.number_format($externObs['eo14'], 2, '.', ','),'ArialNarrowBlackBold12','paraLeftT');
          $tablaEO ->addRow();
          $tablaEO ->addCell(10000, $tblLeft)->addText($param['eo15'],'ArialNarrowBlackBold12','paraLeftT');
          $tablaEO ->addCell(2000, $tblRight)->addText('$ '.number_format($externObs['eo15'], 2, '.', ','),'ArialNarrowBlackBold12','paraLeftT');
          $tablaEO ->addRow();
          $tablaEO ->addCell(10000, $tblLeft)->addText($param['eo16'],'ArialNarrowBlackBold12','paraLeftT');
          $tablaEO ->addCell(2000, $tblRight)->addText('$ '.number_format($externObs['eo16'], 2, '.', ','),'ArialNarrowBlackBold12','paraLeftT');
          $tablaEO ->addRow();
          $tablaEO ->addCell(10000, $tblLeft)->addText($param['eo17'],'ArialNarrowBlackBold12','paraLeftT');
          $tablaEO ->addCell(2000, $tblRight)->addText('$ '.number_format($externObs['eo17'], 2, '.', ','),'ArialNarrowBlackBold12','paraLeftT');
          $tablaEO ->addRow();
          $tablaEO ->addCell(10000, $tblLeft)->addText($param['eo18'],'ArialNarrowBlackBold12','paraLeftT');
          $tablaEO ->addCell(2000, $tblRight)->addText(number_format($externObs['eo18'], 2, '.', ',').' %','ArialNarrowBlackBold12','paraLeftT');
          $tablaEO ->addRow();
          $tablaEO ->addCell(10000, $squareBotLeft)->addText($param['eo19'],'ArialNarrowBlackBold12','paraLeftT');
          $tablaEO ->addCell(2000, $squareBotRight)->addText('$ '.number_format($externObs['eo19'], 2, '.', ','),'ArialNarrowBlackBold12','paraLeftT');
          
          /****************************** EXTERNAL OBSOLESCEN  FIN***********************************/
          $section->addTextBreak();
          
          return $numAP;
          
        } catch (Exception $e) {echo 'creaCoreAPV Excepción: ',  $e, "\n";} 
    }
       
    
    public function creaTablaACAWord($section,$datos,$dc,$md_inmuebles)
    {           
     $er = $dc['exchange_rate'];
     $ft = $dc['ft'];
     
    try{         
        $styleCellAA1  = array('valign' => 'center');
        $styleCellLine = array('valign' => 'center','borderTopSize'    => 2,'borderTopColor'=>'#C5C5C5');
        $sCellLineBot  = array('valign' => 'center','borderBottomSize' => 2,'borderTopColor'=>'#C5C5C5');
        $tablaACA      = $section->addTable(array('cellMargin' => 0, 'cellMarginRight' => 0, 'cellMarginBottom' => 0, 'cellMarginLeft' => 0));

        $subTotColumn = 0;      
        $totalRCN     = 0;
        $totCA        = 0;
        $totGBA       = 0;
        $totTRA       = 0;
        $rowId        = 0;

        $tablaACA->addRow();
        $tablaACA->addCell(3000, $sCellLineBot)->addText(' ','ArialNarrowBlackBold11','paraCenterT');

        foreach ($datos['columns'] as $c)
        {   $cell = $tablaACA->addCell(2000, $sCellLineBot);
            $this->newLineCell($c['type_area'].'\n'.$c['name_area'], $cell, 'ArialNarrowBlackBold11','paraCenterT');
        }
        $tablaACA->addCell(1000, $sCellLineBot)->addText('TOTAL ','ArialNGrayBold11Italic','paraCenterT');//CELDA TOTAL AREA    
        
        foreach ($datos['rows'] as $r)
        {  $rowId++;
           $columnId = 0;
            
           $tablaACA->addRow();
           $leyenda1 =$rowId.". ".$r['zone'];
           $tablaACA->addCell(3000, $styleCellAA1)->addText($leyenda1,'ArialNarrowBlackBold11','paraCenterT');

           foreach ($datos['columns'] as $c)
           {  $columnId++;                   
              $dataId   = $columnId.$rowId;
              $dato     = $md_inmuebles->traCA($datos['id_in'],$dataId);
              $cellData = $tablaACA->addCell(2000, $styleCellAA1);
              $this->newLineCell(number_format($dato, 2, '.', ',').' m2\n'.number_format($dato*$ft, 2, '.', ',').' ft2', $cellData, 'ArialNarrowBlack10','paraCenterT');                   
           }
           $tablaACA->addCell(1000, $styleCellAA1)->addText(' ','ArialNarrowBlackBold11','paraCenterT');//CELDA TOTAL AREA    
        }

         $tablaACA->addRow();
         $tablaACA->addCell(3000, $styleCellLine)->addText(' ','ArialNarrowBlackBold11','paraCenterT');
         $columnId = 0;
         foreach ($datos['columns'] as $c)
         {  $columnId++;
            $subTotColumn  = $md_inmuebles->traSumCAXColumn($datos['id_in'],$columnId);
            $totCA         = $totCA + $subTotColumn;
            $totGBA        = (empty($c['gba']))?$totGBA+0:$totGBA+$subTotColumn;
            $totTRA        = (empty($c['tra']))?$totTRA+0:$totTRA+$subTotColumn;
            $cell          = $tablaACA->addCell(2000, $styleCellLine);
            $this->newLineCell(number_format($subTotColumn, 2, '.', ',').' m2\n'.number_format($subTotColumn*$ft, 2, '.', ',').' ft2', $cell, 'ArialNarrowBlack10','paraCenterT');
         }
         $tablaACA->addCell(1000, $styleCellLine)->addText(' ','ArialNarrowBlackBold11','paraCenterT');//CELDA TOTAL AREA

         $tablaACA->addRow();
         $tablaACA->addCell(3000, array('gridSpan'=> $columnId+2, 'valign' => 'center','spaceAfter' => 0))->addText(' ','ArialNarrowBlackBold11','paraCenterT');
         
         $tablaACA->addRow();
         $tablaACA->addCell(3000, $styleCellLine)->addText('Replacement Cost New','ArialNGrayBold11Italic','paraCenterT');         
         foreach ($datos['columns'] as $c)
         {  $totalRCN     = $totalRCN + $c['rcn'];
            $cell = $tablaACA->addCell(1000, $styleCellLine);
            $this->newLineCell('$ '.number_format($c['rcn'], 2, '.', ',').' mxn\n$'.number_format($c['rcn']/$er, 2, '.', ',').' usd', $cell, 'ArialNarrowBlack10','paraCenterT');
         }
         $cell = $tablaACA->addCell(1000, $styleCellLine);
         $this->newLineCell('$ '.number_format($totalRCN, 2, '.', ',').' mxn\n$'.number_format($totalRCN/$er, 2, '.', ',').' usd', $cell, 'ArialNarrowBlack10','paraCenterT');
         
         $tablaACA->addRow();
         $tablaACA->addCell(3000, array('gridSpan'=> $columnId+1, 'valign' => 'center','spaceAfter' => 0,'borderTopSize' => 2,'borderTopColor'=>'#C5C5C5'))->addText('Total area occupied in plans','ArialNGrayBold11Italic','paraRightT');
         $cell = $tablaACA->addCell(1000, $styleCellLine);
         $this->newLineCell(number_format($totCA, 2, '.', ',').' m2\n'.number_format($totCA*$ft, 2, '.', ',').' ft2', $cell, 'ArialNarrowBlack10','paragraphRight');
         
         $tablaACA->addRow();
         $tablaACA->addCell(3000, array('gridSpan'=> $columnId+1, 'valign' => 'center','spaceAfter' => 0,'borderTopSize' => 2,'borderTopColor'=>'#C5C5C5'))->addText('Gross Building Area','ArialNGrayBold11Italic','paraRightT');
         $cell = $tablaACA->addCell(1000, $styleCellLine);
         $this->newLineCell(number_format($totGBA, 2, '.', ',').' m2\n'.number_format($totGBA*$ft, 2, '.', ',').' ft2', $cell, 'ArialNarrowBlack10','paraRightT');
         
         $tablaACA->addRow();
         $tablaACA->addCell(3000, array('gridSpan'=> $columnId+1, 'valign' => 'center','spaceAfter' => 0,'borderTopSize' => 2,'borderTopColor'=>'#C5C5C5'))->addText('Total Rentable Area','ArialNGrayBold11Italic','paraRightT');
         $cell = $tablaACA->addCell(1000, $styleCellLine);
         $this->newLineCell(number_format($totTRA, 2, '.', ',').' m2\n'.number_format($totTRA*$ft, 2, '.', ',').' ft2', $cell, 'ArialNarrowBlack10','paraRightT');

         $section->addTextBreak();
         
         $tablaAECW = $section->addTable(array('cellMargin' => 0, 'cellMarginRight' => 0, 'cellMarginBottom' => 0, 'cellMarginLeft' => 0));
         $tablaAECW->addRow();
         $tablaAECW->addCell(3000, $sCellLineBot)->addText('ACCESORY EQUIPMENT ','ArialNGrayBold11Italic','paraCenterT');
         $tablaAECW->addCell(2000, $sCellLineBot)->addText('Amount '             ,'ArialNGrayBold11Italic','paraCenterT');
         $tablaAECW->addCell(2000, $sCellLineBot)->addText('Unit '               ,'ArialNGrayBold11Italic','paraCenterT');
         $tablaAECW->addCell(2000, $sCellLineBot)->addText('Unit Value '         ,'ArialNGrayBold11Italic','paraCenterT');
         $tablaAECW->addCell(2000, $sCellLineBot)->addText('Total '              ,'ArialNGrayBold11Italic','paraCenterT');
         $aeId    = 0;
         $totalAE = 0;
         foreach ($datos['ae'] as $ae)
         {   $aeId++;
             $totalAE  = $totalAE + ($ae['unit_value']*$ae['amount']);
             $tablaAECW->addRow();
             $tablaAECW->addCell(3000, $styleCellAA1)->addText($aeId.'. '.$ae['leyenda'],'ArialNarrowBlack10','paraCenterT');
             $tablaAECW->addCell(2000, $styleCellAA1)->addText(number_format(($ae['amount']), 2, '.', ','),'ArialNarrowBlack10','paraCenterT');
             $tablaAECW->addCell(2000, $styleCellAA1)->addText($ae['unit']           ,'ArialNarrowBlack10','paraCenterT');
             $cell = $tablaAECW->addCell(2000, $styleCellAA1);
             $this->newLineCell('$ '.number_format($ae['unit_value'], 2, '.', ',').' mxn\n$'.number_format($ae['unit_value']/$er, 2, '.', ',').' usd', $cell, 'ArialNarrowBlack10','paraCenterT');
             $cell = $tablaAECW->addCell(2000, $styleCellAA1);
             $this->newLineCell('$ '.number_format($ae['unit_value']*$ae['amount'], 2, '.', ',').' mxn\n$'.number_format((($ae['unit_value']*$ae['amount'])/$er), 2, '.', ',').' usd', $cell, 'ArialNarrowBlack10','paraCenterT');                                       
         }
         $tablaAECW->addRow();
         $tablaAECW->addCell(3000, array('gridSpan'=> 4, 'valign' => 'center','spaceAfter' => 0,'borderTopSize' => 2,'borderTopColor'=>'#C5C5C5'))->addText('Total Accesory Equipment','ArialNGrayBold11Italic','paraRightT');
         $cell = $tablaAECW->addCell(1000, $styleCellLine);
         $this->newLineCell('$ '.number_format($totalAE, 2, '.', ',').' mxn\n$'.number_format($totalAE/$er, 2, '.', ',').' usd', $cell, 'ArialNarrowBlack10','paragraphRight');
         
         $section->addTextBreak();
         
         $tablaCW = $section->addTable(array('cellMargin' => 0, 'cellMarginRight' => 0, 'cellMarginBottom' => 0, 'cellMarginLeft' => 0));
         $tablaCW->addRow();
         $tablaCW->addCell(3000, $sCellLineBot)->addText('COMPLEMENTARY WORKS ','ArialNGrayBold11Italic','paraCenterT');
         $tablaCW->addCell(2000, $sCellLineBot)->addText('Amount '             ,'ArialNGrayBold11Italic','paraCenterT');
         $tablaCW->addCell(2000, $sCellLineBot)->addText('Unit '               ,'ArialNGrayBold11Italic','paraCenterT');
         $tablaCW->addCell(2000, $sCellLineBot)->addText('Unit Value '         ,'ArialNGrayBold11Italic','paraCenterT');
         $tablaCW->addCell(2000, $sCellLineBot)->addText('Total '              ,'ArialNGrayBold11Italic','paraCenterT');
         $cwId    = 0;
         $totalCW = 0;
         foreach ($datos['cw'] as $cw)
         {   $cwId++;
             $totalCW  = $totalCW + ($cw['unit_value'] * $cw['amount']);
             $tablaCW->addRow();
             $tablaCW->addCell(3000, $styleCellAA1)->addText($cwId.'. '.$cw['leyenda'],'ArialNarrowBlack10','paraCenterT');
             $tablaCW->addCell(2000, $styleCellAA1)->addText(number_format(($cw['amount']), 2, '.', ','),'ArialNarrowBlack10','paraCenterT');
             $tablaCW->addCell(2000, $styleCellAA1)->addText($cw['unit']           ,'ArialNarrowBlack10','paraCenterT');
             $cell = $tablaCW->addCell(2000, $styleCellAA1);
             $this->newLineCell('$ '.number_format($cw['unit_value'], 2, '.', ',').' mxn\n$'.number_format($cw['unit_value']/$er, 2, '.', ',').' usd', $cell, 'ArialNarrowBlack10','paraCenterT');
             $cell = $tablaCW->addCell(2000, $styleCellAA1);
             $this->newLineCell('$ '.number_format($cw['unit_value']*$cw['amount'], 2, '.', ',').' mxn\n$'.number_format((($cw['unit_value']*$cw['amount'])/$er), 2, '.', ',').' usd', $cell, 'ArialNarrowBlack10','paraCenterT');                                       
         }
         $tablaCW->addRow();
         $tablaCW->addCell(3000, array('gridSpan'=> 4, 'valign' => 'center','spaceAfter' => 0,'borderTopSize' => 2,'borderTopColor'=>'#C5C5C5'))->addText('Total facilities','ArialNGrayBold11Italic','paraRightT');
         $cell = $tablaCW->addCell(1000, $styleCellLine);
         $this->newLineCell('$ '.number_format($totalCW, 2, '.', ',').' mxn\n$'.number_format($totalCW/$er, 2, '.', ',').' usd', $cell, 'ArialNarrowBlack10','paragraphRight');
         
         $tablaCW->addRow();
         $tablaCW->addCell(3000, array('gridSpan'=> 4, 'valign' => 'center','spaceAfter' => 0,'borderTopSize' => 2,'borderTopColor'=>'#C5C5C5'))->addText('Total Accesory Equipment + Complementary Works','ArialNGrayBold11Italic','paraRightT');
         $cell = $tablaCW->addCell(1000, $styleCellLine);
         $this->newLineCell('$ '.number_format(($totalCW+$totalAE), 2, '.', ',').' mxn\n$'.number_format(($totalCW+$totalAE)/$er, 2, '.', ',').' usd', $cell, 'ArialNarrowBlack10','paragraphRight');
         
         $tablaCW->addRow();
         $tablaCW->addCell(3000, array('gridSpan'=> 4, 'valign' => 'center','spaceAfter' => 0,'borderTopSize' => 2,'borderTopColor'=>'#C5C5C5'))->addText('Total replacement cost','ArialNGrayBold11Italic','paraRightT');
         $cell = $tablaCW->addCell(1000, $styleCellLine);
         $this->newLineCell('$ '.number_format(($totalCW+$totalAE+$totalRCN), 2, '.', ',').' mxn\n$'.number_format(($totalCW+$totalAE+$totalRCN)/$er, 2, '.', ',').' usd', $cell, 'ArialNarrowBlack10','paragraphRight');

        } catch (Exception $e) {echo 'creaTablaACAWord Excepción: ',  $e, "\n";}    
    }
               
    
    public function generaConsecutivo($consecutivoBD) {
    try{
        $consecutivo = "";        
        $lonConse= strlen(strval($consecutivoBD));
        for($x = 1; $x <= (3-$lonConse);$x++)
            { $consecutivo .="0";}
         return $consecutivo.=$consecutivoBD;
         
       } catch (Exception $e) {echo 'generaConsecutivo Excepción: ',  $e, "\n";}    
    }         
    
    
    public function converToRoman($num)
    { 
        $n = intval($num); 
        $res = '';         
        $romanNumber_Array = array( 'M'  => 1000, 
                                    'CM' => 900, 
                                    'D'  => 500, 
                                    'CD' => 400, 
                                    'C'  => 100, 
                                    'XC' => 90, 
                                    'L'  => 50, 
                                    'XL' => 40, 
                                    'X'  => 10, 
                                    'IX' => 9, 
                                    'V'  => 5, 
                                    'IV' => 4, 
                                    'I'  => 1);
        foreach ($romanNumber_Array as $roman => $number){         
            $matches = intval($n / $number); 
            $res .= str_repeat($roman, $matches); 
            $n = $n % $number; 
           } 
        return $res; 
    } 
    
    public function redondearHermes($num)
    { 
        $ceros  = "";
        $digitos = (strlen(intval($num)))<=3 ? 1: (strlen(intval($num))) <= 6 ?3:4;
        
        for($x = 1; $x <= $digitos; $x++ )
            { $ceros .= "0"; }    

        $divInt = (int)"1".$ceros;        
        $numRedondeado = $num/$divInt;        
        
        return round($numRedondeado,0)*$divInt;
    }
    
        public function calculaEO($entradas)
    {   
        $eo1 = $entradas['tvl'] + $entradas['rcn'] - $entradas['tpd'];        
        $eo2 = $entradas['cr'];        
        $eo3 = round(($eo1 * ($eo2/100)),4);        
        $eo4 = $entradas['ex'];
        $eo5 = round(($eo3 / (1-($eo4/100))),4);            
        $eo6 = $entradas['vcl'];        
        $eo7 = round(($eo5 / (1-($eo6/100))),4);        
        $eo8 = empty($entradas['tra'])?0:round(($eo7 / $entradas['tra']),4);        
        $eo9 =  round(($eo8 / 12),4);
        $eo10 = $entradas['ivLease'];
        $eo11 = '';
        $eo12 = round($eo9 - $eo10,4);        
        $eo13 = round($eo12 * ($eo6/100),4);
        $eo14 = round($eo12 - $eo13,4);        
        $eo15 = round($eo14 * ($eo4/100),4);
        $eo16 = round($eo14 - $eo15,6);
        $eo17 = round(($eo16*12)*$entradas['tra'],4);        
        $eo18 = $eo2;        
        $eo19 = empty($eo18)?0:$eo17 / ($eo18/100);

        $salidas = array("eo1"=>$eo1,
                         "eo2"=>$eo2,
                         "eo3"=>$eo3,
                         "eo4"=>$eo4,                         
                         "eo5"=>$eo5,            
                         "eo6"=>$eo6,
                         "eo7"=>$eo7,
                         "eo8"=>$eo8,
                         "eo9"=>$eo9,
                         "eo10"=>$eo10,
                         "eo11"=>$eo11,
                         "eo12"=>$eo12,
                         "eo13"=>$eo13,
                         "eo14"=>$eo14,
                         "eo15"=>$eo15,
                         "eo16"=>$eo16,
                         "eo17"=>$eo17,
                         "eo18"=>$eo18,
                         "eo19"=>$eo19,
                         "tvl"=>$entradas['tvl'],
                         "rcn"=>$entradas['rcn'],
                        "tpd"=>$entradas['tpd'],
                        );
        return $salidas;
    }
 
    public function optionsRC8($tipoComp)
    {
        $options = null;
        if($tipoComp=="CRL")
        {
            $options = array("::Choose::","Listing","Real Rent");
        } else { 
            $options = array("::Choose::","Listing","Sale");
        }                                
        return $options;
    }
    
     private function capitalizationRateNumAverage($source, $values)
    {
      try{
       if( $source != 0 ) 
            {array_push($values,$source);}                  
        } catch (Exception $e) {echo 'capitalizationRateNumAverage Excepción: ',  $e, "\n";}    
        
        return $values;
    }
}