<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class UtilLanguage
{         
    public function getLabel($reportPart,$idiom)
    {   switch ($reportPart) 
            {   case "FP": return $this->getLabelsFP($idiom);                
                case "LT": return $this->getLabelsLT($idiom);
                case "SW": return $this->getLabelsSW($idiom);
                case "SM": //SECCION DEL FORMULARIO SUMMARY                    
                    
                break;
                case "API": 

                break;            
                case "APII": 

                break;
                case "APIII": 

                break;
                case "APIV": 

                break;               
                case "APV":

                break;
                case "APVI": 

                break;
                case "APVII":

                break;
                case "APVIII": 

                break; 
                case "APIX": 

                break; 
                case "APX": 

                break; 
                case "APXI": 

                break; 
                case "AA1";
                case "AA3";
                case "AA5";//SECCION DEL FORMULARIO ANNEXS 

                break;
                case "AA2": //SECCION DEL FORMULARIO ANNEXS 2

                break;
                case "AA4": //SECCION DEL FORMULARIO ANNEXS 4                    

                break;
                case "AA6": //SECCION DEL FORMULARIO ANNEXS 6                    

                break;            
                case "ACA":

                break;
                case "LC":

                break;
            }
    }
        
    
    private function getLabelsFP($idiom)
    {
      if ($idiom ===  "ENG")
      { return  array("FP0"      => "CONTENT"
                     ,"FP1"      => "VALUATION REPORT "
                     ,"FP2"      => "Prepared for "
                     ,"FP3"      => "Property "
                     ,"FP4"      => "Appraisal No: "
                     );
      } 
      else
      { return  array("FP0"      => "CONTENIDO"
                     ,"FP1"      => "REPORTE DE VALUACIÓN "
                     ,"FP2"      => "Preparado para "
                     ,"FP3"      => "Propiedad de "
                     ,"FP4"      => "Número de reporte: "
                     );
      }            
    } 
    
    private function getLabelsLT($idiom)
    {
      if ($idiom ===  "ENG")
      { return  array("LT0" => "LETTER OF TRANSMITTAL"
                     ,"LT1" => "Mexico City, "
                     ,"LT2" => "With the followings result as of: "
                     ,"LT3" => "Valuation and Consulting Services "
                     ,"LT4" => "JLL Mexico"
                     ,"LT5" => "DEFINITIONS"
                     ,"LT6" => "Definitions of pertinent terms taken from The Dictionary of Real Estate Appraisal, 5th ed. (Chicago: Appraisal Institute, 2010) are as follows:"
                     );
      } 
      else
      { return  array("LT0" => "CARTA"
                     ,"LT1" => "México, CDMX, a "
                     ,"LT2" => "Con los siguientes resultados al: "
                     ,"LT3" => "Valuación y Estudios de Mercado "
                     ,"LT4" => "JLL México "
                     ,"LT5" => "DEFINICIONES"
                     ,"LT6" => "Las definiciones de los términos más empleados, obtenidos de The Dictionary of Real Estate Appraisal, 5th ed. (Chicago: Appraisal Institute, 2010) se muestran a continuación:"
                     );
      }            
    } 
    
    private function getLabelsSW($idiom)
    {
      if ($idiom ===  "ENG")
      { return  array("SW0" => "SCOPE OF WORK"
                     ,"SW1" => "Mexito City, "
                     ,"SW2" => "With the followings result as of: "
                     ,"SW3" => "Valuation and Consulting Services "
                     ,"SW4" => "JLL Mexico"
                     ,"SW5" => "DEFINITIONS"
                     ,"SW6" => "Definitions of pertinent terms taken from The Dictionary of Real Estate Appraisal, 5th ed. (Chicago: Appraisal Institute, 2010) are as follows:"
                     );
      } 
      else
      { return  array("SW0" => "ALCANCE DEL TRABAJO "
                     ,"SW1" => "México, CDMX, a "
                     ,"SW2" => "Con los siguientes resultados al: "
                     ,"SW3" => "Valuación y Estudios de Mercado "
                     ,"SW4" => "JLL México "
                     ,"SW5" => "DEFINICIONES"
                     ,"SW6" => "Las definiciones de los términos más empleados, obtenidos de The Dictionary of Real Estate Appraisal, 5th ed. (Chicago: Appraisal Institute, 2010) se muestran a continuación:"
                     );
      }            
    } 
    
    private function leyendasFirmas($idiom)
    {
      return  array("vcs"      => "Valuation and Consulting Services"
                   );
    }        
    
    private function leyendasAPV()
    {
      return  array("titulo"      => "PROPERTY APPRAISED",
                    "l1"          => "Description",
                    "t1"          => "In the Cost Approach a property is valued based on a comparison with the cost to build a new or substitute property. The cost estimate is adjusted for all types of depreciation of in the property. To obtain the final value indication the land value is added to the depreciated value of the improvements.",
                    "l2"          => "Land Value",
                    "t2"          => "The area where the land lot is located is a transition zone  surrounded by industrial, commercial, residential  and storage buildings with an average age of twenty years, vacant lots in this area are scarce, so for the analysis of the subject, comparables with industrial and commercial uses will be used.",
                    "t2_1"        => "Annex 1 contains market schedules  market information for different comparables that were found in the area and Annex 2 includes the adjustments that were made in order to arrive to the following conclusion of the land's value ",
                    "t2_2"        => "In Annex 6 there are maps with the location of the subject's land and comparables.",
                    "tTot"        => "Land Surface",
                    "tUV"         => "Unit Value",
                    "un"          => "Unit",
                    "tToV"        => "Total Value of Land",
                    "roun"        => "Rounded",
                    "l3"          => "Value of Improvements:",
                    "t3"          => "In order to obtain replacement cost we use costs manuals..... In additional, the entrepreneurial profit and obsolescence derived from propertys age are included, considering the average date estimated for each built area.",
                    "l4"          => "In the following table we present the cost in USD currency",
                    "fbh"         => "Factor for double height",
                    "ffc"         => "Factor for city",
                    "uc"          => "Unit Cost",
                    "rcnFC"       => "Replacement Cost New",
                    "rcn"         => "Replacement Cost New (USD)",
                    "rrc"         => "Reproduction or replacement costs:",
                    "mb"          => "Main buildings",
                    "qt"          => "Quantity",
                    "tc"          => "Total construction",
                    "ae"          => "Equipment",
                    "Tae"         => "Total Equipment Replacement Cost New ",
                    "cw"          => "Complementary Works",
                    "Tcw"         => "Total Complementary Works",
                    "tf"          => "Total Facilities",
                    "TaeTcw"      => "Total Equipment + Complementary Works",
                    "trc"         => "Total Replacement Cost New",                    
                    "TaeTcwC"     => "Total Replacement Cost New (construction, equipment and complementary works)",
                    "trcC"        => "Total Cost construction and Accessory and Complementary works",                    
                    "l12"         => "No.",
                    "l13"         => "Item",
                    "l14"         => "Age/years",
                    "l15"         => "Useful life",
                    "l16"         => "Age Factor",
                    "l17"         => "Depreciation(USD)",
                    "dep"         => "Depreciation",
                    "l18"         => "Replacement cost",
                    "l19"         => "Total cost",
                    "cpdTit"      => "Curable Physical Deterioration (deferred maintenance)",
                    "cpdTot"      => "Total curable physical deterioration",
                    "cpdTbl"      => "Deferred Maintenance (USD)",
                    "mbTot"       => "Total depreciation of constructions",
                    "depTotCW"    => "Total depreciation complementary works",
                    "depTotAE"    => "Total depreciation of equipment",
                    "depTot"      => "Total depreciation",
                    "groundFloor" => "Ground Floor Area",
                    "gba"         => "Gross Building Area",
                    "tra"         => "Total Rentable Area",
                    "ra"          => "Rentable Area",
                    "amount"      => "Amount",
                    "ealm"        => "Economic Age - Life Method",
                    "aa"          => "Area Analysis",
                    "cap"         => "Construction Area of the Property",
                    "tax"         => "Tax Bill",
                    "deed"        => "Deed",
                    "land"        => "Land Area",
                    "pp"          => "Provided Plans",
                    "ld"          => "Land Difference",
                    "ad"          => "Depreciation",
                    "ipd"         => "Incurable physical deterioration",
                    "tpd"         => "Total physical deterioration",
                    "cfo"         => "Curable functional obsolescence :",
                    "dfA"         => "Deficiency (addition)",
                    "defS"        => "Deficiency (substitution) :",
                    "oa"          => "Superadequacy :",
                    "tcfo"        => "Total curable functional obsolescence :",
                    "tcfoIn"      => "Total incurable functional obsolescence :",
                    "ifo"         => "Incurable functional obsolescence:",
                    "defy"        => "Deficiency:",
                    "oAd"         => "Superadequacy :",                    
                    "EO"          => "External obsolescence :",                    
                    "dvc"         => "Depreciated value of constructions:",
                    "dvc2"        => "Depreciated value of other constructions:",
                    "lv"          => "Land value :",
                    "vica"        => "Value indication by cost approach",
                    "eo1"         => "Total investment (without external obsolescence) (Land + constructions)",
                    "eo2"         => "Capitalization rate: (Minimum feasibility rate)",
                    "eo3"         => "Net rent:",
                    "eo4"         => "Expenses",
                    "eo5"         => "Effective gross income:",
                    "eo6"         => "Vacancy and collection losses:",
                    "eo7"         => "Required annual gross income:",
                    "eo8"         => "Required annual unit rent :",
                    "eo9"         => "Required unit monthly rent :",
                    "eo10"        => "Unit market rent:",
                    "eo11"        => "(Lease adjustment grid)",
                    "eo12"        => "Differences in rent :",
                    "eo13"        => "Vacancy:",
                    "eo14"        => "Effective rent:",
                    "eo15"        => "Operating Expense:",
                    "eo16"        => "NOI (unit cost):",
                    "eo17"        => "NOI (annual cost):",
                    "eo18"        => "Capitalization Rate:",
                    "eo19"        => "Capitalized loss:",
                    "ugmi"        => "Unit Gross Monthly Income",
                    "ugai"        => "Unit Gross Annual Income",
                    "pgi"         => "Potential Gross Income",
                    "rent"        => "Rent (per m2 pear year) ",
                    "anIn"        => "Annual Income (USD)",
                    "err"         => "Expense reimbursement revenue",	
                    "main"        => "[Ej. Maintenance]",
                    "sec"         => "[Ej. Security]",
                    "tpgip"       => "Total potential gross income 100% occupancy",
                    "mvcl"        => "Minus vacancy and collection losses",
                    "egi"         => "Effective gross income:",
                    "st"          => "Short term",
                    "lt"          => "Long term",
                    "serr"        => "Subtotal expense reimbursement revenue",
                    "tpgio"       => "Total potential gross income @ 100% occupancy",                    
                    "oe"          => "Operating expense",                    
                    "se"          => "Subtotal Expenses",
                    "unf"         => "Unforeseen",
                    "totExp"      => "Total expenses",
                    "noi"         => "Net Operating Income",
                    "inEx"        => "Income and Expenses",
                    "source"      => "Source",
                    "cr"          => "Capitalization Rate",
                    "ran"         => "Range",
                    "av"          => "*Average (not necessarily arithmetic mean)",
                    "mir"         => "Mortgage Interest Rate",
                    "mt"          => "Mortgage Term (Amortization Period)",
                    "lvr"         => "Loan-to-Value Ratio",
                    "morCo"       => "Mortgage Constant",
                    "edr"         => "Equity Dividend Rate (EDR)",
                    "morRe"       => "Mortgage Requirement",
                    "eqRe"        => "Equity Requirement",
                    "icr"         => "Indicated Capitalization Rate",
                    "roFor"       => "Ro = DCR x Rm x M",
                    "whereDCR"    => "Where	DCR = Debt Coverage Ratio",
                    "rmLbl"       => "Rm  = Mortgage Capitalization Rate",
                    "mLbl"        => "M   = Loan to Value Ratio",
                    "dcrTit"      => "Considering a typical DCR",
                    "dcrLbl"      => "and similar financing conditions",
                    "ir"          => "Interest rate (i)",
                    "amoPe"       => "Amortization period (n)",
                    "lvLbl"       => "Loan-to-Value:  (M)",
                    "mcLbl"       => "Mortgage Constant (Rm)",
                    "cLbl1"       => "Considering a weighted average from the previous results we have",
                    "cLbl2"       => "With the Net Operating Income and the Capitalization Rate the value under Direct Income Capitalization is obtained as follows",
                    "ica"         => "Income Capitalization Approach",
                    "ivudica"     => "Indicated value under direct income capitalization approach:",
                    "far"         => "Floor Area Ratio :",
                    "mf"          => "Market Factor:",
                    "ls"          => "Land surface:",
                    "fArea"       => "Footing area:",
                    "tls"         => "Typical land surface",
                    "exl"         => "Excess land",
                    "uvel"        => "Unit value of excess land",
                    "vel"         => "Value of excess land",
                    "ivudicael"   => "(including excess land):	",
                    "saop"        => "Saleable area of the property",
                    "uav"         => "Unit adjusted value",
                    "ivusca"      => "Indicated value under Sales Comparison Approach",
                    "cavweo"      => "Cost approach value without external obsolescence",
                    "valAp"       => "Valuation Approach",
                    "currency"    => "usd",
                    "currencyMX"  => "mxn",
                    "costAp"      => "Cost Approach",
                    "sca"         => "Sales Comparison Approach",
                    "vis"         => "Value in Use",
                    "marval"      => "Market Value ",
                    "sugRate"     => "Suggested rate:",
                    "totRiskP"    => "Total risk premium",
                    "discRate"    => "Discount rate",
                    "liqVal"      => "Liquidation Value",
                    "basVal"      => "Base Value",
                    "rfir"        => "Risk-Free interest rate",
                    "adPoin"      => "Additional Points",
                    "oppCos"      => "Opportunity Cost",
                    "estMonS"     => "Liquidation marketing time target (months)",
                    "addRisk"     => "Additional Risk",
                    "difRfir"     => "Dif to RFIR",
                    "mrfir"       => "Monthly Risk free interest Rate",                    
                    "manCos"      => "Management Costs",
                    "perYear"     => "Per Year",
                    "perMonth"    => "Per Month",
                    "month"       => "Monthly",
                    "saleCos"     => "Sale Cost",
                    "commSale"    => "Commission per Sale",
                    "liqFactor"   => "Liquidation Factor",
                    "properTax"   => "Property Tax",
                   );
    }
   
}