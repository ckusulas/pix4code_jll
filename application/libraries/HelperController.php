<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class HelperController
{ 
  
    public function camposRC_5($campo,$relevantCH,$campoLeyenda)
        {            
            $tablaPHC  = '<section class="col col-2">
                            <label class="label">Very Superior</label><label class="input">
                               <i class="icon-append fa fa-percent"></i>'.
                               form_input(array('class'=>'validate[required,custom[number]] text-input', 'name'=>$campo.'v_1', 'id'=>$campo.'v_1','value'=>$relevantCH['v_1'],'maxlength'=> '4')).'
                                <b class="tooltip tooltip-bottom-right">Percentage Very Superior %</b>
                            </label></section>
                            <section class="col col-2">
                            <label class="label">Superior</label><label class="input">
                               <i class="icon-append fa fa-percent"></i>'.
                               form_input(array('class'=>'validate[required,custom[number]] text-input', 'name'=>$campo.'v_2', 'id'=>$campo.'v_2','value'=>$relevantCH['v_2'],'maxlength'=> '4')).'
                                <b class="tooltip tooltip-bottom-right">Percentage Superior %</b>
                            </label></section>
                            <section class="col col-2">
                            <label class="label">Similar</label><label class="input">
                               <i class="icon-append fa fa-percent"></i>'.
                               form_input(array('class'=>'validate[required,custom[number]] text-input', 'name'=>$campo.'v_3', 'id'=>$campo.'v_3','value'=>$relevantCH['v_3'],'maxlength'=> '4')).'
                                <b class="tooltip tooltip-bottom-right">Percentage Similar %</b>
                            </label></section>
                            <section class="col col-2">
                            <label class="label">Inferior</label><label class="input">
                               <i class="icon-append fa fa-percent"></i>'.
                               form_input(array('class'=>'validate[required,custom[number]] text-input', 'name'=>$campo.'v_4', 'id'=>$campo.'v_4','value'=>$relevantCH['v_4'],'maxlength'=> '4')).'
                                <b class="tooltip tooltip-bottom-right">Percentage Inferior %</b>
                            </label></section>
                            <section class="col col-2">
                            <label class="label">Very Inferior</label><label class="input">
                               <i class="icon-append fa fa-percent"></i>'.
                               form_input(array('class'=>'validate[required,custom[number]] text-input', 'name'=>$campo.'v_5', 'id'=>$campo.'v_5','value'=>$relevantCH['v_5'],'maxlength'=> '4')).'
                                <b class="tooltip tooltip-bottom-right">Percentage Very Superior %</b>
                            </label>
                        </section>'.
                            form_input(array('type'=>'hidden','name'=>$campo.'campo','id'=>$campo.'campo', 'value'=>$campoLeyenda));
            return $tablaPHC;
        }
        
        public function camposRC_2($campo,$relevantCH,$leyendas,$campoLeyenda)
        {   $v1 = 0;
            $v2 = 0;
            foreach ($relevantCH as $r)
            {
                if($r['tipo']==="rc8")
                {$v1 = $r['v_1'];
                 $v2 = $r['v_2'];}
            }
            
            $tablaPHC  = '<section class="col col-2">
                            <label class="label">'.$leyendas['l1'].'</label><label class="input">
                               <i class="icon-append fa fa-percent"></i>'.
                               form_input(array('class'=>'validate[required,custom[number]] text-input', 'name'=>$campo.'v_1', 'id'=>$campo.'v_1','value'=>$v1,'maxlength'=> '4')).'
                                <b class="tooltip tooltip-bottom-right">Percentage Very Superior %</b>
                            </label></section>
                            <section class="col col-2">
                            <label class="label">'.$leyendas['l2'].'</label><label class="input">
                               <i class="icon-append fa fa-percent"></i>'.
                               form_input(array('class'=>'validate[required,custom[number]] text-input', 'name'=>$campo.'v_2', 'id'=>$campo.'v_2','value'=>$v2,'maxlength'=> '4')).'
                                <b class="tooltip tooltip-bottom-right">Percentage Superior %</b>
                            </label></section>
                            <section class="col col-1">'.nbs(1).'</section>'.
                            form_input(array('type'=>'hidden','name'=>$campo.'campo','id'=>$campo.'campo', 'value'=>$campoLeyenda));
            return $tablaPHC;
        }
        
         public function camposRC_1($campo,$relevantCH,$leyenda,$textoAlt,$campoLeyenda)
        {
            $subLeyenda = ($leyenda==NULL)?'':'<label class="label">'.$leyenda.'</label>';
            $texto   = ($textoAlt==NULL)?nbs(1):$textoAlt;
            $tablaPHC   = '<section class="col col-2">'.$subLeyenda.'
                            <label class="input">
                               <i class="icon-append fa fa-percent"></i>'.
                               form_input(array('class'=>'validate[required,custom[number]] text-input', 'name'=>$campo.'v_1', 'id'=>$campo.'v_1','value'=>$relevantCH['v_1'],'maxlength'=> '4')).'
                                <b class="tooltip tooltip-bottom-right">Percentage Value %</b>
                            </label></section>
                            <section class="col col-4">'.$texto.'</section>'.
                            form_input(array('type'=>'hidden','name'=>$campo.'campo','id'=>$campo.'campo', 'value'=>$campoLeyenda));
            return $tablaPHC;
        }
        
        public function weight($campo,$value,$ap,$ft, $sumPorWeight,$indiceComp)
        {   $totPor=0;            
            foreach ($sumPorWeight as $pw)
            {
                if (isset($pw[$indiceComp]))
                {
                    $totPor = $totPor + $pw[$indiceComp]; 
                }     
            }
            
            return '<label class="weightPor">'.number_format($totPor, 2, '.', ',').' %</label><br>'.form_input(array('class'=>'weightInput','idComp'=>$campo, 'ap'=>$ap,'apft'=>($ap/$ft), 'name'=>'weight'.$campo, 'id'=>'weight'.$campo,'value'=>($value==0)?NULL:$value, "placeholder"=>0,'maxlength'=> '4',"size"=>'5'))."%";
        }
               
    
        public function composition($campo,$fc,$fcFT,$unidades)
        {            
            return "$<label id='composition".$campo."m2' class='sumTot'>".number_format($fc, 2, '.', ',')."</label> usd/m2".br(1)."$<label id='composition".$campo."ft2'>".number_format($fcFT, 2, '.', ',')." ".$unidades."</label>";
        }
    
        public function creaTablasCompAdjus($relevant_ch,$id_in,$tipoComp)
        {            
             $tablaPHC  =  '<div class="row"><section class="col col-10 subtitulo">RELEVANT CHARACTERISTICS OF THE COMPARABLE</section></div>';
             
             if($tipoComp==="CLS"){
                    $tablaPHC  .= '<div class="row"><section class="col col-2">5) Location                 </section>'.$this->camposRC_5("rc5".$id_in,$relevant_ch[4],"5) Location").'</div>
                                   <div class="row"><section class="col col-10">6) Physical characteristics          </section></div>
                                   <div class="row"><section class="col col-2 rightRC">Size (m2) (ft2)  </section>'.$this->camposRC_1("rc6_0".$id_in,$relevant_ch[5],'X','[X+(X-1)(Superf Comparable / Superf Sujeto))-1]',"Size (m2) (ft2)").'</div>
                                   <div class="row"><section class="col col-2 rightRC">Frontage         </section>'.$this->camposRC_5("rc6_1".$id_in,$relevant_ch[6],"Frontage").'</div>
                                   <div class="row"><section class="col col-2 rightRC">Access           </section>'.$this->camposRC_5("rc6_2".$id_in,$relevant_ch[7],"Access").'</div>
                                   <div class="row"><section class="col col-2 rightRC">Shape            </section>'.$this->camposRC_5("rc6_3".$id_in,$relevant_ch[8],"Shape").'</div>
                                   <div class="row"><section class="col col-2 rightRC">Utilities        </section>'.$this->camposRC_5("rc6_4".$id_in,$relevant_ch[9],"Utilities").'</div>
                                   <div class="row"><section class="col col-2 rightRC">Topography       </section>'.$this->camposRC_5("rc6_5".$id_in,$relevant_ch[10],"Topography").'</div>
                                   <div class="row"><section class="col col-2 rightRC">Depth radio      </section>'.$this->camposRC_5("rc6_6".$id_in,$relevant_ch[11],"Depth radio").'</div>
                                   <div class="row"><section class="col col-2 rightRC">Flood hazard     </section>'.$this->camposRC_5("rc6_7".$id_in,$relevant_ch[12],"Flood hazard ").'</div>
                                   <div class="row"><section class="col col-2 rightRC">Easements        </section>'.$this->camposRC_5("rc6_8".$id_in,$relevant_ch[13],"Easements").'</div>
                                   <div class="row"><section class="col col-2 rightRC">Other 1          </section>'.$this->camposRC_5("rc6_9".$id_in,$relevant_ch[14],"Other 1").'</div>
                                   <div class="row"><section class="col col-2 rightRC">Other 2          </section>'.$this->camposRC_5("rc6_10".$id_in,$relevant_ch[15],"Other 2").'</div>
                                   <div class="row"><section class="col col-2 ">7) Use (zoning)         </section>'.$this->camposRC_5("rc7".$id_in,$relevant_ch[16],"Use (zoning)").'</div>
                                   <div class="row"><section class="col col-2 ">8) Listing / Sale       </section>'.$this->camposRC_2("rc8".$id_in,$relevant_ch,array("l1"=>'Listing',"l2"=>'Sale'),"Listing / Sale").'</div>
                                ';
             } elseif($tipoComp==="CRL"){
                    $tablaPHC  .= '<div class="row"><section class="col col-2">5) Location                 </section>'.$this->camposRC_5("rc5".$id_in,$relevant_ch[4],"5) Location").'</div>
                                  <div class="row"><section class="col col-10">6) Physical characteristics          </section></div>
                                  <div class="row"><section class="col col-2 rightRC">Rentable area (m2)(ft2)</section>'.$this->camposRC_1("rc6_0".$id_in,$relevant_ch[5],'X','[X+(X-1)(Superf Comparable / Superf Sujeto))-1]',"Size (m2) (ft2)").'</div>
                                  <div class="row"><section class="col col-2 rightRC">Condition            </section>'.$this->camposRC_5("rc6_1".$id_in,$relevant_ch[6],"Frontage").'</div>
                                  <div class="row"><section class="col col-2 rightRC">Functional Utility   </section>'.$this->camposRC_5("rc6_2".$id_in,$relevant_ch[7],"Access").'</div>
                                  <div class="row"><section class="col col-2 rightRC">Construction Quality </section>'.$this->camposRC_5("rc6_3".$id_in,$relevant_ch[8],"Shape").'</div>
                                  <div class="row"><section class="col col-2 rightRC">Obsolescence         </section>'.$this->camposRC_5("rc6_4".$id_in,$relevant_ch[9],"Utilities").'</div>
                                  <div class="row"><section class="col col-2 rightRC">Age and Maintenance  </section>'.$this->camposRC_5("rc6_5".$id_in,$relevant_ch[10],"Topography").'</div>                                  
                                  <div class="row"><section class="col col-2 rightRC">Other 1              </section>'.$this->camposRC_5("rc6_9".$id_in,$relevant_ch[11],"Other 1").'</div>
                                  <div class="row"><section class="col col-2 rightRC">Other 2              </section>'.$this->camposRC_5("rc6_10".$id_in,$relevant_ch[12],"Other 2").'</div>
                                  <div class="row"><section class="col col-2 ">7) Use (zoning)             </section>'.$this->camposRC_5("rc7".$id_in,$relevant_ch[13],"Use (zoning)").'</div>
                                  <div class="row"><section class="col col-2 ">8) Listing / Real Rent      </section>'.$this->camposRC_2("rc8".$id_in,$relevant_ch,array("l1"=>'Listing',"l2"=>'Real Rent'),"Listing / Real Rent").'</div>
                                  ';
                    }
                    else{
                    $tablaPHC  .= '<div class="row"><section class="col col-2">5) Location                 </section>'.$this->camposRC_5("rc5".$id_in,$relevant_ch[4],"5) Location").'</div>
                                  <div class="row"><section class="col col-10">6) Physical characteristics          </section></div>
                                  <div class="row"><section class="col col-2 rightRC">Rentable area (m2)(ft2)</section>'.$this->camposRC_1("rc6_0".$id_in,$relevant_ch[5],'X','[X+(X-1)(Superf Comparable / Superf Sujeto))-1]',"Size (m2) (ft2)").'</div>
                                  <div class="row"><section class="col col-2 rightRC">Condition            </section>'.$this->camposRC_5("rc6_1".$id_in,$relevant_ch[6],"Frontage").'</div>
                                  <div class="row"><section class="col col-2 rightRC">Functional Utility   </section>'.$this->camposRC_5("rc6_2".$id_in,$relevant_ch[7],"Access").'</div>
                                  <div class="row"><section class="col col-2 rightRC">Construction Quality </section>'.$this->camposRC_5("rc6_3".$id_in,$relevant_ch[8],"Shape").'</div>
                                  <div class="row"><section class="col col-2 rightRC">Obsolescence         </section>'.$this->camposRC_5("rc6_4".$id_in,$relevant_ch[9],"Utilities").'</div>
                                  <div class="row"><section class="col col-2 rightRC">Age and Maintenance  </section>'.$this->camposRC_5("rc6_5".$id_in,$relevant_ch[10],"Topography").'</div>                                  
                                  <div class="row"><section class="col col-2 rightRC">Other 1              </section>'.$this->camposRC_5("rc6_9".$id_in,$relevant_ch[11],"Other 1").'</div>
                                  <div class="row"><section class="col col-2 rightRC">Other 2              </section>'.$this->camposRC_5("rc6_10".$id_in,$relevant_ch[12],"Other 2").'</div>
                                  <div class="row"><section class="col col-2 ">7) Use (zoning)             </section>'.$this->camposRC_5("rc7".$id_in,$relevant_ch[13],"Use (zoning)").'</div>
                                  <div class="row"><section class="col col-2 ">8) Listing / Sale           </section>'.$this->camposRC_2("rc8".$id_in,$relevant_ch,array("l1"=>'Listing',"l2"=>'Sale'),"Listing / Sale").'</div>
                                  ';
                    }
            return $tablaPHC;
        }
        
        
        public function traeComparableElements()
        {
            return '<strong>Comprabale elements:</strong><br>
                    1) Property rights conveyed<br>
                       Adjustments considered due to the existence of rents below market or rents not on stabilization levels in income-producing properties.<br>
                    2) Financing terms<br>
                       Adjustments considered due to the existence of sales financed through atypical credit conditions.<br>
                    3) Conditions of sale<br>
                       Adjustments considered due to atypical motivation of buyer or seller.<br>
                    4) Market Conditions<br>
                       Adjustments considered due to the price differences in the various cycles that make up the cycle of real estate; that is, effects of inflationary reasons or decreases in prices.<br>
                    5) Location<br>
                       Adjustments considered due to differences such as access, visibility, market reputation and traffic intensity.<br>
                    6) Physical characteristics<br>
                       Adjustments considered due to differences in physical characteristics such as size, conditions, amenities, and property type.<br>
                    7) Use (zoning)<br>
                       Adjustments considered due to differences in the uses and densities permitted.<br>
                    8) Listing / Sale<br>
                       Adjustments considered due to differences between the listing price and the real sale price.<br>
                    ';
        }        
        
        public function creaTablaPhotos($fotos)
    {
        $x          = 0;
        $y          = 0;
        $col        = "";
        $tablaFotos = "";
        foreach ($fotos as $f) 
        {  $x++;
           $y++;         
           $col = $col."<td align='center'><a href='".base_url().$f['nombre']."' target='_blank'> 
                            <img title='Ver Photo' width='430' src='".base_url().$f['nombre']."'></a><br>
                            <label id='lbl".$f['id_gallery']."'>".$f['titulo']."</label>".nbs(3)."<img class='editTit pointer' id='".$f['id_gallery']."' name='".$f['titulo']."' title='Editar Titulo' src='".base_url()."images/edit.png'><br>
                            <img class='delPhoto pointer' title='Borrar Imagen' id='".$f['id_gallery']."' src=\"".base_url()."images/close.png\">
                            <br><br></td> ";
            if (( $x % 2) === 0 )
                {   $tablaFotos = $tablaFotos."<tr><td></td>". $col."</tr>";
                    $col="";
                }
        }
        if (( $x % 3) != 0 )
           { $tablaFotos = $tablaFotos."<tr><td></td>". $col."</tr>"; }
        
       return $tablaFotos;
    }
        
public function creaTablaAAIMG($md_gallery,$id_in,$aaimg) {
    try{         
         $tablaAAIMG  = "";         
         foreach ($aaimg as $aa)
                 {                  
                  $fotosAAIMG  = $md_gallery -> traeFotosAAIMG($id_in,"AAIMG",$aa['titulo']);
                  $tituloAnexo = ($fotosAAIMG[0]['titulo']==="34")?$fotosAAIMG[0]['extraAA']:$aa['anexo'];
                  
                  $tablaAAIMG .=  '<fieldset>
                                        <div class="row"> <h1 id="titAAIMG'.$aa['titulo'].'">'.$tituloAnexo.'</h1> </div>
                                        <div class="row">
                                        <section id="tblAAIMG'.$aa['titulo'].'" class="col col-9">
                                        <center>'
                                          .$this->creaPhotoAAIMG($fotosAAIMG).
                                        '<center>';
                  $tablaAAIMG .=  '     </section>
                                        </div>
                                  </fieldset>';
                 }
        return $tablaAAIMG;
        } catch (Exception $e) {echo 'creaTablaAAIMG Excepción: ',  $e, "\n";}  
    }
    
    public function creaPhotoAAIMG($fotosAAIMG)
    {        
        $p = "";    
        foreach ($fotosAAIMG as $f)
        { 
            $p .= '<div class="row">'.img( array('src'=>base_url().$f['nombre']));
            $p .= br(2).img( array('src'=>base_url()."images/close.png","class"=>"delPhoto pointer","title"=>"Borrar Imagen","id"=>$f['id_gallery'])).'</div>'.br(2);
        }
        
        return $p;
    }
    
    public function creaCoreAnnexsLim($datos) {
    try{        
        $coreAnnL  = "";
        
        $coreAnnL .=  '<fieldset>
                              <div class="row">
                                 <section class="col col-10"><label class="textarea">'.
                                                                form_textarea(array('class' => 'validate[required,custom[onlyLetterNumber]] text-input richTextBigLC',
                                                                                    'rows'  => "5",
                                                                                    'name'  => 'AnnexsLimTxt',
                                                                                    'id'    => 'AnnexsLimTxt',
                                                                                    'value' => $datos['AnnexsLC'][0]['AnnexsLimTxt'] )).'
                                                            </label>
                                   </section>
                                </div>                                
                      </fieldset>
                     ';
        
        return $coreAnnL;
        
        } catch (Exception $e) {echo 'creaCoreAnnexsLim Excepción: ',  $e, "\n";} 
    }
    
    public function creaCoreAnnexsCert($datos) {
    try{        
        $coreAnnCert  = "";
        
        $coreAnnCert .=  '<fieldset>
                              <div class="row">
                                 <section class="col col-10"><label class="textarea">'.
                                                                form_textarea(array('class' => 'validate[required,custom[onlyLetterNumber]] text-input richTextBigLC',
                                                                                    'rows'  => "5",
                                                                                    'name'  => 'AnnexsCertTxt',
                                                                                    'id'    => 'AnnexsCertTxt',
                                                                                    'value' => $datos['AnnexsLC'][0]['AnnexsCertTxt'] )).'
                                                            </label>
                                   </section>
                                </div>                                
                      </fieldset>
                     ';
        
        return $coreAnnCert;
        
        } catch (Exception $e) {echo 'creaCoreAnnexsCert Excepción: ',  $e, "\n";}  
    }
    
    public function creaCoreAPI($datos) {
    try{
        $param  = array("AITit1"     => "Introduction");
        $coreAPI  = "";
        
        $coreAPI .=  '<fieldset>                        
                              <div class="row"><section class="col col-10"><h2>'.$param['AITit1'].'</h2></section></div>
                              <div class="row">
                                <section class="col col-8"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                                                 'name'        => 'AIText1_1',
                                                                                 'id'          => 'AIText1_1',                                                                                 
                                                                                 'value'       => $datos['API'][0]['AIText1_1'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>
                                                           <label class="label" style="padding-top: 12px;">'.$this->creaLocation($datos['frontPage'][0]['calle'],$datos['frontPage'][0]['num'],$datos['frontPage'][0]['col'],$datos['frontPage'][0]['mun'],$datos['frontPage'][0]['edo'],$datos['frontPage'][0]['cp']).'</label>
                                 </section>        
                                 <section class="col col-10"><label class="textarea">'.
                                                                form_textarea(array('class' => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                                                    'rows'  => "3",
                                                                                    'name'  => 'AIText1_2',
                                                                                    'id'    => 'AIText1_2',
                                                                                    'value' => $datos['API'][0]['AIText1_2'] )).'
                                                            </label>
                                   </section>
                                </div>
                                <div class="row"><!--Purpose of the appraisal -->
                                <section class="col col-8"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                                                 'name'        => 'AITit2',
                                                                                 'id'          => 'AITit2',                                                                                 
                                                                                 'value'       => $datos['API'][0]['AITit2'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>                                                           
                                 </section>        
                                 <section class="col col-10"><label class="textarea">'.
                                                                form_textarea(array('class' => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                                                    'rows'  => "3",
                                                                                    'name'  => 'AIText2',
                                                                                    'id'    => 'AIText2',
                                                                                    'value' => $datos['API'][0]['AIText2'] )).'
                                                            </label>
                                   </section>
                                </div>
                                <div class="row"><!-- Intended use of the report -->
                                <section class="col col-8"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                                                 'name'        => 'AITit3',
                                                                                 'id'          => 'AITit3',                                                                                 
                                                                                 'value'       => $datos['API'][0]['AITit3'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>                                                           
                                 </section>        
                                 <section class="col col-10"><label class="textarea">'.
                                                                form_textarea(array('class' => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                                                    'rows'  => "3",
                                                                                    'name'  => 'AIText3',
                                                                                    'id'    => 'AIText3',
                                                                                    'value' => $datos['API'][0]['AIText3'] )).'
                                                            </label>
                                   </section>
                                </div>
                                <div class="row">
                                <section class="col col-8"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                                                 'name'        => 'AITit4',
                                                                                 'id'          => 'AITit4',                                                                                 
                                                                                 'value'       => $datos['API'][0]['AITit4'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>                                                           
                                 </section>
                                 <section class="col col-8"><label class="input"><i class="icon-append fa fa-university"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                                                 'name'        => 'AIText4',
                                                                                 'id'          => 'AIText4',                                                                                 
                                                                                 'value'       => $datos['API'][0]['AIText4'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label> 
                                                            <label class="label" style="padding-top: 12px;">'.br(1).$datos['letter'][0]['client_addres'].' '.$datos['letter'][0]['client_addres_2'].'</label> 
                                </section>
                                </div>
                                <div class="row">
                                <section class="col col-8"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                                                 'name'        => 'AITit5',
                                                                                 'id'          => 'AITit5',                                                                                 
                                                                                 'value'       => $datos['API'][0]['AITit5'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>                                                           
                                 </section>        
                                 <section class="col col-10"> <label class="label" style="padding-top: 12px;">'.$datos['frontPage'][0]['effectiveDate'].' </label> </section>
                                </div>                                
                                <div class="row">
                                <section class="col col-8"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                                                 'name'        => 'AITit6',
                                                                                 'id'          => 'AITit6',                                                                                 
                                                                                 'value'       => $datos['API'][0]['AITit6'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>                                                           
                                 </section>        
                                 <section class="col col-10"> <label class="label" style="padding-top: 12px;">'.date("F j, Y ").' </label> </section>
                                </div>
                                <div class="row">
                                <section class="col col-8"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                                                 'name'        => 'AITit7',
                                                                                 'id'          => 'AITit7',                                                                                 
                                                                                 'value'       => $datos['API'][0]['AITit7'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>                                                           
                                 </section>        
                                 <section class="col col-10"><label class="textarea">'.
                                                                form_textarea(array('class' => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                                                    'rows'  => "3",
                                                                                    'name'  => 'AIText7',
                                                                                    'id'    => 'AIText7',
                                                                                    'value' => $datos['API'][0]['AIText7'] )).'
                                                            </label>
                                   </section>
                                </div>
                                <div class="row">
                                <section class="col col-8"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                                                 'name'        => 'AITit8',
                                                                                 'id'          => 'AITit8',                                                                                 
                                                                                 'value'       => $datos['API'][0]['AITit8'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>                                                           
                                 </section>        
                                 <section class="col col-10"> <label class="label" style="padding-top: 12px;">'.$this->creaLocation($datos['frontPage'][0]['calle'],$datos['frontPage'][0]['num'],$datos['frontPage'][0]['col'],$datos['frontPage'][0]['mun'],$datos['frontPage'][0]['edo'],$datos['frontPage'][0]['cp']).'</label> </section>
                                </div>
                                <div class="row">
                                <section class="col col-8"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                                                 'name'        => 'AITit9',
                                                                                 'id'          => 'AITit9',                                                                                 
                                                                                 'value'       => $datos['API'][0]['AITit9'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>                                                           
                                 </section>        
                                 <section class="col col-10"><label class="textarea">'.
                                                                form_textarea(array('class' => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                                                    'rows'  => "3",
                                                                                    'name'  => 'AIText9',
                                                                                    'id'    => 'AIText9',
                                                                                    'value' => $datos['API'][0]['AIText9'] )).'
                                                            </label>
                                   </section>
                                </div>
                                <div class="row">
                                <section class="col col-8"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                                                 'name'        => 'AITit10',
                                                                                 'id'          => 'AITit10',                                                                                 
                                                                                 'value'       => $datos['API'][0]['AITit10'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>                                                           
                                 </section>        
                                 <section class="col col-10"> <label class="label" style="padding-top: 12px;">'.$datos['frontPage'][0]['propertyOf'].'</label> </section>
                                </div>
                                <div class="row">
                                <section class="col col-8"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                                                 'name'        => 'AITit11',
                                                                                 'id'          => 'AITit11',                                                                                 
                                                                                 'value'       => $datos['API'][0]['AITit11'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>                                                           
                                 </section>        
                                 <section class="col col-10"><label class="textarea">'.
                                                                form_textarea(array('class' => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                                                    'rows'  => "3",
                                                                                    'name'  => 'AIText11',
                                                                                    'id'    => 'AIText11',
                                                                                    'value' => $datos['API'][0]['AIText11'] )).'
                                                            </label>
                                   </section>
                                </div>
                                <div class="row">
                                <section class="col col-8"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                                                 'name'        => 'AITit12',
                                                                                 'id'          => 'AITit12',                                                                                 
                                                                                 'value'       => $datos['API'][0]['AITit12'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>                                                           
                                 </section>
                                 <section class="col col-11"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                                                 'name'        => 'AIText12_1',
                                                                                 'id'          => 'AIText12_1',                                                                                 
                                                                                 'value'       => $datos['API'][0]['AIText12_1'],
                                                                                 'maxlength'   => '100')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>                                                           
                                 </section>                                
                                 <section class="col col-11"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                                                 'name'        => 'AIText12_2',
                                                                                 'id'          => 'AIText12_2',                                                                                 
                                                                                 'value'       => $datos['API'][0]['AIText12_2'],
                                                                                 'maxlength'   => '100')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>                                                           
                                 </section>
                                 <section class="col col-11"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                                                 'name'        => 'AIText12_3',
                                                                                 'id'          => 'AIText12_3',                                                                                 
                                                                                 'value'       => $datos['API'][0]['AIText12_3'],
                                                                                 'maxlength'   => '100')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>
                                 </section>                                      
                                 <section class="col col-11"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                                                 'name'        => 'AIText12_4',
                                                                                 'id'          => 'AIText12_4',                                                                                 
                                                                                 'value'       => $datos['API'][0]['AIText12_4'],
                                                                                 'maxlength'   => '100')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label><br>
                                                            Surrounding Uses Image
                                                            <div id="fileUploaderAPI">&nbsp;Click para seleccionar Imagen&nbsp;</div>
                                                            <div id="statusAPI"></div>'.
                                                                 form_input(array('name'  => 'imgAPI', 
                                                                                  'type'  => 'hidden', 
                                                                                  'id'    => 'imgAPI',
                                                                                  'value' => $datos['API'][0]['imgAPI'])).'
                                                           <div id="divImgAPI">';        
                                                           if( !empty($datos['API'][0]['imgAPI']))
                                                           {
                                                            $coreAPI .= "<img src='".base_url()."gallery/".$datos['id_in']."/".$datos['API'][0]['imgAPI']."'><br><br>".
                                                                         "<img class=\"clImgAPI\" title=\"Borrar imagen\" name='".$datos['API'][0]['imgAPI']."'" .
                                                                         " id=\"".$datos['API'][0]['imgAPI']."\" src=\"".base_url()."/images/close.png\"><br>";
                                                           }
        $coreAPI .=                                       '</div>
                                 </section>                                      
                                 </div>
                      </fieldset>
                     ';
                 
        return $coreAPI;
        } catch (Exception $e) {echo 'creaCoreAPI Excepción: ',  $e, "\n";} 
    }
    
   public function creaCoreAPII($datos,$md_inmuebles) {
    try{$coreAPII  = "";
        $totLand   = empty($datos['land'])?" ":number_format($datos['land'][0]['deed'], 2, '.', ',')." m2";
        $totLandFT = empty($datos['land'])?" ":number_format($datos['land'][0]['deed']*$datos['ft'], 2, '.', ',')." ft2";
        $plans     = empty($datos['land'])?" ":number_format($datos['land'][0]['plans'], 2, '.', ',')." m2";
        $plansFT   = empty($datos['land'])?" ":number_format($datos['land'][0]['plans']*$datos['ft'], 2, '.', ',')." ft2";
        $difLand   = empty($datos['land'])?" ":number_format($datos['land'][0]['deed']-$datos['land'][0]['plans'], 2, '.', ',')." m2";
        $difLandFT = empty($datos['land'])?" ":number_format(($datos['land'][0]['deed']-$datos['land'][0]['plans'])*$datos['ft'], 2, '.', ',')." ft2";
        $coreAPII .=  '<fieldset>                              
                              <div class="row">
                                <section class="col col-8"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                                                 'name'        => 'AIITit1',
                                                                                 'id'          => 'AIITit1',                                                                                 
                                                                                 'value'       => $datos['APII'][0]['AIITit1'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>
                                                           <label class="label" style="padding-top: 12px;">'.$this->creaLocation($datos['frontPage'][0]['calle'],$datos['frontPage'][0]['num'],$datos['frontPage'][0]['col'],$datos['frontPage'][0]['mun'],$datos['frontPage'][0]['edo'],$datos['frontPage'][0]['cp']).'</label>
                                </section>
                                </div>                               
                                <div class="row">
                                <section class="col col-10">Subject Location Image 1
                                                            <div id="fileUploaderAPII_1">&nbsp;Click para seleccionar Imagen&nbsp;</div>
                                                            <div id="statusAPII_1"></div>'.
                                                                 form_input(array('name'  => 'imgAPII_1', 
                                                                                  'type'  => 'hidden', 
                                                                                  'id'    => 'imgAPII_1',
                                                                                  'value' => $datos['APII'][0]['imgApII_1'])).'
                                                           <div id="divImgAPII_1">';        
                                                           if( !empty($datos['APII'][0]['imgApII_1']))
                                                           {
                                                            $coreAPII .= "<img src='".base_url()."gallery/".$datos['id_in']."/".$datos['APII'][0]['imgApII_1']."'><br><br>".
                                                                         "<img class=\"clImgAPII_1\" title=\"Borrar imagen\" name='".$datos['APII'][0]['imgApII_1']."'" .
                                                                         " id=\"".$datos['APII'][0]['imgApII_1']."\" src=\"".base_url()."/images/close.png\"><br>";
                                                           }
        $coreAPII .=                                       '</div>                                                               
                                 </section>
                                </div>
                                <div class="row">
                                <section class="col col-10"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                                                 'name'        => 'AIITit2',
                                                                                 'id'          => 'AIITit2',                                                                                 
                                                                                 'value'       => $datos['APII'][0]['AIITit2'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>                                                           
                                 </section>        
                                 <section class="col col-5"><label class="label">Latitude: </label>
                                                           <label class="input">'.
                                                                form_input(array('class' => 'validate[custom[number]] text-input',                                                                          
                                                                                    'name'  => 'AIIText2_1',
                                                                                    'id'    => 'AIIText2_1',
                                                                                    'value' => $datos['APII'][0]['AIIText2_1'] )).'
                                                            </label>
                                   </section>
                                 <section class="col col-5"><label class="label">Longitude: </label>
                                                           <label class="input">'.
                                                                form_input(array('class' => 'validate[custom[number]] text-input',                                                                          
                                                                                    'name'  => 'AIIText2_2',
                                                                                    'id'    => 'AIIText2_2',
                                                                                    'value' => $datos['APII'][0]['AIIText2_2'] )).'
                                                            </label>
                                </section>                                                            
                                </div>
                                <div class="row"><section class="col col-10">
                                                            Subject Location Image 2
                                                            <div id="fileUploaderAPII_2">&nbsp;Click para seleccionar Imagen&nbsp;</div>
                                                            <div id="statusAPII_2"></div>'.
                                                                 form_input(array('name'  => 'imgAPII_2', 
                                                                                  'type'  => 'hidden', 
                                                                                  'id'    => 'imgAPII_2',
                                                                                  'value' => $datos['APII'][0]['imgApII_2'])).'
                                                            <div id="divImgAPII_2">';
                                                           if( !empty($datos['APII'][0]['imgApII_2']))
                                                           {
                                                            $coreAPII .= "<img src='".base_url()."gallery/".$datos['id_in']."/".$datos['APII'][0]['imgApII_2']."'><br><br>".
                                                                         "<img class=\"clImgAPII_2\" title=\"Borrar imagen\" name='".$datos['APII'][0]['imgApII_2']."'" .
                                                                         " id=\"".$datos['APII'][0]['imgApII_2']."\" src=\"".base_url()."/images/close.png\"><br>";
                                                           }
        $coreAPII .=                                       '</div>                                                                                                                          
                                                </section>                                   
                                </div>
                                <div class="row">
                                 <section class="col col-8"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                                                 'name'        => 'AIITit3',
                                                                                 'id'          => 'AIITit3',                                                                                 
                                                                                 'value'       => $datos['APII'][0]['AIITit3'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>                                                           
                                 </section>
                                </div>
                                <div class="row">
                                 <section class="col col-10">        
                                  <table  cellspacing="0" cellpadding="0" width="80%">
                                       <tr><td class="apv leftCell">According to deeds: </td><td class="apv centerCell" >'.$totLand.'</td><td class="apv centerCell"> '.$totLandFT.'</td></tr>
                                       <tr><td class="apv leftCell">According to plans: </td><td class="apv centerCell" >'.$plans.'</td><td class="apv centerCell">'.$plansFT.'</td></tr>
                                       <tr><td class="leftCell">Difference              </td><td class="centerCell"     >'.$difLand.'</td><td class="centerCell">'.$difLandFT.'</td></tr>
                                   </table>    
                                 </section>
                                </div>
                                <div class="row">
                                 <section class="col col-10"><label class="input">'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                                                 'name'        => 'AIIText3',
                                                                                 'id'          => 'AIITextt3',                                                                                 
                                                                                 'value'       => $datos['APII'][0]['AIIText3'],
                                                                                 'maxlength'   => '80')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>                                                           
                                 </section>
                                 </div>
                                <div class="row">
                                <section class="col col-8"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                                                 'name'        => 'AIITit4',
                                                                                 'id'          => 'AIITit4',                                                                                 
                                                                                 'value'       => $datos['APII'][0]['AIITit4'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>                                                           
                                 </section>
                                 <section class="col col-10"><label class="input">'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                                                 'name'        => 'AIIText4',
                                                                                 'id'          => 'AIITextt4',                                                                                 
                                                                                 'value'       => $datos['APII'][0]['AIIText4'],
                                                                                 'maxlength'   => '80')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>                                                           
                                 </section>
                                 </div>
                                 <div class="row">
                                <section class="col col-8"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                                                 'name'        => 'AIITit5',
                                                                                 'id'          => 'AIITit5',                                                                                 
                                                                                 'value'       => $datos['APII'][0]['AIITit5'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>                                                           
                                 </section>        
                                 <section class="col col-10"><label class="textarea">'.
                                                                form_textarea(array('class' => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                                                    'rows'  => "3",
                                                                                    'name'  => 'AIIText5',
                                                                                    'id'    => 'AIIText5',
                                                                                    'value' => $datos['APII'][0]['AIIText5'] )).'
                                                            </label>
                                   </section>
                                </div>
                                <div class="row">
                                  <section class="col col-11">
                                  <table cellspacing="0" cellpadding="0" class="tblCA" width="100%">
                                   <thead><tr class="tblCA_int"><th class="centerCell">No </th>
                                           <th class="centerCell" >Area</th>';
                                           foreach ($datos['columns'] as $c)
                                            {  $coreAPII .= "<th class='centerCell'>".$c['type_area']." ".$c['name_area']."</th>" ; }
        $coreAPII .=               '        <th class="centerCell" >Total Area (sqm)</th>
                                         </tr>
                                    </thead>
                                    <tbody>';
                                       $rowId = 0;
                                       foreach ($datos['rows'] as $r)
                                        {  
                                        $rowId++;
                                        $subArea= 0;
                                        $columnId = 0;
                                        $coreAPII .='<tr class="tblCA_int"><td class="centerCell">'.$rowId.'    </td>
                                                         <td class="centerCell">'.$r['zone'].'</td>';
                                                        foreach ($datos['columns'] as $c)
                                                            {  $columnId++;
                                                               $dato      = $md_inmuebles->traCA($datos['id_in'],$columnId.$rowId);
                                                               $subArea = $subArea + $dato;
                                                               $celda     = empty($dato)?"":number_format($dato, 2, '.', ',');
                                                               $coreAPII .='<td class="centerCell" >'.$celda.'</td>';
                                                            }
                                        $coreAPII .='<td class="centerCell" >'.number_format($subArea, 2, '.', ',').'</td>'
                                                  . '</tr>';                                           
                                        }
        $coreAPII .=               '<tr><td class="centerCell"> </td>
                                           <td class="centerCell" > Sum </td>';
                                           $columnId = 0;
                                           $subTotal = 0;
                                           foreach ($datos['columns'] as $c)
                                            { $columnId++; 
                                            $st = $md_inmuebles->traSumCAXColumn($datos['id_in'],$columnId);
                                            $subTotal = $subTotal + $st;
                                            $coreAPII .= "<td class='centerCell'>".number_format($st, 2, '.', ',')."</td>" ;
                                            }
        $coreAPII .=               '        <td class="centerCell" >'.number_format($subTotal, 2, '.', ',').'</td>
                                    </tr>
                                    <tbody>
                                   </table>'.br(1).'
                                   <table cellspacing="0" cellpadding="0" width="100%">';
                                   $gfa = empty($datos['totArea'][0])?0:$datos['totArea'][0]['totCA'];
                                   $gba = empty($datos['totArea'][0])?0:$datos['totArea'][0]['totGBA'];
                                   $ra  = empty($datos['totArea'][0])?0:$datos['totArea'][0]['totTRA'];
        $coreAPII .=               '<tr><td class="apBottom leftCell">Ground Floor Area  </td><td class="apBottom centerCell">'.number_format($gfa, 2, '.', ',').' m2</td><td class="bottomLine centerCell">'.number_format($gfa*$datos['ft'], 2, '.', ',').' ft2</td></tr>
                                    <tr><td class="apBottom leftCell">Gross Building Area</td><td class="apBottom centerCell">'.number_format($gba, 2, '.', ',').' m2</td><td class="bottomLine centerCell">'.number_format($gba*$datos['ft'], 2, '.', ',').' ft2</td></tr>
                                    <tr><td class="apBottom leftCell">Rentable Area      </td><td class="apBottom centerCell">'.number_format($ra, 2, '.', ',').' m2</td><td class="bottomLine centerCell">'.number_format($ra*$datos['ft'], 2, '.', ',').' ft2</td></tr>
                                   </table>
                                  </section>
                                </div>                                
                                <div class="row">
                                <section class="col col-8"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                                                 'name'        => 'AIITit6',
                                                                                 'id'          => 'AIITit6',                                                                                 
                                                                                 'value'       => $datos['APII'][0]['AIITit6'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>                                                           
                                 </section>        
                                 <section class="col col-10"><label class="textarea">'.
                                                                form_textarea(array('class' => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                                                    'rows'  => "3",
                                                                                    'name'  => 'AIIText6',
                                                                                    'id'    => 'AIIText6',
                                                                                    'value' => $datos['APII'][0]['AIIText6'] )).'
                                                            </label>
                                   </section>
                                </div>
                                <div class="row">
                                <section class="col col-8"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                                                 'name'        => 'AIITit7',
                                                                                 'id'          => 'AIITit7',                                                                                 
                                                                                 'value'       => $datos['APII'][0]['AIITit7'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>                                                           
                                 </section>        
                                 <section class="col col-10"><label class="textarea">'.
                                                                form_textarea(array('class' => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                                                    'rows'  => "3",
                                                                                    'name'  => 'AIIText7_1',
                                                                                    'id'    => 'AIIText7_1',
                                                                                    'value' => $datos['APII'][0]['AIIText7_1'] )).'
                                                            </label>
                                   </section>
                                </div>                                
                                <div class="row"><section class="col col-10">
                                                            Property Plans Image 3
                                                            <div id="fileUploaderAPII_3">&nbsp;Click para seleccionar Imagen&nbsp;</div>
                                                            <div id="statusAPII_3"></div>'.
                                                                 form_input(array('name'  => 'imgAPII_3', 
                                                                                  'type'  => 'hidden', 
                                                                                  'id'    => 'imgAPII_3',
                                                                                  'value' => $datos['APII'][0]['imgApII_3'])).'
                                                            <div id="divImgAPII_3">';
                                                           if( !empty($datos['APII'][0]['imgApII_3']))
                                                           {
                                                            $coreAPII .= "<img src='".base_url()."gallery/".$datos['id_in']."/".$datos['APII'][0]['imgApII_3']."'><br><br>".
                                                                         "<img class=\"clImgAPII_3\" title=\"Borrar imagen\" name='".$datos['APII'][0]['imgApII_3']."'" .
                                                                         " id=\"".$datos['APII'][0]['imgApII_3']."\" src=\"".base_url()."/images/close.png\"><br> Property Plans";
                                                           }
        $coreAPII .=                                       '</div>                                                                                                                          
                                                </section>                                   
                                </div>
                                <div class="row">
                                <section class="col col-10"><label class="input">'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                                                 'name'        => 'AIIText7_2',
                                                                                 'id'          => 'AIIText7_2',                                                                                 
                                                                                 'value'       => $datos['APII'][0]['AIIText7_2'],
                                                                                 'maxlength'   => '100')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>
                                 </section>
                                 </div>
                                 <div class="row">
                                <section class="col col-10">
                                   <table cellspacing="0" cellpadding="0" width="100%">';
                                  if (sizeof($datos['ae'])>0)
                                  {
        $coreAPII .=              '<tr><td class="apBottom"></td><td class="apBottom" colspan="3">Equipment</td></tr>
                                   <tr><td class="apBottom centerCell" colspan="2">Item </td><td class="apBottom">Unit</td><td class="apBottom">Quantity</td></tr>';
                                   $x=0;
                                    foreach ($datos['ae'] as $ae) {
                                       $x++;                                      
                                      $coreAPII .= '<tr><td class="adprice">'.$x.'</td><td class="adprice leftCell">'.$ae['leyenda'].'</td><td class="adprice centerCell">'.$ae['unit'].'</td><td class="adprice leftCell">'.number_format($ae['amount'], 0, '.', ',').'</td></tr>';
                                   }
        $coreAPII .=              '<tr><td colspan="4"><br></td></tr>';
                                  }
                                  if (sizeof($datos['cw'])>0)
                                  {
        $coreAPII .=              '<tr><td class="apBottom"></td><td class="apBottom" colspan="3">Complementary works and special amenities</td></tr>
                                   <tr><td class="apBottom centerCell" colspan="2">Item </td><td class="apBottom">Unit</td><td class="apBottom">Quantity</td></tr>';
                                   $x=0;
                                    foreach ($datos['cw'] as $cw) {
                                       $x++;                                      
                                      $coreAPII .= '<tr><td class="adprice">'.$x.'</td><td class="adprice leftCell">'.$cw['leyenda'].'</td><td class="adprice centerCell">'.$cw['unit'].'</td><td class="adprice leftCell">'.number_format($cw['amount'], 0, '.', ',').'</td></tr>';
                                   }
                                  }
        $coreAPII .=              '</table>
                                </section>
                                 </div>
                      </fieldset>
                     ';
                 
        return $coreAPII;
        } catch (Exception $e) {echo 'creaCoreAPII Excepción: ',  $e, "\n";}  
    }
        
    public function creaCoreAPIII($datos) {
    try{$coreAPIII  = "";
        
        $coreAPIII .=  '<fieldset>                  
                                 <div class="row">                                
                                 <section class="col col-10"><label class="textarea">'.
                                                                form_textarea(array('class' => 'validate[required,custom[onlyLetterNumber]] text-input richTextBigAPIII',
                                                                                    'rows'  => "3",
                                                                                    'name'  => 'AIIIText1',
                                                                                    'id'    => 'AIIIText1',
                                                                                    'value' => $datos['APIII'][0]['AIIIText1'] )).'
                                                            </label>
                                   </section>
                                </div>                                                                      
                      </fieldset>
                     ';
                 
        return $coreAPIII;
        } catch (Exception $e) {echo 'creaCoreAPIII Excepción: ',  $e, "\n";} 
    }
    
    public function creaCoreAPIV($datos) {
    try{$coreAPIV  = "";
        
        $coreAPIV .=  '<fieldset>                  
                                 <div class="row">
                                <section class="col col-8"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                                                 'name'        => 'AIVTit1',
                                                                                 'id'          => 'AIVTit1',                                                                                 
                                                                                 'value'       => $datos['APIV'][0]['AIVTit1'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>                                                           
                                 </section>        
                                 <section class="col col-10"><label class="textarea">'.
                                                                form_textarea(array('class' => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                                                    'rows'  => "3",
                                                                                    'name'  => 'AIVText1',
                                                                                    'id'    => 'AIVText1',
                                                                                    'value' => $datos['APIV'][0]['AIVText1'] )).'
                                                            </label>
                                   </section>
                                </div>
                                <div class="row">
                                <section class="col col-8"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                                                 'name'        => 'AIVTit2',
                                                                                 'id'          => 'AIVTit2',                                                                                 
                                                                                 'value'       => $datos['APIV'][0]['AIVTit2'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>                                                           
                                 </section>        
                                 <section class="col col-10"><label class="textarea">'.
                                                                form_textarea(array('class' => 'validate[required,custom[onlyLetterNumber]] text-input richTextBigAPIV',
                                                                                    'rows'  => "3",
                                                                                    'name'  => 'AIVText2',
                                                                                    'id'    => 'AIVText2',
                                                                                    'value' => $datos['APIV'][0]['AIVText2'] )).'
                                                            </label>
                                   </section>
                                </div>
                                <div class="row">
                                <section class="col col-8"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                                                 'name'        => 'AIVTit3',
                                                                                 'id'          => 'AIVTit3',                                                                                 
                                                                                 'value'       => $datos['APIV'][0]['AIVTit3'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>                                                           
                                 </section>        
                                 <section class="col col-10"><label class="textarea">'.
                                                                form_textarea(array('class' => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                                                    'rows'  => "3",
                                                                                    'name'  => 'AIVText3',
                                                                                    'id'    => 'AIVText3',
                                                                                    'value' => $datos['APIV'][0]['AIVText3'] )).'
                                                            </label>
                                   </section>
                                </div>
                                <div class="row">
                                <section class="col col-8"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                                                 'name'        => 'AIVTit4',
                                                                                 'id'          => 'AIVTit4',                                                                                 
                                                                                 'value'       => $datos['APIV'][0]['AIVTit4'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>                                                           
                                 </section>        
                                 <section class="col col-10"><label class="textarea">'.
                                                                form_textarea(array('class' => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                                                    'rows'  => "3",
                                                                                    'name'  => 'AIVText4',
                                                                                    'id'    => 'AIVText4',
                                                                                    'value' => $datos['APIV'][0]['AIVText4'] )).'
                                                            </label>
                                   </section>
                                </div>                                                      
                      </fieldset>
                     ';
                 
        return $coreAPIV;
        } catch (Exception $e) {echo 'creaCoreAPIV Excepción: ',  $e, "\n";}  
    }    
    
    
    public function creaCoreAPV($datos,$md_inmuebles,$param) {
    try{
        $coreAPV         = "";        
        $uvUSD           = $datos['iv']['indicated_value_cls'];
        $plans           = empty($datos['land'])?0:$datos['land'][0]['plans'];
        $plansFT         = empty($datos['land'])?0:$datos['land'][0]['plans']*$datos['ft'];
        $totaValueLand   = $plans*$uvUSD;
        $totaValueLandFt = $plansFT*($uvUSD/$datos['ft']);
        $totTRA          = empty($datos['totArea'][0]['totTRA'])?0:$datos['totArea'][0]['totTRA'];
        
        $coreAPV .=  '<fieldset>                              
                             <div class="row">
                                <section class="col col-8"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetNumChar]] text-input',
                                                                                 'name'        => 'AVTit1',
                                                                                 'id'          => 'AVTit1',                                                                                 
                                                                                 'value'       => $datos['APV'][0]['AVTit1'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>                                                           
                                 </section>        
                                 <section class="col col-10"><label class="textarea">'.
                                                                form_textarea(array('class' => 'validate[required,custom[onlyLetNumChar]] text-input richTextBigAPIV',
                                                                                    'rows'  => "5",
                                                                                    'name'  => 'AVText1',
                                                                                    'id'    => 'AVText1',
                                                                                    'value' => $datos['APV'][0]['AVText1'] )).'
                                                            </label>
                                   </section>
                             </div>
                             <div class="row">
                                <section class="col col-8"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetNumChar]] text-input',
                                                                                 'name'        => 'AVTit2',
                                                                                 'id'          => 'AVTit2',                                                                                 
                                                                                 'value'       => $datos['APV'][0]['AVTit2'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>                                                           
                                 </section>        
                                 <section class="col col-10"><label class="textarea">'.
                                                                form_textarea(array('class' => 'validate[required,custom[onlyLetNumChar]] text-input richTextAPV',
                                                                                    'rows'  => "3",
                                                                                    'name'  => 'AVText2_1',
                                                                                    'id'    => 'AVText2_1',
                                                                                    'value' => $datos['APV'][0]['AVText2_1'] )).'
                                                            </label>
                                   </section>
                             </div>                             
                             <div class="row">
                                 <section class="col col-10">
                                  <table  cellspacing="0" cellpadding="0" width="100%">
                                       <tr><td class="apv leftCell">'.$param['tUV'].' </td><td class="apv centerCell">$'.number_format($uvUSD, 2, '.', ',').' USD/m2          </td><td class="apvSinNegrita centerCell">$'.number_format($uvUSD/$datos['ft'], 2, '.', ',').' USD/ft2       </td></tr>
                                       <tr><td class="apv leftCell">'.$param['tTot'].'</td><td class="apv centerCell">' .number_format($plans, 2, '.', ',').' m2</td><td class="apvSinNegrita centerCell">' .number_format($plansFT, 2, '.', ',').' ft2</td></tr>
                                       <tr><td class="apv leftCell">'.$param['tToV'].'</td><td class="apv centerCell">$'.number_format($totaValueLand, 2, '.', ',').'</td><td class="apvSinNegrita centerCell">$'.number_format($totaValueLandFt, 2, '.', ',').' </td></tr>
                                       <tr><td class="apv leftCell">'.$param['roun'].'</td><td class="apv centerCell">$'.number_format($this->redondearHermes($totaValueLand), 2, '.', ',').'</td><td class="centerCell"> </td></tr>
                                   </table>    
                                 </section>
                             </div>                             
                             <div class="row">
                                <section class="col col-8"><label class="input">'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetNumChar]] text-input',
                                                                                 'name'        => 'AVText2_2',
                                                                                 'id'          => 'AVText2_2',                                                                                 
                                                                                 'value'       => $datos['APV'][0]['AVText2_2'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Leyend</b>
                                                            </label>                                                           
                                 </section>
                            </div>
                            <div class="row">
                                <section class="col col-8"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetNumChar]] text-input',
                                                                                 'name'        => 'AVTit3',
                                                                                 'id'          => 'AVTit3',                                                                                 
                                                                                 'value'       => $datos['APV'][0]['AVTit3'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>                                                           
                                 </section>        
                                 <section class="col col-10"><label class="textarea">'.
                                                                form_textarea(array('class' => 'validate[required,custom[onlyLetNumChar]] text-input richTextBigAPIV',
                                                                                    'rows'  => "3",
                                                                                    'name'  => 'AVText3',
                                                                                    'id'    => 'AVText3',
                                                                                    'value' => $datos['APV'][0]['AVText3'] )).'
                                                            </label>
                                   </section>
                             </div>
                            <div class="row">
                                <section class="col col-8"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetNumChar]] text-input',
                                                                                 'name'        => 'AVTit4',
                                                                                 'id'          => 'AVTit4',                                                                                 
                                                                                 'value'       => $datos['APV'][0]['AVTit4'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>                                                           
                                 </section>
                                 <section class="col col-3"> <label for="sky-tab1-9" class="link pointer">'.img( array('src'=>base_url().'images/edit.png',"title"=>"Edit Construction Area of the Property ")).'</label></section>
                                 ';    
             $coreAPV .=  '  </div>
                            <div class="row">';
                           foreach ($datos['columns'] as $c)
                           {   $ucAd = ($c['uc_rcn'] * ($c['por_rcn']/100)) ;                                
                               $coreAPV .= "<table class='tableGrid' cellspacing='0' cellpadding='0' width='100%'>
                                                <tbody><tr><td class='apv leftCell' colspan='3'>".$c['type_area']." ".$c['name_area']."</td>
                                                           <td class='apv centerCell'>Progess                                </td>
                                                           <td class='apv centerCell'>".$param['tUV']."                              </td>
                                                      </tr>
                                                      <tr><td width='10%'>".nbs(1)."</td><td class='adprice' width='40%'>".$c['def_rcn']."       </td><td class='adprice' width='20%'>$".number_format($c['def_value_rcn'], 2, '.', ',')."</td><td class='adprice' colspan='2'>".nbs(1)."</td></tr>
                                                      <tr><td width='10%'>".nbs(1)."</td><td class='adprice' width='40%'>".$param['fbh']."</td><td class='adprice' width='20%'>".$c['fdh_rcn']."</td><td class='adprice' colspan='2'>".nbs(1)."     </td></tr>
                                                      <tr><td width='10%'>".nbs(1)."</td><td class='adprice' width='40%'>".$param['ffc']."</td><td class='adprice' width='20%'>".$c['fc_rcn']." </td><td class='adprice' colspan='2'>".nbs(1)."     </td></tr>
                                                      <tr><td width='10%'>".nbs(1)."</td><td class='adprice' width='40%'>".$param['uc']." </td><td class='adprice' width='20%'>$".number_format($c['uc_rcn'], 2, '.', ',')."       </td><td class='adprice'>".number_format($c['por_rcn'], 2, '.', ',')."%</td><td >$".number_format($ucAd, 2, '.', ',')."</td></tr>
                                               </tbody>
                                             </table> 
                                           ".br(2);
                           }
        $coreAPV .=  '      </div>
                            <div class="row">
                                <section class="col col-8"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetNumChar]] text-input',
                                                                                 'name'        => 'AVTit5',
                                                                                 'id'          => 'AVTit5',                                                                                 
                                                                                 'value'       => $datos['APV'][0]['AVTit5'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>                                                           
                                 </section>
                                 <section class="col col-3"> <label for="sky-tab1-9" class="link pointer">'.img( array('src'=>base_url().'images/edit.png',"title"=>"Edit Construction Area of the Property ")).'</label></section>
                            </div>
                            <div class="row">';
        $coreAPV .= "       <table class='tableGrid' cellspacing='0' cellpadding='0' width='100%'>
                                <tbody>                                       
                                      <tr><td class='apv leftCell' colspan='6'>".$param['mb']."</td></tr>
                                      <tr><td class='apv leftCell'>".$param['l12']."</td>
                                           <td class='apv centerCell'>".$param['l13']."</td>
                                           <td class='apv centerCell'>Unit</td>
                                           <td class='apv centerCell'>".$param['qt']."</td>
                                           <td class='apv centerCell'>".$param['uc']."<br> usd/m2<br>usd/ft2</td>
                                           <td class='apv centerCell'>".$param['rcn']."</td>
                                       </tr>";
                                        $x       = 0;
                                        $totArea = 0;
                                        $totRCN  = 0;
                                        foreach ($datos['columns'] as $c)
                                        {   $x++;
                                            $ucUSD        = $c['uc_rcn']/$datos['er'];
                                            $subTotColumn = $md_inmuebles->traSumCAXColumn($datos['id_in'],$c['column_Id']);
                                            $totArea      = $totArea + $subTotColumn;
                                            $totRCN       = $totRCN + $c['rcn'];

                                            $coreAPV .= "<tr><td class='adprice centerCell'>$x</td>
                                                             <td class='adprice leftCell'>".$c['type_area']." ".$c['name_area']."</td>
                                                             <td class='adprice centerCell'>    sqm <br> sqf</td>
                                                             <td class='adprice centerCell'>".number_format($subTotColumn, 2, '.', ',')." m2<br>".number_format($subTotColumn*$datos['ft'], 2, '.', ',')." ft2</td>
                                                             <td class='adprice centerCell'>$".number_format($ucUSD, 2, '.', ',')." <br> $".number_format($ucUSD/$datos['ft'], 2, '.', ',')."</td>
                                                             <td class='adprice centerCell'>$".number_format($c['rcn']/$datos['er'], 2, '.', ',')."</td>
                                                         </tr>";
                                        }
        $coreAPV .=  '          <tr><td class="apv centerCell" colspan="2"><strong>'.$param['trc'].'</strong></td>
                                    <td class="apv centerCell">    sqm <br> sqf</td>
                                    <td class="apv centerCell"            ><strong>'.number_format($totArea, 2, '.', ',').' m2</strong>'.br(1).number_format($totArea*$datos['ft'], 2, '.', ',').' ft2</td>
                                    <td class="apv centerCell"            >'.  nbs(1).'</td>
                  .                 <td class="apv centerCell"             ><strong>$'.number_format($totRCN/$datos['er'], 2, '.', ',').'</strong></td>
                               </tr>
                                </tbody>
                            </table> 
                         </div>
                         <div class="row">'.br(2);         /// equipment
        $coreAPV .= "       <table class='tableGrid' cellspacing='0' cellpadding='0' width='100%'>
                                <tbody><tr><td class='apv leftCell'> </td><td class='apv leftCell' colspan='6'>".$param['ae']."</td></tr>
                                       <tr><td class='apv leftCell'>".$param['l12']."</td>
                                           <td class='apv centerCell'>".$param['l13']."</td>
                                           <td class='apv centerCell'>Unit</td>
                                           <td class='apv centerCell'>".$param['qt']."</td>
                                           <td class='apv centerCell'>".$param['uc']."<br> usd/m2<br>usd/ft2</td>
                                           <td class='apv centerCell'>".$param['rcn']."</td>
                                       </tr>";
                                        $x        = 0;
                                        $totAERCN = 0;
                                        foreach ($datos['ae'] as $c)
                                        {   $x++;                                            
                                            $totAERCN = $totAERCN + $c['rcn'];
                                            $coreAPV .= "<tr><td class='adprice centerCell'>$x</td>
                                                             <td class='adprice leftCell'>".$c['leyenda']."</td>
                                                             <td class='adprice centerCell'>".$c['unit']."</td>
                                                             <td class='adprice centerCell'>".number_format($c['amount'])."</td>
                                                             <td class='adprice centerCell'>".number_format($c['unit_value'])."</td>
                                                             <td class='adprice centerCell'>$".number_format($c['rcn']/$datos['er'], 2, '.', ',')."</td>
                                                         </tr>";
                                        }
        $coreAPV .=  '                 <tr><td class="apv leftCell" colspan="5"><strong>'.$param['Tae'].'</strong></td>'
                . '                        <td class="apv rightCell"           ><strong>$'.number_format($totAERCN/$datos['er'], 2, '.', ',').'</strong></td>
                                       </tr>
                                       <tr><td class="apv leftCell" colspan="6">'.nbs(1).br(2).'</td></tr>
                                       <tr><td class="apv leftCell"> </td><td class="apv leftCell" colspan="6">'.$param['cw'].'</td></tr>
                                       <tr><td class="apv leftCell">'.$param['l12'].'</td>
                                           <td class="apv centerCell">'.$param['l13'].'</td>
                                           <td class="apv centerCell">Unit</td>
                                           <td class="apv centerCell">'.$param['qt'].'</td>
                                           <td class="apv centerCell">'.$param['uc'].'<br> usd/m2<br>usd/ft2</td>
                                           <td class="apv centerCell">'.$param['rcn'].'</td>
                                       </tr>';
                                        $x        = 0;
                                        $totCWRCN = 0;
                                        foreach ($datos['cw'] as $c)
                                        {   $x++;                                            
                                            $totCWRCN = $totCWRCN + $c['rcn'];
                                            $coreAPV .= "<tr><td class='adprice centerCell'>$x</td>
                                                             <td class='adprice leftCell'>".$c['leyenda']."</td>
                                                             <td class='adprice centerCell'>".$c['unit']."</td>
                                                             <td class='adprice centerCell'>".number_format($c['amount'])."</td>
                                                             <td class='adprice centerCell'>".number_format($c['unit_value'])."</td>
                                                             <td class='adprice centerCell'>$".number_format($c['rcn']/$datos['er'], 2, '.', ',')."</td>
                                                         </tr>";
                                        }
                                        $TOTALRCN = ($totCWRCN+$totAERCN+$totRCN);
        $coreAPV .=  '                 <tr><td class="apv leftCell" colspan="5"><strong>'.$param['Tcw'].'</strong></td>'
                . '                        <td class="apv rightCell"           ><strong>$'.number_format($totCWRCN/$datos['er'], 2, '.', ',').'</strong></td>
                                       </tr>
                                       <tr><td class="apv leftCell" colspan="5"><strong>'.$param['TaeTcw'].'</strong></td>'
                . '                        <td class="apv rightCell"           ><strong>$'.number_format( ($totCWRCN+$totAERCN)/$datos['er'], 2, '.', ',').'</strong></td>
                                       </tr>
                                       <tr><td class="apv leftCell" colspan="5"><strong>'.$param['TaeTcwC'].'</strong></td>'
                . '                        <td class="apv rightCell"           ><strong>$'.number_format($TOTALRCN/$datos['er'], 2, '.', ',').'</strong></td>
                                       </tr>                                       
                               </tbody>
                            </table>'.br(2).'
                            <div class="row">
                                <section class="col col-8"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetNumChar]] text-input',
                                                                                 'name'        => 'AVTit6',
                                                                                 'id'          => 'AVTit6',                                                                                 
                                                                                 'value'       => $datos['APV'][0]['AVTit6'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>                                                           
                                 </section>                                 
                            </div><br>
                            <div class="row">';
                            $cpd      = $this->creaTablaCPD($datos, $param);                            
                            $totalCPD = $cpd['total'];
        $coreAPV .=        $cpd['tabla'].br(1).'</div><br>
                            <div class="row">
                                <section class="col col-8"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetNumChar]] text-input',
                                                                                 'name'        => 'AVTit7',
                                                                                 'id'          => 'AVTit7',                                                                                 
                                                                                 'value'       => $datos['APV'][0]['AVTit7'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>                                                           
                                 </section>                                 
                            </div>                            
                            <div class="row">     
                                 <section class="col col-10"><label class="textarea">'.
                                                                form_textarea(array('class' => 'validate[required,custom[onlyLetNumChar]] text-input',
                                                                                    'rows'  => "3",
                                                                                    'name'  => 'AVText7_1',
                                                                                    'id'    => 'AVText7_1',
                                                                                    'value' => $datos['APV'][0]['AVText7_1'] )).'
                                                            </label>
                                </section>
                                <section class="col col-10"><label class="textarea">'.
                                                                form_textarea(array('class' => 'validate[required,custom[onlyLetNumChar]] text-input',
                                                                                    'rows'  => "3",
                                                                                    'name'  => 'AVText7_2',
                                                                                    'id'    => 'AVText7_2',
                                                                                    'value' => $datos['APV'][0]['AVText7_2'] )).'
                                                            </label>
                                </section>
                                <section class="col col-10"><label class="textarea">'.
                                                                form_textarea(array('class' => 'validate[required,custom[onlyLetNumChar]] text-input',
                                                                                    'rows'  => "3",
                                                                                    'name'  => 'AVText7_3',
                                                                                    'id'    => 'AVText7_3',
                                                                                    'value' => $datos['APV'][0]['AVText7_3'] )).'
                                                            </label>
                                </section>                                
                             </div>';
        /********************************* DEPRECIATION INI *******************************************************/
        $coreAPV .= "       <div class='row'>
                            <section class='col col-8'></section> <section class='col col-3'> <label for='sky-tab1-9' class='link pointer'>".img( array('src'=>base_url().'images/edit.png',"title"=>"Edit Construction Area of the Property "))."</label></section>
                             <table class='tableGrid' cellspacing='0' cellpadding='0' width='100%'>
                                <tbody><tr><td class='apv leftCell' colspan='7'>".$param['mb']."</td></tr>
                                        <tr><td class='apv centerCell'>".$param['l12']."</td>
                                           <td class='apv centerCell'>".$param['l13']."</td>
                                           <td class='apv centerCell'>".$param['rcn']."</td>                                               
                                           <td class='apv centerCell'>".$param['l14']."</td>
                                           <td class='apv centerCell'>".$param['l15']."</td>
                                           <td class='apv centerCell'>".$param['l16']."</td>
                                           <td class='apv centerCell'>".$param['l17']."</td>
                                       </tr>
                                       <tr><td class='apv leftCell' colspan='7'>".$param['l18']."</td></tr>";
                                        $x      = 0;
                                        $totDEP = 0;
                                        foreach ($datos['columns'] as $c)
                                         {   $x++;              
                                             $totDEP = $totDEP + $c['dvc_dep'];
                                             $coreAPV .= "<tr><td class='adprice centerCell'>".$x."</td>
                                                              <td class='adprice leftCell'  >".$c['type_area']." ".$c['name_area']."</td>
                                                              <td class='adprice centerCell'>$".number_format($c['rcn']/$datos['er'], 2, '.', ',')."</td>
                                                              <td class='adprice centerCell'>".$c['efage_dep']."</td>
                                                              <td class='adprice centerCell'>".$c['ul_dep']."</td>
                                                              <td class='adprice centerCell'>".number_format($c['af_dep']*100, 2, '.', ',')." %</td>
                                                              <td class='adprice rightCell' >$".number_format($c['dvc_dep']/$datos['er'], 2, '.', ',')."</td>
                                                          </tr>";
                                         }
        $coreAPV .= "                    <tr><td class='apv centerCell'>".nbs(1)."</td>
                                           <td class='apv leftCell'>".$param['mbTot']." (".$param['lt'].")</td>
                                           <td class='apv centerCell'>".nbs(1)."</td>
                                           <td class='apv centerCell'>".nbs(1)."</td>
                                           <td class='apv centerCell'>".nbs(1)."</td>
                                           <td class='apv centerCell'>".nbs(1)."</td>
                                           <td class='apv rightCell'>$".number_format($totDEP/$datos['er'], 2, '.', ',')."</td>
                                       </tr>
                                       <tr><td class=' leftCell' colspan='7'>".br(1)."</td></tr>
                                    <tr><td class='apv leftCell' colspan='7'>".$param['cw']."</td></tr>
                                    <tr><td class='apv centerCell'>".$param['l12']."</td>
                                        <td class='apv centerCell'>".$param['l13']."</td>
                                        <td class='apv centerCell'>".$param['rcn']."</td>
                                        <td class='apv centerCell'>".$param['l14']."</td>
                                        <td class='apv centerCell'>".$param['l15']."</td>
                                        <td class='apv centerCell'>".$param['l16']."</td>
                                        <td class='apv centerCell'>".$param['l17']."</td>
                                    </tr>";                    
                                    $x        = 0;
                                    $totCWDEP = 0;
                                    foreach ($datos['cw'] as $c)
                                     {   $x++;              
                                         $totCWDEP = $totCWDEP + $c['dvc_dep'];
                                          $coreAPV .= "<tr>   <td class='adprice centerCell'>".$x."</td>
                                                              <td class='adprice leftCell'  >".$c['leyenda']."</td>
                                                              <td class='adprice centerCell'>$".number_format($c['rcn']/$datos['er'], 2, '.', ',')."</td>
                                                              <td class='adprice centerCell'>".$c['efage_dep']."</td>
                                                              <td class='adprice centerCell'>".$c['ul_dep']."</td>
                                                              <td class='adprice centerCell'>".number_format($c['af_dep']*100, 2, '.', ',')." %</td>
                                                              <td class='adprice rightCell' >$".number_format($c['dvc_dep']/$datos['er'], 2, '.', ',')."</td>
                                                      </tr>";                                         
                                     }                        
        $coreAPV .= "               <tr><td class='apv centerCell'>".nbs(1)."</td>
                                        <td class='apv leftCell' colspan='5'>".$param['depTotCW']." (".$param['lt'].")"."</td>
                                        <td class='apv rightCell'>$".number_format($totCWDEP/$datos['er'], 2, '.', ',')."</td>
                                    </tr>
                                    <tr><td class='apv leftCell' colspan='7'>".br(2)."</td></tr>
                                    <tr><td class='apv centerCell'>".nbs(1)."</td>
                                        <td class='apv leftCell' colspan='5'>".$param['depTot']." (".$param['lt'].")"."</td>
                                        <td class='apv rightCell'>$".number_format(($totCWDEP+$totDEP)/$datos['er'], 2, '.', ',')."</td>
                                   </tr>
                            </tbody>
                           </table>
                        </div>
                        <div class='row'>".br(1)."
                             <table class='tableGrid' cellspacing='0' cellpadding='0' width='100%'>
                                <tbody>
                                    <tr><td class='apv leftCell' colspan='7'>".$param['ae']."</td></tr>
                                    <tr><td class='apv centerCell'>".$param['l12']."</td>
                                        <td class='apv centerCell'>".$param['l13']."</td>
                                        <td class='apv centerCell'>".$param['rcn']."</td>
                                        <td class='apv centerCell'>".$param['l14']."</td>
                                        <td class='apv centerCell'>".$param['l15']."</td>
                                        <td class='apv centerCell'>".$param['l16']."</td>
                                        <td class='apv centerCell'>".$param['l17']."</td>
                                    </tr> ";
                                    $x        = 0;
                                    $totAEDEP = 0;
                                    foreach ($datos['ae'] as $c)
                                     {   $x++;              
                                         $totAEDEP = $totAEDEP + $c['dvc_dep'];
                                         $coreAPV .= "<tr>    <td class='adprice centerCell'>".$x."</td>
                                                              <td class='adprice leftCell'  >".$c['leyenda']."</td>
                                                              <td class='adprice centerCell'>$".number_format($c['rcn']/$datos['er'], 2, '.', ',')."</td>
                                                              <td class='adprice centerCell'>".$c['efage_dep']."</td>
                                                              <td class='adprice centerCell'>".$c['ul_dep']."</td>
                                                              <td class='adprice centerCell'>".number_format($c['af_dep']*100, 2, '.', ',')." %</td>
                                                              <td class='adprice rightCell' >$".number_format($c['dvc_dep']/$datos['er'], 2, '.', ',')."</td>
                                                      </tr>";                                        
                                     }
        $coreAPV .= "               <tr><td class='apv centerCell'>".nbs(1)."</td>
                                        <td class='apv leftCell'>".$param['depTotAE']." (".$param['st'].")"."</td>
                                        <td class='apv centerCell'>".nbs(1)."</td>
                                        <td class='apv centerCell'>".nbs(1)."</td>
                                        <td class='apv centerCell'>".nbs(1)."</td>
                                        <td class='apv centerCell'>".nbs(1)."</td>
                                        <td class='apv rightCell'>$".number_format($totAEDEP/$datos['er'], 2, '.', ',')."</td>
                                    </tr>
                                </tbody>
                            </table><br>
                        </div>";
        /********************************* DEPRECIATION FIN *******************************************************/
        $coreAPV .= '    <div class="row">
                                <section class="col col-8"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetNumChar]] text-input',
                                                                                 'name'        => 'AVTit8',
                                                                                 'id'          => 'AVTit8',                                                                                 
                                                                                 'value'       => $datos['APV'][0]['AVTit8'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>                                                           
                                 </section>        
                                 <section class="col col-10"><label class="textarea">'.
                                                                form_textarea(array('class' => 'validate[required,custom[onlyLetNumChar]] text-input richTextAPV',
                                                                                    'rows'  => "3",
                                                                                    'name'  => 'AVText8_1',
                                                                                    'id'    => 'AVText8_1',
                                                                                    'value' => $datos['APV'][0]['AVText8_1'] )).'
                                                            </label>
                                   </section>
                        </div>
                        <div class="row">
                                 <section class="col col-5"><label class="label">'.$datos['APV'][0]['AVTit8'].' </label> </section>
                                 <section class="col col-2"><label class="label"> </label> </section>
                                 <section class="col col-3"><label class="input"><i class="icon-append fa fa-percent"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[number]] text-input',
                                                                                 'name'        => 'AVText8_2',
                                                                                 'id'          => 'AVText8_2',                                                                                 
                                                                                 'value'       => $datos['APV'][0]['AVText8_2'],
                                                                                 'maxlength'   => '6')).'
                                                                 <b class="tooltip tooltip-bottom-right">Ingresar Porcentaje</b>
                                                            </label>
                                 </section>
                        </div>';
                       $por_Ent      = $TOTALRCN * ($datos['APV'][0]['AVText8_2']/100);
                       $tblFinalRCN  = ($TOTALRCN + $por_Ent)/$datos['er'];
                       $tpd          = ($totAEDEP + $totCWDEP + $totDEP + $totalCPD);
                       $cpdTot       = ($totalCPD+$tpd);
                       
                       /****************************** TABLA FINAL COST APPROACH  ***********************************/                                                                               
        $coreAPV .= '    <div class="row">
                                 <section class="col col-10">'.br(1).'
                                    <table class="tableGrid" cellspacing="0" cellpadding="0" width="100%">
                                        <tbody>
                                            <tr><td class="apvThinSinNegrita negrita leftCell" colspan="3" width="75%">'.$param['trc'].'</td>
                                                <td class="apvThinSinNegrita negrita leftCell" width="25%"            >$'.number_format($TOTALRCN/$datos['er'], 2, '.', ',').'</td>
                                            </tr>
                                            <tr><td class="apvThinSinNegrita negrita leftCell" colspan="2" width="50%">'.$datos['APV'][0]['AVTit8'].'</td>
                                                <td class="apvThinSinNegrita negrita centerCell" width="25%"           >'.number_format(empty($datos['APV'][0]['AVText8_2'])?0:$datos['APV'][0]['AVText8_2'], 2, '.', ',').' %</td>
                                                <td class="apvThinSinNegrita negrita leftCell"   width="25%"        >$'.number_format($por_Ent/$datos['er'], 2, '.', ',').'</td>
                                            </tr>
                                            <tr><td class="apvThinSinNegrita negrita rightCell" colspan="2" width="50%">'.$param['rcnFC'].'</td>
                                                <td class="apvThinSinNegrita negrita centerCell" width="25%"           > </td>
                                                <td class="apvThinSinNegrita negrita leftCell"   width="25%"         >$'.number_format(empty($tblFinalRCN)?0:$tblFinalRCN, 2, '.', ',').'</td>
                                            </tr>
                                            <tr><td class="negrita leftCell" colspan="4" width="100%">'.$param['ad'].'</td>
                                            </tr>';
                                            $cpdTitCant = $totalCPD/$datos['er'];
                                            $cpdTitPor  = empty($tblFinalRCN)?0: $cpdTitCant / $tblFinalRCN;          
        $coreAPV .= '                       <tr><td class="apvThinSinNegrita negrita rightCell" colspan="2" width="50%">'.$param['cpdTit'].'</td>
                                                <td class="apvThinSinNegrita centerCell"            width="25%">'. number_format($cpdTitPor*100, 2, '.', ',').' %</td>
                                                <td class="apvThinSinNegrita leftCell"              width="25%">$'.number_format($cpdTitCant   , 2, '.', ',').'</td>
                                            </tr>';
                                            $stCant = $totAEDEP/$datos['er'];
                                            $stPor  = empty($tblFinalRCN)?0: $stCant / $tblFinalRCN;          
        $coreAPV .= '                       <tr><td class="negrita leftCell" colspan="4" width="100%">'.$param['ipd'].'</td></tr>
                                            <tr><td class="apvThinSinNegrita rightCell" colspan="2 width="75%"">'.$param['st'].'</td>
                                                <td class="apvThinSinNegrita centerCell"           width="25%">'. number_format( $stPor*100, 2, '.', ',').' %</td>
                                                <td class="apvThinSinNegrita leftCell"             width="25%">$'.number_format( $stCant   , 2, '.', ',').'</td>
                                            </tr>';
                                            $ltCant = ($totCWDEP+$totDEP)/$datos['er'];
                                            $ltPor  = empty($tblFinalRCN)?0: $ltCant / $tblFinalRCN;          
        $coreAPV .= '                       <tr><td class="apvThinSinNegrita rightCell" colspan="2" width="50%">'.$param['lt'].'</td>
                                                <td class="apvThinSinNegrita centerCell"            width="25%">'. number_format( $ltPor*100, 2, '.', ',').' %</td>
                                                <td class="apvThinSinNegrita leftCell"              width="25%">$'.number_format( $ltCant, 2, '.', ',').'</td>
                                            </tr>';
                                            $tpdCant = $tpd/$datos['er'];
                                            $tpdPor  = empty($tblFinalRCN)?0: $tpdCant / $tblFinalRCN;          
        $coreAPV .= '                       <tr><td class="apvThinSinNegrita negrita rightCell" colspan="2" width="50%">'.$param['tpd'].'</td>
                                                <td class="apvThinSinNegrita centerCell"            width="25%">'. number_format( $tpdPor*100, 2, '.', ',').' %</td>
                                                <td class="apvThinSinNegrita leftCell"              width="25%">$'.number_format( $tpdCant, 2, '.', ',').'</td>
                                            </tr>
                                            <tr><td class="apvThinSinNegrita negrita rightCell"  colspan="2" width="50%">'.$param['cfo'].'</td>
                                                <td class="apvThinSinNegrita centerCell" colspan="2" width="50%"></td>
                                            </tr>';
                                            $AVdfaCant    = $datos['APV'][0]['AVdfaNum']/$datos['er'];
                                            $AVdfaPor     = empty($tblFinalRCN)?0:$AVdfaCant/$tblFinalRCN;
                                            
                                            $AVdefSCant   = $datos['APV'][0]['AVdefSNum']/$datos['er'];
                                            $AVdefSPor    = empty($tblFinalRCN)?0:$AVdefSCant/$tblFinalRCN;
                                            
                                            $AVoaCant    = $datos['APV'][0]['AVoaNum']/$datos['er'];
                                            $AVoaPor     = empty($tblFinalRCN)?0:$AVoaCant/$tblFinalRCN;
                                            $totCFO      = ($datos['APV'][0]['AVdfaNum'] + $datos['APV'][0]['AVdefSNum'] + $datos['APV'][0]['AVoaNum'])/$datos['er'];
                                            $AVtotCfoPor = empty($tblFinalRCN)?0:$totCFO/$tblFinalRCN;
        $coreAPV .= '                       <tr><td class="apvThinSinNegrita rightCell" colspan="2" width="50%">'.$param['dfA'].'</td>
                                                <td class="apvThinSinNegrita centerCell"            width="25%"><label id="AVdfaPorLbl">'.number_format($AVdfaPor*100, 2, '.', ',').'</label> %'.form_input(array('name'=>'AVdfaPor', 'type'=>'hidden','id'=>'AVdfaPor', 'value'=>$AVdfaPor)).'</td>
                                                <td class="apvThinSinNegrita leftCell"              width="25%">'.form_input(array('class'      => 'pmask conborde',
                                                                                                         'name'      => 'AVdfaNumMask',
                                                                                                         'id'        => 'AVdfaNumMask',
                                                                                                         'value'     => $datos['APV'][0]['AVdfaNum'],
                                                                                                         'onChange'  => 'calculaPorCFO(\'AVdfaPor\',this,'.$tblFinalRCN.','.$datos['er'].',\'\')',
                                                                                                         'size'      => '10',
                                                                                                         'maxlength' => '20')).' mxn <input type="hidden" class="cfo" name="AVdfaNum" id="AVdfaNum" value="'.$datos['APV'][0]['AVdfaNum'].'"><br>
                                                                                     $ <label id="AVdfaPorusd">'.number_format($AVdfaCant, 2, '.', ',').'</label> usd                        
                                                </td>
                                            </tr>
                                            <tr><td class="apvThinSinNegrita rightCell" colspan="2" width="50%">'.$param['defS'].'</td>
                                                <td class="apvThinSinNegrita centerCell"            width="25%"><label id="AVdefSPorLbl">'.number_format($AVdefSPor*100, 2, '.', ',').'</label> %'.form_input(array('name'=>'AVdefSPor', 'type'=>'hidden','id'=>'AVdefSPor', 'value'=>$AVdefSPor)).'</td>
                                                <td class="apvThinSinNegrita leftCell"              width="25%">'.form_input(array('class'      => 'pmask conborde',
                                                                                                         'name'      => 'AVdefSNumMask',
                                                                                                         'id'        => 'AVdefSNumMask',
                                                                                                         'value'     => $datos['APV'][0]['AVdefSNum'],
                                                                                                         'onChange'  => 'calculaPorCFO(\'AVdefSPor\',this,'.$tblFinalRCN.','.$datos['er'].',\'\')',
                                                                                                         'size'      => '10',
                                                                                                         'maxlength' => '20')).' mxn <input type="hidden" class="cfo" name="AVdefSNum" id="AVdefSNum" value="'.$datos['APV'][0]['AVdefSNum'].'"><br>
                                                                                     $ <label id="AVdefSPorusd">'.number_format($AVdefSCant, 2, '.', ',').'</label> usd
                                                </td>
                                            </tr>
                                            <tr><td class="apvThinSinNegrita rightCell" colspan="2" width="50%">'.$param['oa'].'</td>
                                                <td class="apvThinSinNegrita centerCell"            width="25%"><label id="AVoaPorLbl">'.number_format($AVoaPor*100, 2, '.', ',').'</label> %'.form_input(array('name'=>'AVoaPor', 'type'=>'hidden','id'=>'AVoaPor', 'value'=>$AVoaPor)).'</td>
                                                <td class="apvThinSinNegrita leftCell"              width="25%">'.form_input(array('class'      => 'pmask conborde',
                                                                                                                   'name'      => 'AVoaNumMask',
                                                                                                                   'id'        => 'AVoaNumMask',
                                                                                                                   'value'     => $datos['APV'][0]['AVoaNum'],
                                                                                                                   'onChange'  => 'calculaPorCFO(\'AVoaPor\',this,'.$tblFinalRCN.','.$datos['er'].',\'\')',
                                                                                                                   'size'      => '10',
                                                                                                                   'maxlength' => '20')).' mxn <input type="hidden" class="cfo" name="AVoaNum" id="AVoaNum" value="'.$datos['APV'][0]['AVoaNum'].'"><br>
                                                                                     $ <label id="AVoaPorusd">'.number_format($AVoaCant, 2, '.', ',').'</label> usd
                                                </td>
                                            </tr>
                                            <tr><td class="apvThinSinNegrita negrita rightCell" colspan="2" width="50%">'.$param['tcfo'].'</td>
                                                <td class="apvThinSinNegrita centerCell"            width="25%"><label id="AVtotCFOPor">'.number_format($AVtotCfoPor*100, 2, '.', ',').'</label> %</td>
                                                <td class="apvThinSinNegrita leftCell"              width="25%">$<label id="AVtotCFO">'.number_format($totCFO*$datos['er'], 2, '.', ',').'</label> mxn <br>
                                                                                     $<label id="AVtotCFOusd">'.number_format($totCFO, 2, '.', ',').'</label> usd   </td>
                                            </tr>
                                            <tr><td class="apvThinSinNegrita negrita rightCell"  colspan="2" width="50%">'.$param['ifo'].'</td>
                                                <td class="apvThinSinNegrita centerCell" colspan="2" width="50%"></td>
                                            </tr>';
                                            $AVdefyCant        = $datos['APV'][0]['AVdefyNum']/$datos['er'];
                                            $AVdefyPor         = empty($tblFinalRCN)?0:$AVdefyCant/$tblFinalRCN;
                                            
                                            $AVoAdCant         = $datos['APV'][0]['AVoAdNum']/$datos['er'];
                                            $AVoAdPor          = empty($tblFinalRCN)?0:$AVoAdCant/$tblFinalRCN;
                                            
                                            $totCFOIn          = ($AVdefyCant + $AVoAdCant);
                                            $AVtotCfoPorIn     = empty($tblFinalRCN)?0:$totCFOIn/$tblFinalRCN;                                                                                        
                                            
                                            $externObs         = $this->calculaEO(array('tvl'     => $totaValueLand,
                                                                                        'rcn'     => $tblFinalRCN,
                                                                                        'tpd'     => $tpdCant,
                                                      
                                                      'cr'      => empty($datos['income'][0]['AVIcap_rate'])?0:$datos['income'][0]['AVIcap_rate'],
                                                                                        
                                                                                        'ex'      => empty($datos['income'][0]['AVI_totExp'])?0:$datos['income'][0]['AVI_totExp'],
                                                                                        'vcl'     => empty($datos['income'][0]['AVIoe_unf'])?0:$datos['income'][0]['AVIoe_unf'],
                                                                                        'ivLease' => empty($datos['iv']['indicated_value_crl'])?0:$datos['iv']['indicated_value_crl'],
                                                                                        'tra'     => $totTRA,
                                                                                       ));
                                            
                                            $totalDepreciation = $tpdCant + $totCFO + $totCFOIn + $externObs['eo19'];
                                            $vica              = ($tblFinalRCN-$totalDepreciation) + $totaValueLand;
                                            
        $coreAPV .= '                       <tr><td class="apvThinSinNegrita rightCell" colspan="2" width="50%">'.$param['defy'].'</td>
                                                <td class="apvThinSinNegrita centerCell"            width="25%"><label id="AVdefyPorLbl">'.number_format($AVdefyPor*100, 2, '.', ',').'</label> %'.form_input(array('name'=>'AVdefyPor', 'type'=>'hidden','id'=>'AVdefyPor', 'value'=>$AVdefyPor)).'</td>
                                                <td class="apvThinSinNegrita leftCell"              width="25%">'.form_input(array('class' => 'pmask conborde',
                                                                                                         'name'      => 'AVdefyNumMask',
                                                                                                         'id'        => 'AVdefyNumMask',
                                                                                                         'value'     => $datos['APV'][0]['AVdefyNum'],
                                                                                                         'onChange'  => 'calculaPorCFO(\'AVdefyPor\',this,'.$tblFinalRCN.','.$datos['er'].',\'In\')',
                                                                                                         'size'      => '10',
                                                                                                         'maxlength' => '20')).' mxn <input type="hidden" class="cfoIn" name="AVdefyNum" id="AVdefyNum" value="'.$datos['APV'][0]['AVdefyNum'].'"><br>
                                                                                     $ <label id="AVdefyPorusd">'.number_format($AVdefyCant, 2, '.', ',').'</label> usd
                                                </td>
                                            </tr>
                                            <tr><td class="apvThinSinNegrita rightCell" colspan="2" width="50%">'.$param['oAd'].'</td>
                                                <td class="apvThinSinNegrita centerCell"            width="25%"><label id="AVoAdPorLbl">'.number_format($AVoAdPor*100, 2, '.', ',').'</label> %'.form_input(array('name'=>'AVoAdPor', 'type'=>'hidden','id'=>'AVoAdPor', 'value'=>$AVoAdPor)).'</td>
                                                <td class="apvThinSinNegrita leftCell"              width="25%">'.form_input(array('class' => 'pmask conborde',
                                                                                                         'name'      => 'AVoAdNumMask',
                                                                                                         'id'        => 'AVoAdNumMask',
                                                                                                         'value'     => $datos['APV'][0]['AVoAdNum'],
                                                                                                         'onChange'  => 'calculaPorCFO(\'AVoAdPor\',this,'.$tblFinalRCN.','.$datos['er'].',\'In\')',
                                                                                                         'size'      => '10',
                                                                                                         'maxlength' => '20')).' mxn <input type="hidden" class="cfoIn" name="AVoAdNum" id="AVoAdNum" value="'.$datos['APV'][0]['AVoAdNum'].'"><br>
                                                                                     $ <label id="AVoAdPorusd">'.number_format($AVoAdCant, 2, '.', ',').'</label> usd
                                                </td>
                                            </tr>
                                            <tr><td class="apvThinSinNegrita negrita rightCell" colspan="2" width="50%">'.$param['tcfoIn'].'</td>
                                                <td class="apvThinSinNegrita centerCell"            width="25%"><label id="AVtotCFOInPor">'.number_format($AVtotCfoPorIn*100, 2, '.', ',').'</label> %</td>
                                                <td class="apvThinSinNegrita leftCell"              width="25%">$<label id="AVtotCFOIn">'.  number_format($totCFOIn*$datos['er'], 2, '.', ',').'</label> mxn <br>
                                                                                     $<label id="AVtotCFOInusd">'.number_format($totCFOIn, 2, '.', ',').'</label> usd   </td>
                                            </tr>
                                            <tr><td class="apvThinSinNegrita negrita rightCell" colspan="2" width="50%">'.$datos['APV'][0]['AVTit10'].form_input(array('name'=>'AV_oe', 'type'=>'hidden', 'id'=>'AV_oe', 'value'=> $externObs['eo19'] )).'</td>
                                                <td class="apvThinSinNegrita centerCell"            width="25%"><label id="APVEO">'.number_format(empty($tblFinalRCN)?0:($externObs['eo19']/$tblFinalRCN)*100, 2, '.', ',').'</label> %</td>
                                                <td class="apvThinSinNegrita leftCell"              width="25%">$'.number_format($externObs['eo19'], 2, '.', ',').'
                                                </td>
                                            </tr>                                            
                                            <tr><td class="apvThinSinNegrita negrita rightCell" colspan="2" width="50%">'.$param['depTot'].'</td>
                                                <td class="apvThinSinNegrita centerCell"            width="25%">'.number_format(empty($tblFinalRCN)?0:($totalDepreciation/$tblFinalRCN)*100, 2, '.', ',').' %</td>
                                                <td class="apvThinSinNegrita leftCell"              width="25%">$'.number_format($totalDepreciation, 2, '.', ',').'
                                                </td>
                                            </tr>
                                            <tr><td class="apvThinSinNegrita negrita rightCell" colspan="2" width="50%">'.$param['dvc'].'</td>
                                                <td class="apvThinSinNegrita centerCell"            width="25%">'.number_format(empty($tblFinalRCN)?0:(($tblFinalRCN-$totalDepreciation)/$tblFinalRCN)*100, 2, '.', ',').' %</td>
                                                <td class="apvThinSinNegrita leftCell"              width="25%">$'.number_format($tblFinalRCN-$totalDepreciation, 2, '.', ',').'
                                                </td>
                                            </tr>
                                            <tr><td class="apvThinSinNegrita negrita rightCell" colspan="2" width="50%">'.$param['lv'].'</td>
                                                <td class="apvThinSinNegrita centerCell"            width="25%">  </td>
                                                <td class="apvThinSinNegrita leftCell"              width="25%">$'.number_format($totaValueLand, 2, '.', ',').'
                                                </td>
                                            </tr>
                                            <tr><td class="apvThinSinNegrita negrita rightCell" colspan="2" width="50%">'.$param['vica'].form_input(array('name'=>'AV_vica', 'type'=>'hidden', 'id'=>'AV_vica', 'value'=> $vica )).'</td>
                                                <td class="apvThinSinNegrita centerCell"            width="25%">  </td>
                                                <td class="apvThinSinNegrita leftCell"              width="25%">$'.number_format($vica, 2, '.', ',').'
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>'.br(1).'
                                </section>
                        </div>
                        <div class="row">
                                 <section class="col col-7"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetNumChar]] text-input',
                                                                                 'name'        => 'AVTit9',
                                                                                 'id'          => 'AVTit9',                                                                                 
                                                                                 'value'       => $datos['APV'][0]['AVTit9'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>
                                 </section>                                         
                                 <section class="col col-3"><label class="label">$'.number_format($this->redondearHermes($vica), 2, '.', ',').'</label>
                                 </section>
                        </div>';
        $coreAPV .=    "<div class='row'><section class='col col-8'><a class='button' href='javascript:recalculaAPV_VI(baseURL,tipoAccion,\"APV\",".$datos['id_in'].",\"\");'>Recalculate</a></section><section id='confirmRecalculateAPV' class='col col-4'></section></div>";
        $coreAPV .=    '<div class="row">
                                <section class="col col-8"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetNumChar]] text-input',
                                                                                 'name'        => 'AVTit10',
                                                                                 'id'          => 'AVTit10',                                                                                 
                                                                                 'value'       => $datos['APV'][0]['AVTit10'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>                                                           
                                 </section>        
                                 <section class="col col-10"><label class="textarea">'.
                                                                form_textarea(array('class' => 'validate[required,custom[onlyLetNumChar]] text-input richTextAPV',
                                                                                    'rows'  => "3",
                                                                                    'name'  => 'AVText10',
                                                                                    'id'    => 'AVText10',
                                                                                    'value' => $datos['APV'][0]['AVText10'] )).'
                                                            </label>
                                </section>
                        </div>';        
        $coreAPV .=    '<div class="row">
                                 <section class="col col-10">'.br(1).'
                                    <table class="tableGrid" cellspacing="0" cellpadding="0" width="75%">
                                        <tbody>
                                            <tr><td class="apvThinSinNegrita leftCell" >'.$param['eo1'].'</td>
                                                <td class="apvThinSinNegrita rightCell">$'.number_format($externObs['eo1'], 2, '.', ',').'</td>
                                            </tr>
                                            <tr><td class="apvThinSinNegrita leftCell" >'.$param['eo2'].'</td>
                                                <td class="apvThinSinNegrita rightCell">'.number_format($externObs['eo2'], 2, '.', ',').' %</td>
                                            </tr>
                                            <tr><td class="apvThinSinNegrita leftCell" >'.$param['eo3'].'</td>
                                                <td class="apvThinSinNegrita rightCell">$'.number_format($externObs['eo3'], 2, '.', ',').'</td>
                                            </tr>
                                            <tr><td class="apvThinSinNegrita leftCell" >'.$param['eo4'].'</td>
                                                <td class="apvThinSinNegrita rightCell">'.number_format($externObs['eo4'], 2, '.', ',').' %</td>
                                            </tr>
                                            <tr><td class="apvThinSinNegrita leftCell" >'.$param['eo5'].'</td>
                                                <td class="apvThinSinNegrita rightCell">$'.number_format($externObs['eo5'], 2, '.', ',').'</td>
                                            </tr>
                                            <tr><td class="apvThinSinNegrita leftCell" >'.$param['eo6'].'</td>
                                                <td class="apvThinSinNegrita rightCell">'.number_format($externObs['eo6'], 2, '.', ',').' %</td>
                                            </tr>
                                            <tr><td class="apvThinSinNegrita leftCell" >'.$param['eo7'].'</td>
                                                <td class="apvThinSinNegrita rightCell">$'.number_format($externObs['eo7'], 2, '.', ',').'</td>
                                            </tr>
                                            <tr><td class="apvThinSinNegrita leftCell" >'.$param['eo8'].'</td>
                                                <td class="apvThinSinNegrita rightCell">$'.number_format($externObs['eo8'], 2, '.', ',').'</td>
                                            </tr>
                                            <tr><td class="apvThinSinNegrita leftCell" >'.$param['eo9'].'</td>
                                                <td class="apvThinSinNegrita rightCell">$'.number_format($externObs['eo9'], 2, '.', ',').'</td>
                                            </tr>
                                            <tr><td class="apvThinSinNegrita leftCell" >'.$param['eo10'].'</td>
                                                <td class="apvThinSinNegrita rightCell">$'.number_format($externObs['eo10'], 2, '.', ',').'</td>
                                            </tr>
                                            <tr><td class="apvThinSinNegrita leftCell" >'.$param['eo11'].'</td>
                                                <td class="apvThinSinNegrita rightCell"> </td>
                                            </tr>
                                            <tr><td class="apvThinSinNegrita leftCell" >'.$param['eo12'].'</td>
                                                <td class="apvThinSinNegrita rightCell">$'.number_format($externObs['eo12'], 2, '.', ',').'</td>
                                            </tr>
                                            <tr><td class="apvThinSinNegrita leftCell" >'.$param['eo13'].'</td>
                                                <td class="apvThinSinNegrita rightCell">$'.number_format($externObs['eo13'], 2, '.', ',').'</td>
                                            </tr>
                                            <tr><td class="apvThinSinNegrita leftCell" >'.$param['eo14'].'</td>
                                                <td class="apvThinSinNegrita rightCell">$'.number_format($externObs['eo14'], 2, '.', ',').'</td>
                                            </tr>
                                            <tr><td class="apvThinSinNegrita leftCell" >'.$param['eo15'].'</td>
                                                <td class="apvThinSinNegrita rightCell">$'.number_format($externObs['eo15'], 2, '.', ',').'</td>
                                            </tr>
                                            <tr><td class="apvThinSinNegrita leftCell" >'.$param['eo16'].'</td>
                                                <td class="apvThinSinNegrita rightCell">$'.number_format($externObs['eo16'], 2, '.', ',').'</td>
                                            </tr>
                                            <tr><td class="apvThinSinNegrita leftCell" >'.$param['eo17'].'</td>
                                                <td class="apvThinSinNegrita rightCell">$'.number_format($externObs['eo17'], 2, '.', ',').'</td>
                                            </tr>
                                            <tr><td class="apvThinSinNegrita leftCell" >'.$param['eo18'].'</td>
                                                <td class="apvThinSinNegrita rightCell">'.number_format($externObs['eo18'], 2, '.', ',').' %</td>
                                            </tr>
                                            <tr><td class="apvThinSinNegrita leftCell" >'.$param['eo19'].'</td>
                                                <td class="apvThinSinNegrita rightCell">$'.number_format($externObs['eo19'], 2, '.', ',').'</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </section>
                        </div>
                      </fieldset>
                     ';
                 
        return $coreAPV;
        
        } catch (Exception $e) {echo 'creaCoreAPV Excepción: ',  $e, "\n";} 
    }
    
    
    
    
    public function creaCoreAPVI($datos,$param) {
    try{$coreAPVI     = "";
        $ra           = empty($datos['totArea'][0])?0:$datos['totArea'][0]['totTRA'];
        $annualIncome = (($datos['iv']['indicated_value_crl']*12) * $ra);
        $coreAPVI    .=  '<fieldset>                  
                            <div class="row">
                                <section class="col col-8"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetNumChar]] text-input',
                                                                                 'name'        => 'AVITit1',
                                                                                 'id'          => 'AVITit1',                                                                                 
                                                                                 'value'       => $datos['APVI'][0]['AVITit1'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>                                                           
                                 </section>        
                                 <section class="col col-10"><label class="textarea">'.
                                                                form_textarea(array('class' => 'validate[required,custom[onlyLetNumChar]] text-input richTextAPVI',
                                                                                    'rows'  => "3",
                                                                                    'name'  => 'AVIText1',
                                                                                    'id'    => 'AVIText1',
                                                                                    'value' => $datos['APVI'][0]['AVIText1'] )).'
                                                            </label>
                                </section>
                            </div>
                            <div class="row">
                                <section class="col col-8"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetNumChar]] text-input',
                                                                                 'name'        => 'AVITit2',
                                                                                 'id'          => 'AVITit2',                                                                                 
                                                                                 'value'       => $datos['APVI'][0]['AVITit2'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>                                                           
                                 </section>        
                                 <section class="col col-10"><label class="textarea">'.
                                                                form_textarea(array('class' => 'validate[required,custom[onlyLetNumChar]] text-input',
                                                                                    'rows'  => "3",
                                                                                    'name'  => 'AVIText2',
                                                                                    'id'    => 'AVIText2',
                                                                                    'value' => $datos['APVI'][0]['AVIText2'] )).'
                                                            </label>
                                </section>
                            </div>
                            <div class="row">
                                <section class="col col-8"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetNumChar]] text-input',
                                                                                 'name'        => 'AVITit3',
                                                                                 'id'          => 'AVITit3',                                                                                 
                                                                                 'value'       => $datos['APVI'][0]['AVITit3'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>                                                           
                                 </section>        
                                 <section class="col col-10"><label class="textarea">'.
                                                                form_textarea(array('class' => 'validate[required,custom[onlyLetNumChar]] text-input',
                                                                                    'rows'  => "3",
                                                                                    'name'  => 'AVIText3',
                                                                                    'id'    => 'AVIText3',
                                                                                    'value' => $datos['APVI'][0]['AVIText3'] )).'
                                                            </label>
                                </section>
                            </div>
                            <div class="row">                            
                                 <section class="col col-10">
                                    <table class="tableIncome" cellspacing="0" cellpadding="0" width="100%">
                                    <tr><td class="apBottom leftCell">'.$param['ra'].'  </td><td class="apBottom centerCell">'.number_format($ra, 2, '.', ',').' m2</td><td class="bottomLine centerCell">'.number_format($ra*$datos['ft'], 2, '.', ',').' ft2</td></tr>
                                    <tr><td class="apBottom leftCell">'.$param['ugmi'].'</td><td class="apBottom centerCell">$ '.number_format($datos['iv']['indicated_value_crl'], 2, '.', ',').' usd/Month/m2</td><td class="bottomLine centerCell">$ '.number_format(($datos['iv']['indicated_value_crl']/$datos['ft'] * 12), 2, '.', ',').' usd/ft2/year</td></tr>
                                    <tr><td class="apBottom leftCell">'.$param['ugai'].'</td><td class="apBottom centerCell">$ '.number_format($datos['iv']['indicated_value_crl']*12, 2, '.', ',').' usd/year/m2</td><td class="bottomLine centerCell">$ '.number_format(($datos['iv']['indicated_value_crl']/$datos['ft'] * 12), 2, '.', ',').' usd/ft2/year</td></tr>
                                    </table><br>
                                 </section>   
                            </div>
                            <div class="row">
                                <section class="col col-8"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetNumChar]] text-input',
                                                                                 'name'        => 'AVITit4',
                                                                                 'id'          => 'AVITit4',
                                                                                 'value'       => $datos['APVI'][0]['AVITit4'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>
                                 </section>
                            </div>
                            <div class="row">                            
                                 <section class="col col-11"><br>
                                    <table class="tableIncome" cellspacing="0" cellpadding="0" width="100%">
                                    <tr><td class="apBottom leftCell">'.nbs(1).'       </td><td class="apBottom leftCell">'.$param['ra'].' (m2)              </td><td class="apBottom leftCell">'.$param['rent'].'                                                    </td><td class="apBottom leftCell">'.$param['anIn'].'</td></tr>
                                    <tr><td class="apBottom leftCell">'.$param['pgi'].'</td><td class="apBottom leftCell">'.number_format($ra, 2, '.', ',').'</td><td class="apBottom leftCell">$ '.number_format($datos['iv']['indicated_value_crl']*12, 2, '.', ',').' /m2/year</td><td class="apBottom leftCell">$'.number_format($annualIncome, 2, '.', ',').'</td></tr>
                                    <tr><td class=" leftCell" colspan="4">'.br(1).$param['err'].br(1).'</td></tr>
                                    <tr class="apvThinSinNegrita">    <td class=" leftCell" colspan="2">'.form_input(array('class'      => 'conborde validate[custom[onlyLetNumChar]] text-input',
                                                                                                                           'name'       => 'AVIpgi_1_Txt',
                                                                                                                           'id'         => 'AVIpgi_1_Txt',
                                                                                                                           'placeholder'=> $param['main'],
                                                                                                                           'value'      => $datos['APVI'][0]['AVIpgi_1_Txt'],
                                                                                                                           'size'       => '18',
                                                                                                                           'maxlength'  => '40')).'
                                            </td>
                                            <td class="leftCell">$ <label id="AVIpgi_1_Mon_mxnLbl">'.number_format( empty($datos['APVI'][0]['AVIpgi_1_Mon_mxn'])?0:$datos['APVI'][0]['AVIpgi_1_Mon_mxn'], 2, '.', ',').'</label> mxn<br>$ <label id="AVIpgi_1_Mon_usdLbl">'.number_format( empty($datos['APVI'][0]['AVIpgi_1_Mon_mxn'])?0:$datos['APVI'][0]['AVIpgi_1_Mon_mxn']/$datos['er'], 2, '.', ',').'</label> '.$param['currency'].''.form_input(array('name'=>'AVIpgi_1_Mon_mxn', 'type'=>'hidden', 'id'=>'AVIpgi_1_Mon_mxn', 'value'=> $datos['APVI'][0]['AVIpgi_1_Mon_mxn'] )).'</td>
                                            <td class="leftCell">'.form_input(array('class'      => 'pmask conborde',
                                                                                     'name'       => 'AVIpgi_1_Year_mxnMask',
                                                                                     'id'         => 'AVIpgi_1_Year_mxnMask',
                                                                                     'placeholder'=> 0.0,
                                                                                     'value'      => $datos['APVI'][0]['AVIpgi_1_Year_mxn'],
                                                                                     'onChange'   => 'calculaPGI(\'Year\', 1, this,'.$datos['er'].','.$datos['iv']['indicated_value_crl'].','.$ra.')',
                                                                                     'size'       => '10',
                                                                                     'maxlength'  => '20')).' mxn <input type="hidden" name="AVIpgi_1_Year_mxn" id="AVIpgi_1_Year_mxn" value="'.$datos['APVI'][0]['AVIpgi_1_Year_mxn'].'"><br>
                                                                          $ <label id="AVIpgi_1_Year_usd">'.number_format( empty($datos['APVI'][0]['AVIpgi_1_Year_mxn'])?0:$datos['APVI'][0]['AVIpgi_1_Year_mxn']/$datos['er'], 2, '.', ',').'</label> '.$param['currency'].'</td>
                                    </tr>
                                    <tr class="apvThinSinNegrita">    <td class=" leftCell" colspan="2">'.form_input(array('class'      => 'conborde validate[custom[onlyLetNumChar]] text-input',
                                                                                                                           'name'       => 'AVIpgi_2_Txt',
                                                                                                                           'id'         => 'AVIpgi_2_Txt',
                                                                                                                           'placeholder'=> $param['sec'],
                                                                                                                           'value'      => $datos['APVI'][0]['AVIpgi_2_Txt'],
                                                                                                                           'size'       => '18',
                                                                                                                           'maxlength'  => '40')).'
                                            </td>
                                            <td class="leftCell">$ <label id="AVIpgi_2_Mon_mxnLbl">'.number_format( empty($datos['APVI'][0]['AVIpgi_2_Mon_mxn'])?0:$datos['APVI'][0]['AVIpgi_2_Mon_mxn'], 2, '.', ',').'</label> mxn<br>$ <label id="AVIpgi_2_Mon_usdLbl">'.number_format( empty($datos['APVI'][0]['AVIpgi_2_Mon_mxn'])?0:$datos['APVI'][0]['AVIpgi_2_Mon_mxn']/$datos['er'], 2, '.', ',').'</label> '.$param['currency'].' '.form_input(array('name'=>'AVIpgi_2_Mon_mxn', 'type'=>'hidden', 'id'=>'AVIpgi_2_Mon_mxn', 'value'=> $datos['APVI'][0]['AVIpgi_2_Mon_mxn'] )).'</td>
                                            <td class="leftCell">'.form_input(array('class'      => 'pmask conborde',
                                                                                     'name'       => 'AVIpgi_2_Year_mxnMask',
                                                                                     'id'         => 'AVIpgi_2_Year_mxnMask',
                                                                                     'placeholder'=> 0.0,
                                                                                     'value'      => $datos['APVI'][0]['AVIpgi_2_Year_mxn'],
                                                                                     'onChange'   => 'calculaPGI(\'Year\', 2, this,'.$datos['er'].','.$datos['iv']['indicated_value_crl'].','.$ra.')',
                                                                                     'size'       => '10',
                                                                                     'maxlength'  => '20')).' mxn <input type="hidden" name="AVIpgi_2_Year_mxn" id="AVIpgi_2_Year_mxn" value="'.$datos['APVI'][0]['AVIpgi_2_Year_mxn'].'"><br>
                                                                          $ <label id="AVIpgi_2_Year_usd">'.number_format( empty($datos['APVI'][0]['AVIpgi_2_Year_mxn'])?0:$datos['APVI'][0]['AVIpgi_2_Year_mxn']/$datos['er'], 2, '.', ',').'</label> '.$param['currency'].'</td>
                                    </tr>
                                    <tr class="apvThinSinNegrita">    <td class=" leftCell" colspan="2">'.form_input(array('class'      => 'conborde validate[custom[onlyLetNumChar]] text-input',
                                                                                                                           'name'       => 'AVIpgi_3_Txt',
                                                                                                                           'id'         => 'AVIpgi_3_Txt',
                                                                                                                           'placeholder'=> '[Concept]',
                                                                                                                           'value'      => $datos['APVI'][0]['AVIpgi_3_Txt'],
                                                                                                                           'size'       => '18',
                                                                                                                           'maxlength'  => '40')).'
                                            </td>                                            
                                            <td class="leftCell">'.form_input(array('class'      => 'pmask conborde',
                                                                                     'name'       => 'AVIpgi_3_Mon_mxnMask',
                                                                                     'id'         => 'AVIpgi_3_Mon_mxnMask',
                                                                                     'placeholder'=> 0.0,
                                                                                     'value'      => $datos['APVI'][0]['AVIpgi_3_Mon_mxn'],
                                                                                     'onChange'   => 'calculaPGI(\'Mon\', 3, this,'.$datos['er'].','.$datos['iv']['indicated_value_crl'].','.$ra.')',
                                                                                     'size'       => '10',
                                                                                     'maxlength'  => '20')).' mxn <input type="hidden" name="AVIpgi_3_Mon_mxn" id="AVIpgi_3_Mon_mxn" value="'.$datos['APVI'][0]['AVIpgi_3_Mon_mxn'].'"><br>
                                                                          $ <label id="AVIpgi_3_Mon_usd">'.number_format( empty($datos['APVI'][0]['AVIpgi_3_Mon_mxn'])?0:$datos['APVI'][0]['AVIpgi_3_Mon_mxn']/$datos['er'], 2, '.', ',').'</label> '.$param['currency'].'</td>
                                            <td class="leftCell">$ <label id="AVIpgi_3_Year_mxnLbl">'.number_format( empty($datos['APVI'][0]['AVIpgi_3_Year_mxn'])?0:$datos['APVI'][0]['AVIpgi_3_Year_mxn'], 2, '.', ',').'</label> mxn<br>$ <label id="AVIpgi_3_Year_usdLbl">'.number_format( empty($datos['APVI'][0]['AVIpgi_3_Year_mxn'])?0:$datos['APVI'][0]['AVIpgi_3_Year_mxn']/$datos['er'], 2, '.', ',').'</label> '.$param['currency'].form_input(array('name'=>'AVIpgi_3_Year_mxn', 'type'=>'hidden', 'id'=>'AVIpgi_3_Year_mxn', 'value'=> $datos['APVI'][0]['AVIpgi_3_Year_mxn'] )).'</td>
                                    </tr>
                                    <tr class="apvThinSinNegrita">    <td class=" leftCell" colspan="2">'.form_input(array('class'      => 'conborde validate[custom[onlyLetNumChar]] text-input',
                                                                                                                           'name'       => 'AVIpgi_4_Txt',
                                                                                                                           'id'         => 'AVIpgi_4_Txt',
                                                                                                                           'placeholder'=> '[Concept]',
                                                                                                                           'value'      => $datos['APVI'][0]['AVIpgi_4_Txt'],
                                                                                                                           'size'       => '18',
                                                                                                                           'maxlength'  => '40')).'
                                            </td>                                            
                                            <td class="leftCell">'.form_input(array('class'      => 'pmask conborde',
                                                                                     'name'       => 'AVIpgi_4_Mon_mxnMask',
                                                                                     'id'         => 'AVIpgi_4_Mon_mxnMask',
                                                                                     'placeholder'=> 0.0,
                                                                                     'value'      => $datos['APVI'][0]['AVIpgi_4_Mon_mxn'],
                                                                                     'onChange'   => 'calculaPGI(\'Mon\', 4, this,'.$datos['er'].','.$datos['iv']['indicated_value_crl'].','.$ra.')',
                                                                                     'size'       => '10',
                                                                                     'maxlength'  => '20')).' mxn <input type="hidden" name="AVIpgi_4_Mon_mxn" id="AVIpgi_4_Mon_mxn" value="'.$datos['APVI'][0]['AVIpgi_4_Mon_mxn'].'"><br>
                                                                          $ <label id="AVIpgi_4_Mon_usd">'.number_format( empty($datos['APVI'][0]['AVIpgi_4_Mon_mxn'])?0:$datos['APVI'][0]['AVIpgi_4_Mon_mxn']/$datos['er'], 2, '.', ',').'</label> '.$param['currency'].'</td>
                                            <td class="leftCell">$ <label id="AVIpgi_4_Year_mxnLbl">'.number_format( empty($datos['APVI'][0]['AVIpgi_4_Year_mxn'])?0:$datos['APVI'][0]['AVIpgi_4_Year_mxn'], 2, '.', ',').'</label> mxn<br>$ <label id="AVIpgi_4_Year_usdLbl">'.number_format( empty($datos['APVI'][0]['AVIpgi_4_Year_mxn'])?0:$datos['APVI'][0]['AVIpgi_4_Year_mxn']/$datos['er'], 2, '.', ',').'</label> '.$param['currency'].form_input(array('name'=>'AVIpgi_4_Year_mxn', 'type'=>'hidden', 'id'=>'AVIpgi_4_Year_mxn', 'value'=> $datos['APVI'][0]['AVIpgi_4_Year_mxn'] )).'</td>
                                    </tr>';
                                    $serr  = $datos['APVI'][0]['AVIpgi_1_Year_mxn'] + $datos['APVI'][0]['AVIpgi_2_Year_mxn'] + $datos['APVI'][0]['AVIpgi_3_Year_mxn'] + $datos['APVI'][0]['AVIpgi_4_Year_mxn'];
                                    $tpgio = $serr + ($annualIncome * $datos['er']);
                                    $mvcl  = ($datos['APVI'][0]['AVIpgi_mvcl_por'] /100) * $tpgio;
                                    $egi   = $tpgio - $mvcl;
                    $coreAPVI .=  '<tr class="apvThinSinNegrita">    <td class=" leftCell" colspan="3">'.$param['serr'].'
                                            </td>                                            
                                            <td class="leftCell">$ <label id="AVIpgi_serr_mxnLbl">'.number_format( empty($serr)?0:$serr, 2, '.', ',').'             </label> mxn<br>'
                . '                                              $ <label id="AVIpgi_serr_usdLbl">'.number_format( empty($serr)?0:$serr/$datos['er'], 2, '.', ',').'</label> '.$param['currency'].'</td>
                                    </tr>
                                    <tr class="apvThinSinNegrita">    <td class=" leftCell" colspan="3">'.$param['tpgio'].'
                                            </td>                                            
                                            <td class="leftCell">$ <label id="AVIpgi_tpgio_mxnLbl">'.number_format( empty($tpgio)?0:$tpgio, 2, '.', ',').'             </label> mxn<br>'
                . '                                              $ <label id="AVIpgi_tpgio_usdLbl">'.number_format( empty($tpgio)?0:$tpgio/$datos['er'], 2, '.', ',').'</label> '.$param['currency'].'</td>
                                    </tr>
                                    <tr class="apvThinSinNegrita">    <td class=" leftCell">'.$param['mvcl'].'</td>
                                            <td class=" leftCell">'.form_input(array('class'      => 'conborde validate[custom[number]] text-input',
                                                                                     'name'       => 'AVIpgi_mvcl_por',
                                                                                     'id'         => 'AVIpgi_mvcl_por',
                                                                                     'placeholder'=> '0.0 %',
                                                                                     'value'      => $datos['APVI'][0]['AVIpgi_mvcl_por'],
                                                                                     'onChange'   => 'calculamvcl(this,'.$datos['er'].','.$tpgio.')',
                                                                                     'size'       => '5',
                                                                                     'maxlength'  => '4')).'%</td>
                                            <td class=" leftCell"></td>
                                            <td class="leftCell">$ <label id="AVIpgi_mvcl_mxnLbl">'.number_format( empty($mvcl)?0:$mvcl, 2, '.', ',').'             </label> mxn<br>'
                . '                                              $ <label id="AVIpgi_mvcl_usdLbl">'.number_format( empty($mvcl)?0:$mvcl/$datos['er'], 2, '.', ',').'</label> '.$param['currency'].'</td>
                                    </tr>
                                    <tr class="apBottom">    <td class=" leftCell" colspan="3">'.$param['egi'].'
                                            </td>                                            
                                            <td class="leftCell">$ <label id="AVIpgi_egi_mxnLbl">'.number_format( empty($egi)?0:$egi, 2, '.', ',').'             </label> mxn<br>'
                . '                                              $ <label id="AVIpgi_egi_usdLbl">'.number_format( empty($egi)?0:$egi/$datos['er'], 2, '.', ',').'</label> '.$param['currency'].'
                                                                 '.form_input(array('name'=>'egi', 'type'=>'hidden', 'id'=>'egi', 'value'=> $egi )).'
                                            </td>
                                    </tr>
                                    </table>'.  br(3);
                                    $eo1   = $datos['APVI'][0]['AVIoe_1_num2'];
                                    $eo2   = $datos['APVI'][0]['AVIoe_2_num1'] ;
                                    $eo3   = $datos['APVI'][0]['AVIoe_3_num1'] ;
                                    $eo4   = $datos['APVI'][0]['AVIoe_4_num1'] ;
                                    $eo5   = $datos['APVI'][0]['AVIoe_5_num1'] ;
                                    $eo6   = $datos['APVI'][0]['AVIoe_6_num1'] ;
                                    $eo7   = $datos['APVI'][0]['AVIoe_7_num1'] ;
                                    $eo8   = $datos['APVI'][0]['AVIoe_8_num1'] ;
                                    $eo9   = $datos['APVI'][0]['AVIoe_9_num1'] ;
                                    $eo10  = $datos['APVI'][0]['AVIoe_10_num2'];
                                    $eo11  = $datos['APVI'][0]['AVIoe_11_num2'];
                                    $egi   = empty($egi)?1:$egi;
                                    $div =                                     
                                    $sePor = $datos['APVI'][0]['AVIoe_1_num1'] + ($eo1/floatval($egi)) + $eo2 + $eo3 + $eo4 + $eo5 + $eo6 + $eo7 + $eo8 + $eo9 + ($eo10/floatval($egi)) +($eo11/floatval($egi));
                                    $sePor = $datos['APVI'][0]['AVIoe_1_num1'] +  $eo2 + $eo3 + $eo4 + $eo5 + $eo6 + $eo7 + $eo8 + $eo9 + ($eo10/floatval($egi)) +($eo11/floatval($egi));
                                   // $sePor = $datos['APVI'][0]['AVIoe_1_num1'];

                                    $seNum = $eo1 + $datos['APVI'][0]['AVIoe_2_num2'] + $datos['APVI'][0]['AVIoe_3_num2'] + $datos['APVI'][0]['AVIoe_4_num2'] + $datos['APVI'][0]['AVIoe_5_num2'] + $datos['APVI'][0]['AVIoe_6_num2'] + $datos['APVI'][0]['AVIoe_7_num2'] + $datos['APVI'][0]['AVIoe_8_num2'] + $datos['APVI'][0]['AVIoe_9_num2'] + $eo10 + $eo11;
                                    $unf   = ($datos['APVI'][0]['AVIoe_unf']/100) * $seNum;
                                    
                                    $totExpPor = $sePor + ( ($unf/$egi)*100 );
                                    $totExpNum = $seNum + $unf;
                                    $noi       = $egi - $totExpNum;

                                    //$datos['APVI'][0]['AVIoe_1_num1']
                                    
                    $coreAPVI .=  '<table class="tableOE" cellspacing="0" cellpadding="0" width="100%">
                                    <tr><td class="apBottom leftCell" colspan="3">'.$param['oe'].'</td></tr>
                                    <tr class="apvThinSinNegrita"> 
                                        <td class=" leftCell">'.form_input(array('class'      => 'conborde validate[custom[onlyLetNumChar]] text-input',
                                                                                 'name'       => 'AVIoe_1_Txt',
                                                                                 'id'         => 'AVIoe_1_Txt',
                                                                                 'placeholder'=> '[Concept]',
                                                                                 'value'      => $datos['APVI'][0]['AVIoe_1_Txt'],
                                                                                 'size'       => '18',
                                                                                 'maxlength'  => '40')).'
                                        </td>                                        
                                        <td class=" leftCell"> <label id="AVIoe_1_num1Lbl">'.number_format(empty($datos['APVI'][0]['AVIoe_1_num1'])?0:$datos['APVI'][0]['AVIoe_1_num1'], 2, '.', ',').'</label> %'.form_input(array('name'=>'AVIoe_1_num1','class'=>'oeNum1', 'type'=>'hidden', 'id'=>'AVIoe_1_num1', 'value'=> $datos['APVI'][0]['AVIoe_1_num1'] )).'</td>
                                        <td class=" leftCell">'.form_input(array('class'      => 'pmask conborde',
                                                                                 'name'       => 'AVIoe_1_num2Mask',
                                                                                 'id'         => 'AVIoe_1_num2Mask',
                                                                                 'placeholder'=> '0.0',
                                                                                 'value'      => $eo1,
                                                                                 'onChange'   => 'calculaOE_Num(this,\'1\','.$datos['er'].','.$egi.')',
                                                                                 'size'       => '15',                                                                                
                                                                                 'maxlength'  => '20')).' mxn <input type="hidden" class="oeNum2" name="AVIoe_1_num2" id="AVIoe_1_num2" value="'.$eo1.'"><br>
                                                                $ <label id="AVIoe_1_num2usd">'.number_format( empty($eo1)?0:$eo1/$datos['er'], 2, '.', ',').'</label> '.$param['currency'].'</td>
                                    </tr>
                                    <tr class="apvThinSinNegrita"> 
                                        <td class=" leftCell">'.form_input(array('class'      => 'conborde validate[custom[onlyLetNumChar]] text-input',
                                                                                 'name'       => 'AVIoe_2_Txt',
                                                                                 'id'         => 'AVIoe_2_Txt',
                                                                                 'placeholder'=> '[Concept]',
                                                                                 'value'      => $datos['APVI'][0]['AVIoe_2_Txt'],
                                                                                 'size'       => '18',
                                                                                 'maxlength'  => '40')).'
                                        </td>
                                        <td class=" leftCell"> '.form_input(array('class'     => 'oeNum1 conborde validate[custom[number]] text-input',
                                                                                 'name'       => 'AVIoe_2_num1',
                                                                                 'id'         => 'AVIoe_2_num1',
                                                                                 'placeholder'=> '0.0',
                                                                                 'value'      => $datos['APVI'][0]['AVIoe_2_num1'],
                                                                                 'size'       => '5',
                                                                                 'onChange'   => 'calculaOE_Por(this,\'2\','.$datos['er'].','.$egi.')',
                                                                                 'maxlength'  => '4')).' %</td>
                                        <td class=" leftCell">$ <label id="AVIoe_2_num2Lbl"   >'.number_format( empty($datos['APVI'][0]['AVIoe_2_num2'])?0:$datos['APVI'][0]['AVIoe_2_num2'], 2, '.', ',').'              </label> mxn<br>
                                                              $ <label id="AVIoe_2_num2Lblusd">'.number_format( empty($datos['APVI'][0]['AVIoe_2_num2'])?0:$datos['APVI'][0]['AVIoe_2_num2']/$datos['er'], 2, '.', ',').'</label> '.$param['currency'].'
                                                              '.form_input(array('name'=>'AVIoe_2_num2','class'=>'oeNum2', 'type'=>'hidden', 'id'=>'AVIoe_2_num2', 'value'=> $datos['APVI'][0]['AVIoe_2_num2'] )).'</td>
                                    </tr>
                                    <tr class="apvThinSinNegrita"> 
                                        <td class=" leftCell">'.form_input(array('class'      => 'conborde validate[custom[onlyLetNumChar]] text-input',
                                                                                 'name'       => 'AVIoe_3_Txt',
                                                                                 'id'         => 'AVIoe_3_Txt',
                                                                                 'placeholder'=> '[Concept]',
                                                                                 'value'      => $datos['APVI'][0]['AVIoe_3_Txt'],
                                                                                 'size'       => '18',
                                                                                 'maxlength'  => '40')).'
                                        </td>
                                        <td class=" leftCell"> '.form_input(array('class'      => 'oeNum1 conborde validate[custom[number]] text-input',
                                                                                 'name'       => 'AVIoe_3_num1',
                                                                                 'id'         => 'AVIoe_3_num1',
                                                                                 'placeholder'=> '0.0',
                                                                                 'value'      => $datos['APVI'][0]['AVIoe_3_num1'],
                                                                                 'size'       => '5',
                                                                                 'onChange'   => 'calculaOE_Por(this,\'3\','.$datos['er'].','.$egi.')',
                                                                                 'maxlength'  => '4')).' %</td>
                                        <td class=" leftCell">$ <label id="AVIoe_3_num2Lbl"   >'.number_format( empty($datos['APVI'][0]['AVIoe_3_num2'])?0:$datos['APVI'][0]['AVIoe_3_num2'], 2, '.', ',').'              </label> mxn<br>
                                                              $ <label id="AVIoe_3_num2Lblusd">'.number_format( empty($datos['APVI'][0]['AVIoe_3_num2'])?0:$datos['APVI'][0]['AVIoe_3_num2']/$datos['er'], 2, '.', ',').'</label> '.$param['currency'].'
                                                              '.form_input(array('name'=>'AVIoe_3_num2','class'=>'oeNum2', 'type'=>'hidden', 'id'=>'AVIoe_3_num2', 'value'=> $datos['APVI'][0]['AVIoe_3_num2'] )).'</td>
                                    </tr>
                                    <tr class="apvThinSinNegrita"> 
                                        <td class=" leftCell">'.form_input(array('class'      => 'conborde validate[custom[onlyLetNumChar]] text-input',
                                                                                 'name'       => 'AVIoe_4_Txt',
                                                                                 'id'         => 'AVIoe_4_Txt',
                                                                                 'placeholder'=> '[Concept]',
                                                                                 'value'      => $datos['APVI'][0]['AVIoe_4_Txt'],
                                                                                 'size'       => '18',
                                                                                 'maxlength'  => '40')).'
                                        </td>
                                        <td class=" leftCell"> '.form_input(array('class'      => 'oeNum1 conborde validate[custom[number]] text-input',
                                                                                 'name'       => 'AVIoe_4_num1',
                                                                                 'id'         => 'AVIoe_4_num1',
                                                                                 'placeholder'=> '0.0',
                                                                                 'value'      => $datos['APVI'][0]['AVIoe_4_num1'],
                                                                                 'size'       => '5',
                                                                                 'onChange'   => 'calculaOE_Por(this,\'4\','.$datos['er'].','.$egi.')',
                                                                                 'maxlength'  => '4')).' %</td>
                                        <td class=" leftCell">$ <label id="AVIoe_4_num2Lbl"   >'.number_format( empty($datos['APVI'][0]['AVIoe_4_num2'])?0:$datos['APVI'][0]['AVIoe_4_num2'], 2, '.', ',').'              </label> mxn<br>
                                                              $ <label id="AVIoe_4_num2Lblusd">'.number_format( empty($datos['APVI'][0]['AVIoe_4_num2'])?0:$datos['APVI'][0]['AVIoe_4_num2']/$datos['er'], 2, '.', ',').'</label> '.$param['currency'].'
                                                              '.form_input(array('name'=>'AVIoe_4_num2','class'=>'oeNum2', 'type'=>'hidden', 'id'=>'AVIoe_4_num2', 'value'=> $datos['APVI'][0]['AVIoe_4_num2'] )).'</td>
                                    </tr>
                                    <tr class="apvThinSinNegrita"> 
                                        <td class=" leftCell">'.form_input(array('class'      => 'conborde validate[custom[onlyLetNumChar]] text-input',
                                                                                 'name'       => 'AVIoe_5_Txt',
                                                                                 'id'         => 'AVIoe_5_Txt',
                                                                                 'placeholder'=> '[Concept]',
                                                                                 'value'      => $datos['APVI'][0]['AVIoe_5_Txt'],
                                                                                 'size'       => '18',
                                                                                 'maxlength'  => '40')).'
                                        </td>
                                        <td class=" leftCell"> '.form_input(array('class'      => 'oeNum1 conborde validate[custom[number]] text-input',
                                                                                 'name'       => 'AVIoe_5_num1',
                                                                                 'id'         => 'AVIoe_5_num1',
                                                                                 'placeholder'=> '0.0',
                                                                                 'value'      => $datos['APVI'][0]['AVIoe_5_num1'],
                                                                                 'size'       => '5',
                                                                                 'onChange'   => 'calculaOE_Por(this,\'5\','.$datos['er'].','.$egi.')',
                                                                                 'maxlength'  => '4')).' %</td>
                                        <td class=" leftCell">$ <label id="AVIoe_5_num2Lbl"   >'.number_format( empty($datos['APVI'][0]['AVIoe_5_num2'])?0:$datos['APVI'][0]['AVIoe_5_num2'], 2, '.', ',').'              </label> mxn<br>
                                                              $ <label id="AVIoe_5_num2Lblusd">'.number_format( empty($datos['APVI'][0]['AVIoe_5_num2'])?0:$datos['APVI'][0]['AVIoe_5_num2']/$datos['er'], 2, '.', ',').'</label> '.$param['currency'].'
                                                              '.form_input(array('name'=>'AVIoe_5_num2','class'=>'oeNum2', 'type'=>'hidden', 'id'=>'AVIoe_5_num2', 'value'=> $datos['APVI'][0]['AVIoe_5_num2'] )).'</td>
                                    </tr>
                                    <tr class="apvThinSinNegrita"> 
                                        <td class=" leftCell">'.form_input(array('class'      => 'conborde validate[custom[onlyLetNumChar]] text-input',
                                                                                 'name'       => 'AVIoe_6_Txt',
                                                                                 'id'         => 'AVIoe_6_Txt',
                                                                                 'placeholder'=> '[Concept]',
                                                                                 'value'      => $datos['APVI'][0]['AVIoe_6_Txt'],
                                                                                 'size'       => '18',
                                                                                 'maxlength'  => '40')).'
                                        </td>
                                        <td class=" leftCell"> '.form_input(array('class'      => 'oeNum1 conborde validate[custom[number]] text-input',
                                                                                 'name'       => 'AVIoe_6_num1',
                                                                                 'id'         => 'AVIoe_6_num1',
                                                                                 'placeholder'=> '0.0',
                                                                                 'value'      => $datos['APVI'][0]['AVIoe_6_num1'],
                                                                                 'size'       => '5',
                                                                                 'onChange'   => 'calculaOE_Por(this,\'6\','.$datos['er'].','.$egi.')',
                                                                                 'maxlength'  => '4')).' %</td>
                                        <td class=" leftCell">$ <label id="AVIoe_6_num2Lbl"   >'.number_format( empty($datos['APVI'][0]['AVIoe_6_num2'])?0:$datos['APVI'][0]['AVIoe_6_num2'], 2, '.', ',').'              </label> mxn<br>
                                                              $ <label id="AVIoe_6_num2Lblusd">'.number_format( empty($datos['APVI'][0]['AVIoe_6_num2'])?0:$datos['APVI'][0]['AVIoe_6_num2']/$datos['er'], 2, '.', ',').'</label> '.$param['currency'].'
                                                              '.form_input(array('name'=>'AVIoe_6_num2','class'=>'oeNum2', 'type'=>'hidden', 'id'=>'AVIoe_6_num2', 'value'=> $datos['APVI'][0]['AVIoe_6_num2'] )).'</td>
                                    </tr>
                                    <tr class="apvThinSinNegrita"> 
                                        <td class=" leftCell">'.form_input(array('class'      => 'conborde validate[custom[onlyLetNumChar]] text-input',
                                                                                 'name'       => 'AVIoe_7_Txt',
                                                                                 'id'         => 'AVIoe_7_Txt',
                                                                                 'placeholder'=> '[Concept]',
                                                                                 'value'      => $datos['APVI'][0]['AVIoe_7_Txt'],
                                                                                 'size'       => '18',
                                                                                 'maxlength'  => '40')).'
                                        </td>
                                        <td class=" leftCell"> '.form_input(array('class'      => 'oeNum1 conborde validate[custom[number]] text-input',
                                                                                 'name'       => 'AVIoe_7_num1',
                                                                                 'id'         => 'AVIoe_7_num1',
                                                                                 'placeholder'=> '0.0',
                                                                                 'value'      => $datos['APVI'][0]['AVIoe_7_num1'],
                                                                                 'size'       => '5',
                                                                                 'onChange'   => 'calculaOE_Por(this,\'7\','.$datos['er'].','.$egi.')',
                                                                                 'maxlength'  => '4')).' %</td>
                                        <td class=" leftCell">$ <label id="AVIoe_7_num2Lbl"   >'.number_format( empty($datos['APVI'][0]['AVIoe_7_num2'])?0:$datos['APVI'][0]['AVIoe_7_num2'], 2, '.', ',').'              </label> mxn<br>
                                                              $ <label id="AVIoe_7_num2Lblusd">'.number_format( empty($datos['APVI'][0]['AVIoe_7_num2'])?0:$datos['APVI'][0]['AVIoe_7_num2']/$datos['er'], 2, '.', ',').'</label> '.$param['currency'].'
                                                              '.form_input(array('name'=>'AVIoe_7_num2','class'=>'oeNum2', 'type'=>'hidden', 'id'=>'AVIoe_7_num2', 'value'=> $datos['APVI'][0]['AVIoe_7_num2'] )).'</td>
                                    </tr>
                                    <tr class="apvThinSinNegrita"> 
                                        <td class=" leftCell">'.form_input(array('class'      => 'conborde validate[custom[onlyLetNumChar]] text-input',
                                                                                 'name'       => 'AVIoe_8_Txt',
                                                                                 'id'         => 'AVIoe_8_Txt',
                                                                                 'placeholder'=> '[Concept]',
                                                                                 'value'      => $datos['APVI'][0]['AVIoe_8_Txt'],
                                                                                 'size'       => '18',
                                                                                 'maxlength'  => '40')).'
                                        </td>
                                        <td class=" leftCell"> '.form_input(array('class'      => 'oeNum1 conborde validate[custom[number]] text-input',
                                                                                 'name'       => 'AVIoe_8_num1',
                                                                                 'id'         => 'AVIoe_8_num1',
                                                                                 'placeholder'=> '0.0',
                                                                                 'value'      => $datos['APVI'][0]['AVIoe_8_num1'],
                                                                                 'size'       => '5',
                                                                                 'onChange'   => 'calculaOE_Por(this,\'8\','.$datos['er'].','.$egi.')',
                                                                                 'maxlength'  => '4')).' %</td>
                                        <td class=" leftCell">$ <label id="AVIoe_8_num2Lbl"   >'.number_format( empty($datos['APVI'][0]['AVIoe_8_num2'])?0:$datos['APVI'][0]['AVIoe_8_num2'], 2, '.', ',').'              </label> mxn<br>
                                                              $ <label id="AVIoe_8_num2Lblusd">'.number_format( empty($datos['APVI'][0]['AVIoe_8_num2'])?0:$datos['APVI'][0]['AVIoe_8_num2']/$datos['er'], 2, '.', ',').'</label> '.$param['currency'].'
                                                              '.form_input(array('name'=>'AVIoe_8_num2','class'=>'oeNum2', 'type'=>'hidden', 'id'=>'AVIoe_8_num2', 'value'=> $datos['APVI'][0]['AVIoe_8_num2'] )).'</td>
                                    </tr>
                                    <tr class="apvThinSinNegrita">
                                        <td class=" leftCell">'.form_input(array('class'      => 'conborde validate[custom[onlyLetNumChar]] text-input',
                                                                                 'name'       => 'AVIoe_9_Txt',
                                                                                 'id'         => 'AVIoe_9_Txt',
                                                                                 'placeholder'=> '[Concept]',
                                                                                 'value'      => $datos['APVI'][0]['AVIoe_9_Txt'],
                                                                                 'size'       => '18',
                                                                                 'maxlength'  => '40')).'
                                        </td>
                                        <td class=" leftCell"> '.form_input(array('class'      => 'oeNum1 conborde validate[custom[number]] text-input',
                                                                                 'name'       => 'AVIoe_9_num1',
                                                                                 'id'         => 'AVIoe_9_num1',
                                                                                 'placeholder'=> '0.0',
                                                                                 'value'      => $datos['APVI'][0]['AVIoe_9_num1'],
                                                                                 'size'       => '5',
                                                                                 'onChange'   => 'calculaOE_Por(this,\'9\','.$datos['er'].','.$egi.')',
                                                                                 'maxlength'  => '4')).' %</td>
                                        <td class=" leftCell">$ <label id="AVIoe_9_num2Lbl"   >'.number_format( empty($datos['APVI'][0]['AVIoe_9_num2'])?0:$datos['APVI'][0]['AVIoe_9_num2'], 2, '.', ',').'             </label> mxn<br>
                                                              $ <label id="AVIoe_9_num2Lblusd">'.number_format( empty($datos['APVI'][0]['AVIoe_9_num2'])?0:$datos['APVI'][0]['AVIoe_9_num2']/$datos['er'], 2, '.', ',').'</label>  '.$param['currency'].'
                                                              '.form_input(array('name'=>'AVIoe_9_num2','class'=>'oeNum2', 'type'=>'hidden', 'id'=>'AVIoe_9_num2', 'value'=> $datos['APVI'][0]['AVIoe_9_num2'] )).'</td>
                                    </tr>
                                    <tr class="apvThinSinNegrita"> 
                                        <td class=" leftCell">'.form_input(array('class'      => 'conborde validate[custom[onlyLetNumChar]] text-input',
                                                                                 'name'       => 'AVIoe_10_Txt',
                                                                                 'id'         => 'AVIoe_10_Txt',
                                                                                 'placeholder'=> '[Concept]',
                                                                                 'value'      => $datos['APVI'][0]['AVIoe_10_Txt'],
                                                                                 'size'       => '18',
                                                                                 'maxlength'  => '40')).'
                                        </td>
                                        <td class=" leftCell"> <label id="AVIoe_10_num1Lbl">'.number_format(empty($datos['APVI'][0]['AVIoe_10_num1'])?0:$datos['APVI'][0]['AVIoe_10_num1'], 2, '.', ',').'</label> %'.form_input(array('name'=>'AVIoe_10_num1','class'=>'oeNum1', 'type'=>'hidden', 'id'=>'AVIoe_10_num1', 'value'=> $datos['APVI'][0]['AVIoe_10_num1'] )).'</td>
                                        <td class=" leftCell">'.form_input(array('class'      => 'pmask conborde',
                                                                                 'name'       => 'AVIoe_10_num2Mask',
                                                                                 'id'         => 'AVIoe_10_num2Mask',
                                                                                 'placeholder'=> '0.0',
                                                                                 'value'      => $eo10,
                                                                                 'size'       => '10',
                                                                                 'onChange'   => 'calculaOE_Num(this,\'10\','.$datos['er'].','.$egi.')',
                                                                                 'maxlength'  => '40')).' mxn <input type="hidden" class="oeNum2" name="AVIoe_10_num2" id="AVIoe_10_num2" value="'.$eo10.'"><br>
                                                                $ <label id="AVIoe_10_num2usd">'.number_format( empty($eo10)?0:$eo10/$datos['er'], 2, '.', ',').'</label> '.$param['currency'].'</td>
                                    </tr>
                                    <tr class="apvThinSinNegrita"> 
                                        <td class=" leftCell">'.form_input(array('class'      => 'conborde validate[custom[onlyLetNumChar]] text-input',
                                                                                 'name'       => 'AVIoe_11_Txt',
                                                                                 'id'         => 'AVIoe_11_Txt',
                                                                                 'placeholder'=> '[Concept]',
                                                                                 'value'      => $datos['APVI'][0]['AVIoe_11_Txt'],
                                                                                 'size'       => '18',
                                                                                 'maxlength'  => '40')).'
                                        </td>             
                                        <td class=" leftCell"> <label id="AVIoe_11_num1Lbl">'.number_format(empty($datos['APVI'][0]['AVIoe_11_num1'])?0:$datos['APVI'][0]['AVIoe_11_num1'], 2, '.', ',').'</label> %'.form_input(array('name'=>'AVIoe_11_num1','class'=>'oeNum1', 'type'=>'hidden', 'id'=>'AVIoe_11_num1', 'value'=> $datos['APVI'][0]['AVIoe_11_num1'] )).'</td>
                                        <td class=" leftCell">'.form_input(array('class'      => 'pmask conborde',
                                                                                 'name'       => 'AVIoe_11_num2Mask',
                                                                                 'id'         => 'AVIoe_11_num2Mask',
                                                                                 'placeholder'=> '0.0',
                                                                                 'value'      => $eo11,
                                                                                 'onChange'   => 'calculaOE_Num(this,\'11\','.$datos['er'].','.$egi.')',
                                                                                 'size'       => '10',
                                                                                 'maxlength'  => '40')).' mxn <input type="hidden" class="oeNum2" name="AVIoe_11_num2" id="AVIoe_11_num2" value="'.$eo11.'"><br>
                                                                $ <label id="AVIoe_11_num2usd">'.number_format( empty($eo11)?0:$eo11/$datos['er'], 2, '.', ',').'</label> '.$param['currency'].'</td>
                                    </tr>
                                    <tr class="apvThinSinNegrita">
                                        <td class=" leftCell">'.$param['se'].'</td>
                                        <td class=" leftCell"> <label id="AVIoe_sePor"    >'.number_format( empty($sePor)?0:$sePor, 2, '.', ',').'             </label>  %</td>
                                        <td class=" leftCell">$ <label id="AVIoe_seNum"   >'.number_format( empty($seNum)?0:$seNum, 2, '.', ',').'             </label> mxn<br>
                                                              $ <label id="AVIoe_seNumusd">'.number_format( empty($seNum)?0:$seNum/$datos['er'], 2, '.', ',').'</label> '.$param['currency'].'</td>
                                    </tr>
                                    <tr class="apvThinSinNegrita">
                                        <td class=" leftCell">'.$param['unf'].'</td>
                                        <td class=" leftCell"> '.form_input(array('class'      => 'conborde validate[custom[number]] text-input',
                                                                                 'name'       => 'AVIoe_unf',
                                                                                 'id'         => 'AVIoe_unf',
                                                                                 'placeholder'=> '0.0',
                                                                                 'value'      => $datos['APVI'][0]['AVIoe_unf'],
                                                                                 'size'       => '5',
                                                                                 'onChange'   => 'calculaOE_unf(this,'.$datos['er'].','.$egi.')',
                                                                                 'maxlength'  => '4')).' %</td>
                                        <td class=" leftCell">$<label id="AVIoe_unfLbl">   '.number_format( empty($unf)?0:$unf, 2, '.', ',').'             </label> mxn<br>
                                                              $<label id="AVIoe_unfLblusd">'.number_format( empty($unf)?0:$unf/$datos['er'], 2, '.', ',').'</label>  '.$param['currency'].'</td>                                                
                                    </tr>
                                    <tr class="apvThinSinNegrita">
                                        <td class=" leftCell">'.$param['totExp'].form_input(array('name'=>'AVI_totExp', 'type'=>'hidden', 'id'=>'AVI_totExp', 'value'=> $totExpPor )).'</td>
                                        <td class=" leftCell"> <label id="totExpPor"       >'.number_format( empty($totExpPor)?0:$totExpPor, 2, '.', ',').'</label> %</td>
                                        <td class=" leftCell">$ <label id="AVIoe_totExp"   >'.number_format( empty($totExpNum)?0:$totExpNum, 2, '.', ',').'             </label> mxn<br>
                                                              $ <label id="AVIoe_totExpusd">'.number_format( empty($totExpNum)?0:$totExpNum/$datos['er'], 2, '.', ',').'</label> '.$param['currency'].'</td>                                                
                                    </tr>
                                    <tr class="apBottom">
                                        <td class=" leftCell"colspan="2">'.$param['noi'].form_input(array('name'=>'AVI_noi', 'type'=>'hidden', 'id'=>'AVI_noi', 'value'=> $noi )).'</td>
                                        <td class=" leftCell">$ <label id="AVIoe_noi"   >'.number_format( empty($noi)?0:$noi, 2, '.', ',').'             </label> mxn<br>
                                                              $ <label id="AVIoe_noiusd">'.number_format( empty($noi)?0:$noi/$datos['er'], 2, '.', ',').'</label>  '.$param['currency'].'</td>                                                
                                    </tr>
                                    </table>
                                 </section>   
                            </div>
                             <div class="row">
                                <section class="col col-8"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetNumChar]] text-input',
                                                                                 'name'        => 'AVITit5',
                                                                                 'id'          => 'AVITit5',                                                                                 
                                                                                 'value'       => $datos['APVI'][0]['AVITit5'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>                                                           
                                 </section>        
                                 <section class="col col-10"><label class="textarea">'.
                                                                form_textarea(array('class' => 'validate[required,custom[onlyLetNumChar]] text-input',
                                                                                    'rows'  => "3",
                                                                                    'name'  => 'AVIText5_1',
                                                                                    'id'    => 'AVIText5_1',
                                                                                    'value' => $datos['APVI'][0]['AVIText5_1'] )).'
                                                            </label>
                                </section>
                             </div>
                             <div class="row">
                                <section class="col col-8"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetNumChar]] text-input',
                                                                                 'name'        => 'AVIText5_2',
                                                                                 'id'          => 'AVIText5_2',                                                                                 
                                                                                 'value'       => $datos['APVI'][0]['AVIText5_2'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>                                                           
                                 </section>
                                <section class="col col-10"><label class="textarea">'.
                                                                form_textarea(array('class' => 'validate[required,custom[onlyLetNumChar]] text-input',
                                                                                    'rows'  => "5",
                                                                                    'name'  => 'AVIText5_3',
                                                                                    'id'    => 'AVIText5_3',
                                                                                    'value' => $datos['APVI'][0]['AVIText5_3'] )).'
                                                            </label>
                                </section>
                            </div>
                            <div class="row"> 
                                <section class="col col-11">'.  br(2).'
                                <table border="1" cellspacing="0" cellpadding="0" width="100%">
                                    <tr><td class="centerCell" rowspan="3">'.$param['source'].'</td>
                                    </tr>
                                    <tr><td class="centerCell" colspan="2">'.$param['cr'].'</td>
                                    </tr>
                                    <tr>
                                        <td class="centerCell" colspan="2">'.$param['ran'].'</td>
                                        <td class="centerCell" >'.$param['av'].'</td>
                                    </tr>';
                                    $values = array($datos['APVI'][0]['AVIsource_1_av']);
                                    $avg1   = array_sum($values) / count($values);
                                    $values = array($datos['APVI'][0]['AVIsource_2_av']);
                                    $avg2   = array_sum($values) / count($values);
                                    $values = array($datos['APVI'][0]['AVIsource_3_av']);
                                    $avg3   = array_sum($values) / count($values);
                                    $values = array($datos['APVI'][0]['AVIsource_4_av']);
                                    $avg4   = array_sum($values) / count($values);
                                    $valuesTot = array();
                                    $valuesTot = $this->capitalizationRateNumAverage($avg1,$valuesTot); 
                                    $valuesTot = $this->capitalizationRateNumAverage($avg2,$valuesTot); 
                                    $valuesTot = $this->capitalizationRateNumAverage($avg3,$valuesTot); 
                                    $valuesTot = $this->capitalizationRateNumAverage($avg4,$valuesTot);
                                    
                                    if(count($valuesTot)>0){
                                      $avgTot = array_sum($valuesTot) / count($valuesTot);
                                    }else{
                                      $avgTot = 0;
                                    }

                                    //$avgTot = array_sum($valuesTot) / count($valuesTot);
                    $coreAPVI .=  '<tr>
                                        <td class="centerCell" >'.form_input(array('class'      => 'conborde validate[custom[onlyLetNumChar]] text-input',
                                                                                   'name'       => 'AVIsource_1_Txt',
                                                                                   'id'         => 'AVIsource_1_Txt',
                                                                                   'placeholder'=> '[Concept]',
                                                                                   'value'      => $datos['APVI'][0]['AVIsource_1_Txt'],
                                                                                   'size'       => '18',
                                                                                   'maxlength'  => '40')).'</td>
                                        <td class="centerCell" >'.form_input(array('class'      => 'conborde validate[custom[number]] text-input',
                                                                                   'name'       => 'AVIsource_1_ran1',
                                                                                   'id'         => 'AVIsource_1_ran1',
                                                                                   'placeholder'=> '0.0',
                                                                                   'value'      => $datos['APVI'][0]['AVIsource_1_ran1'],
                                                                                   'size'       => '8',
                                                                                   'onChange'   => 'calculaSourcePor(this,1,\''.base_url().'\')',
                                                                                   'maxlength'  => '4')).' %</td>
                                        <td class="centerCell" >'.form_input(array('class'      => 'conborde validate[custom[number]] text-input',
                                                                                   'name'       => 'AVIsource_1_ran2',
                                                                                   'id'         => 'AVIsource_1_ran2',
                                                                                   'placeholder'=> '0.0',
                                                                                   'value'      => $datos['APVI'][0]['AVIsource_1_ran2'],
                                                                                   'size'       => '8',
                                                                                   'onChange'   => 'calculaSourcePor(this,1,\''.base_url().'\')',
                                                                                   'maxlength'  => '5')).' %</td>
                                             <td class="centerCell" >'.form_input(array('class'      => 'conborde validate[custom[number]] text-input',
                                                                                   'name'       => 'AVIsource_1_av',
                                                                                   'id'         => 'AVIsource_1_av',
                                                                                   'placeholder'=> '0.0',
                                                                                   'value'      => $datos['APVI'][0]['AVIsource_1_av'],
                                                                                   'size'       => '8',
                                                                                   'onChange'   => 'calculaSourcePor(this,1,\''.base_url().'\')',
                                                                                   'maxlength'  => '5')).' %</td>
                                       
                                    </tr>
                                    <tr>
                                        <td class="centerCell" >'.form_input(array('class'      => 'conborde validate[custom[onlyLetNumChar]] text-input',
                                                                                   'name'       => 'AVIsource_2_Txt',
                                                                                   'id'         => 'AVIsource_2_Txt',
                                                                                   'placeholder'=> '[Concept]',
                                                                                   'value'      => $datos['APVI'][0]['AVIsource_2_Txt'],
                                                                                   'size'       => '18',
                                                                                   'maxlength'  => '40')).'</td>
                                        <td class="centerCell" >'.form_input(array('class'      => 'conborde validate[custom[number]] text-input',
                                                                                   'name'       => 'AVIsource_2_ran1',
                                                                                   'id'         => 'AVIsource_2_ran1',
                                                                                   'placeholder'=> '0.0',
                                                                                   'value'      => $datos['APVI'][0]['AVIsource_2_ran1'],
                                                                                   'size'       => '8',
                                                                                   'onChange'   => 'calculaSourcePor(this,2,\''.base_url().'\')',   
                                                                                   'maxlength'  => '5')).' %</td>
                                        <td class="centerCell" >'.form_input(array('class'      => 'conborde validate[custom[number]] text-input',
                                                                                   'name'       => 'AVIsource_2_ran2',
                                                                                   'id'         => 'AVIsource_2_ran2',
                                                                                   'placeholder'=> '0.0',
                                                                                   'value'      => $datos['APVI'][0]['AVIsource_2_ran2'],
                                                                                   'size'       => '8',
                                                                                   'onChange'   => 'calculaSourcePor(this,2,\''.base_url().'\')',
                                                                                   'maxlength'  => '5')).' %</td>
                                         <td class="centerCell" >'.form_input(array('class'      => 'conborde validate[custom[number]] text-input',
                                                                                   'name'       => 'AVIsource_2_av',
                                                                                   'id'         => 'AVIsource_2_av',
                                                                                   'placeholder'=> '0.0',
                                                                                   'value'      => $datos['APVI'][0]['AVIsource_2_av'],
                                                                                   'size'       => '8',
                                                                                   'onChange'   => 'calculaSourcePor(this,2,\''.base_url().'\')',
                                                                                   'maxlength'  => '5')).' %</td>
                                    </tr>
                                    <tr>
                                        <td class="centerCell" >'.form_input(array('class'      => 'conborde validate[custom[onlyLetNumChar]] text-input',
                                                                                   'name'       => 'AVIsource_3_Txt',
                                                                                   'id'         => 'AVIsource_3_Txt',
                                                                                   'placeholder'=> '[Concept]',
                                                                                   'value'      => $datos['APVI'][0]['AVIsource_3_Txt'],
                                                                                   'size'       => '18',
                                                                                   'maxlength'  => '40')).'</td>
                                        <td class="centerCell" >'.form_input(array('class'      => 'conborde validate[custom[number]] text-input',
                                                                                   'name'       => 'AVIsource_3_ran1',
                                                                                   'id'         => 'AVIsource_3_ran1',
                                                                                   'placeholder'=> '0.0',
                                                                                   'value'      => $datos['APVI'][0]['AVIsource_3_ran1'],
                                                                                   'size'       => '8',
                                                                                   'onChange'   => 'calculaSourcePor(this,3,\''.base_url().'\')',
                                                                                   'maxlength'  => '5')).' %</td>
                                        <td class="centerCell" >'.form_input(array('class'      => 'conborde validate[custom[number]] text-input',
                                                                                   'name'       => 'AVIsource_3_ran2',
                                                                                   'id'         => 'AVIsource_3_ran2',
                                                                                   'placeholder'=> '0.0',
                                                                                   'value'      => $datos['APVI'][0]['AVIsource_3_ran2'],
                                                                                   'size'       => '8',
                                                                                   'onChange'   => 'calculaSourcePor(this,3,\''.base_url().'\')',
                                                                                   'maxlength'  => '5')).' %</td>
                                           <td class="centerCell" >'.form_input(array('class'      => 'conborde validate[custom[number]] text-input',
                                                                                   'name'       => 'AVIsource_3_av',
                                                                                   'id'         => 'AVIsource_3_av',
                                                                                   'placeholder'=> '0.0',
                                                                                   'value'      => $datos['APVI'][0]['AVIsource_3_av'],
                                                                                   'size'       => '8',
                                                                                   'onChange'   => 'calculaSourcePor(this,3,\''.base_url().'\')',
                                                                                   'maxlength'  => '5')).' %</td>
                                    </tr>
                                    <tr>
                                        <td class="centerCell" >'.form_input(array('class'      => 'conborde validate[custom[onlyLetNumChar]] text-input',
                                                                                   'name'       => 'AVIsource_4_Txt',
                                                                                   'id'         => 'AVIsource_4_Txt',
                                                                                   'placeholder'=> '[Concept]',
                                                                                   'value'      => $datos['APVI'][0]['AVIsource_4_Txt'],
                                                                                   'size'       => '18',
                                                                                   'maxlength'  => '40')).'</td>
                                        <td class="centerCell" >'.form_input(array('class'      => 'conborde validate[custom[number]] text-input',
                                                                                   'name'       => 'AVIsource_4_ran1',
                                                                                   'id'         => 'AVIsource_4_ran1',
                                                                                   'placeholder'=> '0.0',
                                                                                   'value'      => $datos['APVI'][0]['AVIsource_4_ran1'],
                                                                                   'size'       => '8',
                                                                                   'onChange'   => 'calculaSourcePor(this,4,\''.base_url().'\')',
                                                                                   'maxlength'  => '5')).' %</td>
                                        <td class="centerCell" >'.form_input(array('class'      => 'conborde validate[custom[number]] text-input',
                                                                                   'name'       => 'AVIsource_4_ran2',
                                                                                   'id'         => 'AVIsource_4_ran2',
                                                                                   'placeholder'=> '0.0',
                                                                                   'value'      => $datos['APVI'][0]['AVIsource_4_ran2'],
                                                                                   'size'       => '8',
                                                                                   'onChange'   => 'calculaSourcePor(this,4,\''.base_url().'\')',
                                                                                   'maxlength'  => '5')).' %</td>

                                           <td class="centerCell" >'.form_input(array('class'      => 'conborde validate[custom[number]] text-input',
                                                                                   'name'       => 'AVIsource_4_av',
                                                                                   'id'         => 'AVIsource_4_av',
                                                                                   'placeholder'=> '0.0',
                                                                                   'value'      => $datos['APVI'][0]['AVIsource_4_av'],
                                                                                   'size'       => '8',
                                                                                   'onChange'   => 'calculaSourcePor(this,4,\''.base_url().'\')',
                                                                                   'maxlength'  => '5')).' %</td>
                                      
                                    </tr>
                                    <tr class="apBottom">  <td class="centerCell" >'.  nbs(1).'</td>
                                          <td class="centerCell" >'.  $param['av'].'</td>
                                          <td class="centerCell" >Ro</td>
                                          <td class="centerCell" ><label id="avTot">'.number_format($avgTot, 2, '.', ',').'</label> %'.form_input(array('name'=>'avgTot', 'type'=>'hidden', 'id'=>'avgTot', 'value'=> $avgTot )).'</td>
                                    </tr>                                    
                                </table>'.  br(2).'
                                </section>
                            </div>';
                $coreAPVI .=  "<div class='row'><section class='col col-8'><a class='button' href='javascript:recalculaAPV_VI(baseURL,tipoAccion,\"APVI\",".$datos['id_in'].",\"A\");'>Recalculate</a></section><section id='confirmRecalculateAPVIA' class='col col-4'></section></div>";
                $coreAPVI .=  '<div class="row">
                                <section class="col col-8"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetNumChar]] text-input',
                                                                                 'name'        => 'AVIText5_4',
                                                                                 'id'          => 'AVIText5_4',                                                                                 
                                                                                 'value'       => $datos['APVI'][0]['AVIText5_4'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>                                                           
                                 </section>
                                <section class="col col-10"><label class="textarea">'.
                                                                form_textarea(array('class' => 'validate[required,custom[onlyLetNumChar]] text-input',
                                                                                    'rows'  => "3",
                                                                                    'name'  => 'AVIText5_5',
                                                                                    'id'    => 'AVIText5_5',
                                                                                    'value' => $datos['APVI'][0]['AVIText5_5'] )).'
                                                            </label>
                                </section>
                            </div>                            
                            <div class="row"> 
                                <section class="col col-11">'.  br(2);
                                    $AVImir   = empty($datos['APVI'][0]['AVImir'])?0:$datos['APVI'][0]['AVImir'];
                                    $AVImt    = $datos['APVI'][0]['AVImt'];
                                    $AVIlvr   = $datos['APVI'][0]['AVIlvr']/100;
                                    $AVIedr   = $datos['APVI'][0]['AVIedr'];
                                    $AVIicr   = $datos['APVI'][0]['AVIicr']*100;
                                    $AVImorCo = $datos['APVI'][0]['AVImorCo']*100;
                                    $morCoTot = $AVIlvr * $AVImorCo;
                                    $edr      = 1 - $AVIlvr;
                                    $edrTot   = $AVIedr * $edr;                                    
             $coreAPVI .=        '<table border="1" cellspacing="0" cellpadding="0" width="100%">
                                    <tr><td class="leftCell">'.$param['mir'].'</td><td class="leftCell">'.form_input(array('class'      => 'conborde validate[custom[number]] text-input',
                                                                                                                             'name'       => 'AVImir',
                                                                                                                             'id'         => 'AVImir',
                                                                                                                             'placeholder'=> '0.0',
                                                                                                                             'value'      => $AVImir,
                                                                                                                             'size'       => '5',
                                                                                                                             'onChange'   => 'calcularICR(this,\''.base_url().'\')',
                                                                                                                             'maxlength'  => '5')).'%</td><td class="centerCell" colspan="4">'.nbs(1).'</td>
                                    </tr>
                                    <tr><td class="leftCell">'.$param['mt'].'</td><td class="leftCell">'.form_input(array('class'      => 'conborde validate[custom[number]] text-input',
                                                                                                                            'name'       => 'AVImt',
                                                                                                                            'id'         => 'AVImt',
                                                                                                                            'placeholder'=> '0.0',
                                                                                                                            'value'      => $AVImt,
                                                                                                                            'size'       => '5',
                                                                                                                            'onChange'   => 'calcularICR(this,\''.base_url().'\')',
                                                                                                                            'maxlength'  => '5')).nbs(1).'</td><td class="centerCell" colspan="4">'.nbs(1).'</td>
                                    </tr>
                                    <tr><td class="leftCell">'.$param['lvr'].'</td><td class="leftCell">'.form_input(array('class'      => 'conborde validate[custom[number]] text-input',
                                                                                                                            'name'       => 'AVIlvr',
                                                                                                                            'id'         => 'AVIlvr',
                                                                                                                            'placeholder'=> '0.0',
                                                                                                                            'value'      => $datos['APVI'][0]['AVIlvr'],
                                                                                                                            'size'       => '5',
                                                                                                                            'onChange'   => 'calcularICR(this,\''.base_url().'\')',
                                                                                                                            'maxlength'  => '5')).'%</td><td class="centerCell" colspan="4">'.nbs(1).'</td>
                                    </tr>
                                    <tr><td class="leftCell">'.$param['morCo'].'</td><td class="leftCell">'.br(1).'<label id="morCo">'.number_format($AVImorCo/100, 2, '.', ',').'</label>%'.br(1).form_input(array('name'=>'AVImorCo', 'type'=>'hidden', 'id'=>'AVImorCo', 'value'=> $AVImorCo/100 )).'</td><td class="centerCell" colspan="4">'.nbs(1).'</td></tr>
                                    <tr><td class="leftCell">'.$param['edr'].'  </td><td class="leftCell">'.form_input(array('class'      => 'conborde validate[custom[number]] text-input',
                                                                                                                               'name'       => 'AVIedr',
                                                                                                                               'id'         => 'AVIedr',
                                                                                                                               'placeholder'=> '0.0',
                                                                                                                               'value'      => $AVIedr,
                                                                                                                               'size'       => '5',
                                                                                                                               'onChange'   => 'calcularICR(this,\''.base_url().'\')',
                                                                                                                               'maxlength'  => '5')).'%</td><td class="centerCell" colspan="4">'.nbs(1).'</td>
                                    </tr>
                                    <tr><td class="leftCell">'.br(1).$param['morRe'].'          </td><td class="centerCell"><label id="lvrLbl"   >'.br(1).number_format( empty($datos['APVI'][0]['AVIlvr'])?0:$datos['APVI'][0]['AVIlvr'], 2, '.', ',').'</label>%</td><td class="centerCell">'.br(1).'X</td><td class="centerCell"><label id="morCoLbl">'.br(1).number_format(empty($AVImorCo)?0:$AVImorCo/100, 2, '.', ',').' </label>%</td><td class="centerCell"> = </td><td class="rightCell"><label id="morCoLblFin">'.br(1).number_format($morCoTot/100, 2, '.', ',').'</label>%</td></tr>
                                    <tr><td class="leftCell">'.$param['eqRe'].'           </td><td class="centerCell"><label id="AVIedrLbl">'.number_format($edr*100, 2, '.', ',').'   </label>%</td><td class="centerCell">X</td><td class="centerCell"><label id="edrLbl"  >'.number_format(empty($AVIedr)?0:$AVIedr, 2, '.', ',').'</label>%</td><td class="centerCell"> = </td><td class="rightCell"><label id="edrLblFin"  >'.number_format(empty($edrTot)?0:$edrTot, 2, '.', ',').'  </label>%</td></tr>
                                    <tr class="apBottom"><td class="leftCell" colspan="4">'.br(1).$param['icr'].'</td><td class="rightCell">'.br(1).'Ro = </td><td class="rightCell"><label id="icrLbl">'.br(1).number_format($AVIicr/100, 2, '.', ',').'</label>%'.form_input(array('name'=>'AVIicr', 'type'=>'hidden', 'id'=>'AVIicr', 'value'=> $AVIicr/100 )).'</td></tr>
                                </table>'.  br(2).'    
                                </section>
                            </div>';
             $coreAPVI .=  "<div class='row'><section class='col col-8'><a class='button' href='javascript:recalculaAPV_VI(baseURL,tipoAccion,\"APVI\",".$datos['id_in'].",\"B\");'>Recalculate</a></section><section id='confirmRecalculateAPVIB' class='col col-4'></section></div>";
             $coreAPVI .=  '<div class="row">
                                <section class="col col-8"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetNumChar]] text-input',
                                                                                 'name'        => 'AVIText5_6',
                                                                                 'id'          => 'AVIText5_6',
                                                                                 'value'       => $datos['APVI'][0]['AVIText5_6'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>
                                 </section>
                                <section class="col col-10"><label class="textarea">'.
                                                                form_textarea(array('class' => 'validate[required,custom[onlyLetNumChar]] text-input',
                                                                                    'rows'  => "3",
                                                                                    'name'  => 'AVIText5_7',
                                                                                    'id'    => 'AVIText5_7',
                                                                                    'value' => $datos['APVI'][0]['AVIText5_7'] )).'
                                                            </label>
                                </section>
                            </div>
                            <div class="row"> 
                                <section class="col col-10">';
                                $AVIdcrTit = $datos['APVI'][0]['AVIdcrTit'];
                                $dcr       = $AVIdcrTit * $AVIlvr * $AVImorCo/100;
            $coreAPVI .=        '<table border="1" cellspacing="0" cellpadding="0" width="100%">
                                    <tr><td class="leftCell" colspan="2">'.$param['roFor'].'  </td></tr>
                                    <tr><td class="leftCell" colspan="2">'.$param['whereDCR'].'</td></tr>
                                    <tr><td class="leftCell" colspan="2">'.nbs(3).$param['rmLbl'].'</td></tr>
                                    <tr><td class="leftCell" colspan="2">'.nbs(3).$param['mLbl'].'</td></tr>
                                    <tr><td class="leftCell">'.br(2).$param['dcrTit'].'</td><td class="leftCell">'.br(2).form_input(array('class'      => 'conborde validate[custom[number]] text-input',
                                                                                                                                'name'       => 'AVIdcrTit',
                                                                                                                                'id'         => 'AVIdcrTit',
                                                                                                                                'placeholder'=> '0.0',
                                                                                                                                'value'      => $AVIdcrTit,
                                                                                                                                'size'       => '5',
                                                                                                                                'onChange'   => 'calcularDCR(this,\''.base_url().'\')',
                                                                                                                                'maxlength'  => '5')).'</td></tr>
                                    <tr><td class="leftCell" colspan="2">'.$param['dcrLbl'].br(2).'</td></tr>
                                    <tr><td class="leftCell">'.nbs(3).$param['ir'].   '</td><td class="leftCell"><label id="AVImirLvlC"  >'.number_format(empty($AVImir)?0:$AVImir, 2, '.', ',').'  </label> %    </td></tr>
                                    <tr><td class="leftCell">'.nbs(3).$param['amoPe'].'</td><td class="leftCell"><label id="AVImtLvlC"   >'.number_format(empty($AVImt)?0:$AVImt, 1, '.', ',').'   </label> years</td></tr>
                                    <tr><td class="leftCell">'.nbs(3).$param['lvLbl'].'</td><td class="leftCell"><label id="AVIlvrLvlC"  >'.number_format(empty($datos['APVI'][0]['AVIlvr'])?0:$datos['APVI'][0]['AVIlvr'], 2, '.', ',').'  </label> %    </td></tr>
                                    <tr><td class="leftCell">'.nbs(3).$param['mcLbl'].'</td><td class="leftCell"><label id="AVImorCoLvlC">'.number_format(empty($AVImorCo)?0:$AVImorCo/100, 2, '.', ',').'</label> %    </td></tr>
                                    <tr class="apBottom"><td class="rightCell"><br>Ro = </td><td class="leftCell">'.br(1).nbs(1).'<label id="dcrFin">'.number_format(empty($dcr)?0:$dcr, 2, '.', ',').'</label> %'.form_input(array('name'=>'dcr', 'type'=>'hidden', 'id'=>'dcr', 'value'=> $dcr )).'</td></tr>
                                   </table>     
                                </section>
                            </div>';
            $coreAPVI .=  "<div class='row'><section class='col col-8'><a class='button' href='javascript:recalculaAPV_VI(baseURL,tipoAccion,\"APVI\",".$datos['id_in'].",\"C\");'>Recalculate</a></section><section id='confirmRecalculateAPVIC' class='col col-4'></section></div>";                            
            $coreAPVI .=  '<div class="row">
                                <section class="col col-8"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetNumChar]] text-input',
                                                                                 'name'        => 'AVITit6',
                                                                                 'id'          => 'AVITit6',                                                                                 
                                                                                 'value'       => $datos['APVI'][0]['AVITit6'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>                                                           
                                 </section>
                                  <section class="col col-10"><label class="textarea">'.
                                                                form_textarea(array('class' => 'validate[required,custom[onlyLetNumChar]] text-input ',
                                                                                    'rows'  => "3",
                                                                                    'name'  => 'AVIText6_1',
                                                                                    'id'    => 'AVIText6_1',
                                                                                    'value' => $datos['APVI'][0]['AVIText6_1'] )).'
                                                            </label>
                                </section>                                
                            </div>
                            <div class="row"> 
                                <section class="col col-10">';
                                   $AVIcapR_por1 = $datos['APVI'][0]['AVIcapR_por1']/100;
                                   $AVIcapR_por2 = $datos['APVI'][0]['AVIcapR_por2']/100;
                                   $AVIcapR_por3 = $datos['APVI'][0]['AVIcapR_por3']/100;
                                   $icr = $AVIicr/100;
                                   $capitalizationRate = ($AVIcapR_por1 * $avgTot) + ($AVIcapR_por2 * $icr) + ($AVIcapR_por3 * $dcr);
                                   $prorrateoCR        = ($AVIcapR_por1+$AVIcapR_por2+$AVIcapR_por3)*100;
                                   $valCR              = ($prorrateoCR==100)?"<img src=\"".base_url()."images/check.png\">".nbs(2):"<img src=\"".base_url()."images/close.png\">".nbs(2);
                    $coreAPVI .=  '<table border="1" cellspacing="0" cellpadding="0" width="100%">
                                    <tr><td class="leftCell">'.$datos['APVI'][0]['AVIText5_2'].'  </td><td class="centerCell"><label id="avgTotLbl">'.number_format($avgTot, 2, '.', ',').'</label>  %</td><td class="centerCell">'.form_input(array('class'      => 'conborde validate[custom[number]] text-input',
                                                                                                                                                                                                                                                    'name'       => 'AVIcapR_por1',
                                                                                                                                                                                                                                                    'id'         => 'AVIcapR_por1',
                                                                                                                                                                                                                                                    'placeholder'=> '0.0',
                                                                                                                                                                                                                                                    'value'      => $datos['APVI'][0]['AVIcapR_por1'],
                                                                                                                                                                                                                                                    'size'       => '5',
                                                                                                                                                                                                                                                    'onChange'   => 'calcularCapRate(this,\''.base_url().'\')',
                                                                                                                                                                                                                                                    'maxlength'  => '5')).' %</td>
                                    </tr>
                                    <tr><td class="leftCell">'.$datos['APVI'][0]['AVIText5_4'].'  </td><td class="centerCell"><label id="icrLbl2">'.number_format($icr, 2, '.', ',').'</label>  %</td><td class="centerCell">'.form_input(array('class'      => 'conborde validate[custom[number]] text-input',
                                                                                                                                                                                                                                                'name'       => 'AVIcapR_por2',
                                                                                                                                                                                                                                                'id'         => 'AVIcapR_por2',
                                                                                                                                                                                                                                                'placeholder'=> '0.0',
                                                                                                                                                                                                                                                'value'      => $datos['APVI'][0]['AVIcapR_por2'],
                                                                                                                                                                                                                                                'size'       => '5',
                                                                                                                                                                                                                                                'onChange'   => 'calcularCapRate(this,\''.base_url().'\')',
                                                                                                                                                                                                                                                'maxlength'  => '5')).' %</td>
                                    </tr>
                                    <tr><td class="leftCell">'.$datos['APVI'][0]['AVIText5_6'].'  </td><td class="centerCell"><label id="dcrLbl">'.number_format($dcr, 2, '.', ',').'</label>  %</td><td class="centerCell">'.form_input(array('class'      => 'conborde validate[custom[number]] text-input',
                                                                                                                                                                                                                                                'name'       => 'AVIcapR_por3',
                                                                                                                                                                                                                                                'id'         => 'AVIcapR_por3',
                                                                                                                                                                                                                                                'placeholder'=> '0.0',
                                                                                                                                                                                                                                                'value'      => $datos['APVI'][0]['AVIcapR_por3'],
                                                                                                                                                                                                                                                'size'       => '5',
                                                                                                                                                                                                                                                'onChange'   => 'calcularCapRate(this,\''.base_url().'\')',
                                                                                                                                                                                                                                                'maxlength'  => '5')).' %</td>
                                    </tr>
                                    <tr class="apBottom"><td class="rightCell">'.br(1).$param['av'].'  </td><td class="centerCell"><label id="capRateTot">'.br(1).number_format($capitalizationRate, 2, '.', ',').'</label>  %'.form_input(array('name'=>'AVIcap_rate', 'type'=>'hidden', 'id'=>'AVIcap_rate', 'value'=>$datos['APVI'][0]['AVIcap_rate'] )).'</td><td class="centerCell"><label id="capRSum" class=\''.(($prorrateoCR == 100)?"msjOkCentro":"msjErrCentro").'\'>'.br(1).$valCR.number_format($prorrateoCR,2, '.', ',' ).' %</label></td></tr>
                                   </table>     
                                </section>
                            </div><br>
                            <div class="row">
                                <section class="col col-10"><label class="textarea">'.
                                                                form_textarea(array('class' => 'validate[required,custom[onlyLetNumChar]] text-input ',
                                                                                    'rows'  => "3",
                                                                                    'name'  => 'AVIText6_2',
                                                                                    'id'    => 'AVIText6_2',
                                                                                    'value' => $datos['APVI'][0]['AVIText6_2'] )).'
                                                            </label>
                                </section>
                            </div>                            
                            <div class="row">
                                <section class="col col-10">';
                                    $ivudica = empty($capitalizationRate)?0:(($noi/$datos['er']) / ($capitalizationRate/100));
                    $coreAPVI .=  '<table border="1" cellspacing="0" cellpadding="0" width="100%">
                                        <tr class="apBottom"><td class="centerCell">'.$param['ica'].'</td><td class="centerCell">'.$param['noi'].'</td> <td class="centerCell">'.$param['cr'].'</td> </tr>
                                        <tr ><td class="centerCell">'.nbs(1).'</td><td class="centerCell">$ '.number_format($noi/$datos['er'], 2, '.', ',').' '.$param['currency'].'</td> <td class="centerCell">'.number_format($capitalizationRate, 2, '.', ',').' %</td> </tr>
                                        <tr ><td class="rightCell" colspan="2">'.br(1).$param['ivudica'].br(1).'</td> <td class="centerCell">'.br(1).'$ '.number_format($ivudica, 2, '.', ',').' '.$param['currency'].br(1).form_input(array('name'=>'AVIivudica', 'type'=>'hidden', 'id'=>'AVIivudica', 'value'=>$ivudica )).'</td> </tr>
                                        <tr ><td class="centerCell apBottom">'.nbs(1).'</td><td class="leftCell apBottom" >'.br(1).$param['roun'].br(1).'</td> <td class="centerCell apBottom">'.br(1).'$ '.number_format($this->redondearHermes($ivudica), 2, '.', ',').' '.$param['currency'].br(1).'</td> </tr>
                                   </table>                                
                                </section>
                            </div>
                            <div class="row">
                                <section class="col col-8"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetNumChar]] text-input',
                                                                                 'name'        => 'AVITit7',
                                                                                 'id'          => 'AVITit7',                                                                                 
                                                                                 'value'       => $datos['APVI'][0]['AVITit7'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>                                                           
                                 </section>        
                                 <section class="col col-10"><label class="textarea">'.
                                                                form_textarea(array('class' => 'validate[required,custom[onlyLetNumChar]] text-input',
                                                                                    'rows'  => "3",
                                                                                    'name'  => 'AVIText7',
                                                                                    'id'    => 'AVIText7',
                                                                                    'value' => $datos['APVI'][0]['AVIText7'] )).'
                                                            </label>
                                </section>
                            </div>
                            <div class="row">
                                <section class="col col-10">';
                                $plans        = empty($datos['land'])?0:$datos['land'][0]['plans'];
                                $plansFT      = empty($datos['land'])?0:$datos['land'][0]['plans']*$datos['ft'];
                                $far          = $datos['APVI'][0]['AVIfar'];
                                $mf           = $datos['APVI'][0]['AVImf'];
                                $fArea        = $datos['APVI'][0]['AVIfArea'];
                                $tls          = $far * $fArea;
                                $exl          = ($plans-$tls)>0?($plans-$tls):0;
                                $uvel         = $datos['iv']['indicated_value_cls'] * $mf;
                                $vel          = $uvel * $exl;
                                $AVIivudicael = $ivudica + $vel;
                    $coreAPVI .=  '<table border="1" cellspacing="0" cellpadding="0" width="100%">
                                        <tr ><td class="leftCell">'.$param['far'].'</td><td class="leftCell" colspan="2">'.form_input(array('class'      => 'conborde validate[custom[number]] text-input',
                                                                                                                                              'name'       => 'AVIfar',
                                                                                                                                              'id'         => 'AVIfar',
                                                                                                                                              'placeholder'=> '0.0',
                                                                                                                                              'value'      => $datos['APVI'][0]['AVIfar'],
                                                                                                                                              'size'       => '5',
                                                                                                                                              'onChange'   => 'calcularExLand(this,'.$datos['iv']['indicated_value_cls'].','.$plans.','.$datos['ft'].')',
                                                                                                                                              'maxlength'  => '5')).'</td></tr>
                                        <tr ><td class="leftCell">'.$param['mf'].'</td><td class="leftCell" colspan="2">'.form_input(array('class'      => 'conborde validate[custom[number]] text-input',
                                                                                                                                              'name'       => 'AVImf',
                                                                                                                                              'id'         => 'AVImf',
                                                                                                                                              'placeholder'=> '0.0',
                                                                                                                                              'value'      => $datos['APVI'][0]['AVImf'],
                                                                                                                                              'size'       => '5',
                                                                                                                                              'onChange'   => 'calcularExLand(this,'.$datos['iv']['indicated_value_cls'].','.$plans.','.$datos['ft'].')',
                                                                                                                                              'maxlength'  => '5')).'</td></tr>
                                        <tr ><td class="leftCell">'.nbs(1).'          </td><td class="centerCell">'.br(1).'sqm'.br(1).'</td> <td class="centerCell">'.br(1).'sqf'.br(1).'</td> </tr>
                                        <tr ><td class="leftCell">'.$param['ls'].'    </td><td class="leftCell"> '.number_format($plans, 2, '.', ',').' </td> <td class="rightCell">'.number_format($plansFT, 2, '.', ',').' </td> </tr>
                                        <tr ><td class="leftCell">'.$param['fArea'].' </td><td class="leftCell"> '.form_input(array('class'      => 'cmask conborde',
                                                                                                                                      'name'       => 'AVIfAreaMask',
                                                                                                                                      'id'         => 'AVIfAreaMask',
                                                                                                                                      'placeholder'=> '0.0',
                                                                                                                                      'value'      => $fArea,
                                                                                                                                      'size'       => '10',
                                                                                                                                      'onChange'   => 'calcularExLand(this,'.$datos['iv']['indicated_value_cls'].','.$plans.','.$datos['ft'].')',
                                                                                                                                      'maxlength'  => '20')).'  <input type="hidden" name="AVIfArea" id="AVIfArea" value="'.$fArea.'"> </td> <td class="rightCell"><label id="AVIfAreaft">'.number_format($fArea*$datos['ft'], 2, '.', ',').' </label></td> </tr>
                                        <tr ><td class="leftCell">'.$param['tls'].' </td><td class="leftCell"><label id="tls">'.number_format($tls, 2, '.', ',').'</label></td> <td class="rightCell"><label id="tlsft">'.number_format($tls*$datos['ft'], 2, '.', ',').'</label></td> </tr>
                                        <tr ><td class="leftCell">'.$param['exl'].' </td><td class="leftCell"><label id="exl">'.number_format($exl, 2, '.', ',').'</label></td> <td class="rightCell"><label id="exlft">'.number_format($exl*$datos['ft'], 2, '.', ',').'</label></td> </tr>    
                                        <tr ><td class="leftCell">'.$param['uvel'].'</td><td class="leftCell">$ <label id="uvel">'.number_format($uvel, 2, '.', ',').'</label> /sqm</td> <td class="rightCell">$ <label id="uvelft">'.number_format($uvel/$datos['ft'], 2, '.', ',').'</label> /sqf</td> </tr>    
                                        <tr ><td class="leftCell">'.$param['vel'].' </td><td class="leftCell">$ <label id="vel">'.number_format($vel, 2, '.', ',').'</label></td> <td class="rightCell">$ <label id="velft">'.number_format(($exl*$datos['ft'])*($uvel/$datos['ft']), 2, '.', ',').'</label></td> </tr>
                                        <tr ><td class="centerCell" colspan="2">'.br(1).$param['ivudica'].br(1).$param['ivudicael'].br(1).'</td> <td class="centerCell">'.br(1).'$ <label id="ivudicaelLbl">'.number_format($AVIivudicael, 2, '.', ',').'</label>  '.$param['currency'].form_input(array('name'=>'AVIivudicael', 'type'=>'hidden', 'id'=>'AVIivudicael', 'value'=>$AVIivudicael )).br(1).'</td> </tr>
                                        <tr ><td class="centerCell apBottom" colspan="2">'.br(1).$param['roun'].br(1).'</td> <td class="centerCell apBottom">'.br(1).'$ '.number_format($this->redondearHermes($AVIivudicael), 2, '.', ',').'  '.$param['currency'].br(1).'</td> </tr>
                                   </table>                                
                                </section>
                            </div>
                      </fieldset>
                     ';
                 
        return $coreAPVI;
        } catch (Exception $e) {echo 'creaCoreAPVI Excepción: ',  $e, "\n";}  
    }
    
    public function creaCoreAPVII($datos,$param) {
    try{$coreAPVII   = "";
        $coreAPVIII  = "";
        $coreAPIX   = "";
        $coreAPX    = "";
        
        $coreAPVII .=  '<fieldset>                  
                            <div class="row">                                 
                                 <section class="col col-10"><label class="textarea">'.
                                                                form_textarea(array('class' => 'validate[required,custom[onlyLetterNumber]] text-input richTextAPVII',
                                                                                    'rows'  => "5",
                                                                                    'name'  => 'AVIIText1',
                                                                                    'id'    => 'AVIIText1',
                                                                                    'value' => $datos['APVII'][0]['AVIIText1'] )).'
                                                            </label>
                                   </section>
                            </div> 
                            <div class="row">                                 
                                 <section class="col col-10"><label class="textarea">'.
                                                                form_textarea(array('class' => 'validate[required,custom[onlyLetterNumber]] text-input richTextAPVII',
                                                                                    'rows'  => "5",
                                                                                    'name'  => 'AVIIText2',
                                                                                    'id'    => 'AVIIText2',
                                                                                    'value' => $datos['APVII'][0]['AVIIText2'] )).'
                                                            </label>
                                   </section>
                            </div> 
                            <div class="row">
                                <section class="col col-10">';                                
                                $ra           = empty($datos['totArea'][0])?0:$datos['totArea'][0]['totTRA'];
                                $uav          = empty($datos['iv']['indicated_value_crs'])?0:$datos['iv']['indicated_value_crs'];

                               
                                $ivusca       = $ra * $uav;

                                $uav = number_format($uav, 2, '.', ',');

                           
                                //$uav = $tmpValue = $CI->session->userdata('totalIVM');

        $coreAPVII .=  '        <table border="1" cellspacing="0" cellpadding="0" width="100%">
                                    <tr><td class="leftCell">'.$param['saop'].'  </td><td class="leftCell">'.number_format($ra, 2, '.', ',').' m2</td></tr>
                                    <tr><td class="leftCell">'.$param['uav'].'   </td><td class="leftCell">'.form_input(array('class' => 'pmask conborde', 'name' => 'AVIIuavMask', 'id' => 'AVIIuavMask', 'disabled' => 'disabled',   'placeholder'=> '0.0', 'value' => $uav,'size' => '10','onChange' => 'calcularivusca('.$ra.',\''.base_url().'\')','maxlength' => '20')).'  '.$param['currency'].'/m2'
                                                                                                            .form_input(array('type'  => 'hidden','name' => 'AVIIuav', 'id' => 'AVIIuav', 'placeholder'=> '0.0', 'value' => $uav)).'</td></tr>                                        
                                    <tr><td class="leftCell"> '.$param['ivusca'].'</td><td class="leftCell">$ <label id="AVIIivuscaLbl"     >'.number_format($ivusca, 2, '.', ',').'                        </label>  '.$param['currency'].form_input(array('name'  => 'AVIIivusca', 'id' => 'AVIIivusca', 'type'=> 'hidden','value' => $ivusca )).'</td></tr>
                                    <tr><td class="rightCell apBottom">'.br(2).$param['roun'].nbs(2).'</td><td class="leftCell apBottom">'.br(2).'$ <label id="AVIIivuscaRoundLbl">'.number_format($this->redondearHermes($ivusca), 2, '.', ',').'</label> '.$param['currency'].'</td></tr>
                                </table>
                                </section>
                            </div>
                      </fieldset>
                     ';
        
        $coreAPVIII .=  '<fieldset>                  
                                 <div class="row">                                
                                 <section class="col col-10"><label class="textarea">'.
                                                                form_textarea(array('class' => 'validate[required,custom[onlyLetterNumber]] text-input  richTextAPVII',
                                                                                    'rows'  => "5",
                                                                                    'name'  => 'AVIIIText1',
                                                                                    'id'    => 'AVIIIText1',
                                                                                    'value' => $datos['APVII'][0]['AVIIIText1'] )).'
                                                            </label>
                                   </section>                                   
                                </div>
                                <div class="row">
                                <section class="col col-10">';  
                                 $cavweo =  empty($datos['costos'][0])?0:($datos['costos'][0]['AV_vica'] + $datos['costos'][0]['AV_oe']);
                                 
        $coreAPVIII .=  '        <table border="1" cellspacing="0" cellpadding="0" width="100%">
                                    <tr><td class="leftCell">'.$param['cavweo'].'  </td><td class="leftCell">$ '.number_format(empty($cavweo)?0:$cavweo, 2, '.', ',').' '.$param['currency'].'</td></tr>
                                    <tr><td class="rightCell apBottom">'.br(2).$param['roun'].nbs(2).'</td><td class="leftCell apBottom">'.br(2).'$ '.number_format($this->redondearHermes(empty($cavweo)?0:$cavweo), 2, '.', ',').' '.$param['currency'].'</td></tr>
                                </table>
                                </section>
                            </div>
                      </fieldset>';
                      
        $coreAPIX  .=  '<fieldset>                  
                                 <div class="row">                                    
                                 <section class="col col-10"><label class="textarea">'.
                                                                form_textarea(array('class' => 'validate[required,custom[onlyLetterNumber]] text-input  richTextAPVII',
                                                                                    'rows'  => "5",
                                                                                    'name'  => 'AIXText1',
                                                                                    'id'    => 'AIXText1',
                                                                                    'value' => $datos['APVII'][0]['AIXText1'] )).'
                                                            </label>
                                   </section>'.br(1);
                                   $totTRA      = (empty($datos['totArea'][0]['totTRA'])?1:$datos['totArea'][0]['totTRA']);
                                   $ca        = empty($datos['costos'][0]['AV_vica'])?0:$datos['costos'][0]['AV_vica'];
                                   $caUV      = $ca / $totTRA;
                                   $ivudicael = empty($datos['income'][0]['AVIivudicael'])?0:$datos['income'][0]['AVIivudicael'];
                                   $incomeUV  = $ivudicael / $totTRA;
                                   
                                   $ivuscaUV  = $ivusca / $totTRA;
                                   $cavweoUV  = $cavweo / $totTRA;
                                   $dcf       = empty($datos['APVII'][0]['AXnum3'])?0:$datos['APVII'][0]['AXnum3'];
                                   $dcfUV     = $dcf / $totTRA;
        $coreAPIX  .=  '
                                   <table border="1" cellspacing="0" cellpadding="0" width="100%">
                                    <tr><td class="centerCell apBottom">'.$param['valAp'].' </td><td class="centerCell apBottom">'.($param['currencyApp']).'                                 </td><td class="centerCell apBottom">'.$param['tUV'].' (USD / m2)</td></tr>
                                    <tr><td class="leftCell   apBottom">'.$param['costAp'].'</td><td class="centerCell apBottom">$ '.number_format($ca, 2, '.', ',').'</td><td class="centerCell apBottom">$ '.number_format($caUV, 2, '.', ',').'</td></tr>
                                    <tr><td class="leftCell   apBottom">'.$param['ica'].'   </td><td class="centerCell apBottom">$ '.number_format($ivudicael, 2, '.', ',').'</td><td class="centerCell apBottom">$ '.number_format($incomeUV, 2, '.', ',').' </td></tr>
                                    <tr><td class="leftCell   apBottom">'.$datos['APVII'][0]['AXTit3'].'</td><td class="centerCell apBottom">$ <label id="AXTit3Sum">'.number_format($dcf, 2, '.', ',').'</label> </td><td class="centerCell apBottom">$ <label id="AXTit3SumUV">'.number_format($dcfUV, 2, '.', ',').'</label> '.form_input(array('type'  => 'hidden','name' => 'totTRA', 'id' => 'totTRA', 'value' => $totTRA)).' </td></tr>
                                    <tr><td class="leftCell   apBottom">'.$param['sca'].'   </td><td class="centerCell apBottom">$ '.number_format($ivusca, 2, '.', ',').' </td><td class="centerCell apBottom">$ '.number_format($ivuscaUV, 2, '.', ',').' </td></tr>
                                    <tr><td class="leftCell   apBottom">'.$param['vis'].'   </td><td class="centerCell apBottom">$ '.number_format($cavweo, 2, '.', ',').'  </td><td class="centerCell apBottom">$ '.number_format($cavweoUV, 2, '.', ',').' </td></tr>
                                </table>
                                </div>
                      </fieldset>';
                      
        $coreAPX  .=  '<fieldset>                  
                                 <div class="row">                                      
                                 <section class="col col-10"><label class="textarea">'.
                                                                form_textarea(array('class' => 'validate[required,custom[onlyLetterNumber]] text-input  richTextAPVII',
                                                                                    'rows'  => "5",
                                                                                    'name'  => 'AXText1',
                                                                                    'id'    => 'AXText1',
                                                                                    'value' => $datos['APVII'][0]['AXText1'] )).'
                                                            </label>
                                   </section>
                                   <section class="col col-10"><label class="textarea">'.
                                                                form_textarea(array('class' => 'validate[required,custom[onlyLetterNumber]] text-input  richTextAPVII',
                                                                                    'rows'  => "5",
                                                                                    'name'  => 'AXText2',
                                                                                    'id'    => 'AXText2',
                                                                                    'value' => $datos['APVII'][0]['AXText2'] )).'
                                                            </label>
                                   </section>
                                </div>'.br(1);
                                    $ponderacionMV1 = ($datos['APVII'][0]['AXpor1']/100) * $ca;
                                    $ponderacionMV2 = ($datos['APVII'][0]['AXpor2']/100) * $ivudicael;
                                    $ponderacionMV3 = ($datos['APVII'][0]['AXpor3']/100) * $datos['APVII'][0]['AXnum3'];
                                    $ponderacionMV5 = ($datos['APVII'][0]['AXpor5']/100) * $ivusca;
                                    $AXmarketVal    = $ponderacionMV1 + $ponderacionMV2 + $ponderacionMV3  + $ponderacionMV5;
                                    $totPor         = $datos['APVII'][0]['AXpor1'] + $datos['APVII'][0]['AXpor2'] + $datos['APVII'][0]['AXpor3']+ $datos['APVII'][0]['AXpor5'];
                                    $valtotPor      = ($totPor==100)?"<img src=\"".base_url()."images/check.png\">".nbs(2):"<img src=\"".base_url()."images/close.png\">".nbs(2);                                    
                                    $ponderacionMV4 = ($datos['APVII'][0]['AXpor4']/100) * $cavweo;
                                    $ch1            = ($datos['APVII'][0]['AXTit1Chkbx']=="1"?"checked":"");
                                    $ch2            = ($datos['APVII'][0]['AXTit2Chkbx']=="1"?"checked":"");
                                    $ch3            = ($datos['APVII'][0]['AXTit3Chkbx']=="1"?"checked":"");
                                    $ch4            = ($datos['APVII'][0]['AXTit4Chkbx']=="1"?"checked":"");
                                    
        $coreAPX  .=  '         <div class="row">                                      
                                 <section class="col col-11">
                                    <table border="1" cellspacing="0" cellpadding="0" width="100%">
                                    <tr><td class="leftCell" colspan="4" width="100%">'.$param['marval'].br(1).'</td></tr>
                                    <tr><td class="centerCell"           width="5%" ><label class="checkbox"><input type="checkbox" name="AXTit1Chkbx" id="AXTit1Chkbx" '.$ch1.' value="1"><i></i></label></td>'
                . '                     <td class="leftCell"             width="40%"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.form_input(array('class' => 'validate[required,custom[onlyLetNumChar]] text-input', 'name' => 'AXTit1', 'id' => 'AXTit1','value' => $datos['APVII'][0]['AXTit1'],'maxlength' => '60')).'<b class="tooltip tooltip-bottom-right">Subtitutlo</b></label></td>
                                        <td class="centerCell"           width="25%">$ '.number_format($ca, 2, '.', ',').form_input(array('name'  => 'AXnum1', 'id' => 'AXnum1', 'type'=> 'hidden','value' => $ca )).'</td><td class="leftCell" width="15%">'.form_input(array('class' => 'conborde', 'name' => 'AXpor1', 'id' => 'AXpor1', 'placeholder'=> '0.0', 'value' => $datos['APVII'][0]['AXpor1'],'size' => '3','onChange' => 'calcularMarketValue(this,\''.base_url().'\', \'AXmarketValLblRH\',false)','maxlength' => '20')).' %</td><td class="leftCell" width="25%">$ <label id="ponderacionMV1">'.number_format($ponderacionMV1, 2, '.', ',').'</label> '.$param['currency'].form_input(array('type'  => 'hidden','name' => 'AX_C_Approach', 'id' => 'AX_C_Approach', 'value' => $ponderacionMV1)).'</td></tr>
                                    <tr><td class="centerCell"           width="5%" ><label class="checkbox"><input type="checkbox" name="AXTit2Chkbx" id="AXTit2Chkbx" '.$ch2.' value="1"><i></i></label></td>'
                . '                     <td class="leftCell"             width="40%"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.form_input(array('class' => 'validate[required,custom[onlyLetNumChar]] text-input', 'name' => 'AXTit2', 'id' => 'AXTit2','value' => $datos['APVII'][0]['AXTit2'],'maxlength' => '60')).'<b class="tooltip tooltip-bottom-right">Subtitutlo</b></label></td>
                                        <td class="centerCell"                      > $ '.number_format($ivudicael, 2, '.', ',').form_input(array('name'  => 'AXnum2', 'id' => 'AXnum2', 'type'=> 'hidden','value' => $ivudicael )).'</td><td class="leftCell">'.form_input(array('class' => 'conborde', 'name' => 'AXpor2', 'id' => 'AXpor2', 'placeholder'=> '0.0', 'value' => $datos['APVII'][0]['AXpor2'],'size' => '3','onChange' => 'calcularMarketValue(this,\''.base_url().'\', \'AXmarketValLblRH\',false)','maxlength' => '20')).' %</td><td class="leftCell">$ <label id="ponderacionMV2">'.number_format($ponderacionMV2, 2, '.', ',').'</label> '.$param['currency'].form_input(array('type'  => 'hidden','name' => 'AX_IC_Approach', 'id' => 'AX_IC_Approach', 'value' => $ponderacionMV2)).'</td></tr>
                                    <tr><td class="centerCell"           width="5%" ><label class="checkbox"><input type="checkbox" name="AXTit3Chkbx" id="AXTit3Chkbx" '.$ch3.' value="1"><i></i></label></td>'
                . '                     <td class="leftCell"             width="40%"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.form_input(array('class' => 'validate[required,custom[onlyLetNumChar]] text-input', 'name' => 'AXTit3', 'id' => 'AXTit3','value' => $datos['APVII'][0]['AXTit3'],'maxlength' => '60')).'<b class="tooltip tooltip-bottom-right">Subtitutlo</b></label></td>
                                        <td class="centerCell"                        >'.form_input(array('class' => 'pmask conborde', 'name' => 'AXnum3Mask', 'id' => 'AXnum3Mask', 'placeholder'=> '0.0', 'value' => $datos['APVII'][0]['AXnum3'],'size' => '9','onChange' => 'calcularMarketValue(this,\''.base_url().'\', \'AXmarketValLblRH\',true)','maxlength' => '20')).' '.$param['currency']   
                                                                                        .form_input(array('type'  => 'hidden','name' => 'AXnum3', 'id' => 'AXnum3', 'value' => $datos['APVII'][0]['AXnum3'])).'</td><td class="leftCell">'.form_input(array('class' => 'conborde', 'name' => 'AXpor3', 'id' => 'AXpor3', 'placeholder'=> '0.0', 'value' => $datos['APVII'][0]['AXpor3'],'size' => '3','onChange' => 'calcularMarketValue(this,\''.base_url().'\', \'AXmarketValLblRH\',false)','maxlength' => '20')).' %</td><td class="leftCell">$ <label id="ponderacionMV3">'.number_format($ponderacionMV3, 2, '.', ',').'</label> '.$param['currency'].form_input(array('type'  => 'hidden','name' => 'AX_DCF_Approach', 'id' => 'AX_DCF_Approach', 'value' => $ponderacionMV3)).'</td></tr>
                                    <tr><td class="centerCell"           width="5%" ><label class="checkbox"><input type="checkbox" name="AXTit4Chkbx" id="AXTit4Chkbx" '.$ch4.' value="1"><i></i></label></td>'
                . '                     <td class="leftCell"             width="40%"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.form_input(array('class' => 'validate[required,custom[onlyLetNumChar]] text-input', 'name' => 'AXTit4', 'id' => 'AXTit4','value' => $datos['APVII'][0]['AXTit4'],'maxlength' => '60')).'<b class="tooltip tooltip-bottom-right">Subtitutlo</b></label></td>
                                        <td class="centerCell"                       >$ '.number_format($ivusca, 2, '.', ',').form_input(array('name'  => 'AXnum5', 'id' => 'AXnum5', 'type'=> 'hidden','value' => $ivusca )).'</td><td class="leftCell">'.form_input(array('class' => 'conborde', 'name' => 'AXpor5', 'id' => 'AXpor5', 'placeholder'=> '0.0', 'value' => $datos['APVII'][0]['AXpor5'],'size' => '3','onChange' => 'calcularMarketValue(this,\''.base_url().'\', \'AXmarketValLblRH\',false)','maxlength' => '20')).' %</td><td class="leftCell">$ <label id="ponderacionMV5">'.number_format($ponderacionMV5, 2, '.', ',').'</label> '.$param['currency'].form_input(array('type'  => 'hidden','name' => 'AX_SC_Approach', 'id' => 'AX_SC_Approach', 'value' => $ponderacionMV5)).'</td></tr>
                                    <tr><td class="leftCell" colspan="3"             >'.nbs(1).'</td><td class="leftCell"><label id="totPorMV" class=\''.(($totPor == 100)?"msjOkCentro":"msjErrCentro").'\'>'.br(1).$valtotPor.number_format($totPor,2, '.', ',' ).' %</label></td><td class="leftCell">'.nbs(1).'</td></tr>
                                    <tr><td class="leftCell" colspan="4"             >'.$param['marval'].'of the property</td><td class="leftCell">$ <label id="AXmarketValLbl">'.number_format($AXmarketVal, 2, '.', ',').'</label> '.$param['currency'].form_input(array('type'  => 'hidden','name' => 'AXmarketValField', 'id' => 'AXmarketValField', 'value' => $AXmarketVal)).'</td></tr>                                    
                                    <tr><td class="rightCell apBottom" colspan="4"   >'.br(2).$param['roun'].nbs(2).'</td><td class="leftCell apBottom">'.br(2).'$ <label id="AXmarketValLblRH">'.number_format($this->redondearHermes(empty($AXmarketVal)?0:$AXmarketVal), 2, '.', ',').'</label> '.$param['currency'].'</td></tr>
                                </table>'.br(2).'
                                <table border="1" cellspacing="0" cellpadding="0" width="100%">
                                    <tr><td class="leftCell" colspan="4"><strong>'.$param['vis'].br(1).'</strong></td></tr>
                                    <tr><td class="leftCell">'.$param['vis'].'</td>
                                        <td class="leftCell">$ '.number_format($cavweo, 2, '.', ',').'</td><td class="leftCell">'.form_input(array('class' => 'conborde', 'name' => 'AXpor4', 'id' => 'AXpor4', 'placeholder'=> '0.0', 'value' => $datos['APVII'][0]['AXpor4'],'size' => '5','onChange' => 'calcularVis(this,'.$cavweo.',\''.base_url().'\',\'visLbl\')','maxlength' => '20')).' %</td><td class="leftCell">$ <label id="ponderacionMV4">'.number_format($ponderacionMV4, 2, '.', ',').'</label> '.$param['currency'].form_input(array('type'  => 'hidden','name' => 'AX_VU_Approach', 'id' => 'AX_VU_Approach', 'value' => $ponderacionMV4)).'</td></tr>
                                    <tr><td class="rightCell apBottom" colspan="3">'.br(2).$param['roun'].nbs(2).'</td><td class="leftCell apBottom">'.br(2).'$ <label id="visLbl">'.number_format($this->redondearHermes(empty($ponderacionMV4)?0:$ponderacionMV4), 2, '.', ',').'</label> '.$param['currency'].'</td></tr>
                                </table>'.br(1).'            
                                 </section>
                      </fieldset>';
                 
        return array("coreAPVII" => $coreAPVII, "coreAPVIII" => $coreAPVIII, "coreAPIX"=> $coreAPIX, "coreAPX" => $coreAPX);
        } catch (Exception $e) {echo 'creaCoreAPVII Excepción: ',  $e, "\n";} 
    }
    
    public function creaCoreAPVIII($datos,$param, $datos2) {
    try{$coreAPXI   = "";
        $coreAPXII  = "";
        $marketVal  = empty($datos['mv'])?0:$datos['mv'][0]['AXmarketVal'];
        $totTRA     = (empty($datos['totArea'][0]['totTRA'])?1:$datos['totArea'][0]['totTRA']);


         $ra           = empty($datos2['totArea'][0])?0:$datos2['totArea'][0]['totTRA'];
                                $uav          = empty($datos2['iv']['indicated_value_crs'])?0:$datos2['iv']['indicated_value_crs'];
                                $ivusca       = $ra * $uav;

  $totTRA      = (empty($datos2['totArea'][0]['totTRA'])?1:$datos2['totArea'][0]['totTRA']);
                                   $ca        = empty($datos2['costos'][0]['AV_vica'])?0:$datos2['costos'][0]['AV_vica'];
                                   $caUV      = $ca / $totTRA;
                                   $ivudicael = empty($datos2['income'][0]['AVIivudicael'])?0:$datos2['income'][0]['AVIivudicael'];
                                   $incomeUV  = $ivudicael / $totTRA;
                                   
                                   //$ivuscaUV  = $ivusca / $totTRA;
                                  // $cavweoUV  = $cavweo / $totTRA;
                                  // $dcf       = empty($datos2['APVII'][0]['AXnum3'])?0:$datos2['APVII'][0]['AXnum3'];
                                  // $dcfUV     = $dcf / $totTRA;


          $ponderacionMV1 = ($datos2['APVII'][0]['AXpor1']/100) * $ca;
          $ponderacionMV2 = ($datos2['APVII'][0]['AXpor2']/100) * $ivudicael;
          $ponderacionMV3 = ($datos2['APVII'][0]['AXpor3']/100) * $datos2['APVII'][0]['AXnum3'];
          $ponderacionMV5 = ($datos2['APVII'][0]['AXpor5']/100) * $ivusca;
          $AXmarketVal    = $ponderacionMV1 + $ponderacionMV2 + $ponderacionMV3  + $ponderacionMV5;

         $valMarket = number_format((empty($AXmarketVal)?0:$AXmarketVal), 2, '.', ',');




        $coreAPXI  .= '<fieldset>                  
                                 <div class="row">
                                 <section class="col col-10"><label class="textarea">'.
                                                                form_textarea(array('class' => 'validate[required,custom[onlyLetterNumber]] text-input  richTextAPVIII',
                                                                                    'rows'  => "5",
                                                                                    'name'  => 'AXIText1',
                                                                                    'id'    => 'AXIText1',
                                                                                    'value' => $datos['APVIII'][0]['AXIText1'] )).'
                                                            </label>
                                   </section>
                                </div>
                                <div class="row">
                                 <section class="col col-10"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetNumChar]] text-input',
                                                                                 'name'        => 'AXIText2',
                                                                                 'id'          => 'AXIText2',
                                                                                 'value'       => $datos['APVIII'][0]['AXIText2'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>
                                 </section>
                                 <section class="col col-8"><label class="textarea">'.
                                                                form_textarea(array('class' => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                                                    'rows'  => "5",
                                                                                    'name'  => 'AXIText3',
                                                                                    'id'    => 'AXIText3',
                                                                                    'value' => $datos['APVIII'][0]['AXIText3'] )).'
                                                            </label>
                                 </section>
                                 <section class="col col-3"><label class="input">$ '. $valMarket .' '.$param['currency'].'</label>
                                 </section>
                                </div>
                                <div class="row">
                                 <section class="col col-10"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetNumChar]] text-input',
                                                                                 'name'        => 'AXIText4',
                                                                                 'id'          => 'AXIText4',                                                                                 
                                                                                 'value'       => $datos['APVIII'][0]['AXIText4'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>
                                 </section>
                                </div>
                                <div class="row">
                                 <section class="col col-10"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetNumChar]] text-input',
                                                                                 'name'        => 'AXIText5',
                                                                                 'id'          => 'AXIText5',                                                                                 
                                                                                 'value'       => $datos['APVIII'][0]['AXIText5'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>
                                 </section>
                                </div>
                                <div class="row">
                                 <section class="col col-10"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetNumChar]] text-input',
                                                                                 'name'        => 'AXIText6',
                                                                                 'id'          => 'AXIText6',                                                                                 
                                                                                 'value'       => $datos['APVIII'][0]['AXIText6'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>
                                 </section>
                                </div>
                                <div class="row">
                                 <section class="col col-10"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetNumChar]] text-input',
                                                                                 'name'        => 'AXIText7',
                                                                                 'id'          => 'AXIText7',                                                                                 
                                                                                 'value'       => $datos['APVIII'][0]['AXIText7'],
                                                                                 'maxlength'   => '80')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>
                                 </section>
                                </div>';
                                $totRate     = $datos['APVIII'][0]['AXIRate1'] + $datos['APVIII'][0]['AXIRate2'] + $datos['APVIII'][0]['AXIRate3'] + $datos['APVIII'][0]['AXIRate4'] + $datos['APVIII'][0]['AXIRate5'];
                                $difDR       = round($totRate, 0) - $datos['APVIII'][0]['AXIRate1'];
                                //$ocdr        = $difDR + $datos['APVIII'][0]['AXIRate9'];
                                $ocdr        = $totRate;


                                $ocdrMonth   = $ocdr / 12 ;                                
                                $AXISaleTerm = empty($datos['APVIII'][0]['AXISaleTerm'])?0:$datos['APVIII'][0]['AXISaleTerm'];
                                $mt          = (empty($datos['frontPage'][0]['mt'])?0:$datos['frontPage'][0]['mt']);
                                $restaMT     = ($mt- $AXISaleTerm); 
                                $restaMT     = $restaMT<0?($restaMT*-1):$restaMT;

                                //$oppCos      = $marketVal - ( $marketVal / pow((1 + ( $ocdrMonth / 100)), $restaMT ) );
                                $oppCos      = ($ocdrMonth/100) * ($mt - $AXISaleTerm) * $marketVal;
                                $rate6Month  = $datos['APVIII'][0]['AXIRate6'] / 12;
                                $manCost     = ($rate6Month/100) * ($mt-$AXISaleTerm)* $marketVal;
                               // $manCost     = ($marketVal); //* $AXISaleTerm * $marketVal;
                                $saleCost    = ($datos['APVIII'][0]['AXIRate7']/100) * $marketVal;
                                $rate8Month  = $datos['APVIII'][0]['AXIRate8'] / 12;
                                $properTax   = ($rate8Month/100) * ($mt - $AXISaleTerm) * $marketVal;
                                $liquidVal   = $marketVal - $oppCos - $manCost - $saleCost - $properTax;
                                //$liquidFactor= $marketVal==0?0:($marketVal - $liquidVal) / $marketVal;
                                $liquidFactor= $marketVal==0?0:(1-($marketVal - $liquidVal) / $marketVal);
                                
                  $coreAPXI .=  '<div class="row">
                                  <section class="col col-3"><label class="input">'.nbs(1).' </label></section>
                                  <section class="col col-4"><label class="input">'.$param['sugRate'].' </label></section>
                                  <section class="col col-3"><label class="input"><i class="icon-append fa fa-percent"></i>'.
                                                                form_input(array('class'       => 'sumRate',
                                                                                 'name'        => 'AXIRate1',
                                                                                 'id'          => 'AXIRate1',
                                                                                 'value'       => $datos['APVIII'][0]['AXIRate1'],
                                                                                 'onChange'    => 'sumarRate(this,\''.base_url().'\',\'totRateLbl\','.$marketVal.','.$totTRA.','.$datos['ft'].')',
                                                                                 'maxlength'   => '10')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>
                                   </section>
                                 </div>
                                 <div class="row">
                                 <section class="col col-10"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetNumChar]] text-input',
                                                                                 'name'        => 'AXIText8',
                                                                                 'id'          => 'AXIText8',                                                                                 
                                                                                 'value'       => $datos['APVIII'][0]['AXIText8'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>
                                 </section>
                                 <section class="col col-10"><label class="textarea">'.
                                                                form_textarea(array('class' => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                                                    'rows'  => "5",
                                                                                    'name'  => 'AXIText9',
                                                                                    'id'    => 'AXIText9',
                                                                                    'value' => $datos['APVIII'][0]['AXIText9'] )).'
                                                            </label>
                                 </section>
                                 <section class="col col-10"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetNumChar]] text-input',
                                                                                 'name'        => 'AXIText10',
                                                                                 'id'          => 'AXIText10',                                                                                 
                                                                                 'value'       => $datos['APVIII'][0]['AXIText10'],
                                                                                 'maxlength'   => '80')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>
                                 </section>
                                 <section class="col col-3"><label class="input">'.nbs(1).' </label></section>
                                  <section class="col col-4"><label class="input">'.$param['sugRate'].' </label></section>
                                  <section class="col col-3"><label class="input"><i class="icon-append fa fa-percent"></i>'.
                                                                form_input(array('class'       => 'sumRate',
                                                                                 'name'        => 'AXIRate2',
                                                                                 'id'          => 'AXIRate2',                                                                                 
                                                                                 'value'       => $datos['APVIII'][0]['AXIRate2'],
                                                                                 'onChange'    => 'sumarRate(this,\''.base_url().'\',\'totRateLbl\','.$marketVal.','.$totTRA.','.$datos['ft'].')',
                                                                                 'maxlength'   => '10')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>
                                   </section>
                                   </div>
                                   <div class="row">
                                   <section class="col col-10"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetNumChar]] text-input',
                                                                                 'name'        => 'AXIText11',
                                                                                 'id'          => 'AXIText11',                                                                                 
                                                                                 'value'       => $datos['APVIII'][0]['AXIText11'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>
                                 </section>
                                 <section class="col col-10"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetNumChar]] text-input',
                                                                                 'name'        => 'AXIText12',
                                                                                 'id'          => 'AXIText12',
                                                                                 'value'       => $datos['APVIII'][0]['AXIText12'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>
                                 </section>
                                 <section class="col col-10"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetNumChar]] text-input',
                                                                                 'name'        => 'AXIText13',
                                                                                 'id'          => 'AXIText13',
                                                                                 'value'       => $datos['APVIII'][0]['AXIText13'],
                                                                                 'maxlength'   => '80')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>
                                 </section>
                                 <section class="col col-3"><label class="input">'.nbs(1).' </label></section>
                                  <section class="col col-4"><label class="input">'.$param['sugRate'].' </label></section>
                                  <section class="col col-3"><label class="input"><i class="icon-append fa fa-percent"></i>'.
                                                                form_input(array('class'       => 'sumRate',
                                                                                 'name'        => 'AXIRate3',
                                                                                 'id'          => 'AXIRate3',
                                                                                 'value'       => $datos['APVIII'][0]['AXIRate3'],
                                                                                 'onChange'    => 'sumarRate(this,\''.base_url().'\',\'totRateLbl\','.$marketVal.','.$totTRA.','.$datos['ft'].')',
                                                                                 'maxlength'   => '10')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>
                                   </section>
                                   </div>
                                   <div class="row">
                                   <section class="col col-10"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetNumChar]] text-input',
                                                                                 'name'        => 'AXIText14',
                                                                                 'id'          => 'AXIText14',
                                                                                 'value'       => $datos['APVIII'][0]['AXIText14'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>
                                 </section>
                                 <section class="col col-10"><label class="textarea">'.
                                                                form_textarea(array('class' => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                                                    'rows'  => "5",
                                                                                    'name'  => 'AXIText15',
                                                                                    'id'    => 'AXIText15',
                                                                                    'value' => $datos['APVIII'][0]['AXIText15'] )).'
                                                            </label>
                                 </section>
                                 <section class="col col-3"><label class="input">'.nbs(1).' </label></section>
                                  <section class="col col-4"><label class="input">'.$param['sugRate'].' </label></section>
                                  <section class="col col-3"><label class="input"><i class="icon-append fa fa-percent"></i>'.
                                                                form_input(array('class'       => 'sumRate',
                                                                                 'name'        => 'AXIRate4',
                                                                                 'id'          => 'AXIRate4',
                                                                                 'value'       => $datos['APVIII'][0]['AXIRate4'],
                                                                                 'onChange'    => 'sumarRate(this,\''.base_url().'\',\'totRateLbl\','.$marketVal.','.$totTRA.','.$datos['ft'].')',
                                                                                 'maxlength'   => '10')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>
                                   </section>
                                   </div>
                                   <div class="row">
                                   <section class="col col-7"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetNumChar]] text-input',
                                                                                 'name'        => 'AXIText16',
                                                                                 'id'          => 'AXIText16',
                                                                                 'value'       => $datos['APVIII'][0]['AXIText16'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>
                                 </section>
                                 <section class="col col-3"><label class="input"><i class="icon-append fa fa-percent"></i>'.
                                                                form_input(array('class'       => 'sumRate',
                                                                                 'name'        => 'AXIRate5',
                                                                                 'id'          => 'AXIRate5',
                                                                                 'value'       => $datos['APVIII'][0]['AXIRate5'],
                                                                                 'onChange'    => 'sumarRate(this,\''.base_url().'\',\'totRateLbl\','.$marketVal.','.$totTRA.','.$datos['ft'].')',
                                                                                 'maxlength'   => '10')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>
                                   </section>
                                 <section class="col col-10"><label class="textarea">'.
                                                                form_textarea(array('class' => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                                                    'rows'  => "6",
                                                                                    'name'  => 'AXIText17',
                                                                                    'id'    => 'AXIText17',
                                                                                    'value' => $datos['APVIII'][0]['AXIText17'] )).'
                                                            </label>
                                 </section>
                                 </div>
                                 <div class="row">
                                 <section class="col col-3"><label class="input">'.nbs(1).' </label></section>
                                 <section class="col col-4"><label class="input">'.$param['totRiskP'].' </label></section>
                                 <section class="col col-3"><label class="input" id="totRiskPLbl">'.number_format($totRate, 2, '.', ',').' %</label></section>
                                 </div>
                                 <div class="row">
                                  <section class="col col-3"><label class="input">'.nbs(1).' </label></section>
                                  <section class="col col-4"><label class="input">'.$param['discRate'].' </label></section>
                                  <section class="col col-3"><label class="input" id="discRateLbl">'.number_format($totRate, 2, '.', ',').' %</label></section>
                                 </div>
                                 <div class="row">
                                  <section class="col col-3"><label class="input">'.nbs(1).' </label></section>
                                  <section class="col col-4"><label class="input"><strong>'.$param['roun'].' </strong></label></section>
                                  <section class="col col-3"><strong><label class="input" id="totRateLbl">'.number_format(round($totRate, 0), 2, '.', ',').' %</label></strong></section>
                                 </div>                                 
                                 <div class="row">
                                    <section class="col col-10"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetNumChar]] text-input',
                                                                                 'name'        => 'AXIText18',
                                                                                 'id'          => 'AXIText18',
                                                                                 'value'       => $datos['APVIII'][0]['AXIText18'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>
                                    </section>                                    
                                    <section class="col col-7"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetNumChar]] text-input',
                                                                                 'name'        => 'AXIText19',
                                                                                 'id'          => 'AXIText19',
                                                                                 'value'       => $datos['APVIII'][0]['AXIText19'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>
                                    </section>
                                    <section class="col col-3"><label class="input"><i class="icon-append fa fa-percent"></i>'.
                                                                form_input(array('class'       => '',
                                                                                 'name'        => 'AXIRate6',
                                                                                 'id'          => 'AXIRate6',
                                                                                 'value'       => $datos['APVIII'][0]['AXIRate6'],
                                                                                 'onChange'    => 'sumarRate(this,\''.base_url().'\',\'totRateLbl\','.$marketVal.','.$totTRA.','.$datos['ft'].')',
                                                                                 'maxlength'   => '10')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>
                                    </section>
                                    <section class="col col-7"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetNumChar]] text-input',
                                                                                 'name'        => 'AXIText20',
                                                                                 'id'          => 'AXIText20',
                                                                                 'value'       => $datos['APVIII'][0]['AXIText20'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>
                                    </section>
                                    <section class="col col-3"><label class="input"><i class="icon-append fa fa-percent"></i>'.
                                                                form_input(array('class'       => '',
                                                                                 'name'        => 'AXIRate7',
                                                                                 'id'          => 'AXIRate7',
                                                                                 'value'       => $datos['APVIII'][0]['AXIRate7'],
                                                                                 'onChange'    => 'sumarRate(this,\''.base_url().'\',\'totRateLbl\','.$marketVal.','.$totTRA.','.$datos['ft'].')',
                                                                                 'maxlength'   => '10')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>
                                    </section>
                                    <section class="col col-7"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetNumChar]] text-input',
                                                                                 'name'        => 'AXIText21',
                                                                                 'id'          => 'AXIText21',
                                                                                 'value'       => $datos['APVIII'][0]['AXIText21'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>
                                    </section>
                                    <section class="col col-3"><label class="input"><i class="icon-append fa fa-percent"></i>'.
                                                                form_input(array('class'       => '',
                                                                                 'name'        => 'AXIRate8',
                                                                                 'id'          => 'AXIRate8',
                                                                                 'value'       => $datos['APVIII'][0]['AXIRate8'],
                                                                                 'onChange'    => 'sumarRate(this,\''.base_url().'\',\'totRateLbl\','.$marketVal.','.$totTRA.','.$datos['ft'].')',
                                                                                 'maxlength'   => '10')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>
                                    </section>
                                    <section class="col col-7"><label class="input"><i class="icon-append fa fa-bookmark"></i>'.
                                                                form_input(array('class'       => 'validate[required,custom[onlyLetNumChar]] text-input',
                                                                                 'name'        => 'AXIText22',
                                                                                 'id'          => 'AXIText22',
                                                                                 'value'       => $datos['APVIII'][0]['AXIText22'],
                                                                                 'maxlength'   => '60')).'
                                                                 <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                            </label>
                                    </section>
                                    <section class="col col-3"><label class="label">'.$param['estMonS'].'</label><label class="input"><i class="icon-append fa fa-calendar"></i>'.
                                                                form_input(array('class'       => '',
                                                                                 'name'        => 'AXISaleTerm',
                                                                                 'id'          => 'AXISaleTerm',
                                                                                 'value'       => $AXISaleTerm,
                                                                                 'onChange'    => 'sumarRate(this,\''.base_url().'\',\'totRateLbl\','.$marketVal.','.$totTRA.','.$datos['ft'].')',
                                                                                 'maxlength'   => '10')).'
                                                                 <b class="tooltip tooltip-bottom-right">Months</b>
                                                            </label>
                                    </section>
                                </div>
                                 <div class="row">
                                    <section class="col col-10">
                                        <table border="1" cellspacing="0" cellpadding="0" width="100%">
                                            <tr><td class="leftCell" colspan="4"><strong>'.br(1).$param['liqVal'].br(1).'</strong></td></tr>
                                            <tr><td class="leftCell">1.- '.$param['basVal'].' '.$param['currency'].'</td>
                                                <td class="leftCell">$ '.number_format($marketVal, 2, '.', ',').'</td>
                                                <td class="centerCell" colspan="2">'.$param['rfir'].'</td>
                                            </tr>
                                            <tr><td class="leftCell" colspan="2">'.nbs(1).'</td>
                                                <td class="leftCell" colspan="2"><section class="col col-7"><label class="input"><i class="icon-append fa fa-calendar"></i>'.
                                                                                        form_input(array('class'       => '',
                                                                                                         'name'        => 'AXIFecha',
                                                                                                         'id'          => 'AXIFecha',
                                                                                                         'value'       => $datos['APVIII'][0]['AXIFecha'],
                                                                                                         'maxlength'   => '60')).'
                                                                                         <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                                                    </label></section></td>
                                            </tr>
                                            <tr><td class="leftCell" colspan="2">'.$param['adPoin'].'</td>                                                
                                                <td class="leftCell" colspan="2"><section class="col col-7"><label class="input"><i class="icon-append fa fa-percent"></i>'.
                                                                                    form_input(array('class'       => 'conborde',
                                                                                                     'name'        => 'AXIRate9',
                                                                                                     'id'          => 'AXIRate9',
                                                                                                     'value'       => $datos['APVIII'][0]['AXIRate9'], 
                                                                                                     'onChange'    => 'sumarRate(this,\''.base_url().'\',\'totRateLbl\','.$marketVal.','.$totTRA.','.$datos['ft'].')',
                                                                                                     'maxlength'   => '10')).'
                                                                                     <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                                                                                </label></section></td>
                                            </tr>
                                            <tr><td class="leftCell" colspan="4">2.- '.$param['oppCos'].'</td></tr>
                                            <tr><td class="leftCell">Marketing Time</td>
                                                <td class="leftCell">'.$mt.' months</td>
                                                <td class="centerCell" colspan="2">'.$param['addRisk'].'</td>
                                            </tr>
                                            <tr><td class="leftCell">'.$param['estMonS'].'</td>
                                                <td class="leftCell" colspan="3"><label id="AXISaleTermLbl">'.$datos['APVIII'][0]['AXISaleTerm'].' months</label></td>
                                            </tr>
                                            <tr><td class="leftCell">'.form_input(array('type'  => 'hidden','name' => 'mt', 'id' => 'mt', 'value' => $mt)).'</td>
                                                <!--<td class="leftCell" style="color:red" colspan="3"><label id="restaMTx">'.($mt ).' months</label></td>-->
                                                 <!--<td class="leftCell" style="color:red" colspan="3"><label id="restaMTx">&nbsp;</label></td>-->
                                            </tr>
                                            <tr><td class="leftCell">'.$param['discRate'].'</td>
                                                <td class="leftCell"><label id="ocdrLbl">'.number_format($ocdr, 2, '.', ',').'</label> %</td>
                                                <td class="centerCell">'.$param['difRfir'].'</td>
                                                <td class="leftCell"><label id="difDRLbl">'.number_format($difDR, 2, '.', ',').'</label> %</td>
                                            </tr>
                                            <tr><td class="leftCell">Monthly risk free interest rate</td>
                                                <td class="leftCell" colspan="3"><label id="ocdrMonthLbl">'.number_format($ocdrMonth, 2, '.', ',').' </label>%</td>
                                            </tr>
                                            <tr><td class="leftCell" colspan="3"><strong>'.$param['oppCos'].'</strong></td>
                                                <td class="leftCell"            ><strong>$ <label id="oppCosLbl">'.number_format($oppCos, 2, '.', ',').'</label></strong></td>
                                            </tr>
                                            <tr><td class="leftCell" colspan="4">3.- '.$param['manCos'].'</td></tr>
                                            <tr><td class="leftCell">'.$param['perYear'].'</td>
                                                <td class="leftCell" colspan="3"><label id="AXIRate6Lbl">'.number_format($datos['APVIII'][0]['AXIRate6'], 2, '.', ',').'</label> %</td>
                                            </tr>
                                            <tr><td class="leftCell">'.$param['perMonth'].'</td>
                                                <td class="leftCell" colspan="3"><label id="rate6MonthLbl">'.number_format($rate6Month, 2, '.', ',').'</label> %</td>
                                            </tr>
                                            <tr><td class="leftCell" colspan="3"><strong>'.$param['manCos'].'</strong></td>
                                                <td class="leftCell"            ><strong>$ <label id="manCostLbl">'.number_format($manCost, 2, '.', ',').'</label> </strong></td>
                                            </tr>
                                            <tr><td class="leftCell" colspan="4">4.- '.$param['saleCos'].'</td></tr>
                                            <tr><td class="leftCell">'.$param['commSale'].'</td>
                                                <td class="leftCell" colspan="3"><label id="AXIRate7Lbl">'.number_format($datos['APVIII'][0]['AXIRate7'], 2, '.', ',').'</label> %</td>
                                            </tr>                                            
                                            <tr><td class="leftCell" colspan="3"><strong>'.$param['saleCos'].'</strong></td>
                                                <td class="leftCell"            ><strong>$ <label id="saleCostLbl">'.number_format($saleCost, 2, '.', ',').'</label> </strong></td>
                                            </tr>
                                            <tr><td class="leftCell" colspan="4">5.- '.$param['properTax'].'</td></tr>
                                            <tr><td class="leftCell">'.$param['perYear'].'</td>
                                                <td class="leftCell" colspan="3"><label id="AXIRate8Lbl">'.number_format($datos['APVIII'][0]['AXIRate8'], 2, '.', ',').'</label> %</td>
                                            </tr>
                                            <tr><td class="leftCell">'.$param['perMonth'].'</td>
                                                <td class="leftCell" colspan="3"><label id="rate8MonthLbl">'.number_format($rate8Month, 2, '.', ',').'</label> %</td>
                                            </tr>
                                            <tr><td class="leftCell" colspan="3"><strong>'.$param['properTax'].'</strong></td>
                                                <td class="leftCell"            ><strong>$ <label id="properTaxLbl">'.number_format($properTax, 2, '.', ',').'</label> </strong></td>
                                            </tr>
                                            <tr><td class="leftCell" colspan="4">6.- '.$param['liqVal'].'</td></tr>
                                            <tr><td class="leftCell" colspan="3"><strong>'.$param['liqVal'].'</strong></td>
                                                <td class="leftCell"            ><strong>$ <label id="liquidValLblTb">'.number_format($liquidVal, 2, '.', ',').'</label> </strong></td>
                                            </tr>
                                            <tr><td class="leftCell" colspan="3"><strong>'.$param['liqFactor'].'</strong></td>
                                                <td class="leftCell"            ><strong><label id="liquidFactorLbl">'.number_format($liquidFactor*100, 2, '.', ',').'</label> %</strong></td>
                                            </tr>
                                            <tr><td class="leftCell" colspan="4">'.br(1).'</td></tr>
                                            <tr><td class="leftCell apBottom" colspan="3"><strong>'.$param['liqVal'].form_input(array('type'  => 'hidden','name' => 'AXIliquidVal', 'id' => 'AXIliquidVal', 'value' => $liquidVal)).'</strong></td>
                                                <td class="leftCell apBottom"            ><strong>$ <label id="liquidValLbl">'.number_format($liquidVal, 2, '.', ',').'</label> </strong></td>
                                            </tr>
                                            <tr><td class="leftCell apBottom" colspan="3"><strong>'.$param['roun'].'</strong></td>
                                                <td class="leftCell apBottom"            ><strong>$ <label id="liquidValLblRH">'.number_format($this->redondearHermes($liquidVal), 2, '.', ',').'</label> </strong></td>
                                            </tr>                                            
                                        </table>'.br(1).'
                                    </section>
                                </div>
                      </fieldset>';
        
        $coreAPXII .=  '<fieldset>  
                                 <div class="row">
                                 <section class="col col-10"><label class="textarea">'.
                                                                form_textarea(array('class' => 'validate[required,custom[onlyLetterNumber]] text-input  richTextAPVIII',
                                                                                    'rows'  => "5",
                                                                                    'name'  => 'AXIIText1',
                                                                                    'id'    => 'AXIIText1',
                                                                                    'value' => $datos['APVIII'][0]['AXIIText1'] )).'
                                                            </label>
                                 </section>
                                 </div>';                                    
                                    $AXmarketVal     = empty($datos['mv'][0]['AXmarketVal'])?0:$datos['mv'][0]['AXmarketVal'];
                                    $AXmarketValMXM  = $AXmarketVal * $datos['er'];
                                    $AXmarketValM2   = $AXmarketVal / $totTRA;
                                    $AXmarketValMXMM2= $AXmarketValMXM / $totTRA;
                                    $AXmarketValft2  = $AXmarketValM2 / $datos['ft'];
                                    
                                    $AX_VU_Approach    = empty($datos['mv'][0]['AX_VU_Approach'])?0:$datos['mv'][0]['AX_VU_Approach'];
                                    $AX_VU_ApproachMXM = $AX_VU_Approach *  $datos['er'];
                                    $AXVUApproachM2    = $AX_VU_Approach / $totTRA;
                                    $AXVUApproachMXMM2 = $AX_VU_ApproachMXM / $totTRA;
                                    $AXVUApproachft2   = $AXVUApproachM2 / $datos['ft'];
                                    
                                    $AXIliquidVal      = empty($liquidVal)?0:$liquidVal;
                                    $AXIliquidValMXM   = $AXIliquidVal *  $datos['er'];
                                    $AXIliquidValM2    = $AXIliquidVal / $totTRA;
                                    $AXIliquidValMXMM2 = $AXIliquidValMXM / $totTRA;
                                    $AXIliquidValft2   = $AXIliquidValM2 / $datos['ft'];
        $coreAPXII .=  '         <div class="row">
                                 <section class="col col-10">'.br(2).form_input(array('type'  => 'hidden','name' => 'erLiqVal', 'id' => 'erLiqVal', 'value' => $datos['er'])).'
                                        <table border="1" cellspacing="0" cellpadding="0" width="100%">
                                            <tr><td class="leftCell  apBottom" width="33%">'.$param['marval'].' of the Property</td>
                                                <td class="rightCell apBottom" width="33%">$ '.number_format($this->redondearHermes($AXmarketVal), 2, '.', ',').' '.$param['currency'].br(1).'
                                                                                           $ '.number_format($AXmarketValM2, 2, '.', ',').' '.$param['currency'].' / m2'.br(1).'
                                                                                           $ '.number_format($AXmarketValft2, 2, '.', ',').' '.$param['currency'].' / ft2    
                                                </td>
                                                <td class="rightCell apBottom" width="33%">$ '.number_format($this->redondearHermes($AXmarketValMXM), 2, '.', ',').' '.$param['currencyMX'].br(1).'
                                                                                           $ '.number_format($AXmarketValMXMM2, 2, '.', ',').' '.$param['currencyMX'].' / m2'.br(2).'                                                     
                                                </td>
                                            </tr>                                            
                                            <tr><td class="leftCell  apBottom" width="33%">'.$param['vis'].'</td>
                                                <td class="rightCell apBottom" width="33%">$ '.number_format($this->redondearHermes($AX_VU_Approach), 2, '.', ',').' '.$param['currency'].br(1).'
                                                                                           $ '.number_format($AXVUApproachM2, 2, '.', ',').' '.$param['currency'].' / m2'.br(1).'
                                                                                           $ '.number_format($AXVUApproachft2, 2, '.', ',').' '.$param['currency'].' / ft2    
                                                </td>
                                                <td class="rightCell apBottom" width="33%">$ '.number_format($this->redondearHermes($AX_VU_ApproachMXM), 2, '.', ',').' '.$param['currencyMX'].br(1).'
                                                                                           $ '.number_format($AXVUApproachMXMM2, 2, '.', ',').' '.$param['currencyMX'].' / m2'.br(2).'                                                                                           
                                                </td>
                                            </tr>
                                            <tr><td class="leftCell  apBottom" width="33%">'.$param['liqVal'].'</td>
                                                <td class="rightCell apBottom" width="33%">$ <label id="AXIliquidValLbl">'.number_format($this->redondearHermes($AXIliquidVal), 2, '.', ',').'</label> '.$param['currency'].br(1).'
                                                                                           $ <label id="AXIliquidValM2Lbl">'.number_format($AXIliquidValM2, 2, '.', ',').'</label> '.$param['currency'].' / m2'.br(1).'
                                                                                           $ <label id="AXIliquidValft2Lbl">'.number_format($AXIliquidValft2, 2, '.', ',').'</label> '.$param['currency'].' / ft2    
                                                </td>
                                                <td class="rightCell apBottom" width="33%">$ <label id="AXIliquidValMXMLbl">'.number_format($this->redondearHermes($AXIliquidValMXM), 2, '.', ',').'</label> '.$param['currencyMX'].br(1).'
                                                                                           $ <label id="AXIliquidValMXMM2Lbl">'.number_format($AXIliquidValMXMM2, 2, '.', ',').'</label> '.$param['currencyMX'].' / m2'.br(2).'
                                                </td>
                                            </tr>
                                        </table>'.br(5).'
                                </section>
                                </div>
                                <div class="row">
                                 <section class="col col-7">'.  nbs(1).'</section><section class="col col-4"> <a href="'.base_url().'inmueble/forma/E/70778/#mt_et"> '.img( array('src'=>base_url().'images/edit.png',"title"=>"Edit")).'</a> </section>
                                </div>
                                <div class="row">
                                 <section class="col col-7">'.  nbs(1).'</section><section class="col col-4"><label class="label">Exposure Time: '.(empty($datos['frontPage'][0]['et'])?0:$datos['frontPage'][0]['et']).' months</label></section>
                                </div>
                                <div class="row">
                                 <section class="col col-7">'.  nbs(1).'</section><section class="col col-4"><label class="label">Marketing Time: '.(empty($datos['frontPage'][0]['mt'])?0:$datos['frontPage'][0]['mt']).' months</label></section>
                                </div>
                                <div class="row">
                                 <section class="col col-10">'.br(6).'
                                        <table border="1" cellspacing="0" cellpadding="0" width="100%">
                                            <tr><td class="centerCell" width="50%">'.$datos['letter']['nombreFP'].(empty($datos['letter']['tituloFP'])?'':', '.$datos['letter']['tituloFP']).br(1).(empty($datos['letter']['puestoFP'])?'':', '.$datos['letter']['puestoFP']).br(1).'Valuation and Consulting Services <br> JLL Mexico</td>
                                                <td class="centerCell" width="50%">'.$datos['letter']['nombreFS'].(empty($datos['letter']['tituloFS'])?'':', '.$datos['letter']['tituloFS']).br(1).(empty($datos['letter']['puestoFS'])?'':', '.$datos['letter']['puestoFS']).br(1).'Valuation and Consulting Services <br> JLL Mexico</td>
                                            </tr>
                                        </table>    
                                 </section>
                                </div>
                      </fieldset>';
                 
        return array("coreAPXI"=>$coreAPXI,"coreAPXII"=>$coreAPXII);
        } catch (Exception $e) {echo 'creaCoreAPVIII Excepción: ',  $e, "\n";}  
    }
    
    public function creaTablaACA($datos, $md_inmuebles,$param)
    { 
     $tablaACA    = "";
     $tablaRCN    = "";
     $tablaDEP    = "";
     $tablaACATOT = "";
     $tablaAECW   = "";
     $numColumn   = sizeof($datos['columns']);
     $numRow      = sizeof($datos['rows']);
     $numAE       = sizeof($datos['ae']);
     $numCW       = sizeof($datos['cw']);
     $er          = $datos['er'];
     $ft          = $datos['ft'];
     
     try{
         if(empty($datos['land']))
             {
                $taxTxt   = 0;
                $taxBill  = 0;
                $taxBillFt= NULL;
                $deedTxt  = 0;
                $deed     = 0;                
                $deedFt   = NULL;
                $plansTxt = 0;
                $plans    = 0;
                $plansFt  = NULL;
                $difLand  = NULL;
                $difFt    = NULL;
             }
         else
             {
                $taxTxt   = $datos['land'][0]['taxTxt'];
                $taxBill  = $datos['land'][0]['taxBill'];
                $taxBillFt= number_format(($taxBill*$ft), 2, '.', ',');
                $deedTxt  = $datos['land'][0]['deedTxt'];
                $deed     = $datos['land'][0]['deed'];                
                $deedFt   = number_format(($deed*$ft), 2, '.', ',');
                $plansTxt = $datos['land'][0]['plansTxt'];
                $plans    = $datos['land'][0]['plans'];
                $plansFt  = number_format(($plans*$ft), 2, '.', ',');
                $difLand  = number_format($deed-$taxBill, 2, '.', ',');
                $difFt    = number_format((($deed-$taxBill)*$ft), 2, '.', ',');
             }

        $tablaACA .= "<table class='tableGrid' cellspacing='0' cellpadding='0' width='100%' id='tblACA'>
                          <thead>
                              <tr><th width='20%' id='0' class='centerCell'>".img( array('src'=>base_url()."images/new.png","title"=>"Add Zone/Type","class"=>"addArea pointer")).nbs(2)."Zone/Type</th>";                                           
                                           $columnSubTot = "";
                                           $columnRCN    = "";
                                           $columnDEP    = "";
                                           $headreColRCN = "";
                                           $headreColDEP = "";
                                           $subTotColumn = 0;                                           
                                           $columnId     = 0;
                                           $totalRCN     = 0;
                                           $totalDEP     = 0;
                                           $totCA        = 0;
                                           $totGBA       = 0;
                                           $totTRA       = 0;
                                           $widthRCN     = 0;
                                           
                                           $subTotDefVaRCN = 0;
                                           $subTotUCRCN    = 0;
                                           $subTotUCAdRCN  = 0;
                                           $totColRow      = 0;
                                           foreach ($datos['columns'] as $c)
                                           {  $columnId++;
                                              $chkTRA = (empty($c['tra']))?"":"checked"; 
                                              $chkGBA = (empty($c['gba']))?"":"checked";
                                           
                                              $areaT         = img(array('src'=>base_url()."images/close.png","title"=>"Delete Area","id"=>$columnId,"class"=>"delArea pointer")).br(2).'<input type="text" name="areaT'.$columnId.'" id="areaT'.$columnId.'" class="validate[custom[onlyLetterNumber]] text-input conborde" onChange="asignaTypeName(this,\'Ty\','.$columnId.')" value="'.$c['type_area'].'" placeholder="Type Area" size="8" maxlength="40">'; 
                                              $areaL         = '<input type="text" name="areaL'.$columnId.'" id="areaL'.$columnId.'" class="validate[required,custom[onlyLetterNumber]] text-input conborde" onChange="asignaTypeName(this,\'Na\','.$columnId.')" value="'.$c['name_area'].'" placeholder="Name Area" size="8" maxlength="40">';
                                              $areaG         = '<label class="checkbox" style="text-align: justify; text-justify: inter-word"><input type="checkbox" class="sumarSub" name="gba'.$columnId.'" value="GBA" '.$chkGBA.'><i></i>GBA*</label>';
                                              $areaR         = '<label class="checkbox" style="text-align: justify; text-justify: inter-word"><input type="checkbox" class="sumarSub" name="tra'.$columnId.'" value="TRA" '.$chkTRA.'><i></i>TRA*</label>';
                                              $tblHeader     = '<table cellspacing="0" cellpadding="0" width="100%"><tr><td class="leftCell">'.$areaT.'</td></tr><tr><td class="leftCell">'.$areaL.'</td></tr><tr><td>'.$areaG.'</td></tr><tr><td>'.$areaR.'</td></tr></table>';
                                              $tablaACA     .= '<th id="'.$columnId.'" class="centerCell">'.$tblHeader.'</th>';
                                              
                                              $subTotColumn  = $md_inmuebles->traSumCAXColumn($datos['id_in'],$columnId);
                                              $totCA         = $totCA + $subTotColumn;
                                              $totGBA        = (empty($c['gba']))?$totGBA+0:$totGBA+$subTotColumn;
                                              $totTRA        = (empty($c['tra']))?$totTRA+0:$totTRA+$subTotColumn;                
                                              
                                              $columnSubTot .= '<td class="subTotColArea" id="areaSubC'.$columnId.'"><label id="lblSub'.$columnId.'"> '.number_format($subTotColumn, 2, '.', ',').'</label> m2<br><label id="lblSub'.$columnId.'ft2"> '.number_format($subTotColumn*$ft, 2, '.', ',').'</label> ft2 <input type="hidden" class="TOT" name="sub'.$columnId.'" id="sub'.$columnId.'" value="'.$subTotColumn.'"></td>';
                                              $totColRow     = $totColRow + $subTotColumn;

                                              $ucAd          = ($c['uc_rcn'] * ($c['por_rcn']/100)) ;
                                              $headreColRCN  .= "<th width='60px' class='leftCell' id='areaHeRCN".$columnId."'><label id='lblhcTyRCN$columnId'> ".$c['type_area']." </label>".br(1)."<label id='lblhcNaRCN'$columnId> ".$c['name_area']." </label></th>";
                                              $headreColDEP  .= "<th width='60px' class='leftCell' id='areaHeDEP".$columnId."'><label id='lblhcTyDEP$columnId'> ".$c['type_area']." </label>".br(1)."<label id='lblhcNaDEP'$columnId> ".$c['name_area']." </label></th>";
                                              $subTotDefVaRCN = $subTotDefVaRCN + $c['def_value_rcn'];
                                              $subTotUCRCN    = $subTotUCRCN    + $c['uc_rcn'];
                                              $subTotUCAdRCN  = $subTotUCAdRCN  + $ucAd;
                                              $totalRCN       = $totalRCN       + $c['rcn'];
                                              $totalDEP       = $totalDEP       + $c['dvc_dep'];
                                              
                                              $widthRCN       = $widthRCN + 60;
                                              $columnRCN     .= '<td class="leftCell adprice" id="areaRCN'.$columnId.'"><textarea rows="6" cols="12" name="defRCN'.$columnId.'"      id="defRCN'.$columnId.'"      class="conborde" placeholder="Definition" maxlength="200">'.$c['def_rcn'].'</textarea><br>
                                                                                             <input type="text"            name="defValueRCN'.$columnId.'Mask" id="defValueRCN'.$columnId.'Mask" class="pmask conborde" onChange="sumaRCN(this,'.$columnId.','.$er.',true)" value="'.$c['def_value_rcn'].'" placeholder="0.00" size="10" maxlength="20"> mxn
                                                                                             <input type="hidden"          name="defValueRCN'.$columnId.'"     id="defValueRCN'.$columnId.'"     class="defvarcn" value="'.$c['def_value_rcn'].'" ><br>
                                                                                             <input type="text"            name="fdhRCN'.$columnId.'"      id="fdhRCN'.$columnId.'"      class="conborde" onChange="sumaRCN(this,'.$columnId.','.$er.',false)" value="'.$c['fdh_rcn'].'"       placeholder="0.00" size="6" maxlength="10"><br>
                                                                                             <input type="text"            name="fcRCN'.$columnId.'"       id="fcRCN'.$columnId.'"       class="conborde" onChange="sumaRCN(this,'.$columnId.','.$er.',false)" value="'.$c['fc_rcn'].'"        placeholder="0.00" size="6" maxlength="10"><br>
                                                                                            $<label id="lblucRCN'.$columnId.'">'.number_format($c['uc_rcn'], 2, '.', ',').'</label> mxn <input type="hidden" class="ucrcn" name="ucRCN'.$columnId.'" id="ucRCN'.$columnId.'"  value="'.$c['uc_rcn'].'"> <br>
                                                                                             <input type="text"           name="porRCN'.$columnId.'"      id="porRCN'.$columnId.'"      class="conborde" onChange="sumaRCN(this,'.$columnId.','.$er.',false)" value="'.$c['por_rcn'].'"       placeholder="0.00" size="4" maxlength="10">% <br>
                                                                                        <br>$<label id="lblucRCNAD'.$columnId.'">'.number_format($ucAd, 2, '.', ',').'</label><input type="hidden" class="ucadrcn" name="ucAdRCN'.$columnId.'" id="ucAdRCN'.$columnId.'"  value="'.$ucAd.'"> mxn <br>
                                                                   <label class="cnAM"> &nbsp;$<label id="lblRcn'.$columnId.'"    >'.number_format($c['rcn'], 2, '.', ',').'    </label> mxn<br>
                                                                                      &nbsp;$<label id="lblRcn'.$columnId.'usd" >'.number_format($c['rcn']/$er, 2, '.', ',').'</label> usd<input type="hidden" class="rcn" name="rcn'.$columnId.'" id="rcn'.$columnId.'" value="'.$c['rcn'].'"><br>
                                                                   </label>                       
                                                                </td>';
                                              
                                              $columnDEP    .= '<td class="leftCell adprice" id="areaDEP'.$columnId.'"><input type="text" name="efageDEP'.$columnId.'" id="efageDEP'.$columnId.'" class="conborde" onChange="obtenDVC('.$columnId.','.$er.')" value="'.$c['efage_dep'].'" placeholder="0.00" size="6" maxlength="10"><br> 
                                                                                                                       <input type="text" name="ulDEP'.$columnId.'"    id="ulDEP'.$columnId.'"    class="conborde" onChange="obtenDVC('.$columnId.','.$er.')" value="'.$c['ul_dep'].'"    placeholder="0.00" size="6" maxlength="10"><br>                                                                                            
                                                                                                                       <br><label id="lblafDEP' .$columnId.'">'.number_format($c['af_dep']*100, 2, '.', ',').'</label> %<input type="hidden" name="afDEP'.$columnId.'" id="afDEP'.$columnId.'" value="'.$c['af_dep'].'"><br>
                                                                                                                       <label class="cnCA"><br>$<label id="lblDvcDEP'.$columnId.'" class="cn">'.number_format($c['dvc_dep'], 2, '.', ',').'</label> mxn<br>&nbsp;$<label id="lblDvcDEP'.$columnId.'usd" class="cn">'.number_format($c['dvc_dep']/$er, 2, '.', ',').'</label> usd'.form_input(array('name'=>'dvc'.$columnId, 'type'=>'hidden',"class"=>"rcnDEP", 'id'=>'dvc'.$columnId, 'value'=> $c['dvc_dep'])).'<br>
                                                                                                                       </label>
                                                                </td>';
                                           }
         $tablaACA .= "        </tr>"
                 . "     </thead>";
         $tablaACA .= "  <tbody><tr id='0'>
                                           <td width='20%' class='leftCell'>" .br(1).img(array('src'=>base_url()."images/new.png","title"=>"Add Area","class"=>"addZone pointer")).nbs(2)."AREA</td>
                                </tr>";
                                $rowId        = 0;
                                $zona         = "";
                                $dataAZ       = "";                                
                                $lblSubTotRow = "";
                                foreach ($datos['rows'] as $r)
                                 {  $rowId++;
                                    $columnId  = 0;
                                    $dataAZ    = "";
                                    $subTotRow = 0;                                    
                                    foreach ($datos['columns'] as $c)
                                        {  $columnId++;
                                           $dataId    = $columnId.$rowId;
                                           $dato      = $md_inmuebles->traCA($datos['id_in'],$dataId);
                                           $dataAZ   .= ' <td class="leftCell areaDato'.$columnId.'"><input type="text"   name="az'.$dataId.'Mask" id="az'.$dataId.'Mask" class="cmask conborde" onChange="sumaTotCA(this,'.$ft.','.$columnId.','.$rowId.','.$er.')" value="'.$dato.'" placeholder="0.00" size="6" maxlength="20"> m2'
                                                   . '                                               <input type="hidden" name="az'.$dataId.'"     id="az'.$dataId.'"     class="GBA TRA col'.$columnId.' row'.$rowId.'"  value="'.$dato.'"></td>';
                                           $subTotRow = $subTotRow + $dato;
                                        }
                                    $zona          = img(array('src'=>base_url()."images/close.png","title"=>"Delete Zone","id"=>$rowId,"class"=>"delZone pointer")).nbs(2).$rowId.'. <input type="text" name="zona'.$rowId.'" id="zona'.$rowId.'" value="'.$r['zone'].'" class="validate[required,custom[onlyLetterNumber]] text-input conborde" placeholder="Zone" maxlength="40" size="6">';
                                    $tablaACA     .= '<tr id="'.$rowId.'"><td class="centerCell">'.$zona.'</td>'.$dataAZ.'</tr>';
                                    $lblSubTotRow .= '<label id="lblSubTotRow'.$rowId.'">'.number_format($subTotRow, 2, '.', ',').'    </label> m2<input type="hidden" name="subTotRow'.$rowId.'" id="subTotRow'.$rowId.'" value="'.$subTotRow.'">'.br(2);
                                 }
                                
        $tablaACA .= "         <tr id='-1'>
                                          <td class='leftCell adprice'></td>".$columnSubTot."
                                </tr>
                         </tbody>";
         $tablaACA .= "</table>";         

         $tablaRCN .=   br(1)."
                        <table class='tableGrid' cellspacing='0' cellpadding='0' width='100%' id='tblRCN'>
                        <thead>
                               <tr> <th width='100%' class='centerCell' colspan='".$columnId."'>Cost Approach".br(3).$param['rrc']." </th> </tr>
                               <tr> <th width='20%'></th> 
                                    $headreColRCN
                               </tr>
                        </thead>
                        <tbody><tr id='0'><td class='adprice' valign='bottom'> <div class='columnaFija'> ".br(4)."Definition: ".br(5)." Unit Cost: ".br(2)."  Factor double height: ".br(2)."  Factor for city: ".br(2)." Unit Cost: ".br(2)." Progress: ".br(2)." Unit Cost: ".br(2)."<label class='cnAM'>Replacement Cost New:</label> </div></td>"
                                          .$columnRCN."
                               </tr>
                        </tbody>
                      </table>
                      ";
         
         $tablaDEP .= br(2)."<table class='tableGrid' cellspacing='0' cellpadding='0' width='100%' id='tblDEP'>
                        <thead>
                              <tr> <th id='0' width='100%' class='centerCell' colspan='".$columnId."'>".$param['dep'].br(1).$param['ealm'].br(2).$param['mb']."</th> </tr>
                              <tr> <th width='20%'></th> 
                                   $headreColDEP 
                              </tr>
                        </thead>
                        <tbody><tr id='0'><td class='adprice' valign='top'> <div class='columnaFija'>".br(1)."Age/years ".br(2)." Useful Life ".br(3)." Age Factor ".br(3)."<label class='cnCA'>Depreciated value of Constructions</label> </div> </td>"
                                          .$columnDEP."
                               </tr>
                        </tbody>
                      </table>
                      ";
         $tablaACATOT .= "<table class='tableGrid' cellspacing='0' cellpadding='0' width='100%' id='tblConsLand'>
                               <thead>                                      
                                      <tr> <th width='100%' class='adprice centerCell borderLeft borderRight' colspan='6'>".br(1).$param['aa'].br(1)." </th> </tr>
                                      <tr> <th width='15%'  class='borderLeft'>".$param['tax']."</th><th width='15%'>".$param['land']."</th><th class ='borderLeft' width='15%'>".$param['deed']."</th><th width='15%'>".$param['land']."</th><th class ='borderLeft' width='15%'>".$param['pp']." <span class='errorMsg' style='color:red;'>* reference data for calculations</span></th><th class ='borderRight' width='15%'>".$param['land']."<span class='errorMsg' style='color:red;'>*<br><br></span></th>
                                      </tr>
                               </thead>
                               <tbody><tr><td class='adprice borderLeft'><input type='text' name='taxTxt' id='taxTxt' value='".$taxTxt."'  class='validate[custom[onlyLetterNumber]] text-input conborde' placeholder='".$param['tax']."' size='10' maxlength='60'><br>".nbs(1)."</td>
                                          <td class='adprice'><input type='text'   name='taxBillMask'  id='taxBillMask' value='".$taxBill."' class='cmask conborde' onChange='LandToft(this,".$ft.")' placeholder='0.00' size='10' maxlength='20'> m2
                                                              <input type='hidden' name='taxBill'      id='taxBill'     value='".$taxBill."'> 
                                                             <br><label id='taxBillFt'>".$taxBillFt."</label> ft2</td>
                                          <td class='adprice borderLeft'><input type='text' name='deedTxt' value='".$deedTxt."'  id='deedTxt'  class='validate[custom[onlyLetterNumber]] text-input conborde' placeholder='Example: ".$param['deed']."' size='10' maxlength='60'><br>".nbs(1)."</td>
                                          <td class='adprice'><input type='text'   name='deedMask'     id='deedMask'     value='".$deed."'     class='cmask conborde' onChange='LandToft(this,".$ft.")' placeholder='0.00' size='10' maxlength='20'> m2
                                                              <input type='hidden' name='deed'         id='deed'         value='".$deed."'> 
                                                              <br><label id='deedFt'>".$deedFt."</label> ft2</td>
                                          <td class='adprice borderLeft'><input type='text' name='plansTxt' value='".$plansTxt."' id='plansTxt' class=' validate[custom[onlyLetterNumber]] text-input conborde'  placeholder='Example: ".$param['pp']."' size='10' maxlength='60'><br>".nbs(1)."</td>
                                          <td class='adprice borderRight'><input type='text'   name='plansMask' id='plansMask' value='".$plans."'  class='cmask conborde' onChange='LandToft(this,".$ft.")' placeholder='0.00' size='10' maxlength='20'> m2
                                                                          <input type='hidden' name='plans'     id='plans'     value='".$plans."'> 
                                                                         <br><label id='plansFt'>".$plansFt."</label> ft2</td>
                                      </tr>
                                      <tr><td class='adprice borderLeft'>". br(3)."</td>
                                          <td class='adprice' colspan='2'>".$param['ld']."</td>
                                          <td class='adprice'><label id='landDif'>".$difLand."</label> m2</td>
                                          <td class='adprice'><label id='landDifFt'>".$difFt."</label> ft2</td>                                          
                                          <td class='adprice borderRight'>".  nbs(1)."</td>
                                      </tr>
                                      <tr> <th width='100%' class='adprice centerCell' colspan='6'>".br(2).$param['cap'].br(2)." </th> </tr>
                               </tbody>
                          </table>".br(3)."<table cellspacing='0' cellpadding='0' width='100%' id='tblACATOT'>";         
         $tablaACATOT .= "  <tbody>
                                <tr><td width='80%' class='leftCell'          >".$tablaACA."                </td><td width='20%' id='SubTotRow' class='subTotRow' valign='top'>".br(6)."SubTotal Area".br(7).$lblSubTotRow."</td></tr>
                                <tr><td width='80%' class='subTotRow adprice' >".$param['groundFloor']."            </td><td width='20%' class='subTotRow adprice'><label id='totCA'> ".number_format($totCA, 2, '.', ',')." </label> m2". form_input(array('name'=>'txttotCA',  'type'=>'hidden','id'=>'txttotCA',  'value'=>$totCA))." <br><label id='totCAft'>".number_format($totCA*$ft, 2, '.', ',')."  </label> ft2</td></tr>
                                <tr><td width='80%' class='subTotRow adprice' >".$param['gba']." (GBA*)   </td><td width='20%' class='subTotRow adprice'><label id='totGBA'>".number_format($totGBA, 2, '.', ',')."</label> m2". form_input(array('name'=>'txttotGBA', 'type'=>'hidden','id'=>'txttotGBA', 'value'=>$totGBA))."<br><label id='totGBAft'>".number_format($totGBA*$ft, 2, '.', ',')."</label> ft2</td></tr>
                                <tr><td width='80%' class='subTotRow adprice' >".$param['tra']." (TRA*)   </td><td width='20%' class='subTotRow adprice'><label id='totTRA'>".number_format($totTRA, 2, '.', ',')."</label> m2". form_input(array('name'=>'txttotTRA', 'type'=>'hidden','id'=>'txttotTRA', 'value'=>$totTRA))."<br><label id='totTRAft'>".number_format($totTRA*$ft, 2, '.', ',')."</label> ft2</td></tr>
                                <tr><td width='90%' class='leftCell'  valign='top' >".$tablaRCN."           </td>
                                    <td valign='bottom'> <div class='columnaFija'> ".br(1)."<label class='subTotRow'>SubTotal RCN</label>".br(1)."
                                                                          <label class='subTotRowSinNeg'>".                                                                                            
                                                                             br(4)."$<label class='subTotRowSinNeg' id='subTotDVRCN'  >".number_format(($subTotDefVaRCN), 2, '.', ',')." </label> mxn<br>$<label id='subTotDVRCNusd' >".number_format(($subTotDefVaRCN/$er), 2, '.', ',')."</label> usd".
                                                                             br(6)."$<label class='subTotRowSinNeg' id='subTotUCRCN'  >".number_format(($subTotUCRCN), 2, '.', ',')."    </label> mxn<br>$<label id='subTotUCRCNusd' >".number_format(($subTotUCRCN/$er)   , 2, '.', ',')."</label> usd".
                                                                             br(4)."$<label class='subTotRowSinNeg' id='subTotUCAdRCN'>".number_format(($subTotUCAdRCN), 2, '.', ',')."  </label> mxn<br>$<label id='subTotUCAdRCNusd'>".number_format(($subTotUCAdRCN/$er) , 2, '.', ',')."</label> usd".
                                                                            "<label class='subTotRowAM'>".
                                                                             br(1)."$<label id='subTotRCN'>".number_format(($totalRCN), 2, '.', ',')."       </label> mxn<br>$<label id='subTotRCNusd'>".number_format(($totalRCN/$er)      , 2, '.', ',')."</label> usd
                                                                            </label>
                                                       </div>                     
                                    </td>
                                </tr>
                                <tr><td width='90%' class='leftCell'          >".$tablaDEP."                </td>
                                    <td width='10%' class='subTotRow' valign='top'>".br(8)."SubTotal Dep.".br(11).
                                                                                   "<label class='subTotRowCA'>$<label id='totDEP'>".number_format(($totalDEP), 2, '.', ',')."</label> mxn<br>$<label id='totDEPusd'>".number_format(($totalDEP/$er), 2, '.', ',')."</label> usd
                                                                                   </label>    
                                    </td>
                                </tr>
                            </tbody>";
         $tablaACATOT .= "</table>";
                  
         $tablaAECW   .= "<table class='tableGrid' cellspacing='0' cellpadding='0' width='100%' id='tblAE'>
                                    <thead><tr><th class='leftCell   adprice' width='20%'>".img( array('src'=>base_url()."images/new.png","title"=>"Agregar Area","class"=>"addAE pointer")).br(1)."EQUIPMENT</th>
                                               <th class='centerCell adprice' width='6%' >".$param['qt']."</th>
                                               <th class='centerCell adprice' width='5%' >".$param['un']."</th>
                                               <th class='centerCell adprice' width='15%' >".$param['uc']."</th>
                                               <th class='rightCell  adprice' width='17%'>".$param['rcn']."</th>
                                               <th class='centerCell adprice celdaDiv' width='5%' >".$param['l14']."</th>
                                               <th class='centerCell adprice' width='6%' >".$param['l15']."</th>
                                               <th class='centerCell adprice' width='6%' >".$param['l16']."</th>
                                               <th class='rightCell  adprice' width='15%'>".$param['l17']."</th>
                                          </tr>
                                    </thead>
                                    <tbody>";
                                           $aeId       = 0;
                                           $totalAE    = 0;
                                           $totalAEDEP = 0;
                                           foreach ($datos['ae'] as $ae)
                                           {   $aeId++;
                                               $totalAE      = $totalAE    + ($ae['unit_value']*$ae['amount']);
                                               $totalAEDEP   = $totalAEDEP + ($ae['dvc_dep']);
                                               $tablaAECW   .= '<tr><td valing="center" class="leftCell"   width="20%">'.img(array('src'=>base_url()."images/close.png","title"=>"Delete ACCESORY EQUIPMENT","id"=>"AE".$aeId,"class"=>"delAECW pointer")).nbs(2).$aeId.'.  <input type="text" name="AE'.$aeId.'"    id="AE'.$aeId.'"   value="'.$ae['leyenda'].'"    class="validate[required,custom[onlyLetterNumber]] text-input conborde" placeholder="Accesory Equipment" maxlength="60" size="12"></td>'
                                                            .'      <td valing="center" class="centerCell" width="6%" >            <input type="text" name="AEAM'.$aeId.'"  id="AEAM'.$aeId.'" value="'.$ae['amount'].'"     class="conborde" onchange="calculaTotAECW(\'AE\','.$aeId.','.$er.')" placeholder="0.00" maxlength="10" size="2"></td>'
                                                            .'      <td valing="center" class="centerCell" width="5%" >            <input type="text" name="AEUN'.$aeId.'"  id="AEUN'.$aeId.'" value="'.$ae['unit'].'"       class="validate[custom[onlyLetterNumber]] text-input conborde" placeholder="[Unit]" maxlength="40" size="3"></td>'
                                                            .'      <td valing="center" class="centerCell" width="15%" >      <br>  <input type="text" name="AEUV'.$aeId.'Mask"  id="AEUV'.$aeId.'Mask" value="'.$ae['unit_value'].'" class="pmask conborde" onchange="calculaTotAECW(\'AE\','.$aeId.','.$er.')" placeholder="0.00" maxlength="20" size="10"> mxn '
                                                            .'                                                                      <input type="hidden" name="AEUV'.$aeId.'"  id="AEUV'.$aeId.'" value="'.$ae['unit_value'].'"></td>'
                                                            .'      <td valing="center" class="rightCell"  width="17%">      <br>$ <label id="lblAETO'.$aeId.'"   >'.number_format(  $ae['unit_value']*$ae['amount']      , 2, '.', ',').'</label> mxn<input type="hidden" class="AE" name="AETO'.$aeId.'" id="AETO'.$aeId.'" value="'.($ae['unit_value'] * $ae['amount']).'"><br>'
                                                            .'                                                                   $ <label id="lblAETO'.$aeId.'usd">'.number_format((($ae['unit_value']*$ae['amount'])/$er), 2, '.', ',').'</label> usd</td>'
                                                            .'      <td valing="center" class="centerCell celdaDiv" width="5%">    <input type="text" name="AEEfAgDEP'.$aeId.'"  id="AEEfAgDEP'.$aeId.'" value="'.$ae['efage_dep'].'" class="conborde" onchange="obtenDVCAECW(\'AE\','.$aeId.','.$er.')" placeholder="0.00" maxlength="10" size="2"></td>'
                                                            .'      <td valing="center" class="centerCell" width="6%">             <input type="text" name="AEULDEP'.$aeId.'"    id="AEULDEP'.$aeId.'"   value="'.$ae['ul_dep'].'"    class="conborde" onchange="obtenDVCAECW(\'AE\','.$aeId.','.$er.')" placeholder="0.00" maxlength="10" size="2"></td>'                                                            
                                                            .'      <td valing="center" class="centerCell" width="6%">             <label id="lblAEAFDEP'.$aeId.'" >'.number_format( $ae['af_dep']*100, 2, '.', ',').'</label><input type="hidden" name="AEAFDEP'.$aeId.'" id="AEAFDEP'.$aeId.'" value="'.$ae['af_dep'].'">%</td>'
                                                            .'      <td valing="center" class="rightCell"  width="15%">      <br>$ <label id="lblAEDVC'.$aeId.'"   >'.number_format( $ae['dvc_dep']    , 2, '.', ',').' </label> mxn <input type="hidden" class="AEDEP" name="AEDVC'.$aeId.'" id="AEDVC'.$aeId.'" value="'.$ae['dvc_dep'].'"><br>'
                                                            .'                                                                   $ <label id="lblAEDVC'.$aeId.'usd">'.number_format(($ae['dvc_dep']/$er), 2, '.', ',').'</label> usd</td>'
                                                            .'</tr>';
                                           }
        $tablaAECW   .=        "           <tr><td colspan='4' class='subTotRow11 adprice' >".$param['depTotAE']."</td>
                                               <td class='subTotRow11 adprice'>$ <label id='subTotalAE'>".number_format($totalAE, 2, '.', ',')."</label> mxn<br>$ <label id='subTotalAEusd'>".number_format($totalAE/$er, 2, '.', ',')."</label> usd</td>
                                               <td colspan='5' class='subTotRow11 adprice celdaDiv'>$ <label id='subTotalAEDEP'>".number_format($totalAEDEP, 2, '.', ',')."</label> mxn<br>$ <label id='subTotalAEDEPusd'>".number_format($totalAEDEP/$er, 2, '.', ',')."</label> usd</td>
                                           </tr>
                                    </tbody>
                               </table>".br(2)."
                               <table class='tableGrid' cellspacing='0' cellpadding='0' width='100%' id='tblCW'>
                                    <thead><tr><th class='leftCell adprice' width='20%'  >".img( array('src'=>base_url()."images/new.png","title"=>"Agregar Complementary Work","class"=>"addCW pointer")).br(1)."COMPLEMENTARY WORKS</th>
                                               <th class='centerCell adprice' width='6%' >".$param['qt']."</th>
                                               <th class='centerCell adprice' width='5%' >".$param['un']."</th>
                                               <th class='centerCell adprice' width='15%' >".$param['uc']."</th>
                                               <th class='rightCell  adprice' width='17%'>".$param['rcn']."</th>
                                               <th class='centerCell adprice celdaDiv' width='5%' >".$param['l14']."</th>
                                               <th class='centerCell adprice' width='6%' >".$param['l15']."</th>
                                               <th class='centerCell adprice' width='6%' >".$param['l16']."</th>
                                               <th class='rightCell  adprice' width='15%'>".$param['l17']."</th>
                                           </tr>
                                    </thead>
                                    <tbody>";
                                           $cwId       = 0;
                                           $totalCW    = 0;
                                           $totalCWDEP = 0;
                                           foreach ($datos['cw'] as $cw)
                                           {   $cwId++;
                                               $totalCW      = $totalCW    + ($cw['unit_value'] * $cw['amount']);
                                               $totalCWDEP   = $totalCWDEP + ($cw['dvc_dep']);
                                               $tablaAECW   .= '<tr><td valing="center" class="leftCell"   width="20%">'.img(array('src'=>base_url()."images/close.png","title"=>"Delete COMPLEMENTARY WORK","id"=>"CW".$cwId,"class"=>"delAECW pointer")).nbs(2).$cwId.'.  <input type="text" name="CW'.$cwId.'"    id="CW'.$cwId.'"   value="'.$cw['leyenda'].'"    class="validate[required,custom[onlyLetterNumber]] text-input conborde" placeholder="COMPLEMENTARY WORKS" maxlength="60" size="12"></td>'
                                                            .'      <td valing="center" class="centerCell" width="6%" >            <input type="text" name="CWAM'.$cwId.'"  id="CWAM'.$cwId.'" value="'.$cw['amount'].'"     class="conborde" onchange="calculaTotAECW(\'CW\','.$cwId.','.$er.')" placeholder="0.00" maxlength="10" size="2"></td>'
                                                            .'      <td valing="center" class="centerCell" width="5%" >            <input type="text" name="CWUN'.$cwId.'"  id="CWUN'.$cwId.'" value="'.$cw['unit'].'"       class="validate[custom[onlyLetterNumber]] text-input conborde" placeholder="Unit" maxlength="40" size="3"></td>'
                                                            .'      <td valing="center" class="centerCell" width="15%" >      <br> <input type="text" name="CWUV'.$cwId.'Mask"  id="CWUV'.$cwId.'Mask" value="'.$cw['unit_value'].'" class="pmask conborde" onchange="calculaTotAECW(\'CW\','.$cwId.','.$er.')" placeholder="0.00" maxlength="20" size="10"> mxn'
                                                            . '                                                                     <input type="hidden" name="CWUV'.$cwId.'"      id="CWUV'.$cwId.'" value="'.$cw['unit_value'].'"></td>'
                                                            .'      <td valing="center" class="rightCell"  width="17%">      <br>$ <label id="lblCWTO'.$cwId.'"   >'.number_format(  $cw['unit_value']*$cw['amount']      , 2, '.', ',').'</label> mxn<input type="hidden" class="CW" name="CWTO'.$cwId.'" id="CWTO'.$cwId.'" value="'.($cw['unit_value'] * $cw['amount']).'"><br>'
                                                            .'                                                                   $ <label id="lblCWTO'.$cwId.'usd">'.number_format((($cw['unit_value']*$cw['amount'])/$er), 2, '.', ',').'</label> usd</td>'
                                                            .'      <td valing="center" class="centerCell celdaDiv" width="5%">    <input type="text" name="CWEfAgDEP'.$cwId.'"  id="CWEfAgDEP'.$cwId.'" value="'.$cw['efage_dep'].'" class="conborde" onchange="obtenDVCAECW(\'CW\','.$cwId.','.$er.')" placeholder="0.00" maxlength="10" size="2"></td>'
                                                            .'      <td valing="center" class="centerCell" width="6%">             <input type="text" name="CWULDEP'.$cwId.'"    id="CWULDEP'.$cwId.'"   value="'.$cw['ul_dep'].'"    class="conborde" onchange="obtenDVCAECW(\'CW\','.$cwId.','.$er.')" placeholder="0.00" maxlength="10" size="2"></td>'                                                            
                                                            .'      <td valing="center" class="centerCell" width="6%">             <label id="lblCWAFDEP'.$cwId.'" >'.number_format( $cw['af_dep']*100, 2, '.', ',').'</label><input type="hidden" name="CWAFDEP'.$cwId.'" id="CWAFDEP'.$cwId.'" value="'.$cw['af_dep'].'">%</td>'
                                                            .'      <td valing="center" class="rightCell"  width="15%">      <br>$ <label id="lblCWDVC'.$cwId.'"   >'.number_format( $cw['dvc_dep']    , 2, '.', ',').' </label> mxn<input type="hidden" class="CWDEP" name="CWDVC'.$cwId.'" id="CWDVC'.$cwId.'" value="'.$cw['dvc_dep'].'"><br>'
                                                            .'                                                                   $ <label id="lblCWDVC'.$cwId.'usd">'.number_format(($cw['dvc_dep']/$er), 2, '.', ',').'</label> usd</td>'
                                                            .'</tr>';
                                           }                                    
        $tablaAECW   .=        "           <tr><td colspan='4' class='subTotRow11 adprice' >".$param['depTotCW']."</td>"
                . "                            <td class='subTotRow11 adprice'>$ <label id='subTotalCW'>".number_format($totalCW, 2, '.', ',')."</label> mxn<br>$ <label id='subTotalCWusd'>".number_format($totalCW/$er, 2, '.', ',')."</label> usd</td>
                                               <td colspan='5' class='subTotRow11 adprice celdaDiv'>$ <label id='subTotalCWDEP'>".number_format($totalCWDEP, 2, '.', ',')."</label> mxn<br>$ <label id='subTotalCWDEPusd'>".number_format($totalCWDEP/$er, 2, '.', ',')."</label> usd</td>
                                           </tr>
                                           <tr><td colspan='4' class='subTotRow11 adprice' >".$param['TaeTcw']."</td>
                                               <td class='subTotRow11 adprice'>$ <label id='subTotalAEWC'>".number_format(($totalCW+$totalAE), 2, '.', ',')."</label> mxn<br>$ <label id='subTotalAEWCusd'>".number_format(($totalCW+$totalAE)/$er, 2, '.', ',')."</label> usd</td>
                                               <td colspan='5' class='subTotRow11 adprice celdaDiv'>$ <label id='subTotalAEWCDEP'>".number_format($totalCWDEP+$totalAEDEP, 2, '.', ',')."</label> mxn<br>$ <label id='subTotalAEWCDEPusd'>".number_format(($totalCWDEP+$totalAEDEP)/$er, 2, '.', ',')."</label> usd</td>
                                           </tr>
                                           <tr><td colspan='4' class='subTotRow11 adprice' >".$param['depTot']."</td>
                                               <td class='subTotRowAM adprice'>$ <label id='subTotalRC'>".number_format(($totalCW+$totalAE+$totalRCN), 2, '.', ',')."</label> mxn<br>$ <label id='subTotalRCusd'>".number_format(($totalCW+$totalAE+$totalRCN)/$er, 2, '.', ',')."</label> usd</td>
                                               <td colspan='5' class='subTotRowCA adprice celdaDiv'>$ <label id='subTotalRCDEP'>".number_format(($totalCWDEP+$totalAEDEP+$totalDEP), 2, '.', ',')."</label> mxn<br>$ <label id='subTotalRCDEPusd'>".number_format(($totalCWDEP+$totalAEDEP+$totalDEP)/$er, 2, '.', ',')."</label> usd</td>
                                           </tr>
                                    </tbody>
                               </table>
                               "
                               .form_input(array('name'=>'numColumn', 'type'=>'hidden', 'id'=>'numColumn', 'value'=> $numColumn))
                               .form_input(array('name'=>'numRow'   , 'type'=>'hidden', 'id'=>'numRow'   , 'value'=> $numRow))
                               .form_input(array('name'=>'numAE'    , 'type'=>'hidden', 'id'=>'numAE'    , 'value'=> $numAE))
                               .form_input(array('name'=>'numCW'    , 'type'=>'hidden', 'id'=>'numCW'    , 'value'=> $numCW));         
         
        return $tablaACATOT. br(4) . $tablaAECW.br(2);
         
        } catch (Exception $e) {echo 'creaTablaACA Excepción: ',  $e, "\n";}  
    }
 
public function creaTablaCPD($datos,$leyendas)
    {
        $tablaCPD = "";
     try{
         $er        = $datos['er'];         
         $tablaCPD .= "<table class='tableGrid' cellspacing='0' cellpadding='0' width='100%' id='tblCPD'>
                                    <thead><tr><th class='leftCell apv' colspan='6'>".img( array('src'=>base_url()."images/new.png","title"=>"Agregar".$leyendas['cpdTit'],"class"=>"addCPD pointer")).br(1).$leyendas['cpdTit']."</th>
                                           </tr>
                                           <tr><th class='leftCell apv' width='5%'  >".$leyendas['l12']."</th>
                                               <th class='centerCell apv' width='24%' >".$leyendas['l13']."</th>                                               
                                               <th class='centerCell apv' width='12%' >".$leyendas['un']."</th>
                                               <th class='centerCell apv' width='12%' >".$leyendas['qt']."</th>
                                               <th class='centerCell apv' width='27%' >".$leyendas['uc']."</th>
                                               <th class='rightCell  apv' width='18%'>".$leyendas['cpdTbl']."</th>                                               
                                           </tr>
                                    </thead>
                                    <tbody>";
                                           $cpdId    = 0;
                                           $totalCPD = 0;
                                           
                                           foreach ($datos['cpd'] as $cpd)
                                           {   $cpdId++;
                                               $totalCPD    = $totalCPD + ($cpd['unit_value'] * $cpd['amount']);
                                               
                                               $tablaCPD   .= '<tr> <td valing="center" class="leftCell" >'.img(array('src'=>base_url()."images/close.png","title"=>"Delete ".$leyendas['cpdTit'],"id"=>"CPD".$cpdId,"class"=>"delCPD pointer")).nbs(2).$cpdId.'. </td>
                                                                    <td valing="center" class="leftCell"><input type="text" name="CPD'.$cpdId.'" id="CPD'.$cpdId.'" value="'.$cpd['leyenda'].'" class="validate[required,custom[onlyLetterNumber]] text-input conborde" placeholder="'.$leyendas['cpdTit'].'" maxlength="60" size="16"></td>'
                                                            .'      <td valing="center" class="centerCell">            <input type="text" name="CPDUN'.$cpdId.'"  id="CPDUN'.$cpdId.'" value="'.$cpd['unit'].'"       class="validate[required,custom[onlyLetterNumber]] text-input conborde" placeholder="Unit" maxlength="40" size="3"></td>'
                                                            .'      <td valing="center" class="centerCell">            <input type="text" name="CPDAM'.$cpdId.'"  id="CPDAM'.$cpdId.'" value="'.$cpd['amount'].'"     class="conborde" onchange="calculaTotCPD(\'CPD\','.$cpdId.','.$er.')" placeholder="0.00" maxlength="10" size="2"></td>'                                                            
                                                            .'      <td valing="center" class="centerCell">            <input type="text" name="CPDUV'.$cpdId.'Mask"  id="CPDUV'.$cpdId.'Mask" value="'.$cpd['unit_value'].'" class="pmask conborde" onchange="calculaTotCPD(\'CPD\','.$cpdId.','.$er.')" placeholder="0.00" maxlength="20" size="8">mxn <input type="hidden" name="CPDUV'.$cpdId.'"  id="CPDUV'.$cpdId.'" value="'.$cpd['unit_value'].'">'
                                                       . '                                                       <br>$<label id="CPDUV'.$cpdId.'usd">'.number_format($cpd['unit_value']/$er, 2, '.', ',').'</label> usd</td>'
                                                            .'      <td valing="center" class="rightCell">       <br>$ <label id="lblCPDTO'.$cpdId.'"   >'.number_format(  $cpd['unit_value']*$cpd['amount']      , 2, '.', ',').'</label> mxn<input type="hidden" class="CPD" name="CPDTO'.$cpdId.'" id="CPDTO'.$cpdId.'" value="'.($cpd['unit_value'] * $cpd['amount']).'"><br>'
                                                            .'                                                                   $ <label id="lblCPDTO'.$cpdId.'usd">'.number_format((($cpd['unit_value']*$cpd['amount'])/$er), 2, '.', ',').'</label> usd</td>'
                                                            .'</tr>';
                                           }                                    
        $tablaCPD   .=        "           <tr><td colspan='5' class='subTotRow11 apv' >".$leyendas['cpdTot']."</td>
                                           <td class='subTotRow11 apv'>$ <label id='subTotalCPD'>".number_format($totalCPD, 2, '.', ',')."</label> mxn<br>$ <label id='subTotalCPDusd'>".number_format($totalCPD/$er, 2, '.', ',')."</label> usd</td>                                               
                                           </tr>                               
                                    </tbody>
                               </table>
                               "    
                               .form_input(array('name'=>'numCPD', 'type'=>'hidden', 'id'=>'numCPD', 'value'=> $cpdId));
         return array("tabla"=>$tablaCPD,"total"=>$totalCPD);
        } catch (Exception $e) {echo 'creaTablaCPD Excepción: ',  $e, "\n";}  
        
    }    
 
    public function creaAP($tipoComp,$ap,$er,$ft,$ftyear)
    {
        if ($tipoComp==="CRL")
                { $AP='$'.number_format($ap/$er, 2, '.', ',').' usd/m2/mo'.br(1).'$'.number_format(($ap/$er)*$ftyear, 2, '.', ',').' usd/ft2/yr';}
             else
                { $AP='$'.number_format($ap/$er, 2, '.', ',').' usd/m2'.br(1).'$'.number_format(($ap/$er)/$ft, 2, '.', ',').' usd/ft2'; }
        
        return $AP;
    }
    
    public function creaUnidades($tipoComp,$ap,$er,$ft,$ftyear)
    {
        if ($tipoComp==="CRL")
                { $unitValueUSD='$'.number_format($ap, 2, '.', ',').' usd/m2/mo'.br(1).'$'.number_format(($ap/$er), 2, '.', ',').' usd/m2/mo'.br(2).'$'.number_format(($ap/$er)*$ftyear, 2, '.', ',').' usd/ft2/yr';}
             elseif($tipoComp==="CRS")
                { $unitValueUSD='$'.number_format($ap, 2, '.', ',').' usd/m2'.br(1).'$'.number_format(($ap/$er)/$ft, 2, '.', ',').' usd/ft2'; }
             else
              { $unitValueUSD='$'.number_format($ap/$er, 2, '.', ',').' usd/m2'.br(1).'$'.number_format(($ap/$er)/$ft, 2, '.', ',').' usd/ft2'; }

       return $unitValueUSD;              
    }
    
    /********************************** OJO FUNCIONES DUPLICADAS DE UTILS***********/
    
    public function creaLocation($calle,$num,$col,$mun,$edo,$cp)
    {     
        $strCalle = empty($calle)?'':$calle;
        $strNum   = empty($num)?'':' '.$num;
        $strCol   = empty($col)?'':', '.$col;
        $strMun   = empty($mun)?'':', '.$mun;
        $strEdo   = empty($edo)?'':', '.$edo;
        $strCP    = empty($cp)?'':', CP '.$cp;
        return $strCalle.$strNum.$strCol.$strMun.$strEdo.$strCP;
    }
    
    public function creaCol($col,$mun,$edo,$cp)
    {             
        $strCol   = empty($col)?'':$col;
        $strMun   = empty($mun)?'':', '.$mun;
        $strEdo   = empty($edo)?'':', '.$edo;
        $strCP    = empty($cp)?'':', CP '.$cp;
        return $strCol.$strMun.$strEdo.$strCP;
    }
 
    public function redondearHermes($num)
    { 
        $ceros  = "";
        $digitos = (strlen(intval($num)))<=3 ? 1: (strlen(intval($num))) <= 6 ?3:4;
        
        for($x = 1; $x <= $digitos; $x++ )
            { $ceros .= "0"; }    

        $divInt = (int)"1".$ceros;        
        $numRedondeado = $num/$divInt;        
        
        return round($numRedondeado,0)*$divInt;        
    }
    
    public function calculaEO($entradas)
    {   
        $eo1 = $entradas['tvl'] + $entradas['rcn'] - $entradas['tpd'];        
        $eo2 = $entradas['cr']; 
       // $eo2 = 8.63;  

        $eo3 = round(($eo1 * ($eo2/100)),4);        
        $eo4 = $entradas['ex'];
        $eo5 = round(($eo3 / (1-($eo4/100))),4);            
        $eo6 = $entradas['vcl'];        
        $eo7 = round(($eo5 / (1-($eo6/100))),4);        
        $eo8 = empty($entradas['tra'])?0:round(($eo7 / $entradas['tra']),4);        
        $eo9 =  round(($eo8 / 12),4);
        $eo10 = $entradas['ivLease'];
        $eo11 = '';
        $eo12 = round($eo9 - $eo10,4);        
        $eo13 = round($eo12 * ($eo6/100),4);
        $eo14 = round($eo12 - $eo13,4);        
        $eo15 = round($eo14 * ($eo4/100),4);
        $eo16 = round($eo14 - $eo15,6);
        $eo17 = round(($eo16*12)*$entradas['tra'],4);        
        $eo18 = $eo2;        
        $eo19 = empty($eo18)?0:$eo17 / ($eo18/100);

        $salidas = array("eo1"=>$eo1,
                         "eo2"=>$eo2,
                         "eo3"=>$eo3,
                         "eo4"=>$eo4,                         
                         "eo5"=>$eo5,            
                         "eo6"=>$eo6,
                         "eo7"=>$eo7,
                         "eo8"=>$eo8,
                         "eo9"=>$eo9,
                         "eo10"=>$eo10,
                         "eo11"=>$eo11,
                         "eo12"=>$eo12,
                         "eo13"=>$eo13,
                         "eo14"=>$eo14,
                         "eo15"=>$eo15,
                         "eo16"=>$eo16,
                         "eo17"=>$eo17,
                         "eo18"=>$eo18,
                         "eo19"=>$eo19,
                         "tvl"=>$entradas['tvl'],
                         "rcn"=>$entradas['rcn'],
                         "tpd"=>$entradas['tpd'],
                        );
        return $salidas;
    }
    
    
    public function creaTablaARE($datos,$objectTable)
    { 
     $tablaARE = "";
     try{
         $objectTable -> clear();        
         $objectTable -> set_template(array('table_open' => "<table class='tableGrid' cellspacing='0' cellpadding='0' width='100%' id='tblARE'>" ));        
         $objectTable -> set_heading (array("RESIDUAL"));                  
         
         $tablaARE .= $objectTable->generate();
         
         return $tablaARE;
        } catch (Exception $e) {echo 'creaTablaARE Excepción: ',  $e, "\n";}  
    }
    
    private function capitalizationRateNumAverage($source, $values)
    {
      try{
       if( $source != 0 ) 
            {array_push($values,$source);}                  
        } catch (Exception $e) {echo 'capitalizationRateNumAverage Excepción: ',  $e, "\n";}  
        
        return $values;
    }
}