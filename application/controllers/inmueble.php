<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inmueble extends CI_Controller {
        
    public $registros_x_pagina = 10;
    public $galeria            = "gallery/";
    public $export             = "exp_formatos/";
    
    
    public function __construct()
    {
        parent::__construct();
        
        $this->load->database('jll_hermes_con');     
        $this->load->model('md_inmuebles');
        $this->load->model('md_comparables');
        $this->load->model('md_catalogo');
        $this->load->model('md_gallery');
        $this->load->library('session');        
        $this->load->library('table');
        $this->load->library('Utils');
        $this->load->library('HelperController');
        $this->load->helper('array');
    }
     
    
    public function index()
    {  $this->validaSesion(NULL,FALSE);    
       $data['inSel'] = "class=\"selected\"";
       $data['usSel'] = "";
       $data['cmSel'] = "";
       $data['titulos'] = array("navegador" => "JLL PROYECTO HERMES",                                
                                 "ventana"   => "HERMES",
                                 "frase"     => "\"Hacer de lo simple algo complicado es com&uacute;n; hacer de lo complicado algo simple, incre&iacute;blemente simple, es creatividad\" <br>- Charles Mingus",
                                 "titulo"    => "Inmuebles ");

        $data['usuario']  = $this->myData['usuario'];
        $data['iconuser'] = $this->myData['iconuser'];
        $data['foto']     = $this->myData['foto'];

        $this -> table -> clear();                  
        $this -> table -> set_template(array('table_open' => '<table cellspacing="0" width="100%" id="inmueblesTable">' ));        
        $this -> table -> set_heading(array('','','Prepared For', 'Location','Report Num','Effective Date','Status','Created By', 'Actions'));
        $this -> table -> add_row(' ');
                                
        $data['gridInmuebles']   = $this->table->generate();
        $data['registrosPagina'] = $this->registros_x_pagina;
        
        $status = "";
        foreach($this -> md_catalogo -> poblarRadioButtonStatus(NULL,NULL,NULL) as $rd)
            {$status = $status."<label class='radio'><input type='radio' name='status' value='".$rd['value']."' ".$rd['checked']."><i></i>".$rd['label']."</label>";}
    
        $data['status'] = $status;
            
        $this->load->view('av',$data);
    }
                       
        
    public function paginarAX()
    {
    try{
        $vs = $this->validaSesion(TRUE,FALSE);
        if( isset($vs['session']))
        {   echo json_encode ($vs); 
            exit(0);
        }
        
        $this->load->helper('date');
        $status = $this -> input -> post('status');
        $pagina = $this -> input -> post('pagina');
        $f1     = $this -> input -> post('f1');
        $f2     = $this -> input -> post('f2');
        $f3     = $this -> input -> post('f3');
        $f4     = $this -> input -> post('f4');
        $f5     = $this->utils->validaDateFormat($this->input->post('f5'));

        $inmuebles = $this -> md_inmuebles -> traeInmueblesFiltros($this->registros_x_pagina,$pagina,$f1,$f2,$f3,$f4,$f5,$status,$this->galeria);            

        echo json_encode ($inmuebles);                          

    } catch (Exception $e) {echo ' paginarAX Excepción: ',  $e, "\n";}      
    }
    
       
    public function forma($accion=null,$id_in=null)
        {
         try{
             $this->validaSesion(NULL,FALSE);
             
             $data['inSel'] = "class=\"selected\"";
             $data['usSel'] = "";
             $data['cmSel'] = "";
             $this->load->helper('date');
             $this->load->model('md_usuario');
             $idStatus = "1";

             $data['titulos'] = array("navegador" => "JLL PROYECTO HERMES",                              
                                      "ventana"   => "HERMES",
                                      "frase"     => "\"Hacer de lo simple algo complicado es com&uacute;n; hacer de lo complicado algo simple, incre&iacute;blemente simple, es creatividad\" <br>- Charles Mingus",
                                      "titulo"    => "VALUATION REPORT");

             $data['usuario']         = $this->myData['usuario'];
             $data['iconuser']        = $this->myData['iconuser'];
             $data['foto']            = $this->myData['foto'];
             $data['accion']          = $accion;
             $data['usuario']         = $this->myData['usuario'];
             $data['iconuser']        = $this->myData['iconuser'];
             $data['foto']            = $this->myData['foto'];             
             $data['teamjll']         = $this->md_usuario -> poblaSelect();
             $data['dirGallery']      = $this->galeria;
             $data['registrosPagina'] = $this->registros_x_pagina;               
             $data['gridCompCRL']     = $this->creaTablasComparables()[0]['CRL'];
             $data['gridCompCLS']     = $this->creaTablasComparables()[1]['CLS'];
             $data['gridCompCRS']     = $this->creaTablasComparables()[2]['CRS'];             
             $data['fechaReporte']    = date(" l, F j, Y ");
             $clave_ano_name          = date("y");
             $idiom                   = "";
             
             if($accion=="N")
             {
                $data['id_in']    = substr(''.now(), -5);
                $inmueble         = array("frontPage" => array(array("id_status"=>$idStatus,"preparedFor"=>'',"propertyOf"=>'',"fotoPortada"=>'logoJLL.jpg',"calle"=>'',"num"=>'',"col"=>'',"mun"=>'',"edo"=>'',"cp"=>'',"effectiveDate"=>'',"repNum"=>'',"date_exchange_rate"=>null,'origen_name'=>0,'concepto_name'=>"0",'clave_ano_name'=>$clave_ano_name,'consecutivo_name'=>1,'dia_name'=>null,'mes_name'=>null,'repNumCap'=>null,'address_jll'=>'Pedregal 57 piso 2, Col. Lomas de Chapultepec, C.P. 11000, CDMX, Mexico.','mt'=>0,'et'=>0,"appraisal_num"=>null,"fecha_name"=>null,"idiom"=>'ENG')));
                $ignoreStatus     = ($this->myData['tipo'] =="1")? array(3): array(3, 4, 5);
                $status           = "";
                foreach($this->md_catalogo->poblarRadioButtonStatus($this->myData['tipo'],$ignoreStatus,$idStatus) as $rd)
                    { $status = $status."<label class='radio'><input type='radio' name='status' value='".$rd['value']."' ".$rd['checked']."><i></i>".$rd['label']."</label>"; }
             }
             else
             {   $idiom           = $this -> md_inmuebles->traeIdiom($id_in);
                 $inmueble        = $this -> traeInmueble($id_in,$idiom,'d',TRUE);                 
                 $data['id_in']   = $id_in;
                 $inmueble['frontPage'][0]['consecutivo_name'] = $this->utils->generaConsecutivo($inmueble['frontPage'][0]['consecutivo_name']);
                 
                 if ($this->myData['tipo'] == "1")
                    { $ignoreStatus = array(0); }
                 elseif($inmueble['frontPage'][0]['id_status']=="4")
                    { $ignoreStatus = array(3, 4); }
                        else { $ignoreStatus = array(3, 4, 5); }
                       
                 $status  = "";                 
                 foreach($this -> md_catalogo -> poblarRadioButtonStatus($this->myData['tipo'],$ignoreStatus,$inmueble['frontPage'][0]['id_status']) as $rd)
                    {$status = $status."<label class='radio'><input type='radio' name='status' value='".$rd['value']."' ".$rd['checked']."><i></i>".$rd['label']."</label>";}
             }
             
             $idiom   = "";
             foreach($this->md_catalogo->poblarRadioButtonIdiom($inmueble['frontPage'][0]['idiom']) as $rd)
                    { $idiom = $idiom."<label class='radio'><input type='radio' name='idiom' value='".$rd['value']."' ".$rd['checked']."><i></i><img title='Change Idiom of Report' src='".base_url()."images/".$rd['value'].".png'>".$rd['label']."</label>"; }
             
             $data['origenOp']    = $this->md_catalogo-> poblarSelect('origen',false);
             $data['conceptos']   = $this->md_catalogo-> traeConceptos();
             $df                  = $this->md_inmuebles->traeDatosFinancierosIn($data['id_in']);
             $data['ft']          = $df['ft'];
             $data['ftyear']      = $df['ftyear'];
             $data['er']          = $df['exchange_rate'];
             $data['d_er']        = '<br><label style="font-size:10px;">'.$df['date_exchange_rate'].'</label>';
             $data["updateER"]    = '<img title="Update Exchange Rate" id="updateER" width="15" height="15" src="'.base_url().'images/edit.png">';
             $data['inmueble']    = $inmueble;
             $data['status']      = $status;
             $data['idiom']       = $idiom;
             $data['tablaPHCAA2'] = $this->helpercontroller->creaTablasCompAdjus($this->md_comparables->traeRCporTrabajo($id_in,"CLS"),$id_in,"CLS");
             $data['tablaPHCAA4'] = $this->helpercontroller->creaTablasCompAdjus($this->md_comparables->traeRCporTrabajo($id_in,"CRL"),$id_in,"CRL");
             $data['tablaPHCAA6'] = $this->helpercontroller->creaTablasCompAdjus($this->md_comparables->traeRCporTrabajo($id_in,"CRS"),$id_in,"CRS");
             $data['reportPart']  = $this->md_inmuebles->traeReportPart($id_in,"FP");
             
             $this->load->view('formulario_in',$data);

            } catch (Exception $e) {echo ' forma Excepción: ',  $e, "\n";}      
        }       
            
    public function guardar($id_in,$accion=null,$tipo=null,$status=null,$idiom=null)
    {try{   $this->validaSesion(NULL,FALSE);
            $this->load->helper('date');
            $this->load->model('md_gallery');
                       
            $hoy           = standard_date('DATE_W3C', time());
            $def           = NULL;
            $camposForm    = NULL;            
            
            $camposForm = array('id_status'=>$status,'modificado_por'=>element('correo', $this->user['0']),"fecha_modificacion"=>$hoy,"idiom"=>$idiom);
            $this -> md_inmuebles -> update_inmuebles( $id_in,$camposForm);                               
            switch ($tipo) 
            {
                case "FP": //SECCION DEL FORMULARIO FRONT PAGE
                    if($accion == "E")// Flag to take action in Controller E= Update, G=Insert
                    {                          
                        $camposForm = array('id_status'=>$status,'modificado_por'=>element('correo', $this->user['0']),"preparedFor"=>$this->input->post('preparedFor'),"propertyOf"=>$this->input->post('propertyOf'),"fotoPortada"=>$this->input->post('fotoPortada'),"calle"=>$this->input->post('calle'),"num"=>$this->input->post('num'),"col"=>$this->input->post('col'),"mun"=>$this->input->post('mun'),"edo"=>$this->input->post('edo'),"cp"=>$this->input->post('cp'),"effectiveDate"=>$this->utils->hermesDateFormat($this->input->post('effectiveDate')),"repNum"=>$this->input->post('repNum'),"fecha_modificacion"=>$hoy, "origen_name"=>$this->input->post('origen_name'), "concepto_name"=>$this->input->post('concepto_name'), "clave_ano_name"=>$this->input->post('clave_ano_name'), "consecutivo_name"=>$this->input->post('consecutivo_name'),"repNumCap"=>$this->input->post('repNumCap'),"address_jll"=>$this->input->post('address_jll'),"mt"=>$this->input->post('mt'),"et"=>$this->input->post('et'),"appraisal_num"=>$this->input->post('appraisal_num'),'idiom'=>$idiom );
                        $this -> md_inmuebles -> update_inmuebles( $id_in,$camposForm);
                    } 
                    else
                    {                          
                        $camposForm = array('id_in'=>$id_in,'id_status'=>$status,'correo'=>element('correo', $this->user['0']),"preparedFor"=>$this->input->post('preparedFor'),"propertyOf"=>$this->input->post('propertyOf'),"fotoPortada"=>$this->input->post('fotoPortada'),"calle"=>$this->input->post('calle'),"num"=>$this->input->post('num'),"col"=>$this->input->post('col'),"mun"=>$this->input->post('mun'),"edo"=>$this->input->post('edo'),"cp"=>$this->input->post('cp'),"effectiveDate"=>$this->utils->hermesDateFormat($this->input->post('effectiveDate')),"repNum"=>$this->input->post('repNum'),"fecha_alta"=>$hoy, "exchange_rate"=>$this->input->post('exchange_rate'),"date_exchange_rate"=>standard_date('DATE_W3C', time()), "origen_name"=>$this->input->post('origen_name'), "concepto_name"=>$this->input->post('concepto_name'), "clave_ano_name"=>$this->input->post('clave_ano_name'), "consecutivo_name"=>$this->input->post('consecutivo_name'),"repNumCap"=>$this->input->post('repNumCap'),"address_jll"=>$this->input->post('address_jll'),"mt"=>$this->input->post('mt'),"et"=>$this->input->post('et'),"appraisal_num"=>$this->input->post('appraisal_num'),'idiom'=>$idiom  );
                        $this -> md_inmuebles -> insert_inmuebles($camposForm);                
                    }
                break;
                case "LT": //SECCION DEL FORMULARIO LETTER                     
                    $this -> md_inmuebles -> borraSeccionInmueble($id_in,'definiciones_in');
                    $this -> md_inmuebles -> borraSeccionInmueble($id_in,'letter_in');
                    
                    $camposForm = array('id_in'=>$id_in,'client'=>$this->input->post('client'),'client_logo'=>$this->input->post('clientLogo'),'client_addres'=>$this->input->post('client_addres'),'client_addres_2'=>$this->input->post('client_addres_2'),'atn_officer'=>$this->input->post('atn_officer'),'jd_atn_officer'=>$this->input->post('jd_atn_officer'),'jd_atn_officer_2'=>$this->input->post('jd_atn_officer_2'),'parrafo1'=>$this->input->post('LTparrafo1'),'parrafo2'=>$this->input->post('LTparrafo2'),'parrafo3'=>$this->input->post('LTparrafo3'),'texto1'=>$this->input->post('LTtexto1'),'texto2'=>$this->input->post('LTtexto2'),'texto3'=>$this->input->post('LTtexto3'),'firma_prin'=>$this->input->post('firma_prin'),'firma_sec'=>$this->input->post('firma_sec'),'LTchbox1'=>$this->input->post('LTchbox1'),'LTchbox2'=>$this->input->post('LTchbox2'),'LTchbox3'=>$this->input->post('LTchbox3'));
                    $this -> md_inmuebles -> insertaSeccionInmueble($camposForm,'letter_in');
                    
                    $def =$this->input->post('def');
                  
                    foreach ($def as $d)
                        { $this -> md_inmuebles -> insertaSeccionInmueble(array('id_definicion'=>$d,'id_in'=>$id_in),'definiciones_in'); }                     
                break;
                case "SW": //SECCION DEL FORMULARIO SCOPE OF WORK                    
                    $this -> md_inmuebles -> borraSeccionInmueble($id_in,'scopeofwork_in');                                                                                                                                                                                                                            
                    $camposForm = array('id_in'=>$id_in,'SWParrafo1'=>$this->input->post('SWParrafo1'),'SWTit1'=>$this->input->post('SWTit1'),'SWText1'=>$this->input->post('SWText1'),'SWTit2'=>$this->input->post('SWTit2'),'SWText2_1'=>$this->input->post('SWText2_1'),'SWText2_2'=>$this->input->post('SWText2_2'),'providedBy'=>$this->input->post('SWprovidedBy'),'SWText2_3'=>$this->input->post('SWText2_3'),'SWTit3'=>$this->input->post('SWTit3'),'SWText3_1'=>$this->input->post('SWText3_1'),'inspector'=>$this->input->post('SWinspector'),'dateInspection'=>$this->utils->hermesDateFormat($this->input->post('SWdateInspection')),'SWText3_2'=>$this->input->post('SWText3_2'),'SWTit4'=>$this->input->post('SWTit4'),'SWText4_1'=>$this->input->post('SWText4_1'),'SWText4_2'=>$this->input->post('SWText4_2'),'SWText4_3'=>$this->input->post('SWText4_3'),'SWTit5'=>$this->input->post('SWTit5'),'SWText5'=>$this->input->post('SWText5'),'SWTit6'=>$this->input->post('SWTit6'),'SWText6'=>$this->input->post('SWText6'),'SWTit7'=>$this->input->post('SWTit7'),'SWText7'=>$this->input->post('SWText7') );
                    $this -> md_inmuebles -> insertaSeccionInmueble($camposForm,'scopeofwork_in');
                    
                break;
                case "SM": //SECCION DEL FORMULARIO SUMMARY                    
                    $this -> md_inmuebles -> borraSeccionInmueble($id_in,'summary_in');
                    $ch1 = "";
                    $ch2 = "";
                    $ch3 = "";
                    $ch4 = "";
                    $ch5 = "";
                    
                    if (isset($_POST['SM_ch_vc4'])) {$ch4="1";} else {$ch4="0";}
                    if (isset($_POST['SM_ch_vc5'])) {$ch5="1";} else {$ch5="0";}
                    
                    $camposForm = array('id_in'=>$id_in,'SMTit1'=>$this->input->post('SMTit1'),'SMText1'=>$this->input->post('SMText1'),'SMTit2'=>$this->input->post('SMTit2'),'SMText2'=>$this->input->post('SMText2'),'SMTit3'=>$this->input->post('SMTit3'),'SMText3'=>$this->input->post('SMText3'),'SMTit4'=>$this->input->post('SMTit4'),'SMText4'=>$this->input->post('SMText4'),'SMTit5'=>$this->input->post('SMTit5'),'SMText5'=>$this->input->post('SMText5'),'SMTit6'=>$this->input->post('SMTit6'),'SMText6'=>$this->input->post('SMText6'),'SMTit7'=>$this->input->post('SMTit7'),'SMText7'=>$this->input->post('SMText7'),'SMvc1_1'=>$this->input->post('SMvc1_1'),'SMvc1_2'=>$this->input->post('SMvc1_2'),'SMvc1_3'=>$this->utils->hermesDateFormat($this->input->post('SMvc1_3')),'SMvc1_4'=>$this->input->post('SMvc1_4'),'SMvc2_1'=>$this->input->post('SMvc2_1'),'SMvc2_2'=>$this->input->post('SMvc2_2'),'SMvc2_3'=>$this->utils->hermesDateFormat($this->input->post('SMvc2_3')),'SMvc2_4'=>$this->input->post('SMvc2_4'),'SMvc3_1'=>$this->input->post('SMvc3_1'),'SMvc3_2'=>$this->input->post('SMvc3_2'),'SMvc3_3'=>$this->utils->hermesDateFormat($this->input->post('SMvc3_3')),'SMvc3_4'=>$this->input->post('SMvc3_4'),'SMvc4_1'=>$this->input->post('SMvc4_1'),'SMvc4_2'=>$this->input->post('SMvc4_2'),'SMvc4_3'=>$this->utils->hermesDateFormat($this->input->post('SMvc4_3')),'SMvc4_4'=>$this->input->post('SMvc4_4'),'SMvc5_1'=>$this->input->post('SMvc5_1'),'SMvc5_2'=>$this->input->post('SMvc5_2'),'SMvc5_3'=>$this->utils->hermesDateFormat($this->input->post('SMvc5_3')),'SMvc5_4'=>$this->input->post('SMvc5_4'),'SM_ch_vc1'=>$ch1,'SM_ch_vc2'=>$ch2,'SM_ch_vc3'=>$ch3,'SM_ch_vc4'=>$ch4,'SM_ch_vc5'=> $ch5);
                    $this -> md_inmuebles -> insertaSeccionInmueble($camposForm,'summary_in');        
                break;
                case "API": 
                    $this -> md_inmuebles -> borraSeccionInmueble($id_in,'apprisal_API');

                    //$campo11
                    //$campo12
                    $campo11 = $this->input->post('AIText11');
                    $campo12 = $this->input->post('AITit12');
                    
                    $camposForm = array('id_in' => $id_in,"AIText1_1" => $this->input->post('AIText1_1'),"AIText1_2" => $this->input->post('AIText1_2'),"AITit2" => $this->input->post('AITit2'),"AIText2" => $this->input->post('AIText2'),"AITit3" => $this->input->post('AITit3'),"AIText3" => $this->input->post('AIText3'),"AITit4" =>$this->input->post('AITit4'),"AIText4" =>$this->input->post('AIText4'),"AITit5" =>$this->input->post('AITit5'),"AITit6" =>$this->input->post('AITit6'),"AITit7" =>$this->input->post('AITit7'),"AIText7" =>$this->input->post('AIText7'),"AITit8" =>$this->input->post('AITit8'),"AITit9" =>$this->input->post('AITit9'),"AIText9" =>$this->input->post('AIText9'),"AITit10" =>$this->input->post('AITit10'),"AITit11" =>$this->input->post('AITit11'),"AIText11 "=>$campo11,"AITit12" =>$campo12,"AIText12_1" =>$this->input->post('AIText12_1'),"AIText12_2" =>$this->input->post('AIText12_2'),"AIText12_3" =>$this->input->post('AIText12_3'),"AIText12_4" =>$this->input->post('AIText12_4'),"imgAPI"=>$this->input->post('imgAPI'));
                    $this -> md_inmuebles -> insertaSeccionInmueble($camposForm,'apprisal_API');
                break;            
                case "APII": 
                    $this -> md_inmuebles -> borraSeccionInmueble($id_in,'apprisal_APII');                                        
                    $camposForm = array('id_in' => $id_in,"AIITit1"     => $this->input->post("AIITit1"),"imgAPII_1"   => $this->input->post("imgAPII_1"),"imgAPII_2"   => $this->input->post('imgAPII_2'),"imgAPII_3"   => $this->input->post('imgAPII_3'),"AIITit2"     => $this->input->post("AIITit2"),"AIIText2_1"  => $this->input->post("AIIText2_1"),"AIIText2_2"  => $this->input->post("AIIText2_2"),"AIITit3"     => $this->input->post("AIITit3"),"AIIText3"    => $this->input->post("AIIText3"),"AIITit4"     => $this->input->post("AIITit4"),"AIIText4"    => $this->input->post("AIIText4"),"AIITit5"     => $this->input->post("AIITit5"),"AIIText5"    => $this->input->post("AIIText5"),"AIITit6"     => $this->input->post("AIITit6"),"AIIText6"    => $this->input->post("AIIText6"),"AIITit7"     => $this->input->post("AIITit7"),"AIIText7_1"  => $this->input->post("AIIText7_1"),"AIIText7_2"  => $this->input->post("AIIText7_2"));
                    $this -> md_inmuebles -> insertaSeccionInmueble($camposForm,'apprisal_APII');
                break;
                case "APIII": 
                    $this -> md_inmuebles -> borraSeccionInmueble($id_in,'apprisal_APIII');
                    $camposForm = array('id_in' => $id_in,"AIIIText1"     => $this->input->post("AIIIText1"));
                    $this -> md_inmuebles -> insertaSeccionInmueble($camposForm,'apprisal_APIII');
                break;
                case "APIV": 
                    $this -> md_inmuebles -> borraSeccionInmueble($id_in,'apprisal_APIV');
                    $camposForm = array('id_in' => $id_in,"AIVTit1"     => $this->input->post("AIVTit1"),"AIVText1"   => $this->input->post("AIVText1"),"AIVTit2"   => $this->input->post('AIVTit2'),"AIVText2"   => $this->input->post('AIVText2'),"AIVTit3"     => $this->input->post("AIVTit3"),"AIVText3"  => $this->input->post("AIVText3"),"AIVTit4"  => $this->input->post("AIVTit4"),"AIVText4"     => $this->input->post("AIVText4"));
                    $this -> md_inmuebles -> insertaSeccionInmueble($camposForm,'apprisal_APIV');
                break;               
                case "APV":
                     $this -> md_inmuebles -> borraSeccionInmueble($id_in,'apprisal_APV');
                    
                    $camposForm = array('id_in'=>$id_in,'AVTit1'=>$this->input->post('AVTit1'),'AVText1'=>$this->input->post('AVText1'),'AVTit2'=>$this->input->post('AVTit2'),'AVText2_1'=>$this->input->post('AVText2_1'),'AVText2_2'=>$this->input->post('AVText2_2'),'AVTit3'=>$this->input->post('AVTit3'),'AVText3'=>$this->input->post('AVText3'),'AVTit4'=>$this->input->post('AVTit4'),'AVTit5'=>$this->input->post('AVTit5'),'AVTit6'=>$this->input->post('AVTit6'),'AVTit7'=>$this->input->post('AVTit7'),'AVText7_1'=>$this->input->post('AVText7_1'),'AVText7_2'=>$this->input->post('AVText7_2'),'AVText7_3'=>$this->input->post('AVText7_3'),'AVTit8'=>$this->input->post('AVTit8'),'AVText8_1'=>$this->input->post('AVText8_1'),'AVText8_2'=>$this->input->post('AVText8_2'),'AVTit9'=>$this->input->post('AVTit9'),'AVTit10'=>$this->input->post('AVTit10'),'AVText10'=>$this->input->post('AVText10'),'AVdfaNum'=>$this->input->post('AVdfaNum'), 'AVdefSNum'=>$this->input->post('AVdefSNum'),'AVoaNum'=>$this->input->post('AVoaNum'),'AVdefyNum'=>$this->input->post('AVdefyNum'),'AVoAdNum'=>$this->input->post('AVoAdNum') ,'AV_oe'=>$this->input->post('AV_oe'),'AV_vica'=>$this->input->post('AV_vica') );
                    $this -> md_inmuebles -> insertaSeccionInmueble($camposForm,'apprisal_APV');
                    
                    $this -> md_inmuebles -> borraDeterioro($id_in,"CP","annexs_ca_aecw_in");
                    $numCPD     = $this->input->post('numCPD');
                    $orden = 0;
                    for($cpd = 1; $cpd <= $numCPD; $cpd++)
                    { if(!empty($this->input->post('CPD'.$cpd)))
                        {$orden++;                        
                         $this -> md_inmuebles -> insertaSeccionInmueble(array("id_in"=>$id_in,"orden"=>$orden,"tipo"=>"CP","leyenda"=>$this->input->post('CPD'.$cpd),"amount"=>$this->input->post('CPDAM'.$cpd),"unit"=>$this->input->post('CPDUN'.$cpd),"unit_value"=>$this->input->post('CPDUV'.$cpd),"rcn"=>$this->input->post('CPDTO'.$cpd) ) ,"annexs_ca_aecw_in" ); }                        
                    }
                break;
                case "APVI": 
                    $this -> md_inmuebles -> borraSeccionInmueble($id_in,'apprisal_APVI');                    
                    $camposForm = array('id_in' => $id_in,"AVITit1" => $this->input->post("AVITit1"),"AVIText1" => $this->input->post("AVIText1"),"AVITit2" => $this->input->post("AVITit2"),"AVIText2" => $this->input->post("AVIText2"),"AVITit3" => $this->input->post("AVITit3"),"AVIText3" => $this->input->post("AVIText3"),"AVITit4" => $this->input->post("AVITit4"),"AVI_totExp" => $this->input->post("AVI_totExp"), "AVI_noi" => $this->input->post("AVI_noi"),"AVITit5" => $this->input->post("AVITit5"),"AVIText5_1" => $this->input->post("AVIText5_1"),"AVIText5_2" => $this->input->post("AVIText5_2"),"AVIText5_3" => $this->input->post("AVIText5_3"),"AVIText5_4" => $this->input->post("AVIText5_4"),"AVIText5_5" => $this->input->post("AVIText5_5"),"AVIText5_6" => $this->input->post("AVIText5_6"),"AVIText5_7" => $this->input->post("AVIText5_7"),"AVITit6" => $this->input->post("AVITit6"),"AVIText6_1" => $this->input->post("AVIText6_1"),"AVIText6_2" => $this->input->post("AVIText6_2"),"AVITit7" => $this->input->post("AVITit7"),"AVIText7" => $this->input->post("AVIText7"),"AVITit8" => $this->input->post("AVITit8"),"AVIText8_1" => $this->input->post("AVIText8_1"),"AVIText8_2" => $this->input->post("AVIText8_2"),"AVIpgi_1_Txt" => $this->input->post("AVIpgi_1_Txt"), "AVIpgi_1_Mon_mxn" => $this->input->post("AVIpgi_1_Mon_mxn"), "AVIpgi_1_Year_mxn" => $this->input->post("AVIpgi_1_Year_mxn"), "AVIpgi_2_Txt" => $this->input->post("AVIpgi_2_Txt"), "AVIpgi_2_Mon_mxn" => $this->input->post("AVIpgi_2_Mon_mxn"), "AVIpgi_2_Year_mxn" => $this->input->post("AVIpgi_2_Year_mxn"), "AVIpgi_3_Txt" => $this->input->post("AVIpgi_3_Txt"), "AVIpgi_3_Mon_mxn" => $this->input->post("AVIpgi_3_Mon_mxn"), "AVIpgi_3_Year_mxn" => $this->input->post("AVIpgi_3_Year_mxn"), "AVIpgi_4_Txt" => $this->input->post("AVIpgi_4_Txt"), "AVIpgi_4_Mon_mxn" => $this->input->post("AVIpgi_4_Mon_mxn"), "AVIpgi_4_Year_mxn" => $this->input->post("AVIpgi_4_Year_mxn"), "AVIpgi_mvcl_por" => $this->input->post("AVIpgi_mvcl_por"),"AVIoe_1_Txt" => $this->input->post("AVIoe_1_Txt"),"AVIoe_1_num1" => $this->input->post("AVIoe_1_num1"), "AVIoe_1_num2" => $this->input->post("AVIoe_1_num2"), "AVIoe_2_Txt" => $this->input->post("AVIoe_2_Txt"), "AVIoe_2_num1" => $this->input->post("AVIoe_2_num1"), "AVIoe_2_num2" => $this->input->post("AVIoe_2_num2"), "AVIoe_3_Txt" => $this->input->post("AVIoe_3_Txt"), "AVIoe_3_num1" => $this->input->post("AVIoe_3_num1"), "AVIoe_3_num2" => $this->input->post("AVIoe_3_num2"), "AVIoe_4_Txt" => $this->input->post("AVIoe_4_Txt"), "AVIoe_4_num1" => $this->input->post("AVIoe_4_num1"), "AVIoe_4_num2" => $this->input->post("AVIoe_4_num2"), "AVIoe_5_Txt" => $this->input->post("AVIoe_5_Txt"), "AVIoe_5_num1" => $this->input->post("AVIoe_5_num1"), "AVIoe_5_num2" => $this->input->post("AVIoe_5_num2"), "AVIoe_6_Txt" => $this->input->post("AVIoe_6_Txt"), "AVIoe_6_num1" => $this->input->post("AVIoe_6_num1"), "AVIoe_6_num2" => $this->input->post("AVIoe_6_num2"), "AVIoe_7_Txt" => $this->input->post("AVIoe_7_Txt"), "AVIoe_7_num1" => $this->input->post("AVIoe_7_num1"), "AVIoe_7_num2" => $this->input->post("AVIoe_7_num2"), "AVIoe_8_Txt" => $this->input->post("AVIoe_8_Txt"), "AVIoe_8_num1" => $this->input->post("AVIoe_8_num1"), "AVIoe_8_num2" => $this->input->post("AVIoe_8_num2"), "AVIoe_9_Txt" => $this->input->post("AVIoe_9_Txt"), "AVIoe_9_num1" => $this->input->post("AVIoe_9_num1"),"AVIoe_9_num2" => $this->input->post("AVIoe_9_num2"), "AVIoe_10_Txt" => $this->input->post("AVIoe_10_Txt"),"AVIoe_10_num1" => $this->input->post("AVIoe_10_num1"),"AVIoe_10_num2" => $this->input->post("AVIoe_10_num2"),"AVIoe_11_Txt" => $this->input->post("AVIoe_11_Txt"), "AVIoe_11_num1" => $this->input->post("AVIoe_11_num1"), "AVIoe_11_num2" => $this->input->post("AVIoe_11_num2"), "AVIoe_unf" => $this->input->post("AVIoe_unf"),"AVIsource_1_Txt" => $this->input->post("AVIsource_1_Txt"),"AVIsource_1_ran1" => $this->input->post("AVIsource_1_ran1"),"AVIsource_1_ran2" => $this->input->post("AVIsource_1_ran2"),"AVIsource_1_av" => $this->input->post("AVIsource_1_av"),"AVIsource_2_Txt" => $this->input->post("AVIsource_2_Txt"),"AVIsource_2_ran1" => $this->input->post("AVIsource_2_ran1"),"AVIsource_2_ran2" => $this->input->post("AVIsource_2_ran2"),"AVIsource_2_av" => $this->input->post("AVIsource_2_av"),"AVIsource_3_Txt" => $this->input->post("AVIsource_3_Txt"),"AVIsource_3_ran1" => $this->input->post("AVIsource_3_ran1"),"AVIsource_3_ran2" => $this->input->post("AVIsource_3_ran2"),"AVIsource_3_av" => $this->input->post("AVIsource_3_av"),"AVIsource_4_Txt" => $this->input->post("AVIsource_4_Txt"),"AVIsource_4_ran1" => $this->input->post("AVIsource_4_ran1"),"AVIsource_4_ran2" => $this->input->post("AVIsource_4_ran2"),"AVIsource_4_av" => $this->input->post("AVIsource_4_av"),"AVImir" => $this->input->post("AVImir"),"AVImt" => $this->input->post("AVImt"),"AVIlvr" => $this->input->post("AVIlvr"),"AVIedr" => $this->input->post("AVIedr"),"AVIicr" => $this->input->post("AVIicr"),"AVImorCo" => $this->input->post("AVImorCo"),"AVIdcrTit" => $this->input->post("AVIdcrTit"), "AVIcapR_por1" => $this->input->post("AVIcapR_por1"), "AVIcapR_por2" => $this->input->post("AVIcapR_por2"), "AVIcapR_por3" => $this->input->post("AVIcapR_por3"),"AVIcap_rate" => $this->input->post("AVIcap_rate"),"AVIivudica"=> $this->input->post("AVIivudica"),"AVIfar"=> $this->input->post("AVIfar"),"AVImf"=> $this->input->post("AVImf"),"AVIfArea"=> $this->input->post("AVIfArea"),"AVIivudicael"=> $this->input->post("AVIivudicael")  );
                    $this -> md_inmuebles -> insertaSeccionInmueble($camposForm,'apprisal_APVI');
                  
                break;
                case "APVII":
                    $ch1="";
                    $ch2="";
                    $ch3="";
                    $ch4="";
                    if (isset($_POST['AXTit1Chkbx'])) {$ch1="1";} else {$ch1="0";}
                    if (isset($_POST['AXTit2Chkbx'])) {$ch2="1";} else {$ch2="0";}
                    if (isset($_POST['AXTit3Chkbx'])) {$ch3="1";} else {$ch3="0";}
                    if (isset($_POST['AXTit4Chkbx'])) {$ch4="1";} else {$ch4="0";}
                    
                    $this -> md_inmuebles -> borraSeccionInmueble($id_in,'apprisal_APVII');
                    $camposForm = array('id_in' => $id_in,"AVIIText1" => $this->input->post("AVIIText1"),"AVIIText2" => $this->input->post("AVIIText2"),"AVIIuav" => $this->input->post("AVIIuav"),"AVIIivusca" => $this->input->post("AVIIivusca") ,"AVIIIText1" => $this->input->post("AVIIIText1"), "AIXText1" => $this->input->post("AIXText1"),"AXText1" => $this->input->post("AXText1"), "AXText2" => $this->input->post("AXText2"),"AXTit1" => $this->input->post("AXTit1"),"AXTit2" => $this->input->post("AXTit2"),"AXTit3" => $this->input->post("AXTit3"),"AXTit4" => $this->input->post("AXTit4"),"AXTit1Chkbx" => $ch1,"AXTit2Chkbx" => $ch2,"AXTit3Chkbx" => $ch3,"AXTit4Chkbx" => $ch4,"AXnum1" => $this->input->post("AXnum1"),"AXnum2" => $this->input->post("AXnum2"),"AXnum3" => $this->input->post("AXnum3"),"AXnum5" => $this->input->post("AXnum5"),"AXpor1" => $this->input->post("AXpor1"),"AXpor2" => $this->input->post("AXpor2"),"AXpor3" => $this->input->post("AXpor3"),"AXpor4" => $this->input->post("AXpor4"),"AXpor5" => $this->input->post("AXpor5"),"AXmarketVal" => $this->input->post("AXmarketValField"),"AX_C_Approach" => $this->input->post("AX_C_Approach"),"AX_IC_Approach" => $this->input->post("AX_IC_Approach"),"AX_DCF_Approach" => $this->input->post("AX_DCF_Approach"),"AX_SC_Approach" => $this->input->post("AX_SC_Approach"),"AX_VU_Approach" => $this->input->post("AX_VU_Approach")  );
                    $this -> md_inmuebles -> insertaSeccionInmueble($camposForm,'apprisal_APVII');
                break;
                case "APVIII": 
                    $this -> md_inmuebles -> borraSeccionInmueble($id_in,'apprisal_APVIII');
                    $camposForm = array('id_in' => $id_in,"AXIText1" => $this->input->post("AXIText1"),"AXIText2" => $this->input->post("AXIText2"),"AXIText3" => $this->input->post("AXIText3"),"AXIText4" => $this->input->post("AXIText4"),"AXIText5" => $this->input->post("AXIText5"),"AXIText6" => $this->input->post("AXIText6"),"AXIText7" => $this->input->post("AXIText7"),"AXIText8" => $this->input->post("AXIText8"),"AXIText9" => $this->input->post("AXIText9"),"AXIText10" => $this->input->post("AXIText10"),"AXIText11" => $this->input->post("AXIText11"),"AXIText12" => $this->input->post("AXIText12"),"AXIText13" => $this->input->post("AXIText13"),"AXIText14" => $this->input->post("AXIText14"),"AXIText15" => $this->input->post("AXIText15"),"AXIText16" => $this->input->post("AXIText16"),"AXIText17" => $this->input->post("AXIText17"),"AXIRate1" => $this->input->post("AXIRate1"),"AXIRate2" => $this->input->post("AXIRate2"),"AXIRate3" => $this->input->post("AXIRate3"),"AXIRate4" => $this->input->post("AXIRate4"),"AXIRate5" => $this->input->post("AXIRate5"),"AXIRate6" => $this->input->post("AXIRate6"),"AXIRate7" => $this->input->post("AXIRate7"),"AXIRate8" => $this->input->post("AXIRate8"),"AXIRate9" => $this->input->post("AXIRate9"),"AXISaleTerm" => $this->input->post("AXISaleTerm"),"AXIText18" => $this->input->post("AXIText18"), "AXIText19" => $this->input->post("AXIText19"), "AXIText20" => $this->input->post("AXIText20"), "AXIText21" => $this->input->post("AXIText21"), "AXIText22" => $this->input->post("AXIText22"), "AXIFecha" => $this->utils->hermesDateFormat($this->input->post('AXIFecha')), "AXIRateAP" => $this->input->post("AXIRateAP"), "AXIliquidVal" => $this->input->post("AXIliquidVal"), "AXIIText1" => $this->input->post("AXIIText1")  );
                    $this -> md_inmuebles -> insertaSeccionInmueble($camposForm,'apprisal_APVIII');
                break;                
                case "AA1";
                case "AA3";
                case "AA5";//SECCION DEL FORMULARIO ANNEXS 
                    $camposForm = array('id_status'=>$status,'modificado_por'=>element('correo', $this->user['0']),"fecha_modificacion"=>$hoy);
                    $this -> md_inmuebles -> update_inmuebles( $id_in,$camposForm);
                    
                    $this -> md_inmuebles -> borraAnexoInmueble($id_in,$this->input->post('tipoComp'),'annexs_a1_in');
                    
                    $compSeleccionados = preg_split("/[,]+/",$this->input->post('compSeleccionados'));
                    $x = 0;
                    foreach ($compSeleccionados as $c)
                    {
                        $x = $x+1;
                        $camposForm = array('orden'=>$x,'id_in'=>$id_in,'id_comp'=>$c,'tipo_comp'=>$this->input->post('tipoComp'),'rc1'=>$this->input->post('rc1_'.$c),'rc1_adjus'=>$this->input->post('rc1_'.$c.'_adjus'),'rc1_por'=>$this->input->post('rc1_'.$c.'_por'),'rc2'=>$this->input->post('rc2_'.$c),'rc2_adjus'=>$this->input->post('rc2_'.$c.'_adjus'),'rc2_por'=>$this->input->post('rc2_'.$c.'_por'),'rc3'=>$this->input->post('rc3_'.$c),'rc3_adjus'=>$this->input->post('rc3_'.$c.'_adjus'),'rc3_por'=>$this->input->post('rc3_'.$c.'_por'),'rc4'=>$this->input->post('rc4_'.$c),'rc4_adjus'=>$this->input->post('rc4_'.$c.'_adjus'),'rc4_por'=>$this->input->post('rc4_'.$c.'_por'),'rc5'=>$this->input->post('rc5_'.$c),'rc6_1'=>$this->input->post('rc6_1_'.$c),'rc6_2'=>$this->input->post('rc6_2_'.$c),'rc6_3'=>$this->input->post('rc6_3_'.$c),'rc6_4'=>$this->input->post('rc6_4_'.$c),'rc6_5'=>$this->input->post('rc6_5_'.$c),'rc6_6'=>$this->input->post('rc6_6_'.$c),'rc6_7'=>$this->input->post('rc6_7_'.$c),'rc6_8'=>$this->input->post('rc6_8_'.$c),'rc6_9'=>$this->input->post('rc6_9_'.$c),'rc6_10'=>$this->input->post('rc6_10_'.$c),'rc6_9_titulo'=>$this->input->post('rc6_9_'.$c.'_titulo'),'rc6_10_titulo'=>$this->input->post('rc6_10_'.$c.'_titulo'),'rc7'=>$this->input->post('rc7_'.$c),'rc7_adjus'=>$this->input->post('rc7_'.$c.'_adjus'),'rc8'=>$this->input->post('rc8_'.$c),'comments'=>$this->input->post('comments'.$c));
                        $this -> md_inmuebles -> insertaSeccionInmueble($camposForm,'annexs_a1_in');
                    }                    
                break;
                case "AA2": //SECCION DEL FORMULARIO ANNEXS 2
                    $this -> md_inmuebles -> borraAnexoInmueble($id_in,"CLS",'relevant_ch');
                   
                    $this -> md_inmuebles -> insertaSeccionInmueble(array("tipo"=>"rc1", "campo"=>"1) Property rights conveyed", "v_1"=>0, "id_in"=>$id_in,"tipo_comp"=>"CLS"), 'relevant_ch');
                    $this -> md_inmuebles -> insertaSeccionInmueble(array("tipo"=>"rc2", "campo"=>"2) Financing terms", "v_1"=>0, "id_in"=>$id_in,"tipo_comp"=>"CLS"), 'relevant_ch');
                    $this -> md_inmuebles -> insertaSeccionInmueble(array("tipo"=>"rc3", "campo"=>"3) Conditions of sale", "v_1"=>0, "id_in"=>$id_in,"tipo_comp"=>"CLS"), 'relevant_ch');
                    $this -> md_inmuebles -> insertaSeccionInmueble(array("tipo"=>"rc4", "campo"=>"4) Market Conditions", "v_1"=>0, "id_in"=>$id_in,"tipo_comp"=>"CLS"), 'relevant_ch');                    
                    $this -> md_inmuebles -> insertaSeccionInmueble(array("tipo"=>"rc5", "campo"=>$this->input->post('rc5'.$id_in.'campo'), "v_1"=>$this->input->post('rc5'.$id_in.'v_1'), "v_2"=>$this->input->post('rc5'.$id_in.'v_2'), "v_3"=>$this->input->post('rc5'.$id_in.'v_3'), "v_4"=>$this->input->post('rc5'.$id_in.'v_4'), "v_5"=>$this->input->post('rc5'.$id_in.'v_5'), "id_in"=>$id_in,"tipo_comp"=>"CLS"), 'relevant_ch');
                    $this -> md_inmuebles -> insertaSeccionInmueble(array("tipo"=>"rc6_0", "campo"=>$this->input->post('rc6_0'.$id_in.'campo'), "v_1"=>$this->input->post('rc6_0'.$id_in.'v_1'), "id_in"=>$id_in,"tipo_comp"=>"CLS"), 'relevant_ch');
                    $this -> md_inmuebles -> insertaSeccionInmueble(array("tipo"=>"rc6_1", "campo"=>$this->input->post('rc6_1'.$id_in.'campo'), "v_1"=>$this->input->post('rc6_1'.$id_in.'v_1'), "v_2"=>$this->input->post('rc6_1'.$id_in.'v_2'), "v_3"=>$this->input->post('rc6_1'.$id_in.'v_3'), "v_4"=>$this->input->post('rc6_1'.$id_in.'v_4'), "v_5"=>$this->input->post('rc6_1'.$id_in.'v_5')     , "id_in"=>$id_in,"tipo_comp"=>"CLS"), 'relevant_ch');
                    $this -> md_inmuebles -> insertaSeccionInmueble(array("tipo"=>"rc6_2", "campo"=>$this->input->post('rc6_2'.$id_in.'campo'), "v_1"=>$this->input->post('rc6_2'.$id_in.'v_1'), "v_2"=>$this->input->post('rc6_2'.$id_in.'v_2'), "v_3"=>$this->input->post('rc6_2'.$id_in.'v_3'), "v_4"=>$this->input->post('rc6_2'.$id_in.'v_4'), "v_5"=>$this->input->post('rc6_2'.$id_in.'v_5')     , "id_in"=>$id_in,"tipo_comp"=>"CLS"), 'relevant_ch');
                    $this -> md_inmuebles -> insertaSeccionInmueble(array("tipo"=>"rc6_3", "campo"=>$this->input->post('rc6_3'.$id_in.'campo'), "v_1"=>$this->input->post('rc6_3'.$id_in.'v_1'), "v_2"=>$this->input->post('rc6_3'.$id_in.'v_2'), "v_3"=>$this->input->post('rc6_3'.$id_in.'v_3'), "v_4"=>$this->input->post('rc6_3'.$id_in.'v_4'), "v_5"=>$this->input->post('rc6_3'.$id_in.'v_5')     , "id_in"=>$id_in,"tipo_comp"=>"CLS"), 'relevant_ch');
                    $this -> md_inmuebles -> insertaSeccionInmueble(array("tipo"=>"rc6_4", "campo"=>$this->input->post('rc6_4'.$id_in.'campo'), "v_1"=>$this->input->post('rc6_4'.$id_in.'v_1'), "v_2"=>$this->input->post('rc6_4'.$id_in.'v_2'), "v_3"=>$this->input->post('rc6_4'.$id_in.'v_3'), "v_4"=>$this->input->post('rc6_4'.$id_in.'v_4'), "v_5"=>$this->input->post('rc6_4'.$id_in.'v_5')     , "id_in"=>$id_in,"tipo_comp"=>"CLS"), 'relevant_ch');
                    $this -> md_inmuebles -> insertaSeccionInmueble(array("tipo"=>"rc6_5", "campo"=>$this->input->post('rc6_5'.$id_in.'campo'), "v_1"=>$this->input->post('rc6_5'.$id_in.'v_1'), "v_2"=>$this->input->post('rc6_5'.$id_in.'v_2'), "v_3"=>$this->input->post('rc6_5'.$id_in.'v_3'), "v_4"=>$this->input->post('rc6_5'.$id_in.'v_4'), "v_5"=>$this->input->post('rc6_5'.$id_in.'v_5')     , "id_in"=>$id_in,"tipo_comp"=>"CLS"), 'relevant_ch');
                    $this -> md_inmuebles -> insertaSeccionInmueble(array("tipo"=>"rc6_6", "campo"=>$this->input->post('rc6_6'.$id_in.'campo'), "v_1"=>$this->input->post('rc6_6'.$id_in.'v_1'), "v_2"=>$this->input->post('rc6_6'.$id_in.'v_2'), "v_3"=>$this->input->post('rc6_6'.$id_in.'v_3'), "v_4"=>$this->input->post('rc6_6'.$id_in.'v_4'), "v_5"=>$this->input->post('rc6_6'.$id_in.'v_5')     , "id_in"=>$id_in,"tipo_comp"=>"CLS"), 'relevant_ch');
                    $this -> md_inmuebles -> insertaSeccionInmueble(array("tipo"=>"rc6_7", "campo"=>$this->input->post('rc6_7'.$id_in.'campo'), "v_1"=>$this->input->post('rc6_7'.$id_in.'v_1'), "v_2"=>$this->input->post('rc6_7'.$id_in.'v_2'), "v_3"=>$this->input->post('rc6_7'.$id_in.'v_3'), "v_4"=>$this->input->post('rc6_7'.$id_in.'v_4'), "v_5"=>$this->input->post('rc6_7'.$id_in.'v_5')     , "id_in"=>$id_in,"tipo_comp"=>"CLS"), 'relevant_ch');
                    $this -> md_inmuebles -> insertaSeccionInmueble(array("tipo"=>"rc6_8", "campo"=>$this->input->post('rc6_8'.$id_in.'campo'), "v_1"=>$this->input->post('rc6_8'.$id_in.'v_1'), "v_2"=>$this->input->post('rc6_8'.$id_in.'v_2'), "v_3"=>$this->input->post('rc6_8'.$id_in.'v_3'), "v_4"=>$this->input->post('rc6_8'.$id_in.'v_4'), "v_5"=>$this->input->post('rc6_8'.$id_in.'v_5')     , "id_in"=>$id_in,"tipo_comp"=>"CLS"), 'relevant_ch');
                    $this -> md_inmuebles -> insertaSeccionInmueble(array("tipo"=>"rc6_9", "campo"=>$this->input->post('rc6_9'.$id_in.'campo'), "v_1"=>$this->input->post('rc6_9'.$id_in.'v_1'), "v_2"=>$this->input->post('rc6_9'.$id_in.'v_2'), "v_3"=>$this->input->post('rc6_9'.$id_in.'v_3'), "v_4"=>$this->input->post('rc6_9'.$id_in.'v_4'), "v_5"=>$this->input->post('rc6_9'.$id_in.'v_5')     , "id_in"=>$id_in,"tipo_comp"=>"CLS"), 'relevant_ch');
                    $this -> md_inmuebles -> insertaSeccionInmueble(array("tipo"=>"rc6_10","campo"=>$this->input->post('rc6_10'.$id_in.'campo'),"v_1"=>$this->input->post('rc6_10'.$id_in.'v_1'), "v_2"=>$this->input->post('rc6_10'.$id_in.'v_2'), "v_3"=>$this->input->post('rc6_10'.$id_in.'v_3'), "v_4"=>$this->input->post('rc6_10'.$id_in.'v_4'), "v_5"=>$this->input->post('rc6_10'.$id_in.'v_5'), "id_in"=>$id_in,"tipo_comp"=>"CLS"), 'relevant_ch');
                    $this -> md_inmuebles -> insertaSeccionInmueble(array("tipo"=>"rc7"   ,"campo"=>"7) ".$this->input->post('rc7'   .$id_in.'campo'),"v_1"=>$this->input->post('rc7'   .$id_in.'v_1'), "v_2"=>$this->input->post('rc7'.$id_in   .'v_2'), "v_3"=>$this->input->post('rc7'.$id_in.'v_3'), "v_4"=>$this->input->post('rc7'.$id_in.'v_4'), "v_5"=>$this->input->post('rc7'.$id_in.'v_5')         , "id_in"=>$id_in,"tipo_comp"=>"CLS"), 'relevant_ch');
                    $this -> md_inmuebles -> insertaSeccionInmueble(array("tipo"=>"rc8"   ,"campo"=>"8) ".$this->input->post('rc8'   .$id_in.'campo'),"v_1"=>$this->input->post('rc8'   .$id_in.'v_1'), "v_2"=>$this->input->post('rc8'.$id_in   .'v_2')                                                                                                                                                      , "id_in"=>$id_in,"tipo_comp"=>"CLS"), 'relevant_ch');
                break;
                case "AA4": //SECCION DEL FORMULARIO ANNEXS 4                    
                    $this -> md_inmuebles -> borraAnexoInmueble($id_in,"CRL",'relevant_ch');
                   
                    $this -> md_inmuebles -> insertaSeccionInmueble(array("tipo"=>"rc1",   "campo"=>"1) Property rights conveyed", "v_1"=>0, "id_in"=>$id_in,"tipo_comp"=>"CRL"), 'relevant_ch');
                    $this -> md_inmuebles -> insertaSeccionInmueble(array("tipo"=>"rc2",   "campo"=>"2) Financing terms", "v_1"=>0, "id_in"=>$id_in,"tipo_comp"=>"CRL"), 'relevant_ch');
                    $this -> md_inmuebles -> insertaSeccionInmueble(array("tipo"=>"rc3",   "campo"=>"3) Conditions of sale", "v_1"=>0, "id_in"=>$id_in,"tipo_comp"=>"CRL"), 'relevant_ch');
                    $this -> md_inmuebles -> insertaSeccionInmueble(array("tipo"=>"rc4",   "campo"=>"4) Market Conditions", "v_1"=>0, "id_in"=>$id_in,"tipo_comp"=>"CRL"), 'relevant_ch');
                    $this -> md_inmuebles -> insertaSeccionInmueble(array("tipo"=>"rc5",   "campo"=>$this->input->post('rc5'.$id_in.'campo'), "v_1"=>$this->input->post('rc5'.$id_in.'v_1'), "v_2"=>$this->input->post('rc5'.$id_in.'v_2'), "v_3"=>$this->input->post('rc5'.$id_in.'v_3'), "v_4"=>$this->input->post('rc5'.$id_in.'v_4'), "v_5"=>$this->input->post('rc5'.$id_in.'v_5'), "id_in"=>$id_in,"tipo_comp"=>"CRL"), 'relevant_ch');
                    $this -> md_inmuebles -> insertaSeccionInmueble(array("tipo"=>"rc6_0", "campo"=>$this->input->post('rc6_0'.$id_in.'campo'), "v_1"=>$this->input->post('rc6_0'.$id_in.'v_1'), "id_in"=>$id_in,"tipo_comp"=>"CRL"), 'relevant_ch');
                    $this -> md_inmuebles -> insertaSeccionInmueble(array("tipo"=>"rc6_1", "campo"=>$this->input->post('rc6_1'.$id_in.'campo'), "v_1"=>$this->input->post('rc6_1'.$id_in.'v_1'), "v_2"=>$this->input->post('rc6_1'.$id_in.'v_2'), "v_3"=>$this->input->post('rc6_1'.$id_in.'v_3'), "v_4"=>$this->input->post('rc6_1'.$id_in.'v_4'), "v_5"=>$this->input->post('rc6_1'.$id_in.'v_5')     , "id_in"=>$id_in,"tipo_comp"=>"CRL"), 'relevant_ch');
                    $this -> md_inmuebles -> insertaSeccionInmueble(array("tipo"=>"rc6_2", "campo"=>$this->input->post('rc6_2'.$id_in.'campo'), "v_1"=>$this->input->post('rc6_2'.$id_in.'v_1'), "v_2"=>$this->input->post('rc6_2'.$id_in.'v_2'), "v_3"=>$this->input->post('rc6_2'.$id_in.'v_3'), "v_4"=>$this->input->post('rc6_2'.$id_in.'v_4'), "v_5"=>$this->input->post('rc6_2'.$id_in.'v_5')     , "id_in"=>$id_in,"tipo_comp"=>"CRL"), 'relevant_ch');
                    $this -> md_inmuebles -> insertaSeccionInmueble(array("tipo"=>"rc6_3", "campo"=>$this->input->post('rc6_3'.$id_in.'campo'), "v_1"=>$this->input->post('rc6_3'.$id_in.'v_1'), "v_2"=>$this->input->post('rc6_3'.$id_in.'v_2'), "v_3"=>$this->input->post('rc6_3'.$id_in.'v_3'), "v_4"=>$this->input->post('rc6_3'.$id_in.'v_4'), "v_5"=>$this->input->post('rc6_3'.$id_in.'v_5')     , "id_in"=>$id_in,"tipo_comp"=>"CRL"), 'relevant_ch');
                    $this -> md_inmuebles -> insertaSeccionInmueble(array("tipo"=>"rc6_4", "campo"=>$this->input->post('rc6_4'.$id_in.'campo'), "v_1"=>$this->input->post('rc6_4'.$id_in.'v_1'), "v_2"=>$this->input->post('rc6_4'.$id_in.'v_2'), "v_3"=>$this->input->post('rc6_4'.$id_in.'v_3'), "v_4"=>$this->input->post('rc6_4'.$id_in.'v_4'), "v_5"=>$this->input->post('rc6_4'.$id_in.'v_5')     , "id_in"=>$id_in,"tipo_comp"=>"CRL"), 'relevant_ch');
                    $this -> md_inmuebles -> insertaSeccionInmueble(array("tipo"=>"rc6_5", "campo"=>$this->input->post('rc6_5'.$id_in.'campo'), "v_1"=>$this->input->post('rc6_5'.$id_in.'v_1'), "v_2"=>$this->input->post('rc6_5'.$id_in.'v_2'), "v_3"=>$this->input->post('rc6_5'.$id_in.'v_3'), "v_4"=>$this->input->post('rc6_5'.$id_in.'v_4'), "v_5"=>$this->input->post('rc6_5'.$id_in.'v_5')     , "id_in"=>$id_in,"tipo_comp"=>"CRL"), 'relevant_ch');                    
                    $this -> md_inmuebles -> insertaSeccionInmueble(array("tipo"=>"rc6_9", "campo"=>$this->input->post('rc6_9'.$id_in.'campo'), "v_1"=>$this->input->post('rc6_9'.$id_in.'v_1'), "v_2"=>$this->input->post('rc6_9'.$id_in.'v_2'), "v_3"=>$this->input->post('rc6_9'.$id_in.'v_3'), "v_4"=>$this->input->post('rc6_9'.$id_in.'v_4'), "v_5"=>$this->input->post('rc6_9'.$id_in.'v_5')     , "id_in"=>$id_in,"tipo_comp"=>"CRL"), 'relevant_ch');
                    $this -> md_inmuebles -> insertaSeccionInmueble(array("tipo"=>"rc6_10","campo"=>$this->input->post('rc6_10'.$id_in.'campo'),"v_1"=>$this->input->post('rc6_10'.$id_in.'v_1'), "v_2"=>$this->input->post('rc6_10'.$id_in.'v_2'), "v_3"=>$this->input->post('rc6_10'.$id_in.'v_3'), "v_4"=>$this->input->post('rc6_10'.$id_in.'v_4'), "v_5"=>$this->input->post('rc6_10'.$id_in.'v_5'), "id_in"=>$id_in,"tipo_comp"=>"CRL"), 'relevant_ch');
                    $this -> md_inmuebles -> insertaSeccionInmueble(array("tipo"=>"rc7",   "campo"=>$this->input->post('rc7'.$id_in.'campo')   , "v_1"=>$this->input->post('rc7'.$id_in.'v_1')  , "v_2"=>$this->input->post('rc7'.$id_in.'v_2')   , "v_3"=>$this->input->post('rc7'.$id_in.'v_3'), "v_4"=>$this->input->post('rc7'.$id_in.'v_4'), "v_5"=>$this->input->post('rc7'.$id_in.'v_5')         , "id_in"=>$id_in,"tipo_comp"=>"CRL"), 'relevant_ch');
                    $this -> md_inmuebles -> insertaSeccionInmueble(array("tipo"=>"rc8",   "campo"=>$this->input->post('rc8'.$id_in.'campo')   , "v_1"=>$this->input->post('rc8'.$id_in.'v_1')  , "v_2"=>$this->input->post('rc8'.$id_in.'v_2')                                                                                                                                                         , "id_in"=>$id_in,"tipo_comp"=>"CRL"), 'relevant_ch');                                                   
                break;
                case "AA6": //SECCION DEL FORMULARIO ANNEXS 6                    
                    $this -> md_inmuebles -> borraAnexoInmueble($id_in,"CRS",'relevant_ch');
                   
                    $this -> md_inmuebles -> insertaSeccionInmueble(array("tipo"=>"rc1", "campo"=>"1) Property rights conveyed", "v_1"=>0, "id_in"=>$id_in,"tipo_comp"=>"CRS"), 'relevant_ch');
                    $this -> md_inmuebles -> insertaSeccionInmueble(array("tipo"=>"rc2", "campo"=>"2) Financing terms", "v_1"=>0, "id_in"=>$id_in,"tipo_comp"=>"CRS"), 'relevant_ch');
                    $this -> md_inmuebles -> insertaSeccionInmueble(array("tipo"=>"rc3", "campo"=>"3) Conditions of sale", "v_1"=>0, "id_in"=>$id_in,"tipo_comp"=>"CRS"), 'relevant_ch');
                    $this -> md_inmuebles -> insertaSeccionInmueble(array("tipo"=>"rc4", "campo"=>"4) Market Conditions", "v_1"=>0, "id_in"=>$id_in,"tipo_comp"=>"CRS"), 'relevant_ch');                    
                    $this -> md_inmuebles -> insertaSeccionInmueble(array("tipo"=>"rc5", "campo"=>$this->input->post('rc5'.$id_in.'campo'), "v_1"=>$this->input->post('rc5'.$id_in.'v_1'), "v_2"=>$this->input->post('rc5'.$id_in.'v_2'), "v_3"=>$this->input->post('rc5'.$id_in.'v_3'), "v_4"=>$this->input->post('rc5'.$id_in.'v_4'), "v_5"=>$this->input->post('rc5'.$id_in.'v_5'), "id_in"=>$id_in,"tipo_comp"=>"CRS"), 'relevant_ch');
                    $this -> md_inmuebles -> insertaSeccionInmueble(array("tipo"=>"rc6_0", "campo"=>$this->input->post('rc6_0'.$id_in.'campo'), "v_1"=>$this->input->post('rc6_0'.$id_in.'v_1'), "id_in"=>$id_in,"tipo_comp"=>"CRS"), 'relevant_ch');
                    $this -> md_inmuebles -> insertaSeccionInmueble(array("tipo"=>"rc6_1", "campo"=>$this->input->post('rc6_1'.$id_in.'campo'), "v_1"=>$this->input->post('rc6_1'.$id_in.'v_1'), "v_2"=>$this->input->post('rc6_1'.$id_in.'v_2'), "v_3"=>$this->input->post('rc6_1'.$id_in.'v_3'), "v_4"=>$this->input->post('rc6_1'.$id_in.'v_4'), "v_5"=>$this->input->post('rc6_1'.$id_in.'v_5')      , "id_in"=>$id_in,"tipo_comp"=>"CRS"), 'relevant_ch');
                    $this -> md_inmuebles -> insertaSeccionInmueble(array("tipo"=>"rc6_2", "campo"=>$this->input->post('rc6_2'.$id_in.'campo'), "v_1"=>$this->input->post('rc6_2'.$id_in.'v_1'), "v_2"=>$this->input->post('rc6_2'.$id_in.'v_2'), "v_3"=>$this->input->post('rc6_2'.$id_in.'v_3'), "v_4"=>$this->input->post('rc6_2'.$id_in.'v_4'), "v_5"=>$this->input->post('rc6_2'.$id_in.'v_5')      , "id_in"=>$id_in,"tipo_comp"=>"CRS"), 'relevant_ch');
                    $this -> md_inmuebles -> insertaSeccionInmueble(array("tipo"=>"rc6_3", "campo"=>$this->input->post('rc6_3'.$id_in.'campo'), "v_1"=>$this->input->post('rc6_3'.$id_in.'v_1'), "v_2"=>$this->input->post('rc6_3'.$id_in.'v_2'), "v_3"=>$this->input->post('rc6_3'.$id_in.'v_3'), "v_4"=>$this->input->post('rc6_3'.$id_in.'v_4'), "v_5"=>$this->input->post('rc6_3'.$id_in.'v_5')      , "id_in"=>$id_in,"tipo_comp"=>"CRS"), 'relevant_ch');
                    $this -> md_inmuebles -> insertaSeccionInmueble(array("tipo"=>"rc6_4", "campo"=>$this->input->post('rc6_4'.$id_in.'campo'), "v_1"=>$this->input->post('rc6_4'.$id_in.'v_1'), "v_2"=>$this->input->post('rc6_4'.$id_in.'v_2'), "v_3"=>$this->input->post('rc6_4'.$id_in.'v_3'), "v_4"=>$this->input->post('rc6_4'.$id_in.'v_4'), "v_5"=>$this->input->post('rc6_4'.$id_in.'v_5')      , "id_in"=>$id_in,"tipo_comp"=>"CRS"), 'relevant_ch');
                    $this -> md_inmuebles -> insertaSeccionInmueble(array("tipo"=>"rc6_5", "campo"=>$this->input->post('rc6_5'.$id_in.'campo'), "v_1"=>$this->input->post('rc6_5'.$id_in.'v_1'), "v_2"=>$this->input->post('rc6_5'.$id_in.'v_2'), "v_3"=>$this->input->post('rc6_5'.$id_in.'v_3'), "v_4"=>$this->input->post('rc6_5'.$id_in.'v_4'), "v_5"=>$this->input->post('rc6_5'.$id_in.'v_5')      , "id_in"=>$id_in,"tipo_comp"=>"CRS"), 'relevant_ch');                    
                    $this -> md_inmuebles -> insertaSeccionInmueble(array("tipo"=>"rc6_9", "campo"=>$this->input->post('rc6_9'.$id_in.'campo'), "v_1"=>$this->input->post('rc6_9'.$id_in.'v_1'), "v_2"=>$this->input->post('rc6_9'.$id_in.'v_2'), "v_3"=>$this->input->post('rc6_9'.$id_in.'v_3'), "v_4"=>$this->input->post('rc6_9'.$id_in.'v_4'), "v_5"=>$this->input->post('rc6_9'.$id_in.'v_5')      , "id_in"=>$id_in,"tipo_comp"=>"CRS"), 'relevant_ch');
                    $this -> md_inmuebles -> insertaSeccionInmueble(array("tipo"=>"rc6_10", "campo"=>$this->input->post('rc6_10'.$id_in.'campo'),"v_1"=>$this->input->post('rc6_10'.$id_in.'v_1'), "v_2"=>$this->input->post('rc6_10'.$id_in.'v_2'), "v_3"=>$this->input->post('rc6_10'.$id_in.'v_3'), "v_4"=>$this->input->post('rc6_10'.$id_in.'v_4'), "v_5"=>$this->input->post('rc6_10'.$id_in.'v_5'), "id_in"=>$id_in,"tipo_comp"=>"CRS"), 'relevant_ch');
                    $this -> md_inmuebles -> insertaSeccionInmueble(array("tipo"=>"rc7"   , "campo"=>$this->input->post('rc7'.$id_in.'campo')   , "v_1"=>$this->input->post('rc7'.$id_in.'v_1')  , "v_2"=>$this->input->post('rc7'.$id_in.'v_2')   , "v_3"=>$this->input->post('rc7'.$id_in.'v_3'), "v_4"=>$this->input->post('rc7'.$id_in.'v_4'), "v_5"=>$this->input->post('rc7'.$id_in.'v_5')         , "id_in"=>$id_in,"tipo_comp"=>"CRS"), 'relevant_ch');
                    $this -> md_inmuebles -> insertaSeccionInmueble(array("tipo"=>"rc8"   , "campo"=>$this->input->post('rc8'.$id_in.'campo')   , "v_1"=>$this->input->post('rc8'.$id_in.'v_1')  , "v_2"=>$this->input->post('rc8'.$id_in.'v_2')                                                                                                                                                         , "id_in"=>$id_in,"tipo_comp"=>"CRS"), 'relevant_ch');                                                   
                break;            
                case "ACA":
                    $numColumn = $this->input->post('numColumn');
                    $numRow    = $this->input->post('numRow');
                    $numAE     = $this->input->post('numAE');
                    $numCW     = $this->input->post('numCW');
                    
                    //Ground Floor Area
                    $this -> md_inmuebles -> borraSeccionInmueble($id_in,"annexs_ca_totales_in");
                    $this -> md_inmuebles -> insertaSeccionInmueble( array("id_in"=>$id_in,"totCA"=>$this->input->post('txttotCA'),"totGBA"=>$this->input->post('txttotGBA'),"totTRA"=>$this->input->post('txttotTRA') ) ,"annexs_ca_totales_in" );
                    
                    $this -> md_inmuebles -> borraSeccionInmueble($id_in,"annexs_ca_area_in");


                    //Depreciation   Economic Age - Life Method
                    for($c = 1; $c <= $numColumn; $c++)
                    { if(!empty($this->input->post('areaL'.$c)))
                        {$this -> md_inmuebles -> insertaSeccionInmueble(array("column_id"=>$c,"id_in"=>$id_in,"type_area"=>$this->input->post('areaT'.$c),"name_area"=>$this->input->post('areaL'.$c),"gba"=>$this->input->post('gba'.$c),"tra"=>$this->input->post('tra'.$c),"def_rcn"=>$this->input->post('defRCN'.$c),"def_value_rcn"=>$this->input->post('defValueRCN'.$c),"fdh_rcn"=>$this->input->post('fdhRCN'.$c),"fc_rcn"=>$this->input->post('fcRCN'.$c),"uc_rcn"=>$this->input->post('ucRCN'.$c),"por_rcn"=>$this->input->post('porRCN'.$c),"rcn"=>$this->input->post('rcn'.$c),"efage_dep"=>$this->input->post('efageDEP'.$c),"ul_dep"=>$this->input->post('ulDEP'.$c),"af_dep"=>$this->input->post('afDEP'.$c),"dvc_dep"=>$this->input->post('dvc'.$c) ) ,"annexs_ca_area_in" ); }                    
                    }

                    //Construction Area of the Property                    
                    $this -> md_inmuebles -> borraSeccionInmueble($id_in,"annexs_ca_zone_in");
                    for($r = 1; $r <= $numRow; $r++)
                    { if(!empty($this->input->post('zona'.$r)))
                        {$this -> md_inmuebles -> insertaSeccionInmueble(array("ca_row_id"=>$r,"id_in"=>$id_in,"zone"=>$this->input->post('zona'.$r) ) ,"annexs_ca_zone_in" );}                        
                    }
                    
                    //Construction Area of the Property + Area
                    $this -> md_inmuebles -> borraSeccionInmueble($id_in,"annexs_ca_data_in");
                    for($c = 1; $c <= $numColumn; $c++){ 
                        for($r = 1; $r <= $numRow; $r++){ 
                            $dataId = $c.$r;
                            $this -> md_inmuebles -> insertaSeccionInmueble(array("data_id"=>$dataId,"id_in"=>$id_in,"ca"=>$this->input->post('az'.$dataId)) ,"annexs_ca_data_in" ); 
                        }
                    }
                    

                    //COMPLEMENTARY WORKS
                    $this -> md_inmuebles -> borraSeccionInmueble($id_in,"annexs_ca_aecw_in");
                    $orden = 0;
                    for($ae = 1; $ae <= $numAE; $ae++)
                    { if(!empty($this->input->post('AE'.$ae)))
                        { $orden++;
                          $this -> md_inmuebles -> insertaSeccionInmueble(array("id_in"=>$id_in,"orden"=>$orden,"tipo"=>"AE","leyenda"=>$this->input->post('AE'.$ae),"amount"=>$this->input->post('AEAM'.$ae),"unit"=>$this->input->post('AEUN'.$ae),"unit_value"=>$this->input->post('AEUV'.$ae),"rcn"=>$this->input->post('AETO'.$ae),"efage_dep"=>$this->input->post('AEEfAgDEP'.$ae),"ul_dep"=>$this->input->post('AEULDEP'.$ae),"af_dep"=>$this->input->post('AEAFDEP'.$ae),"dvc_dep"=>$this->input->post('AEDVC'.$ae) ) ,"annexs_ca_aecw_in" ); }                        
                    }

                    //COMPLEMENTARY WORKS
                    $orden = 0;
                    for($cw = 1; $cw <= $numCW; $cw++)
                    {  if(!empty($this->input->post('CW'.$cw)))
                        { $orden++;
                          $this -> md_inmuebles -> insertaSeccionInmueble(array("id_in"=>$id_in,"orden"=>$orden,"tipo"=>"CW","leyenda"=>$this->input->post('CW'.$cw),"amount"=>$this->input->post('CWAM'.$cw),"unit"=>$this->input->post('CWUN'.$cw),"unit_value"=>$this->input->post('CWUV'.$cw),"rcn"=>$this->input->post('CWTO'.$cw),"efage_dep"=>$this->input->post('CWEfAgDEP'.$cw),"ul_dep"=>$this->input->post('CWULDEP'.$cw),"af_dep"=>$this->input->post('CWAFDEP'.$cw),"dvc_dep"=>$this->input->post('CWDVC'.$cw) ) ,"annexs_ca_aecw_in" ); }                        
                    }
                    
                    //A.- X BUILD AREA / COST APPROACH
                    $this -> md_inmuebles -> borraSeccionInmueble($id_in,"annexs_landarea_in");
                    $this -> md_inmuebles -> insertaSeccionInmueble(array("id_in"=>$id_in,"taxTxt"=>$this->input->post('taxTxt'),"taxBill"=>$this->input->post('taxBill'),"deedTxt"=>$this->input->post('deedTxt'),"deed"=>$this->input->post('deed'),"plansTxt"=>$this->input->post('plansTxt'),"plans"=>$this->input->post('plans') ) ,"annexs_landarea_in" ); 
                break;
                case "LC":
                    $this -> md_inmuebles -> borraSeccionInmueble($id_in,"AnnexsLC");
                    $this -> md_inmuebles -> insertaSeccionInmueble(array("id_in"=>$id_in,"AnnexsLimTxt"=>$this->input->post('AnnexsLimTxt'),"AnnexsCertTxt"=>$this->input->post('AnnexsCertTxt') ) ,"AnnexsLC" ); 
                break;
            }
            
            echo json_encode(array("id_in"=>$id_in,"tipo"=>$tipo,"def"=>$def));
            
        } catch (Exception $e) {echo ' guardar Excepción: ',  $e, "\n";}
    }

    public function seleccioncompAX()
    {
     try{
         $vs = $this->validaSesion(TRUE,FALSE);        
        if( isset($vs['session']))
            {   echo json_encode ($vs); 
                exit(0);
            }
         $tipoComp   = $this -> input -> post('tipoComp');//"CLS";//
     $selecComp  = $this -> input -> post('selecComp');//array("2634","7053","2987","1623",);//
         $id_in      = $this -> input -> post('id_in');//"70778";//
         $tablaANEXO = "";
         $tablaGRID  = "";
         $compSeleccionados ="";
         
         if($tipoComp==="CLS")
         {
            $t1  = "A.1.- MARKET RESEARCH - LAND SCHEDULES";                    
            $t2  = "LAND FOR SALE ";
            $t3  = "Area of Comparable";
            $t4  = "Price";
            $t5  = "Unit Value";
            $t6  = "Time on market";            
         } elseif ($tipoComp==="CRL") {
            $t1  = "A.3.- MARKET RESEARCH - FOR RENT";
            $t2  = "PROPERTY FOR LEASE ";
            $t3  = "Comparable area";
            $t4  = "Monthly Rent";
            $t5  = "Unitary Rent";
            $t6  = "Lease Date";
          } else { 
            $t1  = "A.5.- MARKET RESEARCH - FOR SALE";
            $t2  = "PROPERTY FOR SALE ";
            $t3  = "Comparable area";
            $t4  = "Total Sale Price";
            $t5  = "Unit Sale Price ";
            $t6  = "Lease Date";
          }
         
         $df          = $this->md_inmuebles->traeDatosFinancierosIn($id_in);
         $ft          = $df['ft'];
         $ftyear      = $df['ftyear'];
         $er          = $df['exchange_rate'];
         $rc1Options  = $this->md_catalogo-> poblarSelect('rc1',false);
         $rc2Options  = $this->md_catalogo-> poblarSelect('rc2',false);
         $rc3Options  = $this->md_catalogo-> poblarSelect('rc3',false);
         $rc4Options  = $this->md_catalogo-> poblarSelect('rc4',false);
         $rcVSOptions = $this->md_catalogo-> poblarSelect('rcVS',false);
         $rc8         = $this->utils->optionsRC8($tipoComp);
                  
         $tablaANEXO .= '<section class="row col col-10 subtitulo"><h1>'.$t1.br(1).'</h1></section>';
         $numComp= sizeof($selecComp);
         $rc6_9_titulo  ="";
         $rc6_10_titulo ="";
         $rc7_adjus     ="";
         
         for ($i=0; $i<$numComp; $i++) 
         {
             $comparable = $this->md_comparables->traeComparable($selecComp[$i]);
             $aa1        = $this->md_comparables->traeRCA1porLlavePrimaria($id_in, $selecComp[$i],$tipoComp);
             
             if($i===0){  
                 $rc6_9_titulo  = $aa1[0]['rc6_9_titulo'];
                 $rc6_10_titulo = $aa1[0]['rc6_10_titulo'];
                 $rc7_adjus     = $aa1[0]['rc7_adjus']; }
                                  
             $compSeleccionados .= ($i==0)?$comparable[0]['id_comp']:",".$comparable[0]['id_comp'];
             $rc1_adjus = ($aa1[0]['rc1']==5)  ?'<br><label class="input"><i class="icon-append fa fa-text-width"></i><input type="text" name="rc1_'.$comparable[0]['id_comp'].'_adjus" id="rc1_'.$comparable[0]['id_comp'].'_adjus" class="validate[required,custom[onlyLetterNumber]] text-input" value="'.$aa1[0]['rc1_adjus'].'" placeholder="Value" maxlength="40"><b class="tooltip tooltip-bottom-right">Value</b></label>':'';
             $rc2_adjus = ($aa1[0]['rc2']==6)  ?'<br><label class="input"><i class="icon-append fa fa-text-width"></i><input type="text" name="rc2_'.$comparable[0]['id_comp'].'_adjus" id="rc2_'.$comparable[0]['id_comp'].'_adjus" class="validate[required,custom[onlyLetterNumber]] text-input" value="'.$aa1[0]['rc2_adjus'].'" placeholder="Value" maxlength="40"><b class="tooltip tooltip-bottom-right">Value</b></label>':'';
             $rc3_adjus = ($aa1[0]['rc3']==9)  ?'<br><label class="input"><i class="icon-append fa fa-text-width"></i><input type="text" name="rc3_'.$comparable[0]['id_comp'].'_adjus" id="rc3_'.$comparable[0]['id_comp'].'_adjus" class="validate[required,custom[onlyLetterNumber]] text-input" value="'.$aa1[0]['rc3_adjus'].'" placeholder="Value" maxlength="40"><b class="tooltip tooltip-bottom-right">Value</b></label>':'';
             $rc4_adjus = ($aa1[0]['rc4']==10) ?'<br><label class="input"><i class="icon-append fa fa-text-width"></i><input type="text" name="rc4_'.$comparable[0]['id_comp'].'_adjus" id="rc4_'.$comparable[0]['id_comp'].'_adjus" class="validate[required,custom[onlyLetterNumber]] text-input" value="'.$aa1[0]['rc4_adjus'].'" placeholder="Value" maxlength="40"><b class="tooltip tooltip-bottom-right">Value</b></label>':'';
             $rc1_por   = "<label class='input'><i class='icon-append fa fa-percent'></i><input type='text' name='rc1_".$comparable[0]['id_comp']."_por' id='rc1_".$comparable[0]['id_comp']."_por' class='validate[required,custom[number]] text-input' value='".$aa1[0]['rc1_por']."' placeholder='Percentage Value %' maxlength='12'><b class='tooltip tooltip-bottom-right'>Percentage Value %</b></label>";
             $rc2_por   = "<label class='input'><i class='icon-append fa fa-percent'></i><input type='text' name='rc2_".$comparable[0]['id_comp']."_por' id='rc2_".$comparable[0]['id_comp']."_por' class='validate[required,custom[number]] text-input' value='".$aa1[0]['rc2_por']."' placeholder='Percentage Value %' maxlength='12'><b class='tooltip tooltip-bottom-right'>Percentage Value %</b></label>";
             $rc3_por   = "<label class='input'><i class='icon-append fa fa-percent'></i><input type='text' name='rc3_".$comparable[0]['id_comp']."_por' id='rc3_".$comparable[0]['id_comp']."_por' class='validate[required,custom[number]] text-input' value='".$aa1[0]['rc3_por']."' placeholder='Percentage Value %' maxlength='12'><b class='tooltip tooltip-bottom-right'>Percentage Value %</b></label>";
             $rc4_por   = "<label class='input'><i class='icon-append fa fa-percent'></i><input type='text' name='rc4_".$comparable[0]['id_comp']."_por' id='rc4_".$comparable[0]['id_comp']."_por' class='validate[required,custom[number]] text-input' value='".$aa1[0]['rc4_por']."' placeholder='Percentage Value %' maxlength='12'><b class='tooltip tooltip-bottom-right'>Percentage Value %</b></label>";             
             
             $lada = empty($comparable[0]['lada'])?"":'('.$comparable[0]['lada'].') ';
             
             $tablaANEXO .= '<section class="row col col-10 subtitulo">'.br(1).$t2.($i+1).nbs(7).'<a href="'.base_url().'comparables/forma/'.$tipoComp.'/E/'.$comparable[0]['id_comp'].'/"><img title="Update Exchange Rate" id="updateER" width="15" height="15" src="'.base_url().'images/edit.png"></a>'.br(1).'</section>';
             $tablaANEXO .= '<section class="row col col-10"><center>'.br(1).img( array('src'=>base_url().$this->galeriaComp.$comparable[0]['foto'])).br(2).'</center></section>';
             $tablaANEXO .= '<section class="row col col-5 subtitulo">Type of Property</section>';
             $tablaANEXO .= '<section class="col col-5 subtitulo">Location</section>';
             $tablaANEXO .= '<section class="row col col-5"><center>'.$comparable[0]['type_property'].'</center></section>';
             $tablaANEXO .= '<section class="col col-5"><center>'.$this->helpercontroller->creaLocation($comparable[0]['calle'],$comparable[0]['num'],$comparable[0]['col'],$comparable[0]['mun'],$comparable[0]['edo'],$comparable[0]['cp']).'</center></section>';
             $tablaANEXO .= '<section class="row col col-5 subtitulo">Source Information</section>';
             $tablaANEXO .= '<section class="col col-5 subtitulo">Telephone</section>';
             $tablaANEXO .= '<section class="row col col-5"><center>'.$comparable[0]['source_information'].'</center></section>';
             $tablaANEXO .= '<section class="col col-5"><center>'.$lada.$comparable[0]['phone'].'</center></section>';
             $tablaANEXO .= '<section class="row col col-3 subtitulo">'.$t3.'</section>';
             $tablaANEXO .= '<section class="col col-3 subtitulo">'.$t4.'</section>';
             $tablaANEXO .= '<section class="col col-4 subtitulo">'.$t5.'</section>';
             $tablaANEXO .= '<section class="row col col-3"><center>'.number_format($comparable[0]['land_m2'], 2, '.', ',').' m2<br>'.number_format($comparable[0]['land_ft2'], 2, '.', ',').' ft2</center></section>';
             $tablaANEXO .= '<section class="col col-3"><center>$'.number_format($comparable[0]['price_mx'], 2, '.', ',').' mxn<br>$'.number_format($comparable[0]['price_mx']/$er, 2, '.', ',').' usd</center></section>';
             $unitValueUSD = $this->helpercontroller->creaUnidades($tipoComp,$comparable[0]['unit_value_mx'],$er,$ft,$ftyear);
             $tablaANEXO .= '<section class="col col-4"><center>'.$unitValueUSD.'</center></section>';
             $tablaANEXO .= '<section class="row col col-5 subtitulo">Construction</section>';
             $tablaANEXO .= '<section class="col col-5 subtitulo">'.$t6.'</section>';
             $tablaANEXO .= '<section class="row col col-5"><center>'.number_format($comparable[0]['construction'], 2, '.', ',').' m2<br>'.number_format(sprintf("%.3f", $comparable[0]['construction']*$ft), 2, '.', ',').' ft2</center></section>';
             $tablaANEXO .= '<section class="col col-5"><center>'.$comparable[0]['time_market'].'</center></section>';
             
             $rc1Dropdown='<label class="select"><i class="icon-append"></i>'.form_dropdown('rc1_'.$comparable[0]['id_comp'], $rc1Options, $aa1[0]['rc1'],'id="rc1_'.$comparable[0]['id_comp'].'" class="validate[custom[requiredInFunction]]" onChange="rcExtraField(this);" ').'</label>';
             $rc2Dropdown='<label class="select"><i class="icon-append"></i>'.form_dropdown('rc2_'.$comparable[0]['id_comp'], $rc2Options, $aa1[0]['rc2'],'id="rc2_'.$comparable[0]['id_comp'].'" class="validate[custom[requiredInFunction]]" onChange="rcExtraField(this);" ').'</label>';
             $rc3Dropdown='<label class="select"><i class="icon-append"></i>'.form_dropdown('rc3_'.$comparable[0]['id_comp'], $rc3Options, $aa1[0]['rc3'],'id="rc3_'.$comparable[0]['id_comp'].'" class="validate[custom[requiredInFunction]]" onChange="rcExtraField(this);" ').'</label>';
             $rc4Dropdown='<label class="select"><i class="icon-append"></i>'.form_dropdown('rc4_'.$comparable[0]['id_comp'], $rc4Options, $aa1[0]['rc4'],'id="rc4_'.$comparable[0]['id_comp'].'" class="validate[custom[requiredInFunction]]" onChange="rcExtraField(this);" ').'</label>';
             $rc5Dropdown='<label class="select"><i class="icon-append"></i>'.form_dropdown('rc5_'.$comparable[0]['id_comp'], $rcVSOptions, $aa1[0]['rc5'],'id="rc5_'.$comparable[0]['id_comp'].'" class="validate[custom[requiredInFunction]]" onChange="rcExtraField(this);"').'</label>';
             $rc7Dropdown='<label class="select"><i class="icon-append"></i>'.form_dropdown('rc7_'.$comparable[0]['id_comp'], $rcVSOptions, $aa1[0]['rc7'],'id="rc7_'.$comparable[0]['id_comp'].'" class="validate[custom[requiredInFunction]]"').'</label>';
             $rc8Dropdown='<label class="select"><i class="icon-append"></i>'.form_dropdown('rc8_'.$comparable[0]['id_comp'].'disable', $rc8,  $comparable[0]['rc8'],'id="rc8_'.$comparable[0]['id_comp'].'disable'.'" class="validate[custom[requiredInFunction]] rc8"').'</label>'.form_input(array('name'=>'rc8_'.$comparable[0]['id_comp'], 'type'=>'hidden','id'=>'rc8_'.$comparable[0]['id_comp'],'value'=>$comparable[0]['rc8']));
             
             $size   = ($tipoComp==="CLS")?number_format($comparable[0]['land_m2'], 2, '.', ','):number_format($comparable[0]['construction'], 2, '.', ',');
             $sizeFt = ($tipoComp==="CLS")?number_format($comparable[0]['land_ft2'], 2, '.', ','):number_format(($comparable[0]['construction']*$ft), 2, '.', ',');
             $tablaANEXO .= '<section class="row col col-10"><br></section>
                                <section class="row col col-10 subtitulo">RELEVANT CHARACTERISTICS OF THE COMPARABLE</section>
                                <section class="row col col-5">1) Property rights conveyed </section><section class="col col-4">'.$rc1Dropdown.$rc1_adjus.'</section><section class="col col-2">'.$rc1_por.'</section>
                                <section class="row col col-5">2) Financing terms          </section><section class="col col-4">'.$rc2Dropdown.$rc2_adjus.'</section><section class="col col-2">'.$rc2_por.'</section>
                                <section class="row col col-5">3) Conditions of sale       </section><section class="col col-4">'.$rc3Dropdown.$rc3_adjus.'</section><section class="col col-2">'.$rc3_por.'</section>
                                <section class="row col col-5">4) Market Conditions        </section><section class="col col-4">'.$rc4Dropdown.$rc4_adjus.'</section><section class="col col-2">'.$rc4_por.'</section>
                                <section class="row col col-7">5) Location                 </section><section class="col col-4">'.$rc5Dropdown.'</section>
                                <section class="row col col-7">6) Physical characteristics          </section><section class="col col-4"> </section>
                                <section class="row col col-2">'.nbs(1).'</section><section class="col col-5">Size (m2) (ft2)  </section><section class="col col-2">'.$size.' m2</section><section class="col col-2">'.$sizeFt.' ft2</section>';
             $campoExtra1 = '<label class="input"><i class="icon-append fa fa-text-width"></i><input type="text" name="rc6_9_'.$comparable[0]['id_comp'].'_titulo"  id="rc6_9_'.$comparable[0]['id_comp'].'_titulo"  class="validate[custom[onlyLetterNumber]] text-input" value="'.$aa1[0]['rc6_9_titulo'].'"  placeholder="Other 1" select="rc6_9_'.$comparable[0]['id_comp'].'"  maxlength="40" onChange="validateSelect(this,\'rc6_9\');" ></label>' ;
             $campoExtra2 = '<label class="input"><i class="icon-append fa fa-text-width"></i><input type="text" name="rc6_10_'.$comparable[0]['id_comp'].'_titulo" id="rc6_10_'.$comparable[0]['id_comp'].'_titulo" class="validate[custom[onlyLetterNumber]] text-input" value="'.$aa1[0]['rc6_10_titulo'].'" placeholder="Other 2" select="rc6_10_'.$comparable[0]['id_comp'].'" maxlength="40" onChange="validateSelect(this,\'rc6_10\');"></label>' ;
             $rc7_adCampo = '<label class="input"><i class="icon-append fa fa-text-width"></i><input type="text" name="rc7_'.$comparable[0]['id_comp'].'_adjus" id="rc7_'.$comparable[0]['id_comp'].'_adjus"  class="validate[custom[onlyLetterNumber]] text-input" value="'.$aa1[0]['rc7_adjus'].'" placeholder="Type Value" comp="rc7_'.$comparable[0]['id_comp'].'" maxlength="40"></label>' ;
             
            if($tipoComp!=="CLS")
            {                     
                $rc6_1Dropdown='<label class="select"><i class="icon-append"></i>'.form_dropdown('rc6_1_'.$comparable[0]['id_comp'], $rcVSOptions, $aa1[0]['rc6_1'],'id="rc6_1_'.$comparable[0]['id_comp'].'" class="validate[custom[requiredInFunction]]"').'</label>';
                $rc6_2Dropdown='<label class="select"><i class="icon-append"></i>'.form_dropdown('rc6_2_'.$comparable[0]['id_comp'], $rcVSOptions, $aa1[0]['rc6_2'],'id="rc6_2_'.$comparable[0]['id_comp'].'" class="validate[custom[requiredInFunction]]"').'</label>';
                $rc6_3Dropdown='<label class="select"><i class="icon-append"></i>'.form_dropdown('rc6_3_'.$comparable[0]['id_comp'], $rcVSOptions, $aa1[0]['rc6_3'],'id="rc6_3_'.$comparable[0]['id_comp'].'" class="validate[custom[requiredInFunction]]"').'</label>';
                $rc6_4Dropdown='<label class="select"><i class="icon-append"></i>'.form_dropdown('rc6_4_'.$comparable[0]['id_comp'], $rcVSOptions, $aa1[0]['rc6_4'],'id="rc6_4_'.$comparable[0]['id_comp'].'" class="validate[custom[requiredInFunction]]"').'</label>';
                $rc6_5Dropdown='<label class="select"><i class="icon-append"></i>'.form_dropdown('rc6_5_'.$comparable[0]['id_comp'], $rcVSOptions, $aa1[0]['rc6_5'],'id="rc6_5_'.$comparable[0]['id_comp'].'" class="validate[custom[requiredInFunction]]"').'</label>';                
                $rc6_9Dropdown='<label class="select"><i class="icon-append"></i>'.form_dropdown('rc6_9_'.$comparable[0]['id_comp'], $rcVSOptions, $aa1[0]['rc6_9'],'id="rc6_9_'.$comparable[0]['id_comp'].'"'.(empty($aa1[0]['rc6_9_titulo'])?'class="rc6_9Select"':'class="validate[custom[requiredInFunction]] rc6_9Select"')).'</label>';
                $rc6_10Dropdown='<label class="select"><i class="icon-append"></i>'.form_dropdown('rc6_10_'.$comparable[0]['id_comp'], $rcVSOptions, $aa1[0]['rc6_10'],'id="rc6_10_'.$comparable[0]['id_comp'].'"'.(empty($aa1[0]['rc6_10_titulo'])?'class="rc6_10Select"':'class="validate[custom[requiredInFunction]] rc6_10Select"')).'</label>';                

                
                $tablaANEXO .= '<section class="row col col-2">'.nbs(1).'</section><section class="col col-5">Condition            </section><section class="col col-4">'.$rc6_1Dropdown.'</section>
                                <section class="row col col-2">'.nbs(1).'</section><section class="col col-5">Construction Quality </section><section class="col col-4">'.$rc6_2Dropdown.'</section>
                                <section class="row col col-2">'.nbs(1).'</section><section class="col col-5">Functional Utility   </section><section class="col col-4">'.$rc6_3Dropdown.'</section>
                                <section class="row col col-2">'.nbs(1).'</section><section class="col col-5">Obsolescence         </section><section class="col col-4">'.$rc6_4Dropdown.'</section>
                                <section class="row col col-2">'.nbs(1).'</section><section class="col col-5">Age and Maintenance  </section><section class="col col-4">'.$rc6_5Dropdown.'</section>                                
                                <section class="row col col-2">'.nbs(1).'</section><section class="col col-5">'.$campoExtra1.'</section><section class="col col-4">'.$rc6_9Dropdown.'</section>
                                <section class="row col col-2">'.nbs(1).'</section><section class="col col-5">'.$campoExtra2.'</section><section class="col col-4">'.$rc6_10Dropdown.'</section>    
                                <section class="row col col-3">7) Use (zoning)</section><section class="col col-4">'.$rc7_adCampo.'</section><section class="col col-4">'.$rc7Dropdown.'</section>
                                <section class="row col col-7">8) Listing / Real Rent   </section><section class="col col-4">'.$rc8Dropdown.'</section>'                                
                               ;
            } else {
                $rc6_1Dropdown='<label class="select"><i class="icon-append"></i>'.form_dropdown('rc6_1_'.$comparable[0]['id_comp'], $rcVSOptions, $aa1[0]['rc6_1'],'id="rc6_1_'.$comparable[0]['id_comp'].'" class="validate[custom[requiredInFunction]]"').'</label>';
                $rc6_2Dropdown='<label class="select"><i class="icon-append"></i>'.form_dropdown('rc6_2_'.$comparable[0]['id_comp'], $rcVSOptions, $aa1[0]['rc6_2'],'id="rc6_2_'.$comparable[0]['id_comp'].'" class="validate[custom[requiredInFunction]]"').'</label>';
                $rc6_3Dropdown='<label class="select"><i class="icon-append"></i>'.form_dropdown('rc6_3_'.$comparable[0]['id_comp'], $rcVSOptions, $aa1[0]['rc6_3'],'id="rc6_3_'.$comparable[0]['id_comp'].'" class="validate[custom[requiredInFunction]]"').'</label>';
                $rc6_4Dropdown='<label class="select"><i class="icon-append"></i>'.form_dropdown('rc6_4_'.$comparable[0]['id_comp'], $rcVSOptions, $aa1[0]['rc6_4'],'id="rc6_4_'.$comparable[0]['id_comp'].'" class="validate[custom[requiredInFunction]]"').'</label>';
                $rc6_5Dropdown='<label class="select"><i class="icon-append"></i>'.form_dropdown('rc6_5_'.$comparable[0]['id_comp'], $rcVSOptions, $aa1[0]['rc6_5'],'id="rc6_5_'.$comparable[0]['id_comp'].'" class="validate[custom[requiredInFunction]]"').'</label>';
                $rc6_6Dropdown='<label class="select"><i class="icon-append"></i>'.form_dropdown('rc6_6_'.$comparable[0]['id_comp'], $rcVSOptions, $aa1[0]['rc6_6'],'id="rc6_6_'.$comparable[0]['id_comp'].'" class="validate[custom[requiredInFunction]]"').'</label>';
                $rc6_7Dropdown='<label class="select"><i class="icon-append"></i>'.form_dropdown('rc6_7_'.$comparable[0]['id_comp'], $rcVSOptions, $aa1[0]['rc6_7'],'id="rc6_7_'.$comparable[0]['id_comp'].'" class="validate[custom[requiredInFunction]]"').'</label>';
                $rc6_8Dropdown='<label class="select"><i class="icon-append"></i>'.form_dropdown('rc6_8_'.$comparable[0]['id_comp'], $rcVSOptions, $aa1[0]['rc6_8'],'id="rc6_8_'.$comparable[0]['id_comp'].'" class="validate[custom[requiredInFunction]]"').'</label>';
                $rc6_9Dropdown='<label class="select"><i class="icon-append"></i>'.form_dropdown('rc6_9_'.$comparable[0]['id_comp'], $rcVSOptions, $aa1[0]['rc6_9'],'id="rc6_9_'.$comparable[0]['id_comp'].'"'.(empty($aa1[0]['rc6_9_titulo'])?'class="rc6_9Select"':'class="validate[custom[requiredInFunction]] rc6_9Select"')).'</label>';
                $rc6_10Dropdown='<label class="select"><i class="icon-append"></i>'.form_dropdown('rc6_10_'.$comparable[0]['id_comp'], $rcVSOptions, $aa1[0]['rc6_10'],'id="rc6_10_'.$comparable[0]['id_comp'].'"'.(empty($aa1[0]['rc6_10_titulo'])?'class="rc6_10Select"':'class="validate[custom[requiredInFunction]] rc6_10Select"')).'</label>';                

                
                $tablaANEXO .= '<section class="row col col-2">'.nbs(1).'</section><section class="col col-5">Frontage         </section><section class="col col-4">'.$rc6_1Dropdown.'</section>
                                <section class="row col col-2">'.nbs(1).'</section><section class="col col-5">Access           </section><section class="col col-4">'.$rc6_2Dropdown.'</section>
                                <section class="row col col-2">'.nbs(1).'</section><section class="col col-5">Shape            </section><section class="col col-4">'.$rc6_3Dropdown.'</section>
                                <section class="row col col-2">'.nbs(1).'</section><section class="col col-5">Utilities        </section><section class="col col-4">'.$rc6_4Dropdown.'</section>
                                <section class="row col col-2">'.nbs(1).'</section><section class="col col-5">Topography       </section><section class="col col-4">'.$rc6_5Dropdown.'</section>
                                <section class="row col col-2">'.nbs(1).'</section><section class="col col-5">Depth radio      </section><section class="col col-4">'.$rc6_6Dropdown.'</section>                             
                                <section class="row col col-2">'.nbs(1).'</section><section class="col col-5">Flood hazard     </section><section class="col col-4">'.$rc6_7Dropdown.'</section>
                                <section class="row col col-2">'.nbs(1).'</section><section class="col col-5">Easements        </section><section class="col col-4">'.$rc6_8Dropdown.'</section>
                                <section class="row col col-2">'.nbs(1).'</section><section class="col col-5">'.$campoExtra1.'</section><section class="col col-4">'.$rc6_9Dropdown.'</section>
                                <section class="row col col-2">'.nbs(1).'</section><section class="col col-5">'.$campoExtra2.'</section><section class="col col-4">'.$rc6_10Dropdown.'</section>
                                <section class="row col col-3">7) Use (zoning)</section><section class="col col-4">'.$rc7_adCampo.'</section><section class="col col-4">'.$rc7Dropdown.'</section>
                                <section class="row col col-7">8) Listing / Sale   </section><section class="col col-4">'.$rc8Dropdown.'</section>'
                               ;                
            } 
            
            $tablaANEXO .= '<section class="row col col-5">Additional comments </section><section class="col col-6"><label class="label">'.$comparable[0]['comments'].form_input(array('type' => 'hidden','name'=>'comments'.$comparable[0]['id_comp'],'id'=>'comments'.$comparable[0]['id_comp'], 'value' => $comparable[0]['comments'])).'</label></section>';
         }
         
         $tablaANEXO .= form_input(array('name'  => 'compSeleccionados', 
                                         'type'  => 'hidden', 
                                         'id'    => 'compSeleccionados',
                                         'value' => $compSeleccionados)).
                        form_input(array('name'  => 'tipoComp', 
                                         'type'  => 'hidden', 
                                         'id'    => 'tipoComp',
                                         'value' => $tipoComp))
                        ;
         
         echo json_encode (array("tablaANEXO" => $tablaANEXO,"tablaGRID" => $tablaGRID,"tipoComp"=>$tipoComp));
         
        }  catch (Exception $e) {echo 'seleccioncompAX Excepción: ',  $e, "\n";}                        
    }            

/*****************************
 * Galeria INI
 *****************************/
public function agregaImagenAA8AX()
{
 try{
     $vs = $this->validaSesion(TRUE,FALSE);        
        if( isset($vs['session']))
        {   echo json_encode ($vs); 
            exit(0);
        }
    if(isset($_FILES["myfile"]))
    {
        $ret = array();     
        $error =$_FILES["myfile"]["error"];     
        if(!is_array($_FILES["myfile"]["name"])) //single file
        {
                $fileName =  $this->utils->generaNombreImagen($_FILES["myfile"]["name"]);
                move_uploaded_file($_FILES["myfile"]["tmp_name"],$this->galeria.$fileName);
                $ret[]= $fileName;
        }
        else  //Multiple files, file[]
        {
          $fileCount = count($_FILES["myfile"]["name"]);
          for($i=0; $i < $fileCount; $i++)
          {                
                $fileName =  $this->utils->generaNombreImagen($_FILES["myfile"]["name"][$i]);
                move_uploaded_file($_FILES["myfile"]["tmp_name"][$i],$this->galeria.$fileName);
                $ret[]= $fileName;
          }
        }
        echo json_encode($ret);                
    }
  } catch (Exception $e) {echo ' agregaImagenAA8AX Excepción: ',  $e, "\n";}         
}


public function borraImagenAA8AX()
{
    
    if(isset($_POST["op"]) && $_POST["op"] == "delete" && isset($_POST['name']))
    {
        $fileName = $_POST['name'];
        $filePath = $this->galeria. $fileName;
        if (file_exists($filePath))         
            { unlink($filePath); }
    }
}

public function renombraImagenAA8AX()
{
try{
    $vs = $this->validaSesion(TRUE,FALSE);        
    if( isset($vs['session']))
    {   echo json_encode ($vs); 
        exit(0);
    }
        
    $this->load->helper('date');
    
    $errores       = array();
    $errorTxt      = "";        
    $extension     = $this -> input -> post('extension');
    $nombreArchivo = $this -> input -> post('nombreArchivo');
    $tipoGaleria   = $this -> input -> post('tipoGaleria');
    $extraAA       = ($tipoGaleria==="Photos")?$this -> input -> post('extraAA'):strtoupper("".$this -> input -> post('extraAA')) ;
    $phototitulo   = $this -> input -> post('phototitulo');
    $id_in         = intval($this -> input -> post('id_in'));
    $idImagen      = "he_gl_".intval(substr(now(), -5));
    $nombreArchivo = $this->galeria.$nombreArchivo.".".$extension;
    $nombreHermes  = $this->galeria.$id_in.'/'.$idImagen.".".$extension;
    $resize        = ($tipoGaleria==="Photos")?"466x338! -bordercolor black -border 1x1":( ($phototitulo == "29") || ($phototitulo == "30") )?"850x460! -bordercolor black -border 1x1":"560x760!";
    $id_photo      = 0;
    $tituloAnexo   = $this->md_gallery->traeNombreAnexo($tipoGaleria,$phototitulo);

    if (file_exists($this->galeria.$id_in) == FALSE)
        { mkdir($this->galeria.$id_in, 0777); }

    $cmd = "$nombreArchivo -resize $resize ";  //$cmd = "$nombreArchivo -resize 250x200! -bordercolor black -border 1x1 \( $logoJLL \)  -gravity center -composite "; 
    exec("convert $cmd $nombreHermes ",$errores);
    unlink($nombreArchivo);

    if ( empty($errores) )
      { $id_photo=$this->md_gallery->insert_gallery($id_in,$idImagen,$nombreHermes,$tipoGaleria,$phototitulo,$extraAA);
        if($phototitulo==="34")
        { $this->md_gallery->updateTituloExtraAA($id_in,$phototitulo,$extraAA); }
      }
    else
      { $errorTxt = "<br/> Hubo errores al trabajar conconversion: <br/>".print_r($errores); }       

    echo json_encode (array("nombreHermes" => $nombreHermes,"idPhoto" => $id_photo,"phototitulo"=>$phototitulo,"id_in" => $id_in,"tipoGaleria"=>$tipoGaleria,"tituloAnexo"=>$tituloAnexo,"erroresd" => $errorTxt));

    } catch (Exception $e) {echo 'renombraImagenAX Excepción: ',  $e, "\n";}    
}

public function borraImagenCargadaAA8AX()
{   
try{
    $vs = $this->validaSesion(TRUE,FALSE);        
    if( isset($vs['session']))
    {   echo json_encode ($vs); 
        exit(0);
    }
    $id_photo = $this -> input      -> post('id_photo');    
    $photo    = $this -> md_gallery -> traeFotoGalleryId($id_photo);    
    $result   = false;
    
    $this -> load       -> model('md_gallery');
    $this -> md_gallery -> borraFoto($photo['nombre']);
    
    if (file_exists($photo['nombre']))      
        { $result = unlink($photo['nombre']);}

    echo json_encode (array("result" => $result,"filePath"=>$photo['nombre']));

    } catch (Exception $e) {echo 'renombraAdjuntoAX Excepción: ',  $e, "\n";}   
}
/***************************
 * Galeria FIN
 ***************************/    

/***************************
 * Foto Portada INI
 **************************/
public function agregaImagenFotoPortadaAX()
{
 try{
     $vs = $this->validaSesion(TRUE,FALSE);        
    if( isset($vs['session']))
    {   echo json_encode ($vs); 
        exit(0);
    }
    if(isset($_FILES["myfile"]))
    {
        $ret = array();     
        $error =$_FILES["myfile"]["error"];     
        if(!is_array($_FILES["myfile"]["name"])) //single file
        {
            $fileName =  $this->utils->generaNombreImagen($_FILES["myfile"]["name"]);
            move_uploaded_file($_FILES["myfile"]["tmp_name"],$this->galeria.$fileName);
            $ret[]= $fileName;
        }
        else  //Multiple files, file[]
        {
            $fileCount = count($_FILES["myfile"]["name"]);
            for($i=0; $i < $fileCount; $i++)
            {                  
                  $fileName = $this->utils->generaNombreImagen($_FILES["myfile"]["name"][$i]);
                  move_uploaded_file($_FILES["myfile"]["tmp_name"][$i],$this->galeria.$fileName);
                  $ret[]= $fileName;
            }
        }
        echo json_encode($ret);
     }
  } catch (Exception $e) {echo ' agregaImagenFotoPortadaAX Excepción: ',  $e, "\n";}         
}

public function renombraImagenFotoPortadaAX()
{   
try{
    $vs = $this->validaSesion(TRUE,FALSE);        
    if( isset($vs['session']))
    {   echo json_encode ($vs); 
        exit(0);
    }
    $this->load->helper('date');

    $gridJLL       = "images/rejillaPortada.png";
    $errores       = array();
    $errorTxt      = "";        
    $extension     = $this -> input -> post('extension');
    $nombreArchivo = $this -> input -> post('nombreArchivo');
    $id_in         = intval($this -> input -> post('id_in'));
    $idImagen      = "he_fp_".intval(substr(now(), -5));
    $nombreArchivo = $this->galeria.$nombreArchivo.".".$extension;
    $nombreHermes  = $this->galeria.$id_in.'/'.$idImagen.".".$extension;

    if (file_exists($this->galeria.$id_in) == FALSE)
        {mkdir($this->galeria.$id_in, 0777);}                       

    $cmd = "$nombreArchivo -resize 612x618! -bordercolor black -border 1x1 \( $gridJLL \)  -gravity center -composite "; 
    exec("convert $cmd $nombreHermes ",$errores);
    unlink($nombreArchivo);

    if ( empty($errores) )
       { $camposForm = array("fotoPortada"=>$idImagen );
         $this -> md_inmuebles -> update_in("inmuebles", $id_in,$camposForm); }
     else
        { $errorTxt = "<br />Hubo errores al trabajar conconversion:<br />".print_r($errores); }       

    echo json_encode (array("nombreHermes" => $nombreHermes,"hImg" => $idImagen.".".$extension,"id_in" => $id_in,"errores" => $errorTxt));

    } catch (Exception $e) {echo 'renombraImagenFotoPortadaAX Excepción: ',  $e, "\n";} 
}

public function borraImagenCargadaFotoPortadaAX()
{   
try{
    $vs = $this->validaSesion(TRUE,FALSE);        
    if( isset($vs['session']))
    {   echo json_encode ($vs); 
        exit(0);
    }
    $extension     = $this -> input -> post('extension');
    $nombreArchivo = $this -> input -> post('nombreArchivo');
    $id_in         = $this -> input -> post('id_in');
    $nombreArchivo = $nombreArchivo.".".$extension;
    $filePath      = $this->galeria.$id_in.'/'.$nombreArchivo;
    $result        = false;

    if (file_exists($filePath))         
        { $result = unlink($filePath); }

    $camposForm = array("fotoPortada"=>"logoJLL.jpg" );
    $this -> md_inmuebles -> update_in("inmuebles", $id_in,$camposForm); 
    
    echo json_encode (array("result" => $result,"dirFoto" => $this->galeria));

    } catch (Exception $e) {echo 'borraImagenCargadaFotoPortadaAX Excepción: ',  $e, "\n";} 
}
/************************
 * Foto Portada FIN
 ************************/

/***************************
 * Foto API INI
 **************************/
public function agregaImagenAPIAX()
{
 try{
     $vs = $this->validaSesion(TRUE,FALSE);        
    if( isset($vs['session']))
    {   echo json_encode ($vs); 
        exit(0);
    }
    if(isset($_FILES["myfile"]))
    {
        $ret = array();     
        $error =$_FILES["myfile"]["error"];     
        if(!is_array($_FILES["myfile"]["name"])) //single file
        {            
            $fileName =  $this->utils->generaNombreImagen($_FILES["myfile"]["name"]);
            move_uploaded_file($_FILES["myfile"]["tmp_name"],$this->galeria.$fileName);
            $ret[]= $fileName;
        }
        else  //Multiple files, file[]
        {
            $fileCount = count($_FILES["myfile"]["name"]);
            for($i=0; $i < $fileCount; $i++)
            {                  
                  $fileName =  $this->utils->generaNombreImagen($_FILES["myfile"]["name"][$i]);
                  move_uploaded_file($_FILES["myfile"]["tmp_name"][$i],$this->galeria.$fileName);
                  $ret[]= $fileName;
            }
        }
        echo json_encode($ret);
     }
  } catch (Exception $e) {echo ' agregaImagenAPIAX Excepción: ',  $e, "\n";}         
}

public function renombraImagenAPIAX()
{   
try{
    $vs = $this->validaSesion(TRUE,FALSE);        
    if( isset($vs['session']))
    {   echo json_encode ($vs); 
        exit(0);
    }
    $this->load->helper('date');
    
    $errores       = array();
    $errorTxt      = "";        
    $extension     = $this -> input -> post('extension');
    $nombreArchivo = $this -> input -> post('nombreArchivo');
    $id_in         = intval($this -> input -> post('id_in'));
    $idImagen      = "he_apI_".intval(substr(now(), -5));
    $nombreArchivo = $this->galeria.$nombreArchivo.".".$extension;
    $nombreHermes  = $this->galeria.$id_in.'/'.$idImagen.".".$extension;

    if (file_exists($this->galeria.$id_in) == FALSE)
        {mkdir($this->galeria.$id_in, 0777);}                       

    $cmd = "$nombreArchivo -resize 450x300! -bordercolor black -border 1x1 ";
    exec("convert $cmd $nombreHermes ",$errores);
    unlink($nombreArchivo);
    
    if ( empty($errores) )
      { $this->md_inmuebles->updateImgAPI($id_in,$idImagen.".".$extension);}
    else
        { $errorTxt = "<br />Hubo errores al trabajar conconversion:<br />".print_r($errores); }       

    echo json_encode (array("nombreHermes" => $nombreHermes,"hImg" => $idImagen.".".$extension,"id_in" => $id_in,"erroresd" => $errorTxt));

    } catch (Exception $e) {echo 'renombraImagenAPIAX Excepción: ',  $e, "\n";} 
}

public function borraImagenCargadaAPIAX()
{   
try{
    $vs = $this->validaSesion(TRUE,FALSE);        
    if( isset($vs['session']))
    {   echo json_encode ($vs); 
        exit(0);
    }
    $extension     = $this -> input -> post('extension');
    $nombreArchivo = $this -> input -> post('nombreArchivo');
    $id_in         = $this -> input -> post('id_in');
    $nombreArchivo = $nombreArchivo.".".$extension;
    $filePath      = $this->galeria.$id_in.'/'.$nombreArchivo;
    $result        = false;        
    
    $this->md_inmuebles->updateImgAPI($id_in,NULL);

    if (file_exists($filePath))         
        { $result = unlink($filePath); }

    echo json_encode (array("result" => $result,"dirFoto" => $this->galeria));

    } catch (Exception $e) {echo 'borraImagenCargadaAPIAX Excepción: ',  $e, "\n";} 
}
/************************
 * Foto API FIN
 ************************/

/***************************
 * Foto APII INI
 **************************/
public function agregaImagenAPIIAX()
{
 try{
     $vs = $this->validaSesion(TRUE,FALSE);        
    if( isset($vs['session']))
    {   echo json_encode ($vs); 
        exit(0);
    }
    if(isset($_FILES["myfile"]))
    {
        $ret = array();     
        $error =$_FILES["myfile"]["error"];     
        if(!is_array($_FILES["myfile"]["name"])) //single file
        {            
            $fileName =  $this->utils->generaNombreImagen($_FILES["myfile"]["name"]);
            move_uploaded_file($_FILES["myfile"]["tmp_name"],$this->galeria.$fileName);
            $ret[]= $fileName;
        }
        else  //Multiple files, file[]
        {
            $fileCount = count($_FILES["myfile"]["name"]);
            for($i=0; $i < $fileCount; $i++)
            {                  
                  $fileName =  $this->utils->generaNombreImagen($_FILES["myfile"]["name"][$i]);
                  move_uploaded_file($_FILES["myfile"]["tmp_name"][$i],$this->galeria.$fileName);
                  $ret[]= $fileName;
            }
        }
        echo json_encode($ret);
     }
  } catch (Exception $e) {echo ' agregaImagenAPIAX Excepción: ',  $e, "\n";}         
}

public function renombraImagenAPIIAX()
{   
try{
    $vs = $this->validaSesion(TRUE,FALSE);        
    if( isset($vs['session']))
    {   echo json_encode ($vs); 
        exit(0);
    }
    $this->load->helper('date');
    
    $errores       = array();
    $errorTxt      = "";        
    $extension     = $this -> input -> post('extension');
    $nombreArchivo = $this -> input -> post('nombreArchivo');
    $tipo          = $this -> input -> post('tipo');
    $id_in         = intval($this -> input -> post('id_in'));
    $idImagen      = "he_apII_".intval(substr(now(), -5));
    $nombreArchivo = $this->galeria.$nombreArchivo.".".$extension;
    $nombreHermes  = $this->galeria.$id_in.'/'.$idImagen.".".$extension;

    if (file_exists($this->galeria.$id_in) == FALSE)
        {mkdir($this->galeria.$id_in, 0777);}                       

    $cmd = "$nombreArchivo -resize 450x300! -bordercolor black -border 1x1 ";
    exec("convert $cmd $nombreHermes ",$errores);
    unlink($nombreArchivo);
    
    if ( empty($errores) )
      { $this->md_inmuebles->updateImgAPII($id_in,$tipo,$idImagen.".".$extension);}
    else
        { $errorTxt = "<br />Hubo errores al trabajar conconversion:<br />".print_r($errores); }       

    echo json_encode (array("nombreHermes" => $nombreHermes,"hImg" => $idImagen.".".$extension,"id_in" => $id_in,"tipo" => $tipo,"erroresd" => $errorTxt));

    } catch (Exception $e) {echo 'renombraImagenAPIAX Excepción: ',  $e, "\n";} 
}

public function borraImagenCargadaAPIIAX()
{   
try{
    $vs = $this->validaSesion(TRUE,FALSE);        
    if( isset($vs['session']))
    {   echo json_encode ($vs); 
        exit(0);
    }
    $extension     = $this -> input -> post('extension');
    $nombreArchivo = $this -> input -> post('nombreArchivo');
    $id_in         = $this -> input -> post('id_in');
    $tipo          = $this -> input -> post('tipo');
    $nombreArchivo = $nombreArchivo.".".$extension;
    $filePath      = $this->galeria.$id_in.'/'.$nombreArchivo;
    $result        = false;        
    
    $this->md_inmuebles->updateImgAPII($id_in,$tipo,NULL);

    if (file_exists($filePath))         
        { $result = unlink($filePath); }

    echo json_encode (array("result" => $result,"dirFoto" => $this->galeria));

    } catch (Exception $e) {echo 'borraImagenCargadaAPIAX Excepción: ',  $e, "\n";} 
}
/************************
 * Foto APII FIN
 ************************/


/***************************
 * Foto  Client Logo INI
 **************************/
public function agregaImagenClientLogoAX()
{
 try{
    $vs = $this->validaSesion(TRUE,FALSE);        
    if( isset($vs['session']))
    {   echo json_encode ($vs); 
        exit(0);
    } 
    if(isset($_FILES["myfile"]))
    {
        $ret = array();     
        $error =$_FILES["myfile"]["error"];     
        if(!is_array($_FILES["myfile"]["name"])) //single file
        {            
            $fileName =  $this->utils->generaNombreImagen($_FILES["myfile"]["name"]);
            move_uploaded_file($_FILES["myfile"]["tmp_name"],$this->galeria.$fileName);
            $ret[]= $fileName;
        }
        else  //Multiple files, file[]
        {
            $fileCount = count($_FILES["myfile"]["name"]);
            for($i=0; $i < $fileCount; $i++)
            {
                  $fileName = $_FILES["myfile"]["name"][$i];
                  $fileName =  $this->utils->generaNombreImagen($_FILES["myfile"]["name"][$i]);
                  move_uploaded_file($_FILES["myfile"]["tmp_name"][$i],$this->galeria.$fileName);
                  $ret[]= $fileName;
            }
        }
        echo json_encode($ret);
     }
  } catch (Exception $e) {echo ' agregaImagenClientLogoAX Excepción: ',  $e, "\n";}         
}

public function renombraImagenClientLogoAX()
{   
try{
    $vs = $this->validaSesion(TRUE,FALSE);        
    if( isset($vs['session']))
    {   echo json_encode ($vs); 
        exit(0);
    }
    $this->load->helper('date');
    
    $errores       = array();
    $errorTxt      = "";        
    $extension     = $this -> input -> post('extension');
    $nombreArchivo = $this -> input -> post('nombreArchivo');
    $id_in         = intval($this -> input -> post('id_in'));
    $idImagen      = "he_cl_".intval(substr(now(), -5));
    $nombreArchivo = $this->galeria.$nombreArchivo.".".$extension;
    $nombreHermes  = $this->galeria.$id_in.'/'.$idImagen.".".$extension;

    if (file_exists($this->galeria.$id_in) == FALSE)
        {mkdir($this->galeria.$id_in, 0777);}                       

    $cmd = "$nombreArchivo -resize 132x57!  "; 
    // con marca de agua $cmd = "$nombreArchivo -resize 300x250! -bordercolor black -border 1x1 \( $logoJLL \)  -gravity center -composite "; 
    exec("convert $cmd $nombreHermes ",$errores);
    unlink($nombreArchivo);

    if ( empty($errores) )
       { $camposForm = array('client_logo' => $idImagen.".".$extension );
         $this -> md_inmuebles -> update_in("letter_in", $id_in,$camposForm);
       }
     else
        { $errorTxt = "<br />Hubo errores al trabajar conconversion:<br />".print_r($errores); }       

    echo json_encode (array("nombreHermes" => $nombreHermes,"hImg" => $idImagen.".".$extension,"id_in" => $id_in,"erroresd" => $errorTxt));

    } catch (Exception $e) {echo 'renombraImagenClientLogoAX Excepción: ',  $e, "\n";}  
}

public function borraImagenCargadaClientLogoAX()
{   
try{
    $vs = $this->validaSesion(TRUE,FALSE);        
    if( isset($vs['session']))
    {   echo json_encode ($vs); 
        exit(0);
    }
    $extension     = $this -> input -> post('extension');
    $nombreArchivo = $this -> input -> post('nombreArchivo');
    $id_in         = $this -> input -> post('id_in');
    $nombreArchivo = $nombreArchivo.".".$extension;
    $filePath      = $this->galeria.$id_in.'/'.$nombreArchivo;
    $result        = false;

    if (file_exists($filePath))         
        { $result = unlink($filePath); }

    echo json_encode (array("result" => $result,"dirFoto" => $this->galeria));

    } catch (Exception $e) {echo 'borraImagenCargadaFotoPortadaAX Excepción: ',  $e, "\n";} 
}
/************************
 * Foto Client Logo FIN
 ************************/   

    public function exportaInmuebleAX()
    {try{$vs = $this->validaSesion(TRUE,FALSE);        
        if( isset($vs['session']))
        {   echo json_encode ($vs); 
            exit(0);
        }
        $this->load->helper('date');       
      
        $id_in          = $this -> input -> post('id_in');
        $tipoExp        = $this -> input -> post('tipoExp');
        $reportSections = $this -> input -> post('report'); 
        $annSections    = $this -> input -> post('ann_in'); 
        $apSections     = $this -> input -> post('ap_in');  
        $fecha_name     = $this -> utils -> hermesDateFormat($this->input->post('fecha_name'));
        $repNum         = $this -> input -> post('repNum');
        $idiom          = $this -> input -> post('idiom');
    /*
        $id_in          = "94620";//70778 63153 5404
        $tipoExp        = "W";
        $reportSections = array("FP","LT","SW","SM","PH");
     
        $annSections    = array("AA2","AA3","AA4","AA5","AA6","LIM","CERT");
        $apSections     = array("APIII","APV","APVI","APVIII","APIX","APX","APXI","APXII");
        $fecha_name     = "Apr 10 2016";
        $repNum         = "IND-17-002_GE_MAULEC";
        $idiom          = "ENG";
     */
        $expFile        = '';
        $infoFile       = array("idFile" => $repNum, "id_in" => $id_in, "dir" =>  $this->export, "fechaReporte"=>date(" l, F j, Y "), "ComparableElements"=>  $this->helpercontroller->traeComparableElements(),"idiom"=>$idiom);
       
        $this->md_inmuebles->updateFechaName($id_in,$fecha_name);        
        
        $inmueble = $this -> traeInmueble($id_in,$idiom,'D',FALSE);        
        
        if (file_exists($infoFile['dir'].$infoFile['id_in']) == FALSE)
            { mkdir($this->export.$id_in, 0777); }

        $this->utils->borraArchivoPorExtension($infoFile['dir'].$infoFile['id_in'].'/',$tipoExp);                   

        if($inmueble['frontPage'] == false)
          { $expFile = "No information";}  
        else    
          { $expFile = $this->utils->exportaWord($infoFile,$inmueble,$this->galeria,$this->galeriaComp, $this->md_gallery,$reportSections,$annSections,$apSections,$this->md_inmuebles); }
           
        echo json_encode (array("expFile" => $expFile, "id_in" => $id_in, "dir" => $infoFile['dir'] ));

       } catch (Exception $e) {echo 'ERROR: ',  $e, "\n";}  
    }

    
    public function saveReportPartsAX()
    {
    try{$vs = $this->validaSesion(TRUE,FALSE);        
        if( isset($vs['session']))
        {   echo json_encode ($vs); 
            exit(0);
        }
         
        $id_in       = $this -> input -> post('id_in');
        $reportParts = $this -> input -> post('reportParts');        
        $this -> md_inmuebles -> borraSeccionInmueble($id_in,'reports_parts_in');                                      
        
        foreach ($reportParts as $rp)
          { $this -> md_inmuebles -> insertaSeccionInmueble(array('id_in'=>$id_in,'report_part'=>$rp,),'reports_parts_in'); }
          
        echo json_encode ("OK");

       } catch (Exception $e) {echo 'ERROR: ',  $e, "\n";}  
    }
    
   public function borraExpFormAX()
    {   
    try{
        $vs = $this->validaSesion(TRUE,FALSE);        
        if( isset($vs['session']))
        {   echo json_encode ($vs); 
            exit(0);
        }
        $extension     = $this -> input -> post('extension');
    $nombreArchivo     = $this -> input -> post('nombreArchivo');
    $id_in             = $this -> input -> post('id_in');   
    
    $nombreArchivo = $nombreArchivo.".".$extension;
    $filePath      = $this->export.$id_in.'/'.$nombreArchivo;
        $result        = false;

    if (file_exists($filePath))         
            { $result = unlink($filePath); }
                
    echo json_encode (array("r" => $result, "id_in" => $id_in, "nombreArchivo" => $nombreArchivo, "ext" => $extension));

       } catch (Exception $e) {echo 'borraExpFormAX Excepción: ',  $e, "\n";}   

    }
    
   
    private function traeInmueble($id_in,$idiom,$typeDate,$onlyFrontPage)
    {   
     try{//OJO modificar Appraisal!I735 en LETTER
         if($onlyFrontPage)
         {    return array("frontPage"   => $this -> md_inmuebles   -> traeDetalleInmueble($id_in,'i.id_in,i.id_status,i.preparedFor, i.propertyOf, i.fotoPortada, i.calle, i.num, i.col, i.mun, i.edo, i.cp, i.effectiveDate, i.repNum, i.exchange_rate, i.origen_name, i.concepto_name, i.clave_ano_name, i.consecutivo_name, i.fecha_name, i.repNumCap, i.address_jll, i.mt, i.et, i.appraisal_num, i.idiom',$typeDate)); }
         else
         {    return array("frontPage"   => $this -> md_inmuebles   -> traeDetalleInmueble($id_in,'i.id_in,i.id_status,i.preparedFor, i.propertyOf, i.fotoPortada, i.calle, i.num, i.col, i.mun, i.edo, i.cp, i.effectiveDate, i.repNum, i.exchange_rate, i.origen_name, i.concepto_name, i.clave_ano_name, i.consecutivo_name, i.fecha_name, i.repNumCap, i.address_jll, i.mt, i.et, i.appraisal_num, i.idiom',$typeDate),
                           "letter"      => $this -> md_inmuebles   -> traeLetter($id_in,$idiom,'effectiveDate, l.client, l.client_addres, l.client_addres_2, l.client_logo, l.atn_officer, l.jd_atn_officer, l.jd_atn_officer_2, l.parrafo1, l.texto1, l.texto2, l.texto3, l.parrafo2, l.parrafo3, l.firma_prin, l.firma_sec, fp.puesto as puestoFP, fp.nombre as nombreFP, fp.apellidos as apellidosFP, fs.puesto as puestoFS, fs.nombre as nombreFS, fs.apellidos as apellidosFS, fp.titulo as tituloFP, fs.titulo as tituloFS,l.LTchbox1,l.LTchbox2,l.LTchbox3, i.preparedFor as apprisalI735'),
                           "def"         => $this -> md_inmuebles   -> traeDefiniciones($id_in,$idiom,'inner outer','concepto'.$idiom),
                           "scopeofwork" => $this -> md_inmuebles   -> traeScopeOfWork($id_in,$idiom,'i.preparedFor as client, effectiveDate, dateInspection, sw.providedBy, sw.inspector,sw.SWParrafo1, sw.SWTit1, sw.SWText1, sw.SWTit2, sw.SWText2_1, sw.SWText2_2, sw.SWText2_3, sw.SWTit3, sw.SWText3_1, sw.SWText3_2, sw.SWTit4, sw.SWText4_1, sw.SWText4_2, sw.SWText4_3, sw.SWTit5, sw.SWText5, sw.SWTit6, sw.SWText6, sw.SWTit7, sw.SWText7 , i.calle, i.num, i.col, i.mun, i.edo, i.cp',$typeDate),
                           "summary"     => $this -> md_inmuebles   -> traeSummary($id_in,$idiom,'sm.SMTit1, sm.SMText1, sm.SMTit2, sm.SMText2, sm.SMTit3, sm.SMText3, sm.SMTit4, sm.SMText4, sm.SMTit5, sm.SMText5, sm.SMTit6, sm.SMText6, sm.SMTit7, sm.SMText7, sm.SMvc1_1, sm.SMvc1_2, SMvc1_3, sm.SMvc1_4, sm.SMvc2_1, sm.SMvc2_2, SMvc2_3, sm.SMvc2_4, sm.SMvc3_1, sm.SMvc3_2, SMvc3_3, sm.SMvc3_4, sm.SMvc4_1, sm.SMvc4_2, SMvc4_3, sm.SMvc4_4, sm.SMvc5_1, sm.SMvc5_2, SMvc5_3, sm.SMvc5_4, sm.SM_ch_vc1, sm.SM_ch_vc2, sm.SM_ch_vc3, sm.SM_ch_vc4, sm.SM_ch_vc5, i.propertyOf as SM_po, sm.SMTit1 as SM_sa, sm.SMTit1 as SM_ls, sm.SMTit1 as SM_gba, sm.SMTit1 as SM_ra, i.preparedFor as SM_ur, i.exchange_rate as SM_er, effectiveDate,l.LTchbox1,l.LTchbox2,l.LTchbox3, i.calle, i.num, i.col, i.mun, i.edo, i.cp',null),
                           "AA12"        => $this -> md_comparables -> traeDetalleAnexoA1_2($id_in,"CLS"),
                           "AA34"        => $this -> md_comparables -> traeDetalleAnexoA1_2($id_in,"CRL"),
                           "AA56"        => $this -> md_comparables -> traeDetalleAnexoA1_2($id_in,"CRS"),                           
                           "rcAA2"       => $this -> md_comparables -> traeRCporTrabajo($id_in,"CLS"),
                           "rcAA4"       => $this -> md_comparables -> traeRCporTrabajo($id_in,"CRL"),
                           "rcAA6"       => $this -> md_comparables -> traeRCporTrabajo($id_in,"CRS"),
                           "fotos"       => $this -> md_gallery     -> traeFotosGallery($id_in,"Photos"),
                           "aaimg"       => $this -> md_gallery     -> traeFotosGallery($id_in,"AAIMG"),
                           "dc"          => $this -> md_inmuebles   -> traeDatosFinancierosIn($id_in),
                           "ACA"         => array("columns" => $this->md_inmuebles->traDatosCA($id_in, "column_Id, id_in, type_area, name_area, gba, tra,, def_rcn, def_value_rcn, fdh_rcn, fc_rcn, uc_rcn, por_rcn, rcn, efage_dep, ul_dep, af_dep, dvc_dep","annexs_ca_area_in", NULL, "column_id",FALSE),
                                                  "rows"    => $this->md_inmuebles->traDatosCA($id_in, "ca_row_Id, id_in, zone", "annexs_ca_zone_in", NULL, "ca_row_Id",FALSE),
                                                  "ae"      => $this->md_inmuebles->traDatosCA($id_in, "id_aecw, id_in, leyenda, orden, amount, unit, unit_value, rcn, efage_dep, ul_dep, af_dep, dvc_dep","annexs_ca_aecw_in"  , array("campo"=>"tipo","campoValor"=>"AE"), "orden",FALSE),
                                                  "cw"      => $this->md_inmuebles->traDatosCA($id_in, "id_aecw, id_in, leyenda, orden, amount, unit, unit_value, rcn, efage_dep, ul_dep, af_dep, dvc_dep","annexs_ca_aecw_in"  , array("campo"=>"tipo","campoValor"=>"CW"), "orden",FALSE),
                                                  "cpd"     => $this->md_inmuebles->traDatosCA($id_in, "id_aecw, id_in, leyenda, orden, amount, unit, unit_value, rcn","annexs_ca_aecw_in"  , array("campo"=>"tipo","campoValor"=>"CP"), "orden",FALSE),                                                  
                                                  "id_in"   => $id_in
                                                ),
                           "totArea"     => $this -> md_inmuebles -> traDatosCA($id_in, "totCA, totGBA, totTRA","annexs_ca_totales_in", NULL, "id_in",FALSE),
                           "land"        => $this -> md_inmuebles -> traDatosCA($id_in,"taxTxt, taxBill, deedTxt, deed, plansTxt, plans ","annexs_landarea_in", NULL,NULL,FALSE),
                           "API"         => $this -> md_inmuebles -> traeAP($id_in,$idiom,"API"),
                           "APII"        => $this -> md_inmuebles -> traeAP($id_in,$idiom,"APII"),
                           "APIII"       => $this -> md_inmuebles -> traeAP($id_in,$idiom,"APIII"),
                           "APIV"        => $this -> md_inmuebles -> traeAP($id_in,$idiom,"APIV"),
                           "APV"         => array("datos"   => $this->md_inmuebles->traeAP($id_in,$idiom,"APV") ),
                           "APVI"        => $this -> md_inmuebles -> traeAP($id_in,$idiom,"APVI"),
                           "APVII"       => $this -> md_inmuebles -> traeAP($id_in,$idiom,"APVII"),
                           "APVIII"      => $this -> md_inmuebles -> traeAP($id_in,$idiom,"APVIII"),
                           "mv"          => $this -> md_inmuebles -> traDatosCA($id_in,"AXmarketVal, AX_C_Approach, AX_IC_Approach, AX_DCF_Approach, AX_SC_Approach, AX_VU_Approach","apprisal_APVII", NULL,NULL,FALSE),
                           "iv"          => $this -> md_inmuebles -> traIndicatedValue($id_in),
                           "AnnexsLC"    => $this -> md_inmuebles -> traeAP($id_in,$idiom,"AnnexsLC"),                                   
                         );
         }
        } catch (Exception $e) {echo 'traeInmueble Excepción: ',  $e, "\n";}    
    }
    
        
    public function cargaSeccionInAX()
    {   
    try{
        $vs = $this->validaSesion(TRUE,FALSE);        
        if( isset($vs['session']))
        {   echo json_encode ($vs); 
            exit(0);
        }
        $this->load->helper('date');
        $seccion = $this -> input -> post('seccion');//"LT";//
        $id_in   = $this -> input -> post('id_in');  //"85503";//
        $idiom   = $this -> input -> post('idiom');  //"ESP";//
        $totales = NULL;
        
        switch ($seccion){
            case "LT":
                $definiciones = $this -> md_inmuebles -> traeDefiniciones($id_in,$idiom,'left outer','id_definicion');
                $seccionData  = $this -> md_inmuebles -> traeLetter($id_in,$idiom,'effectiveDate, l.client, l.client_addres, l.client_addres_2, l.client_logo, l.atn_officer, l.jd_atn_officer, l.jd_atn_officer_2, l.parrafo1, l.texto1, l.texto2, l.texto3, l.parrafo2, l.parrafo3, l.firma_prin, l.firma_sec, fp.puesto as puestoFP, fp.nombre as nombreFP, fp.apellidos as apellidosFP, fs.puesto as puestoFS, fs.nombre as nombreFS, fs.apellidos as apellidosFS, fp.titulo as tituloFP, fs.titulo as tituloFS,l.LTchbox1,l.LTchbox2,l.LTchbox3, i.preparedFor as apprisalI735');
                $totales = $this->calculaTotalesMVL($id_in,$idiom);
                $reportPart  = $this->md_inmuebles->traeReportPart($id_in,"LT");
            break;
            case "SW":
                 $seccionData  = $this -> md_inmuebles -> traeScopeOfWork($id_in,$idiom,'i.preparedFor as client, effectiveDate, dateInspection, sw.providedBy, sw.inspector,i.calle,i.num,i.col,i.mun,i.edo,i.cp, sw.SWParrafo1, sw.SWTit1, sw.SWText1, sw.SWTit2, sw.SWText2_1, sw.SWText2_2, sw.SWText2_3, sw.SWTit3, sw.SWText3_1, sw.SWText3_2, sw.SWTit4, sw.SWText4_1, sw.SWText4_2, sw.SWText4_3, sw.SWTit5, sw.SWText5, sw.SWTit6, sw.SWText6, sw.SWTit7, sw.SWText7','d');
                 $definiciones = $this -> md_inmuebles -> traeSourceInformation($id_in);
                 $reportPart  = $this->md_inmuebles->traeReportPart($id_in,"SW");
            break;
            case "SM":
                 $tot      = $this -> md_inmuebles -> traDatosCA($id_in, "totCA, totGBA, totTRA","annexs_ca_totales_in", NULL, "id_in",FALSE);
                 $df       = $this -> md_inmuebles -> traeDatosFinancierosIn($id_in);
                 $land     = $this -> md_inmuebles -> traDatosCA($id_in,"taxTxt, taxBill, deedTxt, deed, plansTxt, plans ","annexs_landarea_in", NULL,NULL,FALSE);
                 
                 $totLS    = empty($land)?" ":number_format($land[0]['plans'], 2, '.', ',')." m2";                 
                 $totGBA   = empty($tot[0]['totGBA'])?" ":number_format($tot[0]['totGBA'], 2, '.', ',')." m2";
                 $totTRA   = empty($tot[0]['totTRA'])?" ":number_format($tot[0]['totTRA'], 2, '.', ',')." m2";
                 $totLSFT  = empty($land)?" ":number_format($land[0]['plans']*$df['ft'], 2, '.', ',')." ft2";
                 $totGBAFT = empty($tot[0]['totGBA'])?" ":number_format($tot[0]['totGBA']*$df['ft'], 2, '.', ',')." ft2";
                 $totTRAFT = empty($tot[0]['totTRA'])?" ":number_format($tot[0]['totTRA']*$df['ft'], 2, '.', ',')." ft2";
                 
                 $seccionData  = $this -> md_inmuebles -> traeSummary($id_in,$idiom,'sm.SMTit1, sm.SMText1, sm.SMTit2, sm.SMText2, sm.SMTit3, sm.SMText3, sm.SMTit4, sm.SMText4, sm.SMTit5, sm.SMText5, sm.SMTit6, sm.SMText6, sm.SMTit7, sm.SMText7, sm.SMvc1_1, sm.SMvc1_2, SMvc1_3, sm.SMvc1_4, sm.SMvc2_1, sm.SMvc2_2, SMvc2_3, sm.SMvc2_4, sm.SMvc3_1, sm.SMvc3_2, SMvc3_3, sm.SMvc3_4, sm.SMvc4_1, sm.SMvc4_2, SMvc4_3, sm.SMvc4_4, sm.SMvc5_1, sm.SMvc5_2, SMvc5_3, sm.SMvc5_4, sm.SM_ch_vc1, sm.SM_ch_vc2, sm.SM_ch_vc3, sm.SM_ch_vc4, sm.SM_ch_vc5, i.propertyOf as SM_po, sm.SMTit1 as SM_sa, sm.SMTit1 as SM_ls, sm.SMTit1 as SM_gba, sm.SMTit1 as SM_ra, i.preparedFor as SM_ur, i.exchange_rate as SM_er, effectiveDate,l.LTchbox1,l.LTchbox2,l.LTchbox3, i.calle, i.num, i.col, i.mun, i.edo, i.cp','d');
                 $letter       = $this -> md_inmuebles -> traeLetter($id_in,$idiom,'effectiveDate, l.client, l.client_addres, l.client_addres_2, l.client_logo, l.atn_officer, l.jd_atn_officer, l.jd_atn_officer_2, l.parrafo1, l.texto1, l.texto2, l.texto3, l.parrafo2, l.parrafo3, l.firma_prin, l.firma_sec, fp.puesto as puestoFP, fp.nombre as nombreFP, fp.apellidos as apellidosFP, fs.puesto as puestoFS, fs.nombre as nombreFS, fs.apellidos as apellidosFS, fp.titulo as tituloFP, fs.titulo as tituloFS,l.LTchbox1,l.LTchbox2,l.LTchbox3, i.preparedFor as apprisalI735');
                 $sw           = $this -> md_inmuebles -> traeScopeOfWork($id_in,$idiom,'i.preparedFor as client, effectiveDate, dateInspection, sw.providedBy, sw.inspector,i.calle,i.num,i.col,i.mun,i.edo,i.cp, sw.SWParrafo1, sw.SWTit1, sw.SWText1, sw.SWTit2, sw.SWText2_1, sw.SWText2_2, sw.SWText2_3, sw.SWTit3, sw.SWText3_1, sw.SWText3_2, sw.SWTit4, sw.SWText4_1, sw.SWText4_2, sw.SWText4_3, sw.SWTit5, sw.SWText5, sw.SWTit6, sw.SWText6, sw.SWTit7, sw.SWText7','d');
                 $API          = $this -> md_inmuebles -> traeAP($id_in,$idiom,"API");

                 $AIText11 = $API[0]['AIText11'];
                 if($AIText11==0){
                    $AIText11 = "";
                 }
 

                 $definiciones = array("hoy"      => date(" l, F j, Y "),
                                       "totLS"    => $totLS,
                                       "totGBA"   => $totGBA,
                                       "totTRA"   => $totTRA,
                                       "totLSFT"  => $totLSFT,
                                       "totGBAFT" => $totGBAFT,
                                       "totTRAFT" => $totTRAFT,
                                       "SWTit6"   => $sw['SWTit6'],
                                       "SWText6"  => $sw['SWText6'],
                                       "SWTit7"   => $sw['SWTit7'],
                                       "SWText7"  => $sw['SWText7'],
                                       "AITit11"  => $API[0]['AITit11'],
                                       "AIText11" => $AIText11,
                                       "client"   => $letter['client']);
                 $totales = $this->calculaTotalesMVL($id_in,$idiom);
                 $reportPart  = $this->md_inmuebles->traeReportPart($id_in,"SM");
            break;        
        }
        
    echo json_encode (array("seccionData" => $seccionData, "definiciones" => $definiciones,"tipo" => $this->myData['tipo'],  "totales" =>  $totales, "reportPart"=>$reportPart) );

       } catch (Exception $e) {echo 'cargaSeccionInAX Excepción: ',  $e, "\n";} 

    }
    
    public function cargaAPInAX()
    {   
    try{
        $vs = $this->validaSesion(TRUE,FALSE);        
        if( isset($vs['session']))
        {   echo json_encode ($vs); 
            exit(0);
        }
        $ap     = $this -> input -> post('ap');//"APVIII";//
        //echo "ap:" . $ap . "<br>";
        $id_in  = $this -> input -> post('id_in');//"94620";//
        //echo "id_in:" . $id_in . "<br>";
        $idiom  = $this -> input -> post('idiom');//"ESP";//
        //echo "idiom:" . $idiom . "<br>";
        $apCore = NULL;
        $er     = NULL;
        $ft     = NULL;
        $reportPart = NULL;

 
        
        switch ($ap){
            case "API":
                $typeDate ='D';
                $datos    =  array("frontPage"   => $this -> md_inmuebles -> traeDetalleInmueble($id_in,'i.id_in,i.id_status,i.preparedFor, i.propertyOf, i.fotoPortada, i.calle, i.num, i.col, i.mun, i.edo, i.cp, i.effectiveDate, i.repNum, i.exchange_rate, i.fecha_alta',$typeDate),
                                   "letter"      => $this -> md_inmuebles -> traeLetterQuery($id_in,'l.client, l.client_addres, l.client_addres_2'),
                                   "scopeofwork" => $this -> md_inmuebles -> traeScopeOfWork($id_in, $idiom, 'i.preparedFor as client, effectiveDate, dateInspection, sw.providedBy, sw.inspector, sw.SWParrafo1, sw.SWTit1, sw.SWText1, sw.SWTit2, sw.SWText2_1, sw.SWText2_2, sw.SWText2_3, sw.SWTit3, sw.SWText3_1, sw.SWText3_2, sw.SWTit4, sw.SWText4_1, sw.SWText4_2, sw.SWText4_3, sw.SWTit5, sw.SWText5, sw.SWTit6, sw.SWText6, sw.SWTit7, sw.SWText7, i.calle, i.num, i.col, i.mun, i.edo, i.cp',$typeDate),
                                   "API"         => $this -> md_inmuebles -> traeAP($id_in,$idiom,"API"),
                                   "id_in"       => $id_in);
                
                $apCore     = $this -> helpercontroller -> creaCoreAPI($datos);
                $reportPart = $this->md_inmuebles->traeReportPart($id_in,"API");
            break;
            case "APII":
                $typeDate ='D';
                $df       = $this -> md_inmuebles->traeDatosFinancierosIn($id_in);
                $datos    =  array("frontPage" => $this->md_inmuebles->traeDetalleInmueble($id_in,'i.id_in,i.id_status,i.preparedFor, i.propertyOf, i.calle, i.num, i.col, i.mun, i.edo, i.cp, i.effectiveDate, i.exchange_rate',$typeDate),
                                   "columns"   => $this->md_inmuebles->traDatosCA($id_in, "column_Id, id_in, type_area, name_area, gba, tra, def_rcn, def_value_rcn, fdh_rcn, fc_rcn, uc_rcn, por_rcn, rcn, efage_dep, ul_dep, af_dep, dvc_dep","annexs_ca_area_in", NULL, "column_id",FALSE),
                                   "rows"      => $this->md_inmuebles->traDatosCA($id_in, "ca_row_Id, id_in, zone"                                         ,"annexs_ca_zone_in"  , NULL                                     , "ca_row_Id",FALSE),
                                   "ae"        => $this->md_inmuebles->traDatosCA($id_in, "id_aecw, id_in, leyenda, orden, amount, unit, unit_value, rcn, efage_dep, ul_dep, af_dep, dvc_dep","annexs_ca_aecw_in"  , array("campo"=>"tipo","campoValor"=>"AE"), "orden",FALSE),
                                   "cw"        => $this->md_inmuebles->traDatosCA($id_in, "id_aecw, id_in, leyenda, orden, amount, unit, unit_value, rcn, efage_dep, ul_dep, af_dep, dvc_dep","annexs_ca_aecw_in"  , array("campo"=>"tipo","campoValor"=>"CW"), "orden",FALSE),
                                   "totArea"   => $this->md_inmuebles->traDatosCA($id_in, "totCA, totGBA, totTRA","annexs_ca_totales_in", NULL, "id_in",FALSE),
                                   "land"      => $this->md_inmuebles->traDatosCA($id_in,"taxTxt, taxBill, deedTxt, deed, plansTxt, plans ","annexs_landarea_in", NULL,NULL,FALSE),
                                   "APII"      => $this->md_inmuebles->traeAP($id_in,$idiom,"APII"),
                                   "ft"        => $df['ft'],
                                   "id_in"     => $id_in);
                
                $apCore     = $this -> helpercontroller -> creaCoreAPII($datos,$this->md_inmuebles);
                $reportPart = $this->md_inmuebles->traeReportPart($id_in,"APII");
            break;
            case "APIII":
                $datos    =  array("APIII"       => $this -> md_inmuebles -> traeAP($id_in,$idiom,"APIII"),
                                   "id_in"       => $id_in);
                
                $apCore     = $this -> helpercontroller -> creaCoreAPIII($datos);
                $reportPart = $this->md_inmuebles->traeReportPart($id_in,"APIII");
            break;
            case "APIV":
                $datos    =  array("APIV"        => $this -> md_inmuebles -> traeAP($id_in,$idiom,"APIV"),
                                   "id_in"       => $id_in);
                
                $apCore     = $this -> helpercontroller -> creaCoreAPIV($datos);
                $reportPart = $this->md_inmuebles->traeReportPart($id_in,"APIV");
            break;
            case "APV":
                $typeDate ='D';
                $df       = $this -> md_inmuebles->traeDatosFinancierosIn($id_in);
                $datos    = array("columns" => $this->md_inmuebles->traDatosCA($id_in, "column_Id, id_in, type_area, name_area, gba, tra, def_rcn, def_value_rcn, fdh_rcn, fc_rcn, uc_rcn, por_rcn, rcn, efage_dep, ul_dep, af_dep, dvc_dep","annexs_ca_area_in", NULL, "column_id",FALSE),
                                  "rows"    => $this->md_inmuebles->traDatosCA($id_in, "ca_row_Id, id_in, zone"                                         ,"annexs_ca_zone_in"  , NULL                                     , "ca_row_Id",FALSE),
                                  "ae"      => $this->md_inmuebles->traDatosCA($id_in, "id_aecw, id_in, leyenda, orden, amount, unit, unit_value, rcn, efage_dep, ul_dep, af_dep, dvc_dep","annexs_ca_aecw_in"  , array("campo"=>"tipo","campoValor"=>"AE"), "orden",FALSE),
                                  "cw"      => $this->md_inmuebles->traDatosCA($id_in, "id_aecw, id_in, leyenda, orden, amount, unit, unit_value, rcn, efage_dep, ul_dep, af_dep, dvc_dep","annexs_ca_aecw_in"  , array("campo"=>"tipo","campoValor"=>"CW"), "orden",FALSE),
                                  "cpd"     => $this->md_inmuebles->traDatosCA($id_in, "id_aecw, id_in, leyenda, orden, amount, unit, unit_value, rcn","annexs_ca_aecw_in"  , array("campo"=>"tipo","campoValor"=>"CP"), "orden",FALSE),
                                  "totArea" => $this->md_inmuebles->traDatosCA($id_in, "totCA, totGBA, totTRA","annexs_ca_totales_in", NULL, "id_in",FALSE),
                                  "land"    => $this->md_inmuebles->traDatosCA($id_in,"taxTxt, taxBill, deedTxt, deed, plansTxt, plans ","annexs_landarea_in", NULL,NULL,FALSE),
                                 "income"  => $this->md_inmuebles->traDatosCA($id_in,"AVIoe_unf, AVIcap_rate, AVI_totExp","apprisal_APVI", NULL,NULL,FALSE),
                                  "iv"      => $this->md_inmuebles->traIndicatedValue($id_in),
                                  "APV"     => $this->md_inmuebles->traeAP($id_in,$idiom,"APV"),                                  
                                  "ft"      => $df['ft'],
                                  "er"      => $df['exchange_rate'],
                                  "id_in"   => $id_in
                               );                
                $apCore     = $this -> helpercontroller -> creaCoreAPV($datos,$this->md_inmuebles,$this->utils->leyendasAPV());
                $reportPart = $this->md_inmuebles->traeReportPart($id_in,"APV");
                $er         = $df['exchange_rate'];
            break;
            case "APVI":
                $df       = $this -> md_inmuebles->traeDatosFinancierosIn($id_in);
                $datos    =  array("APVI"    => $this -> md_inmuebles -> traeAP($id_in,$idiom,"APVI"),
                                   "totArea" => $this -> md_inmuebles -> traDatosCA($id_in, "totCA, totGBA, totTRA","annexs_ca_totales_in", NULL, "id_in",FALSE),
                                   "land"    => $this -> md_inmuebles -> traDatosCA($id_in,"taxTxt, taxBill, deedTxt, deed, plansTxt, plans ","annexs_landarea_in", NULL,NULL,FALSE),
                                   "iv"      => $this -> md_inmuebles -> traIndicatedValue($id_in),
                                   "ft"      => $df['ft'],
                                   "er"      => $df['exchange_rate'],
                                   "id_in"   => $id_in);
                $apCore     = $this -> helpercontroller -> creaCoreAPVI($datos,$this->utils->leyendasAPV());
                $reportPart = $this->md_inmuebles->traeReportPart($id_in,"APVI");
            break;
            case "APVII":
                $datos    =  array("APVII"   => $this -> md_inmuebles -> traeAP($id_in,$idiom,"APVII"),
                                   "totArea" => $this -> md_inmuebles -> traDatosCA($id_in, "totCA, totGBA, totTRA","annexs_ca_totales_in", NULL, "id_in",FALSE),
                                   "costos"  => $this -> md_inmuebles -> traDatosCA($id_in,"AV_vica, AV_oe","apprisal_APV", NULL,NULL,FALSE),
                                   "income"  => $this -> md_inmuebles -> traDatosCA($id_in,"AVIivudicael","apprisal_APVI", NULL,NULL,FALSE),
                                   "iv"      => $this -> md_inmuebles -> traIndicatedValue($id_in),
                                   "id_in"   => $id_in);
                $apCore     = $this -> helpercontroller -> creaCoreAPVII($datos,$this->utils->leyendasAPV());
                $reportPart = array ("APVII"  => $this->md_inmuebles->traeReportPart($id_in,"APVII")
                                     ,"APVIII" => $this->md_inmuebles->traeReportPart($id_in,"APVIII")
                                     ,"APIX"   => $this->md_inmuebles->traeReportPart($id_in,"APIX")
                                     ,"APX"    => $this->md_inmuebles->traeReportPart($id_in,"APX")
                                     );
            break;
            case "APVIII":
                $df       = $this -> md_inmuebles->traeDatosFinancierosIn($id_in);
                $datos    =  array("APVIII"   => $this -> md_inmuebles -> traeAP($id_in,$idiom,"APVIII"),
                                   "mv"       => $this -> md_inmuebles -> traDatosCA($id_in,"AXmarketVal, AX_C_Approach, AX_IC_Approach, AX_DCF_Approach, AX_SC_Approach, AX_VU_Approach","apprisal_APVII", NULL,NULL,FALSE),
                                   "totArea"  => $this -> md_inmuebles -> traDatosCA($id_in, "totCA, totGBA, totTRA","annexs_ca_totales_in", NULL, "id_in",FALSE),
                                   "frontPage"=> $this -> md_inmuebles -> traDatosCA($id_in,"mt, et ","inmuebles", NULL,NULL,FALSE),
                                   "letter"   => $this -> md_inmuebles -> traeLetter($id_in,$idiom,'effectiveDate, l.client, l.client_addres, l.client_addres_2, l.client_logo, l.atn_officer, l.jd_atn_officer, l.jd_atn_officer_2, l.parrafo1, l.texto1, l.texto2, l.texto3, l.parrafo2, l.parrafo3, l.firma_prin, l.firma_sec, fp.puesto as puestoFP, fp.nombre as nombreFP, fp.apellidos as apellidosFP, fs.puesto as puestoFS, fs.nombre as nombreFS, fs.apellidos as apellidosFS, fp.titulo as tituloFP, fs.titulo as tituloFS,l.LTchbox1,l.LTchbox2,l.LTchbox3, i.preparedFor as apprisalI735'),
                                   "ft"       => $df['ft'],
                                   "er"       => $df['exchange_rate'],
                                   "id_in"    => $id_in); 

                  $datos2    =  array("APVII"   => $this -> md_inmuebles -> traeAP($id_in,$idiom,"APVII"),
                                   "totArea" => $this -> md_inmuebles -> traDatosCA($id_in, "totCA, totGBA, totTRA","annexs_ca_totales_in", NULL, "id_in",FALSE),
                                   "costos"  => $this -> md_inmuebles -> traDatosCA($id_in,"AV_vica, AV_oe","apprisal_APV", NULL,NULL,FALSE),
                                   "income"  => $this -> md_inmuebles -> traDatosCA($id_in,"AVIivudicael","apprisal_APVI", NULL,NULL,FALSE),
                                   "iv"      => $this -> md_inmuebles -> traIndicatedValue($id_in),
                                   "id_in"   => $id_in);                       


                $apCore     = $this -> helpercontroller -> creaCoreAPVIII($datos,$this->utils->leyendasAPV(), $datos2);
                $reportPart = array ("APXI"  => $this->md_inmuebles->traeReportPart($id_in,"APXI")
                                     ,"APXII" => $this->md_inmuebles->traeReportPart($id_in,"APXII")                                     
                                     );
            break;
            case "ACA":
                  $df          = $this -> md_inmuebles->traeDatosFinancierosIn($id_in);                    
                  $ft          = $df['ft'];
                  $er          = $df['exchange_rate'];
                  $datos       = array("columns" => $this->md_inmuebles->traDatosCA($id_in, "column_Id, id_in, type_area, name_area, gba, tra, def_rcn, def_value_rcn, fdh_rcn, fc_rcn, uc_rcn, por_rcn, rcn, efage_dep, ul_dep, af_dep, dvc_dep","annexs_ca_area_in", NULL, "column_id",FALSE),
                                       "rows"    => $this->md_inmuebles->traDatosCA($id_in, "ca_row_Id, id_in, zone"                                         ,"annexs_ca_zone_in"  , NULL                                     , "ca_row_Id",FALSE),
                                       "ae"      => $this->md_inmuebles->traDatosCA($id_in, "id_aecw, id_in, leyenda, orden, amount, unit, unit_value, rcn, efage_dep, ul_dep, af_dep, dvc_dep","annexs_ca_aecw_in"  , array("campo"=>"tipo","campoValor"=>"AE"), "orden",TRUE),
                                       "cw"      => $this->md_inmuebles->traDatosCA($id_in, "id_aecw, id_in, leyenda, orden, amount, unit, unit_value, rcn, efage_dep, ul_dep, af_dep, dvc_dep","annexs_ca_aecw_in"  , array("campo"=>"tipo","campoValor"=>"CW"), "orden",TRUE),
                                       "land"    => $this->md_inmuebles->traDatosCA($id_in,"taxTxt, taxBill, deedTxt, deed, plansTxt, plans ","annexs_landarea_in", NULL,NULL,FALSE),
                                       "ft"      => $ft,
                                       "er"      => $er,
                                       "id_in"   => $id_in
                                      );
                  $apCore     = array("tablaACA" => $this->helpercontroller->creaTablaACA($datos,$this->md_inmuebles,$this->utils->leyendasAPV()));
                  $reportPart = NULL;
            break;
        }
        
    echo json_encode (array("apCore" => $apCore,"er"=>$er,"ft"=>$ft,"reportPart"=>$reportPart) );

       } catch (Exception $e) {echo 'cargaSeccionInAX Excepción: ',  $e, "\n";}
       
    }
    
    public function cargaAnexoInAX()
    {   
    try{        
        $vs = $this->validaSesion(TRUE,FALSE);        
        if( isset($vs['session']))
        {   echo json_encode ($vs); 
            exit(0);
        }
        $seccion = $this -> input -> post('seccion');//"AA2";//
        $id_in   = $this -> input -> post('id_in');//5404";//
        $idiom   = $this -> input -> post('idiom');//"ESP";//
        
        if ($seccion === "AA1" | $seccion === "AA2")
            { $tipoComp = "CLS"; } 
        elseif ($seccion === "AA3" | $seccion === "AA4")
            { $tipoComp = "CRL"; }
        else
            { $tipoComp = "CRS"; }
        
        switch ($seccion){            
            case "AA1";
            case "AA3";
            case "AA5";
                $comparables  = $this -> md_comparables -> traeComparableAnexo($id_in,$tipoComp);
                $grid         = NULL;
                $ft           = NULL;
                $ce           = NULL;
                $aaCore       = NULL;
                $reportPart = array ( "AA1" => $this->md_inmuebles->traeReportPart($id_in,"AA1")
                                     ,"AA3" => $this->md_inmuebles->traeReportPart($id_in,"AA3")
                                     ,"AA5" => $this->md_inmuebles->traeReportPart($id_in,"AA5")
                                     );
            break;
            case "AA2";
            case "AA4";
            case "AA6";      
                 $comparables = $this -> md_comparables -> traeDetalleComparableAnexoA1($id_in,$tipoComp);
                 if(sizeof($comparables) === 0)
                 {
                    $rc          = NULL;
                    $df          = NULL;
                    $ft          = NULL;
                    $grid        = NULL;
                    $comparables = false;
                    $ce          = NULL;
                    $aaCore      = NULL;
                 }
                 else
                 {
                    $land        = $this -> md_inmuebles   -> traDatosCA($id_in,"taxTxt, taxBill, deedTxt, deed, plansTxt, plans ","annexs_landarea_in", NULL,NULL,FALSE);
                    $totArea     = $this -> md_inmuebles   -> traDatosCA($id_in, "totCA, totGBA, totTRA","annexs_ca_totales_in", NULL, "id_in",FALSE);
                    $area        = array("land"=> empty($land)?0:$land[0]['plans'],"tra"=>empty($totArea)?0:$totArea[0]['totTRA']);
                    $rc          = $this -> md_comparables -> traeRCporTrabajo($id_in,$tipoComp);
                    $df          = $this -> md_inmuebles   -> traeDatosFinancierosIn($id_in);                    
                    $ft          = $df['ft'];
                    $grid        = $this -> gridAnexo($seccion, $rc, $comparables, $df, $id_in, $area);
                    $comparables = NULL;
                    $ce          = $this -> helpercontroller -> traeComparableElements();                    
                    $aaCore      = NULL;
                 }
                 $reportPart = array ("AA2" => $this->md_inmuebles->traeReportPart($id_in,"AA2")
                                     ,"AA4" => $this->md_inmuebles->traeReportPart($id_in,"AA4")
                                     ,"AA6" => $this->md_inmuebles->traeReportPart($id_in,"AA6")
                                     );
            break;
            case "AA8":
                 $rc          = NULL;
                 $df          = NULL;
                 $ft          = NULL;
                 $grid        = NULL;
                 $comparables = false;
                 $ce          = NULL;
                 $fotos       = $this -> md_gallery -> traeFotosGallery($id_in,"Photos");
                 $tablaFotos  = $this -> helpercontroller -> creaTablaPhotos($fotos);
                
                 $aaimg       = $this -> md_gallery -> traeFotosGallery($id_in,"AAIMG");
                 $tablaAAIMG  = $this -> helpercontroller -> creaTablaAAIMG($this->md_gallery,$id_in,$aaimg);
                 
                 $options = $this->md_catalogo-> poblarSelect('AAIMG',false);
                 $selectAAMIMG = form_dropdown('selectAAIMG', $options, "0",'id="selectAAIMG" onChange="cargaAAIMG(this);" ');                 
                 
                 $aaCore = array("tablaFotos"   => $tablaFotos,
                                 "tablaAAIMG"   => $tablaAAIMG,
                                 "selectAAMIMG" => $selectAAMIMG,
                                );
                 $reportPart = array ("PH" => $this->md_inmuebles->traeReportPart($id_in,"PH")
                                     ,"AAA" => $this->md_inmuebles->traeReportPart($id_in,"AAA")                                     
                                     );
            break;
            case "AA9":
                $comparables  = NULL;
                $grid         = NULL;
                $ft           = NULL;
                $ce           = NULL;
                $datos    =  array("letter"   => $this -> md_inmuebles -> traeLetterQuery($id_in,'l.client, l.client_addres, l.client_addres_2'),
                                   "AnnexsLC" => $this -> md_inmuebles -> traeAP($id_in,$idiom,"AnnexsLC"),                                   
                                   "id_in"    => $id_in);
                
                $annexsLim   = $this -> helpercontroller -> creaCoreAnnexsLim($datos);                
                $annexsCert  = $this -> helpercontroller -> creaCoreAnnexsCert($datos);
                $aaCore      = array("annexsLim"   => $annexsLim,
                                     "annexsCert"  => $annexsCert
                                    );
                $reportPart = array ( "LIM"  => $this->md_inmuebles->traeReportPart($id_in,"LIM")
                                     ,"CERT" => $this->md_inmuebles->traeReportPart($id_in,"CERT")                                     
                                     );
            break;
        }         

        echo json_encode (array("seccion"=>$seccion,"tipoComp"=>$tipoComp,"comparables" => $comparables,"grid" => $grid,"aaCore"=>$aaCore,"ft" => $ft,"ce" => $ce,"tipo" => $this->myData['tipo'], "reportPart"=>$reportPart) );

       } catch (Exception $e) {echo 'cargaAnexoInAX Excepción: ',  $e, "\n";}   

    }

    private function calculaTotalesMVL($id_in,$idiom)
     {
     try{
            $df           = $this -> md_inmuebles->traeDatosFinancierosIn($id_in);
            $datos        = array("APVIII"   => $this -> md_inmuebles -> traeAP($id_in,$idiom,"APVIII"),
                                  "mv"       => $this -> md_inmuebles -> traDatosCA($id_in,"AXmarketVal, AX_C_Approach, AX_IC_Approach, AX_DCF_Approach, AX_SC_Approach, AX_VU_Approach","apprisal_APVII", NULL,NULL,FALSE),
                                  "totArea"  => $this -> md_inmuebles -> traDatosCA($id_in, "totCA, totGBA, totTRA","annexs_ca_totales_in", NULL, "id_in",FALSE),
                                  "ft"      => $df['ft'],
                                 );
            $param           = $this->utils->leyendasAPV();
            $totTRA          = (empty($datos['totArea'][0]['totTRA'])?1:$datos['totArea'][0]['totTRA']);
            $AXmarketVal     = empty($datos['mv'][0]['AXmarketVal'])?0:$datos['mv'][0]['AXmarketVal'];
            $AXmarketValM2   = $AXmarketVal / $totTRA;
            $AXmarketValft2  = $AXmarketValM2 / $datos['ft'];

            $AX_VU_Approach  = empty($datos['mv'][0]['AX_VU_Approach'])?0:$datos['mv'][0]['AX_VU_Approach'];
            $AXVUApproachM2  = $AX_VU_Approach / $totTRA;
            $AXVUApproachft2 = $AXVUApproachM2 / $datos['ft'];

            $AXIliquidVal    = empty($datos['APVIII'][0]['AXIliquidVal'])?0:$datos['APVIII'][0]['AXIliquidVal'];
            $AXIliquidValM2  = $AXIliquidVal / $totTRA;
            $AXIliquidValft2 = $AXIliquidValM2 / $datos['ft'];
            $totales = array('$ '.number_format($this->utils->redondearHermes($AXmarketVal), 2, '.', ',').' '.$param['currency'],
                            '$ '.number_format($AXmarketValM2, 2, '.', ',').' '.$param['currency'].' / m2',
                            '$ '.number_format($AXmarketValft2, 2, '.', ',').' '.$param['currency'].' / ft2',
                            '$ '.number_format($this->utils->redondearHermes($AX_VU_Approach), 2, '.', ',').' '.$param['currency'],
                            '$ '.number_format($AXVUApproachM2, 2, '.', ',').' '.$param['currency'].' / m2',
                            '$ '.number_format($AXVUApproachft2, 2, '.', ',').' '.$param['currency'].' / ft2',
                            '$ '.number_format($this->utils->redondearHermes($AXIliquidVal), 2, '.', ',').'</label> '.$param['currency'],
                            '$ '.number_format($AXIliquidValM2, 2, '.', ',').' '.$param['currency'].' / m2',
                            '$ '.number_format($AXIliquidValft2, 2, '.', ',').' '.$param['currency'].' / ft2'
                            );
            return $totales;
        
        }  catch (Exception $e) {echo 'calculaTotalesMVL Excepción: ',  $e, "\n";}       
    }


    private function gridAnexo($tipo, $rc, $comparables, $df, $id_in, $AreaConst)
    {
     try{
        $headTable = array("ELEMENT","SUBJECT");
        $landStr     = array();
        $priceStr    = array();
        $priceUSD    = array();// PRECIO EN DOLARES
        $priceMX     = array();
        $priceMXAcum = array();// PRECIO EN DOLARES
        $adPrice     = array();
        $adPriceValue= array();    
        $land        = array();
        $weight      = array();
        $weightPor   = array();
        $weightPorTot= array();
        $composition = array();
        $tipoComp   = "";
        $ft         = $df['ft'];
        $ftyear     = $df['ftyear'];
        $er         = $df['exchange_rate'];        

        $maxColumns = sizeof($comparables);        
        for($i = 0; $i < $maxColumns;$i++)
            { $headTable[] = "COMPARABLE ".($i+1);  }
        
        if($tipo == "AA2")
            { $landSunject = empty($AreaConst['land'])?0:$AreaConst['land'];
              $leyendas    = array("A.2 LAND SALES ","Site size(m2)<br>(ft2)","Price ","Adjusted Price","Weight","Composition of Indicated Value per m2","Composition of Indicated Value per ft2"); 
              $tipoComp    = "CLS";
              $limiteRC8   = 22;
            }
        elseif($tipo == "AA4")
            { $landSunject = empty($AreaConst['tra'])?0:$AreaConst['tra'];
              $leyendas    = array("A.4 LEASE ","Rentable area (m2)<br>(ft2)","Price ","Adjusted Price","Weight","Composition of Indicated Value per m2","Composition of Indicated Value per ft2"); 
              $tipoComp    = "CRL"; 
              $limiteRC8   = 24;
            }
            else
            { $landSunject = empty($AreaConst['tra'])?0:$AreaConst['tra'];
              $leyendas    = array("A.6 SALE ","Site size(m2)<br>(ft2)","Price ","Adjusted Price","Weight","Composition of Indicated Value per m2","Composition of Indicated Value per ft2"); 
              $tipoComp    = "CRS";
              $limiteRC8   = 26;
            }
            
        $quitarRC   = $this->md_inmuebles->traeRCVacios($id_in,$tipoComp);
        
        $tabla = br(2).'<section class="row col col-10 subtitulo"><h1>'.$leyendas[0].'COMPARISON DATA AND ADJUSTMENT GRID'.br(2).'</h1></section>';

        $this -> table -> clear();        
        $this -> table -> set_template(array('table_open' => "<table class='tableGrid' cellspacing='0' cellpadding='0' width='100%' id='grid$tipo'>" ));        
        $this -> table -> set_heading($headTable);
        
        foreach ($comparables as $cAA1)
        {
            $comp       = $this -> md_comparables -> traeComparable($cAA1['id_comp']);              
            $landStr [] = array('data'=>number_format((($tipo === "AA2")?$comp[0]['land_m2']:$comp[0]['construction']), 2, '.', ',').' m2<br>'.number_format((($tipo === "AA2")?$comp[0]['land_ft2']:($comp[0]['construction']*$ft)), 2, '.', ',').' ft2', 'class'=>'linead');
            $land    [] = ($tipo === "AA2")?$comp[0]['land_m2']:$comp[0]['construction'];            
            $priceStr[] = array('data'=>$this->helpercontroller->creaAP($tipoComp,$comp[0]['unit_value_mx'],$er,$ft,$ftyear), 'class'=>'linead');
            $priceMX [] = $comp[0]['unit_value_mx'];
            $priceUSD[] = $comp[0]['unit_value_mx'] / $er;//PRECIO EN DOLARES
        }
        $maxP = count($priceUSD)==0?0:max($priceUSD);
        $minP = count($priceUSD)==0?0:min($priceUSD);
        $avP  = count($priceUSD)==0?0:array_sum($priceUSD) / count($priceUSD);
        $std  = count($priceUSD)==0?0:($this->utils->stats_standard_deviation($priceUSD));
        $variationCoeff = count($priceUSD)==0?0:(($std / $avP) * 100 );
        
        if($variationCoeff<=20) { $semaforoVC = "verde"; }
        elseif ($variationCoeff>=21 && $variationCoeff<=30) {  $semaforoVC = "amarillo"; }
            else { $semaforoVC = "rojo" ; }        
        
        $cel1P = array('data'=>$leyendas[1], 'class'=>'linead');
        $cel2P = array('data'=>number_format((($tipo === "AA2")?$AreaConst['land']:$AreaConst['tra']), 2, '.', ',').' m2<br>'.number_format((($tipo === "AA2")?($AreaConst['land']*$ft):($AreaConst['tra']*$ft)), 2, '.', ',').' ft2',          'class'=>'linead'); //SUBJECT
        $this->table->add_row(array_merge(array($cel1P, $cel2P),$landStr));
        
        $cel1L = array('data'=>$leyendas[2], 'class'=>'linead');
        $cel2L = array('data'=>' ', '         class'=>'linead');//SUBJECT
        $this->table->add_row(array_merge(array($cel1L, $cel2L),$priceStr));
        
        /**********  RELEVANT CHARACTERISTICS OF THE COMPARABLE INI*********************/
        $x         = 0;
        $ap        = 0;
        $espacio   = "";
        $leyendaRC = "";        
        $limiteRC7 = ($tipo==="AA2")?16:13;
        $menosRC   = ($tipo==="AA2")?0:3;
        $limiteRC  = 18-$menosRC;
        $totalPrice= array();
        
        foreach ($rc as $r)
        {   $x++;
            $rcValue = array();
            $y       = -1;
            
            foreach ($comparables as $cAA1)
                {   $y++;
                    $leyendaRC    = $this->utils->traeLeyendaRC($x,$cAA1,$r['campo'],$tipo);

                    if($leyendaRC !== NULL)                    
                     {
                        $price      = ($x===1)?$priceMX[$y]:$priceMXAcum[$y];
                        $porcentaje = $this->utils->traeRCAdjustmentCriteria($r['tipo'],$cAA1,$r,$limiteRC8);
                        if($x===6)
                            { $porcentaje = empty($landSunject)?0:($porcentaje+((1-$porcentaje)*($land[$y]/$landSunject))-1); }
                        $ap               = $price*(1+($porcentaje));
                        $rcValue     []   = $this->utils->traeAdjustmentCriteriaPorDesc($r['tipo'],$x,$cAA1,$porcentaje,$tipo,"<br>");
                        $sumPor           = abs(($porcentaje*100));                        
                        $celdaAP          = array('data'=>$this->helpercontroller->creaAP($tipoComp,$ap,$er,$ft,$ftyear), 'class'=>'adprice');
                        $adPrice     []   = $celdaAP;                        
                        $adPriceValue[]   = round($ap,2);
                        $priceMXAcum [$y] = $ap;
                        $weightPor   [$y] = $sumPor;
                        if ($x === $limiteRC)
                            { $totalPrice  [$y]   = round($ap/$er,4); }
                     }
                     else
                    {
                    $rcValue     []   = "";
                    $adPrice     []   = "";
                    }
                }

            if($leyendaRC !== NULL)           
            {   
                $limiteX        = $limiteRC7-$quitarRC;
                $weightPorTot[] = $weightPor; 
                $espacio        = ( $x>5 & $x<=$limiteX )?nbs (4):"";
                $fl             = ( $x === $limiteRC    )?"Final ":"";
                                                
                $this->table->add_row(array_merge(array($espacio.$leyendaRC, ' '),$rcValue));
                if( $x>5 & $x< $limiteX)
                {}
                else
                { $this->table->add_row(array_merge(array( array('data'=>$fl.$leyendas[3], 'class'=>'adprice'), array('data'=>' ', 'class'=>'adprice')), $adPrice)); }
            }            
                
            unset($rcValue);
            unset($adPrice);
            unset($weightPor);
            if( $x !== count($rc))
             { unset($adPriceValue); }
        }                
        
        /**********  RELEVANT CHARACTERISTICS OF THE COMPARABLE FIN*********************/
        $y           = -1;
        $totalIVM    = 0;
        $totalIVFT   = 0;
        $weightTotal = 0;
         
        foreach ($comparables as $cAA1)
        {   $y++;
            $weightTotal   = $weightTotal + $cAA1['weight'];
            $weight     [] = array('data'=>$this->helpercontroller->weight($cAA1['id_comp'],$cAA1['weight'],$totalPrice[$y],$ft,$weightPorTot,$y), 'class'=>'linead');
            $fc            = $totalPrice[$y] * ($cAA1['weight'] / 100);
            $fcFT          = ($tipo === "AA2")?($totalPrice[$y] / $ft) * $fc:($fc/$ft)*12;
            $composition[] = $this->helpercontroller->composition($cAA1['id_comp'],$fc,$fcFT,($tipo==="AA4")?"usd/ft2/year":"usd/ft2");
            $totalIVM      = $totalIVM + $fc;





            $totalIVFT     = ($tipo==="AA2")?$totalIVM / $ft:($totalIVM / $ft)*12;

           // $this -> md_inmuebles -> update_inmuebles( $id_in, array("indicated_value_crs" => number_format($totalIVM, 2, '.', ',')));
        
        }


        // Task ticker 39
        //   $this -> md_inmuebles -> update_inmuebles( $id_in, array("indicated_value_crs" => number_format($totalIVM, 2, '.', ',')));
      
        
        $cel1 = array('data'=>$leyendas[4]."<lable id='weightTot' class='".(($weightTotal===100)?"msjOk":"msjErr")."'>".(($weightTotal===100)?"<img src=\"".base_url()."images/check.png\">100%":"<img src=\"".base_url()."images/close.png\"> Not 100%")."</label>", 'class'=>'linead');
        $cel2 = array('data'=>' '         , 'class'=>'linead');
                
        $this->table->add_row(array_merge(array($cel1, $cel2),$weight));
        $this->table->add_row(array_merge(array($leyendas[5].br(1).$leyendas[6], ' '),$composition));
        
        $tabla .= $this->table->generate();
        $year   = ($tipo==="AA4")?"/year":"";
        $ft2AA4 = ($tipo === "AA4")?" usd/sqf/year":" usd/ft2";
        $fc2AA4 = ($tipo === "AA4")?" usd/sqm/mo." :" usd/m2";
        $this -> table -> clear();
        $this -> table -> set_template(array('table_open' => "<table cellspacing='0' width='100%' id='grid$tipo'>" ));        
        $this -> table -> set_heading(array("Indicated value per m2","Indicated value per ft2", "Maximum","Minimum","Average","Std. Deviation","Variation Coeff."));
        $this->session->userdata['totalIVM'] = number_format($totalIVFT, 2, '.', ',');
        $this->md_inmuebles->update_inmuebles( $id_in, array("indicated_value_crs" =>  $totalIVM));

        

        $this -> table-> add_row("<label id='indicatedValuem2'>".$totalIVM.$ft2AA4."</label>".form_input(array("type"=>"hidden","name"=>"indicated_value","id"=>"indicated_value","value"=>$totalIVM))."<label id='indicatedm2'>$".number_format($totalIVM, 2, '.', ',').$fc2AA4."</label>","<label id='indicatedValueft2'>".$totalIVFT."</label><label id='indicatedft2'>$".number_format($totalIVFT, 2, '.', ',')." usd/ft2".$year."</label>", '$'.number_format($maxP, 2, '.', ','),'$'.number_format($minP, 2, '.', ','),'$'.number_format($avP, 2, '.', ','),'$'.number_format($std, 2, '.', ','), array('data'=>number_format($variationCoeff, 2, '.', ',')."%", 'class'=>$semaforoVC));
        $tabla .= br(2).$this->table->generate().br(2).""
               . "<div class='row'><a class='button' href='javascript:salvarAA2(baseURL,\"$id_in\",\"$tipoComp\");'>Save Section</a>"
                . "<span id='confirm".$tipo."1' class='msjconfirm'></span></section></div>";
        
        return $tabla;
        
        }  catch (Exception $e) {echo 'gridAnexo Excepción: ',  $e, "\n";}                        
    }
    
    
    public function updateTitlePhotoAX()
    {
     try{
         $vs = $this->validaSesion(TRUE,FALSE);        
        if( isset($vs['session']))
        {   echo json_encode ($vs); 
            exit(0);
        }
         $id       = $this -> input -> post('id');
     $titulo   = $this -> input -> post('titulo');
         
         $r = $this -> md_gallery -> updateTitulo($id,$titulo);
         
         echo json_encode (array("result" => $r));
         
        }  catch (Exception $e) {echo 'agregaDefAX Excepción: ',  $e, "\n";}
    }
             
    public function agregaDefAX()
    {
     try{
         $vs = $this->validaSesion(TRUE,FALSE);        
        if( isset($vs['session']))
        {   echo json_encode ($vs); 
            exit(0);
        }
         $def    = $this -> input -> post('def');
     $desc   = $this -> input -> post('desc');
         
         $id_def = $this -> md_inmuebles -> insertaNuevaDef($def,$desc);
         
         echo json_encode (array("id_def" => $id_def,"tipo" => $this->myData['tipo']));
         
        }  catch (Exception $e) {echo 'agregaDefAX Excepción: ',  $e, "\n";}
    }
    
    public function borraDefinicionAX()
    {
     try{
         $vs = $this->validaSesion(TRUE,FALSE);        
        if( isset($vs['session']))
        {   echo json_encode ($vs); 
            exit(0);
        }
         $id_def    = $this -> input -> post('id_def');
         
         $this -> md_inmuebles -> borraDef($id_def);
         
         echo json_encode (array("id_def" => $id_def));
         
        }  catch (Exception $e) {echo 'borraDefinicionAX Excepción: ',  $e, "\n";}
    }
    
    public function salvarAA2AX()
    {
    try{
        $vs = $this->validaSesion(TRUE,FALSE);        
        if( isset($vs['session']))
        {   echo json_encode ($vs); 
            exit(0);
        }
        $id_in           = $this -> input -> post('id_in');
    $compId          = $this -> input -> post('compId');
        $compValue       = $this -> input -> post('compValue');
        $tipoComp        = $this -> input -> post('tipoComp');
        $indicated_value = $this -> input -> post('indicated_value');
        
        $this -> md_inmuebles -> borraAnexoInmueble($id_in,$tipoComp,'annexs_a2_in');
        $this -> md_inmuebles -> update_inmuebles( $id_in, array("indicated_value_$tipoComp"=>$indicated_value));
        
        $numComp= sizeof($compId);        
        for ($i=0; $i<$numComp; $i++) 
         { $this -> md_inmuebles -> insertaSeccionInmueble(array("id_in"=>$id_in,"id_comp"=>$compId[$i],"tipo_comp"=>$tipoComp,"weight"=>$compValue[$i]),'annexs_a2_in'); }
        
         echo json_encode (array("id_in" => $id_in,"compId"=>$compId,"compValue"=>$compValue));
         
     }  catch (Exception $e) {echo 'borraDefinicionAX Excepción: ',  $e, "\n";}
    }
    
    public function generaConsecutivoAX()
    {
    try{
        $vs = $this->validaSesion(TRUE,FALSE);        
        if( isset($vs['session']))
        {   echo json_encode ($vs); 
            exit(0);
        }
        $origen_name    = $this -> input -> post('origen_name'); //"35";//
        $concepto_name  = $this -> input -> post('concepto_name');//"IND";//
    $clave_ano_name = $this -> input -> post('clave_ano_name');//"16";//
        $id_in          = $this -> input -> post('id_in');//"70778";//

        $consecutivo_name = $this->utils->generaConsecutivo($this->md_inmuebles->traeConsecutivoInmueble($origen_name, $concepto_name, $clave_ano_name,$id_in));
        
        echo json_encode ($consecutivo_name);

     }  catch (Exception $e) {echo 'generaConsecutivoAX Excepción: ',  $e, "\n";}
    }
    private function creaTablasComparables()
        {             
             $this->table->clear();
             $this->table->set_template(array('table_open' => '<table cellspacing="0" cellpadding="0" width="100%" id="compTableCRL">' ));
             $this->table->set_heading( array('','','Type of property','Location', 'Construction m2','Monthly Rent MXN','Monthly Rent (MXN/ M2)','Closing/Listing Date','Created By', 'Actions'));
             $this->table->add_row(' ');
             $t1 = array('CRL'=>$this->table->generate());             
             
             $this->table->clear();
             $this->table->set_template(array('table_open' => '<table cellspacing="0" cellpadding="0" width="100%" id="compTableCLS">' ));
             $this->table->set_heading( array('','','Type of property','Location', 'Land m2','Price MXN','Unit Sale Price (MXN/ M2)','Closing/Listing Date','Created By', 'Actions'));
             $this->table->add_row(' ');
             $t2 = array('CLS'=>$this->table->generate());             
             
             $this->table->clear();
             $this->table->set_template(array('table_open' => '<table cellspacing="0" cellpadding="0" width="100%" id="compTableCRS">' ));
             $this->table->set_heading( array('','','Type of property','Location', 'Construction m2','Price MXN','Unit Sale Price (MXN/ M2)','Closing/Listing Date','Created By', 'Actions'));
             $this->table->add_row(' ');
             $t3 = array('CRS'=>$this->table->generate());
             
             return array($t1,$t2,$t3);
        }
    
}//cONTROLLER
