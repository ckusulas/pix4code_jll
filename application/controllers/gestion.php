<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gestion extends CI_Controller 
{	
        public $galeria = "gallery/";
	
        public function __construct()
        {
            parent::__construct();

            $this->load->database('jll_hermes_con');
            $this->load->model('md_usuario');
            $this->load->model('md_inmuebles');
            $this->load->model('md_comparables');
            $this->load->library('session');        
            $this->load->library('table');
            $this->load->library('Utils');
            $this->load->helper('array');
        }
	
        public function keepAlive()
        {
            $this->session->sess_update();        
            $this->session->userdata['last_activity'] = time();
            $datos_sesion = $this -> session -> userdata('datos_sesion');
            $this->session->set_userdata('datos_sesion', $datos_sesion);

            $this->user = $this -> session -> userdata('datos_sesion');
            if( $this->user == NULL )
            { echo 'murio'; }
            echo "sesioUpdate".$this->session->sess_expiration.  br(1);
            echo "time".time().  br(1);
            echo "last_activity".$this->session->userdata('last_activity').  br(1);
            echo "REsta".(time() - $this->session->userdata('last_activity')).  br(1);
            
            echo json_encode( $this->session->sess_expiration );
        }

        public function retrieveTimeLeft()
        {            
            $sesioUpdate = $this->session->sess_expiration;
            $this->user = $this -> session -> userdata('datos_sesion');		
												
	  if( $this->user == NULL )
          { echo 'murio'; }
          //  echo "sesioUpdate".$sesioUpdate.  br(1);
          //  echo "time".time().  br(1);
          //  echo "last_activity".$this->session->userdata('last_activity').  br(1);
            
          echo json_encode($sesioUpdate - (time() - $this->session->userdata('last_activity')) );
        }

        public function timeout()
        {
                $data['titulos']  = array("navegador" => "JLL - HERMES", 
                                          "ventana"   => "HERMES",
                                          "frase"     => "\"Hacer de lo simple algo complicado es com&uacute;n; hacer de lo complicado algo simple, incre&iacute;blemente simple, es creatividad\" <br>- Charles Mingus",
                                          "titulo"    => "Ingreso al Portal HERMES");	
                $data['usuario']  = "";
                $data['iconuser'] = "";
                $data['sesion']   = "Su sesión ha expirado";                            
                $data['foto']     = "";
                $this->session->sess_destroy();
                $this->session->_sess_gc();

                $this->load->view('login',$data);

        }

        public function autenticar($pwd, $usuario)
	{				
		$datos_sesion = $this->md_usuario->validaUsuario($usuario,$pwd);
						
		if ($datos_sesion==FALSE)							
			{
				$this->form_validation->set_message('autenticar', 'Usuario y/o Contraseña incorrectos');
				 return FALSE;
			}	
		else
			{
                                $this->session->sess_create();
				$this->session->set_userdata('datos_sesion', $datos_sesion);
                                $this->session->userdata['last_activity'] = time();
				$this->user = $this->session->userdata('datos_sesion');
                                
				return TRUE;
			}
	}
            
	public function salir()
        {
                $data['titulos']  = array("navegador" => "JLL - HERMES", 
                                          "ventana"   => "HERMES",
                                          "frase"     => "\"Hacer de lo simple algo complicado es com&uacute;n; hacer de lo complicado algo simple, incre&iacute;blemente simple, es creatividad\" <br>- Charles Mingus",
                                          "titulo"    => "Ingreso al Portal HERMES");	
                $data['usuario']  = "";
                $data['iconuser'] = "";
                $data['sesion']   = "";
                $data['foto']     = "";
                
                $this->session->sess_destroy();
                $this->session->_sess_gc();
                
                $this->load->view('login',$data);
        }
        
	public function download($dir,$id_in,$filename)
	{
		$this->load->helper('download');			

		$data = file_get_contents($dir."/".$id_in."/".$filename);

		force_download($filename, $data);		
	}
        
        public function downloadExcel($dir,$filename)
	{
		$this->load->helper('download');			

		$data = file_get_contents($dir."/".$filename);

		force_download($filename, $data);		
	}
	
	
	
	public function index()
	{
		$data['titulos']  = array("navegador" => "JLL - HERMES", 
                                          "ventana"   => "HERMES",
                                          "frase"     => "\"Hacer de lo simple algo complicado es com&uacute;n; hacer de lo complicado algo simple, incre&iacute;blemente simple, es creatividad\" <br>- Charles Mingus",
                                          "titulo"    => "Ingreso al Portal HERMES");
                $data['usuario']  = "";
                $data['iconuser'] = "";
                $data['sesion']   = "";
                $data['foto']     = "";
                
		$this->load->view('login',$data);	
	}
	
        	
	public function login()
	{		
            $this->load->library('form_validation');
						
            $this->form_validation->set_rules('usuario', 
                                              'Usuario', 
                                              'required|trim|min_length[4]');
                
            $this->form_validation->set_rules('pwd',
                                              'Contraseña', 
                                              'required|alpha_dash|trim|min_length[3]|'.
                                              'callback_autenticar['.$this->input->post('usuario').']');        
 
            $this->form_validation->set_message('required', 'Campo %s requerido');
            $this->form_validation->set_message('alpha_dash', 'El campo %s tiene caractres no permitidos');
            $this->form_validation->set_message('min_length', 'El Campo %s debe tener un minimo de %d Caracteres');
	   
	    if ($this->form_validation->run() == FALSE)        
               {  $this->index(); }
            else
                { redirect(base_url('inmueble/'));  }
	}                        
	   
        public function	validaCampoDuplicadoAX()
        {                                
                $correo = $this -> input -> post('correo');
                $repetido = $this -> md_usuario -> validaDuplicidad($correo);
                
               
                if($repetido == FALSE)
                    { $duplicado = array("duplicado"  => FALSE, "nombre"=>"", "correo"=>"");	}
                else	
                    { $duplicado = array("duplicado"  => TRUE,                                                           
                                         "nombre"     => $repetido["0"]["nombre"],
                                         "correo"     => $repetido["0"]["correo"]
                                          );                     
                    }	
                echo json_encode ($duplicado);
       }
       
       
       public function	traeDatosValuadorAX()
        {       
                $vs = $this->validaSesion(TRUE,FALSE);
             if( isset($vs['session']))
             {   echo json_encode ($vs); 
                 exit(0);
             }
                $correo = $this -> input -> post('correo');
                $val = $this -> md_usuario -> traeDetalleUsr($correo);
                                               
                echo json_encode ($val);
       }
       
       
       public function	traeFormatoRedondeadoAX()
        {       
             $vs = $this->validaSesion(TRUE,FALSE);
             if( isset($vs['session']))
             {   echo json_encode ($vs); 
                 exit(0);
             }
             $num          = $this -> input -> post('cantidad');
             $porcentaje   = $this -> input -> post('tipo');
             if($porcentaje == 1)
                { $numRedondeda = round($num, 0); }
             else                 
                { $numRedondeda = number_format($this->utils->redondearHermes($num), 2, '.', ','); }
                                               
              echo json_encode ($numRedondeda);
       }
              
       
       public function	updateERAX()
        {
           $vs = $this->validaSesion(TRUE,FALSE);
            if( isset($vs['session']))
            {   echo json_encode ($vs); 
                exit(0);
            }
           $this->load->helper('date');
           $hoy = standard_date('DATE_W3C', time());
           $tipo= $this -> input -> post('tipo');
            
           $this -> md_inmuebles -> updateER($this -> input -> post('er'),$hoy);
           if($tipo=="in")               
                { $val=$this -> md_inmuebles -> updateERIn($this -> input -> post('er'), $hoy, $this -> input -> post('id')); }
           else
                { $val=$this -> md_comparables -> updateERComp($this -> input -> post('er'), $hoy, $this -> input -> post('id')); }
           
           $df     = $this->md_inmuebles->traeDatosFinancieros();
           $ftyear = $df[0]['ftyear'];
           
           echo json_encode (array("date"=>date("F jS Y g:i a"),"ftyear"=>$ftyear));
        }
        
    public function recuperarPwdAX()
    {
            $this->load->model('md_usuario');	

            $exiteCuenta = $this -> md_usuario -> verificaCuenta($this -> input -> post('correo'));

            if($exiteCuenta == FALSE)
                    $existe = array("existe"  => FALSE);	
            else
            {	
                    $existe = array("existe"  => TRUE);

                    $this ->mandaPwd($exiteCuenta[0]['correo'],
                                     $exiteCuenta[0]['nombre'],
                                     $exiteCuenta[0]['apellidos'],
                                     $exiteCuenta[0]['pwd']);

            }

            echo json_encode ($existe);

    }


    private function mandaPwd($correo,$nombre,$apellido,$pwd)
            {
             try{

                    $to  = $correo;
                    $this->mandarCorreoPWD($to,$nombre,$apellido,$pwd);		

             } catch (Exception $e) {echo 'manda mandaPwda Excepción: ',  $e, "\n";}		
            }
	
	
    private function mandarCorreoPWD($to,$nombre,$apellido,$pwd)
            {	
                    $this->load->library('email');				

                    $config['protocol']  = "smtp";
                    $config['smtp_host'] = "mail.valuationsmexico.com";
                    $config['smtp_port'] = "25";
                    $config['smtp_user'] = "hermes@valuationsmexico.com";
                    $config['smtp_pass'] = "hermes$2016";
                    $config['charset']   = "UTF8";
                    $config['mailtype']  = "html";		
                    $config['wordwrap']  = TRUE;

                    $this->email->initialize($config);

                    $fromMail = 'hermes@valuationsmexico.com';
                    $fromName = 'HERMES Valuations México';
                    $subject  = "Recuperar contraseña HERMES";	
                    $message  = '
                    <html>
                    <head>
                    <title>HERMES Valuations México</title>
                    </head>
                    <body>
                    <hr>
                    <p align="left" style="font-family:\'arial black\',\'avant garde\';font-size:12px"><strong>
                    Estimado '.$nombre.' '.$apellido.',</strong></p>
                    <p align="justify">
                    Hemos recibido la petición de recuperar su contraseña, para poder ingresar al portal de HERMES Valuations Mexico, 
                    a continuación le proporcionamos los siguientes datos de acceso:</p>
                    <p align="center">
                    <strong>Usuario: </strong>'.$to.'<br>
                    <strong>Contraseña:</strong>'.$pwd.'<br><br><br>
                    <a href="'.base_url().'" target="_blank"><strong>
                    Ingresar</strong></a><br>
                    </p><br>
                    <p align="justify"><strong>Saludos Cordiales</strong></p>
                    <hr>
                    <p>
                        <span style="font-family:\'arial black\',\'avant garde\';font-size:large;color:#888888">
                        <strong> HERMES</strong></span>
                    </p>                    
                    </body>
                    </html>
                    ';		

                    $this->email->from($fromMail, $fromName);
                    $this->email->to($to); 		
                    $this->email->subject($subject);
                    $this->email->message($message);

                    $r = $this->email->send();
//                    echo $this->email->print_debugger();		

                    return $r;
            }
            
            
}//Controller

