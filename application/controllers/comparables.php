<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Comparables extends CI_Controller 
{	
        public $registros_x_pagina = 10;
        public $galeria            = "gallery/";
        public $export             = "exp_comparables/";
	
        public function __construct()
        {
            parent::__construct();

            $this->load->database('jll_hermes_con');
            $this->load->model('md_comparables');
            $this->load->model('md_catalogo');
            $this->load->library('session');        
            $this->load->library('table');
            $this->load->library('Utils');
            $this->load->library('Excel');
            $this->load->helper('array');
        }
					
	public function consulta($tipoComp='CLS')
        {
            $this->validaSesion(NULL,FALSE);            
            $this->index($tipoComp);
        }
	
	public function index($tipoComp='CLS',$msj='')
	{
            $this->validaSesion(NULL,FALSE);
            $data['inSel'] = "";
            $data['usSel'] = "";
            $data['cmSel'] = "class=\"selected\"";
            $data['titulos']  = array("navegador" => "JLL - HERMES", 
                                      "ventana"   => "HERMES",
                                      "frase"     => "\"Hacer de lo simple algo complicado es com&uacute;n; hacer de lo complicado algo simple, incre&iacute;blemente simple, es creatividad\" <br>- Charles Mingus",
                                      "titulo"    => "Bandeja de Comparables".$msj);

            $data['usuario']  = $this->myData['usuario'];
            $data['iconuser'] = $this->myData['iconuser'];
            $data['foto']     = $this->myData['foto'];            
            
             if($tipoComp=="CLS"){                    
                    $c1 = "Land m2";
                    $c2 = "Price MXN";
                    $c3 = "Unit Value (MXN/ M2)";                    
                } elseif ($tipoComp=="CRL") {                  
                    $c1 = "Construction m2";
                    $c2 = "Monthly Rent MXN";
                    $c3 = "Monthly Rent";
                }  else {                    
                    $c1 = "Construction m2";
                    $c2 = "Price MXN";
                    $c3 = "Unit Sale Price";                    
                }

            $this   -> table -> clear();		                
            $this   -> table -> set_template(array('table_open' => '<table cellspacing="0" width="100%" id="compTable">' ));                
            $this   -> table -> set_heading (array('','','Type of property','Location', $c1,$c2,$c3,'Closing/Listing Date','Created By', 'Actions'));
            $this   -> table -> add_row(' ');     
            $data['gridComp']        = $this->table->generate();

            $data['registrosPagina'] = $this->registros_x_pagina;
            $data['tipoComp']        = $tipoComp;
            $tipoComRadio = "";
            foreach($this -> md_catalogo -> poblarRadioButtonComp($tipoComp) as $rd)
                { $tipoComRadio = $tipoComRadio."<label class='radio'><input type='radio' name='tipoCompRadio' value='".$rd['value']."' ".$rd['checked']."><i></i>".$rd['label']."</label>"; }
            $data['tipoComRadio'] = $tipoComRadio;

            $this->load->view('grid_comparables',$data);
	}
	
        public function borra($id_comp=null,$tipoComp='CLS')
        {
         try{
                $this->validaSesion(NULL,FALSE);
                
                $this->md_comparables->deleteComparable($id_comp);
                $filePath = $this->galeriaComp.$id_comp;
                                
                system('rm -rf ' . escapeshellarg($filePath), $retval);
                
                    
                $this->index($tipoComp,'Comparable borrado'.$retval);    
                
            } catch (Exception $e) {echo ' borra Excepción: ',  $e, "\n";}		
        }       
        
        public function forma($tipoComp="",$accion=null,$id_comp=null)
        {
            try{
                $this->validaSesion(NULL,FALSE);
                $data['inSel'] = "";
                $data['usSel'] = "";
                $data['cmSel'] = "class=\"selected\"";
                $this->load->helper('date');                
                            
                $data['usuario']     = $this->myData['usuario'];
                $data['iconuser']    = $this->myData['iconuser'];
                $data['foto']        = $this->myData['foto'];
                $data['dirFoto']     = $this->galeriaComp;
                $data['tipo']        = $tipoComp;
                $rc8                 = $this->utils->optionsRC8($tipoComp);                

                if($tipoComp=="CLS")
                {
                    $data['titulo']     = "A.1.- Market Research Sales Schedules";                    
                    $data['subtitulo']  = "LAND FOR SALE";
                    $data['c1']         = "Area of Comparable";
                    $data['c2']         = "Asking Price";
                    $data['c3']         = "Unit Value";
                    $data['c4']         = "Time on market";
                    $data['c5']         = "Listing / Sale";
                } elseif ($tipoComp=="CRL") { 
                    $data['titulo']     = "A.3.- Market Research Lease Schedules";
                    $data['subtitulo']  = "PROPERTY FOR LEASE";
                    $data['c1']         = "Comparable area";
                    $data['c2']         = "Monthly Rent MXN";
                    $data['c3']         = "Monthly Rent ";
                    $data['c4']         = "Closing / Listing Date";
                    $data['c5']         = "Listing / Real Rent";
                }  else {
                    $data['titulo']     = "A.5.- MARKET RESEARCH - FOR SALE";
                    $data['subtitulo']  = "PROPERTY FOR SALE";
                    $data['c1']         = "Comparable area";
                    $data['c2']         = "Total Sale Price MXN";
                    $data['c3']         = "Unit Sale Price ";
                    $data['c4']         = "Lease Date";
                    $data['c5']         = "Listing / Sale";
                }                                
                
                if($accion=="N")
                {                    
                    $data['id_comp']    = substr(''.now(), -4);
                    $data['accion']     = $accion;
                    $data['comparable'] =  array(array("type_property"=>($tipoComp=="CLS")?"Land":null,"calle"=>null,"num"=>null,"col"=>null,"mun"=>null,"edo"=>null, "cp"=>null,"source_information"=>null,"phone"=>null,"land_m2"=>($tipoComp=="CLS")?0:null,"land_ft2"=>($tipoComp=="CLS")?0:null,"price_mx"=>null,"price_usd"=>null,"unit_value_mx"=>null,"unit_value_usd"=>null,"construction"=>null,"time_market"=>null,"latitud"=>null,"longitud"=>null,"comments"=>null,"date_exchange_rate"=>null,"rc8"=>0,"lada"=>null,"closing_listing_date"=>null));
                    $data['fotoIni']    = "jll_in.jpg";
                    $data['fotoClose']  = "";                    
                    $titulo             = "Agregar nuevo comparable";
                    $rc8Dropdown         = '<label class="select"><i class="icon-append"></i>'.form_dropdown('rc8', $rc8, 0,'id="rc8" class="validate[custom[requiredInFunction]]"').'</label>';
                }
                else
                {
                    $data['id_comp']    = $id_comp;
                    $data['accion']     = "E";
                    $data['comparable'] = $this->md_comparables->traeComparable($id_comp);                                        
                    $data['fotoIni']    = $data['comparable'][0]['foto'];
                    $data['fotoClose']  = "<br><br><img class='imgCP' title'Borrar Foto Comparable' name='".$id_comp."'  id='".$data['comparable'][0]['foto']."' src='".base_url()."images/close.png'><br>";                    
                    $titulo             = "Modificar comparable";
                    $rc8Dropdown         = '<label class="select"><i class="icon-append"></i>'.form_dropdown('rc8', $rc8, $data['comparable'][0]['rc8'],'id="rc8" class="validate[custom[requiredInFunction]]"').'</label>';
                }                
                $data['rc8Dropdown'] = $rc8Dropdown;
                $data['titulos'] = array("navegador" => "JLL PROYECTO HERMES",								 
                                         "ventana"   => "HERMES",
                                         "frase"     => "\"Hacer de lo simple algo complicado es com&uacute;n; hacer de lo complicado algo simple, incre&iacute;blemente simple, es creatividad\" <br>- Charles Mingus",
                                         "titulo"    => $titulo);
                
                $df               = $this->md_comparables->traeDatosFinancierosComp($data['id_comp']);
                $data['d_er']     = '<br><label style="font-size:10px;">'.$df['date_exchange_rate'].'</label>';
                $data['er']       = $df['exchange_rate'];
                $data['ft']       = $df['ft'];                
                $data['ftyear']   = $df['ftyear'];
                $data["updateER"] = '<img title="Update Exchange Rate" id="updateER" width="15" height="15" src="'.base_url().'images/edit.png">';
                
                $this->load->view('form_comparables',$data);
                
                } catch (Exception $e) {echo ' nuevo Excepción: ',  $e, "\n";}		
        }       
        
        public function guardar()
        {
         try{
            $this->validaSesion(NULL,FALSE);//sesion error reportado por usuario
            $this->load->helper('date');               

            $accion   = $this -> input -> post('accion');
            $tipoComp = $this -> input -> post('tipocomp');
            $dataComp = array("id_comp"            => $this -> input -> post('id_comp'),
                              "type_property"      => $this -> input -> post('type_property'),
                              "calle"              => $this -> input -> post('calle'),
                              "num"                => $this -> input -> post('num'),
                              "col"                => $this -> input -> post('col'),
                              "mun"                => $this -> input -> post('mun'),
                              "edo"                => $this -> input -> post('edo'),
                              "cp"                 => $this -> input -> post('cp'),
                              "latitud"            => $this -> input -> post('latitud'),
                              "longitud"           => $this -> input -> post('longitud'),                    
                              "source_information" => $this -> input -> post('source_information'),
                              "phone"              => $this -> input -> post('phone'),
                              "lada"               => $this -> input -> post('lada'),
                              "land_m2"            => $this -> input -> post('land_m2'),
                              "land_ft2"           => $this -> input -> post('land_ft2'),
                              "price_mx"           => $this -> input -> post('price_mx'),                              
                              "unit_value_mx"      => $this -> input -> post('unit_value_mx'),
                              "unit_value_usd"     => $this -> input -> post('unit_value_usd'),
                              "construction"       => $this -> input -> post('construction'),
                              "time_market"        => $this -> input -> post('time_market'),
                              "comments"           => $this -> input -> post('comments'),
                              "rc8"                => $this -> input -> post('rc8'),
                              "closing_listing_date"=>$this->utils->hermesDateFormat($this -> input -> post('closing_listing_date')),
                              "foto"               => $this -> input -> post('foto'),
                              "tipo"               => $tipoComp,
                              "exchange_rate"      => $this -> input -> post('exchange_rate'),
                              "date_exchange_rate" => standard_date('DATE_W3C', time()),
                              "fecha_alta"         => standard_date('DATE_W3C', time()),
                              "creado_por"         => element('correo', $this->user['0'])
                              );

            if($accion == "E")// Para actualizar el pedido borra todas las tablas auxiliriares para insertar la nueva 
                {                        
                    unset($dataComp['fecha_alta']);
                    unset($dataComp['id_comp']);
                    $dataComp["fecha_modificacion"] =standard_date('DATE_W3C', time());
                    $this->md_comparables->updateComparable($dataComp,$this -> input -> post('id_comp'));                        
                }
            else
                 { $this->md_comparables->insertComparable($dataComp); }                                                

            $this->index($tipoComp,':<em><u>registro agregado exitósamente</u></em>');
                
        } catch (Exception $e) {echo ' guardar Excepción: ',  $e, "\n";}		
    }

   public function paginarAX()
    {
     try{
           $vs = $this->validaSesion(TRUE,FALSE);
            if( isset($vs['session']))
            {   echo json_encode ($vs); 
                exit(0);
            }
          $pagina   = $this->input->post('pagina');
          $tipoComp = $this->input->post('tipoComp');
          $f1       = $this->input->post('f1');
          $f2Ini    = $this->input->post('f2Ini');
          $f2Fin    = $this->input->post('f2Fin');
          $f3       = $this->input->post('f3');
          $f4       = $this->input->post('f4');//$this->utils->validaDateFormat($this->input->post('f4'));
          $f5       = $this->input->post('f5');//$this->utils->validaDateFormat($this->input->post('f5'));
          $f6Ini    = $this->input->post('f6Ini');
          $f6Fin    = $this->input->post('f6Fin');
          $id_in    = $this->input->post('id_in');
          $rp       = $this->input->post('registrosPagina');
         
         $param = array("registros_x_pagina"=>$rp,"pagina"=>$pagina,"tipoComp"=>$tipoComp,"f1"=>$f1,"f2Ini"=>$f2Ini,"f2Fin"=>$f2Fin,"f3"=>$f3,"f4"=>$f4,"f5"=>$f5,"f6Ini"=>$f6Ini,"f6Fin"=>$f6Fin,"dirFoto"=>$this->galeriaComp,"id_in"=>$id_in);

         $comparables = $this -> md_comparables -> traeComparablesFiltros($param);

        echo json_encode ($comparables);                          

     } catch (Exception $e) {echo ' paginarAX Excepción: ',  $e, "\n";}		
    }
        
      public function agregaImagenCompAX()
        {
         try{
             $vs = $this->validaSesion(TRUE,FALSE);
            if( isset($vs['session']))
            {   echo json_encode ($vs); 
                exit(0);
            }
            if(isset($_FILES["myfile"]))
            {
                    $ret = array();		
                    $error =$_FILES["myfile"]["error"];		
                    if(!is_array($_FILES["myfile"]["name"])) //single file
                    {                            
                            $fileName =  $this->utils->generaNombreImagen($_FILES["myfile"]["name"]);
                            move_uploaded_file($_FILES["myfile"]["tmp_name"],$this->galeriaComp.$fileName);

                            $ret[]= $fileName;
                    }
                    else  //Multiple files, file[]
                    {
                      $fileCount = count($_FILES["myfile"]["name"]);
                      for($i=0; $i < $fileCount; $i++)
                      {
                            $fileName =  $this->utils->generaNombreImagen($_FILES["myfile"]["name"][$i]);
                            move_uploaded_file($_FILES["myfile"]["tmp_name"][$i],$this->galeriaComp.$fileName);
                            $ret[]= $fileName;
                      }

                    }
                    echo json_encode($ret);                
             }
          } catch (Exception $e) {echo ' agregaImagenAX Excepción: ',  $e, "\n";}         
        }


        public function	borraImagenCompAX()
        {

                if(isset($_POST["op"]) && $_POST["op"] == "delete" && isset($_POST['name']))
                {
                        $fileName =$_POST['name'];
                        $filePath = $this->galeriaComp.$fileName;
                        if (file_exists($filePath)) 		
                                unlink($filePath);

                        echo "Deleted File ".$fileName."<br>";
                }
        }

        public function	renombraImagenCompAX()
        {	
        try{	$vs = $this->validaSesion(TRUE,FALSE);
                if( isset($vs['session']))
                {   echo json_encode ($vs); 
                    exit(0);
                }
                $this->load->helper('date');
                
                $errores       = array();
                $errorTxt      = "";        
                $extension     = $this -> input -> post('extension');
                $nombreArchivo = $this -> input -> post('nombreArchivo');
                $id_comp       = intval($this -> input -> post('id_comp'));
                $idImagen      = "comp_".intval(substr(now(), -5));
                $nombreArchivo = $this->galeriaComp.$nombreArchivo.".".$extension;
                $nombreHermes  = $this->galeriaComp.$id_comp.'/'.$idImagen.".".$extension;

                if (file_exists($this->galeriaComp.$id_comp) == FALSE)
                    {mkdir($this->galeriaComp.$id_comp, 0777);}
               
                $cmd = "$nombreArchivo -resize 356x252! "; 
                exec("convert $cmd $nombreHermes ",$errores);
                unlink($nombreArchivo);

                if ( !empty($errores) )
                    { $errorTxt = "<br />Hubo errores al trabajar conconversion:<br />".print_r($errores); }       

                echo json_encode (array("nombreHermes" => $nombreHermes,"hImg" => $idImagen.".".$extension,"id_comp"=>$id_comp,"erroresd" => $errorTxt));

                } catch (Exception $e) {echo 'renombraImagenAX Excepción: ',  $e, "\n";}	
        }
        
        public function	borraImagenCompCargadaAX()
        {	
        try{    $vs = $this->validaSesion(TRUE,FALSE);
                if( isset($vs['session']))
                {   echo json_encode ($vs); 
                    exit(0);
                }
                $this->load->model('md_gallery');
                $extension 	   = $this -> input -> post('extension');
                $nombreArchivo     = $this -> input -> post('nombreArchivo');
                $id_comp           = intval($this -> input -> post('id_comp'));                

                $nombreArchivo = $nombreArchivo.".".$extension;
                $filePath      = $this->galeriaComp.$id_comp.'/'.$nombreArchivo;
                $result        = false;

                if (file_exists($filePath)) 		
                    { 
                        $result = unlink($filePath); 
                        $this -> md_gallery ->updateImagenComp("jll_in.jpg",$id_comp);
                    }

                echo json_encode (array("result" => $result,"dirFoto"=>$this->galeriaComp));

                } catch (Exception $e) {echo 'renombraAdjuntoAX Excepción: ',  $e, "\n";}	
        }

	
        public function exportaExcelAX()
        {
        try{$vs = $this->validaSesion(TRUE,FALSE);        
        if( isset($vs['session']))
        {   echo json_encode ($vs); 
            exit(0);
        }
        $this->load->helper('date');       
         
        $tipo_c   = $this -> input -> post('tipo_c'); //"CLS" "CRL" "CRS"             
        $f1       = $this->input->post('f1');
        $f2Ini    = $this->input->post('f2Ini');
        $f2Fin    = $this->input->post('f2Fin');
        $f3       = $this->input->post('f3');
        $f4       = $this->input->post('f4');//$this->utils->validaDateFormat($this->input->post('f4'));
        $f5       = $this->input->post('f5');//$this->utils->validaDateFormat($this->input->post('f5'));
        $f6Ini    = $this->input->post('f6Ini');
        $f6Fin    = $this->input->post('f6Fin');
          
         
        $param = array("tipo_c"=>$tipo_c,"f1"=>$f1,"f2Ini"=>$f2Ini,"f2Fin"=>$f2Fin,"f3"=>$f3,"f4"=>$f4,"f5"=>$f5,"f6Ini"=>$f6Ini,"f6Fin"=>$f6Fin);
        $expFile  = '';
        $infoFile = array("name" => "Hermes_Comparables_$tipo_c.xlsx","tipo_c" => $tipo_c, "dir" =>  $this->export, "galeria" =>  $this->galeria, "galeriaComp" =>  $this->galeriaComp,"dc" =>$this->md_comparables->traeDatosConversionComp() );
        
        $comparables = $this ->md_comparables->traeComparablesExportExcel($param);
                          
        if (file_exists($infoFile['dir']) == FALSE)
            { mkdir($infoFile['dir'], 0777); }

        $this->utils->borraArchivoPorExtension($infoFile['dir'],"E");                   

        if(empty($comparables))
          { $expFile = "No information";}  
        else    
          { $expFile = $this->utils->exportaExcel($infoFile,$comparables); }
           
        echo json_encode (array("expFile" => $expFile, "dir" => $infoFile['dir'] ));

       } catch (Exception $e) {echo 'ERROR: ',  $e, "\n";}	
    }
        
}//Controller

