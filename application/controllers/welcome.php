<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

 
	
	public function index()
	{
            $this->load->library('session');
            $this->load->library('table');
            $this->load->library('Utils');
            
            $data['titulos']  = array("navegador" => "JLL - HERMES", 
                                      "ventana"   => "HERMES",
                                      "frase"     => "\"Hacer de lo simple algo complicado es com&uacute;n; hacer de lo complicado algo simple, incre&iacute;blemente simple, es creatividad\" <br>- Charles Mingus",
                                      "titulo"	  => "Ingreso al Portal HERMES");
            $data['usuario']  = "";
            $data['iconuser'] = "";
            $data['sesion']   = "";
            $data['foto']    = "";                                    
            
            $this->load->view('login',$data);
	}

}

