<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuario extends CI_Controller 
{	
        public $registros_x_pagina = 10;        
	
        public function __construct()
        {
            parent::__construct();

            $this->load->database('jll_hermes_con');
            $this->load->model('md_usuario');
            $this->load->model('md_catalogo');
            $this->load->library('session');        
            $this->load->library('table');
            $this->load->library('Utils');
            $this->load->helper('array');
        }
					
	
	
	public function index($msj='')
	{
                $this->validaSesion(NULL,TRUE);
                
		$data['inSel'] = "";
                $data['usSel'] = "class=\"selected\"";
                $data['cmSel'] = "";
                $data['titulos']  = array("navegador" => "JLL - HERMES", 
                                          "ventana"   => "HERMES",                                          
                                          "titulo"    => "Bandeja de Usuarios ".$msj);
                $data['usuario']  = $this->myData['usuario'];
                $data['iconuser'] = $this->myData['iconuser'];
                $data['foto']     = $this->myData['foto'];
                
                
                $this   -> table->clear();			
                $tmpl   =  array('table_open' => '<table cellspacing="0" width="100%" id="userTable">' );
                $this   -> table->set_template($tmpl);
                $header =  array('','','Name','User','Job','Profile','Actions');
                $this   -> table -> set_heading($header);
                $this   -> table->add_row(' ');
                
                $data['gridUser']        = $this->table->generate();
                $data['registrosPagina'] = $this->registros_x_pagina;
                
		$this->load->view('usr',$data);	
	}
	
        
        public function nuevo()
        {
            $this->validaSesion(NULL,TRUE);
            try{
                $data['inSel'] = "";
                $data['usSel'] = "class=\"selected\"";
                $data['cmSel'] = "";
                $data['titulos'] = array("navegador" => "JLL PROYECTO HERMES",								 
                                         "ventana"   => "HERMES",
                                         "frase"     => "\"Hacer de lo simple algo complicado es com&uacute;n; hacer de lo complicado algo simple, incre&iacute;blemente simple, es creatividad\" <br>- Charles Mingus",
                                         "titulo"    => "New user");
            
                $data['accion']   = "N";
                $data['usuario']  = $this->myData['usuario'];
                $data['iconuser'] = $this->myData['iconuser'];
                $data['foto']     = $this->myData['foto'];
                $data['dirFoto']  = $this->dirFoto;
                $data['profiles'] = $this->md_catalogo-> poblarSelect('profile',true);
                $data['attP']     = 'id="profile" class="validate[custom[requiredInFunction]]"';
                $data['fotoIni']  = "logojll.png";
                $data['usr']      =  array(array("nombre"=>null,"apellidos"=>null,"tipo"=>'0',"puesto"=>null,"pwd"=>null, "correo"=>null, "titulo"=>null, "telefono"=>null));
                                
                $this->load->view('form_usr',$data);
                
                } catch (Exception $e) {echo ' nuevo Excepción: ',  $e, "\n";}		
        }
        
        public function edita($correo=0)
        {
            $this->validaSesion(NULL,TRUE);
            try{
                $data['inSel'] = "";
                $data['usSel'] = "class=\"selected\"";
                $data['cmSel'] = "";
                $data['titulos'] = array("navegador" => "JLL PROYECTO HERMES",								 
                                         "ventana"   => "HERMES",                
                                         "titulo"    => "Edit user");                
                
                $data['accion']   = "E";
                $data['usuario']  = $this->myData['usuario'];
                $data['iconuser'] = $this->myData['iconuser'];
                $data['foto']     = $this->myData['foto'];
                $data['dirFoto']  = $this->dirFoto;
                $data['profiles'] = $this->md_catalogo-> poblarSelect('profile',true);
                $data['attP']     = 'id="profile" class="validate[custom[requiredInFunction]]"';
                
                $usuarioData      = $this->md_usuario->traeDetalleUsr(rawurldecode($correo));                
                $data['fotoIni']  = element('foto', $usuarioData['0']);
                $data['usr']      = $usuarioData;
                
                $this->load->view('form_usr',$data);
                
                } catch (Exception $e) {echo ' edita Excepción: ',  $e, "\n";}		
        }
        
        public function guardar()
        {
            $this->validaSesion(NULL,TRUE);
            try{
                $this->load->helper('date');               
                
                $accion = $this -> input -> post('accion');
                $dataUsr = array(
                                 'nombre'     =>   $this -> input -> post('firstname'),
                                 'apellidos'  =>   $this -> input -> post('lastname'),
                                 'pwd'        =>   $this -> input -> post('pwd'),
                                 'correo'     =>   $this -> input -> post('correo'),
                                 'tipo'       =>   $this -> input -> post('profile'),
                                 'puesto'     =>   $this -> input -> post('job'),
                                 'titulo'     =>   $this -> input -> post('titulo'),
                                 'telefono'   =>   $this -> input -> post('telefono'),
                                 'foto'       =>   $this -> input -> post('foto'),
                                 'fecha_alta' =>   standard_date('DATE_W3C', time())
                                 );
                
                if($accion == "E")// Para actualizar el pedido borra todas las tablas auxiliriares para insertar la nueva 
                    {
                        unset($dataUsr['correo']);
                        unset($dataUsr['fecha_alta']);
                        $this->md_usuario->updateUsr($dataUsr,$this -> input -> post('correo'));}
                else
                     { $this->md_usuario->insertUsr($dataUsr);}
                
                                                  
                 $this->index(' Usuario '.$this -> input -> post('firstname').' '.$this -> input -> post('lastname').' agregado exitosamente');
                
                } catch (Exception $e) {echo ' guardar Excepción: ',  $e, "\n";}		
        }

       public function paginarAX()
        {           
            try{
                $vs = $this->validaSesion(TRUE,TRUE);
                if( isset($vs['session']))
                {   echo json_encode ($vs); 
                    exit(0);
                }
                
                $pagina = $this -> input -> post('pagina');
                $users  = $this -> md_usuario -> traeUsers($this->registros_x_pagina,$pagina, $this->dirFoto);

                echo json_encode ($users);
                
            } catch (Exception $e) {echo ' paginarAX Excepción: ',  $e, "\n";}		
        }
        
      public function agregaImagenProfileAX()
        {
         try{
             $vs = $this->validaSesion(TRUE,TRUE);
            if( isset($vs['session']))
            {   echo json_encode ($vs); 
                exit(0);
            }
            if(isset($_FILES["myfile"]))
            {
                    $ret = array();		
                    $error =$_FILES["myfile"]["error"];		
                    if(!is_array($_FILES["myfile"]["name"])) //single file
                    {                            
                            $fileName =  $this->utils->generaNombreImagen($_FILES["myfile"]["name"]);
                            move_uploaded_file($_FILES["myfile"]["tmp_name"],$this->dirFoto.$fileName);

                            $ret[]= $fileName;
                    }
                    else  //Multiple files, file[]
                    {
                      $fileCount = count($_FILES["myfile"]["name"]);
                      for($i=0; $i < $fileCount; $i++)
                      {                            
                            $fileName =  $this->utils->generaNombreImagen($_FILES["myfile"]["name"][$i]);
                            move_uploaded_file($_FILES["myfile"]["tmp_name"][$i],$this->dirFoto.$fileName);
                            $ret[]= $fileName;
                      }

                    }
                    echo json_encode($ret);                
             }
          } catch (Exception $e) {echo ' agregaImagenAX Excepción: ',  $e, "\n";}         
        }


        public function	borraImagenProfileAX()
        {
                $vs = $this->validaSesion(TRUE,TRUE);
                if( isset($vs['session']))
                {   echo json_encode ($vs); 
                    exit(0);
                }
                if(isset($_POST["op"]) && $_POST["op"] == "delete" && isset($_POST['name']))
                {
                        $fileName =$_POST['name'];
                        $filePath = $this->dirFoto.$fileName;
                        if (file_exists($filePath)) 		
                                unlink($filePath);

                        echo "Deleted File ".$fileName."<br>";
                }
        }

        public function	renombraImagenProfileAX()
        {	
        try{	
                $vs = $this->validaSesion(TRUE,TRUE);
                if( isset($vs['session']))
                {   echo json_encode ($vs); 
                    exit(0);
                }
                $this->load->helper('date');
                
                $errores       = array();
                $errorTxt      = "";        
                $extension     = $this -> input -> post('extension');
                $nombreArchivo = $this -> input -> post('nombreArchivo');                
                $idImagen      = "tll_".intval(substr(now(), -5));
                $nombreArchivo = $this->dirFoto.$nombreArchivo.".".$extension;
                $nombreHermes  = $this->dirFoto.$idImagen.".".$extension;
               
                $cmd = "$nombreArchivo -resize 55x55! "; 
                exec("convert $cmd $nombreHermes ",$errores);
                unlink($nombreArchivo);

                if ( !empty($errores) )
                    { $errorTxt = "<br />Hubo errores al trabajar conconversion:<br />".print_r($errores); }       

                echo json_encode (array("nombreHermes" => $nombreHermes,"hImg" => $idImagen.".".$extension,"erroresd" => $errorTxt));

                } catch (Exception $e) {echo 'renombraImagenAX Excepción: ',  $e, "\n";}	
        }
        
        public function	borraImagenProfileCargadaAX()
        {	
        try{	$vs = $this->validaSesion(TRUE,TRUE);
                if( isset($vs['session']))
                {   echo json_encode ($vs); 
                    exit(0);
                }
                $extension 	   = $this -> input -> post('extension');
                $nombreArchivo     = $this -> input -> post('nombreArchivo');                

                $nombreArchivo = $nombreArchivo.".".$extension;
                $filePath      = $this->dirFoto.$nombreArchivo;
                $result        = false;

                if (file_exists($filePath)) 		
                    { $result = unlink($filePath); }

                echo json_encode (array("result" => $result,"dirFoto"=>$this->dirFoto));

                } catch (Exception $e) {echo 'renombraAdjuntoAX Excepción: ',  $e, "\n";}	
        }

	
}//Controller

