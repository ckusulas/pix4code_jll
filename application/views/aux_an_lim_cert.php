<?php 
echo form_open(base_url().'inmueble/guardar/'.$id_in.'/'.$accion.'/LC/'.$inmueble['frontPage'][0]['id_status'], array('id' => 'formLC','name' => 'formLC'));    
    
 echo '<fieldset>
            <div class="row">
                    <label class="col col-7 subtitulo">Assumptions and Limiting Conditions</label>
                    <section class="col col-5"><label class="checkbox" style="text-align: justify; text-justify: inter-word"><input type="checkbox" class="includedRep" name="ann_in" id="LIMCheckBox" value="LIM" checked><i></i>Section Included In The Valuation Report</label></section>
            </div>
      </fieldset>
      <fieldset>';
  echo br(2).'<div class="row" id="annexsLim"></div>'.br(2);
  echo '</fieldset>';
 
  echo '<fieldset>
            <div class="row">
                    <label class="col col-7 subtitulo">Certification</label>
                    <section class="col col-5"><label class="checkbox" style="text-align: justify; text-justify: inter-word"><input type="checkbox" class="includedRep" name="ann_in" id="CERTCheckBox" value="CERT" checked><i></i>Section Included In The Valuation Report</label></section>
            </div>
        </fieldset>
        
        <fieldset>';
  echo br(2).'<div class="row" id="annexsCert"></div>'.br(2);
  echo '</fieldset>';
  
  echo "<div class='row'><a class='button' href='javascript:submitInAX(baseURL,\"formLC\",\"$id_in\",\"E\",\"LC\");'>Save Section</a>
          <span id='confirmLC' class='msjconfirm'></span></div>";

   echo form_close();

?>
