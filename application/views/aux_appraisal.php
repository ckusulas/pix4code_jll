
<?php
        echo '<!-- tabs -->
			<div class="sky-tabs sky-tabs-pos-top-left sky-tabs-anim-flip sky-tabs-response-to-icons">
				<input type="radio" name="sky-tabs-3" value="API" id="sky-tab1-1" class="sky-tab-content-1">
				<label for="sky-tab1-1"><span><span><i class="fa fa-building-o"></i>I.-General Information</span></span></label>
				
				<input type="radio" name="sky-tabs-3" value="APII" id="sky-tab1-2" class="sky-tab-content-2">
				<label for="sky-tab1-2"><span><span><i class="fa fa-folder"></i>II.-Sub Property</span></span></label>
				
				<input type="radio" name="sky-tabs-3" value="APIII" id="sky-tab1-3" class="sky-tab-content-3">
				<label for="sky-tab1-3"><span><span><i class="fa fa-book"></i>III.-Appraisal P & M.</span></span></label>
				
				<input type="radio" name="sky-tabs-3" value="APIV" id="sky-tab1-4" class="sky-tab-content-4">
				<label for="sky-tab1-4"><span><span><i class="fa fa-globe"></i>IV.-Highest & Best Use</span></span></label>
                                
                                <input type="radio" name="sky-tabs-3" value="APV" id="sky-tab1-5" class="sky-tab-content-5">
				<label for="sky-tab1-5"><span><span><i class="fa fa-money"></i>V.-Cost Approach</span></span></label>
                                
                                <input type="radio" name="sky-tabs-3" value="APVI" id="sky-tab1-6" class="sky-tab-content-6">
				<label for="sky-tab1-6"><span><span><i class="fa fa-money"></i>VI.-Income C. Approach</span></span></label>
                                
                                <input type="radio" name="sky-tabs-3" value="APVII" id="sky-tab1-7" class="sky-tab-content-7">
				<label for="sky-tab1-7"><span><span><i class="fa fa-folder"></i>VII.-Sales / Value / Summary / Reconcilation</span></span></label>
                                
                                <input type="radio" name="sky-tabs-3" value="APVIII" id="sky-tab1-8" class="sky-tab-content-8">
				<label for="sky-tab1-8"><span><span><i class="fa fa-certificate"></i>VIII.-Liquidation / Conclusion</span></span></label>
				
                                <input type="radio" name="sky-tabs-3" tipoComp="" value="ACA" id="sky-tab1-9" class="sky-tab-content-9">
				<label for="sky-tab1-9"><span><span><i class="fa fa-money"></i> Area / Cost </span></span></label>
                                
				<ul>
					<li class="sky-tab-content-1">';
                                             include("aux_ap_I.php");
	echo '				</li>
					
					<li class="sky-tab-content-2">';
                                             include("aux_ap_II.php");
	echo '				</li>
					
					<li class="sky-tab-content-3">';
                                             include("aux_ap_III.php");
	echo '				</li>
					
					<li class="sky-tab-content-4">';
                                             include("aux_ap_IV.php");
	echo '				</li>	
                                        
					<li class="sky-tab-content-5">';
                                             include("aux_ap_V.php");
	echo '				</li>
            
					<li class="sky-tab-content-6">';
                                             include("aux_ap_VI.php");
	echo '				</li>
            
					<li class="sky-tab-content-7">';
                                             include("aux_ap_VII.php");
	echo '				</li>
            
					<li class="sky-tab-content-8">';
                                             include("aux_ap_VIII.php");
	echo '				</li>
            
                                        <li class="sky-tab-content-9">';
                                            include("aux_constructed_a.php");
        echo '                          </li>                                        
				</ul>
			</div>
			<!--/ tabs -->';
?>  