<?php include("header.php"); ?>

<script>
    
    var baseURL    = '<?php echo base_url(); ?>';
    var tipoAccion = '<?php echo $accion; ?>';
    var tipoComp   = '<?php echo $tipo; ?>';
    var er         = <?php echo $er; ?>;
    var ft         = <?php echo $ft; ?>;
    var ftyear     = <?php echo $ftyear; ?>;
    
    $(document).ready(function(){      
        
    $('#fileUploaderFotoComp').addClass('pointer');    
    subirImagenFotoComp(baseURL);    
    if(tipoAccion=="E")
    {
        $('.imgCP').addClass('pointer');
        $('.imgCP').click(function(i) {var objectImagen = $(this);
                                        jConfirm("¿Borrar Foto del Comparable?","HERMES PROJECT VALUATION REPORT", function(r) {
                                                    if(r) {  borraImagenCargadaFotoCompAX(baseURL,objectImagen);  }                         
                                                });                
                                      });
        var cons_ft2 = Number($("#construction").val()) * ft;
        var uv = $("#unit_value_mx").val();
        var uv_usd = (tipoComp==="CRL")?((uv/er)*ftyear):(tipoComp==="CRS")?( (uv/er)/ft):(uv/er);
        $("#construction_ft2Lbl").html('Construcction '+ $.number(cons_ft2 ,2, '.', ',' ) +' ft2');
        if(tipoComp=="CLS"){
                $("#land_ft2Lbl"      ).html('land '+ $.number($("#land_ft2").val() ,2, '.', ',' ) +' ft2');
                $("#price_usdLbl"     ).html("$"+$.number($("#price_mx").val()/er ,2, '.', ',' )+' USD');
                $("#unit_value_mxLbl" ).html("$"+$.number($("#unit_value_mx").val() ,2, '.', ',' )+" MXN / m2");
                $("#unit_value_usdLbl").html("$"+$.number(uv_usd ,2, '.', ',' )+" USD / m2");
                $("#unit_value_ftLbl" ).html("$"+$.number(uv_usd/ft ,2, '.', ',' )+" USD / ft2");
           
        }else
        if(tipoComp=="CRS"){
                $("#price_mx"         ).attr('disabled','disabled');
                $("#price_usdLbl"     ).html("$"+$.number($("#price_mx").val()/er ,2, '.', ',' )+' USD');                
                $("#unit_value_mxLbl" ).html("$"+$.number($("#unit_value_mx").val() ,2, '.', ',' )+" MXN / m2");
                $("#unit_value_usdLbl").html("$"+$.number(uv_usd ,2, '.', ',' )+" USD / ft2");
            }else{
                    $("#price_mx"    ).attr('disabled','disabled');
                    $("#price_usdLbl").html("$"+$.number($("#price_mx").val()/er ,2, '.', ',' )+' USD');                    
                    var uv =  Number($("#unit_value_mx").val());
                    $("#unit_value_mxLbl" ).html("$"+$.number(uv ,2, '.', ',' )+" MXN / m2 / mo<br>$"+$.number((uv/er),2,'.',',' )+" USD / m2 / mo");
                    $("#unit_value_usdLbl").html("$"+$.number(uv_usd ,2, '.', ',' )+" USD / ft2 / year");
                }
        var ph=$("#phone").val();
        $("#phone").mask('99-99-99-9?9-99', {placeholder:'X'}).val(ph);
       
        $("#lada").mask('99?9', {placeholder:'X'});
    }
    else{
            $("#phone").mask('99-99-99-9?9-99', {placeholder:'X'});
            $("#lada").mask('99?9', {placeholder:'X'});
            if(tipoComp=="CLS")
                $("#price_mx").attr('disabled','disabled');            
        }
    
    $("#updateER").addClass('pointer');
    $("#updateER").click(function(){$("#erDiv").html('Exchange rate $<label class="input"><i class="icon-append fa fa-usd"></i><input type="text" name="newER" value=""  id="newER" placeholder="New Exchange Rate" maxlength="8"><b class="tooltip tooltip-bottom-right">Actualizar Tipo de Cambio</b></label>'+
                                                      '<label id="buttonUpER" class="button">update</label></section>');
                                    $("#buttonUpER").click(function() { updateERAX(baseURL,$("#newER").val(),$("#id_comp").val(),tipoComp,"comp" ); });
                                   });
                                   
    $("#land_m2Mask").change(function(){ var land   = $(this).maskMoney('unmasked')[0];                                        
                                        var land_ft= land * ft;
                                        $("#land_m2").val(land);
                                        $("#land_ft2Lbl").html('land '+$.number(land_ft ,2, '.', ',' ) +' ft2');
                                        $("#land_ft2").val(land_ft.toFixed(2));

                                        if(tipoComp==="CLS")
                                        {
                                         var price  = isNaN($("#price_mx").val())==true?0:Number($("#price_mx").val());
                                         var uv     = land!=0?price/land:0;                                         
                                         var uv_usd = uv/er;                                                                                                                   

                                         $("#unit_value_mxLbl").html('$'+$.number(uv ,2, '.', ',' )+" MXN / m2");
                                         $("#unit_value_mx").val(uv.toFixed(2));

                                         $("#unit_value_usdLbl").html('$'+$.number(uv_usd ,2, '.', ',' )+" USD / m2");
                                         $("#unit_value_ftLbl" ).html("$"+$.number(uv_usd/ft ,2, '.', ',' )+" USD / ft2");
                                     }
                                   });
                                   
    $("#price_mxMask").change(function(){ var price  = $(this).maskMoney('unmasked')[0]; 
                                          var usd    = price/er;                                          
                                          var land   = (tipoComp==="CLS")?Number($("#land_m2").val()) : Number($("#construction").val());
                                          var uv     = land===0?0:price/land;                                          
                                          var uv_usd = (tipoComp==="CRL")?((uv/er)*ftyear):(tipoComp==="CRS")?( (uv/er)/ft):(uv/er);
                                           
                                          $("#price_mx").val(price);
                                          $("#price_usdLbl").html('$'+$.number(usd ,2, '.', ',' ) + ' USD');                                      
                                          $("#price_usd").val(usd.toFixed(2));
                                          
                                          $("#unit_value_mxLbl").html((tipoComp==="CRL")?'$'+$.number(uv,2,'.',',' )+" MXN / m2 / mo<br>$"+$.number((uv/er),2,'.',',' )+" USD / m2 / mo" : "$"+$.number(uv,2,'.',',' )+" MXN / m2");
                                          $("#unit_value_mx").val(uv.toFixed(2));
                                          
                                          $("#unit_value_usdLbl").html((tipoComp==="CRL")?'$'+$.number(uv_usd ,2, '.', ',' )+" USD / ft2 / year" : (tipoComp==="CRS")?'$'+$.number(uv_usd,2, '.', ',' )+" USD / ft2":'$'+$.number(uv_usd ,2, '.', ',' )+" USD / m2");
                                          $("#unit_value_ftLbl" ).html((tipoComp==="CRL")?'' : (tipoComp==="CRS")?'$'+$.number(uv_usd,2, '.', ',' )+" USD / ft2":'$'+$.number(uv_usd/ft ,2, '.', ',' )+" USD / ft2");
                                        });
                                   
     $("#constructionMask").change(function(){ var const_m2     = $(this).maskMoney('unmasked')[0];
                                               var const_ft2    = const_m2 * ft;
                                               $("#construction_ft2Lbl").html('Construcction '+$.number(const_ft2 ,2, '.', ',' ) +' ft2');
                                               $("#construction").val(const_m2);
                                               
                                               if(tipoComp!=="CLS")
                                                {
                                                    var price    = $("#price_mx").val();
                                                    var uv       = price/const_m2;
                                                    
                                                    var uv_usd = (tipoComp==="CRL")?((uv/er)*ftyear):(tipoComp==="CRS")?( (uv/er)/ft):(uv/er);

                                                    $("#unit_value_mxLbl").html((tipoComp==="CRL")?'$'+$.number(uv,2,'.',',' )+" MXN / m2 / mo<br>$"+$.number((uv/er),2,'.',',' )+" USD/ m2 / mo" : "$"+$.number(uv,2,'.',',' )+" MXN / m2");
                                                    $("#unit_value_mx").val(uv.toFixed(2));

                                                    $("#unit_value_usdLbl").html((tipoComp==="CRL")?'$'+$.number(uv_usd ,2, '.', ',' )+" USD / ft2 / year" : (tipoComp==="CRS")?'$'+$.number(uv_usd,2, '.', ',' )+" USD / ft2":'$'+$.number(uv_usd ,2, '.', ',' )+" USD / m2");
                                                }
                                              });

 
     $("#land_m2Mask"     ).maskMoney({allowNegative: false, thousands:',', decimal:'.', allowZero: true,precision:2});
     $("#constructionMask").maskMoney({allowNegative: false, thousands:',', decimal:'.', allowZero: true,precision:2});
     
     if(tipoComp==="CRL")
         $("#price_mxMask").maskMoney({prefix:'$',precision:2,decimal:'.', allowNegative: false, thousands:',', allowZero: true});
     else
        $("#price_mxMask").maskMoney({prefix:'$',precision:2,decimal:'.', allowNegative: false, thousands:',', allowZero: true});
     
     $("#land_m2Mask"     ).maskMoney("mask");     
     $("#price_mxMask"    ).maskMoney("mask");
     $("#constructionMask").maskMoney("mask");
     
     tinymce.init({
                    selector   : '.richTextSmall',
                    setup      : function (editor) { editor.on('change', function () { editor.save(); }); },
                    height     : 150,
                    toolbar    : ' undo redo  | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist |',
                    content_css: ['//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css','//www.tinymce.com/css/codepen.min.css']
                  });

    $("#closing_listing_date").datepicker({dateFormat: 'M d yy'});

    });//document ready
    
   
</script>

<?php
    echo '<div class="body-form">';
          $attributes = array('class' => 'sky-form', 'id' => 'form_comp','name' => 'form_comp');
    echo form_open(base_url().'comparables/guardar/', $attributes);   
    
    echo form_input(array('name'  => 'accion', 
                          'type'  => 'hidden', 
                          'id'    => 'accion',
                          'value' => $accion));
    echo form_input(array('name'  => 'tipocomp', 
                          'type'  => 'hidden', 
                          'id'    => 'tipocomp',
                          'value' => $tipo));
    echo form_input(array('name'  => 'foto', 
                          'type'  => 'hidden', 
                          'id'    => 'foto',
                          'value' => $fotoIni));
     echo form_input(array('name'  => 'id_comp',
                          'type'  => 'hidden', 
                          'id'    => 'id_comp',
                          'value' => $id_comp));
     echo form_input(array('name'  => 'exchange_rate',
                          'type'  => 'hidden', 
                          'id'    => 'exchange_rate',
                          'value' => $er));
    echo '<header>
            <div class="row">
                    <section class="label col col-7">'.$titulos["titulo"].'</section>
                    <section id="erDiv" class="col col-4">Exchange rate $'.$er.nbs(2).$updateER.$d_er.'</section>
            </div>
          </header>';
    
    echo '<fieldset>
            <div class="row"> <label class="label subtitulo">'.$titulo.'</label> </div>
            <div class="row"> <label class="label subtitulo">'.$subtitulo.'</label> </div>
            <div class="row">
                    <section class="col col-7"><br>
                        <div id="FotoComp"><img src="'. base_url().$dirFoto.$fotoIni.'">'.$fotoClose.'</div>
                    </section>
                    <section class="col col-2">'.br(3).'
                        <div id="fileUploaderFotoComp">&nbsp;Click para seleccionar Imagen&nbsp;</div>
                        <div id="statusComp"></div>
                    </section>
            </div>
          </fieldset>
          <fieldset>
          <div class="row">
            <label class="col col-4 label subtitulo">Type of property</label>
            <label class="col col-8 label subtitulo">Location</label>
          </div>
            <div class="row">
            <section class="col col-4">
                <label class="label">'.nbs(1).'</label>
                <label class="input">
                    <i class="icon-append fa fa-university"></i>'.
                    form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input', 
                                     'name'        => 'type_property', 
                                     'id'          => 'type_property',
                                     'placeholder' => "Type of Property",
                                     'value'       => $comparable[0]['type_property'],
                                     'maxlength'   => '40')).'
                     <b class="tooltip tooltip-bottom-right">Tipo de propiedad del comparable</b>
                 </label>
             </section>
            <section class="col col-8">
                 <label class="label">Street/Av</label>
                 <label class="input">
                    <i class="icon-append fa fa-road"></i>'.
                    form_input(array('class'       => 'validate[custom[onlyLetterNumber]] text-input', 
                                     'name'        => 'calle', 
                                     'id'          => 'calle',
                                     'placeholder' => "Street/Av",
                                     'value'       => $comparable[0]['calle'],						 
                                     'maxlength'   => '100')).'                  
                 </label>
         </section>
        </div>
        <div class="row">
        <div class="col col-4"></div>        
        <section class="col col-3">
                 <label class="label">No</label>
                 <label class="input">
                    <i class="icon-append fa fa-road"></i>'.
                    form_input(array('class'       => 'validate[custom[onlyLetterNumber]] text-input', 
                                     'name'        => 'num', 
                                     'id'          => 'num',
                                     'placeholder' => "No",
                                     'value'       => $comparable[0]['num'],						 
                                     'maxlength'   => '30')).'                  
                 </label>
         </section>
         
        <section class="col col-5">
                 <label class="label">Neighborhood</label>
                 <label class="input">
                    <i class="icon-append fa fa-road"></i>'.
                    form_input(array('class'       => 'validate[custom[onlyLetterNumber]] text-input', 
                                     'name'        => 'col', 
                                     'id'          => 'col',
                                     'placeholder' => "Neighborhood",
                                     'value'       => $comparable[0]['col'],						 
                                     'maxlength'   => '100')).'                  
                 </label>
         </section>
        </div>
        <div class="row">
        <div class="col col-4"></div>        
        <section class="col col-3">
                 <label class="label">City / Municipality</label>
                 <label class="input">
                    <i class="icon-append fa fa-road"></i>'.
                    form_input(array('class'       => 'validate[custom[onlyLetterNumber]] text-input', 
                                     'name'        => 'mun', 
                                     'id'          => 'mun',
                                     'placeholder' => "City",
                                     'value'       => $comparable[0]['mun'],						 
                                     'maxlength'   => '100')).'                  
                 </label>
         </section>
         
        <section class="col col-3">
                 <label class="label">State</label>
                 <label class="input">
                    <i class="icon-append fa fa-road"></i>'.
                    form_input(array('class'       => 'validate[custom[onlyLetterNumber]] text-input', 
                                     'name'        => 'edo', 
                                     'id'          => 'edo',
                                     'placeholder' => "State",
                                     'value'       => $comparable[0]['edo'],						 
                                     'maxlength'   => '100')).'                  
                 </label>
         </section>
         
        <section class="col col-2">
                 <label class="label">Zipcode</label>
                 <label class="input">
                    <i class="icon-append fa fa-road"></i>'.
                    form_input(array('class'       => 'validate[custom[number]] text-input', 
                                     'name'        => 'cp', 
                                     'id'          => 'cp',
                                     'placeholder' => "Zipcode",
                                     'value'       => $comparable[0]['cp'],						 
                                     'maxlength'   => '15')).'                  
                 </label>
          </section>
         </div>
         <div class="row">
            <label class="col col-4"></label>
            <section class="col col-4">
                 <label class="label">Latitude</label>
                 <label class="input">
                    <i class="icon-append fa fa-road"></i>'.
                    form_input(array('class'       => 'validate[custom[number]] text-input', 
                                     'name'        => 'latitud', 
                                     'id'          => 'latitud',
                                     'placeholder' => "Latitud",
                                     'value'       => $comparable[0]['latitud'],						 
                                     'maxlength'   => '20')).'                  
                    </label>
            </section>
         
            <section class="col col-4">
                     <label class="label">Longitude</label>
                     <label class="input">
                        <i class="icon-append fa fa-road"></i>'.
                        form_input(array('class'       => 'validate[custom[number]] text-input', 
                                         'name'        => 'longitud', 
                                         'id'          => 'longitud',
                                         'placeholder' => "Longitud",
                                         'value'       => $comparable[0]['longitud'],						 
                                         'maxlength'   => '20')).'                  
                     </label>
              </section>
          </div>  
         </fieldset>
         <fieldset>
            <div class="row">
              <label class="col col-4 label subtitulo">Source of Information</label>
              <label class="col col-2 label subtitulo">'.nbs(1).'</label>
              <label class="col col-2 label subtitulo">Phone Code</label>
              <label class="col col-4 label subtitulo">Telephone</label>
            </div>
            <div class="row">
                <section class="col col-4">
                    <label class="input">
                        <i class="icon-append fa fa-info"></i>'.
                        form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input', 
                                         'name'        => 'source_information', 
                                         'id'          => 'source_information',
                                         'placeholder' => "Source of Information",
                                         'value'       => $comparable[0]['source_information'],
                                         'maxlength'   => '40')).'
                         <b class="tooltip tooltip-bottom-right">Fuente de informacion del comparable</b>
                     </label>
                 </section>
                 <section class="col col-2">'.nbs(1).'</section>
                 <section class="col col-2">
                    <label class="input">
                        <i class="icon-append fa fa-phone"></i>'.
                        form_input(array('name'        => 'lada', 
                                         'id'          => 'lada',
                                         'placeholder' => "Lada",
                                         'value'       => $comparable[0]['lada'],
                                         'maxlength'   => '3')).'
                         <b class="tooltip tooltip-bottom-right">Lada del telefono de la propiedad del comparable</b>
                     </label>
                    </section>
                 <section class="col col-4">
                    <label class="input">
                        <i class="icon-append fa fa-phone"></i>'.
                        form_input(array('name'        => 'phone', 
                                         'id'          => 'phone',
                                         'placeholder' => "Telephone",
                                         'value'       => $comparable[0]['phone'],
                                         'maxlength'   => '40')).'
                         <b class="tooltip tooltip-bottom-right">Telefono de la propiedad del comparable</b>
                     </label>
                 </section>
            </div>
         </fieldset>
          <fieldset>
            <div class="row">
              <label class="col col-3 label subtitulo">'.$c1.'</label>
              <section class="col col-1"></section>
              <label class="col col-3 label subtitulo">'.$c2.'</label>
              <section class="col col-1"></section>
              <label class="col col-3 label subtitulo">'.$c3.'</label>
            </div>
            
            <div class="row">
                <section class="col col-3">
                    <label class="input">
                        <i class="icon-append fa fa-university"></i>'.
                        form_input(array('name'=>'land_m2Mask','id'=>'land_m2Mask','value'=>$comparable[0]['land_m2'],'maxlength'=>'20')).
                        form_input(array('name'=>'land_m2','id'=>'land_m2','type'=>'hidden','value'=>$comparable[0]['land_m2']         ,'maxlength'=>'20')).'
                         <b class="tooltip tooltip-bottom-right">Area de la propiedad del comparable</b>
                     </label>
                     <div class="note note-error">Land m2</div>
                 </section>
                 <section class="col col-1"></section>
                 <section class="col col-3">                 
                    <label class="input">
                        <i class="icon-append fa fa-usd"></i>'.
                        form_input(array('name'=> 'price_mxMask', 'id'=>'price_mxMask','value'=> $comparable[0]['price_mx'],'maxlength' => '20')).
                        form_input(array('name'=> 'price_mx'    , 'id'=>'price_mx'    ,'type'=>'hidden','value'=> $comparable[0]['price_mx'] ,'maxlength' => '20')).'
                         <b class="tooltip tooltip-bottom-right">Area de la propiedad del comparable</b>
                     </label>
                     <div class="note note-error">'.$c2.'</div>
                 </section>
                 <section class="col col-1"></section>
                 <section class="col col-3">
                    <label class="label txt" id="unit_value_mxLbl" ></label>'.
                        form_input(array('name'        => 'unit_value_mx', 
                                         'id'          => 'unit_value_mx',
                                         'type'        => "hidden",
                                         'value'       => $comparable[0]['unit_value_mx'])).'                         
                 </section>                 
             </div>
             
             <div class="row">
                <section class="col col-3">
                    <label id="land_ft2Lbl" class="label txt"></label>'.
                        form_input(array('name'        => 'land_ft2', 
                                         'id'          => 'land_ft2',
                                         'type'        => 'hidden', 
                                         'value'       => $comparable[0]['land_ft2'])).'                         
                 </section>
                 <section class="col col-1"></section>
                 <section class="col col-3">
                    <label id="price_usdLbl" class="label txt"></label>'.
                        form_input(array('name'        => 'price_usd', 
                                         'id'          => 'price_usd',
                                         'type'        => 'hidden',
                                         'value'       => $comparable[0]['price_usd'])).'
                 </section>
                 <section class="col col-1"></section>
                 <section class="col col-3">
                    <label id="unit_value_usdLbl" class="label txt"></label>
                    <label id="unit_value_ftLbl" class="label txt"></label>
                 </section>
             </div>
             
            <div class="row">
                <section class="col col-3">
                    <label class="label">Construction m2</label>
                     <label class="input">
                        <i class="icon-append fa fa-university"></i>'.
                        form_input(array('name'=> 'constructionMask', 'id'=>'constructionMask','value'=> $comparable[0]['construction'],'maxlength'   => '14')).
                        form_input(array('name'=> 'construction','type'=>'hidden', 'id'=>'construction','value'=> $comparable[0]['construction'],'maxlength'   => '14')).'
                         <b class="tooltip tooltip-bottom-right">Construccion del comparable</b>
                     </label>
                      <br><label id="construction_ft2Lbl" class="label txt"></label>
                 </section>
                 <section class="col col-4"> </section>
                 <section class="col col-3">
                    <label class="label">'.$c4.'</label>
                     <label class="input">
                        <i class="icon-append fa fa-clock-o"></i>'.
                        form_input(array('class'       => 'validate[custom[onlyLetterNumber]] text-input', 
                                         'name'        => 'time_market', 
                                         'id'          => 'time_market',
                                         'placeholder' => $c4.":",
                                         'value'       => $comparable[0]['time_market'],
                                         'maxlength'   => '40')).'
                         <b class="tooltip tooltip-bottom-right">Time on market comparable</b>
                     </label>   
                 </section>
             </div>
            </fieldset>            
            <fieldset>
             <div class="row">
                 <section class="col col-5"><label class="label">Additional comments </label>
                                            <label class="textarea">'.
                                                                form_textarea(array('class' => 'validate[custom[onlyLetNumChar]] text-input',
                                                                                    'rows'  => "3",
                                                                                    'name'  => 'comments',
                                                                                    'id'    => 'comments',
                                                                                    'value' => $comparable[0]['comments'] )).'
                                            </label></section>
                 <section class="col col-3"><label class="label">'.$c5.'</label>'.$rc8Dropdown.'</section>
                 <section class="col col-3"><label class="label">Closing / Listing Date</label>
                                            <label class="input">
                                                   <i class="icon-append fa fa-calendar"></i>'.
                                                   form_input(array('class'       => 'validate[custom[dateED]] text-input', 
                                                                    'name'        => 'closing_listing_date', 
                                                                    'id'          => 'closing_listing_date',
                                                                    'placeholder' => "Date",
                                                                    'value'       => $comparable[0]['closing_listing_date'],
                                                                    'maxlength'   => '20')).'
                                                    <b class="tooltip tooltip-bottom-right">Closing / Listing Date</b>
                                                </label>   
                 </section>
             </div>
         </fieldset>
        ';

    
    echo "<footer>\n
            <a class='button' href='javascript:submitFormComp(\"form_comp\");'>Save</a>
            <a class='button button-secondary' href='".base_url()."comparables/consulta/$tipo'>Exit</a>
         </footer>\n";
                           
    echo form_close();
    echo '</div>';
            
    echo br(2);	
    include("footer.php"); 
?>  