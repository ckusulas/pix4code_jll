<?php 
echo form_open(base_url().'inmueble/guardar/'.$id_in.'/'.$accion.'/SM/'.$accion.'/'.$inmueble['frontPage'][0]['id_status'], array('id' => 'formSM','name' => 'formSM'));
echo '  <fieldset>
        <div class="row">
                <label class="col col-7 subtitulo">'.nbs(1).'</label>
                <section class="col col-4"><label class="checkbox" style="text-align: justify; text-justify: inter-word"><input type="checkbox" class="includedRep" name="report" value="SM" id="summaryCheckBox" checked><i></i>Section Included In The Valuation Report</label></section>
        </div>
        </fieldset>
        
        <div class="row">
         <section class="col col-8">
                    <label class="input">
                       <i class="icon-append fa fa-bookmark"></i>'.
                       form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input', 
                                        'name'        => 'SMTit1', 
                                        'id'          => 'SMTit1',
                                        'placeholder' => "",
                                        'value'       => '',
                                        'maxlength'   => '60')).'
                        <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                    </label>
          </section>        
          <section class="col col-10">
                    <label class="textarea">'.
                            form_textarea(array('class' => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                'rows'  => "3",
                                                'name'  => 'SMText1',
                                                'id'    => 'SMText1',
                                                'value' => '' )).'
                    </label>                    
            </section>
         </div>
         <div class="row">
            <section class="col col-3">
                <label class="label">Subject\'s address: </label>
            </section>
            <section class="col col-6">
                <label class="label" id="SM_sa"></label>
            </section>   
            <section class="col col-1"> <label class="label"></label> </section>   
        </div> 
        <div class="row">
            <section class="col col-3">
                <label class="label">Client: </label>
            </section>
            <section class="col col-5">
                <label class="label" id="SM_client"></label>
            </section>   
            <section class="col col-2"> <label class="label"></label> </section>   
        </div> 
         <div class="row">
            <section class="col col-3">
                <label class="label">Property owner: </label>
            </section>
            <section class="col col-5">
                <label class="label" id="SM_po"></label>
            </section>
            <section class="col col-2"> <label class="label"></label> </section>   
        </div>
        <div class="row">
            <section class="col col-3">
                <label class="label" id="AI_Tit112"></label>
            </section>
            <section class="col col-8">
                <label class="label" id="AI_Text11"></label>
            </section>   
        </div>
        <div class="row">
         <section class="col col-8">
                    <label class="input">
                       <i class="icon-append fa fa-bookmark"></i>'.
                       form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input', 
                                        'name'        => 'SMTit2', 
                                        'id'          => 'SMTit2',
                                        'placeholder' => "",
                                        'value'       => '',
                                        'maxlength'   => '60')).'
                        <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                    </label>
          </section>        
          <section class="col col-10">
                    <label class="textarea">'.
                            form_textarea(array('class' => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                'rows'  => "3",
                                                'name'  => 'SMText2',
                                                'id'    => 'SMText2',
                                                'value' => '' )).'
                    </label>                    
            </section>
         </div>         
         <div class="row">
         <section class="col col-10">
                <label class="label">Areas: </label>
            </section>
            <section class="col col-10">
            <table class="tableGrid" cellspacing="0" cellpadding="0" width="100%">         
            <tr><td class="topLine titArea">Land Surface</td>
                <td class="topLine titArea">Gross Building Area</td>
                <td class="topLine titArea">Rentable Area</td>                        
            </tr>
            <tr><td class="titArea"><label id="SM_ls"></label></td>
                <td class="titArea"><label id="SM_gba"></label></td>
                <td class="titArea"><label id="SM_tra"></label></td>                        
            </tr>
            <tr><td class="bottomLine texttArea"><label id="SM_lsFT"></label></td>
                <td class="bottomLine texttArea"><label id="SM_gbaFT"></label></td>
                <td class="bottomLine texttArea"><label id="SM_traFT"></label></td>                        
            </tr>
            </table>
            </section>
         </div>
         <div class="row">
         <section class="col col-8">
                    <label class="input">
                       <i class="icon-append fa fa-bookmark"></i>'.
                       form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input', 
                                        'name'        => 'SMTit3', 
                                        'id'          => 'SMTit3',
                                        'placeholder' => "",
                                        'value'       => '',
                                        'maxlength'   => '60')).'
                        <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                    </label>
          </section>        
          <section class="col col-10">
                    <label class="textarea">'.
                            form_textarea(array('class' => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                'rows'  => "3",
                                                'name'  => 'SMText3',
                                                'id'    => 'SMText3',
                                                'value' => '' )).'
                    </label>                    
            </section>
         </div>
         <div class="row">
         <section class="col col-8">
                    <label class="input">
                       <i class="icon-append fa fa-bookmark"></i>'.
                       form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input', 
                                        'name'        => 'SMTit4', 
                                        'id'          => 'SMTit4',
                                        'placeholder' => "",
                                        'value'       => '',
                                        'maxlength'   => '60')).'
                        <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                    </label>
          </section>        
          <section class="col col-10">
                    <label class="textarea">'.
                            form_textarea(array('class' => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                'rows'  => "3",
                                                'name'  => 'SMText4',
                                                'id'    => 'SMText4',
                                                'value' => '' )).'
                    </label>
            </section>
         </div>
         <div class="row">
            <section class="col col-3">
                <label class="label">Intended user the report: </label>
            </section>
            <section class="col col-4">
                <label class="label" id="SM_ur"></label>
            </section>   
            <section class="col col-3"> <label class="label"></label> </section>   
        </div>
        <div class="row">
         <section class="col col-8">
                    <label class="input">
                       <i class="icon-append fa fa-bookmark"></i>'.
                       form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input', 
                                        'name'        => 'SMTit5', 
                                        'id'          => 'SMTit5',
                                        'placeholder' => "",
                                        'value'       => '',
                                        'maxlength'   => '60')).'
                        <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                    </label>
          </section>        
          <section class="col col-10">
                    <label class="textarea">'.
                            form_textarea(array('class' => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                'rows'  => "3",
                                                'name'  => 'SMText5',
                                                'id'    => 'SMText5',
                                                'value' => '' )).'
                    </label>
            </section>
         </div>
         <div class="row">
         <section class="col col-8">
                    <label class="input">
                       <i class="icon-append fa fa-bookmark"></i>'.
                       form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input', 
                                        'name'        => 'SMTit6', 
                                        'id'          => 'SMTit6',
                                        'placeholder' => "",
                                        'value'       => '',
                                        'maxlength'   => '60')).'
                        <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                    </label>
          </section>        
          <section class="col col-10">
                    <label class="textarea">'.
                            form_textarea(array('class' => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                'rows'  => "3",
                                                'name'  => 'SMText6',
                                                'id'    => 'SMText6',
                                                'value' => '' )).'
                    </label>
            </section>
         </div>
         <div class="row">
         <section class="col col-8">
                    <label class="input">
                       <i class="icon-append fa fa-bookmark"></i>'.
                       form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input', 
                                        'name'        => 'SMTit7', 
                                        'id'          => 'SMTit7',
                                        'placeholder' => "",
                                        'value'       => '',
                                        'maxlength'   => '60')).'
                        <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                    </label>
          </section>        
          <section class="col col-10">
                    <label class="textarea">'.
                            form_textarea(array('class' => 'validate[required,custom[onlyLetterNumber]] text-input',
                                                'rows'  => "3",
                                                'name'  => 'SMText7',
                                                'id'    => 'SMText7',
                                                'value' => '' )).'
                    </label>
            </section>
         </div>
         <div class="row">
            <section class="col col-3">
                <label class="label">Exchange rate: </label>
            </section>
            <section class="col col-4">
                <label class="label" id="SM_er"></label>
            </section>   
            <section class="col col-3"> <label class="label"></label> </section>   
        </div>
        <div class="row">
            <section class="col col-3">
                <label class="label">Effective date of the report (date of value): </label>
            </section>
            <section class="col col-4">
                <label class="label" id="SM_ed"></label>
            </section>   
            <section class="col col-3"> <label class="label"></label> </section>   
        </div>
        <div class="row">
            <section class="col col-3">
                <label class="label">Date of the report (date of delivery): </label>
            </section>
            <section class="col col-4">
                <label class="label" id="SM_dr"></label>
            </section>   
            <section class="col col-3"> <label class="label"></label> </section>   
        </div>
        <div class="row">
            <section class="col col-3">
                <label class="label" id="SW_Tit6"></label>
            </section>
            <section class="col col-8">
                <label class="label" id="SW_Text6"></label>
            </section>               
        </div>
        <div class="row">
            <section class="col col-3">
                <label class="label" id="SW_Tit7"></label>
            </section>
            <section class="col col-8">
                <label class="label" id="SW_Text7"></label>
            </section>   
        </div>
        <div class="row">
         <section class="col col-10">
                <label class="label">Value Conclusion: </label>
            </section>
            <section class="col col-10">
            <table class="tableGrid" cellspacing="0" cellpadding="0" width="100%">         
            <tr><td class="topLine centerCell"></td>
                <td class="topLine centerCell">Appraisal Premise</td>
                <td class="topLine centerCell">Interest Appraised</td>
                <td class="topLine centerCell">Date of Value</td>
                <td class="topLine centerCell">Value Conclusion</td>
            </tr>
            <tr><td class="centerCell" valign="middle"><label class="checkbox"><input type="checkbox" name="SM_ch_vc1" id="SM_ch_vc1" value="1"><i></i></label>
                </td>
                 <td class="centerCell">
                    <label class="input">
                       <i class="icon-append fa fa-bookmark"></i>'.
                       form_input(array('class'       => 'validate[custom[onlyLetterNumber]] text-input', 
                                        'name'        => 'SMvc1_1', 
                                        'id'          => 'SMvc1_1',
                                        'placeholder' => "",
                                        'value'       => '',
                                        'maxlength'   => '40')).'
                        <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                    </label>
                </td>
                <td class="centerCell"><label class="input">
                       <i class="icon-append fa fa-bookmark"></i>'.
                       form_input(array('class'       => 'validate[custom[onlyLetterNumber]] text-input', 
                                        'name'        => 'SMvc1_2', 
                                        'id'          => 'SMvc1_2',
                                        'placeholder' => "",
                                        'value'       => '',
                                        'maxlength'   => '40')).'
                        <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                    </label>
                </td>
                <td class="centerCell"><label class="input">
                       <i class="icon-append fa fa-calendar"></i>'.
                       form_input(array('class'       => 'validate[custom[onlyLetterNumber]] text-input', 
                                        'name'        => 'SMvc1_3', 
                                        'id'          => 'SMvc1_3',
                                        'placeholder' => "",
                                        'value'       => '',
                                        'maxlength'   => '20')).'
                        <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                    </label>
                </td>
                <td class="centerCell"><label id="SM_marketVal"></label>
                </td>
            </tr>
            <tr><td class="centerCell" valign="middle"><label class="checkbox"><input type="checkbox" name="SM_ch_vc2" id="SM_ch_vc2" value="1"><i></i></label>
                </td>
                <td class="centerCell"><label class="input">
                       <i class="icon-append fa fa-bookmark"></i>'.
                       form_input(array('class'       => 'validate[custom[onlyLetterNumber]] text-input', 
                                        'name'        => 'SMvc2_1', 
                                        'id'          => 'SMvc2_1',
                                        'placeholder' => "",
                                        'value'       => '',
                                        'maxlength'   => '40')).'
                        <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                    </label>
                </td>
                <td class="centerCell"><label class="input">
                       <i class="icon-append fa fa-bookmark"></i>'.
                       form_input(array('class'       => 'validate[custom[onlyLetterNumber]] text-input', 
                                        'name'        => 'SMvc2_2', 
                                        'id'          => 'SMvc2_2',
                                        'placeholder' => "",
                                        'value'       => '',
                                        'maxlength'   => '40')).'
                        <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                    </label>
                </td>
                <td class="centerCell"><label class="input">
                       <i class="icon-append fa fa-calendar"></i>'.
                       form_input(array('class'       => 'validate[custom[onlyLetterNumber]] text-input', 
                                        'name'        => 'SMvc2_3', 
                                        'id'          => 'SMvc2_3',
                                        'placeholder' => "",
                                        'value'       => '',
                                        'maxlength'   => '20')).'
                        <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                    </label>
                </td>
                <td class="centerCell"><label id="SM_VU_Approach"></label>
                </td>
            </tr>
            <tr><td class="centerCell" valign="middle"><label class="checkbox"><input type="checkbox" name="SM_ch_vc3" id="SM_ch_vc3" value="1"><i></i></label>
                </td>
                 <td class="centerCell">
                    <label class="input">
                       <i class="icon-append fa fa-bookmark"></i>'.
                       form_input(array('class'       => 'validate[custom[onlyLetterNumber]] text-input', 
                                        'name'        => 'SMvc3_1', 
                                        'id'          => 'SMvc3_1',
                                        'placeholder' => "",
                                        'value'       => '',
                                        'maxlength'   => '40')).'
                        <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                    </label>
                </td>
                <td class="centerCell"><label class="input">
                       <i class="icon-append fa fa-bookmark"></i>'.
                       form_input(array('class'       => 'validate[custom[onlyLetterNumber]] text-input', 
                                        'name'        => 'SMvc3_2', 
                                        'id'          => 'SMvc3_2',
                                        'placeholder' => "",
                                        'value'       => '',
                                        'maxlength'   => '40')).'
                        <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                    </label>
                </td>
                <td class="centerCell"><label class="input">
                       <i class="icon-append fa fa-calendar"></i>'.
                       form_input(array('class'       => 'validate[custom[onlyLetterNumber]] text-input', 
                                        'name'        => 'SMvc3_3', 
                                        'id'          => 'SMvc3_3',
                                        'placeholder' => "",
                                        'value'       => '',
                                        'maxlength'   => '20')).'
                        <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                    </label>
                </td>
                <td class="centerCell"><label id="SM_liquidVal"></label>
                </td>
            </tr>
            <tr><td class="centerCell" valign="middle"><label class="checkbox"><input type="checkbox" name="SM_ch_vc4" id="SM_ch_vc4" value="1"><i></i></label>
                </td>
                 <td class="centerCell">
                    <label class="input">
                       <i class="icon-append fa fa-bookmark"></i>'.
                       form_input(array('class'       => 'validate[custom[onlyLetterNumber]] text-input', 
                                        'name'        => 'SMvc4_1', 
                                        'id'          => 'SMvc4_1',
                                        'placeholder' => "",
                                        'value'       => '',
                                        'maxlength'   => '40')).'
                        <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                    </label>
                </td>
                <td class="centerCell"><label class="input">
                       <i class="icon-append fa fa-bookmark"></i>'.
                       form_input(array('class'       => 'validate[custom[onlyLetterNumber]] text-input', 
                                        'name'        => 'SMvc4_2', 
                                        'id'          => 'SMvc4_2',
                                        'placeholder' => "",
                                        'value'       => '',
                                        'maxlength'   => '40')).'
                        <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                    </label>
                </td>
                <td class="centerCell"><label class="input">
                       <i class="icon-append fa fa-bookmark"></i>'.
                       form_input(array('class'       => 'validate[custom[onlyLetterNumber]] text-input', 
                                        'name'        => 'SMvc4_3', 
                                        'id'          => 'SMvc4_3',
                                        'placeholder' => "",
                                        'value'       => '',
                                        'maxlength'   => '20')).'
                        <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                    </label>
                </td>
                <td class="centerCell"><label class="input">
                       <i class="icon-append fa fa-bookmark"></i>'.
                       form_input(array('class'       => 'validate[custom[onlyLetterNumber]] text-input', 
                                        'name'        => 'SMvc4_4', 
                                        'id'          => 'SMvc4_4',
                                        'placeholder' => "",
                                        'value'       => '',
                                        'maxlength'   => '10')).'
                        <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                    </label>
                </td>
            </tr>
            <tr><td class="bottomLine centerCell" valign="middle"><label class="checkbox"><input type="checkbox" name="SM_ch_vc5" id="SM_ch_vc5" value="1"><i></i></label>
                </td>
                 <td class="bottomLine centerCell">
                    <label class="input">
                       <i class="icon-append fa fa-bookmark"></i>'.
                       form_input(array('class'       => 'validate[custom[onlyLetterNumber]] text-input', 
                                        'name'        => 'SMvc5_1', 
                                        'id'          => 'SMvc5_1',
                                        'placeholder' => "",
                                        'value'       => '',
                                        'maxlength'   => '40')).'
                        <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                    </label>
                </td>
                <td class="bottomLine centerCell"><label class="input">
                       <i class="icon-append fa fa-bookmark"></i>'.
                       form_input(array('class'       => 'validate[custom[onlyLetterNumber]] text-input', 
                                        'name'        => 'SMvc5_2', 
                                        'id'          => 'SMvc5_2',
                                        'placeholder' => "",
                                        'value'       => '',
                                        'maxlength'   => '40')).'
                        <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                    </label>
                </td>
                <td class="bottomLine centerCell"><label class="input">
                       <i class="icon-append fa fa-bookmark"></i>'.
                       form_input(array('class'       => 'validate[custom[onlyLetterNumber]] text-input', 
                                        'name'        => 'SMvc5_3', 
                                        'id'          => 'SMvc5_3',
                                        'placeholder' => "",
                                        'value'       => '',
                                        'maxlength'   => '20')).'
                        <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                    </label>
                </td>
                <td class="bottomLine centerCell"><label class="input">
                       <i class="icon-append fa fa-bookmark"></i>'.
                       form_input(array('class'       => 'validate[custom[onlyLetterNumber]] text-input', 
                                        'name'        => 'SMvc5_4', 
                                        'id'          => 'SMvc5_4',
                                        'placeholder' => "",
                                        'value'       => '',
                                        'maxlength'   => '10')).'
                        <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                    </label>
                </td>
            </tr>            
            </table>
            </section>
         </div>
     ';
echo "<a class='button' href='javascript:submitInAX(baseURL,\"formSM\",\"$id_in\",tipoAccion,\"SM\");'>Save Section</a>
      <span id='confirmSM' class='msjconfirm'></span>";
echo form_close();

?>
