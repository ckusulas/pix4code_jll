<?php 
echo form_open(base_url().'inmueble/guardar/'.$id_in.'/'.$accion.'/LT/'.$accion.'/'.$inmueble['frontPage'][0]['id_status'], array('id' => 'formLT','name' => 'formLT'));
echo '  <fieldset>
        <div class="row">
                <label class="col col-7 subtitulo">'.nbs(1).'</label>
                <section class="col col-4"><label class="checkbox" style="text-align: justify; text-justify: inter-word"><input type="checkbox" class="includedRep" name="report" id="letterCheckBox" value="LT" checked><i></i>Section included in the Valuation Report</label></section>
        </div>
        </fieldset>
        
        <div class="row">
            <section class="col col-5"></section>
            <section class="col col-5"><label class="label">Client Logo </label>
            <div id="fileUpLoaderClientLogo">&nbsp;Click para seleccionar Imagen&nbsp;</div>
               <div id="statusClientLogo"></div>'.
                    form_input(array('name'  => 'clientLogo', 
                                     'type'  => 'hidden', 
                                     'id'    => 'clientLogo',
                                     'value' => '')).'
              <div id="divClientLogo"></div>
            </section>
        </div>    
        <div class="row">
            <section class="col col-5"></section>
            <section class="col col-5"><label class="label">Mexico City,'.$fechaReporte.' </label></section>
        </div>    
        <div class="row">
            <section class="col col-5">
                     <label class="label">Client</label>
                     <label class="input">
                        <i class="icon-append fa fa-university"></i>'.
                        form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input', 
                                         'name'        => 'client', 
                                         'id'          => 'client',
                                         'placeholder' => "Client",
                                         'value'       => '',
                                         'maxlength'   => '80')).'
                         <b class="tooltip tooltip-bottom-right">Nombre del cliente al cual se genera el reporte</b>
                     </label>
             </section>
             <section class="col col-5">
                    <label class="label">Attn Officer</label>
                    <label class="input">
                       <i class="icon-append fa fa-male"></i>'.
                       form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input', 
                                        'name'        => 'atn_officer', 
                                        'id'          => 'atn_officer',
                                        'placeholder' => "Att'n Officer",
                                        'value'       => '',
                                        'maxlength'   => '80')).'
                        <b class="tooltip tooltip-bottom-right">Att\'n Officer</b>
                    </label>
            </section>
         </div>
         <div class="row">  
            <section class="col col-5">
                    <label class="label">Client Address</label>
                    <label class="input">
                    <i class="icon-append fa fa-road"></i>'.
                        form_input(array('class'     => 'validate[custom[onlyLetterNumber]] text-input',
                                         'name'      => 'client_addres',
                                         'id'        => 'client_addres',
                                         'maxlength' => '80',
                                         'value'     => '')).'
                        <b class="tooltip tooltip-bottom-right">Client Address</b>
                    </label>
                    <label class="input">
                    <i class="icon-append fa fa-road"></i>'.
                        form_input(array('class'     => 'validate[custom[onlyLetterNumber]] text-input',
                                         'name'      => 'client_addres_2',
                                         'id'        => 'client_addres_2',
                                         'maxlength' => '80',
                                         'value'     => '')).'
                        <b class="tooltip tooltip-bottom-right">Client Address</b>
                    </label>
            </section>
               <section class="col col-5">
                    <label class="label">Job Description Attn</label>
                    <label class="input">
                    <i class="icon-append fa fa-male"></i>'.
                        form_input(array('class'     => 'validate[custom[onlyLetterNumber]] text-input',
                                         'name'      => 'jd_atn_officer',
                                         'id'        => 'jd_atn_officer',
                                         'maxlength' => '80',
                                         'value'     => '')).'
                        <b class="tooltip tooltip-bottom-right">Job Description Attn</b>
                    </label>
                    <label class="input">
                    <i class="icon-append fa fa-male"></i>'.
                        form_input(array('class'     => 'validate[custom[onlyLetterNumber]] text-input',
                                         'name'      => 'jd_atn_officer_2',
                                         'id'        => 'jd_atn_officer_2',
                                         'maxlength' => '80',
                                         'value'     => '')).'
                        <b class="tooltip tooltip-bottom-right">Job Description Attn</b>
                    </label>
            </section>
         </div>
         
        <div class="row">  
            <section class="col col-10">
                       <label class="input">
                       <i class="icon-append fa fa-comment"></i>'.
                       form_input(array('class'       => 'validate[custom[onlyLetterNumber]] text-input',
                                        'name'        => 'LTparrafo1',
                                        'id'          => 'LTparrafo1',
                                        'placeholder' => "",
                                        'value'       => '',
                                        'maxlength'   => '100')).'
                        <b class="tooltip tooltip-bottom-right">Text</b>
                        </label>
                        <center> <div class="col col-10" id="dirClient"></div> </center>
            </section>
         </div>
         <div class="row">  
            <section class="col col-4"><label id="texto1Lbl" style="padding-top: 12px;" class="label">With the followings result as of: </label> </section>
        </div>            
        <div class="row">
            <section class="col col-3"><label class="checkbox"><input type="checkbox" name="LTchbox1" id="LTchbox1" value="1"><i></i>Included in report</label>
            </section>
            <section class="col col-8">
                <label class="input">
                <i class="icon-append fa fa-comment"></i>'.
                    form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input', 
                                    'name'        => 'LTtexto1', 
                                    'id'          => 'LTtexto1',
                                    'placeholder' => "",
                                    'value'       => '',
                                    'maxlength'   => '80')).'
                    <b class="tooltip tooltip-bottom-right">Text</b>
                </label>
            </section>
            <section class="col col-11"> <hr> </section>
        </section>
       </div>
       <div class="row">  
        <section class="col col-4"> <center><label id="AXmarketVal"    >0</label></center> </section>
        <section class="col col-4"> <center><label id="AXmarketValM2"  >0</label></center> </section>
        <section class="col col-4"> <center><label id="AXmarketValft2" >0</label></center> </section>
        <section class="col col-11"> <hr> </section>
       </div>
       <div class="row">
            <section class="col col-3"><label class="checkbox"><input type="checkbox" name="LTchbox2" id="LTchbox2" value="1"><i></i>Included in report</label>
            </section>
            <section class="col col-8">
                <label class="input">
                <i class="icon-append fa fa-comment"></i>'.
                    form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input', 
                                    'name'        => 'LTtexto2', 
                                    'id'          => 'LTtexto2',
                                    'placeholder' => "",
                                    'value'       => '',
                                    'maxlength'   => '80')).'
                    <b class="tooltip tooltip-bottom-right">Text</b>
                </label>
            </section>
            <section class="col col-11"> <hr> </section>
        </section>
       </div>
       <div class="row">  
        <section class="col col-4"> <center><label id="AX_VU_Approach" >0</label></center> </section>
        <section class="col col-4"> <center><label id="AXVUApproachM2" >0</label></center> </section>
        <section class="col col-4"> <center><label id="AXVUApproachft2" >0</label></center> </section>
        <section class="col col-11"> <hr> </section>
       </div>
       <div class="row">
            <section class="col col-3"><label class="checkbox"><input type="checkbox" name="LTchbox3" id="LTchbox3" value="1"><i></i>Included in report</label>
            </section>
            <section class="col col-8">
                <label class="input">
                <i class="icon-append fa fa-comment"></i>'.
                    form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input', 
                                    'name'        => 'LTtexto3', 
                                    'id'          => 'LTtexto3',
                                    'placeholder' => "",
                                    'value'       => '',
                                    'maxlength'   => '80')).'
                    <b class="tooltip tooltip-bottom-right">Text</b>
                </label>
            </section>
            <section class="col col-11"> <hr> </section>
        </section>
       </div>
       <div class="row">  
        <section class="col col-4"> <center><label id="AXIliquidValLbl"    >0</label></center> </section>
        <section class="col col-4"> <center><label id="AXIliquidValM2Lbl"  >0</label></center> </section>
        <section class="col col-4"> <center><label id="AXIliquidValft2Lbl" >0</label></center> </section>
        <section class="col col-11"> <hr> </section>
       </div>
       <div class="row">  
            <section class="col col-10">'.nbs(1).'
            </section>
        </div>
        <div class="row">  
            <section class="col col-10">'.
                       form_textarea(array('class' 	=> 'validate[custom[onlyLetterNumber]] text-input richTextSmallLT',
                                            'name' 	=> 'LTparrafo2',
                                            'id' 	=> 'LTparrafo2',
                                            'value'     => '')).'
            </section>
         </div>
         <div class="row">  
         <section class="col col-10">'.
                       form_textarea(array('class' 	=> 'validate[custom[onlyLetterNumber]] text-input richTextSmallLT',
                                            'name' 	=> 'LTparrafo3',
                                            'id' 	=> 'LTparrafo3',
                                            'value'     => '')).'
            </section>
         </div>        
         <div class="row">
         <section class="col col-5">
                <label class="select"><i class="icon-append"></i>'.                                    
                    form_dropdown('firma_prin', $teamjll, '0', 'id="firma_prin" class="validate[custom[requiredInFunction]] val"').'
                </label>         
        </section>
        <section class="col col-5">
                <label class="select"><i class="icon-append"></i>'.                                    
                    form_dropdown('firma_sec', $teamjll, '0', 'id="firma_sec" class="validate[custom[requiredInFunction]] val"').'
                </label>
                
        </section>  
         </div>        
        <fieldset>        
            <label id="firma_prinLbl" class="label col col-5" style="text-align: center;"></label>
            <label id="firma_secLbl" class="label col col-5" style="text-align: center;"></label>                                 
        </fieldset>          
        <fieldset>
        <div class="row"> <label class="label" style="font-size: 15px"><strong>DEFINITIONS</strong></label></div>
        <div class="row"> <label class="label" style="font-size: 15px">Definitions of pertinent terms taken from The Dictionary of Real Estate Appraisal, 5th ed. (Chicago: Appraisal Institute, 2010) are as follows:</label></div>                
        <div id="definiciones" class="row">
           <section class="col col-10"> <label class="checkbox"><input type="checkbox" name="allDef" id="allDef" value="0"-><i></i><strong>All</strong></label></section>           
        </div>
        <div  class="row">           
           <section class="col col-1"><img id="addDef" title="Agregar un nueva definicion" src="'.base_url().'images/new.png"/></section>  
        </div>
        <div id="newDef" class="row"> </div>
         </fieldset> 
     ';
echo "<a class='button' href='javascript:submitInAX(baseURL,\"formLT\",\"$id_in\",tipoAccion,\"LT\");'>Save Section</a>
      <span id='confirmLT' class='msjconfirm'></span>";
echo form_close();

?>
