<?php include("header.php"); ?>


<script>
var hrefF           = '<?php echo base_url(); ?>';
var registrosPagina = <?php echo $registrosPagina; ?>;


$(document).ready(function(){        
  
    $("#f5").datepicker({dateFormat: 'M d yy'});
    paginarInAX(1,registrosPagina,hrefF);
    
                       
});

</script>

<?php

    echo '<div class="body-filtros">';
          $attributes = array('class' => 'sky-form', 'id' => 'formaF');
    echo form_open(base_url().'inmueble/paginarAX/', $attributes);       
    echo form_fieldset();
    echo "              <div class='row'>
                        <section class='col col-6'>
                                <label class='input'>
                                        <i class='icon-append fa fa-university'></i>".
                                        form_input(array('class'       => 'validate[custom[onlyLetterNumber]] text-input', 
                                                         'name'        => 'f1', 
                                                         'id'          => 'f1',
                                                         'placeholder' => "Prepared for",                                                         
                                                         'maxlength'   => '30'))."                                         
                                </label>
                        </section>
                         <section class='col col-6'>
                                <label class='input'>
                                        <i class='icon-append fa fa-male'></i>".
                                        form_input(array('class'       => 'validate[custom[onlyLetterNumber]] text-input', 
                                                         'name'        => 'f2', 
                                                         'id'          => 'f2',
                                                         'placeholder' => "Property of",                                                         
                                                         'maxlength'   => '30'))."                                        
                                </label>
                        </section>
                        </div>
                        
                        <div class='row'>
                        <section class='col col-6'>
                                <label class='input'>
                                        <i class='icon-append fa fa-road'></i>".
                                        form_input(array('class'       => 'validate[custom[onlyLetterNumber]] text-input', 
                                                         'name'        => 'f3', 
                                                         'id'          => 'f3',
                                                         'placeholder' => "Location",                                                         
                                                         'maxlength'   => '30'))."                                        
                                </label>
                        </section>
                        <section class='col col-3'>
                                <label class='input'>
                                        <i class='icon-append fa fa-tags'></i>".
                                        form_input(array('class'       => 'validate[custom[onlyLetterNumber]] text-input', 
                                                         'name'        => 'f4', 
                                                         'id'          => 'f4',
                                                         'placeholder' => "Report Num",                                                         
                                                         'maxlength'   => '30'))."                                        
                                </label>
                        </section>
                        <section class='col col-3'>
                                <label class='input'>
                                        <i class='icon-append fa fa-calendar'></i>".
                                        form_input(array('class'       => 'validate[custom[dateED]] text-input datepicker', 
                                                         'name'        => 'f5', 
                                                         'id'          => 'f5',
                                                         'placeholder' => "Effective Date",
                                                         'maxlength'   => '30'))."                                        
                                </label>
                        </section>
                        </div>
                        
                        <div class='row'>                        
                        <section>
                             <label class='label col col-1'>Status</label>
                               <div class='inline-group'>"
                                       .$status.
                               "</div>
                               <a class='button' href='javascript:submitFiltros(\"formaF\",registrosPagina,hrefF);'>Search</a>
                               <a class='button button-secondary' href='javascript:resetFiltros();'>Reset</a>
                        </section>                        
                        </div>
        ";   
    echo form_fieldset_close();    
    echo form_close();
    echo '</div>';        
    echo '<section class="col col-8">
            <label class="subtitulo">
                <a href="'.base_url().'inmueble/forma/N/">
                <img title="Agregar nuevo Trabajo" src="'.base_url().'images/new.png"/> <img title="Agregar nuevo Trabajo al sistema HERMES" src="'.base_url().'images/portafolio.png"/>New valuation Report</a>
            </label>
         </section> ' ;
    echo br(2);
    
    echo $gridInmuebles;
       
    echo br(2).'<div id="linksPaginar"></div><span id="spinPaginar"></span>';

    echo br(2);
    include("footer.php");
?>  