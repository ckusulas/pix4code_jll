
<?php
    
echo "              <div class='row'>
                        <section class='col col-3'>
                                <label class='input'>
                                        <i class='icon-append fa fa-university'></i>".
                                        form_input(array('class'=>'validate[custom[onlyLetterNumber]] text-input', 'name'=>'f1'.$tipoComparable, 'id'=>'f1'.$tipoComparable, 'placeholder'=>"Type of property", 'maxlength'=>'30'))."                                         
                                </label>
                        </section>
                        <section class='col col-3'>
                                <label class='input'>
                                        <i class='icon-append fa fa-road'></i>".
                                        form_input(array('class'=>'validate[custom[onlyLetterNumber]] text-input','name'=>'f3'.$tipoComparable,'id'=>'f3'.$tipoComparable,'placeholder'=>"Location",'maxlength'=>'30'))."                                   
                                </label>
                        </section>
                         <section class='col col-3'>
                                <label class='input'>
                                        <i class='icon-append fa fa-usd'></i>".
                                        form_input(array('class'=>'validate[custom[number]] text-input', 'name'=>'f2Ini'.$tipoComparable,'id'=>'f2Ini'.$tipoComparable,'placeholder'=>"Price From",'maxlength'=>'30'))."                                        
                                </label>
                        </section>
                         <section class='col col-3'>
                                <label class='input'>
                                        <i class='icon-append fa fa-usd'></i>".
                                        form_input(array('class'=>'validate[custom[number]] text-input', 'name'=>'f2Fin'.$tipoComparable,'id'=>'f2Fin'.$tipoComparable,'placeholder'=>"Price to",'maxlength'=>'30'))."                                        
                                </label>
                        </section>
                    </div>

                    <div class='row'>
                        <section class='col col-3'>
                                <label class='input'>
                                        <i class='icon-append fa fa-calendar'></i>".
                                        form_input(array('class'=>'validate[custom[dateED]] text-input datepicker','name'=>'f4'.$tipoComparable,'id'=>'f4'.$tipoComparable,'placeholder'=>"Closing/Listing From",'maxlength'=>'30'))."                                   
                                </label>
                        </section>
                        <section class='col col-3'>
                                <label class='input'>
                                        <i class='icon-append fa fa-calendar'></i>".
                                        form_input(array('class'=>'validate[custom[dateED]] text-input datepicker','name'=> 'f5'.$tipoComparable,'id'=>'f5'.$tipoComparable,'placeholder' => "Closing/Listing To",'maxlength' =>'30'))."
                                </label>
                        </section>
                        <section class='col col-3'>
                                <label class='input'>
                                        <i class='icon-append fa fa-usd'></i>".
                                        form_input(array('class'=>'validate[custom[number]] text-input', 'name'=>'f6Ini'.$tipoComparable,'id'=>'f6Ini'.$tipoComparable,'placeholder'=>"Land m2 From",'maxlength'=>'30'))."                                        
                                </label>
                        </section>
                         <section class='col col-3'>
                                <label class='input'>
                                        <i class='icon-append fa fa-usd'></i>".
                                        form_input(array('class'=>'validate[custom[number]] text-input', 'name'=>'f6Fin'.$tipoComparable,'id'=>'f6Fin'.$tipoComparable,'placeholder'=>"Land m2 to",'maxlength'=>'30'))."                                        
                                </label>
                        </section>
                        </div>";