<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!-- saved from url=(0034)http://www.jll.com.mx/mexico/es-mx -->
<html dir="ltr" lang="es-MX" style=""><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta http-equiv="Expires" content="0"/>	
<meta name="description" content="Jones Lang LaSalle is a global real estate services firm specializing in commercial property management, leasing, and investment management."/>
<meta content="Commercial real estate services. Jones Lang LaSalle."/>
<meta content="Jones Lang LaSalle is a global real estate services firm specializing in commercial property management, leasing, and investment management."/>

<title>Commercial real estate services. Jones Lang LaSalle.</title>
<link id="JLLCommonDesktop" rel="stylesheet" type="text/css" href="http://localhost/hermes/css/jllStyle.css"/>

<body>

<div id="s4-workspace" class="ms-core-overlay" style="height: 745px; width: 1535px;">
    <div id="s4-bodyContainer">
        <div class="container">
            <header class="clearfix noindex">
                        <div class="logo">
                            <a id="ctl00_logoLink" href="http://www.jll.com.mx/mexico/es-mx/">
                                <img id="logoImg" src="http://localhost/hermes/jllSite/files//logo-noReg1.png" alt="JLL Logo" title="JLL: Real value in a changing world">
                            </a>
                        </div>
                        <div class="secondary_navigation_container noindex">
                            <ul class="language_navigation clearfix"> 
                                <li class="country_selector">
                                <a href="http://www.jll.com.mx/" ><span class="jll_OpenPicker">México</span></a>
                                </li>
                            </ul>
                        </div>
                <ul class="primary_navigation">
                        <li><a title="Investigación" href="/mexico/es-mx/investigacion-de-mercados">Investigación</a></li>
                        <li class="selected"><a title="Oficinas" href="/mexico/es-mx/nuestras-oficinas">Oficinas</a></li>
                        <li><a title="Propiedades" href="http://www.jllproperty.com.mx/es-mx">Propiedades</a></li>
                        <li><a title="Noticias" href="/mexico/es-mx/noticias">Noticias</a></li>
                        <li><a title="Acerca de nosotros" href="/mexico/es-mx/acerca-de-nosotros">Acerca de nosotros</a></li>
                </ul>
            </header>
            <!-- End header -->                       
            <div id="contentRow">

            <div id="contentBox" aria-live="polite" aria-relevant="all">

                <div id="DeltaPlaceHolderMain">

                    <div id="defaultChannelDiv">
                        <div>
                                <div class="defaulthomepagecontent_nosubfooter">
                                    <section class="clearfix">
                                cuerpo
                                <p>sdf</p>
                                <p>sdf</p>
                                <p>sdf</p>v<p>sdf</p>
                                    </section>
                                </div> 
                         </div>
                        </div> 
                </div>   
            </div>
            </div>
            <!-- FOOTER -->
            <footer class="noindex">
            <ul>
                <li><a id="ctl00_JLLTranslatedLink2" href="http://www.jll.com.mx/mexico/es-mx/declaracion-de-privacidad">Aviso de privacidad</a></li>
                <li><a id="ctl00_JLLTranslatedLink3" href="http://www.jll.com.mx/mexico/es-mx/condiciones-de-uso">Condiciones de uso</a></li>        	                  
                </ul>
            <div class="copyright"> © <span>Copyright</span> 2016<span>Jones Lang LaSalle México, IP, Inc.</span></div>
            </footer>
            <!-- End footer -->             
            <!-- closes container -->
</div>
</div>        
</div>                    

</body></html>