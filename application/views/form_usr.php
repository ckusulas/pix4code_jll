<?php include("header.php"); ?>

<script>
    
    var baseURL    = '<?php echo base_url(); ?>';
    var tipoAccion = '<?php echo $accion; ?>';    
    
    $(document).ready(function(){      
        
    $('#fileUploaderFotoPerfil').addClass('pointer');    
    subirImagenProfile(baseURL);
    $("#profile option[value=0]").attr('disabled','disabled');
    
        
    });//document ready
  
</script>

<?php
    echo '<div class="body-form">';
          $attributes = array('class' => 'sky-form', 'id' => 'form_usr','name' => 'form_usr');
    echo form_open(base_url().'usuario/guardar/', $attributes);   
    
    echo form_input(array('name'  => 'accion', 
                          'type'  => 'hidden', 
                          'id'    => 'accion',
                          'value' => $accion));
    echo form_input(array('name'  => 'foto', 
                          'type'  => 'hidden', 
                          'id'    => 'foto',
                          'value' => $fotoIni));
    echo '<header>'.$titulos["titulo"].'</header>';
    
    echo '<fieldset>
                    <div class="row">
                            <section class="col col-6 ">
                                    <label class="label">First Name</label>
                                    <label class="input">
                                            <i class="icon-append fa fa-user"></i>'.                                            
                                            form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input', 
                                                             'name'        => 'firstname', 
                                                             'id'          => 'firstname',
                                                             'placeholder' => "First name",
                                                             'value'       => $usr[0]['nombre'],						 
                                                             'maxlength'   => '20')).'                                            
                                    </label>
                            </section>
                            <section class="col col-6">
                                    <label class="label">Last Name</label>
                                    <label class="input">
                                            <i class="icon-append fa fa-user"></i>'.                                           
                                            form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input', 
                                                             'name'        => 'lastname', 
                                                             'id'          => 'lastname',
                                                             'placeholder' => "Last name",
                                                             'value'       => $usr[0]['apellidos'],						 
                                                             'maxlength'   => '20')).'
                                    </label>
                            </section>
                    </div>
                    <div class="row">
                            <section class="col col-6">
                                            <label class="label">Picture</label>
                                            <div id="fotoPerfil">'.img( array('src' =>  ( empty($fotoIni)?base_url().'images/logoJLL.jpg':base_url().$dirFoto.$fotoIni),'class'=>"img-circle", "width"=>"55", "heigth"=>"55")).'
                                            </div><br>
                                            <div id="fileUploaderFotoPerfil">&nbsp;Click para seleccionar Imagen&nbsp;</div>
                                            <div id="statusPF"></div>
                                    
                            </section>
                           <section class="col col-6">
                                    <label class="label">Profile</label>
                                    <label class="select"><i class="icon-append"></i>'.                                    
                                        form_dropdown('profile', $profiles, $usr[0]['tipo'],$attP).'                                            
                                    </label>
                            </section>                            
                    </div>
                    <section>
                            <label class="label">Job</label>
                            <label class="input">
                                    <i class="icon-append fa fa-suitcase"></i>'.
                                    form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input', 
                                                     'name'        => 'job', 
                                                     'id'          => 'job',
                                                     'placeholder' => "job",
                                                     'value'       => $usr[0]['puesto'],						 
                                                     'maxlength'   => '60')).'
                                    <b class="tooltip tooltip-bottom-right">Job Description</b>
                            </label>
                    </section>
            </fieldset>
            <fieldset>
                <div class="row">
                            <section class="col col-5">
                            <label class="label">Job</label>
                            <label class="input">
                                    <i class="icon-append fa fa-suitcase"></i>'.
                                    form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input', 
                                                     'name'        => 'titulo', 
                                                     'id'          => 'titulo',
                                                     'placeholder' => "Job Title. Ej MBA",
                                                     'value'       => $usr[0]['titulo'],						 
                                                     'maxlength'   => '60')).'
                                    <b class="tooltip tooltip-bottom-right">Job Title</b>
                            </label>
                            </section>
                </div>
                <div class="row">
                            <section class="col col-5">
                            <label class="label">Job</label>
                            <label class="input">
                                    <i class="icon-append fa fa-suitcase"></i>'.
                                    form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input', 
                                                     'name'        => 'telefono', 
                                                     'id'          => 'telefono',
                                                     'placeholder' => "Phone",
                                                     'value'       => $usr[0]['telefono'],						 
                                                     'maxlength'   => '20')).'
                                    <b class="tooltip tooltip-bottom-right">Phone</b>
                            </label>
                            </section>
                </div>
            </fieldset>               
            <fieldset>
                    <section>
                            <label class="label">Email</label>
                            <label class="input">
                                    <i class="icon-append fa fa-envelope-o"></i>'.
                                    form_input(array('class'       => 'validate[required,custom[email]] text-input',
                                                     'name'        => 'correo',
                                                     'id'          => 'correo',
                                                     'placeholder' => "Email address username",
                                                     'value'       => $usr[0]['correo'],
                                                     'maxlength'   => '30',
                                                     'onChange'    => 'javascript:validateDuplicateAX(baseURL,this)'
                                                    )                                               
                                               ).'
                                    <b class="tooltip tooltip-bottom-right">Email Address</b>
                            </label><span id="msjCorreo" class="errorMsg" style="color:red; size:14px;"></span>
                    </section>
                    <div class="row">
                            <section class="col col-6 ">
                                    <label class="label">Password</label>
                                    <label class="input">
                                        <i class="icon-append fa fa-lock"></i>'.
                                        form_input(array('type'        => 'password',
                                                         'class'       => 'validate[required,custom[onlyLetterNumber]] text-input', 
                                                         'name'        => 'pwd', 
                                                         'id'          => 'pwd',
                                                         'placeholder' => "Password",
                                                         'value'       => $usr[0]['pwd'],						 
                                                         'maxlength'   => '20')).'                                                                      
                                        <b class="tooltip tooltip-bottom-right">Dont forget your password</b>
                                    </label>
                            </section>
                            <section class="col col-6">
                                    <label class="label">Confirm Password</label>
                                    <label class="input">
                                        <i class="icon-append fa fa-lock"></i>'.
                                        form_input(array('type'        => 'password',
                                                         'class'       => 'validate[required,custom[confirmPwd]] text-input', 
                                                         'name'        => 'pwdC', 
                                                         'id'          => 'pwdC',
                                                         'placeholder' => "Confirm password",
                                                         'value'       => $usr[0]['pwd'],						 
                                                         'maxlength'   => '20')).'                                                                
                                        <b class="tooltip tooltip-bottom-right">Dont forget your password</b>
                                    </label>
                            </section>
                    </div>                    
            </fieldset>';

    
    echo "<footer>\n
            <a class='button' href='javascript:submitForm(\"form_usr\");'>Save</a>
            <a class='button button-secondary' href='".base_url()."usuario/'>Exit</a>
         </footer>\n";
                           
    echo form_close();
    echo '</div>';
            
    echo br(2);	
    include("footer.php"); 
?>  