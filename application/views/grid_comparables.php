<?php include("header.php"); ?>
<script>
var hrefF           = '<?php echo base_url();       ?>';
var tipoComp        = '<?php echo $tipoComp;        ?>';
var registrosPagina =  <?php echo $registrosPagina; ?> ;

$(document).ready(function(){
                                paginarCompAX(1,registrosPagina,hrefF,tipoComp,false,null,null,null);                                
                                $("#f4").datepicker({dateFormat: 'M d yy'});
                                $("#f5").datepicker({dateFormat: 'M d yy'});
                                
                                /******************* GENERATED REPORT INI ******************/        
                                $('#excelLink').click(function() { exportaComparablesAX($("input[name=tipoCompRadio]:checked").val()
                                                                                       ,$("#f1"   ).val()
                                                                                       ,$("#f3"   ).val()
                                                                                       ,$("#f2Ini").val()
                                                                                       ,$("#f2Fin").val()
                                                                                       ,$("#f4"   ).val()
                                                                                       ,$("#f5"   ).val()
                                                                                       ,$("#f6Ini").val()
                                                                                       ,$("#f6Fin").val()                                                                                       
                                                                                       ,hrefF); });
                                /******************* GENERATED REPORT FIN ******************/                                    
                            });

</script>

<?php

echo '<div class="body-filtros">';          
echo form_open(base_url().'comparables/paginarAX/', array('class' => 'sky-form', 'id' => 'formaF'));       
echo form_fieldset();

$tipoComparable = NULL;
include("grid_filtros_comp.php");  

echo "
                        <div class='row'>                        
                        <section class='col col-10'>
                             <label class='label col col-1'>Type</label>
                               <div class='inline-group'>"
                                       .$tipoComRadio.
                               "
                               <a class='button' href='javascript:submitCompFiltros(\"formaF\",registrosPagina,hrefF,$(\"input[name=tipoCompRadio]:checked\").val(),false,null);'>Search</a>
                               <a class='button button-secondary' href='javascript:resetCompFiltros();'>Reset</a>
                               </div>
                        </section>                        
                    </div>
    ";   
echo form_fieldset_close();    
echo form_close();
echo '</div>';

echo br(1).'<div class="row">                
                <section class="col col-10">
                  <label class="subtitulo">
                     '.nbs(5).$titulos["titulo"].nbs(strlen($titulos["titulo"])>22?10:65).' <a href="'.base_url().'comparables/forma/CLS/N/"> <img title="Agregar un nuevo comparable" src="'.base_url().'images/new.png"/><img title="Agregar Nuevo Comparable Land Sale" src="'.base_url().'images/bank.png"/> Land Sale</a>
                  </label>
                  <label class="subtitulo">
                     '.nbs(7).' <a href="'.base_url().'comparables/forma/CRL/N/"><img title="Agregar un nuevo comparable" src="'.base_url().'images/new.png"/><img title="Agregar Nuevo Comparable Ref Lease" src="'.base_url().'images/bank.png"/> Ref Lease</a>
                  </label>
                  <label class="subtitulo">
                     '.nbs(7).' <a href="'.base_url().'comparables/forma/CRS/N/"><img title="Agregar un nuevo comparable" src="'.base_url().'images/new.png"/><img title="Agregar Nuevo Comparable Ref Sale" src="'.base_url().'images/bank.png"/> Ref Sale</a>
                  </label>
                   '.nbs(5).' <span id="excelLink" class="pointer">'.img( array('src' =>  base_url()."images/filedownload.png",'boder'=>0,'title' => 'DownLoad')).nbs(2).'<i class="fa fa-file-excel-o"></i>'.nbs(2).'Download </span>
                 </section>       
                 <CENTER><span id="generaDOC"></span></CENTER>
            </div>'. br(1);
echo br(1);

echo $gridComp.br(2).'<div id="linksPaginar"></div><span id="spinPaginar"></span>';

echo br(2);

include("footer.php"); ?>