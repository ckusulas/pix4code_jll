<?php 

echo form_open(base_url().'inmueble/guardar/'.$id_in.'/'.$accion.'/FP/'.$accion.'/'.$inmueble['frontPage'][0]['id_status'], array('id' => 'formFP','name' => 'formFP'));
echo form_input(array('name'=>'exchange_rate','type'=>'hidden','id'=>'exchange_rate','value'=>$er));
echo br(1).'
        <fieldset>
        <div class="row">
                <label class="col col-7 subtitulo">Front Page Valuation Report</label>
                <section class="col col-4"><label class="checkbox" style="text-align: justify; text-justify: inter-word"><input type="checkbox" class="includedRep" name="report" value="FP" '.$reportPart.'><i></i>Section Included In The Valuation Report</label></section>
        </div>
        <section class="col col-10">
                 <label class="label">Prepared For</label>
                 <label class="input">
                    <i class="icon-append fa fa-university"></i>'.
                    form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input', 
                                     'name'        => 'preparedFor', 
                                     'id'          => 'preparedFor',
                                     'placeholder' => "Prepared For",
                                     'value'       => $inmueble['frontPage'][0]['preparedFor'],
                                     'onChange'    => "nomenclaturaRepNum(baseURL,'.$id_in.',null);",
                                     'maxlength'   => '40')).'
                     <b class="tooltip tooltip-bottom-right">Nombre del cliente al cual se genera el reporte</b>
                 </label>
         </section>
           
         <section class="col col-10">
                 <label class="label">Property of</label>
                 <label class="input">
                    <i class="icon-append fa fa-male"></i>'.
                    form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input', 
                                     'name'        => 'propertyOf', 
                                     'id'          => 'propertyOf',
                                     'placeholder' => "Property of",
                                     'value'       => $inmueble['frontPage'][0]['propertyOf'],						 
                                     'maxlength'   => '40')).'
                     <b class="tooltip tooltip-bottom-right">Dueño de la propiedad</b>
                 </label>
         </section>
         </fieldset>
         
        <fieldset>
        <div class="row">
                <label class="subtitulo">BUILDING APPRAISAL</label>							
        </div>
        <section class="col col-5">'.br(3).'La imagen de la portada, para su correcta visualización debe tener las siguientes medidas: 1083 pixeles o 18.3 cm de ancho por 1083 pixeles o 18.3 cm de largo 
               <div id="mulitplefileuploaderFotoPortada">&nbsp;Click para seleccionar Imagen&nbsp;</div>
               <div id="statusFotoPortada"></div>'.
               form_input(array('name'  => 'fotoPortada', 
                                'type'  => 'hidden', 
                                'id'    => 'fotoPortada',
                                'value' => $inmueble['frontPage'][0]['fotoPortada'])).' 
        </section>
        <section class="col col-6">
               <div id="divFotoPortada">'.img( array('width'=>'412','height'=>'418','src' =>  ($inmueble['frontPage'][0]['fotoPortada']=='logoJLL.jpg')?base_url().$dirGallery.$inmueble['frontPage'][0]['fotoPortada']:$dirGallery.$id_in."/".$inmueble['frontPage'][0]['fotoPortada'],'title' => 'Imagen de la Portada del Reporte de Valuacion')).'<br><br>'
                                         .img( array('src' =>  base_url().'/images/close.png','title' => 'Borrar Foto de Portada','class'=>'imgFP','name'=>$inmueble['frontPage'][0]['fotoPortada'],'id'=>$inmueble['frontPage'][0]['fotoPortada'])).
               '</div>
        </section>
        </fieldset>

        <fieldset> 
        <div class="row">
                <label class="label">LOCATION OF THE PROPERTY</label>							
        </div>
        <section class="col col-4">
                 <label class="label">Street/Av</label>
                 <label class="input">
                    <i class="icon-append fa fa-road"></i>'.
                    form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input', 
                                     'name'        => 'calle', 
                                     'id'          => 'calle',
                                     'placeholder' => "Street/Av",                                     
                                     'value'       => $inmueble['frontPage'][0]['calle'],						 
                                     'maxlength'   => '40')).'                  
                 </label>
         </section>
                 
        <section class="col col-2">
                 <label class="label">No</label>
                 <label class="input">
                    <i class="icon-append fa fa-road"></i>'.
                    form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input', 
                                     'name'        => 'num', 
                                     'id'          => 'num',
                                     'placeholder' => "No",
                                     'value'       => $inmueble['frontPage'][0]['num'],						 
                                     'maxlength'   => '30')).'                  
                 </label>
         </section>
         
        <section class="col col-4">
                 <label class="label">Neighborhood</label>
                 <label class="input">
                    <i class="icon-append fa fa-road"></i>'.
                    form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input', 
                                     'name'        => 'col', 
                                     'id'          => 'col',                                     
                                     'placeholder' => "Neighborhood",
                                     'value'       => $inmueble['frontPage'][0]['col'],						 
                                     'maxlength'   => '40')).'                  
                 </label>
         </section>       
                
        <section class="col col-4">
                 <label class="label">City</label>
                 <label class="input">
                    <i class="icon-append fa fa-road"></i>'.
                    form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input', 
                                     'name'        => 'mun', 
                                     'id'          => 'mun',
                                     'placeholder' => "City",                                     
                                     'value'       => $inmueble['frontPage'][0]['mun'],						 
                                     'maxlength'   => '40')).'                  
                 </label>
         </section>
         
        <section class="col col-4">
                 <label class="label">State</label>
                 <label class="input">
                    <i class="icon-append fa fa-road"></i>'.
                    form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input', 
                                     'name'        => 'edo', 
                                     'id'          => 'edo',
                                     'placeholder' => "State",
                                     'value'       => $inmueble['frontPage'][0]['edo'],						 
                                     'maxlength'   => '40')).'                  
                 </label>
         </section>
         
        <section class="col col-2">
                 <label class="label">Zipcode</label>
                 <label class="input">
                    <i class="icon-append fa fa-road"></i>'.
                    form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input', 
                                     'name'        => 'cp', 
                                     'id'          => 'cp',
                                     'placeholder' => "Zipcode",
                                     'value'       => $inmueble['frontPage'][0]['cp'],						 
                                     'maxlength'   => '15')).'                  
                 </label>
         </section>
         </fieldset>
         
        <fieldset> 
        '.
                    form_input(array('name'        => 'address_jll', 
                                     'id'          => 'address_jll',                                     
                                     'value'       => $inmueble['frontPage'][0]['address_jll'],
                                     'type'=>'hidden',
                                     'maxlength'   => '100')).'
        
       <div class="row">
                <section class="col col-3"><a name="mt_et"></a>
                  <label class="label">Marketing Time: (months)</label>
                  <label class="input">
                    <i class="icon-append fa fa-calendar"></i>'.
                    form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input', 
                                     'name'        => 'mt', 
                                     'id'          => 'mt',                                     
                                     'value'       => $inmueble['frontPage'][0]['mt'],						 
                                     'maxlength'   => '10')).'
                   </label>
                </section>
                <section class="col col-2"></section>
                <section class="col col-3">
                  <label class="label">Exposure Time: (months)</label>
                  <label class="input">
                    <i class="icon-append fa fa-calendar"></i>'.
                    form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input', 
                                     'name'        => 'et', 
                                     'id'          => 'et',                                     
                                     'value'       => $inmueble['frontPage'][0]['et'],                                      
                                     'maxlength'   => '10')).'
                  </label>
                </section>
                <section class="col col-2"></section>
        </div>        
        </fieldset>
         
        <fieldset>
        <div class="row">
                <label class="label">Nomenclatura</label>							
        </div>
        <section class="col col-3">
                 <label class="label">Origen</label>
                 <label class="select">'.
                    form_dropdown('origen_name', $origenOp, $inmueble['frontPage'][0]['origen_name'], 'id="origen_name" class="validate[custom[requiredInFunction]]" onChange="nomenclaturaRepNum(baseURL,'.$id_in.',null);" ').'
                <i></i> 
                </label>   
         </section>
           
         <section  class="col col-3">
                 <label class="label">Concepto</label>
                 <label class="select">'.
                 form_dropdown('concepto_name', $conceptos, $inmueble['frontPage'][0]['concepto_name'], 'id="concepto_name" class="validate[custom[requiredInFunction]]" onChange="nomenclaturaRepNum(baseURL,'.$id_in.',null);"').'
                <i></i> 
                </label>   
         </section>        

        <section  class="col col-3">
                 <label class="label">Clave Año</label>'.
                 $inmueble['frontPage'][0]['clave_ano_name'].
                form_input(array('name'=>'clave_ano_name','type'=>'hidden','id'=>'clave_ano_name','value'=>$inmueble['frontPage'][0]['clave_ano_name'])).'
         </section>         
         
        <section  class="col col-3">
                 <label class="label">Consecutivo</label>
                 <label id="consecutivoNameLbl">'.$inmueble['frontPage'][0]['consecutivo_name'].'</label>'.
                  form_input(array('name'=>'consecutivo_name','type'=>'hidden','id'=>'consecutivo_name','value'=>$inmueble['frontPage'][0]['consecutivo_name'])).
                  form_input(array('name'=>'appraisal_num','type'=>'hidden','id'=>'appraisal_num','value'=>$inmueble['frontPage'][0]['appraisal_num'])).'
         </section>
         </fieldset>
        <fieldset>
        <section class="col col-3">
                 <label class="label">Effective date of the appraisal</label>
                 <label class="input">
                    <i class="icon-append fa fa-calendar"></i>'.
                    form_input(array('class'       => 'validate[required,custom[dateED]] text-input datepicker', 
                                     'name'        => 'effectiveDate', 
                                     'id'          => 'effectiveDate',
                                     'placeholder' => "Effective date of the appraisal",
                                     'value'       => $inmueble['frontPage'][0]['effectiveDate'],						 
                                     'maxlength'   => '20')).'
                     <b class="tooltip tooltip-bottom-right">Fecha del Reporte</b>
                 </label>
         </section>
           
         <section class="col col-5">
                 <label class="label">Report Number</label>
                 <label id="lblrepNum" class="label">'.$inmueble['frontPage'][0]['repNum'].' +</label>
         </section>           
         <section class="col col-4">
                 <label class="label">Customer reference in Report Number</label>
                 <label class="input">
                 <i class="icon-append fa fa-tag"></i>'.
                    form_input(array('name'=>'repNumCap','id'=>'repNumCap','class'=>'validate[required,custom[onlyLetterNumber]] text-input','placeholder' => "Customer reference in Report Number",'value'=>$inmueble['frontPage'][0]['repNumCap'],'onChange'=>"nomenclaturaRepNum(baseURL,".$id_in.",null);", )).'
                 <b class="tooltip tooltip-bottom-right">Customer reference in Report Number. Example: IND-16-001-GE_StaFe</b>
                 </label>'.
                 form_input(array('name'=>'repNum','type'=>'hidden','id'=>'repNum','value'=>$inmueble['frontPage'][0]['repNum'])).'
         </section>
         </fieldset>
     ';

 echo "<a class='button' href='javascript:submitInAX(baseURL,\"formFP\",\"$id_in\",tipoAccion,\"FP\");'>Save Section</a>
      <span id='confirmFP' class='msjconfirm'></span>";
 echo br(2);
 echo form_close();

?>
