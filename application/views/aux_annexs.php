
<?php
        echo '<!-- tabs -->
			<div class="sky-tabs sky-tabs-pos-top-left sky-tabs-anim-flip sky-tabs-response-to-icons">
				<input type="radio" name="sky-tabs-2" tipoComp="CLS" value="AA1" id="sky-tab2-1" class="sky-tab-content-1">
				<label for="sky-tab2-1"><span><span><i class="fa fa-bolt"></i>A.1.Land Sales</span></span></label>
				
				<input type="radio" name="sky-tabs-2" tipoComp="CLS" value="AA2" id="sky-tab2-2" class="sky-tab-content-2">
				<label for="sky-tab2-2"><span><span><i class="fa fa-bars"></i>A.2.-Land Adjustments</span></span></label>
				
				<input type="radio" name="sky-tabs-2" tipoComp="CRL" value="AA3" id="sky-tab2-3" class="sky-tab-content-3">
				<label for="sky-tab2-3"><span><span><i class="fa fa-bolt"></i>A.3.RefLease</span></span></label>
				
				<input type="radio" name="sky-tabs-2" tipoComp="CRL" value="AA4" id="sky-tab2-4" class="sky-tab-content-4">
				<label for="sky-tab2-4"><span><span><i class="fa fa-bars"></i>A.4.-LeaseGrid</span></span></label>
                                
                                <input type="radio" name="sky-tabs-2" tipoComp="CRS" value="AA5" id="sky-tab2-5" class="sky-tab-content-5">
				<label for="sky-tab2-5"><span><span><i class="fa fa-bolt"></i>A.5.Ref Sale</span></span></label>
                                
                                <input type="radio" name="sky-tabs-2" tipoComp="CRS" value="AA6" id="sky-tab2-6" class="sky-tab-content-6">
				<label for="sky-tab2-6"><span><span><i class="fa fa-bars"></i>A.6.-Sale Grid</span></span></label>

                                <input type="radio" name="sky-tabs-2" tipoComp="" value="AA8" id="sky-tab2-8" class="sky-tab-content-8">
				<label for="sky-tab2-8"><span><span><i class="fa fa-picture-o"></i>A.7.- Photos & Plans</span></span></label>
                                
                                <input type="radio" name="sky-tabs-2" tipoComp="" value="AA9" id="sky-tab2-9" class="sky-tab-content-9">
				<label for="sky-tab2-9"><span><span><i class="fa fa-picture-o"></i>A.8.- Assumptions & Certification</span></span></label>                               

				<ul>
					<li class="sky-tab-content-1">';
                                            include("aux_a1_land_sales.php");
        echo '                          </li>
					<li class="sky-tab-content-2">';
                                            include("aux_a2_land_adjustments.php");
        echo '                          </li>
					<li class="sky-tab-content-3">';
                                            include("aux_a3_ref_lease.php");    
        echo '                          </li>
					<li class="sky-tab-content-4">';
                                            include("aux_a4_lease_grid.php");
        echo '                          </li>	
					<li class="sky-tab-content-5">';
                                            include("aux_a5_ref_sale.php");
        echo '              		</li>					                                        
					<li class="sky-tab-content-6">';
                                            include("aux_a6_ref_sale_grid.php");
        echo '                          </li>			
					<li class="sky-tab-content-8">';
                                            include("aux_an_ppl.php");
        echo '                          </li>
                                        <li class="sky-tab-content-9">';
                                            include("aux_an_lim_cert.php");
        echo '                          </li>

				</ul>
			</div>
		<!--/ tabs -->';
?>  