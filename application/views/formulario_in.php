<?php include("header.php"); ?>

<script>    
    var baseURL             = '<?php echo base_url(); ?>';
    var tipoAccion          = '<?php echo $accion; ?>';    
    var registrosPagina     =  <?php echo $registrosPagina; ?>;
    var comparablesElegidos = [];
    var comparablesElegidosBorrados = [];
    
$(document).ready(function(){


    if($("#AI_Text11").html()==0){
     $("#AI_Text11").html("");

    }
    /****************** CARGA SECCIONES INI ******************/
    $("input[name='sky-tabs']").click(function (){ cargaSeccionInAX(baseURL,$(this).val(),$("#id_in").val()); });
    /****************** CARGA SECCIONES FIN ******************/
    
    /****************** CARGA AP INI ******************/
    $("input[name='sky-tabs-3']").click(function (){ cargaAPInAX(baseURL,$(this).val(),$("#id_in").val(),false,""); });
    /****************** CARGA AP FIN ******************/
    
    /****************** CARGA ANEXOS INI ******************/
    $("input[name='sky-tabs-2']").click(function (){ cargaAnexoInAX(baseURL,$(this).val(),$(this).attr("tipoComp"),$("#id_in").val()); });    
    /******************* CARGA ANEXOS FIN ******************/
    
    /******************* GENERATED REPORT INI ******************/        
    $('#wordLink').click(function() {  var report = [];
                                       var ann_in = [];
                                       var ap_in  = [];
                                       var idiom  = $('input[name=idiom]:checked').val();
                                       $("input[name='report']:checked" ).each(function() { report.push($(this).val()); });
                                       $("input[name='ann_in']:checked" ).each(function() { ann_in.push($(this).val()); });
                                       $("input[name='ap_in']:checked"  ).each(function() { ap_in.push($(this).val());  });
                                       
                                       $("#formEXP").validationEngine();
                                       if($("#formEXP").validationEngine('validate'))
                                        { var dateTime    = new Date($("#fecha_name").datepicker("getDate"));
                                          var strDateTime = dateTime.getDate()+"-"+(dateTime.getMonth()+1);
                                          var nomen       = $("#repNum").val()+"_"+strDateTime;
                                          
                                          exportaInmuebleAX('<?php echo $id_in; ?>',baseURL,'W',report,ann_in,ap_in,$("#fecha_name").val(),nomen,idiom); }
                                    });
/******************* GENERATED REPORT FIN ******************/                                    
    
    if(tipoAccion =="E")
    {
         /*********** Front Page INI **********/
            $('.imgFP').addClass('pointer');
            $('.imgFP').click(function(i) { jConfirm("¿Borrar Foto de Portada?","HERMES PROJECT VALUATION REPORT", function(r) {
                                                     if(r) { borraImagenCargadaFotoPortadaAX(baseURL,$(i));   }                         
                                                   });
                                          });        
        /*********** Front Page FIN **********/
    }
    
    /*********** Front Page INI **********/
    $("#effectiveDate").datepicker({dateFormat: 'M d yy'});
    $("#fecha_name"   ).datepicker({dateFormat: 'M d yy'});
    $('#mulitplefileuploaderFotoPortada').addClass('pointer');
    subirImagenFotoPortada(baseURL);
    $("#updateER").addClass('pointer');
    $("#updateER").click(function(){$("#erDiv").html('Exchange rate $<label class="input"><i class="icon-append fa fa-usd"></i><input type="text" name="newER" value=""  id="newER" placeholder="New Exchange Rate" maxlength="8"><b class="tooltip tooltip-bottom-right">Actualizar Tipo de Cambio</b></label>'+
                                                      '<label id="buttonUpER" class="button">update</label></section>');                                     
                                    $("#buttonUpER").click(function() { updateERAX(baseURL,$("#newER").val(),$("#id_in").val(),null,"in" ); });
                                   });
    /*********** Front Page FIN **********/
    
    /*********** LETTER INI **********/
    
    subirImagenClientLogo(baseURL);
    $("#allDef").click(function() { ($(this).attr('checked')=='checked')? $('.defCheck').attr('checked','checked'): $('.defCheck').removeAttr("checked"); });
    $("#addDef").addClass('pointer');
    $("#addDef").click(function() { $("#newDef").fadeIn("slow").html('');
                                    $("#newDef").html('<section class="col col-1"><label class="checkbox"><input type="checkbox"><i></i>New</label></section>'+
                                                      '<section class="col col-3"><label class="input"><i class="icon-append fa fa-gavel"></i><input type="text" name="newTitDef" value=""  id="newTitDef" placeholder="Agregar nueva definicion" maxlength="40"><b class="tooltip tooltip-bottom-right">Nueva definicion</b></label></section>'+
                                                      '<section class="col col-3"><label class="input"><i class="icon-append fa fa-gavel"></i><input type="text" name="newDescDef" value=""  id="newDescDef" placeholder="Agregar descripcion" maxlength="40"><b class="tooltip tooltip-bottom-right">Descripcion de una nueva definicion</b></label>'+
                                                      '<label id="buttonNewDef" class="button">Add</label></section>');
                                    $("#buttonNewDef").click(function() { agregaDefAX(baseURL,$("#newTitDef").val(),$("#newDescDef").val()); });
                                  });
    /*********** LETTER Page FIN **********/
    
    /*********** SCOPE OF WORK INI **********/
    $("#SWdateInspection").datepicker({dateFormat: 'M d yy'});
    /*********** SCOPE OF WORK FIN **********/  
    
    /*********** SUMMARY INI **********/
    $("#SMvc1_3").datepicker({dateFormat: 'M d yy'});
    $("#SMvc2_3").datepicker({dateFormat: 'M d yy'});
    $("#SMvc3_3").datepicker({dateFormat: 'M d yy'});
    $("#SMvc4_3").datepicker({dateFormat: 'M d yy'});
    $("#SMvc5_3").datepicker({dateFormat: 'M d yy'});
    /*********** SUMMARY FIN **********/  
    
     /****************** ANEXOS AA8 INI ******************/
    subirImagenAA8(baseURL);
    /******************* ANEXOS AA8 FIN ******************/ 
});//document ready

/****************************** SELECCION DE COMPARABLES INI VENTANA ************************/
$(function() {  var dialogCLS,dialogCRL,dialogCRS,dialogAdjusCLS,dialogAdjusCRL,dialogAdjusCRS;
                
                function selectComp(tipoComp,comparablesElegidos,comparablesElegidosBorrados,dialog)
                {
                    if (comparablesElegidos.length === 0)
                        { jAlert("Favor de seleccionar, al menos un comparable para incluir en el reporte","HERMES PROJECT VALUATION REPORT"); }
                    else{ selectCompAX(baseURL,comparablesElegidos,comparablesElegidosBorrados,tipoComp,$("#id_in").val());
                          tinymce.init({    selector: '.richTextSmall'+tipoComp+'',
                                            setup: function (editor) { editor.on('change', function () { editor.save(); }); },
                                            height: 150,
                                            toolbar: ' undo redo  | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist |',
                                            content_css: [ '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css', '//www.tinymce.com/css/codepen.min.css' ]
                                        });
                          dialog.dialog( "close" );  }
                }
                

                dialogCRL = $("#dialog-formCRL").dialog({
                  autoOpen: false, height : 700, width : 1350, modal : true,
                  buttons: {"Select" : function() { selectComp("CRL",comparablesElegidos,comparablesElegidosBorrados,dialogCRL); },
                             Cancel  : function() { dialogCRL.dialog( "close" ); }
                           },
                  close  : function() {  dialogCRL.dialog( "close" ); }
                });
                
                dialogCLS = $("#dialog-formCLS").dialog({
                  autoOpen: false, height : 700, width : 1350, modal : true,
                  buttons: {"Select" : function() { selectComp("CLS",comparablesElegidos,comparablesElegidosBorrados,dialogCLS); },
                             Cancel  : function() { dialogCLS.dialog( "close" ); }
                           },
                  close  : function() {  dialogCLS.dialog( "close" ); }
                });
                
                 dialogCRS = $("#dialog-formCRS").dialog({
                  autoOpen: false, height : 700, width : 1350, modal : true,
                  buttons: {"Select" : function() { selectComp("CRS",comparablesElegidos,comparablesElegidosBorrados,dialogCRS); },
                             Cancel  : function() { dialogCRS.dialog( "close" ); }
                           },
                  close  : function() {  dialogCRS.dialog( "close" ); }
                });
               
                $( "#selectCompButtonCLS" ).button().on( "click", function() {
                    comparablesElegidos         = [];
                    comparablesElegidosBorrados = [];
                    $("#f4CLS").datepicker({dateFormat: 'M d yy'});
                    $("#f5CLS").datepicker({dateFormat: 'M d yy'});
                    if($('#annexsAA1 select').length === 0 ) {                            
                            paginarCompAX(1,5,baseURL,'CLS',true,$("#id_in").val(),comparablesElegidos,comparablesElegidosBorrados);
                            dialogCLS.dialog( "open" );   
                        }
                    else{   jConfirm("Está acción puede modificar los comparables seleccionados previamente, ¿Desea continiar?","HERMES PROJECT VALUATION REPORT", function(r) {
                                if(r) { paginarCompAX(1,5,baseURL,'CLS',true,$("#id_in").val(),comparablesElegidos,comparablesElegidosBorrados);
                                        dialogCLS.dialog( "open" );   }
                             });
                        }
                 });
                 
                $( "#selectCompButtonCRL" ).button().on( "click", function() {
                    comparablesElegidos         = [];
                    comparablesElegidosBorrados = [];
                    $("#f4CRL").datepicker({dateFormat: 'M d yy'});
                    $("#f5CRL").datepicker({dateFormat: 'M d yy'});
                    if($('#annexsAA3 select').length === 0 ) {                            
                            paginarCompAX(1,5,baseURL,'CRL',true,$("#id_in").val(),comparablesElegidos,comparablesElegidosBorrados);
                            dialogCRL.dialog( "open" );
                        }
                    else{   jConfirm("Está acción puede modificar los comparables seleccionados previamente, ¿Desea continiar?","HERMES PROJECT VALUATION REPORT", function(r) {
                                if(r) { paginarCompAX(1,5,baseURL,'CRL',true,$("#id_in").val(),comparablesElegidos,comparablesElegidosBorrados);
                                        dialogCRL.dialog( "open" );  }
                             });
                         }     
                 });
                 
                 $( "#selectCompButtonCRS" ).button().on( "click", function() {
                    comparablesElegidos         = [];
                    comparablesElegidosBorrados = [];
                    $("#f4CRS").datepicker({dateFormat: 'M d yy'});
                    $("#f5CRS").datepicker({dateFormat: 'M d yy'});
                    if($('#annexsAA5 select').length === 0 ) {                            
                            paginarCompAX(1,5,baseURL,'CRS',true,$("#id_in").val(),comparablesElegidos,comparablesElegidosBorrados);
                            dialogCRS.dialog( "open" );
                        }
                    else{   jConfirm("Está accion puede modificar los comparables seleccionados previamente, ¿Desea continiar?","HERMES PROJECT VALUATION REPORT", function(r) {
                                if(r) { paginarCompAX(1,5,baseURL,'CRS',true,$("#id_in").val(),comparablesElegidos,comparablesElegidosBorrados);
                                        dialogCRS.dialog( "open" );  }
                             });
                         }     
                 });
                 
                dialogAdjusCLS= $("#dialog-formAdjusAA2").dialog({
                  autoOpen: false, height : 700, width : 1350, modal : true,
                  buttons: {"Save" : function() { submitAnnexA2InAX(baseURL,"formAA2",$("#id_in").val(),"E","AA2",dialogAdjusCLS); },
                             Cancel: function() { dialogAdjusCLS.dialog( "close" ); }
                           },
                  close  : function() {  dialogAdjusCLS.dialog( "close" ); }
                });                
                $( "#selectAdjusButtonAA2" ).button().on( "click", function() { dialogAdjusCLS.dialog( "open" );  });
                 
                dialogAdjusCRL= $("#dialog-formAdjusAA4").dialog({
                  autoOpen: false, height : 700, width : 1350, modal : true,
                  buttons: {"Save" : function() { submitAnnexA2InAX(baseURL,"formAA4",$("#id_in").val(),"E","AA4",dialogAdjusCRL); },
                             Cancel: function() { dialogAdjusCRL.dialog( "close" ); }
                           },
                  close  : function() {  dialogAdjusCRL.dialog( "close" ); }
                });                
                $( "#selectAdjusButtonAA4" ).button().on( "click", function() { dialogAdjusCRL.dialog( "open" );  });
                
                dialogAdjusCRS= $("#dialog-formAdjusAA6").dialog({
                  autoOpen: false, height : 700, width : 1350, modal : true,
                  buttons: {"Save" : function() { submitAnnexA2InAX(baseURL,"formAA6",$("#id_in").val(),"E","AA6",dialogAdjusCRS); },
                             Cancel: function() { dialogAdjusCRS.dialog( "close" ); }
                           },
                  close  : function() {  dialogAdjusCRS.dialog( "close" ); }
                });                
                $( "#selectAdjusButtonAA6" ).button().on( "click", function() { dialogAdjusCRS.dialog( "open" );  });
                
            });
/****************************** SELECCION DE COMPARABLES fIN VENTANA ************************/
</script>

<?php
    echo '<div class="body-hermesForm">
           <div class="hermesForm">';   
    echo   form_input(array('name'=>'id_in', 'type'=>'hidden','id'=>'id_in', 'value'=>$id_in));
    echo   form_input(array('name'=>'accion','type'=>'hidden','id'=>'accion','value'=>$accion));    
    echo '  <header> 
             <section class="col col-11">                     
                     <div class="label inline-group"><label class="radio"><b>Status:</b></label> '.$status.'</div>
             </section>             
           </header>';
    echo '<div class="row">            
                    <section id="erDiv" class="col col-7 erStyle">Exchange rate $'.$er.nbs(3).$updateER.$d_er.'</section>                    
                        <section class="col col-4 erStyle">
                        <div class="label inline-group"><label class="radio"></label> '.$idiom.'</div>
                        </section>
            </div>';
    echo '  <!-- tabs -->
              <div class="sky-tabs sky-tabs-amount-7 sky-tabs-pos-top-justify sky-tabs-response-to-icons">
                    <input type="radio" name="sky-tabs" value="FP" checked id="sky-tab1" class="sky-tab-content-1">
                    <label for="sky-tab1"><span><span><i class="fa fa-building-o"></i>Front Page</span></span></label>

                    <input type="radio" name="sky-tabs" value="LT" id="sky-tab2" class="sky-tab-content-2">
                    <label for="sky-tab2"><span><span><i class="fa fa-file-text-o"></i>Letter</span></span></label>

                    <input type="radio" name="sky-tabs" value="SW" id="sky-tab3" class="sky-tab-content-3">
                    <label for="sky-tab3"><span><span><i class="fa fa-th-large"></i>S W </span></span></label>

                    <input type="radio" name="sky-tabs" value="SM" id="sky-tab4" class="sky-tab-content-4">
                    <label for="sky-tab4"><span><span><i class="fa fa-tasks"></i>Summary</span></span></label>

                    <input type="radio" name="sky-tabs" value="AP" id="sky-tab5" class="sky-tab-content-5">
                    <label for="sky-tab5"><span><span><i class="fa fa-university"></i>Appraisal</span></span></label>

                    <input type="radio" name="sky-tabs" value="AN" id="sky-tab6" class="sky-tab-content-6">
                    <label for="sky-tab6"><span><span><i class="fa fa-cogs"></i>Annexes</span></span></label>
                    
                    <input type="radio" name="sky-tabs" value="R" id="sky-tab7" class="sky-tab-content-7">
                    <label for="sky-tab7"><span><span><i class="fa fa-file-word-o"></i>Report</span></span></label>

                    <ul>
                        <li class="sky-tab-content-1">';
                            include("aux_frontpage.php");     
    echo '               </li>

                        <li class="sky-tab-content-2">';
                                include("aux_letter.php");                                
    echo '              </li>

                        <li class="sky-tab-content-3">';
                            include("aux_scopeofwork.php");
    echo '              </li>

                        <li class="sky-tab-content-4">';                              
                            include("aux_summary.php");     
    echo '               </li>
        
                         <li class="sky-tab-content-5">';
                            include("aux_appraisal.php");    
    echo '               </li>
        
                         <li class="sky-tab-content-6">';
                            include("aux_annexs.php");    
    echo '               </li>
                        <li class="sky-tab-content-7">';
    echo form_open(base_url().'inmueble/guardar/', array('id' => 'formEXP','name' => 'formEXP'));
    echo '               <fieldset>
                            <div class="row">
                                <label class="col col-10 subtitulo">Export to Final Report</label>'.br(2).'                   
                            </div>
                        </fieldset>
                        <fieldset>
                            <div class="row">
                                <section class="col col-3">
                                        <label class="label">Fecha del Trabajo</label>
                                        <label class="input">
                                           <i class="icon-append fa fa-calendar"></i>'.
                                           form_input(array('class'       => 'validate[required,custom[dateED]] text-input datepicker', 
                                                            'name'        => 'fecha_name', 
                                                            'id'          => 'fecha_name',
                                                            'placeholder' => "Fecha del Trabajo",
                                                            'value'       => $inmueble['frontPage'][0]['fecha_name'],
                                                            'onChange'    => "nomenclaturaRepNum(baseURL,'.$id_in.',$(this).val())",
                                                            'maxlength'   => '20')).'
                                            <b class="tooltip tooltip-bottom-right">Fecha del Reporte</b>
                                        </label>
                                </section>
                                <section  class="col col-2 "> </section>
                                <section  class="col col-5 ">
                                     <CENTER><span id="wordLink" class="pointer">'.img( array('src' =>  base_url()."images/filedownload.png",'boder'=>0,'title' => 'Generate Report')).nbs(2).'<i class="fa fa-file-word-o"></i>'.nbs(2).'Generate Report</span></CENTER>
                                </section>
                            </div>
                            <div class="row">
                                        <section  class="col col-10">
                                        <CENTER><span id="generaDOC"></span></CENTER>
                                        </section>
                                </div>
                        </fieldset>
                        ';
    echo form_close();                             
    echo '          </li>
                  </ul>
            </div>
            <!--/ tabs -->';
                             
    
    echo "<footer>\n            
           <a class='button button-secondary' href='".base_url()."inmueble/'>Exit</a>
         </footer>\n";
    echo '</div>
         </div>';         
    echo br(2);	
    
    include("footer.php"); 
    
?>  