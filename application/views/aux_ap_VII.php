

<?php 
    
echo form_open(base_url().'inmueble/guardar/'.$id_in.'/'.$accion.'/APVII/'.$inmueble['frontPage'][0]['id_status'], array('id' => 'formAPVII','name' => 'formAPVII'));    

    echo '<fieldset></fieldset>
          <fieldset>
            <div class="row">
                    <label class="col col-5 subLeft">VII. Sales Comparison Approach</label>
                    <section class="col col-6"><label class="checkbox" style="text-align: justify; text-justify: inter-word"><input type="checkbox" class="includedRep" name="ap_in" id="apVIICheckBox" value="APVII" checked><i></i>Section Included In The Valuation Report</label></section>
            </div>
          </fieldset>';

    echo '<div id="divAPVII"> </div>';
    
    echo '<fieldset></fieldset>
            <fieldset>
                <div class="row">
                        <label class="col col-5 subLeft">VIII. Value in Use</label>
                        <section class="col col-6"><label class="checkbox" style="text-align: justify; text-justify: inter-word"><input type="checkbox" class="includedRep" name="ap_in" id="apVIIICheckBox" value="APVIII" checked><i></i>Section Included In The Valuation Report</label></section>
                </div>
              </fieldset>';
    
    echo '<div id="divAPVIII"> </div>';
    
    echo '<fieldset></fieldset>
           <fieldset>
                <div class="row">
                        <label class="col col-5 subLeft">IX. Summary of Values</label>
                        <section class="col col-6"><label class="checkbox" style="text-align: justify; text-justify: inter-word"><input type="checkbox" class="includedRep" name="ap_in" id="apIXCheckBox" value="APIX" checked><i></i>Section Included In The Valuation Report</label></section>
                </div>
           </fieldset>';
    echo '<div id="divAPIX"> </div>';
    
    echo '<fieldset></fieldset><fieldset>
                        <div class="row">
                                <label class="col col-5 subLeft">X. Reconciliation of Values (Market Value)</label>
                                <section class="col col-6"><label class="checkbox" style="text-align: justify; text-justify: inter-word"><input type="checkbox" class="includedRep" name="ap_in" id="apXCheckBox" value="APX" checked><i></i>Section Included In The Valuation Report</label></section>
                        </div>
                      </fieldset>';
    echo '<div id="divAPX"> </div>';

    echo "<div class='row'><a class='button' href='javascript:submitInAX(baseURL,\"formAPVII\",\"$id_in\",tipoAccion,\"APVII\");'>Save Section</a>
          <span id='confirmAPVII' class='msjconfirm'></span></div>";
echo form_close();

?>
