<?php 

echo form_open(base_url().'inmueble/guardar/'.$id_in.'/'.$accion.'/SW/'.$accion.'/'.$inmueble['frontPage'][0]['id_status'], array('id' => 'formSW','name' => 'formSW'));
echo ' <fieldset>
        <div class="row">
                <label class="col col-7 subtitulo">'.nbs(1).'</label>
                <section class="col col-4"><label class="checkbox" style="text-align: justify; text-justify: inter-word"><input type="checkbox" class="includedRep" name="report" id="scopeWorkCheckBox" value="SW" checked><i></i>Section Included In The Valuation Report</label></section>
        </div>
        </fieldset>
        <div class="row">
            <section class="col col-10">'.
                form_textarea(array('class' 	=> '  text-input richTextSmallSW',
                                    'name' 	=> 'SWParrafo1',
                                    'id' 	=> 'SWParrafo1',
                                    'value'     => '')).'
             </section>             
         </div>         
         
         <div class="row">  
            <section class="col col-8">
                    <label class="input">
                       <i class="icon-append fa fa-bookmark"></i>'.
                       form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input', 
                                        'name'        => 'SWTit1', 
                                        'id'          => 'SWTit1',
                                        'placeholder' => "",
                                        'value'       => '',
                                        'maxlength'   => '60')).'
                        <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                    </label>
            </section>            
            <section class="col col-10">
                    <label class="textarea">'.
                            form_textarea(array('class' => '  text-input',
                                                'rows'  => "3",
                                                'name'  => 'SWText1',
                                                'id'    => 'SWText1',
                                                'value' => '' )).'
                    </label>
                    <center> <div class="col col-10 dirClientSW"></div> </center>                                
            </section>
         </div>
         <div class="row">  
            <section class="col col-8">
                    <label class="input">
                       <i class="icon-append fa fa-bookmark"></i>'.
                       form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input', 
                                        'name'        => 'SWTit2', 
                                        'id'          => 'SWTit2',
                                        'placeholder' => "",
                                        'value'       => '',
                                        'maxlength'   => '60')).'
                        <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                    </label>
            </section>        
            <section class="col col-10">
                    <label class="input">'.
                       form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input', 
                                        'name'        => 'SWText2_1', 
                                        'id'          => 'SWText2_1',
                                        'placeholder' => "",
                                        'value'       => '',
                                        'maxlength'   => '20')).'
                        <b class="tooltip tooltip-bottom-right">Leyenda</b>
                    </label>                    
                    <center> <div class="col col-10 dirComClientSW"></div> </center>
            </section>            
            <section class="col col-8">
                    <label class="input">'.
                       form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input', 
                                        'name'        => 'SWText2_2', 
                                        'id'          => 'SWText2_2',
                                        'placeholder' => "",
                                        'value'       => '',
                                        'maxlength'   => '100')).'
                        <b class="tooltip tooltip-bottom-right">Leyenda</b>
                    </label>                    
            </section>
            <section class="col col-8">
                    <label class="input">'.
                       form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input', 
                                        'name'        => 'SWprovidedBy', 
                                        'id'          => 'SWprovidedBy',
                                        'placeholder' => "",
                                        'value'       => '',
                                        'maxlength'   => '50')).'
                        <b class="tooltip tooltip-bottom-right">Provided By</b>
                    </label>
            </section>
            <section class="col col-10">
                    <label class="textarea">'.
                            form_textarea(array('class' => 'validate[custom[onlyLetterNumber]] text-input',
                                                'rows'  => "3",
                                                'name'  => 'SWText2_3',
                                                'id'    => 'SWText2_3',
                                                'value' => '' )).'
                    </label>                    
            </section>
         </div>
         <div class="row">
         <section class="col col-8">
                    <label class="input">
                       <i class="icon-append fa fa-bookmark"></i>'.
                       form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input', 
                                        'name'        => 'SWTit3', 
                                        'id'          => 'SWTit3',
                                        'placeholder' => "",
                                        'value'       => '',
                                        'maxlength'   => '60')).'
                        <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                    </label>
          </section>        
          </div>
         <div class="row">
          <section class="col col-7">
                    <label class="input">'.
                       form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input', 
                                        'name'        => 'SWText3_1', 
                                        'id'          => 'SWText3_1',
                                        'placeholder' => "",
                                        'value'       => '',
                                        'maxlength'   => '40')).'
                        <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                    </label>
            </section>
             <section class="col col-3">
                    <label class="input">
                       <i class="icon-append fa fa-calendar"></i>'.
                       form_input(array('class'       => 'validate[required,custom[dateED]] text-input datepicker', 
                                        'name'        => 'SWdateInspection', 
                                        'id'          => 'SWdateInspection',
                                        'placeholder' => "",
                                        'value'       => '',
                                        'maxlength'   => '40')).'
                        <b class="tooltip tooltip-bottom-right">Fecha de inspeccion</b>
                    </label>
            </section>
            <section class="col col-3">
                <label class="label" style="padding-top: 12px;">, by</label>
            </section>
             <section class="col col-5">
                    <label class="input">
                       <i class="icon-append fa fa-male"></i>'.
                       form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input', 
                                        'name'        => 'SWinspector', 
                                        'id'          => 'SWinspector',
                                        'placeholder' => "",
                                        'value'       => '',
                                        'maxlength'   => '40')).'
                        <b class="tooltip tooltip-bottom-right">Inspector</b>
                    </label>
            </section>
            <section class="col col-4">
                <label class="label" style="padding-top: 12px;"> </label>
            </section>
          </div>
          <div class="row">
            <section class="col col-10">
                    <label class="input">'.
                       form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input', 
                                        'name'        => 'SWText3_2', 
                                        'id'          => 'SWText3_2',
                                        'placeholder' => "",
                                        'value'       => '',
                                        'maxlength'   => '80')).'
                        <b class="tooltip tooltip-bottom-right">Scope Of Work</b>
                    </label>                   
             </section>
         </div>  
         <div class="row">  
            <section class="col col-8">
                    <label class="input">
                       <i class="icon-append fa fa-bookmark"></i>'.
                       form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input', 
                                        'name'        => 'SWTit4', 
                                        'id'          => 'SWTit4',
                                        'placeholder' => "",
                                        'value'       => '',
                                        'maxlength'   => '60')).'
                        <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                    </label>
            </section>        
            <section class="col col-10">
                    <label class="input">'.
                       form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input', 
                                        'name'        => 'SWText4_1', 
                                        'id'          => 'SWText4_1',
                                        'placeholder' => "",
                                        'value'       => '',
                                        'maxlength'   => '20')).'
                        <b class="tooltip tooltip-bottom-right">Leyenda</b>
                    </label>                    
            </section>
            <section class="col col-10">
                    <label class="input">'.
                       form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input', 
                                        'name'        => 'SWText4_2', 
                                        'id'          => 'SWText4_2',
                                        'placeholder' => "",
                                        'value'       => '',
                                        'maxlength'   => '20')).'
                        <b class="tooltip tooltip-bottom-right">Leyenda</b>
                    </label>                    
            </section>
            <section class="col col-10">
                    <label class="input">'.
                       form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input', 
                                        'name'        => 'SWText4_3', 
                                        'id'          => 'SWText4_3',
                                        'placeholder' => "",
                                        'value'       => '',
                                        'maxlength'   => '20')).'
                        <b class="tooltip tooltip-bottom-right">Leyenda</b>
                    </label>                    
            </section>
        </div>
        <div class="row">
         <section class="col col-8">
                    <label class="input">
                       <i class="icon-append fa fa-bookmark"></i>'.
                       form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input', 
                                        'name'        => 'SWTit5', 
                                        'id'          => 'SWTit5',
                                        'placeholder' => "",
                                        'value'       => '',
                                        'maxlength'   => '60')).'
                        <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                    </label>
          </section>        
          <section class="col col-10">'.
                form_textarea(array('class' 	=> ' text-input richTextMediumSW',
                                    'name' 	=> 'SWText5',
                                    'id' 	=> 'SWText5',
                                    'value'     => '')).'
             </section>                         
         </div>
         <div class="row">  
            <section class="col col-8">
                    <label class="input">
                       <i class="icon-append fa fa-bookmark"></i>'.
                       form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input', 
                                        'name'        => 'SWTit6', 
                                        'id'          => 'SWTit6',
                                        'placeholder' => "",
                                        'value'       => '',
                                        'maxlength'   => '60')).'
                        <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                    </label>
            </section>                    
            <section class="col col-10">
                    <label class="textarea">'.
                            form_textarea(array('class' => 'validate[custom[onlyLetterNumber]] text-input',
                                                'rows'  => "3",
                                                'name'  => 'SWText6',
                                                'id'    => 'SWText6',
                                                'value' => '' )).'
                    </label>                    
            </section>
         </div>
         <div class="row">  
            <section class="col col-8">
                    <label class="input">
                       <i class="icon-append fa fa-bookmark"></i>'.
                       form_input(array('class'       => 'validate[required,custom[onlyLetterNumber]] text-input', 
                                        'name'        => 'SWTit7', 
                                        'id'          => 'SWTit7',
                                        'placeholder' => "",
                                        'value'       => '',
                                        'maxlength'   => '60')).'
                        <b class="tooltip tooltip-bottom-right">Subtitutlo</b>
                    </label>
            </section>                    
            <section class="col col-10">
                    <label class="textarea">'.
                            form_textarea(array('class' => 'validate[custom[onlyLetterNumber]] text-input',
                                                'rows'  => "3",
                                                'name'  => 'SWText7',
                                                'id'    => 'SWText7',
                                                'value' => '' )).'
                    </label>                    
            </section>
         </div>
       
     ';
echo "<a class='button' href='javascript:submitInAX(baseURL,\"formSW\",\"$id_in\",tipoAccion,\"SW\");'>Save Section</a>
      <span id='confirmSW' class='msjconfirm'></span>";
echo form_close();


?>
