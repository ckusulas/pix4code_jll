<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/**
 * Modelo para interactuar en la BD
 */
 
class Md_gallery extends CI_Model {
		
	
	public function insert_gallery($id_in,$idImagen,$nombre,$tipoGaleria,$titulo,$extraAA)
        {
            $data = array(
                'id_in'        =>  $id_in,
                'id_gallery'   =>  $idImagen,
                'nombre'       =>  $nombre,
                'tipo_gallery' =>  $tipoGaleria,
                'titulo'       =>  $titulo,
                'extraAA'      =>  $extraAA
                );
            
            $this->db->insert('gallery',$data);
            
            $this->db->select('id_gallery');
            $this->db->from('gallery');		
            $this->db->where('id_in'       ,$id_in);
            $this->db->where('nombre'      ,$nombre);
            $this->db->where('tipo_gallery',$tipoGaleria);
            $this->db->where('titulo'      ,$titulo);
            $query = $this->db->get();
            if($query->num_rows() > 0 )      
            { $r=$query->result_array();
              return $r[0]['id_gallery'];              
            }
        }
	
	
   public function traeFotosGallery($id_inmueble, $tipoGaleria)
    {		                     
        if ($tipoGaleria==="Photos")
            { 
                $this -> db -> select('g.id_gallery,g.nombre,g.titulo');
                $this -> db -> order_by("g.id_gallery","asc");                
            }
        else
            {
                $this -> db -> select('g.titulo,aai.opcion_catalogo as anexo, g.extraAA');
                $this -> db -> join('catalogo aai', 'aai.id_opcion = g.titulo','inner outer');
                $this -> db -> group_by('g.titulo,aai.opcion_catalogo'); 
                $this -> db -> order_by("g.titulo, g.id_gallery","asc");             
            }
        $this->db->from ('gallery g');
	$this->db->where('g.id_in',$id_inmueble);
        $this->db->where('g.tipo_gallery',$tipoGaleria);     
	$query = $this->db->get();
						
        if($query->num_rows() > 0 )
            { return $query->result_array(); }
        else 
            { return array(); }
    }
    
     public function traeFotosAAIMG($id_in,$tipoGaleria,$titulo)
     {
         $this->db->select('g.id_gallery,g.nombre,g.titulo,g.extraAA');
        $this->db->from('gallery g');		
	$this->db->where('g.id_in'       ,$id_in);
        $this->db->where('g.tipo_gallery',$tipoGaleria); 
        $this->db->where('g.titulo'      ,$titulo); 
        $this -> db -> order_by("g.id_gallery","asc"); 
	$query = $this->db->get();		       					        
						
        if($query->num_rows() > 0 )      
            { return $query->result_array(); }
        else 
            { return array(); }
     }
     public function traeFotoGalleryId($id_photo)
    {		      
        $this->db->select('titulo,nombre');
        $this->db->from('gallery');		
	$this->db->where('id_gallery',$id_photo);
	$query = $this->db->get();		       					        
						
        if($query->num_rows() > 0 )
            { $foto=$query->result_array(); 
              return $foto[0];
            }
        else 
            { return array(); }
    }
    
    public function traeNombreAnexo($tipoGaleria,$phototitulo) {
        if($tipoGaleria==="AAIMG")
        {
            $this->db->select('opcion_catalogo');
            $this->db->from('catalogo');		
            $this->db->where('id_opcion',$phototitulo);
            $query = $this->db->get();		       					        

            if($query->num_rows() > 0 )
                { $t=$query->result_array(); 
                  return $t[0]['opcion_catalogo'];
                }
            else 
                { return NULL; }
        }
        else
        { return NULL; }
    }
    
    public function borraGallery($id_inmueble,$tipoGaleria)
    {
       $this->db->delete('gallery', array('id_in' => $id_inmueble,'tipo_gallery'=>$tipoGaleria));            
    }
    
    public function borraFoto($nombre)
    {
       $this->db->delete('gallery', array('nombre' => $nombre));            
    }
    
    public function updateImagenComp($nombreFoto,$id_comp)
    {
      $this->db->where('id_comp',  $id_comp);
      $up = $this->db->update('comparables', array("foto"=>$nombreFoto));  
      return $up;                    
    }
    
    
    public function updateTitulo($id,$titulo)
    {
      $this->db->where('id_gallery',  $id);
      $up = $this->db->update('gallery', array("titulo"=>$titulo));  
      return $up;                    
    }
    
    public function updateTituloExtraAA($id_in,$titulo,$extraAA)
    {
      $this->db->where('id_in',  $id_in);
      $this->db->where('titulo',  $titulo);
      $up = $this->db->update('gallery', array("extraAA"=>$extraAA));  
      return $up;                    
    }
   
}
