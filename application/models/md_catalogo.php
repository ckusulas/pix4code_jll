<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/**
 * Modelo para interactuar en la BD
 */
 
class Md_catalogo extends CI_Model {
					
  		
    public function insert_catalogo($campo,$opcion_catalogo)
    {		
		
        $data = array(
            'campo'          =>  $campo,
            'opcion_catalogo' =>  $opcion_catalogo,
            );
            $this->db->insert('catalogo',$data);
    }
       
   
    public function traeConceptos()
    {					
        $this->db->select("valor_catalogo as value, opcion_catalogo as text");
        $this->db->where("campo","concepto");         
        $query = $this->db->get("catalogo");
						
       $options = array();		
        if($query -> num_rows() > 0 )
        {
        $options[0] = '::Choose::';
        foreach ($query->result() as $row)							 
            { $options[$row->value] = $row->text; }
        }
        return $options;        
    }
    
   public function poblarSelect($campo,$valorProfile)
    {    						
        $this -> db -> select('c.id_opcion,c.opcion_catalogo');
	$this -> db -> where('c.campo',$campo);
        $this -> db -> order_by("c.id_opcion","asc");		
        $query = $this -> db -> get('catalogo c');
		
	$options = array();		
        if($query -> num_rows() > 0 )
		{
                    if($valorProfile == true)
                        {$options[0] = 'Profile';}
                    else
                        {$options[0] = '::Choose::';}
		foreach ($query->result() as $row)							 
				$options[$row->id_opcion] = $row->opcion_catalogo;				          	   
		}
		return $options;        
    }	
    
    
    public function poblarRadioButtonStatus($tipouSR,$ignore,$valorSel)
    {    						
        $this -> db -> select('s.id_status,s.descripcion');
        
        if (empty($tipouSR))
            { $options[] =  array('value'=>'0','checked'=>'checked','label'=>'All'); }
        else
            { $this -> db -> where_not_in('s.id_status', $ignore); }        
                
        $this -> db -> order_by("s.id_status","asc");
        $query = $this -> db -> get('status_in s');
		                        
        if($query -> num_rows() > 0 )
		{ foreach ($query->result() as $row)							 				
                            {  $options[] =  array('value'=> $row->id_status,'checked'=>($row->id_status == $valorSel ? 'checked' : ''),'label'=>$row->descripcion);}}
	return $options;        
    }
    
     public function poblarRadioButtonIdiom($valorSel)
    {    						
        
       $options[] =  array('value'=>'ENG','checked'=>($valorSel == 'ENG' ? 'checked' : ''),'label'=>'English');
       $options[] =  array('value'=>'ESP','checked'=>($valorSel == 'ESP' ? 'checked' : ''),'label'=>'Spanish');
       
       return $options;        
    }
	

    public function poblarRadioButtonComp($valorSel)
    {        
        $options[] =  array('value'=> "CLS",'checked'=>("CLS" == $valorSel ? 'checked' : ''),'label'=>"Land Sale");
        $options[] =  array('value'=> "CRL",'checked'=>("CRL" == $valorSel ? 'checked' : ''),'label'=>"Ref Lease");
        $options[] =  array('value'=> "CRS",'checked'=>("CRS" == $valorSel ? 'checked' : ''),'label'=>"Ref Sale");
        
	return $options;        
    }
    
    public function traeDescOpcion($id_opcion)
    {    						
        $this -> db -> select('c.opcion_catalogo');
		$this -> db -> where('c.id_opcion',$id_opcion);
        $query = $this -> db -> get('catalogo c');		

		$registro = $query->row();
		
		return $registro->opcion_catalogo;        
    }		
   
    //ejemplo de eliminar al usuario con id = 1
    public function delete_catalogo()
    {
        $this->db->delete('users', array('id' => 1));
    }
 
    //ejemplo actualizar los datos del usuario con id = 3
    public function update_catalogo()
    {
        $data = array(
            'username' => 'silvia',
            'fname' => 'madrejo',
            'lname' => 'sánchez'
        );
        $this->db->where('id', 3);
        $this->db->update('users', $data);
    }
}
