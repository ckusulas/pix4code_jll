<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/**
 * Modelo para interactuar en la BD
 */
 
class Md_usuario extends CI_Model {
	
	
	public function validaUsuario($usuario,$pwd)
    {
        
        $this->db->select('nombre,apellidos,tipo,correo,foto');
        $this->db->from('usuarios ');        
        $this->db->where('correo',$usuario);
	$this->db->where('pwd',$pwd);
       
        $query = $this->db->get();
		
        if($query->num_rows() > 0 )      
            return $query->result_array();
	else 
            return false;
        
    }
    
    
		
  public function traeUsers($registros_x_pagina,$offset,$dirFoto)
    {                   
        $off  = (($offset-1) * $registros_x_pagina);        
        
    	$this -> db -> select('count(u.correo) as conteo');
        $this -> db -> from('usuarios u');
        $this -> db -> join('catalogo c', 'c.id_opcion = u.tipo','inner outer');
        $query = $this->db->get()->result();
	$conteo = $query[0]->conteo;				

        $this -> db -> select('u.correo, u.nombre, u.apellidos, c.opcion_catalogo as tipo, u.puesto, u.telefono, u.titulo, u.foto');
        $this -> db -> join('catalogo c', 'c.id_opcion = u.tipo','inner outer');
        $this -> db -> order_by("u.nombre","asc");		
        $query = $this -> db -> get('usuarios u',$registros_x_pagina,$off);			

        if($query -> num_rows() > 0 )      
           { return array("conteo"=>$conteo, "registros"=>$query->result_array(), "offset"=>$off, "dirFoto"=>$dirFoto); }
	else 
            { return false;  }
    }	
    
  //ejemplo nuevo usuario en la tabla users
    public function insert_usuario_cliente($correo,$rs)
    {
        $this->load->helper('date');
        $hoy = standard_date('DATE_W3C', time());
        $pwd = now().'p';
		
        $data = array(
            'correo'     => $correo,
            'nombre'     => $rs,
            'fecha_alta' => $hoy,
            'tipo'  	 => 'C',
            'pwd'  		 => $pwd
            );
        $this->db->insert('usuarios',$data);
    }
  
	
	//ejemplo nuevo usuario en la tabla users
    public function insertUsr($data)
    {        
            $this->db->insert('usuarios',$data);
    }
   
    //ejemplo de eliminar al usuario con id = 1
    public function delete_user()
    {
        $this->db->delete('usuarios', array('id' => 1));
    }
 
    //ejemplo actualizar los datos del usuario con id = 3
    public function updateUsr($data,$id)
    {        
        $this->db->where('correo', $id);
        $this->db->update('usuarios', $data);
    }
	
    public function verificaCuenta($correo)
    {        
        $this->db->select('nombre, apellidos, pwd, correo');
        $this->db->from  ('usuarios ');		
	$this->db->where ('correo',$correo);		
       
        $query = $this->db->get();
		
        if($query->num_rows() > 0 )      
            return $query->result_array();
		else 
			return FALSE;     
    }
	
	public function traeAdmin()
	{
		$options = array();
		
		$this->db->select('`correo` as id,  CONCAT(`titulo`,\' \',`nombre`,\' \',`apellidos`) as ec', FALSE);
		$this->db->where("(TIPO = 'A' OR TIPO = 'V')");
        $query = $this->db->get('usuarios');
						
       if($query -> num_rows() > 0 )
		{		  
		    $options[0] = '::Elegir::';
			foreach ($query->result() as $row)							 
				$options[$row->id] = $row->ec;				          	   
		}
		return $options;       
    }	
    
    public function poblaSelect()
    {
	$options = array();
		
	$this->db->select('`correo` as id,  CONCAT(`nombre`,\' \',`apellidos`) as ec', FALSE);		
        $query = $this->db->get('usuarios');
	$options[0] = 'Valuador';					
       if($query -> num_rows() > 0 )
        {		  		    
            foreach ($query->result() as $row)							 
                    $options[$row->id] = $row->ec;				          	   
        }
        
        return $options;       
    }	
	
    public function traeDetalleUsr($correo)
    {
        $this->db->select('*');
        $this->db->where('correo',$correo);
        $query = $this->db->get('usuarios');

        if($query->num_rows() > 0 )      
            { return $query->result_array(); }
        else 
            { return false; }
    }
    
    public function validaDuplicidad($correo)
    {   
        
        $this -> db -> select('`correo`, CONCAT(`nombre`," ",`apellidos`) as nombre ', FALSE);        
        $this -> db -> where('correo',$correo);
        $query = $this->db->get('usuarios');
		
        if($query->num_rows() > 0 )      
            return $query->result_array();
	else 
            return false;     
    }
    
}
