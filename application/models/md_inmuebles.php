<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/**
 * Modelo para interactuar en la BD
 */
 
class Md_inmuebles extends CI_Model {
        
    
    public function insert_inmuebles($data)
    {       
        $this->db->insert('inmuebles',$data);
    }
    
    public function insertaSeccionInmueble($data,$tabla)
    {       
        return $this->db->insert($tabla,$data);
    }
    
    public function updateSeccionInmueble( $id_in, $data,$tabla)
    {           
    $this->db->where('id_in',  $id_in);
        $this->db->update($tabla, $data);           
    }   
     
    public function updateAnnInmueble( $id_in,$id_comp, $data,$tabla)
    {           
    $this->db->where('id_in',   $id_in);
        $this->db->where('id_comp', $id_comp);
        $this->db->update($tabla, $data);           
    }
    
    public function borraSeccionInmueble($id_in,$tabla)
    {
       return $this->db->delete($tabla, array('id_in' => $id_in));     
    }
    
    public function borraDeterioro($id_in,$tipo,$tabla)
    {
       $this->db->delete($tabla, array('id_in' => $id_in,"tipo"=>$tipo));     
    }
    
    
    public function borraAnexoInmueble($id_in,$tipoComp,$tabla)
    {
       $this->db->delete($tabla, array('id_in' => $id_in,"tipo_comp"=>$tipoComp));     
    }
    
    public function update_inmuebles( $id_in, $data)
    {           
    $this->db->where('id_in',  $id_in);
        $this->db->update('inmuebles', $data);           
    }   
    
    public function update_in($tabla, $id_in, $data)
    {           
    $this->db->where('id_in',  $id_in);
        $this->db->update($tabla, $data);           
    }   
    
   
   public function validaDuplicidad($rfc,$correo,$nombre)
    {        
        $this->db->select('rfc,nombre,correo,calle,numero,colonia,delegacion,cp,estado,pais');
        $this->db->from('inmuebles ');
        if($rfc == NULL)
        { $this->db->where('correo',$correo); }
        else
                    if($nombre == NULL)
                        { $this->db->where('rfc',$rfc); }
                    else
                        { $this->db->where('nombre',$nombre); }
       
        $query = $this->db->get();
        
        if($query->num_rows() > 0 )      
            { return $query->result_array(); }
        else 
            { return false; }
    }
    
    public function poblarSelect($tipoServicio)
    {                       
        $this -> db -> select('pr.id_prove,pr.nombre');
        $this -> db -> order_by("pr.nombre","asc");
        
        if($tipoServicio != "0")
                    { $this->db->where('tipo_servicio',$tipoServicio); }
            
        $query = $this -> db -> get('inmuebles pr');
        
    $options = array();
        if($query -> num_rows() > 0 )
            {         
                $options[0] = '';
                    foreach ($query->result() as $row)                           
                        { $options[$row->id_prove] = $row->nombre; }
            }

        return $options;  
    }
    
    public function poblarSelectAX($tipoServicio)
    {                       
        $this -> db -> select('pr.id_prove,pr.nombre');
        $this -> db -> order_by("pr.nombre","asc");
        
        if($tipoServicio != "0")
            $this->db->where('tipo_servicio',$tipoServicio);
            
        $query = $this -> db -> get('inmuebles pr');
        
        $options = array();
        $options[] = array("id_prove" => 0,"nombre"  =>'');
        
        if($query -> num_rows() > 0 )          
            foreach ($query->result() as $row)                           
                $options[] = array("id_prove" => $row->id_prove, "nombre" => $row->nombre);
        
        
    return $options;  
    }   
   
   
    
    
    public function traeInmueblesFiltros($registros_x_pagina,$offset,$f1,$f2,$f3,$f4,$f5,$status,$dirFoto)
    {                   
        $off  = (($offset-1) * $registros_x_pagina);
        
        $this -> db -> select('count(i.id_in) as conteo');
        $this -> db -> from('inmuebles i');
        $this -> db -> join('status_in st', 'st.id_status = i.id_status','inner outer');
        $this -> db -> join('usuarios u', 'u.correo = i.correo','inner outer');
        if (!empty($f1))
            { $this -> db -> or_like(array('i.preparedFor' => $f1)); }
        if (!empty($f2))
            { $this -> db -> or_like(array('i.propertyOf' => $f2)); }
        if (!empty($f3))
            { $this -> db -> or_like(array('i.calle' => $f3,'i.num' => $f3,'i.col' => $f3,'i.mun' => $f3,'i.edo' => $f3,'i.cp' => $f3)); }
        if (!empty($f4))
            { $this -> db -> or_like(array('i.repNum' => $f4)); }
        if (!empty($f5))
            { $this -> db -> or_like(array('i.effectiveDate' => $f5)); }   
        if ($status != '0')
            { $this -> db -> or_like(array('i.id_status' => $status)); }
        $query = $this->db->get()->result();
    $conteo = $query[0]->conteo;                

        $this -> db -> select('st.descripcion as status,i.id_in, i.preparedFor, i.propertyOf, i.fotoPortada, i.repNum, i.calle, i.num, i.col, i.mun, i.edo, i.cp');
        $this -> db -> select("DATE_FORMAT(`i`.`effectiveDate`,'%b %D %Y') as effectiveDate, CONCAT_WS(' ',`u`.`nombre`,`u`.`apellidos`) as creadoPor", FALSE);
        $this -> db -> join('status_in st', 'st.id_status = i.id_status','inner outer');
        $this -> db -> join('usuarios u', 'u.correo = i.correo','inner outer');
                if (!empty($f1))
            { $this -> db -> or_like(array('i.preparedFor' => $f1)); }
        if (!empty($f2))
            { $this -> db -> or_like(array('i.propertyOf' => $f2)); }
        if (!empty($f3))
            { $this -> db -> or_like(array('i.calle' => $f3,'i.num' => $f3,'i.col' => $f3,'i.mun' => $f3,'i.edo' => $f3,'i.cp' => $f3)); }
        if (!empty($f4))
            { $this -> db -> or_like(array('i.repNum' => $f4)); }
        if (!empty($f5))
            { $this -> db -> or_like(array('i.effectiveDate' => $f5)); }    
        if (!empty($status))
            { $this -> db -> or_like(array('i.id_status' => $status)); }
        $this -> db -> order_by("i.fecha_alta","asc");      
        $query = $this -> db -> get('inmuebles i',$registros_x_pagina,$off);            

        if($query -> num_rows() > 0 )      
           { return array("conteo"=>$conteo, "registros"=>$query->result_array(), "offset"=>$off, "dirFoto"=>$dirFoto); }
    else 
            { return false;  }
    }   

    public function traeDetalleInmueble($id_in,$selectC,$typeeffectiveDate)
    {                           
        $this->db->select($selectC);
        $this->db->select("DATE_FORMAT(`i`.`fecha_alta`,'%b %$typeeffectiveDate %Y') as fecha_alta", FALSE);
        $this->db->select("DATE_FORMAT(`i`.`effectiveDate`,'%b %$typeeffectiveDate %Y') as effectiveDate", FALSE);
        $this->db->select("DATE_FORMAT(`i`.`fecha_name`,'%b %$typeeffectiveDate %Y') as fecha_name", FALSE);
        $this->db->select("DATE_FORMAT(`i`.`date_exchange_rate`,'%b %D %Y %h %i %p') as date_exchange_rate", FALSE);
        $this->db->join('status_in st', 'st.id_status = i.id_status','inner outer');

        $this->db->where('i.id_in',$id_in);
        $query = $this->db->get('inmuebles i');                         
                        
        if($query->num_rows() > 0 )      
            { return $query->result_array(); }
        else 
            { return false; }
    }
    
    public function traeEncabezadoInmueble($id_in,$selectC)
    {                           
        $this->db->select($selectC);
        $this->db->select("DATE_FORMAT(`i`.`effectiveDate`,'%b %D %Y') as effectiveDate", FALSE);        
        $this->db->where('i.id_in',$id_in);
        $query = $this->db->get('inmuebles i');                                     
                        
        if($query->num_rows() > 0 )      
            { return $query->result_array(); }
        else 
            { return false; }
    }
    
    public function traeLetterQuery($id_in,$selectC)
    {                           
        $this->db->select($selectC);        
        $this->db->where('l.id_in',$id_in);
        $query = $this->db->get('letter_in l');                                     
                        
        if($query->num_rows() > 0 )      
            { return $query->result_array(); }
        else 
            { return false; }
    }
    
    public function traeLetter($id_in,$idiom,$selectC)
    {                           
        $this->db->select($selectC);
        $this->db->select("DATE_FORMAT(`i`.`effectiveDate`,'%b %D %Y') as effectiveDate", FALSE);
        $this->db->join('letter_in l', 'l.id_in = i.id_in','inner outer');
        $this->db->join('usuarios fp', 'fp.correo = l.firma_prin','left outer');
        $this->db->join('usuarios fs', 'fs.correo = l.firma_sec','left outer');
        $this->db->where('i.id_in',$id_in);
        $query = $this->db->get('inmuebles i');                                     

        if($query->num_rows() > 0 )      
            {
                $lt    = $query->result_array();
                $leyendaLT  =  array("client"          => $lt[0]['client'],
                                     "client_addres"   => $lt[0]['client_addres'],
                                     "client_addres_2" => $lt[0]['client_addres_2'],
                                     "client_logo"     => $lt[0]['client_logo'],
                                     "atn_officer"     => $lt[0]['atn_officer'],
                                     "jd_atn_officer"  => $lt[0]['jd_atn_officer'],
                                     "jd_atn_officer_2"=> $lt[0]['jd_atn_officer_2'],
                                     "parrafo1"        => $lt[0]['parrafo1'],
                                     "texto1"          => $lt[0]['texto1'],
                                     "texto2"          => $lt[0]['texto2'],
                                     "texto3"          => $lt[0]['texto3'],
                                     "parrafo2"        => $lt[0]['parrafo2'],
                                     "parrafo3"        => $lt[0]['parrafo3'],
                                     "effectiveDate"   => $lt[0]['effectiveDate'],
                                     "firma_prin"      => $lt[0]['firma_prin'],
                                     "firma_sec"       => $lt[0]['firma_sec'],
                                     "puestoFP"        => $lt[0]['puestoFP'], 
                                     "puestoFS"        => $lt[0]['puestoFS'],
                                     "nombreFP"        => $lt[0]['nombreFP'].' '.$lt[0]['apellidosFP'],
                                     "nombreFS"        => $lt[0]['nombreFS'].' '.$lt[0]['apellidosFS'],
                                     "tituloFP"        => $lt[0]['tituloFP'],
                                     "tituloFS"        => $lt[0]['tituloFS'],
                                     "LTchbox1"        => $lt[0]['LTchbox1'],
                                     "LTchbox2"        => $lt[0]['LTchbox2'],
                                     "LTchbox3"        => $lt[0]['LTchbox3'],
                                     "accion"          => 'E') ;                  //25
                
                
                return $leyendaLT;
            }
        else 
            {  
                $encabezado = $this->traeEncabezadoInmueble($id_in,'i.preparedFor,effectiveDate');
                $lt         = $this->traeLeyendasIn('campo, leyenda'.$idiom,'LT',NULL,"l.orden");                 
                $leyendaLT  =  array("client"          => $encabezado[0]['preparedFor'],
                                     "client_addres"   => $lt[1]['leyenda'.$idiom],
                                     "client_addres_2" => $lt[2]['leyenda'.$idiom],
                                     "client_logo"     => $lt[3]['leyenda'.$idiom],
                                     "atn_officer"     => $lt[4]['leyenda'.$idiom],
                                     "jd_atn_officer"  => $lt[5]['leyenda'.$idiom],
                                     "jd_atn_officer_2"=> $lt[6]['leyenda'.$idiom],
                                     "parrafo1"        => $lt[7]['leyenda'.$idiom],
                                     "texto1"          => $lt[8]['leyenda'.$idiom],
                                     "texto2"          => $lt[9]['leyenda'.$idiom],
                                     "texto3"          => $lt[10]['leyenda'.$idiom],
                                     "parrafo2"        => $lt[11]['leyenda'.$idiom],
                                     "parrafo3"        => $lt[12]['leyenda'.$idiom],
                                     "effectiveDate"   => $encabezado[0]['effectiveDate'],
                                     "firma_prin"      => '0',
                                     "firma_sec"       => '0',
                                     "puestoFP"        => '',
                                     "puestoFS"        => '',
                                     "nombreFP"        => '',
                                     "nombreFS"        => '',
                                     "tituloFP"        => '',
                                     "tituloFS"        => '',
                                     "LTchbox1"        => '',
                                     "LTchbox2"        => '',
                                     "LTchbox3"        => '',
                                     "accion"          => 'N');

                return $leyendaLT;
            }
    }
    
    public function traeScopeOfWork($id_in,$idiom,$selectC,$typeDate)
    {   
        $this->db->select($selectC);
        $this->db->select("DATE_FORMAT(`i`.`effectiveDate`,'%b %D %Y') as effectiveDate, DATE_FORMAT(`sw`.`dateInspection`,'%b %D %Y') as dateInspection", FALSE);
        $this->db->join('scopeofwork_in sw', 'sw.id_in = i.id_in','inner outer');        
        $this->db->where('i.id_in',$id_in);
        $query = $this->db->get('inmuebles i');
                        
        if($query->num_rows() > 0 )
            { 
                $lt        = $query->result_array();                
                $leyendaLT =  array("SWParrafo1"     => $lt[0]['SWParrafo1'],
                                   "SWTit1"         => $lt[0]['SWTit1'],
                                   "SWText1"        => $lt[0]['SWText1'],
                                   "SWTit2"         => $lt[0]['SWTit2'],
                                   "SWText2_1"      => $lt[0]['SWText2_1'],
                                   "SWText2_2"      => $lt[0]['SWText2_2'],
                                   "providedBy"     => $lt[0]['providedBy'],
                                   "SWText2_3"      => $lt[0]['SWText2_3'],
                                   "SWTit3"         => $lt[0]['SWTit3'],
                                   "SWText3_1"      => $lt[0]['SWText3_1'],
                                   "dateInspection" => $lt[0]['dateInspection'],
                                   "inspector"      => $lt[0]['inspector'],
                                   "SWText3_2"      => $lt[0]['SWText3_2'],
                                   "SWTit4"         => $lt[0]['SWTit4'],
                                   "SWText4_1"      => $lt[0]['SWText4_1'],
                                   "SWText4_2"      => $lt[0]['SWText4_2'],
                                   "SWText4_3"      => $lt[0]['SWText4_3'],        
                                   "SWTit5"         => $lt[0]['SWTit5'],
                                   "SWText5"        => $lt[0]['SWText5'],
                                   "SWTit6"         => $lt[0]['SWTit6'],
                                   "SWText6"        => $lt[0]['SWText6'],
                                   "SWTit7"         => $lt[0]['SWTit7'],
                                   "SWText7"        => $lt[0]['SWText7'],
                                   "client"         => $lt[0]['client'],
                                   "effectiveDate"  => $lt[0]['effectiveDate'],
                                   "loc"            => $this->utils->creaLocation($lt[0]['calle'],$lt[0]['num'],$lt[0]['col'],$lt[0]['mun'],$lt[0]['edo'],$lt[0]['cp']),
                                   "col"            => $this->utils->creaCol($lt[0]['col'],$lt[0]['mun'],$lt[0]['edo'],NULL),
                                   "accion"         => 'E') ;
                
                return $leyendaLT;
            }
        else 
            { 
                $encabezado = $this->traeEncabezadoInmueble($id_in,'i.preparedFor as client,effectiveDate,i.calle,i.num,i.col,i.mun,i.edo,i.cp');
                $lt         = $this->traeLeyendasIn('id_leyenda, campo, leyenda'.$idiom,'SW',NULL,"l.orden");
                $leyendaLT  = array("SWParrafo1"    => $lt[0]['leyenda'.$idiom],
                                   "SWTit1"         => $lt[1]['leyenda'.$idiom],
                                   "SWText1"        => $lt[2]['leyenda'.$idiom],
                                   "SWTit2"         => $lt[3]['leyenda'.$idiom],
                                   "SWText2_1"      => $lt[4]['leyenda'.$idiom],
                                   "SWText2_2"      => $lt[5]['leyenda'.$idiom],
                                   "providedBy"     => $lt[6]['leyenda'.$idiom],
                                   "SWText2_3"      => $lt[7]['leyenda'.$idiom],
                                   "SWTit3"         => $lt[8]['leyenda'.$idiom],
                                   "SWText3_1"      => $lt[9]['leyenda'.$idiom],
                                   "dateInspection" => $lt[10]['leyenda'.$idiom],
                                   "inspector"      => $lt[11]['leyenda'.$idiom],
                                   "SWText3_2"      => $lt[12]['leyenda'.$idiom],
                                   "SWTit4"         => $lt[13]['leyenda'.$idiom],
                                   "SWText4_1"      => $lt[14]['leyenda'.$idiom],
                                   "SWText4_2"      => $lt[15]['leyenda'.$idiom],
                                   "SWText4_3"      => $lt[16]['leyenda'.$idiom], 
                                   "SWTit5"         => $lt[17]['leyenda'.$idiom],
                                   "SWText5"        => $lt[18]['leyenda'.$idiom],
                                   "SWTit6"         => $lt[19]['leyenda'.$idiom],
                                   "SWText6"        => $lt[20]['leyenda'.$idiom],
                                   "SWTit7"         => $lt[21]['leyenda'.$idiom],
                                   "SWText7"        => $lt[22]['leyenda'.$idiom],
                                   "client"         => $encabezado[0]['client'],
                                   "effectiveDate"  => $encabezado[0]['effectiveDate'],                    
                                   "loc"            => $this->utils->creaLocation($encabezado[0]['calle'],$encabezado[0]['num'],$encabezado[0]['col'],$encabezado[0]['mun'],$encabezado[0]['edo'],$encabezado[0]['cp']),
                                   "col"            => $this->utils->creaCol($encabezado[0]['col'],$encabezado[0]['mun'],$encabezado[0]['edo'],NULL),
                                   "accion"         => 'N') ;
                
                return $leyendaLT;
            }
    }
    
    public function traeSummary($id_in,$idiom,$selectC,$typeDate)
    {
        $this->load->library('Utils');
        $this->db->select($selectC);
        $this->db->select("DATE_FORMAT(`i`.`effectiveDate`,'%b %D %Y') as effectiveDate,DATE_FORMAT(`sm`.`SMvc1_3`,'%b %D %Y') as SMvc1_3,DATE_FORMAT(`sm`.`SMvc2_3`,'%b %D %Y') as SMvc2_3,DATE_FORMAT(`sm`.`SMvc3_3`,'%b %D %Y') as SMvc3_3,DATE_FORMAT(`sm`.`SMvc4_3`,'%b %D %Y') as SMvc4_3,DATE_FORMAT(`sm`.`SMvc5_3`,'%b %D %Y') as SMvc5_3", FALSE);
        $this->db->join('summary_in sm', 'sm.id_in = i.id_in','inner outer');        
        $this->db->join('letter_in l', 'l.id_in = i.id_in','inner outer');        
        $this->db->where('i.id_in',$id_in);
        $query = $this->db->get('inmuebles i');
                        
        if($query->num_rows() > 0 )
            { 
                $lt    = $query->result_array();
                $leyendaLT =  array("SMTit1"    => $lt[0]['SMTit1'],
                                    "SMText1"   => $lt[0]['SMText1'],
                                    "SMTit2"    => $lt[0]['SMTit2'],
                                    "SMText2"   => $lt[0]['SMText2'],
                                    "SMText3"   => $lt[0]['SMText3'],
                                    "SMTit4"    => $lt[0]['SMTit4'],
                                    "SMText4"   => $lt[0]['SMText4'],
                                    "SMTit5"    => $lt[0]['SMTit5'],
                                    "SMText5"   => $lt[0]['SMText5'],
                                    "SMTit6"    => $lt[0]['SMTit6'],
                                    "SMText6"   => $lt[0]['SMText6'],
                                    "SMTit7"    => $lt[0]['SMTit7'],
                                    "SMText7"   => $lt[0]['SMText7'],
                                    "SMvc1_1"   => $lt[0]['SMvc1_1'],
                                    "SMvc1_2"   => $lt[0]['SMvc1_2'],
                                    "SMvc1_3"   => $lt[0]['SMvc1_3'],
                                    "SMvc1_4"   => $lt[0]['SMvc1_4'],
                                    "SMvc2_1"   => $lt[0]['SMvc2_1'],
                                    "SMvc2_2"   => $lt[0]['SMvc2_2'],
                                    "SMvc2_3"   => $lt[0]['SMvc2_3'],
                                    "SMvc2_4"   => $lt[0]['SMvc2_4'],
                                    "SMvc3_1"   => $lt[0]['SMvc3_1'],
                                    "SMvc3_2"   => $lt[0]['SMvc3_2'],
                                    "SMvc3_3"   => $lt[0]['SMvc3_3'],
                                    "SMvc3_4"   => $lt[0]['SMvc3_4'],
                                    "SMvc4_1"   => $lt[0]['SMvc4_1'],
                                    "SMvc4_2"   => $lt[0]['SMvc4_2'],
                                    "SMvc4_3"   => $lt[0]['SMvc4_3'],
                                    "SMvc4_4"   => $lt[0]['SMvc4_4'],
                                    "SMvc5_1"   => $lt[0]['SMvc5_1'],
                                    "SMvc5_2"   => $lt[0]['SMvc5_2'],
                                    "SMvc5_3"   => $lt[0]['SMvc5_3'],
                                    "SMvc5_4"   => $lt[0]['SMvc5_4'],
                                    "LTchbox1"  => $lt[0]['LTchbox1'],
                                    "LTchbox2"  => $lt[0]['LTchbox2'],
                                    "LTchbox3"  => $lt[0]['LTchbox3'],
                                    "SM_ch_vc4" => $lt[0]['SM_ch_vc4'],
                                    "SM_ch_vc5" => $lt[0]['SM_ch_vc5'],
                                    "SM_po"     => $lt[0]['SM_po'],                                                
                                    "SM_sa"     => $this->utils->creaLocation($lt[0]['calle'],$lt[0]['num'],$lt[0]['col'],$lt[0]['mun'],$lt[0]['edo'],$lt[0]['cp']),
                                    "SM_ls"     => $lt[0]['SM_ls'],
                                    "SM_gba"    => $lt[0]['SM_gba'],
                                    "SM_ra"     => $lt[0]['SM_ra'],
                                    "SM_ur"     => $lt[0]['SM_ur'],
                                    "SM_er"     => $lt[0]['SM_er'],
                                    "SM_ed"     => $lt[0]['effectiveDate']);
                                
                return $leyendaLT;
            }
        else 
            { 
                $encabezado = $this->traeEncabezadoInmueble($id_in,'i.preparedFor as client,effectiveDate,i.propertyOf,i.exchange_rate,i.calle,i.num,i.col,i.mun,i.edo,i.cp');
                $lt         = $this->traeLeyendasIn('id_leyenda, campo, leyenda'.$idiom,'SM',NULL,"l.orden");
                $leyendaLT  =  array("SMTit1"   => $lt[0]['leyenda'.$idiom],
                                    "SMText1"   => $lt[1]['leyenda'.$idiom],
                                    "SMText2"   => $lt[3]['leyenda'.$idiom],
                                    "SMTit3"    => $lt[4]['leyenda'.$idiom],
                                    "SMText3"   => $lt[5]['leyenda'.$idiom],
                                    "SMTit4"    => $lt[6]['leyenda'.$idiom],
                                    "SMText4"   => $lt[7]['leyenda'.$idiom],
                                    "SMTit5"    => $lt[8]['leyenda'.$idiom],
                                    "SMText5"   => $lt[9]['leyenda'.$idiom],
                                    "SMTit6"    => $lt[10]['leyenda'.$idiom],
                                    "SMText6"   => $lt[11]['leyenda'.$idiom],
                                    "SMTit7"    => $lt[12]['leyenda'.$idiom],
                                    "SMText7"   => $lt[13]['leyenda'.$idiom],
                                    "SMvc1_1"   => $lt[14]['leyenda'.$idiom],
                                    "SMvc1_2"   => $lt[15]['leyenda'.$idiom],
                                    "SMvc1_3"   => $lt[16]['leyenda'.$idiom],
                                    "SMvc1_4"   => $lt[17]['leyenda'.$idiom],
                                    "SMvc2_1"   => $lt[18]['leyenda'.$idiom],
                                    "SMvc2_2"   => $lt[19]['leyenda'.$idiom],
                                    "SMvc2_3"   => $lt[20]['leyenda'.$idiom],
                                    "SMvc2_4"   => $lt[21]['leyenda'.$idiom],
                                    "SMvc3_1"   => $lt[22]['leyenda'.$idiom],
                                    "SMvc3_2"   => $lt[23]['leyenda'.$idiom],
                                    "SMvc3_3"   => $lt[24]['leyenda'.$idiom],
                                    "SMvc3_4"   => $lt[25]['leyenda'.$idiom],
                                    "SMvc4_1"   => $lt[26]['leyenda'.$idiom],
                                    "SMvc4_2"   => $lt[27]['leyenda'.$idiom],
                                    "SMvc4_3"   => $lt[28]['leyenda'.$idiom],
                                    "SMvc4_4"   => $lt[29]['leyenda'.$idiom],
                                    "SMvc5_1"   => $lt[30]['leyenda'.$idiom],
                                    "SMvc5_2"   => $lt[31]['leyenda'.$idiom],
                                    "SMvc5_3"   => $lt[32]['leyenda'.$idiom],
                                    "SMvc5_4"   => $lt[33]['leyenda'.$idiom],       
                                    "LTchbox1"  => "0",
                                    "LTchbox2"  => "0",
                                    "LTchbox3"  => "0",
                                    "SM_ch_vc4" => "",
                                    "SM_ch_vc5" => "",
                                    "SM_po"     => $encabezado[0]['propertyOf'],                                    
                                    "SM_sa"     => $this->utils->creaLocation($encabezado[0]['calle'],$encabezado[0]['num'],$encabezado[0]['col'],$encabezado[0]['mun'],$encabezado[0]['edo'],$encabezado[0]['cp']),
                                    "SM_ls"     => "",
                                    "SM_gba"    => "",
                                    "SM_ra"     => "",
                                    "SM_ur"     => $encabezado[0]['client'],
                                    "SM_er"     => $encabezado[0]['exchange_rate'],
                                    "SM_ed"     => $encabezado[0]['effectiveDate']);
                return $leyendaLT;
            }
    }
    
    public function traeDefiniciones($id_in,$idiom,$tipoUnion, $orderBy)
    {
        $this->db->select('cd.id_definicion as id_def, cd.concepto'.$idiom.', cd.descripcion'.$idiom.', di.id_in');
        $this->db->join("`definiciones_in` di", "`di.id_definicion = cd.id_definicion and di.id_in = $id_in",$tipoUnion, FALSE);
        $this->db->group_by("cd.id_definicion, cd.concepto$idiom, cd.descripcion$idiom, di.id_in"); 
        $this->db->order_by("cd.$orderBy","asc");
        $query = $this->db->get('catalogo_definiciones cd');        
                        
        if($query->num_rows() > 0 )      
            { return $query->result_array(); }
        else 
            { return array();                }
    }
    
    public function traeIdiom($id_in)
    {
        $this->db->select('i.idiom');
        $this->db->where('id_in',$id_in);
        $query = $this->db->get('inmuebles i');        
                        
        if($query->num_rows() > 0 )      
            { return $query->result_array()[0]['idiom']; }
        else 
            { return "ENG";                              }
    }
    
     public function traeRCVacios($id_in,$tipoComp)
    {
        $this->db->select('rc6_9_titulo');
        $this->db->where('id_in',$id_in);
        $this->db->where('tipo_comp',$tipoComp);
        $this->db->where('rc6_9_titulo <> ""');
        $this->db->group_by("rc6_9_titulo");
        $query = $this->db->get('annexs_a1_in');
        $quitarRC9  = 0;
        $quitarRC10 = 0;
        if($query->num_rows() > 0 )      
            { $result = $query->result_array();
              $quitarRC9 = empty($result[0]['rc6_9_titulo'])?1:0;
            }
        
        $this->db->select('rc6_10_titulo');
        $this->db->where('id_in',$id_in);
        $this->db->where('tipo_comp',$tipoComp);
        $this->db->where('rc6_10_titulo <> ""');
        $this->db->group_by("rc6_10_titulo");
        $query = $this->db->get('annexs_a1_in');
        if($query->num_rows() > 0 )      
            { $result = $query->result_array();
              $quitarRC10 = empty($result[0]['rc6_10_titulo'])?1:0;
            }
        return $quitarRC9 + $quitarRC10;
    }
    
    public function traeSourceInformation($id_in)
    {
        $this->db->select('cm.source_information');        
        $this->db->join('annexs_a1_in a1', 'a1.id_comp = cm.id_comp','inner outer');        
        $this->db->where('a1.id_in',$id_in);
        $this->db->group_by("cm.source_information"); 
        $this->db->order_by("cm.source_information","asc");
        $query = $this->db->get('comparables cm');        
        
        if($query->num_rows() > 0 )      
            {   $StrSI = "";
                $x = -1;
                foreach($query->result_array() as $si)
                {
                    $x++;
                    if($x <= 5)
                        {  $StrSI = ($x!=5)?$StrSI.$si['source_information'].', ':$StrSI.$StrSI.$si['source_information'];}
                }
                
                return    $StrSI;        
            }
        else 
            { return ""; }
    }
    
    public function traDatosCA($id_in,$camposQuery,$tabla, $condicion, $order,$muestraCeros)
    {                           
        $this->db->select($camposQuery);
        if($order !== NULL)
         { $this->db->order_by($order,"asc"); }
        if($condicion !== NULL)
         { $this->db->where($condicion['campo'],$condicion['campoValor']); }
        if($tabla === "annexs_ca_aecw_in" && $condicion['campoValor']!="CP" && $muestraCeros === FALSE)
         { $this->db->where("rcn >",0);
           $this->db->where("dvc_dep >",0);} 
        
        $this->db->where("id_in",$id_in); 
        $query = $this->db->get($tabla);

        if($query->num_rows() > 0 )      
            { return $query->result_array(); }
        else 
            { return array(); }
    }
    
    public function traDatosAPV($id_in,$camposQuery,$tabla, $condicion, $order)
    {                           
        $this->db->select($camposQuery);
        if($order !== NULL)
         { $this->db->order_by($order,"asc"); }
        if($condicion !== NULL)
         { $this->db->where($condicion['campo'],$condicion['campoValor']); }
        
        $this->db->where("id_in",$id_in); 
        $query = $this->db->get($tabla);
                        
        if($query->num_rows() > 0 )      
            { return $query->result_array(); }
        else 
            { return array(array("desc_apv"=>NULL, "land_value_apv"=>NULL, "land_value_2_apv"=>NULL, "value_improvements"=>NULL)); }
    }
    
    public function traCA($id_in,$data_in)
    {                           
        $this->db->select("ca");                        
        $this->db->where("data_id",$data_in); 
        $this->db->where("id_in"  ,$id_in);
        $query = $this->db->get("annexs_ca_data_in")->result();                             
    return $query[0]->ca;
    }
            
    public function traeConsecutivoInmueble($origen_name, $concepto_name, $clave_ano_name,$id_in)
    {
        $this->db->select("consecutivo_name");
        $this->db->where("origen_name"   ,$origen_name); 
        $this->db->where("concepto_name" ,$concepto_name);
        $this->db->where("clave_ano_name",$clave_ano_name);
        $this->db->where("id_in"  ,$id_in);
        $this->db->order_by("consecutivo_name","desc");
        $query = $this->db->get("inmuebles");        
    if($query->num_rows() > 0 ) 
            { 
              $result = $query->result_array();              
              return $result[0]['consecutivo_name'];              
            }
            else
            { $this->db->select("consecutivo_name");
              $this->db->where("origen_name"   ,$origen_name); 
              $this->db->where("concepto_name" ,$concepto_name);
              $this->db->where("clave_ano_name",$clave_ano_name);        
              $this->db->order_by("consecutivo_name","desc");
              $query = $this->db->get("inmuebles");
              if($query->num_rows() > 0 ) 
                { 
                  $result = $query->result_array();              
                  $consecutivo = intval($result[0]['consecutivo_name'])+1;
                  return $consecutivo;
                }
              else
                { return 1; }  
            }                    
    }
    
    
    public function traSumCAXColumn($id_in,$column_in)
    {                           
        $this->db->select("sum(ca) as ca");
        $this->db->where("id_in"  ,$id_in);
        $this->db->like('data_id',$column_in,'after');
        $query = $this->db->get("annexs_ca_data_in");
        if($query->num_rows() > 0 )      
        { $result = $query->result_array();           
          return $result[0]['ca'];
        }
        else
            { return array(0); }
    }
    
    public function traIndicatedValue($id_in)
    {                           
        $this->db->select("indicated_value_cls, indicated_value_crl,indicated_value_crs");
        $this->db->where("id_in"  ,$id_in);
        $query = $this->db->get("inmuebles");
    if($query->num_rows() > 0 )
        { $result = $query->result_array();
          return array("indicated_value_cls"=>$result[0]['indicated_value_cls'], "indicated_value_crl"=>$result[0]['indicated_value_crl'],"indicated_value_crs"=>$result[0]['indicated_value_crs']);
        }
        else
            { return array("indicated_value_cls"=>0, "indicated_value_crl"=>0,"indicated_value_crs"=>0); }
    }
    
    public function traeReportPart($id_in,$reportPart)
    {                           
        $this->db->select("report_part");
        $this->db->where("id_in"  ,$id_in);
        $this->db->where("report_part"  ,$reportPart);
        $query = $this->db->get("reports_parts_in");
    if($query->num_rows() > 0 )
            { return "checked"; }
        else
            { return "";}
    }
    
    public function traSumCA($id_in)
    {                           
        $this->db->select("sum(ca) as ca");
        $this->db->where("id_in"  ,$id_in);
        $query = $this->db->get("annexs_ca_data_in");
        if($query->num_rows() > 0 )      
        { $result = $query->result_array();           
          return $result[0]['ca'];
        }
        else
            { return array(0); }
    }
    
    public function insertaNuevaDef($def,$desc)
    {
        $this->db->select('cd.id_definicion as id_def');
        $this->db->order_by("cd.id_definicion","desc");
        $query = $this->db->get('catalogo_definiciones cd');                                        
        $id_def = $query->result_array()[0]['id_def']+1;
        
        $this->db->insert('catalogo_definiciones',array("id_definicion"=>$id_def,"concepto"=>$def,"descripcion"=>$desc,"id_type"=>1));
        
        return $id_def;        
    }
    
    public function borraDef($id_in)
    {
       $this->db->delete("definiciones_in",       array('id_definicion' => $id_in));
       $this->db->delete("catalogo_definiciones", array('id_definicion' => $id_in));
    }   

    public function traeLeyendasIn($camposSelect, $tipoSeccion,$excluir,$ordenamiento)
    {                           
        $this->db->select($camposSelect);        
        $this->db->where('l.seccion',$tipoSeccion);
        if(!empty($excluir))
            { $this->db->where('l.id_leyenda !=', $excluir); }
        $this->db->order_by($ordenamiento,"asc");
        $query = $this->db->get('leyendas l');
        
                        
        if($query->num_rows() > 0 )      
            { return $query->result_array(); }
        else 
            { return array(); }
    }
    
    public function borraInmueble($id_inmueble)
    {
       $this->db->delete('inmuebles', array('id_in' => $id_inmueble));     
    }      

    public function traeDatosFinancieros()
    {
        $this -> db -> select("dc.tipo_cambio, dc.exchange_rate,dc.ft,dc.ftyear");
        $this->db->where('dc.id_df',1);
        $query = $this -> db -> get("datos_conversion dc");
        
        return $query->result_array();        
    }
    
     public function traeAP($id_in,$idiom,$tipoAP)
    {   $tabla   = "";
        $apVacio = array();
        switch ($tipoAP){
            case "API":
                 $letter  = $this -> md_inmuebles -> traeLetterQuery($id_in,'l.client, l.client_addres, l.client_addres_2');
                 $tabla   = "apprisal_API";
                 $sufijo  = "API";
                 $apVacio = array(array("AIText1_1"  => 'This is an Industrial property located at',
                                        "AIText1_2"  => ', developed in a land area of:',
                                        "AITit2"     => "Purpose of the appraisal",
                                        "AIText2"    => '',
                                        "AITit3"     => "Intended use of the report",
                                        "AIText3"    => '',
                                        "AITit4"     => "Intended user of the report",
                                        "AIText4"    => $letter[0]["client"],
                                        "AITit5"     => "Effective date of the appraisal",
                                        "AITit6"     => "Date of the report",
                                        "AITit7"     => "Property appraised",
                                        "AIText7"    => '',
                                        "AITit8"     => "Location of the Property",
                                        "AITit9"     => "Type of Ownership",
                                        "AIText9"     => "",
                                        "AITit10"    => "Property Owner",
                                        "AITit11"    => "Property interest appraised",
                                        "AIText11"   => "",
                                        "AITit12"    => "Surrounding land uses",                     
                                        "AIText12_1" => "According to direct observation during the physical inspection of the property current uses in the surrounding areas are",
                                        "AIText12_2" => "",
                                        "AIText12_3" => "is I-G-N, Industria Grande no Contaminante (Non-polluting large industry), and in the area are firms like ",
                                        "AIText12_4" => "",
                                        "imgAPI"     => NULL));
                 $this -> db -> select("$sufijo.AIText1_1,$sufijo.AIText1_2, $sufijo.AITit2, $sufijo.AIText2, $sufijo.AITit3, $sufijo.AIText3, $sufijo.AITit4, $sufijo.AIText4, $sufijo.AITit5, $sufijo.AITit6, $sufijo.AITit7, $sufijo.AIText7, $sufijo.AITit8, $sufijo.AITit9, $sufijo.AIText9, $sufijo.AITit10, $sufijo.AITit11, $sufijo.AIText11, $sufijo.AITit12, $sufijo.AIText12_1, $sufijo.AIText12_2, $sufijo.AIText12_3, $sufijo.AIText12_4,$sufijo.imgAPI");
            break;
            case "APII":
                 $tabla   = "apprisal_APII";
                 $sufijo  = "APII";
                 $apVacio = array(array("AIITit1"     => "Address",                                        
                                        "imgApII_1"   => "",
                                        "imgApII_2"   => '',
                                        "imgApII_3"   => '',
                                        "AIITit2"     => "Geographic coordinates (Decimal degrees)",
                                        "AIIText2_1"  => "",
                                        "AIIText2_2"  => "",
                                        "AIITit3"     => "Land surface",
                                        "AIIText3"    => "Land surface according to deed will be used (included in Annex X)",
                                        "AIITit4"     => "Metes and Bounds",
                                        "AIIText4"    => "Metes and Bounds are included in title deed in Annex X",
                                        "AIITit5"     => "Construction Areas of the Property",
                                        "AIIText5"    => "The subject property comprises the following construction areas that can be divided as follows (areas break down):",
                                        "AIITit6"     => "Land description",
                                        "AIIText6"    => "",
                                        "AIITit7"     => "Brief description of improvements",
                                        "AIIText7_1"  => "",
                                        "AIIText7_2"  => "The equipment and complementary works are shown in the following table.",
                                        ));
                 $this -> db -> select("$sufijo.AIITit1,$sufijo.imgApII_1, $sufijo.imgApII_2, $sufijo.imgApII_3, $sufijo.AIITit2, $sufijo.AIIText2_1, $sufijo.AIIText2_2, $sufijo.AIITit3, $sufijo.AIIText3, $sufijo.AIITit4, $sufijo.AIIText4, $sufijo.AIITit5, $sufijo.AIIText5, $sufijo.AIITit6, $sufijo.AIIText6, $sufijo.AIITit7, $sufijo.AIIText7_1, $sufijo.AIIText7_2");
            break;
            case "APIII":
                $tabla   = "apprisal_APIII";
                $sufijo  = "APIII";
                $apVacio = array(array("AIIIText1"    => "<p style=\"text-align: justify;\">The process followed by this work comprises the description of the area where the subject property is located, as well as the identification of the characteristics of the said property regarding location, access roads, topographic conditions, panoramic views, etc. The work includes the application of the sales comparison approach in order to arrive to an indication of the land value plus the application of the cost approach and the income capitalization approach using Direct Capitalization and Discounted Cash Flow Analysis, in order to obtain the market value of the property. Finally, at the express request of the client an opinion of the liquidation value is given.</p>
                                                          <p style=\"text-align: justify;\">This report meets the requirements for a good Mexican valuation practice and it also complies with the Uniform Standards of Professional Appraisal Practice (USPAP) and the International Valuation Standards (IVS).</p>"
                                ));
                $this -> db -> select("$sufijo.AIIIText1");
            break;            
            case "APIV":
                $tabla   = "apprisal_APIV";
                $sufijo  = "APIV";
                $apVacio = array(array("AIVTit1"   => "Definition",                                        
                                       "AIVText1"  => "The highest and best use among the reasonable and probable uses of a property, is the one which is physically possible, legally permissible, and financially feasible, and which results in the highest present value of the property (maximal productivity).",
                                       "AIVTit2"   => "Highest and best use of the land as vacant:",
                                       "AIVText2"   => "<p style=\"text-align: justify;\">a) Physically possible uses: Considering there are no problems with the conditions of the soil and because of the size of the property, under a well-defined project, any use that meets all other requirements such as zoning, financial feasibility, etc. can be considered applicable to the subject property</p>
                                                       <p style=\"text-align: justify;\">b) Legally permissible uses: According to the land use permit issued by the municipality (Annex X), the legally permissible use is</p>
                                                       <p style=\"text-align: justify;\">c) Financially feasible uses: Financial feasibility is the ability of the property to generate sufficient income to support the use for which it was designed. The legally authorized uses and the location of the property define</p>
                                                       <p style=\"text-align: justify;\">d) Maximally profitable: The use that will provide the largest profitability to the subject property is</p>
                                                        ",
                                       "AIVTit3"   => "Conclusion",                                        
                                       "AIVText3"  => "Highest and best use of the",
                                       "AIVTit4"   => "Highest and best use of the land with improvements",                                        
                                       "AIVText4"  => "Due to the characteristics of the property, the surrounding areas and the authorized uses, the highest and best use of the"
                                ));
                $this -> db -> select("$sufijo.AIVTit1,$sufijo.AIVText1,$sufijo.AIVTit2,$sufijo.AIVText2,$sufijo.AIVTit3,$sufijo.AIVText3,$sufijo.AIVTit4,$sufijo.AIVText4");
            break;
            case "APV":
                $tabla   = "apprisal_APV";
                $sufijo  = "APV";
                $apVacio = array(array("AVTit1"   => "Definition",
                                       "AVText1"  => "In the Cost Approach a property is valued based on a comparison with the cost to build a new or substitute property. The cost estimate is adjusted for all types of depreciation in the property. To obtain the final value indication the land value is added to the depreciated value of the improvements.",
                                       "AVTit2"   => "Land Value",
                                       "AVText2_1"=> "<p style=\"text-align: justify;\">The area where the land lot is located is a ...... surrounded by ......, vacant lots in this area are scarce, so for the analysis of the subject, comparables with ...... uses will be used.</p>
                                                      <p style=\"text-align: justify;\">Annex 1 contains market schedules market information for different comparables that were found in the area and Annex 2 includes the adjustments that were made in order to arrive to the following conclusion of the land's value :</p>",
                                       "AVText2_2"=> "In Annex 6 there are maps with the location of the subject's land and comparables.",
                                       "AVTit3"   => "Value of Improvements",                                        
                                       "AVText3"  => "In order to obtain the replacement cost new of the improvements costs manuals such as \"Bimsa Reports\" and ...... are used and are included in the following table. In addition, the entrepreneurial incentive and any type of obsolescence if applicable is included. Once the depreciated value of improvements is obtained the value of the land is added to arrive to an indication of the cost approach value.",
                                       "AVTit4"   => "Unit Cost Computations",
                                       "AVTit5"   => "Reproduction or Replacement Cost New",
                                       "AVTit6"   => "Depreciation computation",
                                       "AVTit7"   => "Depreciated value of constructions",
                                       "AVText7_1"=> "Depreciation is computed using the Age minus Life Method which is given by the following formula:",
                                       "AVText7_2"=> "Depreciation = (Effective Age / Total Economic Life)  x  Cost",
                                       "AVText7_3"=> "According to the information provided by owner the effective age of the construction is \"0\" because constructions are recently concluded.",
                                       "AVTit8"   => "Entrepreneurial Incentive",
                                       "AVText8_1"=> "<p style=\"text-align: justify;\">An economic reward sufficient to induce an entrepreneur to incur the risk associated with the project has to be included.</p>
                                                      <p style=\"text-align: justify;\">According to information received from different investors plus own market information the following entrepreneurial incentive is defined:</p>",
                                       "AVText8_2"=> "",
                                       "AVTit9"   => "Indicated value by the cost approach (rounded):",
                                       "AVTit10"  => "External Obsolescence",
                                       "AVText10" => "<p style=\"text-align: justify;\">When a property produces income, the income loss caused by external obsolescence can be capitalized into an estimate of the loss of the total property value. This procedure is applied in two steps. First, the market is analyzed to quantify the income loss. Next, the income loss is capitalized to obtain the value loss affecting the property as a whole.</p>",
                                       "AVdfaNum" => 0,
                                       "AVdefSNum"=> 0,
                                       "AVoaNum"  => 0,
                                       "AVdefyNum"=> 0,
                                       "AVoAdNum" => 0,
                                       "AV_oe"    => 0,
                                       "AV_vica"  => 0,
                                ));
                $this -> db -> select("$sufijo.AVTit1,$sufijo.AVText1,$sufijo.AVTit2,$sufijo.AVText2_1,$sufijo.AVText2_2,$sufijo.AVTit3,$sufijo.AVText3,$sufijo.AVTit4,$sufijo.AVTit5,$sufijo.AVTit6,$sufijo.AVTit7,$sufijo.AVText7_1,$sufijo.AVText7_2,$sufijo.AVText7_3,$sufijo.AVTit8, $sufijo.AVText8_1,$sufijo.AVText8_2,$sufijo.AVTit9,$sufijo.AVTit10,$sufijo.AVText10,$sufijo.AVdfaNum,$sufijo.AVdefSNum,$sufijo.AVoaNum,$sufijo.AVdefyNum,$sufijo.AVoAdNum, $sufijo.AV_oe, $sufijo.AV_vica");
            break;
            case "APVI":
                $tabla   = "apprisal_APVI";
                $sufijo  = "APVI";
                $apVacio = array(array("AVITit1"           => "Description",
                                       "AVIText1"          => "<p style=\"text-align: justify;\">Based on the Economic Principle of Anticipation, in the income capitalization approach a property's capacity to generate future benefits is analyzed and the income is capitalized into an indication of value. Income-producing properties are typically purchased as an investment and, from the point of view of the investor; the income-generating capacity of a property is the critical element that affects the value.</p>
                                                               <p style=\"text-align: justify;\">A basic premise is one that says that holding risk constant, the greater the income, the greater the value. An investor who acquires an income-producing property, in essence is exchanging a present day investment for future revenue. The two most common methods of converting income into value are direct capitalization and discounted cash flow analysis.</p>",
                                       "AVITit2"           => "Direct Capitalization",
                                       "AVIText2"          => "This is a method used to convert an estimate of a single year's income expectancy into an indication of value in one direct step, either by dividing the net income estimate by an appropriate capitalization rate or by multiplying the income estimate by an appropriate factor. Direct capitalization employs capitalization rates and multipliers extracted or developed from market data. Only a single year's income is used. Yield and value changes are implied but not identified.",
                                       "AVITit3"           => "Potential Gross Income",
                                       "AVIText3"          => "In annex 3, the market rent of comparable properties is presented with its features, plus its adjustment grid, where the following numbers are obtained:",
                                       "AVITit4"           => "Income and Expenses",
                                       "AVITit5"           => "Capitalization Rate Calculation",
                                       "AVIText5_1"        => "Overall capitalization rates can be estimated with various techniques which depend on the quantity and quality of data available. The following provides different methods to derive to a reasonable number:",
                                       "AVIText5_2"        => "a) Publications and / or files data",
                                       "AVIText5_3"        => "Below are different sources that were consulted together with the recommended capitalization rates for similar properties and / or markets",
                                       "AVIText5_4"        => "b) Band of Investment",
                                       "AVIText5_5"        => "The Band of Investment Method is selected to obtain the Capitalization Rate which is a technique in which the capitalization rates attributable to components of a capital investment are combined to derive a weighted minus average rate attributable to the total investment.",
                                       "AVIText5_6"        => "c) Debt Coverage Ratio",
                                       "AVIText5_7"        => "The capitalization rate will be given by the following expression: ",
                                       "AVITit6"           => "Final Conclusion of Capitalization Rate",
                                       "AVIText6_1"        => "Considering a weighted average from previous result we have:",
                                       "AVIText6_2"        => "Dividing Net Operating Income and the Capitalization Rate the value under Direct Income Capitalization is obtained as follows: ",
                                       "AVITit7"           => "Excess Land",
                                       "AVIText7"          => "",
                                       "AVITit8"           => "Discounted cash flow analysis",
                                       "AVIText8_1"        => "<p style=\"text-align: justify;\">Discounted cash flow  analysis (DCF) is a procedure in which a yield rate is applied to a set of income streams and a reversion to determine whether the investment property will produce a required yield given a known acquisition price. If the rate of return is known, DCF analysis can be used to solve for the present value of the property. If the propertyŽs purchase price is known, DCF analysis can be applied to find the rate of return.</p>
                                                               <p style=\"text-align: justify;\">The main premises for the cash flow are as follows:</p>",
                                       "AVIText8_2"        => "In Annex A.7 the Discounted Cash Flow is presented with the following results as of the date of the report:",
                                       "AVIpgi_1_Txt"      => "",
                                       "AVIpgi_1_Mon_mxn"  => "",
                                       "AVIpgi_1_Year_mxn" => "",
                                       "AVIpgi_2_Txt"      => "",
                                       "AVIpgi_2_Mon_mxn"  => "",
                                       "AVIpgi_2_Year_mxn" => "",
                                       "AVIpgi_3_Txt"      => "",
                                       "AVIpgi_3_Mon_mxn"  => "",
                                       "AVIpgi_3_Year_mxn" => "",
                                       "AVIpgi_4_Txt"      => "",
                                       "AVIpgi_4_Mon_mxn"  => "",
                                       "AVIpgi_4_Year_mxn" => "",
                                       "AVIpgi_mvcl_por"   => "",
                                       "AVIoe_1_Txt"       => "Property Tax",
                                       "AVIoe_1_num1"      => "",
                                       "AVIoe_1_num2"      => "",
                                       "AVIoe_2_Txt"       => "Insurance",
                                       "AVIoe_2_num1"      => "",
                                       "AVIoe_2_num2"      => "",
                                       "AVIoe_3_Txt"       => "Administration",
                                       "AVIoe_3_num1"      => "",
                                       "AVIoe_3_num2"      => "",
                                       "AVIoe_4_Txt"       => "Replacement reserves",
                                       "AVIoe_4_num1"      => "",
                                       "AVIoe_4_num2"      => "",
                                       "AVIoe_5_Txt"       => "Maintenance",
                                       "AVIoe_5_num1"      => "",
                                       "AVIoe_5_num2"      => "",
                                       "AVIoe_6_Txt"       => "Security",
                                       "AVIoe_6_num1"      => "",
                                       "AVIoe_6_num2"      => "",
                                       "AVIoe_7_Txt"       => "Broker\'s Commission",
                                       "AVIoe_7_num1"      => "",
                                       "AVIoe_7_num2"      => "",
                                       "AVIoe_8_Txt"       => "",
                                       "AVIoe_8_num1"      => "",
                                       "AVIoe_8_num2"      => "",
                                       "AVIoe_9_Txt"       => "",
                                       "AVIoe_9_num1"      => "",
                                       "AVIoe_9_num2"      => "",
                                       "AVIoe_10_Txt"      => "",
                                       "AVIoe_10_num1"     => "",
                                       "AVIoe_10_num2"     => "",
                                       "AVIoe_11_Txt"      => "",
                                       "AVIoe_11_num1"     => "",
                                       "AVIoe_11_num2"     => "",
                                       "AVIoe_unf"         => "",
                                       "AVIsource_1_Txt"   => "",
                                       "AVIsource_1_ran1"  => "",
                                       "AVIsource_1_ran2"  => "",
                                       "AVIsource_1_av"  => "",
                                       "AVIsource_2_Txt"   => "",
                                       "AVIsource_2_ran1"  => "",
                                       "AVIsource_2_ran2"  => "",
                                       "AVIsource_2_av"  => "",
                                       "AVIsource_3_Txt"   => "",
                                       "AVIsource_3_ran1"  => "",
                                       "AVIsource_3_ran2"  => "",
                                       "AVIsource_3_av"  => "",
                                       "AVIsource_4_Txt"   => "",
                                       "AVIsource_4_ran1"  => "",
                                       "AVIsource_4_ran2"  => "",
                                       "AVIsource_4_av"  => "",
                                       "AVImir"            => "",
                                       "AVImt"             => "",
                                       "AVIlvr"            => "",
                                       "AVIedr"            => "",
                                       "AVIicr"            => "",
                                       "AVImorCo"          => "",
                                       "AVIdcrTit"         => "",
                                       "AVIcapR_por1"      => "",
                                       "AVIcapR_por2"      => "",
                                       "AVIcapR_por3"      => "",
                                       "AVIcap_rate"       => "",
                                       "AVIivudica"        => "",
                                       "AVIfar"            => "",
                                       "AVImf"             => "",
                                       "AVIfArea"          => "",
                                       "AVIivudicael"      => "",
                                       "AVI_noi"           => "",
                                       "AVI_totExp"        => "",
                                ));
                $this -> db -> select(" $sufijo.AVITit1,$sufijo.AVIText1,$sufijo.AVITit2,$sufijo.AVIText2,$sufijo.AVITit3,$sufijo.AVIText3,$sufijo.AVITit4,$sufijo.AVITit5,$sufijo.AVIText5_1 ,$sufijo.AVIText5_2,$sufijo.AVIText5_3,$sufijo.AVIText5_4,$sufijo.AVIText5_5,$sufijo.AVIText5_6,$sufijo.AVIText5_7,$sufijo.AVITit6,$sufijo.AVIText6_1,$sufijo.AVIText6_2,$sufijo.AVITit7,$sufijo.AVIText7,$sufijo.AVITit8,$sufijo.AVIText8_1,$sufijo.AVIText8_2, $sufijo.AVIpgi_1_Txt, $sufijo.AVIpgi_1_Mon_mxn, $sufijo.AVIpgi_1_Year_mxn, $sufijo.AVIpgi_2_Txt, $sufijo.AVIpgi_2_Mon_mxn, $sufijo.AVIpgi_2_Year_mxn, $sufijo.AVIpgi_3_Txt, $sufijo.AVIpgi_3_Mon_mxn, $sufijo.AVIpgi_3_Year_mxn, $sufijo.AVIpgi_4_Txt, $sufijo.AVIpgi_4_Mon_mxn, $sufijo.AVIpgi_4_Year_mxn, $sufijo.AVIpgi_mvcl_por, "
                                    . "$sufijo.AVIoe_1_Txt, $sufijo.AVIoe_1_num1, $sufijo.AVIoe_1_num2, $sufijo.AVIoe_2_Txt, $sufijo.AVIoe_2_num1, $sufijo.AVIoe_2_num2, $sufijo.AVIoe_3_Txt, $sufijo.AVIoe_3_num1, $sufijo.AVIoe_3_num2, $sufijo.AVIoe_4_Txt, $sufijo.AVIoe_4_num1, $sufijo.AVIoe_4_num2, $sufijo.AVIoe_5_Txt, $sufijo.AVIoe_5_num1, $sufijo.AVIoe_5_num2, $sufijo.AVIoe_6_Txt, $sufijo.AVIoe_6_num1, $sufijo.AVIoe_6_num2, $sufijo.AVIoe_7_Txt, $sufijo.AVIoe_7_num1, $sufijo.AVIoe_7_num2, $sufijo.AVIoe_8_Txt, $sufijo.AVIoe_8_num1, $sufijo.AVIoe_8_num2, $sufijo.AVIoe_9_Txt, $sufijo.AVIoe_9_num1, $sufijo.AVIoe_9_num2, $sufijo.AVIoe_10_Txt, $sufijo.AVIoe_10_num1, $sufijo.AVIoe_10_num2, $sufijo.AVIoe_11_Txt, $sufijo.AVIoe_11_num1, $sufijo.AVIoe_11_num2, $sufijo.AVIoe_unf,"
                                    . "$sufijo.AVIsource_1_Txt,$sufijo.AVIsource_1_ran1,$sufijo.AVIsource_1_ran2,$sufijo.AVIsource_1_av, "
                                    . "$sufijo.AVIsource_2_Txt,$sufijo.AVIsource_2_ran1,$sufijo.AVIsource_2_ran2,$sufijo.AVIsource_2_av, "
                                    . "$sufijo.AVIsource_3_Txt,$sufijo.AVIsource_3_ran1,$sufijo.AVIsource_3_ran2,$sufijo.AVIsource_3_av, "
                                    . "$sufijo.AVIsource_4_Txt,$sufijo.AVIsource_4_ran1,$sufijo.AVIsource_4_ran2,$sufijo.AVIsource_4_av, "
                                    . "$sufijo.AVImir, $sufijo.AVImt, $sufijo.AVIlvr, $sufijo.AVIedr, $sufijo.AVIicr, $sufijo.AVImorCo, $sufijo.AVIdcrTit, $sufijo.AVIcapR_por1, $sufijo.AVIcapR_por2, $sufijo.AVIcapR_por3, $sufijo.AVIcap_rate, $sufijo.AVIivudica, "
                                    . "$sufijo.AVIfar, $sufijo.AVImf, $sufijo.AVIfArea, $sufijo.AVIivudicael, $sufijo.AVI_noi, $sufijo.AVI_totExp ");
            break;
            case "APVII":
                $tabla   = "apprisal_APVII";
                $sufijo  = "APVII";
                $apVacio = array(array("AVIIText1"  => "",
                                       "AVIIText2"  => "<p style=\"text-align: justify;\">Understood as “a set of procedures through which a value is derived, by comparing the property being valuated with similar properties that have been sold recently or that are offered in the market applying, subsequently, factors of adjust based on elements of comparison”, under this premise the properties are valued as if offered or sold in the market, for a reasonable period of time.</p><p style=\"text-align: justify;\">Meaning, according to this method, we compare the subject property with others of similar characteristics, recently sold or that are currently found on sale in the market, performing then an adjustment analysis taking into account adjustment elements, such as age, location, state of preservation, etc.</p>",
                                       "AVIIuav"    => "",
                                       "AVIIivusca" => "",
                                       "AVIIIText1" => "<p style=\"text-align: justify;\">It will be governed by the value of the Cost Approach without considering any type of functional / external obsolescence.</p>",                                       
                                       "AIXText1"   => "The values obtained by the application of the different approaches are as follows",
                                       "AXText1"    => "<p style=\"text-align: justify;\">From the above results, the following conclusions can be obtained: The cost approach reflects the value under fee simple conditions. It was obtained through adjusted land prices plus construction costs taken from cost manuals. The constructions areas are based on construction plans provided, so its calculations can be considered within a reasonable range of accuracy.</p><p style=\"text-align: justify;\">Additionally, both the income approach, as well as the sales comparison approach applied are based on market research of similar properties for rent / sale found in industrial sites such as Industrial parks, brokers, etc., all located in nearby properties analyzed, with appropriate quantities and prices that are reasonably within market ranges.</p><p style=\"text-align: justify;\">The type of property being valued could be considered as a typical income producing property, however, it could also be purchased for owner occupation. The market value of the property will be governed by the following blended value:</p>",
                                       "AXText2"    => "",
                                       "AXTit1"     => "Cost Approach",
                                       "AXTit2"     => "Income Capitalization Approach",
                                       "AXTit3"     => "Discounted Cash Flow Approach",
                                       "AXTit4"     => "Sales Comparison Approach",
                                       "AXTit1Chkbx"=> "1",                    
                                       "AXTit2Chkbx"=> "1",                    
                                       "AXTit3Chkbx"=> "1",                    
                                       "AXTit4Chkbx"=> "1",                    
                                       "AXnum1"     => "0",
                                       "AXnum2"     => "0",
                                       "AXnum3"     => "0",
                                       "AXnum5"     => "0",
                                       "AXpor1"     => "0",
                                       "AXpor2"     => "0",
                                       "AXpor3"     => "0",                                       
                                       "AXpor4"     => "0",
                                       "AXpor5"     => "0",
                                       "AXmarketVal"=> "0",
                                ));
                $this -> db -> select(" $sufijo.AVIIText1,$sufijo.AVIIText2,$sufijo.AVIIuav,$sufijo.AVIIivusca,$sufijo.AVIIIText1,$sufijo.AIXText1,$sufijo.AXText1,$sufijo.AXText2,$sufijo.AXText2,$sufijo.AXTit1,$sufijo.AXTit2,$sufijo.AXTit3,$sufijo.AXTit4,$sufijo.AXTit1Chkbx,$sufijo.AXTit2Chkbx,$sufijo.AXTit3Chkbx,$sufijo.AXTit4Chkbx,$sufijo.AXnum1,$sufijo.AXnum2,$sufijo.AXnum3,$sufijo.AXnum5,$sufijo.AXpor1,$sufijo.AXpor2,$sufijo.AXpor3,$sufijo.AXpor4,$sufijo.AXpor5,$sufijo.AXmarketVal " );
            break;
            case "APVIII":
                $tabla   = "apprisal_APVIII";
                $sufijo  = "APVIII";
                $apVacio = array(array("AXIText1"   => "<div><p style=\"text-align: justify;\">Liquidation Value considers the sale of the property in a limited or severely limited exposure time on the market and thus atypical motivation of the seller, which is found in compulsion.</p>".
                                                        "<p style=\"text-align: justify;\">Liquidation Value is obtained using the methodology that establishes a direct relationship between the market value of the property under typical conditions and the value of the property under said limited exposure time. This is accomplished by applying a discount factor that considers time value of money and the costs generated during the exposure time.</p>".
                                                        "<p style=\"text-align: justify;\">In the present case, considering the opinion of the market value of the property and an exposure time of 180 days, the liquidation value is obtained as follows: </p></div>",
                                       "AXIText2"   => "1) Base value:",
                                       "AXIText3"   => "Considering the opinion of the Market Value of the property:",
                                       "AXIText4"   => "2) Discount rate:",
                                       "AXIText5"   => "Risk-free interest rate (RFIR):",
                                       "AXIText6"   => "U.S. Treasury Note (20 years):",
                                       "AXIText7"   => "Source:  ",
                                       "AXIText8"   => "Country risk:",
                                       "AXIText9"   => "Risk by idiosyncrasy components of the country, such as political disturbance, possible expropriation of private assets, barriers to free flow of capital, devaluations, hyperinflations, etc.",
                                       "AXIText10"  => "Source:  ",
                                       "AXIText11"  => "Market risk premium:",                    
                                       "AXIText12"  => "Additional returns when investing in bonds or assets.",
                                       "AXIText13"  => "Source:  ",
                                       "AXIText14"  => "Liquidity:",                    
                                       "AXIText15"  => "Difficulty in converting a cash investment in a reasonable price to a market value in a reasonable time:",                    
                                       "AXIText16"  => "Additional risk",
                                       "AXIText17"  => "The additional risk percentage is established for the purpose of coming to a liquidation value that represents a discount percentage of between 15 and 25% above market value, the previous derived from the property inspection and discussions with real estate brokers and market participants, as to which discount can be applied to similar properties for purposes of achieving the sale within six months.",
                                       "AXIText18"  => "3) Costs:",
                                       "AXIText19"  => "Managing:",
                                       "AXIText20"  => "Sale:",
                                       "AXIText21"  => "Property Tax:",
                                       "AXIText22"  => "4) Sale Term:",                                       
                                       "AXIRateAP"  => "",
                                       "AXISaleTerm"=> "0",
                                       "AXIRate1"   => "0",
                                       "AXIRate2"   => "0",
                                       "AXIRate3"   => "0",
                                       "AXIRate4"   => "0",
                                       "AXIRate5"   => "0",
                                       "AXIRate6"   => "0",
                                       "AXIRate7"   => "0",
                                       "AXIRate8"   => "0",
                                       "AXIRate9"   => "0",
                                       "AXIliquidVal"=> "0",
                                       "AXIIText1"  => "",
                                       "AXIFecha"  => ""
                                ));
                $this -> db -> select(" $sufijo.AXIText1, $sufijo.AXIText2, $sufijo.AXIText3, $sufijo.AXIText4, $sufijo.AXIText5, $sufijo.AXIText6, $sufijo.AXIText7, $sufijo.AXIText8, $sufijo.AXIText9, $sufijo.AXIText10, $sufijo.AXIText11, $sufijo.AXIText12, $sufijo.AXIText13, $sufijo.AXIText14, $sufijo.AXIText15, $sufijo.AXIText16, $sufijo.AXIText17, $sufijo.AXIRate1, $sufijo.AXIRate2, $sufijo.AXIRate3, $sufijo.AXIRate4, $sufijo.AXIRate5, $sufijo.AXIRate6, $sufijo.AXIRate7, $sufijo.AXIRate8, $sufijo.AXIRate9, $sufijo.AXISaleTerm, $sufijo.AXIText18, $sufijo.AXIText19, $sufijo.AXIText20, $sufijo.AXIText21, $sufijo.AXIText22, $sufijo.AXIFecha, $sufijo.AXIRateAP, $sufijo.AXIliquidVal, $sufijo.AXIIText1 ");
                $this -> db -> select("DATE_FORMAT(`$sufijo`.`AXIFecha`,'%b %D %Y') as AXIFecha", FALSE);
            break;
        case "AnnexsLC":
                $letter  = $this -> md_inmuebles   -> traeLetter($id_in,$idiom,'effectiveDate, l.client, l.client_addres, l.client_addres_2, l.client_logo, l.atn_officer, l.jd_atn_officer, l.jd_atn_officer_2, l.parrafo1, l.texto1, l.texto2, l.texto3, l.parrafo2, l.parrafo3, l.firma_prin, l.firma_sec, fp.puesto as puestoFP, fp.nombre as nombreFP, fp.apellidos as apellidosFP, fs.puesto as puestoFS, fs.nombre as nombreFS, fs.apellidos as apellidosFS, fp.titulo as tituloFP, fs.titulo as tituloFS,l.LTchbox1,l.LTchbox2,l.LTchbox3, i.preparedFor as apprisalI735');
                $sw      = $this -> md_inmuebles   -> traeScopeOfWork($id_in,$idiom,'i.preparedFor as client, effectiveDate, dateInspection, sw.providedBy, sw.inspector,sw.SWParrafo1, sw.SWTit1, sw.SWText1, sw.SWTit2, sw.SWText2_1, sw.SWText2_2, sw.SWText2_3, sw.SWTit3, sw.SWText3_1, sw.SWText3_2, sw.SWTit4, sw.SWText4_1, sw.SWText4_2, sw.SWText4_3, sw.SWTit5, sw.SWText5, sw.SWTit6, sw.SWText6, sw.SWTit7, sw.SWText7 , i.calle, i.num, i.col, i.mun, i.edo, i.cp',"D");
                $tabla   = "AnnexsLC";
                $sufijo  = "AnnexsLC";
                $apVacio = array(array("AnnexsLimTxt"   =>  '<div><p style="text-align: justify;"><strong>1. Liability Limit:</strong> The appraiser\'s liability is to whom provided the service; there is no liability to third parties or institutions</p>'.
                                                            '<p style="text-align: justify;"><strong>2. Title Deed:</strong> The appraiser(s) assume(s) no responsibility for matters of a legal nature affecting the property appraised or its title deed. The appraiser assumes the title deed is the proper one and is thus marketable, therefore no opinion of said title is provided.</p>'.
                                                            '<p style="text-align: justify;"><strong>3. Use of the Report:</strong> Standard appraisal regulations request that each member or candidate control the use and distribution of each appraisal duly signed. . Except as maybe otherwise stated in the letter of engagement the report may not be used by any other person(s) other than the intended user(s) or for purposes other than the intended use. JLL will not be responsible for unauthorized uses of this report, its conclusions or contents used partially or in its entirety if it is delivered to non-intended users. Neither all nor any part of the content of the report shall be used for advertisement, newspaper, sales or any other purpose without specific authorization of the appraiser.</p>'.
                                                            '<p style="text-align: justify;"><strong>4. Changes in the Appraisal:</strong> The present work shall be used only in its entirety. All the conclusions and opinions included are those of the appraiser(s) signing this appraisal. No changes shall be made by anyone other than the appraiser.</p>'.
                                                            '<p style="text-align: justify;"><strong>5. Information Used:</strong> All property information provided by the client for the subject including sizes, depictions, entitlements, utilities, plans, development costs, etc. as presented in this report are assumed to be correct and reliable. JLL assumes no responsibility for the accuracy of the information provided by other people, by the client, its representatives or by the public documentation inquired. Attempting to verify all engineering and market aspects would be very expensive. If the client would want to verify the information, we suggest that the corroboration of sale and rent values expressed herein, or any other data regarding the property, be carried out by a specialist in the field.</p>'.
                                                            '<p style="text-align: justify;"><strong>6. Court Testimony:</strong> Total payment of this work will be made upon the complete submittal of the present work. Except as may be otherwise stated in the letter of engagement, neither the appraiser(s), nor those who participated as assistants will be required to appear in court to give testimony or to be present at a hearing, nor will they commit to further consultations, unless arrangements have been previously made involving the corresponding payment. In the event any of the people mentioned above are required through a legal notification, the client will be responsible for the time and the expenses incurred into, regardless of the party requesting the appearance in court.</p>'.
                                                            '<p style="text-align: justify;"><strong>7. Plan, Location Maps and Photographs:</strong> The main purpose of the plans and location maps presented herein is to help the reader of the document fully identify the property and are not necessarily at scale. The photographs have the same purpose; furthermore, the location plans of the land are not topographic surveys, unless otherwise indicated.</p>'.
                                                            '<p style="text-align: justify;"><strong>8. Structure and Conditions of the Subsoil:</strong> The appraiser inspected, as intensely as possible through direct observation, the land and improvements on the property; however it was not possible to prove the ruling conditions in the subsoil, or to carry out a thorough inspection of the mechanical conditions of the structure. The value estimate considers that in the previous aspects there are no conditions that can cause a loss in the property\'s value. The land and the subsoil under it do not show unfavorable conditions, however, the appraiser cannot guarantee there will not be any problems with the subsoil in the future. If considered convenient, we recommend to the parties using the relevant documentation, or in such case, the opinion of specialists in each area.</p>'.
                                                            '<p style="text-align: justify;"><strong>9. Environment and Ecology:</strong> The present work is based on the fact that the property fully complies with the federal and local environmental and ecology provisions, unless otherwise indicated in this report. Likewise, we consider that all the construction and zoning regulations have been met, and all the permits and licenses are valid and may be renovated without any trouble whatsoever on their expiration date.</p>'.
                                                            '<p style="text-align: justify;"><strong>10. Auxiliary Studies:</strong> The present work is based on the fact that the property fully complies with the federal and local environmental and ecology provisions, unless otherwise indicated in this report. Likewise, we consider that all the construction and zoning regulations have been met, and all the permits and licenses are valid and may be renovated without any trouble whatsoever on their expiration date.</p>'.
                                                            '<p style="text-align: justify;"><strong>11. Value of the Currency and Purchasing Power:</strong> The estimated values and the costs used correspond to the day the value was estimated. All the amounts in national currency and in American Dollars are based on the purchasing power and price of our currency on the day of the estimate; likewise, the exchange rate used regarding the United States Dollar could be the one obtained from banks or exchange shops quotations and not necessarily the official one issued by the Bank of Mexico.</p>'.
                                                            '<p style="text-align: justify;"><strong>12. Other types of Properties:</strong> The furniture and the appliances that may be considered personal property as well as operating conditions of the going concern have not been included unless we deal with installations that are typically considered part of the property. In some types of property, the real estate and going concern values may be changed.</p>'.
                                                            '<p style="text-align: justify;"><strong>13. Market Dynamics and Changes in Value:</strong> The estimated Market Value is subject to changes in the market conditions that take place; the value of a property is highly related to its physical conditions, as well as to social, economic and political conditions of its environment. The "Value Estimate" of the present work is obtained as at "The Effective Date of the Appraisal" and thus, is subject to change given the dynamics of the market stated before. The appraiser reserves the right to modify the opinion of value issued here, based on the information not obtained during the normal research work.</p>'.
                                                            '<p style="text-align: justify;"><strong>14. Professional Fees:</strong> The fees paid for the present work correspond to the professional services and not to the time it took to physically carry out the appraisal.</p>'.
                                                            '<p style="text-align: justify;"><strong>15. Toxic Materials:</strong> Unless otherwise indicated, in the present work the appraiser had no knowledge of the presence or absence of toxic materials in the property; the existence of such elements may adversely affect the property being appraised, therefore, the recommendation in such a case would be to appraise it again in order to take into account these negative effects.</p>'.
                                                            '<p style="text-align: justify;"><strong>16. Right to Modify the Values:</strong> The appraisers or the authorized personnel of JLL reserve the right to modify affirmations, analyses, conclusions or any other value estimate contained in this report, if data or general information not known previously, is obtained before the work is finished.</p>'.
                                                            '<p style="text-align: justify;">The acceptance or use of the present work also constitutes the acceptance of conditions stated above.</p></div>',
                    
                                       "AnnexsCertTxt"   => '<div><p style="text-align: justify;"><strong>1)</strong> The statements of fact contained in this report are true and correct.</p>'.
                                                            '<p style="text-align: justify;"><strong>2)</strong> The reported analyses, opinions, and conclusions are limited only by the reported/assumptions and limiting conditions and are my personal, impartial, and unbiased/professional analyses, opinions, and conclusions.</p>'.
                                                            '<p style="text-align: justify;"><strong>3)</strong> We have no present or prospective interest in the property that is the subject of this report and no (or the specified) personal interest with respect to the parties involved.</p>'.
                                                            '<p style="text-align: justify;"><strong>4)</strong> We have performed no (or the specified) services, as an appraiser or in any other capacity, regarding the property that is the subject of this report within the three-year period immediately preceding acceptance of this assignment.</p>'.
                                                            '<p style="text-align: justify;"><strong>5)</strong> We have no bias with respect to the property that is the subject of this report or to the parties involved with this assignment.</p>'.
                                                            '<p style="text-align: justify;"><strong>6)</strong> Our engagement in this assignment was not contingent upon developing or reporting predetermined results.</p>'.
                                                            '<p style="text-align: justify;"><strong>7)</strong> Our compensation for completing this assignment is not contingent upon the development or reporting of a predetermined value or direction in value that favors the cause of the client, the amount of the value opinion, the attainment of a stipulated result, or the occurrence of a subsequent event directly related to the intended use of this appraisal.</p>'.
                                                            '<p style="text-align: justify;"><strong>8)</strong> Our analyses, opinions, and conclusions were developed, and this report has been prepared, in conformity with the Uniform Standards of Professional Appraisal Practice.</p>'.
                                                            '<p style="text-align: justify;"><strong>9)</strong> '.$letter["nombreFP"].' did not inspect the property and '.$sw["inspector"].' made a personal inspection of the property that is the subject of this report.</p>'.
                                                            '<p style="text-align: justify;"><strong>10)</strong> No one provided significant real property appraisal assistance to the person signing this certification. (If there are exceptions, the name of each individual providing significant real property appraisal assistance must be stated.)</p></div>'
                                                            ,
                                ));
                $this -> db -> select(" $sufijo.AnnexsLimTxt, $sufijo.AnnexsCertTxt");
            break;        
        }
        $this -> db -> where("$sufijo.id_in",$id_in);
        $query = $this -> db -> get("$tabla $sufijo");
        
        if($query->num_rows() > 0 )      
            { return $query->result_array(); }
        else 
            { return $apVacio; }
    }
    
    public function traeDatosFinancierosIn($id_in)
    {
        $this -> db -> select(" i.exchange_rate");
        $this -> db -> select("DATE_FORMAT(`i`.`date_exchange_rate`,'%b %D %Y %h %i %p') as date_exchange_rate", FALSE);
        $this->db->where('i.id_in',$id_in);
        $queryI = $this -> db -> get("inmuebles i");
        $in = $queryI->result_array();
                
        $this -> db -> select("dc.tipo_cambio, dc.exchange_rate,dc.ft, dc.ftyear");
        $this -> db -> select("DATE_FORMAT(`dc`.`date_exchange_rate`,'%b %D %Y %h %i %p') as date_exchange_rate", FALSE);
        $this->db->where('dc.id_df',1);
        $query = $this -> db -> get("datos_conversion dc");
        $df = $query->result_array();
        
        if(empty($in[0]['exchange_rate'])) { 
            $exchange_rate      = $df[0]['exchange_rate'];
            $date_exchange_rate = $df[0]['date_exchange_rate'];
        }
        else{ 
            $exchange_rate      = $in[0]['exchange_rate'];
            $date_exchange_rate = $in[0]['date_exchange_rate'];
        }        
        return array("exchange_rate"=>$exchange_rate,"date_exchange_rate"=>$date_exchange_rate,"ft"=>$df[0]['ft'],"ftyear"=>$df[0]['ftyear']);        
    }
    
     public function updateImgAPI($id_in,$nombreImgAPI)
    {                
        $this->db->where('id_in', $id_in);
        $up = $this->db->update('apprisal_API', array("imgAPI"=>$nombreImgAPI));  
        return $up;        
    }
    
    public function updateImgAPII($id_in,$campo,$nombreImgAPI)
    {                
        $this->db->where('id_in', $id_in);
        $up = $this->db->update('apprisal_APII', array("img$campo"=>$nombreImgAPI));  
        return $up;        
    }
    
    public function updateER($er,$hoy)
    {                
        $this->db->where('id_df',  1);
        $up = $this->db->update('datos_conversion', array("exchange_rate"=>$er,"date_exchange_rate"=>$hoy));  
        return $up;        
    }
    
    public function updateFechaName($id_in,$fecha_name)
    {                
        $this->db->where('id_in',  $id_in);
        $up = $this->db->update('inmuebles', array("fecha_name"=>$fecha_name));  
        return $up;        
    }
    
     public function updateERIn($er,$d_er,$id_in)
    {                
        $this->db->where('id_in',  $id_in);
        $up = $this->db->update('inmuebles', array("exchange_rate"=>$er,"date_exchange_rate"=>$d_er));  
        return $up;        
    }
    
}
