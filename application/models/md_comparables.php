<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/**
 * Modelo para interactuar en la BD
 */
 
class Md_comparables extends CI_Model {
	

    public function insertComparable($data)
    {        
       $this->db->insert('comparables',$data);
    }
    
    public function insertComparableRC($data)
    {        
       $this->db->insert('relevant_ch',$data);
    }
    public function deleteComparableRC($id_comp)
    {
        $this->db->delete('relevant_ch', array('id_comp' => $id_comp));
    }  
    
     public function updateComparable($data,$id)
    {        
        $this->db->where('id_comp', $id);
        $this->db->update('comparables', $data);
    }
   
    //ejemplo de eliminar al usuario con id = 1
    public function deleteComparable($id_comp)
    {
        $this->db->delete('comparables', array('id_comp' => $id_comp));
    }       
    
    public function traeDatosFinancierosComp($id_comp)
    {
        $this -> db -> select(" cmp.exchange_rate");
        $this -> db -> select("DATE_FORMAT(`cmp`.`date_exchange_rate`,'%b %D %Y %h %i %p') as date_exchange_rate", FALSE);
        $this->db->where('cmp.id_comp',$id_comp);
        $queryC = $this -> db -> get("comparables cmp");
        $comp = $queryC->result_array();
                
        $this -> db -> select("dc.tipo_cambio, dc.exchange_rate,dc.ft, dc.ftyear");
        $this -> db -> select("DATE_FORMAT(`dc`.`date_exchange_rate`,'%b %D %Y %h %i %p') as date_exchange_rate", FALSE);
        $this->db->where('dc.id_df',1);
        $query = $this -> db -> get("datos_conversion dc");
        $df = $query->result_array();
        
        if(empty($comp[0]['exchange_rate']))
        { 
            $exchange_rate      = $df[0]['exchange_rate'];
            $date_exchange_rate = $df[0]['date_exchange_rate'];
        }
        else
        { 
            $exchange_rate      = $comp[0]['exchange_rate'];
            $date_exchange_rate = $comp[0]['date_exchange_rate'];
        }
        
        return array("exchange_rate"=>$exchange_rate,"date_exchange_rate"=>$date_exchange_rate,"ft"=>$df[0]['ft'],"ftyear"=>$df[0]['ftyear']);
    }
    
    public function traeDatosConversionComp()
    {
        $this -> db -> select("dc.tipo_cambio, dc.exchange_rate,dc.ft, dc.ftyear");
        $this -> db -> select("DATE_FORMAT(`dc`.`date_exchange_rate`,'%b %D %Y %h %i %p') as date_exchange_rate", FALSE);
        $this->db->where('dc.id_df',1);
        $query = $this -> db -> get("datos_conversion dc");
        $df = $query->result_array();                
        
        return array("ft"=>$df[0]['ft'],"ftyear"=>$df[0]['ftyear']);
    }
    
    public function traeComparablesExportExcel($param)
    {	$tipoTabla = "cp";
        if (!empty($param['f1']))
               { $this -> db -> or_like(array("$tipoTabla.type_property" => $param['f1'])); }
           if (!empty($param['f2Ini']) & !empty($param['f2Fin']))
                   { $this->db->where("(`cp`.`price_mx` BETWEEN ".$param['f2Ini']." AND ".$param['f2Fin'].")",NULL, FALSE ); }
           if (!empty($param['f3']))
               { $this -> db -> where("(`$tipoTabla`.`calle` LIKE '%".$param['f3']."%' or `$tipoTabla`.`num` LIKE '%".$param['f3']."%' or `$tipoTabla`.`col` LIKE '%".$param['f3']."%' or `$tipoTabla`.`mun` LIKE '%".$param['f3']."%' or `$tipoTabla`.`edo` LIKE '%".$param['f3']."%' or `$tipoTabla`.`cp` LIKE '%".$param['f3']."%' )"); }
           if (!empty($param['f4']) & !empty($param['f5']))
                   { $this->db->where("(DATE_FORMAT(`cp`.`closing_listing_date`,'%b %d %Y') BETWEEN '".$param['f4']."' AND '".$param['f5']."' )",NULL, FALSE ); }

           if (!empty($param['f6Ini']) & !empty($param['f6Fin']))
                   { $this->db->where("(`cp`.`land_m2` BETWEEN ".$param['f6Ini']." AND ".$param['f6Fin'].")",NULL, FALSE ); }                   

        $this->db->select("cp.id_comp,cp.foto, cp.type_property, cp.calle, cp.num, cp.col, cp.mun, cp.edo, cp.cp, cp.latitud, cp.longitud, cp.source_information, cp.phone, cp.lada, cp.land_m2, cp.land_ft2, cp.construction, cp.price_mx, cp.price_usd, cp.unit_value_mx, cp.time_market, cp.comments, cp.exchange_rate, cp.rc8, cp.closing_listing_date");
        $this->db->select("DATE_FORMAT(`cp`.`date_exchange_rate`,'%b %D %Y %h %i %p') as date_exchange_rate, DATE_FORMAT(`cp`.`closing_listing_date`,'%b %d %Y') as closing_listing_date", FALSE);        
        $this->db->where("cp.tipo", $param["tipo_c"]);
        $query = $this->db->get('comparables cp');        
						
        if($query->num_rows() > 0 )      
            { return $query->result_array(); }
        else 
            { return array(); }
    }
	  	
    public function traeComparablesFiltros($param)
    {                   
        $off       = ( ($param['pagina']-1) * $param['registros_x_pagina']);
        $tipoTabla = "cp";
        $id_in     = $param['id_in'];
        
    	$this -> db -> select("count($tipoTabla.id_comp) as conteo");
        $this -> db -> join('usuarios u', 'u.correo = cp.creado_por','inner outer');
        $this -> db -> join('annexs_a1_in concomp', 'concomp.id_comp = cp.id_comp','left outer');
        if (!empty($id_in))
            { 
            $this->db->_protect_identifiers = FALSE;
            $this -> db -> join('annexs_a1_in aa', 'aa.id_in = '.$id_in.' and aa.id_comp = '.$tipoTabla.'.id_comp','left outer', FALSE);
            }
            
        $this -> db -> from("comparables $tipoTabla");
        $this -> db -> where("$tipoTabla.tipo", $param['tipoComp']);        
           
            if (!empty($param['f1']))
                { $this -> db -> or_like(array("$tipoTabla.type_property" => $param['f1'])); }
            if (!empty($param['f2Ini']) & !empty($param['f2Fin']))
                    { $this->db->where("(`cp`.`price_mx` BETWEEN ".$param['f2Ini']." AND ".$param['f2Fin'].")",NULL, FALSE ); }
            if (!empty($param['f3']))
                { $this -> db -> where("(`$tipoTabla`.`calle` LIKE '%".$param['f3']."%' or `$tipoTabla`.`num` LIKE '%".$param['f3']."%' or `$tipoTabla`.`col` LIKE '%".$param['f3']."%' or `$tipoTabla`.`mun` LIKE '%".$param['f3']."%' or `$tipoTabla`.`edo` LIKE '%".$param['f3']."%' or `$tipoTabla`.`cp` LIKE '%".$param['f3']."%' )"); }
                //{ $this -> db -> or_like(array("$tipoTabla.calsdle" => $param['f3'],"$tipoTabla.num" => $param['f3'],"$tipoTabla.col" => $param['f3'],"$tipoTabla.mun" => $param['f3'],"$tipoTabla.edo" => $param['f3'],"$tipoTabla.cp" => $param['f3'])); }
            if (!empty($param['f4']) & !empty($param['f5']))
                    { $this->db->where("(DATE_FORMAT(`cp`.`closing_listing_date`,'%b %d %Y') BETWEEN '".$param['f4']."' AND '".$param['f5']."' )",NULL, FALSE ); }
            
            if (!empty($param['f6Ini']) & !empty($param['f6Fin']))
                    { $this->db->where("(`cp`.`land_m2` BETWEEN ".$param['f6Ini']." AND ".$param['f6Fin'].")",NULL, FALSE ); }                   
        
        $queryC = $this->db->get()->result();
	$conteo = $queryC[0]->conteo;				
        
        $this -> db -> select("$tipoTabla.id_comp,$tipoTabla.foto,$tipoTabla.type_property, $tipoTabla.calle, $tipoTabla.num, $tipoTabla.col, $tipoTabla.mun, $tipoTabla.edo, $tipoTabla.cp, $tipoTabla.land_m2, $tipoTabla.construction, $tipoTabla.price_mx, $tipoTabla.unit_value_mx, concomp.id_in as trabajoAsignado");
        $this -> db -> select("DATE_FORMAT(`cp`.`fecha_alta`,'%b %D %Y') as fecha_alta, CONCAT_WS(' ',`u`.`nombre`,`u`.`apellidos`) as creadoPor, DATE_FORMAT(`cp`.`closing_listing_date`,'%b %d %Y') as closing_listing_date", FALSE);        
        $this -> db -> join('usuarios u', 'u.correo = cp.creado_por','inner outer');
        $this -> db -> join('annexs_a1_in concomp', 'concomp.id_comp = cp.id_comp','left outer');
       if (!empty($id_in))
            { $this -> db -> select("aa.id_comp as selectComp");
              $this -> db -> join('annexs_a1_in aa', 'aa.id_in = "'.$id_in.'" and aa.id_comp = '.$tipoTabla.'.id_comp','left outer');
            }
        $this -> db -> where("$tipoTabla.tipo", $param['tipoComp']);
        
            if (!empty($param['f1']))
                { $this -> db -> or_like(array("$tipoTabla.type_property" => $param['f1'])); }
            if (!empty($param['f2Ini']) & !empty($param['f2Fin']))
                    { $this->db->where("(`cp`.`price_mx` BETWEEN ".$param['f2Ini']." AND ".$param['f2Fin'].")",NULL, FALSE ); }
            if (!empty($param['f3']))
                 { $this -> db -> where("(`$tipoTabla`.`calle` LIKE '%".$param['f3']."%' or `$tipoTabla`.`num` LIKE '%".$param['f3']."%' or `$tipoTabla`.`col` LIKE '%".$param['f3']."%' or `$tipoTabla`.`mun` LIKE '%".$param['f3']."%' or `$tipoTabla`.`edo` LIKE '%".$param['f3']."%' or `$tipoTabla`.`cp` LIKE '%".$param['f3']."%' )"); }
            if (!empty($param['f4']) & !empty($param['f5']))
                    { $this->db->where("(DATE_FORMAT(`cp`.`closing_listing_date`,'%b %d %Y') BETWEEN '".$param['f4']."' AND '".$param['f5']."' )",NULL, FALSE ); }
            if (!empty($param['f6Ini']) & !empty($param['f6Fin']))
                    { $this->db->where("(`cp`.`land_m2` BETWEEN ".$param['f6Ini']." AND ".$param['f6Fin'].")",NULL, FALSE ); }
        
        $this -> db -> order_by("$tipoTabla.fecha_alta","asc");
        $query = $this -> db -> get("comparables $tipoTabla",$param['registros_x_pagina'],$off);
        $this->db->_protect_identifiers = TRUE;
        if($query -> num_rows() > 0 )
           { return array("conteo"=>$conteo, "registros"=>$query->result_array(), "offset"=>$off, "dirFoto"=>$param['dirFoto'],"tipoComp"=>$param['tipoComp']); }
	else 
            { return false;  }          
  
    }
    
    public function poblaSelect()
    {
	$options = array();
		
	$this->db->select('`correo` as id,  CONCAT(`nombre`,\' \',`apellidos`) as ec', FALSE);		
        $query = $this->db->get('usuarios');
	$options[0] = 'Valuador';					
       if($query -> num_rows() > 0 )
        {		  		    
            foreach ($query->result() as $row)							 
                { $options[$row->id] = $row->ec; }
        }
        
        return $options;       
    }	

    public function traeComparable($id_comp)
    {							
        $this->db->select("cp.id_comp,cp.foto, cp.type_property, cp.calle, cp.num, cp.col, cp.mun, cp.edo, cp.cp, cp.latitud, cp.longitud, cp.source_information, cp.phone, cp.lada, cp.land_m2, cp.land_ft2, cp.construction, cp.price_mx, cp.price_usd, cp.unit_value_mx, cp.time_market, cp.comments, cp.exchange_rate, cp.rc8");
        $this->db->select("DATE_FORMAT(`cp`.`date_exchange_rate`,'%b %D %Y %h %i %p') as date_exchange_rate, DATE_FORMAT(`cp`.`closing_listing_date`,'%b %d %Y') as closing_listing_date", FALSE);
        $this->db->where('cp.id_comp',$id_comp);
        $query = $this->db->get('comparables cp');        
						
        if($query->num_rows() > 0 )      
            { return $query->result_array(); }
        else 
            { return array(); }
    }
    
    public function traeComparableAnexo($id_in,$tipoComp)
    {
	$options = array();	
	$this->db->select("aa.id_comp, aa.rc1, aa.rc2, aa.rc2_adjus, aa.rc3, aa.rc4, aa.rc4_adjus, aa.rc5, aa.rc6_1 as rc6_0, aa.rc6_1, aa.rc6_2, aa.rc6_3, aa.rc6_4, aa.rc6_5, aa.rc6_6, aa.rc6_7, aa.rc6_8, aa.rc6_9, aa.rc6_9_titulo, aa.rc6_10, aa.rc6_10_titulo, aa.rc7, aa.rc8, aa.comments");
        $this->db->join('comparables  cp', 'cp.id_comp = aa.id_comp','inner outer');
        $this->db->where('aa.id_in',$id_in);
        $this->db->where('cp.tipo',$tipoComp);
        $this->db->order_by("aa.orden","asc");
        $query = $this->db->get('annexs_a1_in aa');
       if($query -> num_rows() > 0 )
        {		  		    
            foreach ($query->result() as $row)							 
                { $options[] = $row->id_comp; }
                
             return $options;
        }
        else
            { return false;  }
    }
    
    public function traeDetalleComparableAnexoA1($id_in,$tipoComp)
    {	
	$this->db->select("aa.id_comp, aa.rc1,aa.rc1_adjus,aa.rc1_por, aa.rc2,aa.rc2_adjus,aa.rc2_por, aa.rc3,aa.rc3_adjus,aa.rc3_por, aa.rc4,aa.rc4_adjus,aa.rc4_por, aa.rc5, aa.rc6_1 as rc6_0, aa.rc6_1, aa.rc6_2, aa.rc6_3, aa.rc6_4, aa.rc6_5, aa.rc6_6, aa.rc6_7, aa.rc6_8, aa.rc6_9, aa.rc6_9_titulo, aa.rc6_10, aa.rc6_10_titulo, aa.rc7,aa.rc7_adjus, aa.rc8, aa.comments, aa2.weight");
        $this->db->select("r1.opcion_catalogo as descr1, r2.opcion_catalogo as descr2, r3.opcion_catalogo as descr3, r4.opcion_catalogo as descr4, r5.opcion_catalogo as descr5, r7.opcion_catalogo as descr7, r8.opcion_catalogo as descr8, r9.opcion_catalogo as descr9, r10.opcion_catalogo as descr10, r11.opcion_catalogo as descr11, r12.opcion_catalogo as descr12, r13.opcion_catalogo as descr13, r14.opcion_catalogo as descr14, r15.opcion_catalogo as descr15, r16.opcion_catalogo as descr16, r17.opcion_catalogo as descr17, r18.opcion_catalogo as descr18");
        $this->db->select("CONCAT_WS(' ',' ') as descr6",FALSE);
        $this->db->join('catalogo r1', 'r1.id_opcion = aa.rc1',    'left outer');
        $this->db->join('catalogo r2', 'r2.id_opcion = aa.rc2',    'left outer');
        $this->db->join('catalogo r3', 'r3.id_opcion = aa.rc3',    'left outer');
        $this->db->join('catalogo r4', 'r4.id_opcion = aa.rc4',    'left outer');
        $this->db->join('catalogo r5', 'r5.id_opcion = aa.rc5',    'left outer');
        $this->db->join('catalogo r7', 'r7.id_opcion = aa.rc6_1',  'left outer');
        $this->db->join('catalogo r8', 'r8.id_opcion = aa.rc6_2',  'left outer');
        $this->db->join('catalogo r9', 'r9.id_opcion = aa.rc6_3',  'left outer');
        $this->db->join('catalogo r10', 'r10.id_opcion = aa.rc6_4','left outer');
        $this->db->join('catalogo r11', 'r11.id_opcion = aa.rc6_5','left outer');
        $this->db->join('catalogo r12', 'r12.id_opcion = aa.rc6_6','left outer');
        $this->db->join('catalogo r13', 'r13.id_opcion = aa.rc6_7','left outer');
        $this->db->join('catalogo r14', 'r14.id_opcion = aa.rc6_8','left outer');
        $this->db->join('catalogo r15', 'r15.id_opcion = aa.rc6_9','left outer');
        $this->db->join('catalogo r16', 'r16.id_opcion = aa.rc6_10','left outer');
        $this->db->join('catalogo r17', 'r17.id_opcion = aa.rc7',   'left outer');
        $this->db->join('catalogo r18', 'r18.id_opcion = aa.rc8',   'left outer');
        $this->db->join('annexs_a2_in aa2', 'aa2.id_in = aa.id_in and aa2.id_comp = aa.id_comp and aa2.tipo_comp=aa.tipo_comp','left outer');
        $this->db->where('aa.id_in',$id_in);
        $this->db->where('aa.tipo_comp',$tipoComp);
        $this->db->order_by("aa.orden","asc");
        $query = $this->db->get('annexs_a1_in aa');
       if($query->num_rows() > 0 )      
            { return $query->result_array(); }
        else 
            { return array(); }
    }
    
    public function traeDetalleAnexoA1_2($id_in,$tipoComp)
    {	
	$this->db->select("aa.id_comp, aa.rc1,aa.rc1_adjus,aa.rc1_por, aa.rc2, aa.rc2_adjus,aa.rc2_por, aa.rc3,aa.rc3_adjus,aa.rc3_por, aa.rc4, aa.rc4_adjus,aa.rc4_por, aa.rc5, aa.rc6_1 as rc6_0, aa.rc6_1, aa.rc6_2, aa.rc6_3, aa.rc6_4, aa.rc6_5, aa.rc6_6, aa.rc6_7, aa.rc6_8, aa.rc6_9, aa.rc6_9_titulo, aa.rc6_10, aa.rc6_10_titulo, aa.rc7,aa.rc7_adjus, aa.rc8, aa.comments, aa2.weight");
        $this->db->select("r1.opcion_catalogo as descr1, r2.opcion_catalogo as descr2, r3.opcion_catalogo as descr3, r4.opcion_catalogo as descr4, r5.opcion_catalogo as descr5, r7.opcion_catalogo as descr7, r8.opcion_catalogo as descr8, r9.opcion_catalogo as descr9, r10.opcion_catalogo as descr10, r11.opcion_catalogo as descr11, r12.opcion_catalogo as descr12, r13.opcion_catalogo as descr13, r14.opcion_catalogo as descr14, r15.opcion_catalogo as descr15, r16.opcion_catalogo as descr16, r17.opcion_catalogo as descr17, r18.opcion_catalogo as descr18");
        $this->db->select("cp.id_comp,cp.foto, cp.type_property, cp.calle, cp.num, cp.col, cp.mun, cp.edo, cp.cp, cp.latitud, cp.longitud, cp.source_information, cp.phone, cp.lada, cp.land_m2, cp.land_ft2, cp.construction, cp.price_mx, cp.price_usd, cp.unit_value_mx, cp.unit_value_usd, cp.time_market, cp.exchange_rate");
        $this->db->select("CONCAT_WS(' ',' ') as descr6",FALSE);
        $this->db->join('catalogo r1', 'r1.id_opcion = aa.rc1','left outer');
        $this->db->join('catalogo r2', 'r2.id_opcion = aa.rc2','left outer');
        $this->db->join('catalogo r3', 'r3.id_opcion = aa.rc3','left outer');
        $this->db->join('catalogo r4', 'r4.id_opcion = aa.rc4','left outer');
        $this->db->join('catalogo r5', 'r5.id_opcion = aa.rc5','left outer');
        $this->db->join('catalogo r7', 'r7.id_opcion = aa.rc6_1','left outer');
        $this->db->join('catalogo r8', 'r8.id_opcion = aa.rc6_2','left outer');
        $this->db->join('catalogo r9', 'r9.id_opcion = aa.rc6_3','left outer');
        $this->db->join('catalogo r10', 'r10.id_opcion = aa.rc6_4','left outer');
        $this->db->join('catalogo r11', 'r11.id_opcion = aa.rc6_5','left outer');
        $this->db->join('catalogo r12', 'r12.id_opcion = aa.rc6_6','left outer');
        $this->db->join('catalogo r13', 'r13.id_opcion = aa.rc6_7','left outer');
        $this->db->join('catalogo r14', 'r14.id_opcion = aa.rc6_8','left outer');
        $this->db->join('catalogo r15', 'r15.id_opcion = aa.rc6_9','left outer');
        $this->db->join('catalogo r16', 'r16.id_opcion = aa.rc6_10','left outer');
        $this->db->join('catalogo r17', 'r17.id_opcion = aa.rc7','left outer');
        $this->db->join('catalogo r18', 'r18.id_opcion = aa.rc8','left outer');
        $this->db->join('annexs_a2_in aa2', 'aa2.id_in = aa.id_in and aa2.id_comp = aa.id_comp','left outer');
        $this->db->join('comparables  cp', 'cp.id_comp = aa.id_comp','inner outer');
        $this->db->where('aa.id_in',$id_in);
        $this->db->where('aa.tipo_comp',$tipoComp);
        $this->db->order_by("aa.orden","asc");
        $query = $this->db->get('annexs_a1_in aa');
       if($query->num_rows() > 0 )      
            { return $query->result_array(); }
        else 
            { return array(); }
    }
    
    public function traeRCA1porLlavePrimaria($id_in, $id_comp,$tipoComp)
    {							
        $this->db->select("aa.rc1,aa.rc1_adjus,aa.rc1_por, aa.rc2,aa.rc2_adjus,aa.rc2_por, aa.rc3,aa.rc3_adjus,aa.rc3_por,aa.rc4,aa.rc4_adjus,aa.rc4_por, aa.rc4_adjus, aa.rc5, aa.rc6_1, aa.rc6_2, aa.rc6_3, aa.rc6_4, aa.rc6_5, aa.rc6_6, aa.rc6_7, aa.rc6_8, aa.rc6_9, aa.rc6_9_titulo, aa.rc6_10, aa.rc6_10_titulo, aa.rc7,aa.rc7_adjus, aa.rc8, aa.comments");
        $this->db->where('aa.id_in',$id_in);
        $this->db->where('aa.id_comp',$id_comp);
        $this->db->where('aa.tipo_comp',$tipoComp);
        $query = $this->db->get('annexs_a1_in aa');        

        if($query->num_rows() > 0 )      
            { return $query->result_array(); }
        else 
            { return array(array("rc1"=>0,"rc1_adjus"=>null,"rc1_por"=>0,"rc2"=>0,"rc2_adjus"=>null,"rc2_por"=>0,"rc3"=>0,"rc3_adjus"=>null,"rc3_por"=>0,"rc4"=>0,"rc4_adjus"=>null,"rc4_por"=>0,"rc5"=>0,"rc6_1"=>0,"rc6_2"=>0,"rc6_3"=>0,"rc6_4"=>0,"rc6_5"=>0,"rc6_6"=>0,"rc6_7"=>0,"rc6_8"=>0,"rc6_9"=>0,"rc6_9_titulo"=>null,"rc6_10"=>0,"rc6_10_titulo"=>null,"rc7"=>0,"rc7_adjus"=>null,"rc8"=>0,"comments"=>null)); }
    }
    
    public function traeRC($tipo)
    {							
        $this->db->select("r.id, r.tipo, r.v_1, r.v_2, r.v_3, r.v_4, r.v_5");
        $this->db->where('r.tipo',$tipo);
        $this->db->order_by("r.id","asc");
        $query = $this->db->get('relevant_ch r');
						
        if($query->num_rows() > 0 )      
            { return $query->result_array(); }
        else 
            { return array(); }
    }
    
       
    public function traeRCporTrabajo($id_in,$tipoComp)
    {							
        $this->db->select("r.id, r.tipo, r.campo, r.v_1, r.v_2, r.v_3, r.v_4, r.v_5");
        $this->db->where('r.id_in',$id_in);
        $this->db->where('r.tipo_comp',$tipoComp);
        $this->db->order_by("r.id","asc");
        $query = $this->db->get('relevant_ch r');
						
        if($query->num_rows() > 0 )
            {   return $query->result_array();  }
        else 
            {   $this->db->select("r.id, r.tipo, r.campo, r.v_1, r.v_2, r.v_3, r.v_4, r.v_5");
                $this->db->where('r.id_in',NULL);
                $this->db->where('r.tipo_comp',$tipoComp);
                $this->db->order_by("r.id","asc");
                $query = $this->db->get('relevant_ch r');
                if($query->num_rows() > 0 )
                    { return $query->result_array(); }
                 else 
                    { return array(); }                        
            }
    }
    
     public function updateERComp($er,$d_er,$id_comp)
    {                
        $this->db->where('id_comp',  $id_comp);
        $up = $this->db->update('comparables', array("exchange_rate"=>$er,"date_exchange_rate"=>$d_er));  
        return $up;        
    }
    
    
}
