-- MySQL dump 10.14  Distrib 5.5.47-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: jll_hermes_webapp
-- ------------------------------------------------------
-- Server version	5.5.47-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `annexs_a1_in`
--

DROP TABLE IF EXISTS `annexs_a1_in`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `annexs_a1_in` (
  `id_in` int(11) NOT NULL,
  `id_comp` int(11) NOT NULL,
  `tipo_comp` varchar(3) COLLATE latin1_spanish_ci DEFAULT NULL,
  `orden` int(11) NOT NULL DEFAULT '0',
  `unit_value_mx` double NOT NULL DEFAULT '0',
  `rc1` int(11) DEFAULT '0',
  `rc1_adjus` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `rc1_por` double DEFAULT '0',
  `rc2` int(11) DEFAULT '0',
  `rc2_adjus` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `rc2_por` double DEFAULT '0',
  `rc3` int(11) NOT NULL DEFAULT '0',
  `rc3_adjus` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `rc3_por` double DEFAULT '0',
  `rc4` int(11) NOT NULL DEFAULT '0',
  `rc4_adjus` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `rc4_por` double DEFAULT '0',
  `rc5` int(11) NOT NULL DEFAULT '0',
  `rc6_1` int(11) NOT NULL DEFAULT '0',
  `rc6_2` int(11) NOT NULL DEFAULT '0',
  `rc6_3` int(11) NOT NULL DEFAULT '0',
  `rc6_4` int(11) NOT NULL DEFAULT '0',
  `rc6_5` int(11) NOT NULL DEFAULT '0',
  `rc6_6` int(11) NOT NULL DEFAULT '0',
  `rc6_7` int(11) NOT NULL DEFAULT '0',
  `rc6_8` int(11) NOT NULL DEFAULT '0',
  `rc6_9` int(11) NOT NULL DEFAULT '0',
  `rc6_9_titulo` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `rc6_10` int(11) NOT NULL DEFAULT '0',
  `rc6_10_titulo` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `rc7` int(11) NOT NULL DEFAULT '0',
  `rc7_adjus` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `rc8` int(11) NOT NULL DEFAULT '0',
  `comments` mediumtext COLLATE latin1_spanish_ci,
  PRIMARY KEY (`id_in`,`id_comp`),
  KEY `id_in` (`id_in`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `annexs_a1_in`
--

LOCK TABLES `annexs_a1_in` WRITE;
/*!40000 ALTER TABLE `annexs_a1_in` DISABLE KEYS */;
INSERT INTO `annexs_a1_in` VALUES (5404,5651,'CLS',1,0,3,'0',0,7,'0',0,8,'0',0,11,'0',0,16,15,15,17,15,15,15,15,15,15,'',0,'',16,'Industrial or commercial',1,''),(5404,5792,'CLS',2,0,3,'0',0,7,'0',0,8,'0',0,11,'0',0,15,16,15,15,15,15,15,15,15,0,'',0,'',15,'Industrial or commercial',1,''),(5404,6143,'CLS',3,0,3,'0',0,7,'0',0,8,'0',0,11,'0',0,16,16,15,16,15,15,15,15,15,0,'',0,'',15,'Industrial or commercial',1,''),(5404,6212,'CLS',4,0,3,'0',0,7,'0',0,8,'0',0,11,'0',0,14,16,15,16,15,15,15,15,15,0,'',0,'',14,'Industrial or commercial',1,''),(63153,6428,'CLS',1,0,3,'0',5,7,'0',5,8,'0',5,11,'0',5,13,13,13,13,13,13,13,13,13,0,'',0,'',13,'10',1,'prueba'),(70778,1623,'CLS',4,0,3,'0',0,7,'0',0,8,'0',0,11,'0',0,14,15,15,15,14,15,15,15,15,0,'',0,'',14,'industrial',1,'Land 180000 m2, flat regular shape, Industrial use, water well, sell total or parts.'),(70778,2244,'CRL',5,0,3,'0',0,7,'0',0,8,'0',0,11,'0',0,14,15,15,16,14,15,0,0,0,0,'',0,'',15,'Industrial and commercial',1,'undividied, heigth 9 to 13 m, eigth load and unload bay with manuever yard, high resistance floor, termoacustic cover.'),(70778,2540,'CRS',1,0,3,'0',0,7,'0',0,8,'0',0,11,'0',0,14,15,16,13,15,15,0,0,0,15,'nuevo A6',0,'',14,'Industrial and commercial',1,'Industrial warehouse with offices, mezzanine;covered with galvanized sheet, Average Building Height 10.20 m (88.26 ft).'),(70778,2634,'CLS',1,0,3,'0',0,7,'0',0,8,'0',0,11,'0',0,14,14,15,15,14,14,15,15,15,0,'',0,'',14,'industrial',1,'Land 45,000 m2, flat regular shape, Industrial and commercial use.'),(70778,2987,'CLS',3,0,3,'0',0,7,'0',0,8,'0',0,11,'0',0,17,15,15,17,14,16,15,15,15,0,'',0,'',14,'industrial',1,'Regular Shape, three front, industrial use, near Atlacomulco boot and 500 m from.'),(70778,2995,'CRS',2,0,3,'0',0,7,'0',0,8,'0',0,11,'0',0,13,15,16,13,14,16,0,0,0,15,'nuevo A6',0,'',14,'Industrial and commercial',1,'Industrial Bay whit 28,693 m2, included offices, manuever yard, average height 10 to 13 m.'),(70778,3152,'CRS',3,0,3,'0',0,7,'0',0,8,'0',0,11,'0',0,13,14,14,14,15,14,0,0,0,15,'nuevo A6',0,'',14,'Industrial and commercial',1,'Warehouse in industrial complex, average height 9 mts, loading and unloading area and maneuvering yard.'),(70778,3345,'CRS',4,0,3,'0',0,7,'0',0,8,'0',0,11,'0',0,13,15,14,14,15,16,0,0,0,15,'nuevo A6',0,'',14,'Industrial and commercial',1,'5% office area included, manuever yard, parking yard for visitors, high resistance floor.'),(70778,3491,'CRS',5,0,3,'0',0,7,'0',0,8,'0',0,11,'0',0,14,15,16,14,15,15,0,0,0,15,'nuevo A6',0,'',14,'Industrial and commercial',1,'Industrial bay, offices, tilt-up walls, parking area, cistern.'),(70778,4306,'CRL',1,0,3,'0',0,7,'0',0,8,'0',0,11,'0',0,14,14,15,16,14,15,0,0,0,0,'',0,'',15,'Industrial and commercial',1,'Industrial warehouse with offices, mezzanine, covered with galvanized sheet, Average Building Height 10.20 m (88.26 ft).'),(70778,4608,'CRL',2,0,3,'0',0,7,'0',0,8,'0',0,11,'0',0,14,14,15,16,14,15,0,0,0,0,'',0,'',15,'Industrial and commercial',1,'Industrial Bay whit 28,693 m2, included offices, manuever yard, average height 10 to 13 m.'),(70778,4923,'CRL',3,0,3,'0',0,7,'0',0,8,'0',0,11,'0',0,15,15,15,16,14,15,0,0,0,0,'',0,'',15,'Industrial and commercial',1,'Warehouse in industrial complex, average height 9 mts, loading and unloading area and maneuvering yard.'),(70778,5166,'CRL',4,0,3,'0',0,7,'0',0,8,'0',0,11,'0',0,16,15,15,16,15,15,0,0,0,0,'',0,'',15,'Industrial and commercial',1,' In Industrial park, steel columns, 15 cm concrete floor, average height 15 m.'),(70778,6428,'CLS',5,0,3,'0',0,7,'0',0,8,'0',6,11,'0',0,14,15,16,15,14,15,15,15,15,0,'',0,'',15,'industrial',1,'Land on industrial zone, with all services, for industrial use behind Jugos del Valle.'),(70778,7053,'CLS',2,0,3,'0',0,7,'0',0,8,'0',0,11,'0',0,15,14,15,14,14,15,15,15,15,0,'',0,'',14,'industrial',1,'Land to 800m of Toluca-Atlacomulco Highway suitable for industry and storage bay.'),(85503,1766,'CLS',1,0,3,'0',0,7,'0',0,8,'0',0,11,'0',0,15,15,16,15,15,15,15,15,15,0,'',0,'',15,'Industrial and Commercial',2,' Flat, regular shape, Industrial and commercial use.'),(85503,2100,'CLS',2,0,3,'0',0,7,'0',0,8,'0',0,11,'0',0,14,15,16,15,15,15,15,15,15,0,'',0,'',14,'Residential and commmercial',2,'Flat, near to highway, mixed use.'),(85503,2280,'CLS',3,0,3,'0',0,7,'0',0,8,'0',0,11,'0',0,16,16,16,15,15,15,15,15,15,0,'',0,'',14,'Residential and commercial',1,'Near Zula river, flat, industrial use.'),(85503,2452,'CLS',4,0,3,'0',0,7,'0',0,8,'0',0,11,'0',0,16,15,16,15,16,15,15,15,15,0,'',0,'',15,'Industrial and commercial',1,'Near to highway, flat, irregular shape, mix use, ejidal land in process of regularation.'),(85503,2563,'CLS',5,0,3,'0',0,7,'0',0,8,'0',0,11,'0',0,17,15,16,15,15,15,15,15,15,0,'',0,'',17,'Industrial and commercial',1,'Near to highway, flat, irregular shape, mix use.');
/*!40000 ALTER TABLE `annexs_a1_in` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `annexs_a2_in`
--

DROP TABLE IF EXISTS `annexs_a2_in`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `annexs_a2_in` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_in` int(11) NOT NULL,
  `id_comp` int(11) NOT NULL,
  `tipo_comp` varchar(3) COLLATE latin1_spanish_ci DEFAULT NULL,
  `weight` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=144 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `annexs_a2_in`
--

LOCK TABLES `annexs_a2_in` WRITE;
/*!40000 ALTER TABLE `annexs_a2_in` DISABLE KEYS */;
INSERT INTO `annexs_a2_in` VALUES (74,70778,2540,'CSR',10),(75,70778,2995,'CSR',0),(76,70778,3152,'CSR',0),(77,70778,3345,'CSR',0),(78,70778,3491,'CSR',0),(129,70778,2634,'CLS',25),(130,70778,7053,'CLS',20),(131,70778,2987,'CLS',10),(132,70778,1623,'CLS',25),(133,70778,6428,'CLS',20),(134,70778,4306,'CRL',20),(135,70778,4608,'CRL',20),(136,70778,4923,'CRL',20),(137,70778,5166,'CRL',20),(138,70778,2244,'CRL',20),(139,85503,1766,'CLS',30),(140,85503,2100,'CLS',25),(141,85503,2280,'CLS',15),(142,85503,2452,'CLS',20),(143,85503,2563,'CLS',10);
/*!40000 ALTER TABLE `annexs_a2_in` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `annexs_ca_aecw_in`
--

DROP TABLE IF EXISTS `annexs_ca_aecw_in`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `annexs_ca_aecw_in` (
  `id_aecw` int(11) NOT NULL AUTO_INCREMENT,
  `id_in` int(11) NOT NULL,
  `orden` int(11) NOT NULL DEFAULT '0',
  `tipo` varchar(2) COLLATE latin1_spanish_ci DEFAULT NULL,
  `leyenda` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `unit` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `unit_value` double DEFAULT NULL,
  `rcn` double DEFAULT NULL,
  `efage_dep` double DEFAULT NULL,
  `ul_dep` double DEFAULT NULL,
  `af_dep` double DEFAULT NULL,
  `dvc_dep` double DEFAULT NULL,
  PRIMARY KEY (`id_aecw`,`id_in`)
) ENGINE=InnoDB AUTO_INCREMENT=417 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `annexs_ca_aecw_in`
--

LOCK TABLES `annexs_ca_aecw_in` WRITE;
/*!40000 ALTER TABLE `annexs_ca_aecw_in` DISABLE KEYS */;
INSERT INTO `annexs_ca_aecw_in` VALUES (282,70778,1,'AE','Recirculation Unit for Quick Frozen',1,'Lot',579876.8,579876.8,5,15,0.3333333333333333,193292.26666666666),(283,70778,2,'AE','Conservation Chamber Evaporators',8,'Unit',579876.85,4639014.8,5,15,0.3333333333333333,1546338.2666666666),(284,70778,3,'AE','Unload and Unload Evaporators',4,'Unit',807024.3,3228097.2,5,15,0.3333333333333333,1076032.4),(285,70778,4,'AE','Maquila Area Evaporators',3,'Unit',175686.17,527058.51,5,15,0.3333333333333333,175686.16999999998),(286,70778,5,'AE','Quick Frozen Chamber Evaporators',1,'Lot',1229803.18,1229803.18,5,15,0.3333333333333333,409934.3933333333),(287,70778,6,'AE','Recirculation Air System',1,'Lot',3741924.32,3741924.32,5,15,0.3333333333333333,1247308.1066666665),(288,70778,7,'AE','Condenser and equipment',1,'Lot',1893657.26,1893657.26,5,15,0.3333333333333333,631219.0866666667),(289,70778,8,'AE','Compressor',1,'Lot',6025094.54,6025094.54,5,15,0.3333333333333333,2008364.8466666667),(290,70778,9,'AE','Substation 1500 Kva',1,'Lot',250000,250000,5,15,0.3333333333333333,83333.33333333333),(291,70778,1,'CW','Cistern Potable Water',200,'m3',50368.31,10073662,1,50,0.02,201473.24),(292,70778,2,'CW','Cistern Treated Water',0,'m3',0,0,0,0,0,0),(293,70778,3,'CW','Water Treatment Plant  Cap. 70,000 liters',0,'m3',0,0,0,0,0,0),(294,70778,4,'CW','Cistern ',0,'m3',0,0,0,0,0,0),(295,70778,5,'CW','Cistern ',0,'m3',0,0,0,0,0,0),(296,70778,6,'CW','Manuever yard',3465,'m3',450,1559250,5,30,0.16666666666666666,259875),(377,70778,1,'CP','Paving repairs of manuever yard',5,'m2',100,500,NULL,NULL,NULL,NULL),(378,70778,2,'CP','Metal cover sheet repair',10,'m2',300,3000,NULL,NULL,NULL,NULL),(379,70778,3,'CP','Southeast Wall repairs/complete',0,'m2',150,0,NULL,NULL,NULL,NULL),(380,70778,4,'CP','Paint for rusty areas',0,'m2',89,0,NULL,NULL,NULL,NULL),(409,85503,1,'AE','Substation and transformers',1,'lot',3491586,3491586,7,15,0.4666666666666667,1629406.8),(410,85503,2,'AE','Water Heater',1,'lot',588000,588000,3,15,0.2,117600),(411,85503,3,'AE','Air conditioning',1,'lot',649540.6,649540.6,7,15,0.4666666666666667,303118.94666666666),(412,85503,4,'AE','Scale',3,'unit',1238015.9,3714047.7,7,15,0.4666666666666667,1733222.26),(413,85503,1,'CW','Control access doors and security boot',30,'m2',7197,215910,7,15,0.4666666666666667,100758),(414,85503,2,'CW','Perimeter enclosing wall',548,'ml',795,435660,7,15,0.4666666666666667,203308),(415,85503,3,'CW','Maneuvering yard paving',18095.44,'m2',574,10386782.56,7,15,0.4666666666666667,4847165.194666666),(416,85503,4,'CW','Paving yard and circulations',11162.24,'m2',550,6139232,7,15,0.4666666666666667,2864974.9333333336);
/*!40000 ALTER TABLE `annexs_ca_aecw_in` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `annexs_ca_area_in`
--

DROP TABLE IF EXISTS `annexs_ca_area_in`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `annexs_ca_area_in` (
  `column_Id` int(11) NOT NULL,
  `id_in` int(11) NOT NULL,
  `type_area` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `name_area` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `gba` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `tra` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `def_rcn` varchar(200) COLLATE latin1_spanish_ci DEFAULT NULL,
  `def_value_rcn` double DEFAULT NULL,
  `fdh_rcn` double DEFAULT NULL,
  `fc_rcn` double DEFAULT NULL,
  `uc_rcn` double DEFAULT NULL,
  `por_rcn` double DEFAULT NULL,
  `rcn` double DEFAULT NULL,
  `efage_dep` double DEFAULT NULL,
  `ul_dep` double DEFAULT NULL,
  `af_dep` double DEFAULT NULL,
  `dvc_dep` double DEFAULT NULL,
  PRIMARY KEY (`id_in`,`column_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `annexs_ca_area_in`
--

LOCK TABLES `annexs_ca_area_in` WRITE;
/*!40000 ALTER TABLE `annexs_ca_area_in` DISABLE KEYS */;
INSERT INTO `annexs_ca_area_in` VALUES (1,68447,'Type A','Industrial Bay','GBA','TRA',NULL,0,0,0,0,0,10000,0,0,NULL,0),(2,68447,'Type B','Offices','GBA','0',NULL,0,0,0,0,0,0,0,0,NULL,0),(1,70778,'Type 1','ndustrial Bay I','GBA','TRA','Bimsa 2016 1st Act  Class 4 steel structure industrial warehouse with offices:',7221,1,0.885,6390.585,100,67848840.94500001,15,50,0.3,20354652.2835),(2,70778,'Type 2','ndustrial Bay II','GBA','TRA','Bimsa 2016 1st Act  class 4 Metal structure industrial warehouse with offices:',6391,1.2,0.885,6787.242,100,9936522.288,4,50,0.08,794921.7830400001),(3,70778,'Type 3','ndustrial Bay III','GBA','TRA','Bimsa 2016 1st Act  class 2 Metal Structure Storage Building without offices:',3943,1,0.885,3489.555,100,0,3,50,0.06,0),(4,70778,'Type 4','Administrative Offices','GBA','0','Bimsa 2016 1st Act  Office Building class 2 low 4 stories concrete building:',6089,1,0.885,5388.765,90,8162362.3455,1,50,0.02,163247.24691),(5,70778,'Type 5','ndustrial Bay IV','GBA','TRA','Bimsa 2016 1st Act  Office Building class 2 low 4 stories concrete building:',3493,1,0.885,3091.305,100,766643.64,2,50,0.04,30665.745600000002),(6,70778,'Manuever','Parking yard','0','0',' Bimsa Reports 2016 1st Act Parking Yard',0,1,0.885,0,100,0,0,0,0,0),(7,70778,'Undeveloped Land','Easement Area','0','0',' Bimsa 2016 1st Act  Class 4 steel structure industrial warehouse with offices:',0,1,0,0,100,0,0,0,0,0),(1,85503,'Type I','Industrial building I','GBA','TRA',' Source: Bimsa Reports 2016 2nd edition, Industrial warehouse mixed structure with offices:',4817.97,1,0.939,4524.07383,100,34196343.0625125,12,50,0.24,8207122.335003),(2,85503,'Type II','Industrial building II','GBA','TRA','  Source: Bimsa Reports 2016 2nd edition, Industrial warehouse mixed structure with offices:',3621.21,1,0.939,3400.31619,100,13767438.2122053,12,50,0.24,3304185.1709292717),(3,85503,'Type III','Process building','GBA','TRA','  Source: Bimsa Reports 2016 2nd edition, Industrial warehouse metal structure with offices:',6335.36,1,0.939,5948.903039999999,100,26529252.0849408,22,50,0.44,11672870.917373952),(4,85503,'Type IV','Production plant (32 meter heigth)','GBA','TRA','  Source: Bimsa Reports 2016 2nd edition, Industrial warehouse metal structure with offices:',8462.99,1.5,0.939,11920.121415,100,21013624.43886105,4,50,0.08,1681089.955108884),(5,85503,'Type V','Storage building','GBA','TRA','  Source: Bimsa Reports 2016 2nd edition, Industrial warehouse metal structure with offices:',4831.54,1,0.939,4536.81606,100,9412623.016003199,4,50,0.08,753009.8412802559),(6,85503,'Type VI','Offices','GBA','TRA','Source: Bimsa Reports 2016 2nd edition, Office building',7605.22,1,0.939,7141.301579999999,100,9545920.6480176,15,70,0.21428571428571427,2045554.4245751998),(7,85503,'Type VII','Silos','GBA','TRA',' ',0,1,0.939,0,100,0,0,0,0,0),(8,85503,'Type VIII','Light cover','GBA','TRA','  Source: Bimsa Reports 2016 2nd edition, Storage building with metal structure.\n',2367.17,1,0.939,2222.77263,100,5068343.9231997,17,50,0.34,1723236.9338878982),(9,85503,'Type IX','Machine room','GBA','TRA','Source: Bimsa Reports 2016 2nd edition, Storage building, without offices, metal structure.',3621.21,1,0.939,3400.31619,100,1081300.54842,17,50,0.34,367642.18646279996);
/*!40000 ALTER TABLE `annexs_ca_area_in` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `annexs_ca_data_in`
--

DROP TABLE IF EXISTS `annexs_ca_data_in`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `annexs_ca_data_in` (
  `data_id` int(11) NOT NULL,
  `id_in` int(11) NOT NULL,
  `ca` double NOT NULL,
  PRIMARY KEY (`data_id`,`id_in`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `annexs_ca_data_in`
--

LOCK TABLES `annexs_ca_data_in` WRITE;
/*!40000 ALTER TABLE `annexs_ca_data_in` DISABLE KEYS */;
INSERT INTO `annexs_ca_data_in` VALUES (11,68447,4634.64),(11,70778,8368),(11,85503,7558.75),(12,68447,4604.85),(12,70778,0),(12,85503,0),(13,68447,0),(13,70778,2249),(13,85503,0),(14,70778,0),(14,85503,0),(15,70778,0),(15,85503,0),(16,70778,0),(16,85503,0),(17,70778,0),(17,85503,0),(18,70778,0),(18,85503,0),(19,85503,0),(21,68447,0),(21,70778,0),(21,85503,0),(22,68447,0),(22,70778,925),(22,85503,541.39),(23,68447,1148.37),(23,70778,0),(23,85503,2611.38),(24,70778,539),(24,85503,896.1),(25,70778,0),(25,85503,0),(26,70778,0),(26,85503,0),(27,70778,0),(27,85503,0),(28,70778,0),(28,85503,0),(29,85503,0),(31,70778,0),(31,85503,0),(32,70778,0),(32,85503,0),(33,70778,0),(33,85503,0),(34,70778,0),(34,85503,0),(35,70778,0),(35,85503,4459.52),(36,70778,0),(36,85503,0),(37,70778,0),(37,85503,0),(38,70778,0),(38,85503,0),(39,85503,0),(41,70778,0),(41,85503,0),(42,70778,0),(42,85503,0),(43,70778,0),(43,85503,0),(44,70778,0),(44,85503,0),(45,70778,1683),(45,85503,0),(46,70778,0),(46,85503,1762.87),(47,70778,0),(47,85503,0),(48,70778,0),(48,85503,0),(49,85503,0),(51,70778,0),(51,85503,0),(52,70778,0),(52,85503,0),(53,70778,0),(53,85503,0),(54,70778,0),(54,85503,0),(55,70778,0),(55,85503,0),(56,70778,248),(56,85503,0),(57,70778,0),(57,85503,1230.37),(58,70778,0),(58,85503,844.35),(59,85503,0),(61,70778,0),(61,85503,0),(62,70778,0),(62,85503,0),(63,70778,0),(63,85503,0),(64,70778,0),(64,85503,0),(65,70778,0),(65,85503,0),(66,70778,0),(66,85503,0),(67,70778,3465),(67,85503,0),(68,70778,0),(68,85503,0),(69,85503,426.22),(71,70778,0),(71,85503,0),(72,70778,0),(72,85503,0),(73,70778,0),(73,85503,0),(74,70778,0),(74,85503,0),(75,70778,0),(75,85503,0),(76,70778,0),(76,85503,0),(77,70778,0),(77,85503,0),(78,70778,5222),(78,85503,0),(79,85503,0),(81,85503,0),(82,85503,0),(83,85503,0),(84,85503,0),(85,85503,0),(86,85503,0),(87,85503,0),(88,85503,0),(89,85503,0),(91,85503,0),(92,85503,0),(93,85503,0),(94,85503,0),(95,85503,0),(96,85503,0),(97,85503,0),(98,85503,0),(99,85503,0),(110,85503,0),(111,85503,0),(112,85503,0),(113,85503,0),(114,85503,0),(115,85503,0),(210,85503,0),(211,85503,0),(212,85503,0),(213,85503,0),(214,85503,0),(215,85503,0),(310,85503,0),(311,85503,0),(312,85503,0),(313,85503,0),(314,85503,0),(315,85503,0),(410,85503,0),(411,85503,0),(412,85503,0),(413,85503,0),(414,85503,0),(415,85503,0),(510,85503,0),(511,85503,0),(512,85503,0),(513,85503,0),(514,85503,0),(515,85503,0),(610,85503,381.5),(611,85503,295),(612,85503,234),(613,85503,0),(614,85503,0),(615,85503,0),(710,85503,0),(711,85503,0),(712,85503,0),(713,85503,4638),(714,85503,0),(715,85503,0),(810,85503,0),(811,85503,0),(812,85503,0),(813,85503,0),(814,85503,2280.19),(815,85503,0),(910,85503,0),(911,85503,0),(912,85503,0),(913,85503,0),(914,85503,0),(915,85503,318);
/*!40000 ALTER TABLE `annexs_ca_data_in` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `annexs_ca_totales_in`
--

DROP TABLE IF EXISTS `annexs_ca_totales_in`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `annexs_ca_totales_in` (
  `id_in` int(11) NOT NULL,
  `totCA` double DEFAULT NULL,
  `totGBA` double DEFAULT NULL,
  `totTRA` double DEFAULT NULL,
  PRIMARY KEY (`id_in`),
  CONSTRAINT `tot_CA` FOREIGN KEY (`id_in`) REFERENCES `inmuebles` (`id_in`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `annexs_ca_totales_in`
--

LOCK TABLES `annexs_ca_totales_in` WRITE;
/*!40000 ALTER TABLE `annexs_ca_totales_in` DISABLE KEYS */;
INSERT INTO `annexs_ca_totales_in` VALUES (31259,0,0,0),(70778,22699,14012,12329),(85503,28477.64,28477.64,28477.64);
/*!40000 ALTER TABLE `annexs_ca_totales_in` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `annexs_ca_zone_in`
--

DROP TABLE IF EXISTS `annexs_ca_zone_in`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `annexs_ca_zone_in` (
  `ca_row_id` int(11) NOT NULL,
  `id_in` int(11) NOT NULL,
  `zone` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`ca_row_id`,`id_in`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `annexs_ca_zone_in`
--

LOCK TABLES `annexs_ca_zone_in` WRITE;
/*!40000 ALTER TABLE `annexs_ca_zone_in` DISABLE KEYS */;
INSERT INTO `annexs_ca_zone_in` VALUES (1,68447,'Production'),(1,70778,'Conservation Building'),(1,85503,'Storage No. 1, 2, 3, 4 and 5'),(2,68447,'Packaging'),(2,70778,'Conservation Load and Unload Area'),(2,85503,'Storage'),(3,68447,'Administrative Offices'),(3,70778,'Frozen Building'),(3,85503,'Load and unload bay'),(4,70778,'Frozen Load and Unload Area'),(4,85503,'Maintenance building'),(5,70778,'Administrative offices'),(5,85503,'Process building'),(6,70778,'Machine room'),(6,85503,'Production plant'),(7,70778,'Manuever yard'),(7,85503,'Storage (mascotas)'),(8,70778,'Easement Area (High Voltage Towers)'),(8,85503,'Storage (mascotas)'),(9,85503,'Office'),(10,85503,'Office'),(11,85503,'Office'),(12,85503,'Office'),(13,85503,'Silos'),(14,85503,'Light cover'),(15,85503,'Machine room');
/*!40000 ALTER TABLE `annexs_ca_zone_in` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `annexs_landarea_in`
--

DROP TABLE IF EXISTS `annexs_landarea_in`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `annexs_landarea_in` (
  `id_in` int(11) NOT NULL,
  `taxTxt` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `taxBill` double DEFAULT NULL,
  `deedTxt` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `deed` double DEFAULT NULL,
  `plansTxt` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `plans` double DEFAULT NULL,
  PRIMARY KEY (`id_in`),
  CONSTRAINT `land_in` FOREIGN KEY (`id_in`) REFERENCES `inmuebles` (`id_in`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `annexs_landarea_in`
--

LOCK TABLES `annexs_landarea_in` WRITE;
/*!40000 ALTER TABLE `annexs_landarea_in` DISABLE KEYS */;
INSERT INTO `annexs_landarea_in` VALUES (31259,'21900',43747,'043746.93',0,'0',0),(70778,'101-30-006-60-01-0009',19600,'02081',19600,'Plan',14378),(85503,'Three tax bill',119939,'0',0,'',119939);
/*!40000 ALTER TABLE `annexs_landarea_in` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apprisal_API`
--

DROP TABLE IF EXISTS `apprisal_API`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apprisal_API` (
  `id_API` int(11) NOT NULL AUTO_INCREMENT,
  `id_in` int(11) NOT NULL,
  `AIText1_1` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AIText1_2` mediumtext COLLATE latin1_spanish_ci,
  `AITit2` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AIText2` mediumtext COLLATE latin1_spanish_ci,
  `AITit3` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AIText3` mediumtext COLLATE latin1_spanish_ci,
  `AITit4` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AITit5` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AITit6` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AITit7` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AIText7` mediumtext COLLATE latin1_spanish_ci,
  `AITit8` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AITit9` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AIText9` mediumtext COLLATE latin1_spanish_ci,
  `AITit10` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AITit11` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AIText11` mediumtext COLLATE latin1_spanish_ci,
  `AITit12` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AIText12_1` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AIText12_2` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AIText12_3` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AIText12_4` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `imgAPI` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id_API`,`id_in`),
  KEY `FK_API_IN` (`id_in`),
  CONSTRAINT `FK_API_IN` FOREIGN KEY (`id_in`) REFERENCES `inmuebles` (`id_in`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apprisal_API`
--

LOCK TABLES `apprisal_API` WRITE;
/*!40000 ALTER TABLE `apprisal_API` DISABLE KEYS */;
INSERT INTO `apprisal_API` VALUES (9,70778,'This is an Industrial property located at',', developed in a land area of:','Purpose of the appraisal','jk','Intended use of the report','jk','Intended user of the report','Effective date of the appraisal','Date of the report','Property appraised','jk','Location of the Property','Type of Ownership','jkj','Property Owner','Property interest appraised','jkjk','Surrounding land uses','According to direct observation during the physical inspection of the property current uses in the s','jj','is I-G-N, Industria Grande no Contaminante (Non-polluting large industry), and in the area are firms','kk','he_apI_46546.jpg'),(10,31259,'This is an Industrial property located at',', developed in a land area of: 43,746.92m2, with a Gross Building Area of: 18,047.91 m2, consisting in administrative offices, Packaging plant, Silos, warehouse for raw material, and loading docks.','Purpose of the appraisal','To give an opinion of the Market Value and Orderly Disposition Value of the subject property.','Intended use of the report','Loan Purposes','Intended user of the report','Effective date of the appraisal','Date of the report','Property appraised','Industrial Warehouses','Location of the Property','Type of Ownership','For the purposes of this report we will consider that it is under private property regime, legally regularized and under transferability conditions.','Property Owner','Property interest appraised','The Fee Simple ownership is defined as the set of interests that include the right to own and dispose of a property. These interests constitute the legal and economic ownership, free of encumbrances that may be exchanged for money or a similar asset. The Fee Simple is an interest subject only to limitations of government power, taxation, condemnation, police power or escheat','Surrounding land uses','According to direct observation during the physical inspection of the property current uses in the s','Industrial use','is I-G-N, Industria Grande no Contaminante (Non-polluting large industry), and in the area are firms',' Industrial','');
/*!40000 ALTER TABLE `apprisal_API` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apprisal_APII`
--

DROP TABLE IF EXISTS `apprisal_APII`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apprisal_APII` (
  `id_in` int(11) NOT NULL,
  `AIITit1` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `imgAPII_1` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `imgAPII_2` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `imgAPII_3` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AIITit2` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AIIText2_1` mediumtext COLLATE latin1_spanish_ci,
  `AIIText2_2` mediumtext COLLATE latin1_spanish_ci,
  `AIITit3` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AIIText3` mediumtext COLLATE latin1_spanish_ci,
  `AIITit4` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AIIText4` mediumtext COLLATE latin1_spanish_ci,
  `AIITit5` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AIIText5` mediumtext COLLATE latin1_spanish_ci,
  `AIITit6` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AIIText6` mediumtext COLLATE latin1_spanish_ci,
  `AIITit7` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AIIText7_1` mediumtext COLLATE latin1_spanish_ci,
  `AIIText7_2` mediumtext COLLATE latin1_spanish_ci,
  PRIMARY KEY (`id_in`),
  CONSTRAINT `inmuebles` FOREIGN KEY (`id_in`) REFERENCES `inmuebles` (`id_in`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apprisal_APII`
--

LOCK TABLES `apprisal_APII` WRITE;
/*!40000 ALTER TABLE `apprisal_APII` DISABLE KEYS */;
INSERT INTO `apprisal_APII` VALUES (31259,'Address','','','','Geographic coordinates (Decimal degrees)','19.15','97.50','Land surface','Land surface according to deed will be used (included in Annex X)','Metes and Bounds','Metes and Bounds are included in title deed in Annex X','Construction Areas of the Property','The subject property comprises the following construction areas that can be divided as follows (areas break down):','Land description','Flat Land','Brief description of the property','Industrial','The equipment and complementary works are shown in the following table.'),(70778,'Address','','he_apII_83992.jpg','','Geographic coordinates (Decimal degrees)','19.702344','-99.235016','Land surface','Land surface according to deed will be used (included in Annex X)','Metes and Bounds','Metes and Bounds are included in title deed in Annex X','Construction Areas of the Property','The subject property comprises the following construction areas that can be divided as follows (areas break down):','Land description','land','Brief description of the property','brief','The equipment and complementary works are shown in the following table.');
/*!40000 ALTER TABLE `apprisal_APII` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apprisal_APIII`
--

DROP TABLE IF EXISTS `apprisal_APIII`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apprisal_APIII` (
  `id_in` int(11) NOT NULL,
  `AIIIText1` longtext COLLATE latin1_spanish_ci,
  PRIMARY KEY (`id_in`),
  CONSTRAINT `IN_APIII` FOREIGN KEY (`id_in`) REFERENCES `inmuebles` (`id_in`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apprisal_APIII`
--

LOCK TABLES `apprisal_APIII` WRITE;
/*!40000 ALTER TABLE `apprisal_APIII` DISABLE KEYS */;
INSERT INTO `apprisal_APIII` VALUES (31259,'<p style=\"text-align: justify;\">The process followed by this work comprises the description of the area where the subject property is located, as well as the identification of the characteristics of the said property regarding location, access roads, topographic conditions, panoramic views, etc. The work includes the application of the sales comparison approach in order to arrive to an indication of the land value plus the application of the cost approach and the income capitalization approach using Direct Capitalization and Discounted Cash Flow Analysis, in order to obtain the market value of the property. Finally, at the express request of the client an opinion of the liquidation value is given.</p>\n                                                          <p style=\"text-align: justify;\">This report meets the requirements for a good Mexican valuation practice and it also complies with the Uniform Standards of Professional Appraisal Practice (USPAP) and the International Valuation Standards (IVS).</p>'),(70778,'<p style=\"text-align: justify;\">The process followed by this work comprises the description of the area where the subject property is located, as well as the identification of the characteristics of the said property regarding location, access roads, topographic conditions, panoramic views, etc. The work includes the application of the sales comparison approach in order to arrive to an indication of the land value plus the application of the cost approach and the income capitalization approach using Direct Capitalization and Discounted Cash Flow Analysis, in order to obtain the market value of the property. Finally, at the express request of the client an opinion of the liquidation value is given.</p>\n<p style=\"text-align: justify;\">This report meets the requirements for a good Mexican valuation practice and it also complies with the Uniform Standards of Professional Appraisal Practice (USPAP) and the International Valuation Standards (IVS).</p>'),(85503,'<p style=\"text-align: justify;\">T<span style=\"font-family: Arial, sans-serif;\">The process followed by this work comprises the description of the area where the subject property is located, as well as the identification of the characteristics of the same property regarding location, access roads, topographic conditions, panoramic views, etc.</span><span style=\"font-family: Arial, sans-serif;\">&nbsp;&nbsp; </span><span style=\"font-family: Arial, sans-serif;\">The work includes the application of the cost approach in order to obtain the market value of the property plus the calculation of the liquidation value.</span></p>\n<p class=\"MsoNormal\"><span style=\"font-family: \'Arial\',sans-serif;\">The present report includes the computation of the Market Value for a typical buyer that is not interested in the current use of the property (Value in Exchange) and also the Value in Use which represents the value considering the use for which the property is devoted. After this a blended value is obtained taking into account an estimate of probabilistic factors of possible purchasers.</span></p>\n<p class=\"MsoNormal\"><span style=\"font-family: \'Arial\',sans-serif;\">As to the machinery and equipment, the process followed was the implementation of the cost approach, affecting the property for demerits of age and any existing obsolescence, to arrive at the Market Value for Continued Use, which includes all direct and indirect costs of freight, installation, various materials, etc. necessary for the operation of the equipment.</span></p>');
/*!40000 ALTER TABLE `apprisal_APIII` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apprisal_APIV`
--

DROP TABLE IF EXISTS `apprisal_APIV`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apprisal_APIV` (
  `id_in` int(11) NOT NULL,
  `AIVTit1` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AIVText1` mediumtext COLLATE latin1_spanish_ci,
  `AIVTit2` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AIVText2` longtext COLLATE latin1_spanish_ci,
  `AIVTit3` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AIVText3` mediumtext COLLATE latin1_spanish_ci,
  `AIVTit4` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AIVText4` mediumtext COLLATE latin1_spanish_ci,
  PRIMARY KEY (`id_in`),
  CONSTRAINT `IN_APIV` FOREIGN KEY (`id_in`) REFERENCES `inmuebles` (`id_in`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apprisal_APIV`
--

LOCK TABLES `apprisal_APIV` WRITE;
/*!40000 ALTER TABLE `apprisal_APIV` DISABLE KEYS */;
INSERT INTO `apprisal_APIV` VALUES (31259,'Definition','The highest and best use among the reasonable and probable uses of a property, is the one which is physically possible, legally permissible, and financially feasible, and which results in the highest present value of the property (maximal productivity).','Highest and best use of the land as vacant:','<p style=\"text-align: justify;\">a) Physically possible uses: Considering there are no problems with the conditions of the soil and because of the size of the property, under a well defined project, any use that meets all other requirements such as zoning, financial feasibility, etc. can be considered applicable to the subject property</p>\n                                                       <p style=\"text-align: justify;\">b) Legally permissible uses: According to the land use permit issued by the municipality (Annex X), the legally permissible use is</p>\n                                                       <p style=\"text-align: justify;\">c) Financially feasible uses: Financial feasibility is the ability of the property to generate sufficient income to support the use for which it was designed. The legally authorized uses and the location of the property define</p>\n                                                       <p style=\"text-align: justify;\">d) Maximally profitable: The use that will provide the largest profitability to the subject property is</p>\n                                                        ','Conclusion','Highest and best use of the','Highest and best use of the land with improvements','Due to the characteristics of the property, the surrounding areas and the authorized uses, the highest and best use of the'),(70778,'Definition','The highest and best use among the reasonable and probable uses of a property, is the one which is physically possible, legally permissible, and financially feasible, and which results in the highest present value of the property (maximal productivity).','Highest and best use of the land as vacant:','<p style=\"text-align: justify;\">a) Physically possible uses: Considering there are no problems with the conditions of the soil and because of the size of the property, under a well defined project, any use that meets all other requirements such as zoning, financial feasibility, etc. can be considered applicable to the subject property</p>\n<p style=\"text-align: justify;\">b) Legally permissible uses: According to the land use permit issued by the municipality (Annex X), the legally permissible use is</p>\n<p style=\"text-align: justify;\">c) Financially feasible uses: Financial feasibility is the ability of the property to generate sufficient income to support the use for which it was designed. The legally authorized uses and the location of the property define</p>\n<p style=\"text-align: justify;\">d) Maximally profitable: The use that will provide the largest profitability to the subject property is</p>','Conclusion','Highest and best use of the','Highest and best use of the land with improvements','Due to the characteristics of the property, the surrounding areas and the authorized uses, the highest and best use of the'),(85503,'Definition','The highest and best use among the reasonable and probable uses of a property, is the one which is physically possible, legally permissible, and financially feasible, and which results in the highest present value of the property (maximal productivity).','Highest and best use of the land as vacant:','<p style=\"text-align: justify;\">a) Physically possible uses:&nbsp;<span style=\"font-family: Arial, sans-serif; font-size: 11.5pt;\">Considering there are no problems with the conditions of the soil and because of the size of the property, under a well defined project, the Industrial use can be considered applicable to the subject property</span></p>\n<p style=\"text-align: justify;\">b) Legally permissible uses: <span style=\"color: #c00000;\"><span style=\"background-color: #ffff00;\">&nbsp;</span></span><span style=\"text-indent: -18pt; font-family: Arial, sans-serif;\">According to the zoning permit by the municipality, the zoning is Industria Pesada (T-3).</span></p>\n<p class=\"MsoListParagraph\" style=\"margin-bottom: 12.0pt; mso-add-space: auto; text-indent: -18.0pt; mso-list: l0 level1 lfo1;\">&nbsp;</p>\n<p style=\"text-align: justify;\">c) Financially feasible uses: Financial feasibility is the ability of the property to generate sufficient income to support the use for which it was designed. The legally authorized uses and the location of the property define Industrial.</p>\n<p style=\"text-align: justify;\">d) Maximally profitable: The use that will provide the largest profitability to the subject property is Industrial.</p>','Conclusion','Highest and best use of the land as vacant corresponds to industrial use. with the above considerations.','Highest and best use of the land with improvements','Due to the characteristics of the property, the surrounding areas and the authorized uses, the highest and best use of the land with improvement are those of industrial use.');
/*!40000 ALTER TABLE `apprisal_APIV` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apprisal_APV`
--

DROP TABLE IF EXISTS `apprisal_APV`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apprisal_APV` (
  `id_in` int(11) NOT NULL,
  `AVTit1` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AVText1` mediumtext COLLATE latin1_spanish_ci,
  `AVTit2` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AVText2_1` mediumtext COLLATE latin1_spanish_ci,
  `AVText2_2` mediumtext COLLATE latin1_spanish_ci,
  `AVTit3` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AVText3` mediumtext COLLATE latin1_spanish_ci,
  `AVTit4` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AVTit5` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AVTit6` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AVTit7` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AVText7_1` mediumtext COLLATE latin1_spanish_ci,
  `AVText7_2` mediumtext COLLATE latin1_spanish_ci,
  `AVText7_3` mediumtext COLLATE latin1_spanish_ci,
  `AVTit8` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AVText8_1` mediumtext COLLATE latin1_spanish_ci,
  `AVText8_2` mediumtext COLLATE latin1_spanish_ci,
  `AVTit9` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AVTit10` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AVText10` mediumtext COLLATE latin1_spanish_ci,
  `AVdfaNum` double NOT NULL DEFAULT '0',
  `AVdefSNum` double NOT NULL DEFAULT '0',
  `AVoaNum` double NOT NULL DEFAULT '0',
  `AVdefyNum` double NOT NULL DEFAULT '0',
  `AVoAdNum` double NOT NULL DEFAULT '0',
  `AV_oe` double DEFAULT NULL,
  `AV_vica` double DEFAULT NULL,
  PRIMARY KEY (`id_in`),
  CONSTRAINT `FK_APV_IN` FOREIGN KEY (`id_in`) REFERENCES `inmuebles` (`id_in`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apprisal_APV`
--

LOCK TABLES `apprisal_APV` WRITE;
/*!40000 ALTER TABLE `apprisal_APV` DISABLE KEYS */;
INSERT INTO `apprisal_APV` VALUES (70778,'Definition','In the Cost Approach a property is valued based on a comparison with the cost to build a new or substitute property. The cost estimate is adjusted for all types of depreciation of in the property. To obtain the final value indication  the land value is added to the depreciated value of the improvements.','Land Value','<p style=\"text-align: justify;\">The area where the land lot is located is a ...... surrounded by ......, vacant lots in this area are scarce, so for the analysis of the subject, comparables with ...... uses will be used.</p>\n                                                      <p style=\"text-align: justify;\">Annex 1 contains market schedules market information for different comparables that were found in the area and Annex 2 includes the adjustments that were made in order to arrive to the following conclusion of the land\'s value :</p>','In Annex 6 there are maps with the location of the subject\'s land and comparables.','Value of Improvements','In order to obtain the replacement cost new of the improvements costs manuals such as \"Bimsa Reports\" and ...... are used and are included in the following table. In addition, the entrepreneurial incentive and any type of obsolescence if applicable is included. Once the depreciated value of improvements is obtained the value of the land is added to arrive to an indication of the cost approach value.','Unit Cost Computations','Reproduction or Replacement Cost New','Depreciation computation','Depreciated value of constructions','Depreciation is computed using the Age minus Life Method which is given by the following formula:','Depreciation = (Effective Age / Total Economic Life)  x  Cost','According to the information provided by owner the effective age of the construction is \"0\" because constructions are recently concluded.','Entrepreneurial Incentive','<p style=\"text-align: justify;\">An economic reward sufficient to induce an entrepreneur to incur the risk associated with the project has to be included.</p>\n                                                      <p style=\"text-align: justify;\">According to information received from different investors plus own market information the following entrepreneurial incentive is defined:</p>','12','Indicated value by the cost approach (rounded):','External Obsolescence','<p style=\"text-align: justify;\">When a property produces income, the income loss caused by external obsolescence can be capitalized into an estimate of the loss in total property value. This procedure is applied in two steps. First, the market is analyzed to quantify the income loss. Next, the income loss is capitilized to obtain the value loss affecting the property as a whole.</p>',0,0,0,0,15000000,1674663.8345198,5508913.9328816),(85503,'Definition','In the Cost Approach a property is valued based on a comparison with the cost to build a new or substitute property. The cost estimate is adjusted for all types of depreciation of in the property. To obtain the final value indication  the land value is added to the depreciated value of the improvements.','Land Value','<p class=\"MsoNormal\" style=\"margin-bottom: .0001pt;\"><span style=\"font-family: \'Arial\',sans-serif;\">The area where the land lot is located is surrounded by residential, commercial and some industrial development according to land use expedited by Ocotal municipality.</span></p>\n<p style=\"text-align: justify;\">&nbsp;</p>\n<p class=\"MsoNormal\" style=\"margin-bottom: .0001pt;\"><span style=\"font-family: \'Arial\',sans-serif;\">Attached you will find charts with market information for different comparables that we found in the area and adjustments that were made in order to arrive to the following conclusion of the land&rsquo;s value :</span></p>','In Annex 6 there are maps with the location of the subject\'s land and comparables.','Value of Improvements','In order to obtain the replacement cost new of the improvements costs manuals such as \"Bimsa Reports\" and ...... are used and are included in the following table. In addition, the entrepreneurial incentive and any type of obsolescence if applicable is included. Once the depreciated value of improvements is obtained the value of the land is added to arrive to an indication of the cost approach value.','Unit Cost Computations','Reproduction or Replacement Cost New','Depreciation computation','Depreciated value of constructions','Depreciation is computed using the Age minus Life Method which is given by the following formula:','Depreciation = (Effective Age / Total Economic Life)  x  Cost','According to the information provided by owner the effective age of the construction is \"0\" because constructions are recently concluded.','Entrepreneurial Incentive','<p style=\"text-align: justify;\">An economic reward sufficient to induce an entrepreneur to incur the risk associated with the project has to be included.</p>\n                                                      <p style=\"text-align: justify;\">According to information received from different investors plus own market information the following entrepreneurial incentive is defined:</p>','12','Indicated value by the cost approach (rounded):','External Obsolescence','<p style=\"text-align: justify;\">When a property produces income, the income loss caused by external obsolescence can be capitalized into an estimate of the loss in total property value. This procedure is applied in two steps. First, the market is analyzed to quantify the income loss. Next, the income loss is capitilized to obtain the value loss affecting the property as a whole.</p>',0,0,0,0,0,0,8075198.2709928);
/*!40000 ALTER TABLE `apprisal_APV` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apprisal_APVI`
--

DROP TABLE IF EXISTS `apprisal_APVI`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apprisal_APVI` (
  `id_in` int(11) NOT NULL,
  `AVITit1` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AVIText1` mediumtext COLLATE latin1_spanish_ci,
  `AVITit2` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AVIText2` mediumtext COLLATE latin1_spanish_ci,
  `AVITit3` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AVIText3` mediumtext COLLATE latin1_spanish_ci,
  `AVITit4` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AVITit5` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AVIText5_1` mediumtext COLLATE latin1_spanish_ci,
  `AVIText5_2` mediumtext COLLATE latin1_spanish_ci,
  `AVIText5_3` mediumtext COLLATE latin1_spanish_ci,
  `AVIText5_4` mediumtext COLLATE latin1_spanish_ci,
  `AVIText5_5` mediumtext COLLATE latin1_spanish_ci,
  `AVIText5_6` mediumtext COLLATE latin1_spanish_ci,
  `AVIText5_7` mediumtext COLLATE latin1_spanish_ci,
  `AVITit6` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AVIText6_1` mediumtext COLLATE latin1_spanish_ci,
  `AVIText6_2` mediumtext COLLATE latin1_spanish_ci,
  `AVITit7` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AVIText7` mediumtext COLLATE latin1_spanish_ci,
  `AVITit8` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AVIText8_1` mediumtext COLLATE latin1_spanish_ci,
  `AVIText8_2` mediumtext COLLATE latin1_spanish_ci,
  `AVIpgi_1_Txt` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AVIpgi_1_Mon_mxn` double DEFAULT NULL,
  `AVIpgi_1_Year_mxn` double DEFAULT NULL,
  `AVIpgi_2_Txt` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AVIpgi_2_Mon_mxn` double DEFAULT NULL,
  `AVIpgi_2_Year_mxn` double DEFAULT NULL,
  `AVIpgi_3_Txt` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AVIpgi_3_Mon_mxn` double DEFAULT NULL,
  `AVIpgi_3_Year_mxn` double DEFAULT NULL,
  `AVIpgi_4_Txt` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AVIpgi_4_Mon_mxn` double DEFAULT NULL,
  `AVIpgi_4_Year_mxn` double DEFAULT NULL,
  `AVIpgi_mvcl_por` double DEFAULT NULL,
  `AVIoe_1_Txt` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AVIoe_1_num1` double DEFAULT NULL,
  `AVIoe_1_num2` double DEFAULT NULL,
  `AVIoe_2_Txt` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AVIoe_2_num1` double DEFAULT NULL,
  `AVIoe_2_num2` double DEFAULT NULL,
  `AVIoe_3_Txt` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AVIoe_3_num1` double DEFAULT NULL,
  `AVIoe_3_num2` double DEFAULT NULL,
  `AVIoe_4_Txt` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AVIoe_4_num1` double DEFAULT NULL,
  `AVIoe_4_num2` double DEFAULT NULL,
  `AVIoe_5_Txt` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AVIoe_5_num1` double DEFAULT NULL,
  `AVIoe_5_num2` double DEFAULT NULL,
  `AVIoe_6_Txt` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AVIoe_6_num1` double DEFAULT NULL,
  `AVIoe_6_num2` double DEFAULT NULL,
  `AVIoe_7_Txt` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AVIoe_7_num1` double DEFAULT NULL,
  `AVIoe_7_num2` double DEFAULT NULL,
  `AVIoe_8_Txt` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AVIoe_8_num1` double DEFAULT NULL,
  `AVIoe_8_num2` double DEFAULT NULL,
  `AVIoe_9_Txt` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AVIoe_9_num1` double DEFAULT NULL,
  `AVIoe_9_num2` double DEFAULT NULL,
  `AVIoe_10_Txt` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AVIoe_10_num1` double DEFAULT NULL,
  `AVIoe_10_num2` double DEFAULT NULL,
  `AVIoe_11_Txt` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AVIoe_11_num1` double DEFAULT NULL,
  `AVIoe_11_num2` double DEFAULT NULL,
  `AVIoe_unf` double DEFAULT NULL,
  `AVIsource_1_Txt` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AVIsource_1_ran1` double DEFAULT NULL,
  `AVIsource_1_ran2` double DEFAULT NULL,
  `AVIsource_2_Txt` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AVIsource_2_ran1` double DEFAULT NULL,
  `AVIsource_2_ran2` double DEFAULT NULL,
  `AVIsource_3_Txt` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AVIsource_3_ran1` double DEFAULT NULL,
  `AVIsource_3_ran2` double DEFAULT NULL,
  `AVIsource_4_Txt` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AVIsource_4_ran1` double DEFAULT NULL,
  `AVIsource_4_ran2` double DEFAULT NULL,
  `AVImir` double DEFAULT NULL,
  `AVImt` double DEFAULT NULL,
  `AVIlvr` double DEFAULT NULL,
  `AVIedr` double DEFAULT NULL,
  `AVIicr` double DEFAULT NULL,
  `AVImorCo` double DEFAULT NULL,
  `AVIdcrTit` double DEFAULT NULL,
  `AVIcapR_por1` double DEFAULT NULL,
  `AVIcapR_por2` double DEFAULT NULL,
  `AVIcapR_por3` double DEFAULT NULL,
  `AVIcap_rate` double DEFAULT NULL,
  `AVIivudica` double DEFAULT NULL,
  `AVIfar` double DEFAULT NULL,
  `AVImf` double DEFAULT NULL,
  `AVIfArea` double DEFAULT NULL,
  `AVIivudicael` double DEFAULT NULL,
  `AVI_noi` double NOT NULL,
  `AVI_totExp` double NOT NULL,
  PRIMARY KEY (`id_in`),
  CONSTRAINT `FK_APVI_IN` FOREIGN KEY (`id_in`) REFERENCES `inmuebles` (`id_in`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apprisal_APVI`
--

LOCK TABLES `apprisal_APVI` WRITE;
/*!40000 ALTER TABLE `apprisal_APVI` DISABLE KEYS */;
INSERT INTO `apprisal_APVI` VALUES (70778,'Description','<p style=\"text-align: justify;\">Based on the Economic Principle of Anticipation, in the income capitalization approach a property\'s capacity to generate future benefits is analyzed and the income is capitalized into an indication of value. Income-producing properties are typically purchased as an investment and, from the point of view of the investor; the income-generating capacity of a property is the critical element that affects the value.</p>\n                                                       <p style=\"text-align: justify;\">A basic premise is one that says that holding risk constant, the greater the income, the greater the value. An investor who acquires an income-producing property, in essence is exchanging a present day investment for future revenue. The two most common methods of converting income into value are direct capitalization and discounted cash flow analysis.</p>','Direct Capitalization','This is a method used to convert an estimate of a single year\'s income expectancy into an indication of value in one direct step, either by dividing the net income estimate by an appropriate capitalization rate or by multiplying the income estimate by an appropriate factor. Direct capitalization employs capitalization rates and multipliers extracted or developed from market data. Only a single year\'s income is used. Yield and value changes are implied but not identified.','Potential Gross Income','In annex 3, the market rent of comparable properties is presented with its features, plus its adjustment grid, where the following numbers are obtained:','Income and Expenses','Capitalization Rate Calculation','Ovarall capitalization rates can be estimated with various techniques which depend on the quantity and quality of data available. The following provides different methods to derive to a reasonable number:','a) Publications and / or files data','Below are different sources that were consulted together with the recommended capitalization rates for similar properties and / or markets','b) Band of Investment','The Band of Investment Method is selected to obtain the Capitalization Rate which is a technique in which the capitalization rates attributable to components of a capital investment are combined to derive a weighted minus average rate attributable to the total investment.','c) Debt Coverage Ratio:','The capitalization rate will be given by the following expression:','Final Conclusion of Capitalization Rate','Considering a weighted average from previous result we have:','With the Net Operating Income and the Capitalization Rate the value under Direct Income Capitalization is obtained as follows: ','Excess Land','NONE','0','0','0','Maintenance',24658,295896,'Security',5952.033333333333,71424.4,'',0,0,'',0,0,5,'Property Tax',0,0,'Insurance',2,219298.48670864,'Administration',1.5,164473.86503148,'Replacement reserves',2.5,274123.10838580003,'Maintenance',1.2,131579.092025184,'Security',1,109649.24335432,'Broker\'s comision',1.5,164473.86503148,'',0,0,'',0,0,'',0,0,'',0,0,5,'Capright Mexico Investor\'s Survey  3rd Q',7.25,8.75,'RCA Analytics 2014-2016',7.2,9.24,'JLL files data January 2016',8.75,11,'',0,0,7.5,20,75,9,9.5003387419662,9.6671183226216,1.3,10,50,40,9.39017885013887,5738749.8598808,2,0.8,5000,6059793.1004648,9622275.0906887,10.195200878867),(85503,'Description','<p style=\"text-align: justify;\">Based on the Economic Principle of Anticipation, in the income capitalization approach a property\'s capacity to generate future benefits is analyzed and the income is capitalized into an indication of value. Income-producing properties are typically purchased as an investment and, from the point of view of the investor; the income-generating capacity of a property is the critical element that affects the value.</p>\n                                                               <p style=\"text-align: justify;\">A basic premise is one that says that holding risk constant, the greater the income, the greater the value. An investor who acquires an income-producing property, in essence is exchanging a present day investment for future revenue. The two most common methods of converting income into value are direct capitalization and discounted cash flow analysis.</p>','Direct Capitalization','This is a method used to convert an estimate of a single year\'s income expectancy into an indication of value in one direct step, either by dividing the net income estimate by an appropriate capitalization rate or by multiplying the income estimate by an appropriate factor. Direct capitalization employs capitalization rates and multipliers extracted or developed from market data. Only a single year\'s income is used. Yield and value changes are implied but not identified.','Potential Gross Income','In annex 3, the market rent of comparable properties is presented with its features, plus its adjustment grid, where the following numbers are obtained:','Income and Expenses','Capitalization Rate Calculation','Ovarall capitalization rates can be estimated with various techniques which depend on the quantity and quality of data available. The following provides different methods to derive to a reasonable number:','a) Publications and / or files data','Below are different sources that were consulted together with the recommended capitalization rates for similar properties and / or markets','b) Band of Investment','The Band of Investment Method is selected to obtain the Capitalization Rate which is a technique in which the capitalization rates attributable to components of a capital investment are combined to derive a weighted minus average rate attributable to the total investment.','c) Debt Coverage Ratio','The capitalization rate will be given by the following expression: ','Final Conclusion of Capitalization Rate','Considering a weighted average from previous result we have:','With the Net Operating Income and the Capitalization Rate the value under Direct Income Capitalization is obtained as follows: ','Excess Land','Considering the information provided, we observe that there is excess land in the land lot, analyzed in the following:','0','0','0','',0,0,'',0,0,'',0,0,'',0,0,0,'Property Tax',0,0,'Insurance',0,0,'Administration',0,0,'Replacement reserves',0,0,'Maintenance',0,0,'Security',0,0,'Broker\'s Commission',0,0,'',0,0,'',0,0,'',0,0,'',0,0,0,'',0,0,'',0,0,'',0,0,'',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0);
/*!40000 ALTER TABLE `apprisal_APVI` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apprisal_APVII`
--

DROP TABLE IF EXISTS `apprisal_APVII`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apprisal_APVII` (
  `id_in` int(11) NOT NULL,
  `AVIIText1` longtext COLLATE latin1_spanish_ci,
  `AVIIText2` longtext COLLATE latin1_spanish_ci,
  `AVIIuav` double DEFAULT NULL,
  `AVIIivusca` double DEFAULT NULL,
  `AVIIIText1` longtext COLLATE latin1_spanish_ci,
  `AVIIIText2` longtext COLLATE latin1_spanish_ci,
  `AIXText1` longtext COLLATE latin1_spanish_ci,
  `AXText1` longtext COLLATE latin1_spanish_ci,
  `AXText2` longtext COLLATE latin1_spanish_ci,
  `AXTit1` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AXTit2` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AXTit3` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AXTit4` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AXTit1Chkbx` tinyint(4) NOT NULL DEFAULT '0',
  `AXTit2Chkbx` tinyint(4) NOT NULL DEFAULT '0',
  `AXTit3Chkbx` tinyint(4) NOT NULL DEFAULT '0',
  `AXTit4Chkbx` tinyint(4) NOT NULL DEFAULT '0',
  `AXnum1` double NOT NULL DEFAULT '0',
  `AXnum2` double NOT NULL DEFAULT '0',
  `AXnum3` double NOT NULL DEFAULT '0',
  `AXpor1` double NOT NULL DEFAULT '0',
  `AXpor2` double NOT NULL DEFAULT '0',
  `AXpor3` double NOT NULL DEFAULT '0',
  `AXmarketVal` double NOT NULL DEFAULT '0',
  `AX_C_Approach` double NOT NULL DEFAULT '0',
  `AX_IC_Approach` double NOT NULL DEFAULT '0',
  `AX_DCF_Approach` double NOT NULL DEFAULT '0',
  `AX_SC_Approach` double NOT NULL DEFAULT '0',
  `AX_VU_Approach` double NOT NULL DEFAULT '0',
  `AXpor4` double NOT NULL DEFAULT '0',
  `AXnum5` double NOT NULL DEFAULT '0',
  `AXpor5` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_in`),
  CONSTRAINT `FK_APVII_IN` FOREIGN KEY (`id_in`) REFERENCES `inmuebles` (`id_in`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apprisal_APVII`
--

LOCK TABLES `apprisal_APVII` WRITE;
/*!40000 ALTER TABLE `apprisal_APVII` DISABLE KEYS */;
INSERT INTO `apprisal_APVII` VALUES (70778,'<p>TES</p>','<p>test II</p>',455.66,5617832.140000001,'<p>ASD</p>',NULL,'<p>ASD</p>','<p style=\"text-align: justify;\">From the above results, the following conclusions can be obtained: Both the income approach, as well as the Cost Approach applied are based on market research of similar properties for rent / sale found in industrial sites such as Industrial parks, brokers, etc., all located in nearby properties analyzed, with appropriate quantities and prices that are reasonably within market ranges. The cost approach reflects the value under fee simple conditions. It was obtained through adjusted land prices plus construction costs taken from cost manuals. The constructions areas are based on construction plans provided, so its calculations can be considered within a reasonable range of accuracy.</p>','<p style=\"text-align: justify;\">Also, the income capitalization approach is based on rents of income-producing properties found in the market. The type of property being valued could be considered as a typical income producing property, however, it could also be purchased for owner occupation. Considering the two types of values obtained (Value in Exchange and Value in Use), the market value of the property will be governed by the following blended value:</p>','Cost Approach','Income Capitalization Approach','Discounted Cash Flow Approach','Sales Comparison Approach',1,1,1,1,5508913.9328816,6059793.1004648,100000,45,40,5,5469711.7239826,2479011.2697967,2423917.2401859,5000,561783.214,3591788.8837007,50,5617832.140000001,10),(85503,'','',0,0,'<p style=\"text-align: justify;\">It will be governed by the value of the Cost Approach without considering any type of functional / external obsolescence.</p>',NULL,'<p class=\"MsoNormal\"><span style=\"font-family: \'Arial\',sans-serif;\">The values obtained by the application of the different approaches are as follows:</span></p>','<p style=\"text-align: justify;\">From the above results, the following conclusions can be obtained: Both the income approach, as well as the Cost Approach applied are based on market research of similar properties for rent / sale found in industrial sites such as Industrial parks, brokers, etc., all located in nearby properties analyzed, with appropriate quantities and prices that are reasonably within market ranges. The cost approach reflects the value under fee simple conditions. It was obtained through adjusted land prices plus construction costs taken from cost manuals. The constructions areas are based on construction plans provided, so its calculations can be considered within a reasonable range of accuracy.</p>','<p style=\"text-align: justify;\">Also, the income capitalization approach is based on rents of income-producing properties found in the market.</p>\n<p style=\"text-align: justify;\">The type of property being valued could be considered as a typical income producing property, however, it could also be purchased for owner occupation. Considering the two types of values obtained (Value in Exchange and Value in Use), the market value of the property will be governed by the following blended value:</p>','Cost Approach','Income Capitalization Approach','Discounted Cash Flow Approach','Sales Comparison Approach',1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
/*!40000 ALTER TABLE `apprisal_APVII` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apprisal_APVIII`
--

DROP TABLE IF EXISTS `apprisal_APVIII`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apprisal_APVIII` (
  `id_in` int(11) NOT NULL,
  `AXIText1` longtext COLLATE latin1_spanish_ci NOT NULL,
  `AXIText2` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AXIText3` mediumtext COLLATE latin1_spanish_ci,
  `AXIText4` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AXIText5` varchar(80) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AXIText6` varchar(80) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AXIText7` varchar(80) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AXIText8` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AXIText9` longtext COLLATE latin1_spanish_ci,
  `AXIText10` varchar(80) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AXIText11` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AXIText12` varchar(80) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AXIText13` varchar(80) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AXIText14` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AXIText15` mediumtext COLLATE latin1_spanish_ci,
  `AXIText16` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AXIText17` longtext COLLATE latin1_spanish_ci,
  `AXIRate1` double NOT NULL DEFAULT '0',
  `AXIRate2` double NOT NULL DEFAULT '0',
  `AXIRate3` double NOT NULL DEFAULT '0',
  `AXIRate4` double NOT NULL DEFAULT '0',
  `AXIRate5` double NOT NULL DEFAULT '0',
  `AXIRate6` double NOT NULL DEFAULT '0',
  `AXIRate7` double NOT NULL DEFAULT '0',
  `AXIRate8` double NOT NULL DEFAULT '0',
  `AXIRate9` double NOT NULL DEFAULT '0',
  `AXISaleTerm` int(11) NOT NULL DEFAULT '0',
  `AXIText18` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AXIText19` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AXIText20` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AXIText21` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AXIText22` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `AXIFecha` date DEFAULT NULL,
  `AXIRateAP` double NOT NULL DEFAULT '0',
  `AXIliquidVal` double NOT NULL DEFAULT '0',
  `AXIIText1` mediumtext COLLATE latin1_spanish_ci,
  PRIMARY KEY (`id_in`),
  CONSTRAINT `FK_APVIII_IN` FOREIGN KEY (`id_in`) REFERENCES `inmuebles` (`id_in`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apprisal_APVIII`
--

LOCK TABLES `apprisal_APVIII` WRITE;
/*!40000 ALTER TABLE `apprisal_APVIII` DISABLE KEYS */;
INSERT INTO `apprisal_APVIII` VALUES (70778,'<p style=\"text-align: justify;\">Liquidation Value considers the sale of the property in a limited or severely limited exposure time on the market and thus atypical motivation of the seller, which is found in compulsion.</p>\n                                                        <p style=\"text-align: justify;\">Liquidation Value is obtained using the methodology that establishes a direct relationship between the market value of the property under typical conditions and the value of the property under said limited exposure time. This is accomplished by applying a discount factor that considers time value of money and the costs generated during the exposure time.</p> \n                                                        <p style=\"text-align: justify;\">In the present case, considering the opinion of the market value of the property and an exposure time of 180 days, the liquidation value is obtained as follows: </p>','1) Base value:','Considering the opinion of the Market Value of the property:','2) Discount rate:','Risk-free interest rate (RFIR):','U.S. Treasury Note (20 years):','Source:  ','Country risk:','Risk by idiosyncrasy components of the country, such as political disturbance, possible expropriation of private assets, barriers to free flow of capital, devaluations, hyperinflations, etc.','Source: ','Market risk premium:','Additional returns when investing in bonds or assets.','Source:','Liquidity:','Difficulty in converting a cash investment in a reasonable price to a market value in a reasonable time:','Additional risk','The additional risk percentage is established for the purpose of coming to a liquidation value that represents a discount percentage of between 15 and 25% above market value, the previous derived from the property inspection and discussions with real estate brokers and market participants, as to which discount can be applied to similar properties for purposes of achieving the sale within six months.',2.58,1.82,1.4,5,25,3,6,5,2.65,6,'3) Costs:','Managing:','Sale:','Property Tax:','4) Sale Term:','2016-08-03',0,4032270.01187401,'');
/*!40000 ALTER TABLE `apprisal_APVIII` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalogo`
--

DROP TABLE IF EXISTS `catalogo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalogo` (
  `id_opcion` int(11) NOT NULL AUTO_INCREMENT,
  `campo` varchar(15) COLLATE latin1_spanish_ci DEFAULT NULL COMMENT 'Campo identificador del catalogo',
  `opcion_catalogo` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `valor_catalogo` varchar(30) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id_opcion`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalogo`
--

LOCK TABLES `catalogo` WRITE;
/*!40000 ALTER TABLE `catalogo` DISABLE KEYS */;
INSERT INTO `catalogo` VALUES (1,'profile','Administrador',NULL),(2,'profile','Valuador',NULL),(3,'rc1','Fee simple',NULL),(4,'rc1','Lease fee',NULL),(5,'rc1','Other',NULL),(6,'rc2','Yes',NULL),(7,'rc2','None',NULL),(8,'rc3','Arms length',NULL),(9,'rc3','None',NULL),(10,'rc4','Yes',NULL),(11,'rc4','None',NULL),(13,'rcVS','Very Superior',NULL),(14,'rcVS','Superior',NULL),(15,'rcVS','Similar',NULL),(16,'rcVS','Inferior',NULL),(17,'rcVS','Very Inferior',NULL),(23,'rc8CLS','Listing',NULL),(24,'rc8CLS','Actual Sale',NULL),(25,'rc8CRL','Listing',NULL),(26,'rc8CRL','Actual Rent',NULL),(27,'rc8CRS','Listing',NULL),(28,'rc8CRS','Actual Rent',NULL),(29,'AAIMG','A.- X LOCATION MAP OF SUBJECT AND COMPARABLES',NULL),(30,'AAIMG','A.- X PLANS',NULL),(31,'AAIMG','A.- X DEEDS',NULL),(32,'AAIMG','A.- X LAND USE PERMIT',NULL),(33,'AAIMG','A.- X PROPERTY TAX BILL',NULL),(34,'AAIMG','ADDITIONAL DOCUMENTS',NULL),(35,'origen','CDMX',NULL),(36,'origen','MTY',NULL),(39,'concepto','Terreno','TER'),(40,'concepto','Habitacional U de V','HUV'),(41,'concepto','Habitacional ( otros)','HAB'),(42,'concepto','Industrial','IND'),(43,'concepto','Oficinas ( no edificio)','OFI'),(44,'concepto','Oficinas ( edificio)','EOF'),(45,'concepto','Comercial ( locales)','COM'),(46,'concepto','Centro Comercial ','CC'),(47,'concepto','Hotel','TUR'),(48,'concepto','Estudio Mayor y Mejor Uso','MYMU'),(49,'concepto','Estudio de Mercado','MER'),(50,'concepto','Uso especializado','ESP');
/*!40000 ALTER TABLE `catalogo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalogo_definiciones`
--

DROP TABLE IF EXISTS `catalogo_definiciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalogo_definiciones` (
  `id_definicion` int(11) NOT NULL AUTO_INCREMENT,
  `concepto` varchar(80) COLLATE latin1_spanish_ci DEFAULT NULL,
  `descripcion` longtext COLLATE latin1_spanish_ci,
  `id_type` int(11) NOT NULL,
  PRIMARY KEY (`id_definicion`),
  KEY `id_type` (`id_type`),
  CONSTRAINT `fk_type` FOREIGN KEY (`id_type`) REFERENCES `type_in` (`id_type`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalogo_definiciones`
--

LOCK TABLES `catalogo_definiciones` WRITE;
/*!40000 ALTER TABLE `catalogo_definiciones` DISABLE KEYS */;
INSERT INTO `catalogo_definiciones` VALUES (1,'As is market value:','The estimate of the market value of real property in its current physical condition, use, and zoning as of the appraisal date.',1),(2,'Cost:','The total dollar expenditure to develop an improvement (structure); applies to either reproduction of an identical improvement or replacement with a functional equivalent, not exchange (price).',1),(3,'Cost Approach:','A set of procedures through which a value indication is derived for the fee simple interest in a property by estimating the current cost to construct a reproduction of (or replacement for) the existing structure, including an entrepreneurial incentive, deducting depreciation from the total cost, and adding the estimated land value. Adjustments may then be made to the indicated fee simple value of the subject property to reflect the value of the property interest being appraised.',1),(4,'Effective Date: ','The date on which the analyses, opinions, and advice in an appraisal, review, or consulting service apply.',1),(5,'Exposure Time:','The estimated length of time the property interest being appraised would have been offered on the market prior to the hypothetical consummation of a sale at market value on the effective date of the appraisal.',1),(6,'Effective Date of the Appraisal:','The date at which the analyses, and advice in an appraisal, review, or consulting service apply (USPAP 2002, 2nd Edition).',1),(7,'Effective age:','The age of property that is based on the amount of observed deterioration and obsolescence it has sustained, which may be different from its chronological age.',1),(8,'Excess Land:','Land that is not needed to serve or support the existing improvement. The highest and best use of the excess land may or may not be the same as the highest and best use of the improved parcel. Excess land may have the potential to be sold separately and is valued separate.',1),(9,'External Obsolescence:','An element of depreciation; a diminution in value caused by negative externalities and generally incurable on the part of the owner, landlord, or tenant.',1),(10,'Extraordinary Assumption:','An assumption directly related to a specific assignment, which, if found to be false, could alter the appraiser\'s opinion or conclusion. Extraordinary assumptions presume as fact otherwise uncertain information about physical, legal, or economic characteristics of the subject property or about conditions external to the property such as market conditions or trends,; or about the integrity of data used in an analysis (Uniform Standards of Professional Appraisal Practice).',1),(11,'Fee Simple Estate:','Absolute ownership unencumbered by any other interest or estate, subject only to the limitations imposed by the governmental powers of taxation, eminent domain, police power, and escheat.',1),(12,'Functional Obsolescence:','The impairment of functional capacity of a property according to market tastes and standards.',1),(13,'Highest and Best use of land or a site as though vacant:','Among all reasonable, alternative uses, the use that yields the highest present land value, after payments are made for labor, capital, and coordination. The use of a property based on the assumption that the parcel of land is vacant or can be made vacant by demolishing any improvements.',1),(14,'Highest and best use:','The reasonably probable and legal use of vacant land or an improved property that is physically possible, appropriately supported, financially feasible, and that results in the highest value. The four criteria the highest and best use must meet are legal permissibility, physical possibility, financial feasibility, and maximum productivity.\nAlternatively, the probable use of land or improved property—specific with respect to the user and timing of the use—that is adequately supported and results in the highest present value.Highest and best use of property as improved.\nThe use that should be made of a property as it exists. An existing improvement should be renovated or retained as is so long as it continues to contribute to the total market value of the property, or until the return from a new improvement would more than offset the cost of demolishing the existing building and constructing a new one.',1),(15,'Hypothetical Condition: ','That what is contrary to what exists but that it is supposed for the purpose of the analysis. Hypothetical conditions assume conditions contrary to known facts about physical, legal or economic characteristics of the subject property; or about conditions external to the property such as market conditions or trends,; or about the integrity of data used in an analysis.',1),(16,'Income Capitalization Approach:','A set of procedures through which an appraiser derives a value indication for an income-producing property by converting its anticipated benefits (cash flows and reversion) into property value. This conversion can be accomplished in two ways. One year’s income expectancy can be capitalized at a market-derived capitalization rate or at a capitalization rate that reflects a specified income pattern, return on investment, and change in the value of the investment. Alternatively, the annual cash flows for the holding period and the reversion can be discounted at a specified yield rate.',1),(17,'Investment value:','The value of a property interest to a particular investor or class of investors based on the investor’s specific requirements. Investment value may be different from market value because it depends on a set of investment criteria that are not necessarily typical of the market.',1),(18,'Leased fee interest:','An ownership interest held by a landlord with the rights of use and occupancy conveyed by lease to others. The rights of the lessor (the leased fee owner) and the lessee are specified by contract terms contained within the lease.',1),(19,'Liquidation Value:','The most probable price that a specified interest in real property is likely to bring under all of the following conditions:\n<ol><li>Consummation of a sale will occur within a severely limited future marketing period specified by the client</li>\n<li>The actual market conditions currently prevailing are those to which the appraised property interest is subject</li>\n<li>The buyer is acting prudently and knowledgeably</li>\n<li>The seller is under extreme compulsion to sell</li>\n<li>The buyer is typically motivated</li>\n<li>The buyer is acting in what he or she considers his or her best interest</li>\n<li>A limited marketing effort and time will be allowed for the completion of a sale</li>\n<li>Payment will be made in cash in US dollars or in terms of financial arrangements comparable thereto</li>\n<li>The price represents the normal consideration for the property sold, unaffected by special or creative financing or sales concessions granted by anyone associated with the sale </li></ol>\n(The Appraisal of Real Estate; Appraisal Institute; Twelfth Edition)',1),(20,'Market Rent:','The most probable rent that a property should bring in a competitive and open market reflecting all conditions and restrictions of the lease agreement, including permitted uses, use restrictions, expense obligations, term, concessions, renewal and purchase options, and tenant improvements (TIs).',1),(21,'Market Value:','The most probable price that a property should bring in a competitive and open market under all conditions requisite to a fair sale, the buyer and seller each acting prudently and knowledgeably, and assuming the price is not affected by undue stimulus. Implicit in this definition is the consummation of a sale as of a specified date and the passing of title from seller to buyer under conditions whereby:\n<ul> <li>Buyer and seller are typically motivated;</li>\n<li>Both parties are well informed or well advised, and acting in what they consider their best interests;</li>\n<li>A reasonable time is allowed for exposure in the open market;</li>\n<li>Payment is made in terms of cash in U.S. dollars or in terms of financial arrangements comparable thereto; and</li>\n<li>The price represents the normal consideration for the property sold unaffected by special or creative financing or sales concessions granted by anyone associated with the sale.</li></ul> \n(12 C.F.R. Part 34.42(g); 55 Federal Register 34696, August 24, 1990, as amended at 57 Federal Register 12202, April 9, 1992; 59 Federal Register 29499, June 7, 1994)',1),(22,'Replacement Cost:','The estimated cost to construct, at current prices as of the effective appraisal date, a substitute for the building being appraised, using modern materials and current standards, design, and layout.',1),(23,'Report Date:','The date of the valuation report.',1),(24,'Sales Comparison Approach:','The process of deriving a value indication for the subject property by comparing market information for similar properties with the property being appraised, identifying appropriate units of comparison, and making qualitative comparisons with or quantitative adjustments to the sale prices (or unit prices, as appropriate) of the comparable properties based on relevant, market-derived elements of comparison.',1),(25,'Scope of work:','The amount and type of information researched and the analysis applied in an assignment. Scope of work includes, but is not limited to, the following:\n<ul>\n<li>The degree to which the property is inspected or identified;</li>\n<li>The extent of research into physical or economic factors that could affect the property;</li>\n<li>The extent of data research; and</li>\n<li>The type and extent of analysis applied to arrive at opinions or conclusions.</li></ul>',1),(26,'Scope of work (description):','A statement describing the extent of the process in which data is collected, confirmed, and reported.',1),(27,'Value in Exchange: ','Attribution of value to goods or services based on what can be obtained for them in exchange for other goods and services. As reflected in Financial Accounting Standards Statement No. 157, financial assets may be valued “in use” or “in exchange” under specified conditions.',1),(28,'Value in use:','The value of a property assuming a specific use, which may or may not be the property’s highest and best use on the effective date of the appraisal. Value in use may or may not be equal to market value but is different conceptually.',1);
/*!40000 ALTER TABLE `catalogo_definiciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ci_sessions`
--

DROP TABLE IF EXISTS `ci_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) NOT NULL,
  `ip_address` varchar(45) DEFAULT NULL,
  `user_agent` varchar(120) DEFAULT NULL,
  `last_activity` int(10) NOT NULL,
  `user_data` text,
  PRIMARY KEY (`session_id`,`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ci_sessions`
--

LOCK TABLES `ci_sessions` WRITE;
/*!40000 ALTER TABLE `ci_sessions` DISABLE KEYS */;
INSERT INTO `ci_sessions` VALUES ('1a7f1a8a4d5ed4cf012e3fefcd776ece','108.171.132.165','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',1475272485,'a:2:{s:9:\"user_data\";s:0:\"\";s:12:\"datos_sesion\";a:1:{i:0;a:5:{s:6:\"nombre\";s:5:\"Oscar\";s:9:\"apellidos\";s:16:\"Alonso Martínez\";s:4:\"tipo\";s:1:\"2\";s:6:\"correo\";s:23:\"oscar.alonso@am.jll.com\";s:4:\"foto\";s:13:\"tll_30378.png\";}}}'),('26ecedbeaac39fb10c56f26958e033e8','189.217.231.241','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.82 Safari/537.36',1475288205,''),('31b0fc8466155925528e22408408c96a','108.171.132.165','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',1475280322,'a:2:{s:9:\"user_data\";s:0:\"\";s:12:\"datos_sesion\";a:1:{i:0;a:5:{s:6:\"nombre\";s:5:\"Oscar\";s:9:\"apellidos\";s:16:\"Alonso Martínez\";s:4:\"tipo\";s:1:\"2\";s:6:\"correo\";s:23:\"oscar.alonso@am.jll.com\";s:4:\"foto\";s:13:\"tll_30378.png\";}}}'),('4d0bef0396ba39affae54c0ed9c6dde6','108.171.132.165','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',1475272425,''),('561fb5da5a2a90364987fa5f764c65ef','189.217.231.241','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.82 Safari/537.36',1475288226,'a:2:{s:9:\"user_data\";s:0:\"\";s:12:\"datos_sesion\";a:1:{i:0;a:5:{s:6:\"nombre\";s:3:\"Edu\";s:9:\"apellidos\";s:6:\"Mussil\";s:4:\"tipo\";s:1:\"1\";s:6:\"correo\";s:23:\"eduardo.ms@senni.com.mx\";s:4:\"foto\";s:13:\"tll_32810.jpg\";}}}'),('6e4fbabbf9e7bfe60cddd22841e3cb1e','108.171.132.165','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',1475273808,''),('86aa85c4457d1dbfb0b44b847f415da1','80.82.70.24','Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.28) Gecko/20120306 Firefox/3.6.28 (.NET CLR 3.5.30729)',1475267945,''),('a1caff962977b888dcdb4e0523570fea','108.171.132.165','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',1475264341,'a:2:{s:9:\"user_data\";s:0:\"\";s:12:\"datos_sesion\";a:1:{i:0;a:5:{s:6:\"nombre\";s:5:\"Oscar\";s:9:\"apellidos\";s:16:\"Alonso Martínez\";s:4:\"tipo\";s:1:\"2\";s:6:\"correo\";s:23:\"oscar.alonso@am.jll.com\";s:4:\"foto\";s:13:\"tll_30378.png\";}}}'),('ba7590bf606e1042ab4135cd266c9a78','108.171.132.165','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',1475261659,'');
/*!40000 ALTER TABLE `ci_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comparables`
--

DROP TABLE IF EXISTS `comparables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comparables` (
  `id_comp` int(11) NOT NULL,
  `foto` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `type_property` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `calle` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `num` varchar(20) COLLATE latin1_spanish_ci DEFAULT NULL,
  `col` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `mun` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `edo` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `cp` varchar(20) COLLATE latin1_spanish_ci DEFAULT NULL,
  `latitud` double DEFAULT NULL,
  `longitud` double DEFAULT NULL,
  `source_information` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `phone` varchar(30) COLLATE latin1_spanish_ci DEFAULT NULL,
  `land_m2` double(11,1) DEFAULT '0.0',
  `land_ft2` double(11,1) DEFAULT '0.0',
  `construction` double(11,1) DEFAULT '0.0',
  `price_mx` double DEFAULT '0',
  `price_usd` double DEFAULT '0',
  `unit_value_mx` double NOT NULL DEFAULT '0',
  `unit_value_usd` double NOT NULL DEFAULT '0',
  `time_market` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `comments` longtext COLLATE latin1_spanish_ci,
  `rc8` int(11) NOT NULL DEFAULT '0',
  `closing_listing_date` date DEFAULT NULL,
  `lada` varchar(3) COLLATE latin1_spanish_ci DEFAULT NULL,
  `exchange_rate` double DEFAULT NULL,
  `date_exchange_rate` datetime NOT NULL,
  `fecha_alta` datetime DEFAULT NULL,
  `fecha_modificacion` datetime NOT NULL,
  `tipo` varchar(3) COLLATE latin1_spanish_ci DEFAULT NULL,
  `creado_por` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`id_comp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comparables`
--

LOCK TABLES `comparables` WRITE;
/*!40000 ALTER TABLE `comparables` DISABLE KEYS */;
INSERT INTO `comparables` VALUES (45,'45/comp_20072.JPG','Terreno','Carretera Río Verde','S/N','Rancho La Libertad','Cerro de San Pedro','San Luis Potosí','78440',0,0,'Ricardo Ramírez','(01) 44-48-17-73-23',28217.0,303725.3,0.0,33861288,NULL,1200.03,0,'17-03-2016',NULL,0,NULL,NULL,17.8561,'2016-07-28 18:08:02','2016-07-21 12:59:48','2016-07-28 18:08:02','CLS','octavio.gonzalez@am.jll.com'),(143,'143/comp_172.JPG','Terreno','Av. Tamaulipas','48 A','Santa Fe','Álvaro Obregón','Ciudad de México','01376',0,0,'Ana Karen Ramírez Aranda','(01) 55-55-88-95-63',11628.0,125162.8,0.0,155117520,NULL,13340,0,'15- 02-2016',NULL,0,NULL,NULL,17.8561,'2016-08-03 15:16:52','2016-07-22 11:29:37','2016-08-03 15:16:52','CLS','octavio.gonzalez@am.jll.com'),(198,'198/comp_30326.JPG','Local Comercial','Av. De la Industria','S/N','Moctezuma 2da Sección','Venustiano Carranza','Ciudad de México','15530',0,0,'Coldwell Banker Acro','(01) 55-52-77-83-66',0.0,0.0,233.0,46750,NULL,200.64,0,'24-05-2016',NULL,0,NULL,NULL,18.3826,'2016-08-04 13:14:28','2016-08-04 13:14:28','0000-00-00 00:00:00','CRL','octavio.gonzalez@am.jll.com'),(314,'314/comp_20395.JPG','Departamento','Cda de Guillermo Prieto','36','Jesus del Monte','Huixquilucan','Estado de México','52764',0,0,'Ventas  Vistainterlomas','55-26-50-62-94',0.0,0.0,65.0,1500000,0,23076.92,0,'10-03-2016',' Departamento con dos recámaras, dos baños y un cajón de estacionamiento, gimnasio, salón de fiestas.',1,NULL,'01',19.6,'2016-09-05 20:13:16','2016-09-05 20:13:16','0000-00-00 00:00:00','CRS','octavio.gonzalez@am.jll.com'),(339,'339/comp_80424.JPG','Casa','Emiliano Zapata','S/N','Revolución','Cuautla','De Morelos','62748',0,0,'Abigail Contreras','(01) 73-51-77-77-55',397.0,4273.3,125.0,1380000,NULL,11040,0,'07-03-2016',NULL,0,NULL,NULL,17.7723,'2016-08-02 19:32:31','2016-08-02 19:32:31','0000-00-00 00:00:00','CRS','octavio.gonzalez@am.jll.com'),(361,'361/comp_70374.JPG','Departamento','Emerson','S/N','Polanco','Miguel Hidalgo','Ciudad de México','11550',0,0,'Miroslava Castrejón','(01) 55-55-20-40-00',0.0,0.0,180.0,30000,NULL,166.67,0,'14-01-2016',NULL,0,NULL,NULL,17.8561,'2016-08-03 15:28:43','2016-07-26 18:03:21','2016-08-03 15:28:43','CRL','octavio.gonzalez@am.jll.com'),(400,'400/comp_19355.JPG','Nave Industrial','Ignacio Fanduas','S/N','Abastos','San Luis ','San Luis Potosí','78390',0,0,'Abelardo Huerta','(45) 44-42-92-37-78',1000.0,10763.9,700.0,20000,NULL,28.57,0,'17-03-2016',NULL,0,NULL,NULL,17.8561,'2016-07-21 12:42:41','2016-07-18 12:52:27','2016-07-21 12:42:41','CRL','octavio.gonzalez@am.jll.com'),(428,'428/comp_451.JPG','Casa','Circuito Univercidades','S/N','Zibatá','El Marqués','Querétaro','76269',0,0,'Martha Patricia Aviles','44-23-71-34-74',0.0,0.0,133.0,1770000,0,13308.27,0,'16-03-2016',' Casas de 2 plantas, sala comedor abiertos, cocina integral equipada, cuarto de lavado, 2 lugares de estacionamiento. Planta alta con 3 recamaras, principal con vestidor y baño completo, 2 recamaras secundarias, baño completo compartido. Todos los modelos opción de hall de tv.',1,NULL,'01',19.8925,'2016-09-19 12:00:13','2016-09-19 12:00:13','0000-00-00 00:00:00','CRS','octavio.gonzalez@am.jll.com'),(470,'470/comp_20511.JPG','Departamento','Ejercito Nacional','225','Granada','Miguel Hidalgo','Ciudad de México','11520',0,0,'Elizabeth Alvarado (Hares Select)','(01) 55-52-03-64-03',0.0,0.0,82.0,4769000,NULL,58158.54,0,'12-04-2016',' El condominio contará con 190 departamentos, El departamento cuenta con sala, comedor, cocina, 2 recámaras, 2 baños y 2 estacionamientos, el condominio cuenta con alberca semiolímpica techada, 2 pisos de gimnasio, salón de yoga y spinning, spa, vapor y cancha de usos múltiples, terrazas, áreas verdes, ludoteca, área especial con juegos infantiles, snack bar, centro de negocios y un lobby doble altura.',1,NULL,'',17.8561,'2016-09-12 18:40:43','2016-07-28 11:46:59','2016-09-12 18:40:43','CRS','octavio.gonzalez@am.jll.com'),(515,'515/comp_60570.JPG','Departamento','Homero','S/N','Polanco','Miguel Hidalgo','Ciudad de México','11550',0,0,'Gabriel Hernández','(01) 55-63-02-94-25',0.0,0.0,210.0,30000,NULL,142.86,0,'14-01-2016',NULL,0,NULL,NULL,17.8561,'2016-08-03 15:27:41','2016-07-26 15:25:03','2016-08-03 15:27:41','CRL','octavio.gonzalez@am.jll.com'),(608,'608/comp_30630.JPG','Local en Plaza Comercial','Av.Jaime Balmes','S/N','Polanco','Miguel Hidalgo','Ciudad de México','11510',0,0,'Coldwell Banker Invest (Bienes Raices)','(01) 55-52-54-20-11',0.0,0.0,68.0,29000,NULL,426.47,0,'01-14-2016',NULL,0,NULL,NULL,17.8561,'2016-08-03 15:22:45','2016-07-20 12:43:54','2016-08-03 15:22:45','CRL','octavio.gonzalez@am.jll.com'),(689,'689/comp_60733.JPG','Casa','Av. Sin Nombre','S/N','Rancho  de La Mora','Tonanitla','Estado de México','54930',0,0,'Guillermo Arena','91-15-69-74-',90.0,968.8,117.2,1099000,0,9377.13,0,'23-05-2016',' Casa habitación desarollada en dos niveles, cuenta con sala, comedor, cocina, 1/2, baño, jardín, área de lavado, 2 recámaras, recámara principal con baño completo y dos cajones de estacionamiento.',1,NULL,'',18.4444,'2016-09-22 13:03:49','2016-09-22 13:03:49','0000-00-00 00:00:00','CRS','octavio.gonzalez@am.jll.com'),(701,'701/comp_60756.JPG','Local Comercial','Av. Insurgentes Sur ','1391','San josé Insurgentes','Benito Juárez','Ciudad de México','3900',0,0,'Ipiranga Bienes Raíces','55-56-35-28-43',0.0,0.0,100.0,37000,0,370,0,'',' Centro Armand renta local de 100m2 para uso de oficina o comercial. Local ubicado en la planta baja del centro comercial con todos los servicios',1,NULL,'01',15.6918,'2016-08-09 12:46:00','2016-08-09 12:46:00','0000-00-00 00:00:00','CRL','octavio.gonzalez@am.jll.com'),(709,'709/comp_50766.JPG','Departamento','Lago Taulebe','15','Pensil Norte','Miguel Hidalgo','Ciudad de México','11430',0,0,'Karla Serratos','55-43-43-04-59',0.0,0.0,51.0,1207850,0,23683.33,0,'15- 02-2016','Edificio de cinco niveles, departamento ubicado en primer nivel, cuenta con dos recámaras, 1.5 baños, sala, comedor, cocina, área de lavado y 1 cajón de estacionamiento.',1,NULL,'044',18.3748,'2016-09-01 13:34:22','2016-09-01 13:34:22','0000-00-00 00:00:00','CRS','octavio.gonzalez@am.jll.com'),(734,'734/comp_60813.JPG','Terreno','Oriente 3 esq. Sur 4','S/N','Ciudad Industrial','Tizayuca','Hidalgo','43800',0,0,'Sr. Jaime Valdez','55-61-35-29-45',5000.0,53819.6,0.0,15000000,0,3000,0,'24-36 meses',' Regular-shaped lot.',1,NULL,'044',17.1767,'2016-08-17 15:14:04','2016-08-17 15:14:04','0000-00-00 00:00:00','CLS','octavio.gonzalez@am.jll.com'),(738,'738/comp_90802.JPG','Terreno','Blvd. Bosque Real','S/N','Bosque Real','Huixquilucan','Estado de México','52774',0,0,'Corporativo Inmobiliario','55-51-09-08-26',25216.0,271422.8,0.0,131040000,0,5196.7,0,'10-03-2016','  Macrolote con 168 indivisos, 101 m de frente hasta 20 niveles',1,NULL,'01',19.6,'2016-09-05 12:01:50','2016-09-05 12:01:50','0000-00-00 00:00:00','CLS','octavio.gonzalez@am.jll.com'),(809,'809/comp_81472.JPG','Casa','Franc Minaisco Javier','10','Miguel Hidalgo','Cuautla','De Morelos','62748',0,0,'Camacho Bienes Raices','(01) 73-53-98-17-52',250.0,2691.0,225.0,2088000,NULL,9280,0,'07-03-2016',NULL,0,NULL,NULL,17.7723,'2016-08-02 19:48:45','2016-08-02 19:48:45','0000-00-00 00:00:00','CRS','octavio.gonzalez@am.jll.com'),(833,'833/comp_21200.JPG','Departamento','Lago Neuchatel','45','Granada','Miguel Hidalgo','Ciudad de México','11529',0,0,'Gabriela Figueroa','(01) 55-58-15-77-93',0.0,0.0,109.0,5850000,NULL,53669.72,0,'12-04-2016',' El condominio contará con 190 departamentos, se encuentra en etapa de preventa. El departamento cuenta con sala, comedor, cocina, 3 recámaras, 2.5 baños y 2 estacionamientos, el condominio cuenta con dos albercas, gimnasio, salón de spinning, spa, vapor y cancha de usos múltiples, roof garden con asadores, áreas verdes con juegos infantiles, ludoteca, bissnes center, minisuper, lavandería, cine, salón de adultos, salón de fiestas.',1,NULL,'',17.8561,'2016-09-12 18:22:53','2016-07-28 11:59:45','2016-09-12 18:22:53','CRS','octavio.gonzalez@am.jll.com'),(842,'842/comp_30884.JPG','Casa','Alpes','1230','Lomas de Chapultepec','Miguel Hidalgo','Ciudad de México','11000',0,0,'Elena Miranda','(01) 55-22-23-11-81',900.0,9687.5,1015.0,59962980,NULL,59076.83,0,'10-02-2016',NULL,0,NULL,NULL,17.7866,'2016-08-19 14:45:54','2016-07-27 10:59:35','2016-08-03 17:45:20','CRS','octavio.gonzalez@am.jll.com'),(927,'927/comp_19393.JPG','Nave Industrial','Periférico','S/N','Valle Dorado','San Luis','San Luis Potosí','78399',0,0,'Benny Reyes','(45) 44-48-45-82-01',2000.0,21527.8,1500.0,39000,NULL,26,0,'17-03-2016',NULL,0,NULL,NULL,17.8561,'2016-07-21 12:43:20','2016-07-18 13:02:10','2016-07-21 12:43:20','CRL','octavio.gonzalez@am.jll.com'),(933,'933/comp_81007.JPG','Terreno','Vicente Guerrero','S/N','San Martín','Tepotzotlán','Estado de México','54600',0,0,'Coldwell Banker','55-55-63-16-30',73904.0,795496.0,73904.0,48000000.21,0,649.49,0,'1-3 meses',' Regular Shape, residential and comercial use, in front of deportivo Israelita.',1,NULL,'01',17.8561,'2016-08-24 19:40:48','2016-08-24 19:40:48','0000-00-00 00:00:00','CLS','octavio.gonzalez@am.jll.com'),(999,'999/comp_11025.JPG','Departamento','Via Magna','6','Interlomas','Huixquilucan','Estado de México','52787',0,0,'Sra. Martha Bandera','55-52-90-09-99',0.0,0.0,96.0,4190000,0,43645.83,0,'10-03-2016',' Dos recámaras, 2 baños 2 cajones de estacionamiento. Salón de eventos, alberca, gym, areas verdes, sala de cine, etc.',1,NULL,'01',19.6,'2016-09-05 17:42:58','2016-09-05 17:42:58','0000-00-00 00:00:00','CRS','octavio.gonzalez@am.jll.com'),(1005,'1005/comp_81243.JPG','Nave Industrial','Norte 45','812','Industrial Vallejo','Azcapotzalco','Ciudad de México','2300',0,0,'Susana Ureña Mariscal Real Estate','55-70-18-98',7900.0,85034.9,7900.0,124506200,0,15760.28,0,'06-05-2016',': Industrial Bay whit 28,693 m2, included offices, manuever yard,  average height 10 to 13 m.',1,NULL,'01',17.7866,'2016-08-22 12:50:26','2016-08-22 12:50:26','0000-00-00 00:00:00','CRS','octavio.gonzalez@am.jll.com'),(1027,'1027/comp_61120.JPG','Terreno','Carr.México Cuautla','S/N','Tetelcingo','Cuautla','De Morelos','62757',0,0,'Camacho Bienes Raices','73-53-56-66-38',1421.0,15295.5,0.0,3907750,NULL,2750,0,'05-03-2016',' Terreno de forma refugular en esquina, de topografía plana.',1,NULL,'01',17.7723,'2016-08-29 13:44:59','2016-08-02 14:12:51','2016-08-29 13:44:59','CLS','octavio.gonzalez@am.jll.com'),(1122,'1122/comp_41220.JPG','Casa','Sierra Leona','778','Lomas de Chapultepec','Miguel Hidalgo','Ciudad de México','11000',0,0,'Más por tu casa','(01) 55-41-92-79-88',580.0,6243.1,650.0,149590.31,NULL,230.14,0,'10-02-2016',NULL,0,NULL,NULL,17.8561,'2016-08-03 15:29:28','2016-07-27 14:40:11','2016-08-03 15:29:28','CRL','octavio.gonzalez@am.jll.com'),(1164,'1164/comp_61185.JPG','Departamento','Horacio','S/N','Polanco','Miguel Hidalgo','Ciudad de México','11510',0,0,'Coldwell Banker Esteban','(01) 55-55-50-27-16',0.0,0.0,105.0,4950000,NULL,47142.86,0,'14-01-2016',NULL,0,NULL,NULL,17.8561,'2016-08-03 17:35:20','2016-07-25 11:52:16','2016-08-03 17:35:20','CRS','octavio.gonzalez@am.jll.com'),(1204,'1204/comp_1699.JPG','Casa','Circuito Univercidades','S/N','Zibatá','El Marqués','Querétaro','76269',0,0,'Lienzo Grupo Inmobiliario','44-22-23-66-82',168.0,1808.3,146.0,2000000,0,13698.63,0,'16-03-2016',' 146 m2 (Construcción) 3 Niveles / 3 Recámaras / 3.5 Baños - Recámara principal con vestidor, baño y balcón - Recámaras secundaria con clóset y baño propio - Cocina integral equipada - Amplia sala y comedor - Estudio - Jardín - Roof Garden con Vista Panorámica - Casa Club con Alberca y Salón de Fiestas - En privada con acceso controlado',1,NULL,'01',19.8925,'2016-09-19 12:24:00','2016-09-19 12:24:00','0000-00-00 00:00:00','CRS','octavio.gonzalez@am.jll.com'),(1237,'1237/comp_31251.JPG','Terreno','Explanada esquina con Sierra Nevada','S/N','Lomas de Chapultepec','Miguel Hidalgo','Ciudad de México','11000',0,0,'Norma Villanueva','56-83-12-15-',1531.0,16479.5,0.0,68903835,0,45005.77,0,'12 -24 meses',' Property with constructions sold as vacant land, flat regular-shaped lot with two fronts.',1,NULL,'01',17.8971,'2016-08-11 13:16:20','2016-08-11 12:28:58','2016-08-11 13:16:20','CLS','octavio.gonzalez@am.jll.com'),(1253,'1253/comp_11429.JPG','Departamento','Av.Cuauhtémoc','590','Narvarte','Benito Juárez','Ciudad de México','03020',0,0,'Inmuebles MS','(01) 55-65-82-81-94',0.0,0.0,63.0,2152000,NULL,34158.73,0,'03-07-2015',NULL,0,NULL,NULL,15.6918,'2016-08-05 12:14:58','2016-08-05 12:14:58','0000-00-00 00:00:00','CRS','octavio.gonzalez@am.jll.com'),(1276,'1276/comp_61377.JPG','Terreno','Francico I Madero','S/N','Nacozari','Tizayuca','Hidalgo','43803',0,0,'Alfa Nevado Inmobiliaria','22-48-72-65-80',4800.0,51666.8,0.0,12960000,0,2700,0,'12-18 meses',' Land 4,800 m2, flat topography.',1,NULL,'017',17.1767,'2016-08-17 15:43:57','2016-08-17 15:43:57','0000-00-00 00:00:00','CLS','octavio.gonzalez@am.jll.com'),(1315,'1315/comp_51370.JPG','Departamento ','Nuevo León','88','Roma Norte','Cuauhtémoc','Ciudad de México','06700',0,0,'Easy Broker','55-70-18-98',0.0,0.0,190.0,9500000,0,50000,0,'26-02-2016',' Ph de dos niveles con sala, comedor, cocina, área de lavado, dos recámaras con baño completo, terraza y dos cajones de estacionamiento ',1,'2016-02-26','',18.1706,'2016-09-30 12:13:58','2016-09-30 12:13:58','0000-00-00 00:00:00','CRS','octavio.gonzalez@am.jll.com'),(1377,'1377/comp_51401.JPG','Departamento','Homero','1303','Polanco','Miguel Hidalgo','Ciudad de México','11550',0,0,'Asesoría Inmobiliaria M y Z, S.C.','(01) 55-52-81-39-88',0.0,0.0,200.0,25000,NULL,125,0,'14-01-2016',NULL,0,NULL,NULL,17.8561,'2016-08-03 15:25:23','2016-07-26 12:53:55','2016-08-03 15:25:23','CRL','octavio.gonzalez@am.jll.com'),(1417,'1417/comp_51445.JPG','Casa','Amado Nervo','S/N','La Florida','Cerro de San Pedro','San Luis Potosí','78448',0,0,'Habita forte Inmo','(01) 44-41-29-73-72',1150.0,12378.5,539.0,4200000,NULL,7792.21,0,'17-03-2016',NULL,0,NULL,NULL,17.8561,'2016-07-19 15:01:39','2016-07-19 14:45:07','2016-07-19 15:01:39','CRS','octavio.gonzalez@am.jll.com'),(1581,'1581/comp_11696.JPG','Casa','Vía Dr.Jorge Jiménez Cantú','S/N','Fracc. Acua ','Atizapán de Zaragoza','Estado de México','52970',0,0,'Sr. Ramón Aguilar','65-82-10-73-',126.0,1356.2,230.0,4500000,0,19565.22,0,'23-03-2016',' Casa en condominio, cuenta con sala comedor a doble altura, cocina, 1/2 baño, área de lavado, jardín, 3 recámaras, 3 baños y 2 cajones de estacionamiento.',1,NULL,'',19.8925,'2016-09-19 15:07:14','2016-09-19 15:07:14','0000-00-00 00:00:00','CRS','octavio.gonzalez@am.jll.com'),(1594,'1594/comp_17213.JPG','Terreno','Blvd.Calacoaya','S/N','Calacoaya','Atizapán de Zaragoza','Estado de México','52996',0,0,'Sr. Erik Cuevas','(01) 55-70-31-97-26',45000.0,484376.0,0.0,180000000,NULL,4000,0,'15- 02-2016',NULL,0,NULL,NULL,17.7723,'2016-08-02 14:26:41','2016-07-12 12:58:36','2016-07-28 18:01:14','CLS','octavio.gonzalez@am.jll.com'),(1606,'1606/comp_21665.JPG','Departamentos','Presa Falcón','S/N','Granada','Miguel Hidalgo','Ciudad de México','11320',0,0,'Línea Creativa','(01) 55-91-98-36-99',0.0,0.0,109.0,5800000,NULL,53211.01,0,'12-04-2016',' Departamento con sala, comedor, área de lavado, cocina, dos recámaras, 2.5 baños, sala de t.v, dos estacionamientos, el condominio cuenta con alberca, spa, gimnasio, ludoteca, sala de lectura, business center, salon de eventos y acceso directo a plaza carso.',1,NULL,'',17.8561,'2016-09-12 18:24:23','2016-07-28 12:08:53','2016-09-12 18:24:23','CRS','octavio.gonzalez@am.jll.com'),(1623,'1623/comp_45805.jpg','Land','Avenida Santa Fé','23','San Cayetano Morelos','Toluca','Estado de México','14275',0,0,'Alfa Nevado Inmobiliaria','(01) 72-24-87-26',8800.0,94722.4,0.0,23000000,77419.355,2613.64,0,'24-36 months',NULL,0,NULL,NULL,17.8561,'2016-07-19 03:05:48','2016-03-08 00:02:05','2016-07-19 03:05:48','CLS','eduardo.ms@senni.com.mx'),(1634,'1634/comp_31853.JPG','Casa','Fray P de Rivera','S/N','Lomas de Chapultepec','Miguel Hidalgo','Ciudad de México','11000',0,0,'Remax/TK','(01) 55-52-91-52-55',700.0,7534.7,741.0,38158260,NULL,51495.63,0,'10-02-2016',NULL,0,NULL,NULL,17.8561,'2016-08-03 17:45:50','2016-07-27 11:34:29','2016-08-03 17:45:50','CRS','octavio.gonzalez@am.jll.com'),(1663,'1663/comp_81735.JPG','Departamento','Av. Lic. Manuel Rojas','174','Constituyentes 1917','Huixquilucan','Estado de México','52775',0,0,'Base de datos JLL','55-59-80-88-50',0.0,0.0,72.0,2100000,0,29166.67,0,'10-03-2016',' 1 recámaras, 1 baño, 1 cajón de estacionamiento, alberca, gimnasio, salon de eventos, salon de adultos, de jovenes y de ninos, acceso directo al centro comercial Parque Interlomas ',1,NULL,'01',19.6,'2016-09-06 13:19:11','2016-09-06 13:19:11','0000-00-00 00:00:00','CRS','octavio.gonzalez@am.jll.com'),(1668,'1668/comp_19478.JPG','Nave Industrial','Antonio Rocha Cordero','S/N','Pedregal de Guadalupe','San Luis Potosí','San Luis Potosí','78395',0,0,'Ana Lucía Camacho','(45) 44-44-29-28-72',3290.0,35413.3,2500.0,90000,NULL,36,0,'17-03-2016',NULL,0,NULL,NULL,17.8561,'2016-07-21 12:44:47','2016-07-18 13:24:58','2016-07-21 12:44:47','CRL','octavio.gonzalez@am.jll.com'),(1678,'1678/comp_41956.jpg','centro comercial II','pedregal','57 ','Lomas de Chapultepec','Alvaro Obregon','Ciudad de México','232442',0,0,'asdsadasd','(12) 55-55-23-24',850000.0,9149323.9,200000.0,150000,9677.419,0.176,0,'12-18 months',NULL,0,NULL,NULL,15.5,'2016-08-01 11:03:13','2016-03-09 18:13:39','2016-08-01 11:03:13','CLS','octavio.gonzalez@am.jll.com'),(1752,'1752/comp_81793.JPG','Casa','Campanulas','56','Brisas de Cuautla','Cuautla','De Morelos','62757',0,0,'Camacho Bienes Raices','(01) 73-53-98-17-52',525.0,5651.1,266.0,2146000,NULL,8067.67,0,'07-03-2016',NULL,0,NULL,NULL,17.7723,'2016-08-02 19:55:05','2016-08-02 19:55:05','0000-00-00 00:00:00','CRS','octavio.gonzalez@am.jll.com'),(1766,'1766/comp_61920.JPG','Land','Carretera Guadalajara- El Salto','KM. 3.6','','','Jalisco','',0,0,'Remax Activa','36-16-18-00-',151910.0,1635145.6,0.0,39000000,0,256.73,0,'',' Flat, regular shape, Industrial and commercial use.',2,'2016-07-15','133',18.1706,'2016-09-30 15:01:31','2016-09-30 15:01:31','0000-00-00 00:00:00','CLS','oscar.alonso@am.jll.com'),(1800,'1800/comp_81820.JPG','Terreno','Lago Yogoa','161','Cinco de Mayo','Miguel Hidalgo','Ciudad de México','11470',0,0,'Centuri 21 Universum','65-83-21-73',368.0,3961.1,0.0,5500000,0,14945.65,0,'15- 02-2016',' Terreno baldio de forma irregular, un frente, uso habitacional HC 3 20 Z, actualmente se trata de un estacionamiento.',1,NULL,'',18.3748,'2016-08-31 18:34:21','2016-08-31 18:34:21','0000-00-00 00:00:00','CLS','octavio.gonzalez@am.jll.com'),(1802,'1802/comp_12065.JPG','Departamento','Carr.México -Huixquilucan','S/N','Bosque Real','Huixquilucan','Estado de México','52774',0,0,'BR Living','55-21-06-41-47',0.0,0.0,129.0,4450000,0,34496.12,0,'10-03-2016',' Piso alto, terminados de lujo 2 recámaras con baño completo con vestidor, sala y comedor con efecto terraza, vista a campo de golf y a la ciudad, baño de visitas, cocina integral equipada, cuarto de lavado, cuarto y área de servicio.  Salón de fiestas, salón de negocios, ludoteca y juegos para niños, salón de eventos, 2 lugares de estacionamiento y bodega.',1,NULL,'044',19.6,'2016-09-05 18:38:27','2016-09-05 18:38:27','0000-00-00 00:00:00','CRS','octavio.gonzalez@am.jll.com'),(1855,'1855/comp_1875.JPG','Terreno','Av. Prologación Paseo de la Reforma','799','San Gabriel','Álvaro Obregón','Ciudad de México','01310',0,0,'Ziti Brokers','(01) 55-52-91-44-33',17173.0,184848.6,0.0,404960806,NULL,23581.25,0,'15- 02-2016',NULL,0,NULL,NULL,17.8561,'2016-08-03 15:17:33','2016-07-22 11:43:50','2016-08-03 15:17:33','CLS','octavio.gonzalez@am.jll.com'),(1941,'1941/comp_52158.JPG','Terreno','Oriente-Poniente Esq. Sur 4,','S/N','Ciudad industrial','Tizayuca','Hidalgo','43800',0,0,'Celestino Salgado','55-16-56-27-13',12280.0,132180.8,1960.0,23500000,0,1913.68,0,'12-18 meses',' Oriente Poniente and Sur 4, it has an industrial shed of 1,960m2, sold as vacant land.',1,NULL,'044',17.1767,'2016-08-17 14:06:48','2016-08-17 13:21:24','2016-08-17 14:06:48','CLS','octavio.gonzalez@am.jll.com'),(1976,'1976/comp_72387.JPG','Nave Industrial','Oriente-Poniente','S/N','Ciudad Industrial','Tizayuca','Hidalgo','43800',0,0,'Josefina Serrano','53-61-67-17-1',6000.0,64583.5,1500.0,58005.71,0,38.67,0,'',' Industrial warehouse with offices.',1,NULL,'',17.1767,'2016-08-17 18:52:13','2016-08-17 18:52:13','0000-00-00 00:00:00','CRL','octavio.gonzalez@am.jll.com'),(2084,'2084/comp_52183.JPG','Departamento ','Cordoba','S/N','Roma','Cuauhtémoc','Ciudad de México','06700',0,0,'Alejandra Ferrer','55-41-94-07-01',0.0,0.0,186.0,8600000,0,46236.56,0,'26-02-2016',' Ph de dos niveles con sala, comedor, cocina, área de lavado , 3recámaras, 4 baños compleos, sala de t.v., terraza y dos cajones de estacionamiento',1,'2016-02-26','044',18.1706,'2016-09-30 12:40:22','2016-09-30 12:40:22','0000-00-00 00:00:00','CRS','octavio.gonzalez@am.jll.com'),(2100,'2100/comp_62114.JPG','Land','Manuel Enriquez','','','Ocotlán','Jalisco','',0,0,'Federico González','39-29-30-23-77',95000.0,1022571.5,0.0,9500000,0,100,0,'','Flat, near to highway, mixed use.',2,'2016-08-17','045',18.1706,'2016-09-30 15:03:39','2016-09-30 15:03:39','0000-00-00 00:00:00','CLS','oscar.alonso@am.jll.com'),(2101,'2101/comp_82974.JPG','Casa','Puerta de Barcelona','S/N','Bosque Esmeralda','Atizapan de Zaragoza','Estado de México','52930',0,0,'Guadalupe Briseño','55-72-73-23-',260.0,2798.6,252.0,5500000,0,21825.4,0,'23-03-2016',' Casa en condominio con sala, comedor, cocina, desayunador, área de lavaso, sala de T.V., tres recámaras, 3 baños completos, jardín posterior, 3 estacionamientos',1,NULL,'',19.8925,'2016-09-20 10:50:00','2016-09-19 15:15:39','2016-09-20 10:50:00','CRS','octavio.gonzalez@am.jll.com'),(2113,'2113/comp_82341.JPG','Terreno','Tlacateco','S/N','Villas del Convento','Tepotzotlán','Estado de México','54605',0,0,'Promoluxury Inmobiliaria','55-66-39-65-75',8800.0,94722.4,0.0,22999990.1,0,2613.64,0,'03-03-2016',': Land Lot in Tlacateco, Near Tepotzotlan Town.',1,NULL,'01',17.8561,'2016-08-24 20:08:10','2016-08-24 20:08:10','0000-00-00 00:00:00','CLS','octavio.gonzalez@am.jll.com'),(2182,'2182/comp_22234.JPG','Departamento',' Mariano Escobedo y lago Alberto','S/N','Anáhuac','Miguel Hidalgo','Ciudad de México','11320',0,0,'Yosy Arzamendi','(01) 55-52-94-15-51',0.0,0.0,97.0,5239197.95,NULL,54012.35,0,'12-04-2016','',1,NULL,'',17.8561,'2016-09-12 18:30:43','2016-07-28 12:21:26','2016-09-12 18:30:43','CRS','octavio.gonzalez@am.jll.com'),(2244,'jll_in.jpg','Industrial Bay','Mexico-Queretaro highway km','42.5','Prologis, Industrial Park, Storage Buldi','Tepotzotlan','Edo de Mexico','54600',0,0,'JLL DataBase','(01) 55-59-80-88-50',0.0,0.0,15958.0,1188231.67,NULL,74.46,0,'2Q2015',NULL,0,NULL,NULL,17.8561,'2016-07-19 03:18:31','2016-06-27 23:20:41','2016-07-19 03:18:31','CRL','eduardo.ms@senni.com.mx'),(2280,'2280/comp_62286.JPG','Land','','','El Pedregal','Ocotlán','Jalisco','',0,0,'Federico González','39-29-30-23-77',50000.0,538195.5,0.0,8000000,0,160,0,'12-36 months','Near Zula river, flat, industrial use.',1,'2016-09-30','045',18.1706,'2016-09-30 15:07:24','2016-09-30 15:07:24','0000-00-00 00:00:00','CLS','oscar.alonso@am.jll.com'),(2335,'2335/comp_22408.JPG','Terreno','Circuito Univercidades','S/N','Zibatá','El Marqués','Queretaro','76269',0,0,'Alleanza Inmobiliaria','44-23-64-72-22',183.0,1969.8,0.0,750000,0,4098.36,0,'16-03-2016',' Terreno de 183.17 m2 frente a Áreas verdes.',1,NULL,'01',19.8925,'2016-09-13 12:56:13','2016-09-12 19:58:01','2016-09-13 12:56:13','CLS','octavio.gonzalez@am.jll.com'),(2348,'2348/comp_42372.JPG','Casa','Paseo del Valle','1','Paseo del Valle','Nextlalpan','Estado de México','55796',0,0,'Iván Arenas','80-07-46-42-73',60.0,645.8,52.5,490000,0,9333.33,0,'23-05-2016','Condominio formado por varias vivendas,las cuales se desarrollan en dos niveles, Cuenta con sala, comedor, cocina, área de lavado, 2 recámaras, 1.5 baños y dos cajones de estacionamiento.',1,'2016-05-23','01',18.4444,'2016-09-23 12:15:34','2016-09-23 11:29:41','2016-09-23 12:15:34','CRS','octavio.gonzalez@am.jll.com'),(2353,'2353/comp_2366.JPG','Terreno','Francisco J. Serrano','S/N','Santa Fe La Loma','Cuajimalpa','Ciudad de México','01219',0,0,'Silvia Villaseñor','(01) 72-22-71-30-34',18413.0,198195.9,0.0,459264000,NULL,24942.38,0,'15- 02-2016',NULL,0,NULL,NULL,17.8561,'2016-08-03 15:19:24','2016-07-22 11:53:14','2016-08-03 15:19:24','CLS','octavio.gonzalez@am.jll.com'),(2384,'2384/comp_12493.JPG','Terreno','Oriente 2','S/N','Santa Rita','Celaya','Guanajuato','38010',0,0,'D Casa Angel Garcia','46-16-13-66-33',4452.0,47920.9,0.0,491879,0,110.48,0,'01-03-2-16',' 2 land lots adding 4,452 sqm, industrial use.',1,'2016-03-01','01',18.102,'2016-09-27 18:21:10','2016-09-27 18:21:10','0000-00-00 00:00:00','CLS','octavio.gonzalez@am.jll.com'),(2421,'2421/comp_18456.JPG','Departamento','Perdiz','5 a','Lomas de Guadalupe','Atizapán de Zaragoza','Estado de México','52985',0,0,'Sra. Perla','(01) 55-22-08-29-21',0.0,0.0,96.0,2142039,NULL,22312.91,0,'15- 02-2016',NULL,0,NULL,NULL,17.8561,'2016-07-21 12:27:50','2016-07-13 19:36:26','2016-07-21 12:27:50','CRS','octavio.gonzalez@am.jll.com'),(2431,'2431/comp_17259.JPG','Terreno','Cóporo','S/N','Lopez Mateos','Atizapán de zaragoza','Estado de México','52900',0,0,'Carmén Iglesias','(01) 55-15-09-01-37',21000.0,226042.1,0.0,90000000,NULL,4285.71,0,'15- 02-2016',NULL,0,NULL,NULL,17.8561,'2016-08-01 11:49:39','2016-07-13 11:12:57','2016-08-01 11:49:39','CLS','octavio.gonzalez@am.jll.com'),(2452,'2452/comp_62459.JPG','Land','Paso de la Comunidad','','','Ocotlán','Jalisco','',0,0,'Federico González','39-29-30-23-77',90000.0,968751.9,0.0,22500000,0,250,0,'24-36 months','Near to highway, flat, irregular shape, mix use, ejidal land in process of regularation.',1,'2016-09-30','045',18.1706,'2016-09-30 15:09:15','2016-09-30 15:09:15','0000-00-00 00:00:00','CLS','oscar.alonso@am.jll.com'),(2537,'2537/comp_12722.JPG','Casa','Av. Los Sabinos','10','Cuautlixco','Cuautla','Estado de Morelos','62747',0,0,'Bim Bolsa Inmobiliaria','73-51-02-45-40',700.0,7534.7,140.0,9280,0,66.29,0,'03-05-2016',' Casa en venta de 1 nivel en fraccionamiento, con  700 M2 de Terreno y 140 M2 de construcción. La renta incluye mantenimiento del Fraccionamiento, 3 recámaras, 1 y medio baños, cocina, sala, comedor, estacionamiento 2 autos, jardín, cisterna, chapoteadero, terraza y área de lavado.',1,'2016-03-05','01',17.7723,'2016-09-26 20:04:51','2016-09-26 14:26:10','2016-09-26 20:04:51','CRL','octavio.gonzalez@am.jll.com'),(2540,'2540/comp_82935.jpg','Industrial Bay A.5','Av. Industria Automotriz','s/n','Prologis Park Toluca','Toluca','Estado de Mexico','1234',0,0,'JLL DatabasE','(01) 55-59-80-88-41',3.0,32.3,28693.0,10000000,644745.33,348.52,2.09,'1Q2015',NULL,0,NULL,NULL,15.51,'2016-03-30 03:03:49','2016-03-18 02:00:42','2016-03-30 03:03:49','CRS','alfredo.giorgana@am.jll.com'),(2563,'2563/comp_62569.JPG','Land','Carretera Ocotlán-Jamay','','','Ocotlán','Jalisco','',0,0,'Century 21','33-31-21-00-21',240000.0,2583338.5,0.0,38400000,0,160,0,'24-36 months','Near to highway, flat, irregular shape, mix use.',1,'2016-09-30','01',18.1706,'2016-09-30 15:11:11','2016-09-30 15:11:11','0000-00-00 00:00:00','CLS','oscar.alonso@am.jll.com'),(2634,'2634/comp_44824.jpg','Land','Road Tecaxic to Calixtlahuaca','s/n','Tecaxic.','Puebla','Puebla','0909',19.606122,-99.205527,'Realty World Key House','(01) 72-26-24-39',20000.0,215278.2,0.0,50000000,64516.13,2500,0,'12-18 months',NULL,0,NULL,NULL,17.8561,'2016-08-01 11:02:32','2016-03-04 01:53:39','2016-08-01 11:02:32','CLS','octavio.gonzalez@am.jll.com'),(2661,'2661/comp_2722.JPG','Casa','Circuito Univercidades','S/N','Zibatá','El Marqués','Querétaro','76269',0,0,'Imagen Inmobiliaria','44-21-99-01-91',0.0,0.0,154.0,1695000,0,11006.49,0,'16-03-2016',' Casa en venta - 3 recamaras, 2.5 baños, sala, comedor, cocina equipada con granito muy amplia, area de lavado, bodega, pequeño jardin privada con vigilancia, areas verdes, club deportivo con alberca, gym, areas verdes.',1,NULL,'01',19.8925,'2016-09-19 12:38:05','2016-09-19 12:38:05','0000-00-00 00:00:00','CRS','octavio.gonzalez@am.jll.com'),(2826,'2826/comp_42851.JPG','Nave Industrial','Norte 45 esquina con Pte 128','S/N','Industrial Vallejo','Azcapotzalco','Ciudad de México','2300',0,0,'Luis Enrique Sanchez Palacios','55-29-00-15-67',6000.0,64583.5,5600.0,556800.61,0,99.43,0,'3-6 meses',' Bodega industrial, cuenta con bodega, almacenes menores, oficinas, andén de carga y descarga, patio de maniobras, estacionamiento, altura promedio 10.00 m.',1,NULL,'044',17.7866,'2016-08-19 17:55:14','2016-08-19 17:55:14','0000-00-00 00:00:00','CRL','octavio.gonzalez@am.jll.com'),(2836,'2836/comp_63166.JPG','Local Comercial','Cuauhtémoc','S/N','Vertiz Narvarte','Benito Juárez','Ciudad de México','3600',0,0,'Contacto Bienes Raíces Gold','55-67-18-70-11',0.0,0.0,232.0,85000,0,366.38,0,'',' Amplio Local Comercial con 232m2 de construcción en dos niveles, el nivel superior con tapanco. Cuatro estacionamientos en batería con excelente ubicación sobre Eje Vial y en zona comercial cercana a estación del metro. Alto flujo de peatones. Comercios vecinos: Oxxo, Clínicas, Panaderías, Farmacias.',1,NULL,'01',15.6918,'2016-08-09 13:29:36','2016-08-09 13:29:36','0000-00-00 00:00:00','CRL','octavio.gonzalez@am.jll.com'),(2842,'2842/comp_62932.JPG','Terreno','Av. Tenayuca- Santa Cecilia','290',' Santa Cecilia','Tlalnepantla','Estado de México','54149',0,0,'Kareg Inmobiliaria','81-86-34-89-18',11200.0,120555.8,0.0,67205280,0,6000.47,0,'12-18 meses',' Land 11,200 m, flat regular shape, commercial industrial use.',1,NULL,'01',17.7866,'2016-08-18 19:45:54','2016-08-18 19:45:54','0000-00-00 00:00:00','CLS','octavio.gonzalez@am.jll.com'),(2964,'2964/comp_62980.JPG','Terreno','Agapando','100','Brisas de Cuautla','Cuautla','De Morelos','62753',0,0,'Sr. Martín G.  Srita. Viviana','77-73-10-32-53',750.0,8072.9,0.0,1392000,NULL,1856,0,'05-03-2016',' terreno completamente plano, cuenta con 15 metros de frente y 50 metros de fondo, cuenta con fosa septica y todos los servicios. (drenaje en proceso) 3 bardas y porton.',1,NULL,'01',17.7723,'2016-08-29 14:11:17','2016-08-02 14:42:01','2016-08-29 14:11:17','CLS','octavio.gonzalez@am.jll.com'),(2980,'2980/comp_83033.JPG','Terreno','Laguna del Carmen','140','Anáhuac ll Sección','Miguel Hidalgo','Ciudad de México','11320',0,0,'Desarrolladora Inmobiliaria México','63-95-31-34',627.0,6749.0,0.0,12500000,0,19936.2,0,'15- 02-2016',' Terreno de forma refugular en esquina, de topografía plana.',1,NULL,'',18.3748,'2016-08-31 18:55:06','2016-08-31 18:55:06','0000-00-00 00:00:00','CLS','octavio.gonzalez@am.jll.com'),(2987,'2987/comp_65613.jpg','Land','Almoloya de Juerez','123','centro','Almoloya de Juerez','Estado de Mexico','1234',0,0,'Century 21 Vilchis / Hugo Hernández','(01) 72-22-77-10',73904.0,795496.0,0.0,48000000,309677.419,649.49,0,'24-36 months',NULL,0,NULL,NULL,17.8561,'2016-07-19 03:04:40','2016-03-04 04:47:29','2016-07-19 03:04:40','CLS','eduardo.ms@senni.com.mx'),(2995,'2995/comp_63282.jpg','Industrial Bay A.5','Av. De la industria Automotriz','s/n','Industrial Park Doña Rosa','Toluca','Estado de Mexico','123',0,0,'Jll Database','(01) 55-59-80-88-41',0.0,0.0,58689.0,282182580.9,18193590,4808.1,310,'1Q2015',NULL,0,NULL,NULL,15.51,'2016-03-28 08:42:46','2016-03-18 02:06:25','2016-03-28 08:42:46','CRS','alfredo.giorgana@am.jll.com'),(3019,'3019/comp_55052.JPG','Terreno','Jaime Nuño','S/N',' Estrella','Cuautla','De Morelos','62747',0,0,'Sr. Vallejo','55-32-36-75-88',575.0,6189.2,0.0,1800000,NULL,3130.43,0,'05-03-2016',' Terreno bardeado, en esquina, topografía plana, todos los servicios básicos de infraestructura.',1,NULL,'01',17.7723,'2016-08-29 13:05:12','2016-08-02 13:00:51','2016-08-29 13:05:12','CLS','octavio.gonzalez@am.jll.com'),(3050,'3050/comp_18576.JPG','Departamento','Domingo de Ramos','S/N','Calacoaya','Atizapán de Zaragoza','Estado de México','52996',0,0,'Joel Ortega','(01) 55-67-25-64-72',0.0,0.0,117.0,2350000,NULL,20085.47,0,'15- 02-2016',NULL,0,NULL,NULL,17.8561,'2016-07-21 12:29:48','2016-07-13 19:45:23','2016-07-21 12:29:48','CRS','octavio.gonzalez@am.jll.com'),(3062,'3062/comp_17083.JPG','Casa','Lago Ness','S/N','Lago Esmeralda','Atizapán de Zaragoza','Estado de México','52989',0,0,'Ricardo Corro','(01) 55-63-10-29-69',220.0,2368.1,147.0,3800000,NULL,25850.34,0,'15- 02-2016',NULL,0,NULL,NULL,17.8561,'2016-07-21 12:04:56','2016-07-11 18:11:20','2016-07-21 12:04:56','CRS','octavio.gonzalez@am.jll.com'),(3105,'3105/comp_3158.JPG','Casa','Circuito Univercidades','S/N','Zibatá','El Marqués','Querétaro','76269',0,0,'Tu Inmobiliaria','44-23-41-30-37',0.0,0.0,160.0,1740000,0,10875,0,'16-03-2016',' 3 Recámaras principal con baño, cocina equipada, lavandería, jardín, bodega, calentador solar y de paso, cisterna 2,500 lts, focos de led, hidroneumático, 2 cámaras con DVR con capacidad hasta 4 cámaras.',1,NULL,'01',19.8925,'2016-09-19 12:44:08','2016-09-19 12:44:08','0000-00-00 00:00:00','CRS','octavio.gonzalez@am.jll.com'),(3152,'3152/comp_83164.jpg','Industrial A.5','Isidro Fabela','126','San Blas Otzacatipan','Toluca','Estado de Mexico','1234',0,0,'JLL Database','(01) 55-59-80-88-41',0.0,0.0,5139.0,25505884.8,1644480,4963.2,320,'1Q2015',NULL,0,NULL,NULL,15.51,'2016-03-28 08:43:53','2016-03-22 22:48:54','2016-03-28 08:43:53','CRS','alfredo.giorgana@am.jll.com'),(3183,'3183/comp_83243.JPG','Terreno','Puertade Maria','S/N','Bosque Esmeralda','Atizapán de Zaragoza','Estado de México','52930',0,0,'Maribel Castillo','53-93-27-00-',375.0,4036.5,0.0,3750000,0,10000,0,'23-03-2016',' Terreno plano con uso de suelo habitacional ',1,NULL,'',19.8925,'2016-09-20 11:10:22','2016-09-20 11:10:22','0000-00-00 00:00:00','CLS','octavio.gonzalez@am.jll.com'),(3286,'3286/comp_63355.JPG','Local Comercial','Colonias entre Av. Libertad y Av. La Paz','S/N','Americana','Guadalajara','Jalisco','44160',0,0,'Guía Excelencia Inmobiliaria','38-17-48-01-',0.0,0.0,64.0,19115,0,298.67,0,'',' Local comercial en Vía Libertad, plaza de cultura y gastronomía.',1,NULL,'',19.5997,'2016-09-09 19:33:59','2016-09-09 19:33:59','0000-00-00 00:00:00','CRL','octavio.gonzalez@am.jll.com'),(3322,'3322/comp_93399.JPG','Departamento','Av. Lapaz esq.Unión','S/N','Lafayette','Guadalajara','Jalisco','44150',0,0,'Eduardo Martínez Valencia','33-36-31-98-',0.0,0.0,116.0,3112000,0,26827.59,0,'10-03-2016',' Departamento con 2 recámaras, 2 baños, 2 estacionamientos. Ubicado en el segundo piso. Alberca de uso común y elevador.',1,NULL,'',19.5997,'2016-09-12 11:35:11','2016-09-12 11:35:11','0000-00-00 00:00:00','CRS','octavio.gonzalez@am.jll.com'),(3345,'3345/comp_83365.jpg','Industrial Bay A.5','Av. Industria Automotriz, bay 2','s/n','Prologis Park','Toluca','Estado de Mexico','123',0,0,'JLL Database','(01) 55-59-80-88-41',0.0,0.0,6156.0,28643868,1846800,4653,300,'1Q2015',NULL,0,NULL,NULL,15.51,'2016-03-28 08:44:37','2016-03-22 22:51:23','2016-03-28 08:44:37','CRS','alfredo.giorgana@am.jll.com'),(3363,'3363/comp_23488.JPG','Nave Industrial','Noorte 59 esquina con Pte 150','S/N','Industrial Vallejo','Azcapotzalco','Ciudad de México','2300',0,0,'JLL Database','55-59-80-81-49',0.0,0.0,2750.0,280761.48,0,102.1,0,'10-02-2016',' Industrial warehouse with offices, mezzanine,  covered with galvanized sheet, Average Building Height: 12.00 m (129.17 ft).',2,NULL,'01',17.7866,'2016-08-19 12:41:47','2016-08-19 12:41:47','0000-00-00 00:00:00','CRL','octavio.gonzalez@am.jll.com'),(3376,'3376/comp_16994.JPG','Casa','Petirrojo','34','Las Alamedas','Atizapan de Zaragoza','Estado de México','52970',0,0,'Guillermo Arena','(01) 55-91-15-69-74',160.0,1722.2,170.0,3385000,NULL,19911.76,0,'15- 02-2017',NULL,0,NULL,NULL,17.8561,'2016-07-21 12:03:29','2016-07-11 14:59:32','2016-07-21 12:03:29','CRS','octavio.gonzalez@am.jll.com'),(3435,'3435/comp_63486.JPG','Terreno','Primavera','S/N','Lázaro Cárdenas','Cuautla','De Morelos','62757',0,0,'Jesús Franco','73-51-94-34-87',1000.0,10763.9,0.0,2320000,NULL,2320,0,'05-03-2016',' terreno completamente plano, cuenta con 15 metros de frente y 50 metros de fondo, cuenta con fosa septica y todos los servicios. (drenaje en proceso) 3 bardas y porton.',1,NULL,'01',17.7723,'2016-08-29 14:13:29','2016-08-02 14:49:49','2016-08-29 14:13:29','CLS','octavio.gonzalez@am.jll.com'),(3476,'3476/comp_3575.JPG','Casa','Circuito Univercidades','S/N','Zibatá','El Marqués','Querétaro','76269',0,0,'Huerta Bienes Raíces','44-23-52-50-07',150.0,1614.6,150.0,1790000,0,11933.33,0,'16-03-2016','3 recámaras, 2 baños, medio baño, 2 estacionamientos, cocina integral.',1,NULL,'045',19.8925,'2016-09-19 12:57:12','2016-09-19 12:57:12','0000-00-00 00:00:00','CRS','octavio.gonzalez@am.jll.com'),(3491,'3491/comp_83502.jpg','Industrial Bay A.5','Av. Industria Automotriz','s/n','Parque Industrial Toluca','Toluca','Estado de Mexico','14275',0,0,'Bufete Logistico Inmobiliario','(01) 55-85-90-80-00',0.0,0.0,8000.0,39705600,2560000,4963.2,320,'1Q2015',NULL,0,NULL,NULL,15.51,'2016-03-28 08:45:18','2016-03-22 22:53:17','2016-03-28 08:45:18','CRS','alfredo.giorgana@am.jll.com'),(3590,'3590/comp_63602.JPG','Departamento','Campos Eliseos','457','Polanco','Miguel Hidalgo','Ciudad de México','11550',0,0,'Lorena Davila','(01) 55-39-55-85-10',0.0,0.0,177.0,7950000,NULL,44915.25,0,'14-01-2016',NULL,0,NULL,NULL,17.8561,'2016-08-03 17:43:48','2016-07-25 12:44:59','2016-08-03 17:43:48','CRS','octavio.gonzalez@am.jll.com'),(3631,'3631/comp_25634.JPG','Local Comercial','Aviación Civil','15','Industrial Puerto Aéreo','Venustiano Carranza','Ciudad de México','15710',0,0,'Rojkind Inmobiliaria','(01) 55-55-20-40-00',313.0,3369.1,0.0,109550,NULL,0,0,'24-05-2016',NULL,0,NULL,NULL,18.3826,'2016-08-04 12:09:56','2016-08-04 12:09:56','0000-00-00 00:00:00','CRL','octavio.gonzalez@am.jll.com'),(3645,'3645/comp_93686.JPG','Terreno','Andres de la Concha','S/N','San José Insurgentes','Benito Juárez','Ciudad de México','03900',0,0,'Grupo Navel','55-68-70-50',801.0,8621.9,0.0,31500,0,39.33,0,'14-03-2016','Terreno con uso H 3 20 Esquina Plano Regular',1,NULL,'',17.75,'2016-08-30 18:29:46','2016-08-30 18:26:45','2016-08-30 18:29:46','CLS','octavio.gonzalez@am.jll.com'),(3662,'3662/comp_63680.JPG','Nave Industrial','Autopista México-Queretaro  Km 42.5 Nave 1','S/N','Cedros','Tepotzotlán','Estado de México','54600',0,0,'Glosermex Global Servicio','55-56-64-18-45',0.0,0.0,6000.0,514255.68,0,85.71,0,'10-02-2015',' Industrial Bay  AAA , included offices, manuever yard,  average height 7 m.',2,NULL,'01',17.8561,'2016-08-25 19:04:11','2016-08-25 19:04:11','0000-00-00 00:00:00','CRL','octavio.gonzalez@am.jll.com'),(3666,'3666/comp_18621.JPG','Departamento','Cda.Colima','S/N','México Nuevo','Atizapán de Zaragoza','Estado de México','52966',0,0,'Omar Trejo','(55) 55-11-28-34-21',0.0,0.0,115.0,2400000,NULL,20869.57,0,'15- 02-2016',NULL,0,NULL,NULL,17.8561,'2016-07-21 12:30:36','2016-07-13 19:58:16','2016-07-21 12:30:36','CRS','octavio.gonzalez@am.jll.com'),(3735,'3735/comp_13870.JPG','Departamento','Cumbres de Maltrata','S/N','Narvarte','Benito Juárez','Ciudad de México','03020',0,0,'Grupo Vía Inmuebles','(01) 55-62-64-90-78',0.0,0.0,74.0,2500000,NULL,33783.78,0,'03-07-2015',NULL,0,NULL,NULL,15.6918,'2016-08-05 13:02:40','2016-08-05 13:02:40','0000-00-00 00:00:00','CRS','octavio.gonzalez@am.jll.com'),(3743,'3743/comp_43828.JPG','Nave Industrial','Norte 45','669','Industrial Vallejo','Azcapotzalco','Ciudad de México','2300',0,0,'Anamaria Dueñas Oliveros Remax Grand','55-52-53-23-95',37199.5,400412.1,13300.0,1311388.23,0,98.6,0,'3-6 meses',' Are three industrial warehouse, offices, tilt-up walls, parking area, cistern.',1,NULL,'01',17.7866,'2016-08-19 18:07:15','2016-08-19 18:07:15','0000-00-00 00:00:00','CRL','octavio.gonzalez@am.jll.com'),(3779,'3779/comp_33991.JPG','Casa','Mariano Matamoros','S/N','Miguel Hidalgo','Cuautla','Estado de Morelos','62748',0,0,'M Inmobiliaria','73-53-03-16-34',840.0,9041.7,330.0,17400,0,52.73,0,'05-03-2016',' Casa habitación con alberca, en dos niveles, cuenta con: sala, cocina, comedor a doble altura con cúpula catalana, estudio, oratorio, ajardín, área de lavado, cuarto de blancos, estacionamiento para 5 autos.',1,'2016-03-05','01',17.7723,'2016-09-26 19:59:37','2016-09-26 19:59:37','0000-00-00 00:00:00','CRL','octavio.gonzalez@am.jll.com'),(3849,'3849/comp_63908.JPG','Casa','Melitón Velázquez','230','Ex Hacienda Santa Inés','Nextlalpán','Estado de México','55796',0,0,'Sr. Jesús Uribe','80-00-22-05-81',60.0,645.8,59.0,538645,0,9129.58,0,'23-05-2016',' Condominio horizontal formado por 54 casas, La vivienda se desarrolla en dos niveles y cuenta con sala, comedor, cocina, área de lavado, 3 recámaras, 3.5 baños y dos cajones de estacionamiento.',1,NULL,'01',18.4444,'2016-09-22 13:33:14','2016-09-22 13:33:14','0000-00-00 00:00:00','CRS','octavio.gonzalez@am.jll.com'),(3853,'3853/comp_63318.JPG','Nave Industrial','Autopista México-Queretaro  Km 42.5 Nave 10','S/N','Cedros','Tepotzotlán','Estado de México','54600',0,0,'JLL Database','55-59-80-88-50',0.0,0.0,11304.0,976931.51,0,86.42,0,'10-06-2016',' Industrial warehouse with insulated panel offices, mezzanine,  Average Building Height: 14 meter, with subestation, natural gas, 40,000 lts water cistern',2,NULL,'01',17.8561,'2016-08-25 18:19:30','2016-08-25 13:32:22','2016-08-25 18:19:30','CRL','octavio.gonzalez@am.jll.com'),(3916,'3916/comp_45861.jpg','Land','Periferico','11','San Pablo Autopan','Palmillas','Estado de Mexico','122344',0,0,'Century 21 Vilchis / Hugo Hernández','(01) 72-22-77-10',7000.0,75347.4,0.0,428927.4,27672.74,61.28,0,'3Q2013',NULL,0,NULL,NULL,15.5,'2016-08-01 11:49:16','2016-03-08 06:13:45','2016-08-01 11:49:16','CLS','octavio.gonzalez@am.jll.com'),(3926,'3926/comp_73978.JPG','Terreno','Jalapa','S/N','Tenopalco','Melchor Ocampo','Estado de México','54882',0,0,'Rosa María Razo','53-60-61-31-',141537.0,1523491.6,0.0,56615144,0,400,0,'23-05-2016',' Terreno plano con  uso de suelo autorizado  h100 en la calle Jalapa, intensidad máxima de construcción del 70% de la superficie del predio, tiene una altura máxima de tres niveles o 10 metros.',1,NULL,'',18.4444,'2016-09-21 12:23:53','2016-09-21 12:23:53','0000-00-00 00:00:00','CLS','octavio.gonzalez@am.jll.com'),(3944,'3944/comp_54107.JPG','Casa','Amado Nervo','37','La Florida','Cerro de San Pedro','San Luis Potosí','78448',0,0,'Habita forte Inmo','(01) 44-41-29-73-72',700.0,7534.7,350.0,3150000,NULL,9000,0,'17-03-2016',NULL,0,NULL,NULL,17.8561,'2016-07-19 15:00:13','2016-07-19 15:00:13','0000-00-00 00:00:00','CRS','octavio.gonzalez@am.jll.com'),(4000,'4000/comp_94070.JPG','Terreno','Av. del Club','S/N','Bosque Real','Huixquilucan','Estado de México','52774',0,0,'Corporativo Inmobiliario','55-51-09-08-26',9309.0,100201.2,0.0,74472000,0,8000,0,'10-03-2016',' Se vende excelente macrolote para hacer 23 casas, terreno plano con vistas a campo , aproveche, único macrolote en todo bosque Real para condominio Horizontal  casas ',1,NULL,'01',19.6,'2016-09-05 12:54:04','2016-09-05 12:54:04','0000-00-00 00:00:00','CLS','octavio.gonzalez@am.jll.com'),(4012,'4012/comp_64063.JPG','Terreno','Av. Centro Industrial','S/N','Zona Industrial Los Reyes Sección 1','Tlalnepantla','Estado de México','54098',0,0,'Ampsa Esmeralda','55-53-08-22-57',12000.0,129166.9,0.0,79200000,0,6600,0,'12-18 meses',' Terreno industrial bardeado con dos frentes, subdividido en 4 lotes.',1,NULL,'01',17.7866,'2016-08-18 20:00:42','2016-08-18 20:00:42','0000-00-00 00:00:00','CLS','octavio.gonzalez@am.jll.com'),(4032,'4032/comp_34051.JPG','Local Comecial','Ejercito Nacional','S/N','Polanco','Miguel Hidalgo','Ciudad de México','11560',0,0,'Tar Internacional S.C.','(01) 55-53-50-30-50',0.0,0.0,147.0,88200,NULL,600,0,'01-14-2016',NULL,0,NULL,NULL,17.8561,'2016-08-03 17:22:30','2016-07-20 13:30:05','2016-08-03 17:22:30','CRL','octavio.gonzalez@am.jll.com'),(4052,'4052/comp_17038.JPG','Casa','Nezahualcóyotl ','21','Las Alamedas','Atizapan de Zaragoza','Estado de México','52970',0,0,'Ana Moreno (Zuribi)','(01) 55-68-19-92-92',140.0,1507.0,166.0,3600000,NULL,21686.75,0,'15- 02-2016',NULL,0,NULL,NULL,17.8561,'2016-07-21 12:04:07','2016-07-11 15:20:31','2016-07-21 12:04:07','CRS','octavio.gonzalez@am.jll.com'),(4067,'4067/comp_64302.JPG','Local Comercial','Francisco Javier Gamboa entre López Cotilla y Av La Paz','S/N','Arcos Vallarta','Guadalajara','Jalisco','44160',0,0,'Urban Properties Guadalajara ','33-35-70-86-75',0.0,0.0,90.0,22000,0,244.44,0,'10-03-2016',' Se ubica a dos cuadras de Centro Magno, tiene una antiguedad de 3 años, 2 baños y 4 cajones de estacionamiento.',1,NULL,'',19.5997,'2016-09-09 19:49:27','2016-09-09 19:49:27','0000-00-00 00:00:00','CRL','octavio.gonzalez@am.jll.com'),(4186,'4186/comp_84258.JPG','Terreno','Lago Buenos Aires equina Lago Marcaibo','34','Argentina Antigua','Miguel Hidalgo','Ciudad de México','11270',0,0,'Irma Rodríguez','43-36-89-94',180.0,1937.5,0.0,2750000,0,15277.78,0,'',' Terreno en esquina, plano con uso de suelo habitacional  H 3 30 M ',1,NULL,'',18.3748,'2016-08-31 19:10:43','2016-08-31 19:10:43','0000-00-00 00:00:00','CLS','octavio.gonzalez@am.jll.com'),(4203,'4203/comp_64265.JPG','Terreno','Asistencia Pública','481','Federal','Venustiano Carranza','Ciudad de México','15700',0,0,'Jica Corporativo Inmobiliario','(01) 55-56-45-35-86',5105.5,54955.1,0.0,90000000,NULL,17628.05,0,'24-05-2016',NULL,0,NULL,NULL,18.3826,'2016-08-03 18:51:10','2016-08-03 18:51:10','0000-00-00 00:00:00','CLS','octavio.gonzalez@am.jll.com'),(4227,'4227/comp_64445.JPG','Local Comercial','Insurgentes','1548','Crédito Constructor','Benito Juárez','Ciudad de México','3940',0,0,'Quality Triple A , Pilar López','55-15-52-45-59',0.0,0.0,448.0,150000,0,334.82,0,'',' Local comercial ubicado sobre Insurgentes Sur cerca del Teatro Insurgentes, gran afluencia vehicular y peatonal, cuenta con 4 lugares de estacionamiento. Se puede rentar todo o 206m2 en 80,000 ó 201m2 en 70,000.',1,NULL,'01',15.6918,'2016-08-09 13:58:54','2016-08-09 13:58:54','0000-00-00 00:00:00','CRL','octavio.gonzalez@am.jll.com'),(4259,'4259/comp_84303.JPG','Terreno','Lago Azul','S/N','Bosque Esmeralda','Atizapán de Zaragoza','Estado de México','52930',0,0,'Miriam González','63-10-29-69-',120.0,1291.7,0.0,1920000,0,16000,0,'23-03-2016',' Lote de terreno en condominio.',1,NULL,'',19.8925,'2016-09-20 11:25:16','2016-09-20 11:25:16','0000-00-00 00:00:00','CLS','octavio.gonzalez@am.jll.com'),(4306,'4306/comp_82797.jpg','Industrial Bay A.3','Av. Industria Automotriz','s/n','Prologis Park Toluca','Toluca','Estado de Mexico','1234',0,0,'JLL Database','(01) 55-59-80-88',0.0,0.0,11304.0,976931.52,103294.8,86.42,0,'1Q2015',NULL,0,NULL,NULL,17.8561,'2016-07-19 03:15:28','2016-03-17 20:55:48','2016-07-19 03:15:28','CRL','eduardo.ms@senni.com.mx'),(4358,'4358/comp_74445.JPG','Nave Industrial','México','12','Huitzila','Tizayuca','Hidalgo','43820',0,0,'Manuel Baños','77-17-09-12-19',20000.0,215278.2,5000.0,119996.42,0,24,0,'',' Industrial bay of 28,693 m2, with office area and manuever yard,  average height 10 to 13 m.',1,NULL,'045',17.1767,'2016-08-17 19:09:32','2016-08-17 19:09:32','0000-00-00 00:00:00','CRL','octavio.gonzalez@am.jll.com'),(4392,'4392/comp_54534.JPG','Departamentos','Cumbres de Maltrata','S/N','Narvarte','Benito Juárez','Ciudad de México','3020',0,0,'Infinity Bienes Raíces','55-47-37-16-35',0.0,0.0,64.0,2200000,0,34375,0,'','Se encuentra en 1er. piso, con jardín privado de 9 m2, los amenities serán sky lounge, snack bar, putting green, futbol, basquetbol, pista de jogging, roof top, cinema, bar, sky park, gimnasio, área de yoga, jacuzzi, alberca, biblioteca, cafetería, kids club, spa, business center, área de picnic, juegos infantiles y salón de eventos.',1,NULL,'01',15.6918,'2016-08-09 11:32:03','2016-08-09 11:21:55','2016-08-09 11:32:03','CRS','octavio.gonzalez@am.jll.com'),(4436,'4436/comp_14476.JPG','Terreno','Sierra Mojada','S/N','Lomas de Chapultepec','Miguel Hidalgo','Ciudad de México','11000',0,0,'Conserje','59-89-22-41',435.0,4682.3,0.0,17897100,0,41142.76,0,'24-36 meses',' Property with constructions sold as vacant land, flat irregular-shaped.',1,NULL,'01',17.8971,'2016-08-12 11:27:47','2016-08-12 11:27:47','0000-00-00 00:00:00','CLS','octavio.gonzalez@am.jll.com'),(4466,'4466/comp_44619.JPG','Casa','Av. León Felipe','S/N','Bosque de la Florida','Cerro de San Pedro','San Luis Potosí','78440',0,0,'Habita forte Inmo','(01) 44-41-29-73-72',1100.0,11840.3,300.0,2950000,NULL,9833.33,0,'17-03-2016',NULL,0,NULL,NULL,17.8561,'2016-08-03 15:11:22','2016-07-19 12:24:01','2016-08-03 15:11:22','CRS','octavio.gonzalez@am.jll.com'),(4581,'4581/comp_76615.JPG','Casa','Héroe de la Indep Pedro Ascencio Moreno','S/N','Miguel Hidalgo','Cuautla','De Morelos','62748',0,0,'Reyna Torres','(01) 73-53-98-74-95',331.0,3562.8,258.0,2204000,NULL,8542.64,0,'07-03-2016',NULL,0,NULL,NULL,17.7723,'2016-08-02 18:37:39','2016-08-02 18:36:38','2016-08-02 18:37:39','CRS','octavio.gonzalez@am.jll.com'),(4599,'4599/comp_54717.JPG','Terreno','Oriente-Poniente Esquina. Sur 6','S/N','Ciudad Industrial','Tizayuca','Hidalgo','43800',0,0,'Sra. Hinojosa','77-12-28-90-28',12280.0,132180.8,1960.0,23500000,0,1913.68,0,'24-36 meses',' Land with a construction of 1,480, sold as vacant land.',1,NULL,'',17.1767,'2016-08-17 14:06:19','2016-08-17 14:06:19','0000-00-00 00:00:00','CLS','octavio.gonzalez@am.jll.com'),(4608,'4608/comp_44746.jpg','Industrial Bay A.3','Av. De la industria Automotriz','Km. 53.5','Industrial Park Doña Rosa','Toluca','Estado de Mexico','23',0,0,'Jll Database','(01) 55-59-80-88',0.0,0.0,6000.0,514255.68,16270.97,85.71,0,'1Q2015',NULL,0,NULL,NULL,17.8561,'2016-07-19 03:16:38','2016-03-17 21:01:52','2016-07-19 03:16:38','CRL','eduardo.ms@senni.com.mx'),(4649,'4649/comp_94786.JPG','Departamento','Av. la Paz','1766','Americana','Guadalajara','Jalisco','44600',0,0,'Grupo Inmobiliario Alcance','36-15-78-89-',0.0,0.0,108.0,3582977,0,33175.71,0,'10-03-2016',' Departamento en el piso 6. Conjunto con alberca, baños, terraza, 2 elevadores, 1 montacarga, 8 niveles, 45 departamentos, 3 locales comerciales, estacionamientos para visitas; aire acondicionado en todos los departamentos, doble cristal en las ventanas.',1,NULL,'',19.5997,'2016-09-12 11:57:54','2016-09-12 11:57:54','0000-00-00 00:00:00','CRS','octavio.gonzalez@am.jll.com'),(4712,'4712/comp_64766.JPG','Terreno','Blvd. Puerto Aéreo','34','Federal','Venustiano Carranza','Ciudad de México','15540',0,0,'Realty World Koinox','(01) 81-81-74-70-02',12944.5,139333.4,0.0,174750750,NULL,13500,0,'24-05-2016',NULL,0,NULL,NULL,18.3826,'2016-08-03 19:04:42','2016-08-03 19:04:42','0000-00-00 00:00:00','CLS','octavio.gonzalez@am.jll.com'),(4714,'4714/comp_17414.JPG','Terreno','Blvd Valle Escondido','S/N','El Capulin','Atizapán de zaragoza','Estado de México','52915',0,0,'Isabel Campos','(01) 55-55-60-10-27',150000.0,1614586.6,0.0,600000000,NULL,4000,0,'15- 02-2016',NULL,0,NULL,NULL,17.8561,'2016-07-21 12:10:25','2016-07-13 14:35:08','2016-07-21 12:10:25','CLS','octavio.gonzalez@am.jll.com'),(4737,'4737/comp_34811.JPG','Casa','Fracc. Los Sabinos','S/N','Los Sabinos','Cuautla','Estado de Morelos','62747',0,0,'Reyna Torres','73-51-36-96-34',120.0,1291.7,120.0,10000,0,83.33,0,'05-03-2016',' La renta incluye mantenimiento del fraccionamiento (vigilancia, recolección de basura y limpieza de áreas comunes), además de poda del jardín interior. Casa habitación que cuenta con sala, comedor, cocina integral, terraza, estacionamiento para un auto, 2 recámaras, 1.5 baños. Se renta amueblada.',1,'2016-03-05','045',17.7723,'2016-09-26 20:14:52','2016-09-26 20:14:52','0000-00-00 00:00:00','CRL','octavio.gonzalez@am.jll.com'),(4756,'4756/comp_54832.JPG','Departamento','Felipe Carrillo Puerto ','S/N','Legaria','Miguel Hidalgo','Ciudad de México','11410',0,0,'Inmobiliaria Casa Bonilla Angelica Gámez','46-22-74-30',0.0,0.0,55.0,1248000,0,22690.91,0,'15- 02-2016','Edificio con 13 deptos,departamento exterior 1er nivel,cuenta con sala, comedor, cocina abierta, 2 recamaras y 1 baño con ventilación natural,patio de cervicio. Además caseta de vigilancia con circuito cerrado y vigilancia las 24 Hrs,portón eléctrico, no incluye estacionamiento. el costo del estacionamiento desde  98.000.pesos. Descuento si se entrega sin acabados 150.000.pesos ',1,NULL,'',18.3748,'2016-09-01 18:14:51','2016-09-01 14:57:48','2016-09-01 18:14:51','CRS','octavio.gonzalez@am.jll.com'),(4878,'4878/comp_45022.JPG','Casa','Paseo de la Reforma','S/N','Lomas de Chapultepec','Miguel Hidalgo','Ciudad de México','11000',0,0,'SS Propiedades','(01) 55-18-01-92-42',390.0,4197.9,420.0,90000,NULL,214.29,0,'10-02-2016',NULL,0,NULL,NULL,17.8561,'2016-08-03 17:23:27','2016-07-27 15:14:03','2016-08-03 17:23:27','CRL','octavio.gonzalez@am.jll.com'),(4900,'4900/comp_14943.JPG','Terreno','Poniente 4 esquina con Norte 9','S/N','Santa Rita','Celaya','Guanajuato','38010',0,0,'Miguel Angel Carreon','46-11-69-81-34',5315.0,57210.2,0.0,645951,0,121.53,0,'01-03-2016','  Industrial use and services',1,'2016-03-01','01',18.102,'2016-09-27 18:44:49','2016-09-27 18:44:49','0000-00-00 00:00:00','CLS','octavio.gonzalez@am.jll.com'),(4923,'4923/comp_82778.jpg','Industrial A.3','Isidro Fabela','126','San Blas Otzacatipan','Toluca','Estado de Mexico','1213',0,0,'JLL Database','(01) 55-59-80-88',0.0,0.0,21562.0,1617150,1159.67,75,0,'1Q2015',NULL,0,NULL,NULL,17.8561,'2016-07-19 03:17:37','2016-03-17 21:05:53','2016-07-19 03:17:37','CRL','eduardo.ms@senni.com.mx'),(4936,'4936/comp_24988.JPG','Nave Industrial','Acatl','322','Industrial San Antonio','Azcapotzalco','Ciudad de México','2300',0,0,'JLL Database','55-59-80-88-41',0.0,0.0,11183.0,1227257.61,0,109.74,0,'10-02-2016',' Industrial Bay whit 11,183 m, included offices, manuever yard,  average height 8 to 10 m.',2,NULL,'01',17.7866,'2016-08-19 12:57:02','2016-08-19 12:57:02','0000-00-00 00:00:00','CRL','octavio.gonzalez@am.jll.com'),(4977,'4977/comp_65026.JPG','Local Comercial','Av. Hidalgo','S/N','Americana','Guadalajara','Jalisco','44160',0,0,'Success Grupo Inmobiliario','33-36-28-84-33',0.0,0.0,45.0,13000,0,288.89,0,'10-03-2016',' Fachada de cristal templado, con terraza al frente, terminados de ladrillo y concreto aparente, piso de concreto pulido, barra, tarja, mezannine y 1 baño. 45 m2 de terreno. A 2 cuadras de Av. Chapultepec, cerca de Av. México, Av. Unión, Av. Vallarta.',1,NULL,'',19.5997,'2016-09-09 19:55:42','2016-09-09 19:55:42','0000-00-00 00:00:00','CRL','octavio.gonzalez@am.jll.com'),(5005,'5005/comp_65017.JPG','Casa','Av. Galdiolas','S/N','La Florida','Cerro de San Pedro','San Luis Potosí','78448',0,0,'Habita forte Inmo','(01) 44-41-29-73-72',770.0,8288.2,650.0,5200000,NULL,8000,0,'17-03-2016',NULL,0,NULL,NULL,17.8561,'2016-07-19 17:57:47','2016-07-19 17:57:47','0000-00-00 00:00:00','CRS','octavio.gonzalez@am.jll.com'),(5034,'5034/comp_75096.JPG','Terreno','Av.México Japon esquina con San José Iturbide','S/N','Guanajuato','Celaya','Guanajuato','38010',0,0,'Laura Juarez Bienes Raices','46-11-20-46-17',3600.0,38750.1,0.0,10799997.14,0,3000,0,'01-03-2016',' Near Ciudad Industrial',1,'2016-03-01','045',18.102,'2016-09-28 11:36:41','2016-09-28 11:36:41','0000-00-00 00:00:00','CLS','octavio.gonzalez@am.jll.com'),(5052,'5052/comp_75124.JPG','Terreno','Jalapa','S/N','Tenopalco','Melchor Ocampo','Estado de México','54882',0,0,'Carmen Loera','49-91-18-69-',140000.0,1506947.5,0.0,70800000,0,505.71,0,'23-05-2016','  Uso de suelo Industrial y H-100  Intensidad 2.1 veces la superficie del terreno; Altura permitida 3 niveles o 10 m.',1,NULL,'',18.4444,'2016-09-21 12:34:33','2016-09-21 12:34:33','0000-00-00 00:00:00','CLS','octavio.gonzalez@am.jll.com'),(5061,'5061/comp_21096.JPG','Terreno','Camino a Portezuelo','S/N','Los Gómez','Cerro de San Pedro','San Luis Potosi','78347',0,0,'Roberto Hernández','(45) 44-41-53-05-82',13000.0,139930.8,0.0,5000000,NULL,384.62,0,'17-03-2016',NULL,0,NULL,NULL,17.8561,'2016-07-21 13:11:48','2016-07-18 11:30:42','2016-07-21 13:11:48','CLS','octavio.gonzalez@am.jll.com'),(5080,'5080/comp_85206.JPG','Terreno','Lago Ginebra','147','Pensil Sur','Miguel Hidalgo','Ciudad de México','11490',0,0,'Inmobiliaria  IMB','22-81-86-82-37',225.0,2421.9,0.0,4500000,0,20000,0,'15- 02-2016',' Uso de suelo HC 3 30 M, se vende como terreno, tiene una construcción de 81 m2. Cuenta con 7.5 m de frente y 30  m de fondo.',1,NULL,'01',18.3748,'2016-08-31 19:28:19','2016-08-31 19:28:19','0000-00-00 00:00:00','CLS','octavio.gonzalez@am.jll.com'),(5136,'5136/comp_85177.JPG','Terreno','Puerta de Quiroga','S/N','Bosque Esmeralda','Atizapán de Zaragoza','Estado de México','52930',0,0,'Luis Esquivel','55-46-04-61-56',250.0,2691.0,0.0,2760000,0,11040,0,'23-03-2016',' Lote de terreno en zona habitacional.dentro de un condominio.',1,NULL,'044',19.8925,'2016-09-20 11:33:03','2016-09-20 11:33:03','0000-00-00 00:00:00','CLS','octavio.gonzalez@am.jll.com'),(5142,'5142/comp_55174.JPG','Terreno','Circuito Univercidades','S/N','Zibatá','El Marqués','Querétaro','76269',0,0,'Suma Bienes Raices','44-24-04-12-68',139.6,1502.6,0.0,588,0,4.21,0,'16-03-2016','Terreno plano de configuracion regular con terreno excedente, en esquina, cuenta con franja de area verde al frente.',1,NULL,'01',19.8925,'2016-09-15 12:04:12','2016-09-15 12:04:12','0000-00-00 00:00:00','CLS','octavio.gonzalez@am.jll.com'),(5166,'5166/comp_45343.jpg','Industrial Bay A.3','Av. Industria Automotriz ','s/n','Prologis Park Toluca','Toluca','Estado de Mexico','123',0,0,'JLL Database','(01) 55-59-80-88-41',0.0,0.0,15000.0,1124934.3,1389.17,75,0,'1Q2015',NULL,0,NULL,NULL,17.8561,'2016-07-19 03:19:59','2016-03-17 21:08:37','2016-07-19 03:19:59','CRL','eduardo.ms@senni.com.mx'),(5194,'5194/comp_15340.JPG','Departamento','Av. de las Plazas','S/N','Bosque Real','Huixquilucan','Estado de México','52774',0,0,'VIJADI Bienes Raices','55-53-07-56-0',0.0,0.0,148.0,4100000,0,27702.7,0,'10-03-2016',' Modelo B con 3 recámaras, 3 baños, 2 cajones de estacionamiento, Casa Club, 58,000 m. de terreno, extensas áreas verdes, cuarto de servicio o desayunador, cancha de tenis, alberca techada, terraza con snack, alberca infantil, gimnasio, salón de fiestas, vestidores, vapor, 2 lugares de estacionamiento, bodega y tratamiento de aguas',1,NULL,'01',19.6,'2016-09-05 19:01:01','2016-09-05 19:01:01','0000-00-00 00:00:00','CRS','octavio.gonzalez@am.jll.com'),(5356,'5356/comp_45402.jpg','Industrial Bay A.3','Parque Industrial Toluca','s/n','Parque Industrial Toluca','Toluca','Estado de Mexico','1213',0,0,'Bufete Logistico Inmobiliario','(01) 55-85-90-80-00',0.0,0.0,8000.0,27200,1753.71,3.4,3.79,'6- 12 months',NULL,0,NULL,NULL,15.51,'2016-03-22 22:38:47','2016-03-17 21:11:38','2016-03-22 22:38:47','CRL','alfredo.giorgana@am.jll.com'),(5373,'5373/comp_65412.JPG','Local Comercial','José Guadalupe Monte Negro','S/N','Americana','Guadalajara','Jalisco','44160',0,0,'Inmobili Bienes Raíces','33-30-70-05-83',0.0,0.0,81.0,31176,0,384.89,0,'10-03-2016',' Local en planta baja en edificio nuevo, altura al techo de 4 metros aprox.y un patio de 84.2 m2, cuenta con dos estacionamientos con bodega subterráneos con portón eléctrico, sistema de alarma, circuito cerrado con cámaras internas y externas. mantenimiento ya incluido.',1,NULL,'',19.5997,'2016-09-09 20:00:57','2016-09-09 20:00:57','0000-00-00 00:00:00','CRL','octavio.gonzalez@am.jll.com'),(5440,'5440/comp_45673.JPG','Casa','Gral Juaquin Amaro','S/N','Real Toscana','Tecámac','Estado de México','',0,0,'Srita. Judith Jiménez','50-10-73-56-',90.0,968.8,77.0,959000,0,12454.55,0,'23-05-2016',' Casa en condominio, cuenta con sala, comedor, cocina, 1/2 baño, área de lavado, jardín, 3 recámaras, 1.5 baños y un cajón de estacionamiento.',1,'2016-05-23','',18.4444,'2016-09-23 12:14:43','2016-09-23 12:14:43','0000-00-00 00:00:00','CRS','octavio.gonzalez@am.jll.com'),(5449,'5449/comp_25845.JPG','Casa','Guadalupe Victoria','1','Guadalupe Victoria','Cuautla','Estado de Morelos','62746',0,0,'Josefina Castillo','73-51-02-45-40',140.0,1507.0,150.0,8700,0,58,0,'05-03-2016',' 3 habitaciones, una de ellas en planta baja, 2 baños completos, 2 terrazas, cocina equipada, áreas verdes y alberca compartidas, patio de lavado, estacionamiento para un auto.',1,'2016-03-05','01',17.7723,'2016-09-26 18:32:47','2016-09-26 18:32:47','0000-00-00 00:00:00','CRL','octavio.gonzalez@am.jll.com'),(5451,'5451/comp_71379.JPG','Departamento','Emerson','S/N','Polanco','Miguel Hidalgo','Ciudad de México','11560',0,0,'Miroslava Castrejón','(01) 55-55-20-40-00',0.0,0.0,180.0,7500000,NULL,41666.67,0,'14-01-2016',NULL,0,NULL,NULL,17.8561,'2016-08-03 17:44:19','2016-07-25 13:18:03','2016-08-03 17:44:19','CRS','octavio.gonzalez@am.jll.com'),(5462,'5462/comp_55509.JPG','Terreno','Circuito Univercidades','S/N','Zibatá','El Marqués','Querétaro','76269',0,0,'Cuatro Inmobiliario','44-21-57-73-19',190.0,2045.1,0.0,1000000,0,5263.16,0,'16-03-2016',' En Zibatá Condominio Opuntia. Superficie de 190 m2 y ya cuenta con terracería y muros de mampostería. Escriturado y con deslinde pagado',1,NULL,'01',19.8925,'2016-09-15 12:08:32','2016-09-15 12:08:32','0000-00-00 00:00:00','CLS','octavio.gonzalez@am.jll.com'),(5488,'5488/comp_45545.JPG','Casa','Claveles','S/N','La Florida','Cerro de San Pedro','San Luis Potosí','78448',0,0,'Habita forte Inmo','(01) 44-41-29-73-72',1270.0,13670.2,514.0,4200000,NULL,8171.21,0,'17-03-2016',NULL,0,NULL,NULL,17.8561,'2016-07-19 15:03:20','2016-07-19 12:57:56','2016-07-19 15:03:20','CRS','octavio.gonzalez@am.jll.com'),(5517,'5517/comp_95602.JPG','Departamento','Carretera México Huixquilucan','S/N','Ex Ejidos de San Cristóbal','Huixquilucan','Estado de México','52770',0,0,'VIJADI Bienes Raíces','55-53-30-75-60',0.0,0.0,99.0,3700000,0,37373.74,0,'10-03-2016',':Piso 16, tres recámaras, sala comedor,  gym, vapor y regaderas, asoleadero, area de asador, planta de emergencia, planta de tratamiento de agua, internet inalambrico, caseta de vigilancia, casa club y áreas verdes, dos cajones de estacionamiento, en obra blanca. 5 por ciento  de descuento en pago de contado.',1,NULL,'01',19.6,'2016-09-05 13:48:38','2016-09-05 13:48:38','0000-00-00 00:00:00','CRS','octavio.gonzalez@am.jll.com'),(5526,'5526/comp_75639.JPG','Nave Industrial','Sur 2','S/N','Ciudad Industrial','Tizayuca','Hidalgo','43800',0,0,'JLL Database','55-59-80-88-41',1400.0,15069.5,1200.0,81194.26,0,67.66,0,'',' Industrial shed of 1,200m, with office area.',1,NULL,'01',17.1767,'2016-08-17 19:25:02','2016-08-17 19:25:02','0000-00-00 00:00:00','CRL','octavio.gonzalez@am.jll.com'),(5597,'5597/comp_85642.JPG','Terreno','Puerta de Valladolid','S/N','Bosque Esmeralda','Atizapán de Zaragoza','Estado de México','52930',0,0,'Sr Julio Cesar','55-51-03-30-06',485.0,5220.5,0.0,4800000,0,9896.91,0,'23-03-2016',' Terreno plano, forma irregular dentro de un condominio',1,NULL,'044',19.8925,'2016-09-20 11:45:09','2016-09-20 11:45:09','0000-00-00 00:00:00','CLS','octavio.gonzalez@am.jll.com'),(5651,'5651/comp_5689.JPG','Land','San Nicolas Esq. Jose Angel','','','San Nicolas de los Garza','Nuevo Leon','',0,0,'Eduardo Padilla','81-14-66-18-52',23000.0,247569.9,0.0,64177590.75,0,2790.33,0,'','',1,NULL,'044',18.6022,'2016-09-09 12:04:17','2016-09-07 23:36:17','2016-09-09 12:04:17','CLS','daniel.algazi@am.jll.com'),(5678,'5678/comp_69256.JPG','Departamento','Aristóteles','S/N','Polanco','Miguel Hidalgo','Ciudad de México','11550',0,0,'Mexi Hom','(01) 55-15-20-20-40',0.0,0.0,178.0,14150000,NULL,79494.38,0,'14-01-2016',NULL,0,NULL,NULL,17.8561,'2016-08-03 17:30:39','2016-07-19 18:56:40','2016-08-03 17:30:39','CRS','octavio.gonzalez@am.jll.com'),(5682,'5682/comp_65713.JPG','Terreno','Paseo de las Buganbilias','S/N','San Martin','Tepotzotlán','Estado de México','54600',0,0,'BAI Arquitectos Inmuebles','55-53-63-40-48',34700.0,373507.7,0.0,52066494.85,0,1500.48,0,'03-03-2016',' 3 land lots, 2 of 10,333 m2 and 1 of 14,045 m2.',1,NULL,'01',17.8561,'2016-08-24 15:21:14','2016-08-24 15:21:14','0000-00-00 00:00:00','CLS','octavio.gonzalez@am.jll.com'),(5716,'5716/comp_55824.JPG','Casa','Sierra Ventana','S/N','Lomas de Chapultepec','Miguel Hidalgo','Ciudad de México','11000',0,0,'REMAX','(01) 55-52-92-32-93',800.0,8611.1,450.0,109783.5,NULL,243.96,0,'10-02-2016',NULL,0,NULL,NULL,17.8561,'2016-08-03 17:24:26','2016-07-27 17:48:06','2016-08-03 17:24:26','CRL','octavio.gonzalez@am.jll.com'),(5722,'5722/comp_55757.JPG','Terreno','Circuito Univercidades','S/N','Conjunto Opuntia','El Marqués','Querétaro','76269',0,0,'Georgina Cueto','44-23-63-90-27',148.0,1593.1,0.0,670000,0,4527.03,0,'16-03-2016','Terreno de 148 m2',1,NULL,'045',19.8925,'2016-09-15 12:47:28','2016-09-15 12:47:28','0000-00-00 00:00:00','CLS','octavio.gonzalez@am.jll.com'),(5725,'5725/comp_15797.JPG','Terreno','Sierra Nevada esquina con Sierra Madre','S/N','Lomas de Chapultepec','Miguel Hidalgo','Ciudad de México','11000',0,0,'Gama 6','52-02-03-37',2755.0,29654.6,1450.0,134228250,0,48721.69,0,'24-36 meses',' Property with constructions sold as vacant land, flat irregular-shaped lot with two fronts.',1,NULL,'01',17.8971,'2016-08-12 11:44:08','2016-08-12 11:44:08','0000-00-00 00:00:00','CLS','octavio.gonzalez@am.jll.com'),(5792,'5792/comp_5819.JPG','Land','Diego Díaz de Berlana ','1000','','San Nicolas de los Garza','Nuevo Leon','',0,0,'Realty World Inco Valle','82-20-84-80-',25215.0,271412.0,0.0,115000000,0,4560.78,0,'','',1,NULL,'',18.6022,'2016-09-07 23:41:06','2016-09-07 23:41:06','0000-00-00 00:00:00','CLS','daniel.algazi@am.jll.com'),(5809,'5809/comp_66117.JPG','Nave Industrial','Poniente 116','509','Industrial Vallejo','Azcapotzalco','Ciudad de México','2300',0,0,'JLL Database','55-59-80-81-49',18577.5,199966.5,14616.0,245239097.3,0,16778.81,0,'10-11- 2015',' Warehouse in industrial complex, average height 9 mts, loading and unloading area and maneuvering yard.',2,NULL,'01',19.8925,'2016-09-13 13:03:04','2016-08-23 11:48:38','0000-00-00 00:00:00','CRS','octavio.gonzalez@am.jll.com'),(5915,'5915/comp_35971.JPG','Casa','Sierra Paracaima','S/N','Lomas de Chapultepec','Miguel Hidalgo','Ciudad de México','11000',0,0,'Sra. Ana Laura Nuñez','(01) 55-85-34-52-65',939.0,10107.3,937.0,63415394,NULL,67679.18,0,'10-02-2016',NULL,0,NULL,NULL,17.8561,'2016-08-03 17:46:32','2016-07-27 12:25:08','2016-08-03 17:46:32','CRS','octavio.gonzalez@am.jll.com'),(5964,'5964/comp_21147.JPG','Terreno','Arbolitos','S/N','Santa Rita','Villa de Pozos','San Luis Potosi','78421',0,0,'Fernando Vega','(01) 44-43-51-77-11',30000.0,322917.3,0.0,6750000,NULL,225,0,'17-03-2016',NULL,0,NULL,NULL,17.8561,'2016-07-21 13:12:35','2016-07-18 11:50:40','2016-07-21 13:12:35','CLS','octavio.gonzalez@am.jll.com'),(6011,'6011/comp_86108.JPG','Departamento','Prolongacion Paseo de la Reforma','S/N','Santa Fé','Alvaro Obregón','Ciudad de México','01376',0,0,'Promotores Inmobiliarios','55-36-21-70-21',0.0,0.0,97.0,4373000,0,45082.47,0,'07-03-2016',' Penthouse, con terraza privada, bodega, Gym, vapor y regaderas, plaza central, guarderia, juegos infantiles, chapoteadero, ludoteca,salón de fiestas, áreas verdes, área comercial, roof garden y vistas panoramicas.',1,NULL,'01',19.4409,'2016-09-06 15:00:32','2016-09-06 15:00:32','0000-00-00 00:00:00','CRS','octavio.gonzalez@am.jll.com'),(6109,'6109/comp_17303.JPG','Terreno','Bosque Esmeralda','S/N','Bosque Esmeralda','Atizapán de zaragoza','Estado de México','52930',0,0,'Salvador Jiménez','(01) 55-53-70-36-00',26008.0,279947.8,0.0,80000000,NULL,3075.98,0,'15- 02-2016',NULL,0,NULL,NULL,17.8561,'2016-07-21 12:08:26','2016-07-13 12:16:40','2016-07-21 12:08:26','CLS','octavio.gonzalez@am.jll.com'),(6115,'6115/comp_36164.JPG','Terreno','Rosedal','69','Lomas de Chapultepec','Miguel Hidalgo','Ciudad de México','11000',0,0,'Cristal Zepeda','63-95-31-34',383.0,4122.6,0.0,23000000,0,60052.22,0,'12-24 meses',' Property with constructions sold as vacant land, flat regular-shaped lot with one front.',1,NULL,'01',17.8971,'2016-08-11 13:29:30','2016-08-11 13:29:30','0000-00-00 00:00:00','CLS','octavio.gonzalez@am.jll.com'),(6123,'6123/comp_56432.JPG','Terreno','Av. Ceylan','793','Industrial Vallejo','Azcapotzalco','Ciudad de México','2300',0,0,'Martha Pavon  Bienes Raices Hahn   ','55-25-88-11-89',12050.0,129705.1,0.0,90000000,0,7468.88,0,'3-6 meses',' Industrial warehouse as land 12,050 m, flat irregular shape, Industrial and commercial use.',1,NULL,'01',17.7866,'2016-08-18 17:47:08','2016-08-18 17:47:08','0000-00-00 00:00:00','CLS','octavio.gonzalez@am.jll.com'),(6131,'6131/comp_56179.JPG','Casa','Sierra Vertientes','S/N','Lomas de Chapultepec','Miguel Hidalgo','Ciudad de México','11000',0,0,'Vertical Inmobiliaria','(01) 55-13-25-57-45',721.0,7760.8,665.0,147378,NULL,221.62,0,'10-02-2016',NULL,0,NULL,NULL,17.8561,'2016-08-03 17:25:01','2016-07-27 17:59:38','2016-08-03 17:25:01','CRL','octavio.gonzalez@am.jll.com'),(6143,'6143/comp_6149.JPG','Land','Av. Alfonso Reyes','','','San Nicolas de los Garza','Nuevo Leon','',0,0,'Area Bienes Raices','83-38-71-61-',9533.0,102612.4,0.0,64975695,0,6815.87,0,'','',1,NULL,'',18.6022,'2016-09-07 23:43:25','2016-09-07 23:43:25','0000-00-00 00:00:00','CLS','daniel.algazi@am.jll.com'),(6183,'6183/comp_16211.JPG','Departamento','Diagonal San Jorge','100','Villas de San Javier','Zapopan','Jalisco','45120',0,0,'Sierra Plus','33-33-42-24-24',0.0,0.0,113.0,4670000,0,41327.43,0,'10-03-2016',' 2 recámaras, 2 baños, 2 estacionamientos. Con alberca, sky bar, áreas comunes, áreas verdes arboladas, salón de cine, salón de eventos, gimnasio, centro de negocios, terraza lounge.',1,NULL,'',19.5997,'2016-09-12 18:03:26','2016-09-12 18:03:26','0000-00-00 00:00:00','CRS','octavio.gonzalez@am.jll.com'),(6212,'6212/comp_6236.JPG','Land','Av. Sendero Divisorio','16000','','San Nicolas de los Garza','Nuevo Leon','',0,0,'Realty World Inco Valle','82-20-84-80-',16000.0,172222.6,0.0,112000000,0,7000,0,'','',1,NULL,'',18.6022,'2016-09-07 23:45:03','2016-09-07 23:45:03','0000-00-00 00:00:00','CLS','daniel.algazi@am.jll.com'),(6376,'6376/comp_86439.JPG','Terreno','Carretera a Madin','S/N','Bosque Esmeralda','Atizapán de Zaragoza','Estado de México','52930',0,0,'Anna Díaz','55-28-53-24-99',282.0,3035.4,0.0,3700000,0,13120.57,0,'23-03-2016','  Lote de terreno con topografía descendente con uso habitacional',1,NULL,'044',19.8925,'2016-09-20 11:53:37','2016-09-20 11:53:37','0000-00-00 00:00:00','CLS','octavio.gonzalez@am.jll.com'),(6406,'6406/comp_96457.JPG','Departamento','Av. México','2588','Ladrón de Guevara ','Guadalajara','Jalisco','44600',0,0,'Certera Grupo Inmobiliario','20-03-03-92-',0.0,0.0,107.0,3103000,0,29000,0,'10-03-2016',' Departamento 2 baños, 2 recámaras, en conjunto con área de fogatas, área comercial, alberca, bar, home theater, gimnasio y spa.',1,NULL,'',19.5997,'2016-09-12 12:15:49','2016-09-12 12:15:49','0000-00-00 00:00:00','CRS','octavio.gonzalez@am.jll.com'),(6418,'6418/comp_56774.JPG','Terreno','Álbaro Obregón','S/N','Barrio de San Bartolo Cuautlalpán','Zumpango','Estado de México','55630',0,0,'Federico Molinar','',100000.0,1076391.0,0.0,45000000,0,450,0,'23-05-2016',' Lote de terreno con topografía plana uso de suelo H 100,',1,NULL,'',18.4444,'2016-09-22 11:15:33','2016-09-22 11:15:33','0000-00-00 00:00:00','CLS','octavio.gonzalez@am.jll.com'),(6419,'6419/comp_76433.JPG','Casa','Cerro Mayka','225','Lomas de Chapultepec','Miguel Hidalgo','Ciudad de México','11000',0,0,'Star Properties-Luordes Máequez','(01) 55-52-59-23-34',700.0,7534.7,600.0,36341200,NULL,60568.67,0,'10-02-2016',NULL,0,NULL,NULL,17.8561,'2016-08-03 17:44:48','2016-07-26 19:48:49','2016-08-03 17:44:48','CRS','octavio.gonzalez@am.jll.com'),(6428,'6428/comp_26469.jpg','Land','Calle libramiento Sur','','','Tepozotlan','Estado de Mexico','',0,0,'Berry Ramos Mora','(01) 55-29-09-46-21',29761.0,320344.7,0.0,66962250,NULL,2250,0,'6-12 months',NULL,0,NULL,NULL,17.8561,'2016-07-19 03:06:45','2016-06-24 02:02:45','2016-07-19 03:06:45','CLS','eduardo.ms@senni.com.mx'),(6435,'6435/comp_66517.JPG','Nave Industrial','Av.del Trabajo','S/N','Texcacoa','Tepotzotlán','Estado de México','54605',0,0,'CBRE','55-85-26-88-61',0.0,0.0,21562.0,1617150.01,0,75,0,'03-03-2016',' Warehouse in industrial complex, average height 10 mts, loading and unloading area and maneuvering yard.',1,NULL,'01',17.8561,'2016-08-25 19:24:10','2016-08-25 19:24:10','0000-00-00 00:00:00','CRL','octavio.gonzalez@am.jll.com'),(6475,'6475/comp_86631.JPG','Local Comercial','Calz. Legaria','432','Deportiva Pensil','Miguel Hidalgo','Ciudad de México','11470',0,0,'Bernardo Noriega','55-52-81-39-88',0.0,0.0,160.0,56000,0,350,0,'15- 02-2016',' Cuenta con dos cajones de estacionamiento, vigilancia privada y cuota de mantenimiento de 6,600.00 mensuales.',1,NULL,'01',18.3748,'2016-08-31 19:47:33','2016-08-31 19:47:33','0000-00-00 00:00:00','CRL','octavio.gonzalez@am.jll.com'),(6527,'6527/comp_46541.JPG','Terreno','Av. López Mateos Sur','1700','Chapalita','Guadalajara','Jalisco','44500',0,0,'Hilda Rivera','17-31-89-30-',2016.0,21700.0,0.0,36228000,0,17970.24,0,'10-03-2016','Terreno regular, uso mixto MC3',1,NULL,'',19.5997,'2016-09-09 19:12:25','2016-09-09 15:11:00','2016-09-09 19:12:25','CLS','octavio.gonzalez@am.jll.com'),(6545,'6545/comp_96566.JPG','Terreno','Georgia','79','Nápoles','Benito Juárez','Ciudad de México','03810',0,0,'Finding Home','55-14-71-90-27',627.0,6749.0,0.0,27000000,0,43062.2,0,'14-03-2016','Terreno con uso H 3 20 Intermedio Plano Regular',1,NULL,'',17.75,'2016-08-30 19:08:36','2016-08-30 19:08:36','0000-00-00 00:00:00','CLS','octavio.gonzalez@am.jll.com'),(6593,'6593/comp_76989.JPG','Terreno','Lerdo de Tejada','S/N','Americana','Guadalajara','Jalisco','44160',0,0,'APV Internacional','36-42-04-35-',0.0,0.0,901.0,15000000,0,0,0,'10-03-2016','Uso de suelo habitacional mixto MB3',1,NULL,'',19.5997,'2016-09-09 18:57:47','2016-09-08 19:38:07','2016-09-09 18:57:47','CLS','octavio.gonzalez@am.jll.com'),(6636,'6636/comp_66684.JPG','Local Comercial','Extremadura','55','Insurgentes Mixcoac','Benito Juárez','Ciudad de México','3920',0,0,'Grupo Cyte','55-67-23-44-11',0.0,0.0,764.0,150000,0,196.34,0,'',' Esta distribuido en dos plantas, cuenta con 15 cajones de estacionamiento',1,NULL,'01',15.6918,'2016-08-09 14:39:38','2016-08-09 14:39:38','0000-00-00 00:00:00','CRL','octavio.gonzalez@am.jll.com'),(6736,'6736/comp_36777.JPG','Casa','  Sierra Amatepec','282','Lomas de Chapultepec','Miguel Hidalgo','Ciudad de México','11000',0,0,'Sra. Guadalupe Valencia','(00) 00-00-00-00-00',819.0,8815.6,530.0,34524140,NULL,65139.89,0,'10-02-2016',NULL,0,NULL,NULL,17.8561,'2016-08-03 17:46:57','2016-07-27 13:02:32','2016-08-03 17:46:57','CRS','octavio.gonzalez@am.jll.com'),(6754,'6754/comp_16814.JPG','Departamento','Carretera México-Huixquilucan','180','Bosque Real','Huixquilucan','Estado de México','52774',0,0,'Ventas Bosque Real','55-53-21-60-00',0.0,0.0,128.0,4700000,0,36718.75,0,'10-03-2016',' Piso 20, vista panorámica, tres recámaras, 2 baños, 2 cajones de estacionamiento, casa club, spa, gimnasio, salón de fiestas, área para jóvenes, salón de negocios.',1,NULL,'01',19.6,'2016-09-05 19:12:39','2016-09-05 19:12:39','0000-00-00 00:00:00','CRS','octavio.gonzalez@am.jll.com'),(6802,'6802/comp_56848.JPG','Casa','Alpes','535','Lomas de Chapultepec','Miguel Hidalgo','Ciudad de México','11000',0,0,'Vertical Inmobiliaria','(01) 55-13-25-57-45',750.0,8072.9,750.0,131678,NULL,175.57,0,'10-02-2016',NULL,0,NULL,NULL,17.8561,'2016-08-03 17:25:38','2016-07-27 18:05:47','2016-08-03 17:25:38','CRL','octavio.gonzalez@am.jll.com'),(6842,'6842/comp_36917.JPG','Terreno','Paseo de la Reforma','S/N','Real de las Lomas','Miguel Hidalgo','Ciudad de México','11920',0,0,'Sergio Peña','55-55-19-55-18',2000.0,21527.8,0.0,71588400,0,35794.2,0,'12 -24 meses',' Property with constructions sold as vacant land, flat regular-shaped lot with one front.',1,NULL,'01',17.8971,'2016-08-11 13:43:03','2016-08-11 13:43:03','0000-00-00 00:00:00','CLS','octavio.gonzalez@am.jll.com'),(6846,'6846/comp_17350.JPG','Terreno','Paseo de Los Coches','S/N','El Dorado','Atizapán de zaragoza','Estado de México','54020',0,0,'Arq. Evaristo Manjarrez','(01) 55-53-98-58-85',88376.0,951271.3,0.0,309316000,NULL,3500,0,'15- 02-2016',NULL,0,NULL,NULL,17.8561,'2016-07-21 12:09:15','2016-07-13 12:34:41','2016-07-21 12:09:15','CLS','octavio.gonzalez@am.jll.com'),(6886,'6886/comp_56959.JPG','Terreno','Av. Ceylan','6','San Pedro Xalpa','Tlalnepantla','Estado de México','2300',0,0,'JLL Database','55-59-80-81-49',44319.0,477045.8,0.0,197071081,0,4446.65,0,'',' Nave industrial como terreno de forma ligeramente irregular, topografía plana.',2,NULL,'015',17.7866,'2016-08-18 18:35:32','2016-08-18 18:35:32','0000-00-00 00:00:00','CLS','octavio.gonzalez@am.jll.com'),(7008,'7008/comp_97085.JPG','Departamento','Av.Ignacio L. Vallarta','3298','Vallarta Nte','Guadalajara','Jalisco','44690',0,0,'Marlene Tapia Trujillo','33-14-10-17-75',0.0,0.0,135.0,3900000,0,28888.89,0,'10-03-2016',' Departamento 2 recámaras y 2 lugares de estacionamiento, el desarrollo cuenta con alberca, gimnasio, biblioteca, sala de cine y salones de eventos.',1,NULL,'',19.5997,'2016-09-12 12:30:50','2016-09-12 12:30:50','0000-00-00 00:00:00','CRS','octavio.gonzalez@am.jll.com'),(7021,'7021/comp_77068.JPG','Terreno','Salamanca Celaya Autopista','S/N','Santa Rita','Celaya','Guanajuato','38010',0,0,'Miguel Angel Carreon','46-11-69-81-34',9000.0,96875.2,0.0,15299991.42,0,1700,0,'01-03-2016',' With regular shape, front to Salamanca-Celaya Highway, Commercial and industrial use.',1,'2016-03-01','01',18.102,'2016-09-28 12:16:12','2016-09-28 12:16:12','0000-00-00 00:00:00','CLS','octavio.gonzalez@am.jll.com'),(7053,'7053/comp_45370.jpg','Land','Rancho San Juan','s/n','NA','Toluca','Estado de Mexico','14275',123456789,987654321,'Vicarther Inmobiliaria','(04) 45-55-49-53',34711.0,373626.1,3000.0,52066500,746962.158,1500,0,'24-36 months','$23',1,'2016-09-14','',17.8561,'2016-09-23 22:18:47','2016-03-04 03:07:12','2016-09-23 22:18:47','CLS','eduardo.ms@senni.com.mx'),(7056,'7056/comp_21218.JPG','Terreno','Sin Nombre','S/N','Villa Pozos','Villa de Pozos','San Luis Potosí','78421',0,0,'Ramón Olugin','(01) 44-42-66-08-98',35807.0,385423.3,0.0,7161400,NULL,200,0,'17-03-2016',NULL,0,NULL,NULL,17.8561,'2016-07-21 13:13:51','2016-07-18 11:57:21','2016-07-21 13:13:51','CLS','octavio.gonzalez@am.jll.com'),(7079,'7079/comp_77093.JPG','Terreno','Ubicación Confidencial','S/N','Confidencial','Tecámac','Estado de México','',0,0,'Base de datos JLL','55-59-80-88-48',241281.6,2597133.5,0.0,134533493,0,557.58,0,'03-02-2016','  Uso de suelo Industrial y H-100-A, terreno plano de configuración irregular',2,NULL,'',18.4444,'2016-09-21 13:12:52','2016-09-21 13:12:52','0000-00-00 00:00:00','CLS','octavio.gonzalez@am.jll.com'),(7082,'7082/comp_17386.JPG','Departamento','Av. Cuauhtémoc','830','Narvarte','Benito Juárez','Ciudad de México','3020',0,0,'La Campana','(01) 55-56-44-44-70',0.0,0.0,100.0,2900000,NULL,29000,0,'03-07-2015',NULL,0,NULL,NULL,15.6918,'2016-08-05 13:21:10','2016-08-05 13:21:10','0000-00-00 00:00:00','CRS','octavio.gonzalez@am.jll.com'),(7185,'7185/comp_47251.JPG','Local en Plaza Comercial','Calz.Legaria','238','Pensil Norte','Miguel Hidalgo','Ciudad de México','11430',0,0,'Base de datos JLL','55-59-80-81-49',0.0,0.0,400.0,100000,0,250,0,'15- 02-2016',' Plaza Cornercial, cuenta con vigilancia privada, circuito cerrado de T.V.  estacionamiento para 30 autos',1,NULL,'01',18.3748,'2016-09-01 12:36:22','2016-09-01 12:36:22','0000-00-00 00:00:00','CRL','octavio.gonzalez@am.jll.com'),(7245,'7245/comp_27305.JPG','Nave Industrial','Poniente 116','718','Industrial Vallejo','Azcapotzalco','Ciudad de México','2300',0,0,'JLL Database','55-59-80-81-49',0.0,0.0,3193.0,274874.11,0,86.09,0,'10-11- 2015',' Industrial warehouse, average height 9 mts, loading and unloading area and maneuvering yard.',2,NULL,'01',17.7866,'2016-08-19 13:30:41','2016-08-19 13:30:41','0000-00-00 00:00:00','CRL','octavio.gonzalez@am.jll.com'),(7264,'7264/comp_17311.JPG','Departamento','Lago Zurich','S/N','Ampliación granada','Miguel Hidalgo','Ciudad de México','11529',0,0,'Manuela','(01) 55-38-99-08-28',0.0,0.0,80.0,23500,NULL,293.75,0,'12-04-2016',NULL,0,NULL,NULL,17.8561,'2016-08-03 17:28:30','2016-07-28 10:53:14','2016-08-03 17:28:30','CRL','octavio.gonzalez@am.jll.com'),(7275,'7275/comp_37404.JPG','Terreno','Oriente 182','S/N','Moctezuma 2da Sección','Venustiano Carranza','Ciudad de México','15530',0,0,'Arq. Ernestina Amero','(01) 55-54-12-82-61',1325.0,14262.2,0.0,18750000,NULL,14150.94,0,'24-05-2016',NULL,0,NULL,NULL,18.3826,'2016-08-03 15:21:19','2016-08-03 11:44:44','2016-08-03 15:21:19','CLS','octavio.gonzalez@am.jll.com'),(7338,'7338/comp_77403.JPG','Nave Industrial','Av. de las Diligencias','S/N','Tepojaco','Tizayuca','Hidalgo','43823',0,0,'Jorge Garcia','77-12-46-28-75',3000.0,32291.7,3000.0,119996.42,0,40,0,'',' Inustrial building in Parque Logistico Tizayuca',1,NULL,'045',17.1767,'2016-08-17 19:50:22','2016-08-17 19:50:22','0000-00-00 00:00:00','CRL','octavio.gonzalez@am.jll.com'),(7349,'7349/comp_8407.JPG','Casa','Petirrojo','34','Las Alamedas','Atizapán de zaragoza','Estado de México','52970',0,0,'Guillermo Arena','91-15-69-74-',160.0,1722.2,170.0,3385000,0,19911.76,0,'23-03-2016',' Casa habitación desarollada en dos niveles, cuenta con sala, comedor, cocina, área de lavado, 3 recámaras, 2.5 baños y dos cajones de estacionamiento.',1,NULL,'',19.8925,'2016-09-19 14:54:42','2016-09-19 14:54:42','0000-00-00 00:00:00','CRS','octavio.gonzalez@am.jll.com'),(7397,'7397/comp_67478.JPG','Nave Industrial','Poniente 146','710','Industrial Vallejo','Azcapotzalco','Ciudad de México','2300',0,0,'Srita.Areli Dieboqui Bienes Industriales','55-55-44-22-90',11596.0,124818.3,7096.0,91999996.8,0,12965.05,0,'06-05-2016',' Cuenta con 1000 m de áreas comunes, 3,500 m de patio de maniobras y oficinas. Altura máxima 10.50 m.',1,NULL,'01',17.7866,'2016-08-23 12:04:53','2016-08-23 12:04:53','0000-00-00 00:00:00','CRS','octavio.gonzalez@am.jll.com'),(7503,'7503/comp_57548.JPG','Casa','Av. Arturo Montiel','1','Ex Hacienda de Xaltipa','Cuautitlán','Estado de México','54850',0,0,'Israel Camacho','58-85-38-17-',74.0,796.5,87.0,798000,0,9172.41,0,'23-05-2016','Condominio horizontal. La vivienda se desarrolla en dos niveles y cuenta con sala, comedor, cocina, 1/2 baño, cubo de escalera, y patio posterior, además cuenta en planta alta con 2 recámaras con baño completo y área de T.V. cuenta con un estaciomiento al frente del predio.',1,NULL,'',18.4444,'2016-09-22 12:11:04','2016-09-22 12:09:49','2016-09-22 12:11:04','CRS','octavio.gonzalez@am.jll.com'),(7512,'7512/comp_19116.JPG','Nave Industrial','Eje 122','S/N','Zona Industrial','San Luis Potosí','San Luis Potosí','78395',0,0,'Alejandro Martínez','(01) 44-48-14-67-66',1500.0,16145.9,1500.0,57000,NULL,38,0,'17-03-2016',NULL,0,NULL,NULL,17.8561,'2016-07-21 13:34:22','2016-07-18 12:11:20','2016-07-21 13:34:22','CRL','octavio.gonzalez@am.jll.com'),(7517,'7517/comp_87626.JPG','Local en Plaza Comercial','Calz. Legaria','238','Pensil Norte','Miguel Hidalgo','Ciudad de México','11430',0,0,'Remax Esmeralda','55-53-08-39-39',0.0,0.0,170.0,42500,0,250,0,'15- 02-2016',' Plaza Cornercial, cuenta con vigilancia privada, circuito cerrado de T.V. estacionamiento para 30 autos y se encuentra en el primer piso.',1,NULL,'01',18.3748,'2016-08-31 20:10:02','2016-08-31 20:10:02','0000-00-00 00:00:00','CRL','octavio.gonzalez@am.jll.com'),(7568,'7568/comp_47731.JPG','Nave Industrial','Av. de las Granjas','388','Santo Tomás','Azcapotzalco','Ciudad de México','02020',0,0,'JLL Database','55-59-80-81-49',0.0,0.0,17521.0,212955404.5,0,12154.3,0,'10-02-2016',' Industrial warehouse with offices, mezzanine,  covered with galvanized sheet, Average Building Height: 10.20 m (88.26 ft).',2,NULL,'01',17.7866,'2016-08-19 19:15:48','2016-08-19 19:15:48','0000-00-00 00:00:00','CRS','octavio.gonzalez@am.jll.com'),(7694,'7694/comp_17880.JPG','Departamento','Romero de Terreros','1','Narvarte','Benito Juárez','Ciudad de México','3020',0,0,'Cataño Bienes Raíces','(01) 55-55-68-43-81',0.0,0.0,67.0,2310000,NULL,34477.61,0,'03-07-2015',NULL,0,NULL,NULL,15.6918,'2016-08-05 13:32:26','2016-08-05 13:32:26','0000-00-00 00:00:00','CRS','octavio.gonzalez@am.jll.com'),(7707,'7707/comp_17753.JPG','Departamento','Lago Zurich','S/N','Ampliación Granada','Miguel Hidalgo','Ciudad de México','11529',0,0,'Casas en Venta','(01) 55-43-26-80-11',0.0,0.0,120.0,45000,NULL,375,0,'12-04-2016',NULL,0,NULL,NULL,17.8561,'2016-08-03 17:29:17','2016-07-28 11:03:50','2016-08-03 17:29:17','CRL','octavio.gonzalez@am.jll.com'),(7720,'7720/comp_97941.JPG','Departamento','Carr.México Toluca','5860','El Contadero','Cuajimalpa','Ciudad de México','05500',0,0,'Stampa Recidencial','55-29-74-28-35',0.0,0.0,90.0,2800000,0,31111.11,0,'07-03-2016',' Segunda etapa del condominio conformado por 88 departamentos. El departamento cuenta con 2 recámaras, 2 baños y 2 estacionamientos.',1,NULL,'01',19.4409,'2016-09-06 17:48:18','2016-09-06 17:48:18','0000-00-00 00:00:00','CRS','octavio.gonzalez@am.jll.com'),(7722,'7722/comp_81238.JPG','Departamento','Av.Lic. Manuel Rojas','174','Constituyentes 1917','Huixquilucan','Estado de México','52775',0,0,'Base de datos JLL','55-59-80-88-50',0.0,0.0,90.0,1980000,0,22000,0,'10-03-2016',' Modelo con 3 recámaras, 2 baños, 1 cajón de estacionamiento, Salón de usos múltiples para 150 personas aprox., Ludoteca o salón de juegos para niños, salón de juegos para adultos, Business Center, área para gimnasio, área de oficinas administrativas, Lobby recepción, roof garden común por torre, amplio jardín, estacionamiento para visitas, acceso controlado, cancha de usos múltiples',1,NULL,'01',19.6,'2016-09-06 13:20:53','2016-09-06 12:50:57','2016-09-06 13:20:53','CRS','octavio.gonzalez@am.jll.com'),(7743,'7743/comp_27843.JPG','Local en Plaza Comercial','Blvrd. Puerto Aéreo','123','Moctezuma 2da Sección','Venustiano Carranza','Ciudad de México','15530',0,0,'Sandra Samano  Invest 4 real','(01) 55-91-85-79-16',281.0,3024.7,2.8,69000,NULL,24555.16,0,'24-05-2016',NULL,0,NULL,NULL,18.3826,'2016-08-04 12:29:43','2016-08-04 12:29:43','0000-00-00 00:00:00','CRL','octavio.gonzalez@am.jll.com'),(7828,'7828/comp_67900.JPG','Nave Industrial','Av. de la Industria','S/N','El Trebol','Tepotzotlán','Estado de México','54614',0,0,'JLL Database','55-59-80-88-50',0.0,0.0,15958.0,1188231.67,0,74.46,0,'10-11- 2015',' undividied, heigth 9 to 13 m, eigth load and unload bay with manuever yard, high resistance floor, termoacustic cover.',2,NULL,'01',17.8561,'2016-08-25 19:56:31','2016-08-25 19:56:31','0000-00-00 00:00:00','CRL','octavio.gonzalez@am.jll.com'),(7839,'7839/comp_47886.JPG','Local en Plaza Comercial','Legaria ','549','10 de Abril','Miguel Hidalgo','Ciudad de México','11250',0,0,'Base de datos JLL','59-80-81-49',0.0,0.0,357.0,103242,0,289.19,0,'15-08-2014',' Local comercial ubicado en planta baja, rentado desde tercer trimestre de 2014',2,NULL,'',18.3748,'2016-09-01 12:57:38','2016-09-01 12:57:38','0000-00-00 00:00:00','CRL','octavio.gonzalez@am.jll.com'),(7849,'7849/comp_57969.JPG','Departamento','Lago Zurich','S/N','Ampliación Granada','Miguel Hidalgo','Ciudad de México','11529',0,0,'Veronica','(01) 55-55-06-08-25',0.0,0.0,70.6,28000,NULL,396.71,0,'19-04-2016',NULL,0,NULL,NULL,17.8561,'2016-08-03 17:28:06','2016-07-27 18:50:17','2016-08-03 17:28:06','CRL','octavio.gonzalez@am.jll.com'),(7893,'7893/comp_38053.JPG','Land','Road to Hacienda Tamariz, Maximo Serdán, Puebla','sn','Maximo Serdán','Puebla','Puebla','',0,0,'Edgar Parra','22-21-90-05-60',60000.0,645834.6,0.0,0,0,0,0,'','',1,NULL,'045',13.6115,'2016-08-25 11:19:05','2016-08-25 11:19:05','0000-00-00 00:00:00','CLS','daniel.algazi@am.jll.com'),(7900,'7900/comp_88559.JPG','Local Comercial','Av.Chapultepec Sur','480','Obrera ','Guadalajara','Jalisco','44140',0,0,'Enrique Meyabii','33-12-08-48-65',0.0,0.0,108.0,28000,0,259.26,0,'10-03-2016',' Local comercial en Plaza las Ramblas ubicada en el Desarrollo Horizontes Chapultepec, con estacionamiento.',1,NULL,'044',19.5997,'2016-09-09 19:16:37','2016-09-07 19:09:54','2016-09-09 19:16:37','CRL','octavio.gonzalez@am.jll.com'),(7924,'7924/comp_79521.JPG','Casa','Nicolás Bravo','S/N','Miguel Hidalgo','Cuautla','De Morelos','62748',0,0,'Construrarte Inmobiliaria','(01) 55-62-60-22-73',1128.0,12141.7,372.0,3480000,NULL,9354.84,0,'07-03-2016',NULL,0,NULL,NULL,17.7723,'2016-08-02 19:23:06','2016-08-02 19:23:06','0000-00-00 00:00:00','CRS','octavio.gonzalez@am.jll.com'),(7979,'7979/comp_78037.JPG','Terreno','Av. Hidalgo','1571','Americana','Guadalajara','Jalisco','44600',0,0,'MYC Soluciones Inmobiliarias','44-91-45-14-23',0.0,0.0,923.0,13845000,0,0,0,'10-03-2016',' Terreno con uso de suelo Mixto sobre Avenida, junto al Hotel Fiesta  Americana. MB3',1,NULL,'',19.5997,'2016-09-09 19:04:34','2016-09-08 19:46:08','2016-09-09 19:04:34','CLS','octavio.gonzalez@am.jll.com'),(8047,'8047/comp_78093.JPG','Terreno','16 de Septiembre','S/N','Barrio San Bartolo Cuautlalpán','Zumpango','Estado de México','55630',0,0,'Enrique Palacios','55-29-00-15-67',180000.0,1937503.9,0.0,90000000,0,500,0,'23-05-2016',' Terreno plano, forma irregular, 2 frentes a vialidad, cuenta con uso habitacional H 100-A.  El predio se conecta fácilmente a las autopistas, La Mexiquense y Arco Norte.',1,NULL,'044',18.4444,'2016-09-21 13:28:33','2016-09-21 13:28:33','0000-00-00 00:00:00','CLS','octavio.gonzalez@am.jll.com'),(8141,'8141/comp_68191.JPG','Terreno','Heliodoro Valle','221','Merced Balbuena','Venustiano Carranza','Ciudad de México','15810',0,0,'Grupo Guemer, S.A. de C.V.','(01) 55-55-95-53-82',1725.0,18567.8,0.0,38000000,NULL,22028.99,0,'24-05-2016','',1,NULL,'',18.3826,'2016-08-11 11:24:00','2016-08-03 19:58:29','2016-08-11 11:24:00','CLS','octavio.gonzalez@am.jll.com'),(8260,'8260/comp_18318.JPG','Departamento','Cda. de Gillermo Prieto ','36','Jesus del  Monte','Huixquilucan','Estado de México','52764',0,0,'Base de datos JLL','55-59-80-88-50',0.0,0.0,75.0,1850000,0,24666.67,0,'10-03-2016',' 2 recamaras, 2 baños 1 cajon de estacionamiento.',1,NULL,'01',19.6,'2016-09-05 20:04:49','2016-09-05 20:04:49','0000-00-00 00:00:00','CRS','octavio.gonzalez@am.jll.com'),(8261,'8261/comp_68371.JPG','Departamento','Mar Cantábrico','16','Pensil Sur','Miguel Hidalgo','Ciudad de México','11490',0,0,'Vetro Inmuebles Sr. Edson','55-43-84-19-76',0.0,0.0,65.0,1730000,0,26615.38,0,'15- 02-2016',' Departamento en una planta, cuenta con 2 recámaras, 1.5 baños, sala, comedor, cocina y 1 cajón de estacionamiento, terraza. Además cuenta con amenidades como roof garden, bodegas,interfón, elevador.',1,NULL,'044',18.3748,'2016-09-01 18:33:03','2016-09-01 18:33:03','0000-00-00 00:00:00','CRS','octavio.gonzalez@am.jll.com'),(8272,'8272/comp_98314.JPG','Terreno','Prol.Vasco de Quiroga','1920','Santa Fe La Loma','Álvaro Obregón','Ciudad de México','01376',0,0,'Martha Montaño Ledesma','(01) 55-63-01-12-90',7601.0,81816.5,0.0,165000000,NULL,21707.67,0,'15- 02-2016',NULL,0,NULL,NULL,17.8561,'2016-08-03 15:14:37','2016-07-22 10:48:24','2016-08-03 15:14:37','CLS','octavio.gonzalez@am.jll.com'),(8282,'8282/comp_18327.JPG','Departamento','Lago Alberto','300','Ampliación Granada','Miguel Hidalgo','Ciudad de México','11520',0,0,'William','(01) 55-10-78-56-81',0.0,0.0,73.0,19750,NULL,270.55,0,'12-04-2016',NULL,0,NULL,NULL,17.8561,'2016-08-03 17:29:43','2016-07-28 11:23:42','2016-08-03 17:29:43','CRL','octavio.gonzalez@am.jll.com'),(8386,'8386/comp_78425.JPG','Terreno','Niños Heroes esq. con Cauda','S/N','Jardines del Bosque','Guadalajara','Jalisco','44520',0,0,'APV Internacional','36-42-04-35-',1506.0,16210.5,800.0,21000000,0,13944.22,0,'10-03-2016',' Terreno con uso de suelo habitacional mixto en esquina, tiene una casa para demoler. MD3',1,NULL,'',19.5997,'2016-09-09 19:08:34','2016-09-08 19:57:30','2016-09-09 19:08:34','CLS','octavio.gonzalez@am.jll.com'),(8434,'8434/comp_79640.JPG','Terreno','Capuchinas esq. Damas','S/N','San José Insurgentes','Benito Juárez','Ciudad de México','03900',0,0,'Grupo Aitex','55-91-10-13',947.0,10193.4,0.0,43500000,0,45934.53,0,'14-03-2016','Terreno con uso H 3 20 B  Esquina  Plano  Regular',1,NULL,'',17.75,'2016-08-30 14:51:22','2016-08-30 14:49:42','2016-08-30 14:51:22','CLS','octavio.gonzalez@am.jll.com'),(8544,'8544/comp_98600.JPG','Departamento','Carretera Méxoco Toluca','3703','Paseo de las Lomas','Álvaro Obregón','Ciudad de México','01330',0,0,'Be Grand Lomas','55-52-92-58-88',0.0,0.0,109.0,3948558,0,36225.3,0,'07-03-2016',' Ultima etapa del condominio. El departamento cuenta con 2 recámaras, 2 baños y 2 estacionamientos.',1,NULL,'01',19.4409,'2016-09-06 17:57:22','2016-09-06 17:57:22','0000-00-00 00:00:00','CRS','octavio.gonzalez@am.jll.com'),(8553,'8553/comp_98634.JPG','Terreno','Av. Porfirio Díaz','53','Del Valle','Benito Juárez','Ciudad de México','03100',0,0,'Quality Inmobiliario','12-09-12-07',328.0,3530.6,0.0,16000000,0,48780.49,0,'14-03-2016','Terreno con uso H 4 20 M Esquina Plano Regular',1,NULL,'',17.75,'2016-08-30 19:46:01','2016-08-30 19:35:22','2016-08-30 19:46:01','CLS','octavio.gonzalez@am.jll.com'),(8555,'8555/comp_49334.JPG','Departamento','Sonora','S/N','Roma Norte','Cuauhtémoc','Ciudad de México','06700',0,0,'Emi Contaste','55-13-40-24-35',0.0,0.0,220.0,9800000,0,44545.45,0,'26-02-2016','Ph de dos niveles con sala, comedor, cocina, área de lavado, 2 recámaras con baño completo, family room, terraza, cuarto de servicio con baño completo, dos cajones de estacionamiento',1,'2016-02-26','044',18.1706,'2016-09-30 12:00:25','2016-09-30 12:00:25','0000-00-00 00:00:00','CRS','octavio.gonzalez@am.jll.com'),(8661,'8661/comp_88801.JPG','Departamento','Carretera México - Toluca','5095','Cuajimalpa','Cuajimalpa de Morelos','Ciudad de México','05000',0,0,'Promotores Inmobiliarios','55-52-92-00-31',0.0,0.0,75.0,2950000,0,39333.33,0,'07-03-2016',' El condominio contará con 111 departamentos, se encuentra en etapa de preventa. El departamento cuenta con 2 recámaras, 2 baños y 2 estacionamientos.',1,NULL,'01',19.4409,'2016-09-06 15:21:39','2016-09-06 15:21:39','0000-00-00 00:00:00','CRS','octavio.gonzalez@am.jll.com'),(8677,'8677/comp_28791.JPG','Nave Industrial','Av. 2 de Noviembre','S/N','Barrio las Animas','Tepotzotlán','Estado de México','54616',0,0,'Centuri 21 Vilchis y Asociados','72-22-77-10-00',0.0,0.0,15000.0,1124934.3,0,75,0,'03-03-2016',' In Industrial park, steel columns, 15 cm concrete floor, average height 15 m.',1,NULL,'01',17.8561,'2016-08-26 13:13:59','2016-08-26 13:13:59','0000-00-00 00:00:00','CRL','octavio.gonzalez@am.jll.com'),(8717,'8717/comp_18175.JPG','Departamento','Av.Jorge Jiménez Cantu','S/N','Fracc. Lago Esmeralda','Atizapán de zaragoza','Estado de México','52989',0,0,'Ricardo Coro','(01) 55-63-10-29-69',0.0,0.0,78.0,1980000,NULL,25384.62,0,'15- 02-2016',NULL,0,NULL,NULL,17.8561,'2016-07-21 12:23:09','2016-07-13 18:35:36','2016-07-21 12:23:09','CRS','octavio.gonzalez@am.jll.com'),(8844,'8844/comp_58857.JPG','Departamento','Lagrange','S/N','Polanco','Miguel Hidalgo','Ciudad de México','11550',0,0,'Fernández y Asociados','(01) 55-54-25-82-64',0.0,0.0,262.0,39000,NULL,148.85,0,'14-01-2016',NULL,0,NULL,NULL,17.8561,'2016-08-03 15:26:14','2016-07-26 15:03:35','2016-08-03 15:26:14','CRL','octavio.gonzalez@am.jll.com'),(8900,'8900/comp_68962.JPG','Nave Industrial','Poniente 140','','Industrial Vallejo','Azcapotzalco','Ciudad de México','2300',0,0,'Gloria Murillo  ColdwellBancker Luxury ','55-59-52-32-59',7923.0,85282.5,4328.0,78000003.86,0,18022.18,0,'06-05-2016',': Industrial bay, offices 576 m, tilt-up walls, parking area, cistern, average heigth 9.50 m',1,NULL,'01',17.7866,'2016-08-23 12:31:50','2016-08-23 12:31:50','0000-00-00 00:00:00','CRS','octavio.gonzalez@am.jll.com'),(8926,'8926/comp_98944.JPG','Terreno','Av. Vasco de Quiroga','S/N','Santa Fe','Álvaro Obregón','Ciudad de México','01376',0,0,'Rocio Garcia Esquivel','(01) 55-66-39-65-74',26204.0,282057.5,0.0,450660000,NULL,17198.14,0,'15- 02-2016',NULL,0,NULL,NULL,17.8561,'2016-08-03 15:15:49','2016-07-22 11:01:11','2016-08-03 15:15:49','CLS','octavio.gonzalez@am.jll.com'),(9065,'9065/comp_99144.JPG','Departamento','Carr.México Toluca','5392','El Yaqui','Cuajimalpa','Ciudad de México','05320',0,0,'We Santa FéSky Homes','55-52-92-45-82',0.0,0.0,91.0,3154591,0,34665.84,0,'07-03-2016',' El condominio contará con 372 departamentos, se encuentra en etapa de preventa. El departamento cuenta con 2 recámaras, 2 baños y 2 estacionamientos.',1,NULL,'01',19.4409,'2016-09-06 18:09:44','2016-09-06 18:09:44','0000-00-00 00:00:00','CRS','octavio.gonzalez@am.jll.com'),(9099,'9099/comp_79145.JPG','Terreno','Reforma','2726','Circunvalación Vallarta','Guadalajara','Jalisco','44680',0,0,'Sierra Plus','33-42-24-24-',1435.0,15446.2,0.0,31588260,0,22012.72,0,'10-03-2016','Terreno en esquina, para 24 departamentos en 8 niveles, incluyendo uso comercial para planta baja. MB3',1,NULL,'',19.5997,'2016-09-09 19:09:53','2016-09-08 20:10:07','2016-09-09 19:09:53','CLS','octavio.gonzalez@am.jll.com'),(9197,'9197/comp_29476.JPG','Casa','Fracc. Los Sabinos','S/N','Santa Cruz','Cuautla','Estado de Morelos              ','62747',0,0,'Propiedades de Morelos','73-51-66-07-95',700.0,7534.7,140.0,9280,0,66.29,0,'05-03-2016',' Casa habitación desarrollada en un nivel, cuenta con sala, comedor, cocina, tres recámaras, 2 baños, área de lavado, jardín y estacionamiento para dos autos.',1,'2016-03-05','01',17.7723,'2016-09-26 19:35:06','2016-09-26 19:35:06','0000-00-00 00:00:00','CRL','octavio.gonzalez@am.jll.com'),(9209,'9209/comp_69391.JPG','Departamento','Lago Ginebra','418','Francisco I.Madero','Miguel Hidalgo','Ciudad de México','11480',0,0,'Gabriel B Barroso','68-48-82-25',0.0,0.0,64.0,1500000,0,23437.5,0,'15- 02-2016',' Cuenta con 2 recámaras, 1 baño, sala, comedor, cocina, cuenta con un cajón de estacionamiento',1,NULL,'',18.3748,'2016-09-01 18:47:59','2016-09-01 18:47:59','0000-00-00 00:00:00','CRS','octavio.gonzalez@am.jll.com'),(9345,'9345/comp_70469.JPG','Departamento','Edgar Allan Poe','138','Polanco','Miguel Hidalgo','Ciudad de México','11550',0,0,'República Arquitectos','(01) 55-45-93-49-14',0.0,0.0,150.0,11000000,NULL,73318.67,0,'01-14-2016',NULL,0,NULL,NULL,17.8561,'2016-08-03 17:31:18','2016-07-19 19:28:35','2016-08-03 17:31:18','CRS','octavio.gonzalez@am.jll.com'),(9384,'9384/comp_18395.JPG','Departamento','Pioneros de Rochdale','28','Nuevo México','Atizapán de zaragoza','Estado de México','52966',0,0,'Sra. Perla','(01) 55-22-08-29-21',0.0,0.0,98.0,1876000,NULL,19142.86,0,'15- 02-2016',NULL,0,NULL,NULL,17.8561,'2016-07-21 12:26:45','2016-07-13 18:59:14','2016-07-21 12:26:45','CRS','octavio.gonzalez@am.jll.com'),(9397,'9397/comp_89419.JPG','Terreno','Circuito Univercidades','S/N','Zibatá','El Marqués','Querétaro','76269',0,0,'Cecilia Volnie','44-21-25-51-64',300.0,3229.2,500.0,1100000,0,3666.67,0,'16-03-2016',' Terreno de 13.10 de frente y fondo por 23 metros de largo.',2,NULL,'01',19.8925,'2016-09-13 14:22:02','2016-09-13 14:22:02','0000-00-00 00:00:00','CLS','octavio.gonzalez@am.jll.com'),(9522,'9522/comp_49535.JPG','Departamento ','Hegel','S/N','Polanco','Miguel Hidalgo','Ciudad de México','11560',0,0,'Inmobiliaria MINM','(01) 55-26-99-18-96',0.0,0.0,180.0,32000,NULL,177.78,0,'14-01-2016','',1,NULL,'',17.8561,'2016-09-09 13:06:20','2016-07-26 12:33:37','2016-09-09 13:06:20','CRL','daniel.algazi@am.jll.com'),(9571,'9571/comp_16936.JPG','Casa','Av Jorge Jiménez Cantú','S/N','Fracc. Lago Esmeralda','Atizapan de Zaragoza','Estado de México','52989',0,0,'Elizabeth Iglesias','(01) 80-02-62-10-10',150.0,1614.6,204.0,4330000,NULL,21225.49,0,'14-02-2016',NULL,0,NULL,NULL,17.8561,'2016-08-03 11:50:17','2016-07-08 14:32:39','2016-08-03 11:50:17','CRS','octavio.gonzalez@am.jll.com'),(9581,'9581/comp_59654.JPG','Local Comercial','Torres Adalid','1503 Int-A','Del Valle','Benito Juárez','Ciudad de México','3100',0,0,'AC Bienes Raíces','55-11-63-01-00',0.0,0.0,48.0,10000,0,208.33,0,'',' A solo una cuadra de Av. Cuauhtémoc. Con 3 divisiones para bodegas o privados, patio techado con domo, cortina nueva uso de suelo comercial en orden.',1,NULL,'01',15.6918,'2016-08-09 12:37:30','2016-08-09 12:37:30','0000-00-00 00:00:00','CRL','octavio.gonzalez@am.jll.com'),(9582,'9582/comp_60203.JPG','Terreno','Santo Degollado','S/N','Año de Juárez','Cuautla','De Morelos','62748',0,0,'Camacho Bienes Raices','73-53-56-66-38',1000.0,10763.9,0.0,2552000,NULL,2552,0,'05-03-2016',' Terreno baldio de forma regular, un frente, uso habitacional, construcciones en obra negra.',1,NULL,'01',17.7723,'2016-08-29 13:13:21','2016-08-02 14:03:09','2016-08-29 13:13:21','CLS','octavio.gonzalez@am.jll.com'),(9612,'9612/comp_19291.JPG','Nave Industial','Manuel Díaz','S/N','Abastos','San Luis Potosí','San Luis Potosí','78390',0,0,'Abelardo Huerta','(45) 44-42-92-37-78',1250.0,13454.9,1250.0,35000,NULL,28,0,'17-03-2016',NULL,0,NULL,NULL,17.8561,'2016-08-03 11:51:07','2016-07-18 12:45:41','2016-08-03 11:51:07','CRL','octavio.gonzalez@am.jll.com'),(9705,'9705/comp_89778.JPG','Terreno','Av. del Club','S/N','Bosque Real','Huixquilucan','Estado de México','52774',0,0,'Corporativo Inmobiliario','55-51-09-08-26',9309.0,100201.2,0.0,74931151.5,0,8049.32,0,'10-03-2016',' Se vende macrolote para 30 departamentos con posibilidad de crecer a mas deptos .',1,NULL,'01',19.6,'2016-09-05 12:03:39','2016-09-05 11:47:34','2016-09-05 12:03:39','CLS','octavio.gonzalez@am.jll.com'),(9728,'9728/comp_49790.JPG','Local Comercial','Calz. México Tacuba','S/N','Tlaxpana','Miguel Hidalgo','Ciudad de México','11370',0,0,'Century 21 Bátiz','53-43-15-55',490.0,5274.3,200.0,50000,0,250,0,'15- 02-2016',' Local comercial en planta baja, no cuenta con estacionamientos. Anteriormente funcionaba como tintorería, cerca del metro Normal.',1,NULL,'',18.3748,'2016-09-01 13:21:30','2016-09-01 13:21:30','0000-00-00 00:00:00','CRL','octavio.gonzalez@am.jll.com'),(9768,'9768/comp_19810.JPG','Departamentos','Ejercito Nacional','453','Granada','Miguel Hidalgo','Ciudad de México','11520',0,0,'Fany Bentivogli','(01) 55-55-96-92-20',0.0,0.0,115.0,6500000,NULL,56521.74,0,'12-04-2016',' Departamento con sala, comedor, balcón, cocina, área de lavado, tres recámaras, 3 baños,  y dos cajones de estacionamiento, el edificio cuenta con áreas comunes, elevador, pasillos y roof garden.',1,NULL,'',17.8561,'2016-09-12 18:14:37','2016-07-28 11:36:23','2016-09-12 18:14:37','CRS','octavio.gonzalez@am.jll.com'),(9785,'9785/comp_59854.JPG','Terreno','Poniente 146','915','Azcapotzalco','Tlalnepantla','Estado de México','2300',0,0,'JLL Database','55-59-80-81-49',30005.0,322971.1,0.0,133421733,0,4446.65,0,'04-11-2015',' Vacant land of irregular Shape, one front, industrial use.',2,NULL,'01',17.7866,'2016-08-18 19:26:11','2016-08-18 19:26:11','0000-00-00 00:00:00','CLS','octavio.gonzalez@am.jll.com'),(9864,'9864/comp_17127.JPG','Casa','A.v Jorge Jiménez Cantú ','S/N','Fracc. Aqua Esmeralda','Atizapan de Zaragoza','Estado de México','52989',0,0,'Ricardo Corro','(01) 55-63-10-29-69',220.0,2368.1,147.0,3800000,NULL,25850.34,0,'15- 02-2016',NULL,0,NULL,NULL,17.8561,'2016-08-03 15:11:44','2016-07-12 12:26:32','2016-08-03 15:11:44','CRS','octavio.gonzalez@am.jll.com'),(9875,'9875/comp_69909.JPG','Terreno','Laguna de Términos','525','Anahuác','Miguel Hidalgo','Ciudad de México','11320',0,0,'Fernando González Salazar','55-52-81-38-81',428.0,4606.9,0.0,6000000,0,14018.69,0,'15- 02-2016',' Inmueble que se vende como terreno, uso de suelo habitacional con comercio HC3 30, a dos calle de Av. Marina Nacion',1,NULL,'01',18.3748,'2016-08-31 15:09:37','2016-08-31 15:09:37','0000-00-00 00:00:00','CLS','octavio.gonzalez@am.jll.com'),(9957,'9957/comp_59978.JPG','Terreno','Libramiento Sur','S/N','Tlacateco','Tepotzotlán','Estado de México','54605',0,0,'Betty Ramos Mora','55-29-09-46-21',20000.0,215278.2,0.0,50000008.4,0,2500,0,'03-03-2016',' Land on industrial zone, with all services, for industrial use behind Jugos del Valle.',1,NULL,'01',17.8561,'2016-08-24 13:38:18','2016-08-24 13:38:18','0000-00-00 00:00:00','CLS','octavio.gonzalez@am.jll.com');
/*!40000 ALTER TABLE `comparables` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `datos_conversion`
--

DROP TABLE IF EXISTS `datos_conversion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `datos_conversion` (
  `id_df` int(11) NOT NULL,
  `tipo_cambio` double NOT NULL,
  `exchange_rate` double NOT NULL,
  `date_exchange_rate` datetime DEFAULT NULL,
  `ft` double NOT NULL,
  `ftyear` double DEFAULT NULL,
  PRIMARY KEY (`id_df`),
  UNIQUE KEY `id_df` (`id_df`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `datos_conversion`
--

LOCK TABLES `datos_conversion` WRITE;
/*!40000 ALTER TABLE `datos_conversion` DISABLE KEYS */;
INSERT INTO `datos_conversion` VALUES (1,15.5,18.1706,'2016-09-30 11:56:58',10.76391041671,1.11483648);
/*!40000 ALTER TABLE `datos_conversion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `definiciones_in`
--

DROP TABLE IF EXISTS `definiciones_in`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `definiciones_in` (
  `id_definicion` int(11) NOT NULL,
  `id_in` int(11) NOT NULL,
  KEY `id_in` (`id_in`),
  KEY `id_definicion` (`id_definicion`),
  CONSTRAINT `fk_def` FOREIGN KEY (`id_definicion`) REFERENCES `catalogo_definiciones` (`id_definicion`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_in` FOREIGN KEY (`id_in`) REFERENCES `inmuebles` (`id_in`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `definiciones_in`
--

LOCK TABLES `definiciones_in` WRITE;
/*!40000 ALTER TABLE `definiciones_in` DISABLE KEYS */;
INSERT INTO `definiciones_in` VALUES (1,70778),(2,70778),(3,70778),(4,70778),(19,70778),(2,31259),(3,31259),(4,31259),(5,31259),(11,31259),(15,31259),(16,31259),(19,31259),(21,31259),(22,31259),(23,31259),(24,31259),(28,31259),(1,85503),(2,85503),(3,85503),(4,85503),(5,85503),(6,85503),(7,85503),(8,85503),(9,85503),(10,85503),(11,85503),(12,85503),(13,85503),(14,85503),(15,85503),(16,85503),(17,85503),(18,85503),(19,85503),(20,85503),(21,85503),(22,85503),(23,85503),(24,85503),(25,85503),(26,85503),(27,85503),(1,85503),(2,85503),(3,85503),(4,85503),(5,85503),(6,85503),(7,85503),(8,85503),(9,85503),(10,85503),(11,85503),(12,85503),(13,85503),(14,85503),(15,85503),(16,85503),(18,85503),(19,85503),(20,85503),(21,85503),(22,85503),(23,85503),(24,85503),(25,85503),(27,85503);
/*!40000 ALTER TABLE `definiciones_in` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gallery`
--

DROP TABLE IF EXISTS `gallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gallery` (
  `id_gallery` int(11) NOT NULL AUTO_INCREMENT,
  `id_in` int(11) DEFAULT NULL,
  `tipo_gallery` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `nombre` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `titulo` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `extraAA` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id_gallery`),
  KEY `fk_inmueble_idx` (`id_in`),
  CONSTRAINT `fk_inmueble` FOREIGN KEY (`id_in`) REFERENCES `inmuebles` (`id_in`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=119 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gallery`
--

LOCK TABLES `gallery` WRITE;
/*!40000 ALTER TABLE `gallery` DISABLE KEYS */;
INSERT INTO `gallery` VALUES (56,70778,'Photos','gallery/70778/he_gl_99247.JPG','Exterior',NULL),(66,70778,'AAIMG','gallery/70778/he_gl_77822.jpg','33',NULL),(67,70778,'AAIMG','gallery/70778/he_gl_77843.jpg','31',NULL),(68,70778,'AAIMG','gallery/70778/he_gl_77854.jpg','31',NULL),(75,70778,'Photos','gallery/70778/he_gl_1678.JPG','te',''),(78,70778,'Photos','gallery/70778/he_gl_2461.JPG','dg',''),(79,70778,'Photos','gallery/70778/he_gl_2673.jpg','asf',''),(90,70778,'AAIMG','gallery/70778/he_gl_62468.JPG','29','0'),(91,70778,'AAIMG','gallery/70778/he_gl_62479.jpg','30','0'),(92,70778,'AAIMG','gallery/70778/he_gl_62493.jpg','30','0'),(93,70778,'AAIMG','gallery/70778/he_gl_63766.JPG','34','A.- X JHJ'),(94,70778,'AAIMG','gallery/70778/he_gl_63995.JPG','34','A.- X JHJ'),(95,70778,'AAIMG','gallery/70778/he_gl_67210.JPG','34','A.- X JHJ'),(96,70778,'Photos','gallery/70778/he_gl_19411.jpg','bath',''),(97,70778,'Photos','gallery/70778/he_gl_19676.png','dinner',''),(104,70778,'AAIMG','gallery/70778/he_gl_21398.jpg','32',''),(109,70778,'API','gallery/70778/he_apI_58812.jpg','Surrounding Uses',NULL),(112,70778,'API','gallery/70778/he_apI_59319.jpg','Surrounding Uses',NULL),(113,70778,'API','gallery/70778/he_apI_60441.jpg','Surrounding Uses',NULL),(114,70778,'AAIMG','gallery/70778/he_gl_46845.jpg','31',''),(115,5404,'AAIMG','gallery/5404/he_gl_6937.JPG','29',''),(116,5404,'Photos','gallery/5404/he_gl_37190.JPG','foto1',''),(117,5404,'AAIMG','gallery/5404/he_gl_37310.JPG','29',''),(118,5404,'AAIMG','gallery/5404/he_gl_37526.png','33','');
/*!40000 ALTER TABLE `gallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inmuebles`
--

DROP TABLE IF EXISTS `inmuebles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inmuebles` (
  `id_in` int(11) NOT NULL,
  `id_status` int(11) DEFAULT NULL,
  `id_type` int(11) DEFAULT NULL,
  `comparable` int(11) DEFAULT NULL,
  `effectiveDate` datetime DEFAULT NULL,
  `repNum` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `repNumCap` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `preparedFor` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `propertyOf` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `fotoPortada` varchar(25) COLLATE latin1_spanish_ci DEFAULT NULL,
  `calle` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `num` varchar(30) COLLATE latin1_spanish_ci DEFAULT NULL,
  `col` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `mun` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `edo` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `cp` int(11) DEFAULT NULL,
  `exchange_rate` double DEFAULT NULL,
  `date_exchange_rate` datetime DEFAULT NULL,
  `fecha_alta` datetime DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  `modificado_por` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `correo` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `origen_name` int(11) DEFAULT '0',
  `concepto_name` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `clave_ano_name` int(11) NOT NULL,
  `consecutivo_name` int(11) NOT NULL,
  `fecha_name` datetime DEFAULT NULL,
  `indicated_value_CLS` double NOT NULL DEFAULT '0',
  `indicated_value_CRL` double NOT NULL DEFAULT '0',
  `indicated_value_CRS` double NOT NULL DEFAULT '0',
  `address_jll` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `mt` int(11) NOT NULL DEFAULT '0',
  `et` int(11) NOT NULL DEFAULT '0',
  `appraisal_num` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id_in`),
  UNIQUE KEY `id_in_UNIQUE` (`id_in`,`correo`),
  KEY `fk_usuario_idx` (`correo`),
  KEY `id_status` (`id_status`),
  KEY `id_type` (`id_type`),
  CONSTRAINT `correo` FOREIGN KEY (`correo`) REFERENCES `usuarios` (`correo`),
  CONSTRAINT `status` FOREIGN KEY (`id_status`) REFERENCES `status_in` (`id_status`),
  CONSTRAINT `type` FOREIGN KEY (`id_type`) REFERENCES `type_in` (`id_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inmuebles`
--

LOCK TABLES `inmuebles` WRITE;
/*!40000 ALTER TABLE `inmuebles` DISABLE KEYS */;
INSERT INTO `inmuebles` VALUES (5404,1,NULL,NULL,'2016-07-22 00:00:00','M-MYMU-16-001_FRATERNA','Fraterna','Fraterna','Fraterna','he_fp_35479.JPG','Av. Félix U. Gomez ','1732','Juana De Arco','Monterrey','Nuevo Leon',64150,18.6022,'2016-09-07 23:33:42','2016-09-07 23:33:42','2016-09-12 02:50:01','eduardo.ms@senni.com.mx','daniel.algazi@am.jll.com',36,'MYMU',16,1,'2016-09-07 00:00:00',0,0,0,'Pedregal 57 piso 2, Col. Lomas de Chapultepec, C.P. 11000, CDMX, Mexico.',2,1,'M-MYMU-16-001'),(31259,1,NULL,NULL,'2014-11-21 00:00:00','IND-16-003_GE_MAULEC','GE Maulec','GE CAPITAL','PASTEURIZADORA MAULEC, SA DE CV','he_fp_31430.JPG','Fracción I de la Ex-Hacienda a de San An','s/n','Nopalucan de la Granja','Tepeaca','Puebla',11000,13.6115,'2016-08-10 23:54:36','2016-07-22 20:05:52','2016-08-11 00:13:32','daniel.algazi@am.jll.com','daniel.algazi@am.jll.com',35,'IND',16,3,'2016-08-25 00:00:00',0,0,0,'Pedregal 57 piso 2, Col. Lomas de Chapultepec, C.P. 11000, CDMX, Mexico.',1,1,'IND-16-003'),(63153,1,NULL,NULL,'2016-02-24 00:00:00','sdaf',NULL,'test','test2','logoJLL.jpg','test','2','asd','as','fd',12,17.8561,'2016-06-24 03:30:39','2016-02-25 02:21:10','2016-08-02 03:07:02','eduardo.ms@senni.com.mx','eduardo.ms@senni.com.mx',35,'IND',16,2,'0000-00-00 00:00:00',0,0,0,NULL,0,0,'IND-16-002'),(70778,2,NULL,NULL,'2016-02-29 00:00:00','IND-16-004_GE_CAPITAL_POLANCO','ge capital polanco','GE CAPITÁL, S.A. DE C.V','ABA, S. A. de CV','he_fp_96667.jpg','Horacio','12134','Polanco','Alvaro Obregón','Ciudad de México',12345,17.8561,'2016-06-27 23:03:39','2016-02-17 02:00:09','2016-08-05 14:46:18','eduardo.ms@senni.com.mx','alfredo.giorgana@am.jll.com',35,'IND',16,4,'2016-07-05 00:00:00',91.663785,4.140000000000001,0,'Pedregal 57 piso 2, Col. Lomas de Chapultepec, C.P. 11000, CDMX, Mexico.',1,1,'IND-16-004'),(85503,1,NULL,NULL,'2016-09-27 00:00:00','IND-16-005_IND16-008_NOGAL_GUADALAJARA_2','IND16-008 Nogal Guadalajara 27 09','Engenium Capital','Antonio Ornelas Cortés','he_fp_85741','Av. 20 de Noviembre','934','Nuevo Fuerte','México','CDMX',47899,18.102,'2016-09-29 17:47:52','2016-09-29 17:47:52','2016-09-30 19:55:16','oscar.alonso@am.jll.com','oscar.alonso@am.jll.com',35,'IND',16,5,NULL,11.03,0,0,'Pedregal 57 piso 2, Col. Lomas de Chapultepec, C.P. 11000, CDMX, Mexico.',2,2,'IND-16-005');
/*!40000 ALTER TABLE `inmuebles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `letter_in`
--

DROP TABLE IF EXISTS `letter_in`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `letter_in` (
  `id_in` int(11) NOT NULL,
  `client` varchar(80) COLLATE latin1_spanish_ci DEFAULT NULL,
  `client_addres` mediumtext COLLATE latin1_spanish_ci,
  `client_addres_2` varchar(80) COLLATE latin1_spanish_ci DEFAULT NULL,
  `client_logo` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `atn_officer` varchar(80) COLLATE latin1_spanish_ci DEFAULT NULL,
  `jd_atn_officer` varchar(80) COLLATE latin1_spanish_ci DEFAULT NULL,
  `jd_atn_officer_2` varchar(80) COLLATE latin1_spanish_ci DEFAULT NULL,
  `parrafo1` longtext COLLATE latin1_spanish_ci,
  `texto1` varchar(80) COLLATE latin1_spanish_ci DEFAULT NULL,
  `texto2` varchar(80) COLLATE latin1_spanish_ci DEFAULT NULL,
  `texto3` varchar(80) COLLATE latin1_spanish_ci DEFAULT NULL,
  `parrafo2` longtext COLLATE latin1_spanish_ci,
  `parrafo3` longtext COLLATE latin1_spanish_ci,
  `firma_prin` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `firma_sec` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `LTchbox1` varchar(2) COLLATE latin1_spanish_ci DEFAULT NULL,
  `LTchbox2` varchar(2) COLLATE latin1_spanish_ci DEFAULT NULL,
  `LTchbox3` varchar(2) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id_in`),
  UNIQUE KEY `id_in` (`id_in`),
  CONSTRAINT `inmueblesPK` FOREIGN KEY (`id_in`) REFERENCES `inmuebles` (`id_in`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `letter_in`
--

LOCK TABLES `letter_in` WRITE;
/*!40000 ALTER TABLE `letter_in` DISABLE KEYS */;
INSERT INTO `letter_in` VALUES (31259,'GE CAPITAL','Av. Antonio Dovalí Jaime 70, Tower C, 5th floor, Santa Fe, DF 01210','Mexico','he_cl_38835.JPG','Attn: Nicolás Santacruz','Associate- Underwriting',' ','In accordance whit your request, we are presenting the Real Estate Appraisal Report of the Industrial Building located in ','Market value of the property:','Value in use:','Liquidation value of property:','<p style=\"text-align: justify;\">The analysis contained in this appraisal is based upon the assumptions, and estimations that are subject to uncertainty and variation. These estimations are often based on data obtained on interviews with third parties, and such data are not always completely reliable.</p>\n<p style=\"text-align: justify;\">The following appraisal report has been prepared in conformance with, the guidelines and recommendations established in the Uniform Standards of Professional Appraisal Practice (USPAP), the requirements of the Code of Professional Ethics and Standards of Professional Appraisal Practice of the Appraisal Institute as well as the International Valuation Standards (IVS).</p>\n<p style=\"text-align: justify;\">Except as may be otherwise stated in the letter of engagement the report may not be used by any other person(s) other than the intended user(s) or for purposes other than the intended use. &nbsp;</p>\n<p style=\"text-align: justify;\">JLL will not be responsible for unauthorized uses of this report, its conclusions or contents used partially or in its entirety if it is delivered to non-intended users.</p>\n<p style=\"text-align: justify;\">We did not ascertain the legal, engineering, and regulatory requirements applicable to the property, including zoning and other state and local government regulations, permits and licenses. No effort has been made to determine the possible effect on the property.</p>\n<p style=\"text-align: justify;\">We take no responsibility for any events, conditions, or circumstances affecting the market that exists subsequent to the last day of our fieldwork, represented by the effective date of the appraisal.</p>\n<p style=\"text-align: justify;\">This letter is invalid as an opinion of value if detached from the report, which contains the text, exhibits, and addenda.</p>','<p style=\"text-align: justify;\">We did not ascertain the legal, engineering, and regulatory requirements applicable to the property, including zoning and other state and local government regulations, permits and licenses. No effort has been made to determine the possible effect on the property.</p>\n<p style=\"text-align: justify;\">We take no responsibility for any events, conditions, or circumstances affecting the market that exists subsequent to the last day of our fieldwork, represented by the effective date of the appraisal.</p>\n<p style=\"text-align: justify;\">This letter is invalid as an opinion of value if detached from the report, which contains the text, exhibits, and addenda.</p>','daniel.algazi@am.jll.com','alfredo.giorgana@am.jll.com','1','1','1'),(63153,'test','',' ','he_cl_21533.png','Attn: ','',' ','In accordance whit your request, we are presenting the Real Estate Appraisal Report of the Industrial Building located in ','Market value of the property:','Value in use:','Liquidation value of property:','<p style=\"text-align: justify;\">The analysis contained in this appraisal is based upon the assumptions, and estimations that are subject to uncertainty and variation. These estimations are often based on data obtained on interviews with third parties, and such data are not always completely reliable.</p>\n<p style=\"text-align: justify;\">The following appraisal report has been prepared in conformance with, the guidelines and recommendations established in the Uniform Standards of Professional Appraisal Practice (USPAP), the requirements of the Code of Professional Ethics and Standards of Professional Appraisal Practice of the Appraisal Institute as well as the International Valuation Standards (IVS).</p>\n<p style=\"text-align: justify;\">Except as may be otherwise stated in the letter of engagement the report may not be used by any other person(s) other than the intended user(s) or for purposes other than the intended use. &nbsp;</p>\n<p style=\"text-align: justify;\">JLL will not be responsible for unauthorized uses of this report, its conclusions or contents used partially or in its entirety if it is delivered to non-intended users.</p>\n<p style=\"text-align: justify;\">We did not ascertain the legal, engineering, and regulatory requirements applicable to the property, including zoning and other state and local government regulations, permits and licenses. No effort has been made to determine the possible effect on the property.</p>\n<p style=\"text-align: justify;\">We take no responsibility for any events, conditions, or circumstances affecting the market that exists subsequent to the last day of our fieldwork, represented by the effective date of the appraisal.</p>\n<p style=\"text-align: justify;\">This letter is invalid as an opinion of value if detached from the report, which contains the text, exhibits, and addenda.</p>','<p style=\"text-align: justify;\">We did not ascertain the legal, engineering, and regulatory requirements applicable to the property, including zoning and other state and local government regulations, permits and licenses. No effort has been made to determine the possible effect on the property.</p>\n<p style=\"text-align: justify;\">We take no responsibility for any events, conditions, or circumstances affecting the market that exists subsequent to the last day of our fieldwork, represented by the effective date of the appraisal.</p>\n<p style=\"text-align: justify;\">This letter is invalid as an opinion of value if detached from the report, which contains the text, exhibits, and addenda.</p>','alfredo.giorgana@am.jll.com','daniel.algazi@am.jll.com','0','0','0'),(70778,'GE CAPITÁL, Real State','Antonio Dovali Jaime 70, 5 piso,','Torre C, Alvaro Obregon, 01210, CDMX','he_cl_19234.jpg','Nicolas Santacruz','Associate Underwritting','Risk Managment','In accordance whit your request, we are presenting the Real Estate Appraisal Report of the Industrial Building located in','Market value of the property:','Value in use:','Liquidation value of property:','<p style=\"text-align: justify;\">The analysis contained in this appraisal is based upon the assumptions, and estimations that are subject to uncertainty and variation. These estimations are often based on data obtained on interviews with third parties, and such data are not always completely reliable.</p>\n<p style=\"text-align: justify;\">The following appraisal report has been prepared in conformance with, the guidelines and recommendations established in the Uniform Standards of Professional Appraisal Practice (USPAP), the requirements of the Code of Professional Ethics and Standards of Professional Appraisal Practice of the Appraisal Institute as well as the International Valuation Standards (IVS).</p>\n<p style=\"text-align: justify;\">Except as may be otherwise stated in the letter of engagement the report may not be used by any other person(s) other than the intended user(s) or for purposes other than the intended use. &nbsp;</p>\n<p style=\"text-align: justify;\">JLL will not be responsible for unauthorized uses of this report, its conclusions or contents used partially or in its entirety if it is delivered to non-intended users.</p>\n','<p style=\"text-align: justify;\">We did not ascertain the legal, engineering, and regulatory requirements applicable to the property, including zoning and other state and local government regulations, permits and licenses. No effort has been made to determine the possible effect on the property.</p>\n<p style=\"text-align: justify;\">We take no responsibility for any events, conditions, or circumstances affecting the market that exists subsequent to the last day of our fieldwork, represented by the effective date of the appraisal.</p>\n<p style=\"text-align: justify;\">This letter is invalid as an opinion of value if detached from the report, which contains the text, exhibits, and addenda.</p>','oscar.alonso@am.jll.com','alfredo.giorgana@am.jll.com','1','0','0'),(85503,'Engenium Capital','Antonio Dovalí Jaime 70, Torre C, Piso 5',' Santa Fé, 01210. México, D.F., México.','he_cl_85867.JPG','Attn: Nicolás Santacruz','',' Associate Valuation and Underwriting','In accordance whit your request, we are presenting the Real Estate Appraisal Report of the Industrial Building located in ','Market value of the property:','Value in use:','Liquidation value of property:','<p style=\"text-align: justify;\">The analysis contained in this appraisal is based upon the assumptions, and estimations that are subject to uncertainty and variation. These estimations are often based on data obtained on interviews with third parties, and such data are not always completely reliable.</p>\n<p style=\"text-align: justify;\">The following appraisal report has been prepared in conformance with, the guidelines and recommendations established in the Uniform Standards of Professional Appraisal Practice (USPAP), the requirements of the Code of Professional Ethics and Standards of Professional Appraisal Practice of the Appraisal Institute as well as the International Valuation Standards (IVS).</p>\n<p style=\"text-align: justify;\">Except as may be otherwise stated in the letter of engagement the report may not be used by any other person(s) other than the intended user(s) or for purposes other than the intended use. &nbsp;</p>\n<p style=\"text-align: justify;\">JLL will not be responsible for unauthorized uses of this report, its conclusions or contents used partially or in its entirety if it is delivered to non-intended users.</p>\n<p style=\"text-align: justify;\">We did not ascertain the legal, engineering, and regulatory requirements applicable to the property, including zoning and other state and local government regulations, permits and licenses. No effort has been made to determine the possible effect on the property.</p>\n<p style=\"text-align: justify;\">We take no responsibility for any events, conditions, or circumstances affecting the market that exists subsequent to the last day of our fieldwork, represented by the effective date of the appraisal.</p>\n<p style=\"text-align: justify;\">This letter is invalid as an opinion of value if detached from the report, which contains the text, exhibits, and addenda.</p>','','alfredo.giorgana@am.jll.com','oscar.alonso@am.jll.com','1','0','1');
/*!40000 ALTER TABLE `letter_in` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leyendas`
--

DROP TABLE IF EXISTS `leyendas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leyendas` (
  `id_leyenda` int(11) NOT NULL AUTO_INCREMENT,
  `orden` int(11) NOT NULL,
  `campo` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `leyenda` longtext COLLATE latin1_spanish_ci NOT NULL,
  `leyenda_esp` longtext COLLATE latin1_spanish_ci,
  `seccion` varchar(10) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`id_leyenda`)
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leyendas`
--

LOCK TABLES `leyendas` WRITE;
/*!40000 ALTER TABLE `leyendas` DISABLE KEYS */;
INSERT INTO `leyendas` VALUES (1,0,'client','Client',NULL,'LT'),(2,1,'client_addres','Client Address',NULL,'LT'),(3,4,'atn_officer','Attn: ',NULL,'LT'),(4,7,'parrafo1','In accordance whit your request, we are presenting the Real Estate Appraisal Report of the Industrial Building located in ',NULL,'LT'),(5,8,'texto1','Market value of the property:',NULL,'LT'),(6,9,'texto2','Value in use:',NULL,'LT'),(7,11,'parrafo2','<p style=\"text-align: justify;\">The analysis contained in this appraisal is based upon the assumptions, and estimations that are subject to uncertainty and variation. These estimations are often based on data obtained on interviews with third parties, and such data are not always completely reliable.</p>\n<p style=\"text-align: justify;\">The following appraisal report has been prepared in conformance with, the guidelines and recommendations established in the Uniform Standards of Professional Appraisal Practice (USPAP), the requirements of the Code of Professional Ethics and Standards of Professional Appraisal Practice of the Appraisal Institute as well as the International Valuation Standards (IVS).</p>\n<p style=\"text-align: justify;\">Except as may be otherwise stated in the letter of engagement the report may not be used by any other person(s) other than the intended user(s) or for purposes other than the intended use. &nbsp;</p>\n<p style=\"text-align: justify;\">JLL will not be responsible for unauthorized uses of this report, its conclusions or contents used partially or in its entirety if it is delivered to non-intended users.</p>\n<p style=\"text-align: justify;\">We did not ascertain the legal, engineering, and regulatory requirements applicable to the property, including zoning and other state and local government regulations, permits and licenses. No effort has been made to determine the possible effect on the property.</p>\n<p style=\"text-align: justify;\">We take no responsibility for any events, conditions, or circumstances affecting the market that exists subsequent to the last day of our fieldwork, represented by the effective date of the appraisal.</p>\n<p style=\"text-align: justify;\">This letter is invalid as an opinion of value if detached from the report, which contains the text, exhibits, and addenda.</p>',NULL,'LT'),(13,0,'title','valuation report',NULL,'FP'),(14,0,'preparedFor','Prepared for:',NULL,'FP'),(15,0,'propertyOf','property of:',NULL,'FP'),(16,0,'location','LOCATION OF THE PROPERTY',NULL,'FP'),(17,0,'effectiveDate','Effective date of appraisal:',NULL,'FP'),(18,0,'reportNumber','Report number: ',NULL,'FP'),(19,0,'SWParrafo1','<p style=\"text-align: justify;\">The scope of work refers to the manner and extent to which the identification, inspection, investigation, data collection and analysis were carried out, pertaining to the purpose and use of this report.</p>\n<p style=\"text-align: justify;\">In order to reach the opinion of the market value of the subject property, the following was carried out:</p>',NULL,'SW'),(20,1,'SWTit1','Analysis of the neighborhood',NULL,'SW'),(21,2,'SWText1','Includes an analysis of the neighborhood, city, and region in general; More specifically, those areas include the subject neighborhood, defined as',NULL,'SW'),(22,3,'SWTit2','Identification of the property',NULL,'SW'),(23,4,'SWText2_1','The subject property is located on',NULL,'SW'),(24,5,'SWText2_2',', and was identified through physical and electronic documentation provided by ',NULL,'SW'),(25,7,'SWText2_3','which consisted of: property layouts, property tax bill, and property deeds, also the property was located on internet by an aerial photograph. ',NULL,'SW'),(26,6,'providedBy','',NULL,'SW'),(27,8,'SWTit3','Inspection of the property',NULL,'SW'),(28,9,'SWText3_1','The property was inspected on ',NULL,'SW'),(29,10,'dateInspection','',NULL,'SW'),(30,11,'inspector','',NULL,'SW'),(31,12,'SWText3_2','and the inspection consisted on a site tour, however, we get permission to access to the areas indicated in the property plan (II.- Subject Property)',NULL,'SW'),(32,13,'SWTit4','Investigation performed',NULL,'SW'),(33,14,'SWText4_1','The market research was carried out using information provided to us directly by real estate brokers such as',NULL,'SW'),(34,15,'SWText4_2','',NULL,'SW'),(36,5,'jd_atn_officer','Job Description Attn',NULL,'LT'),(37,3,'client_logo','No Logo',NULL,'LT'),(39,10,'texto3','Liquidation value of property:',NULL,'LT'),(41,12,'parrafo3','<p style=\"text-align: justify;\">We did not ascertain the legal, engineering, and regulatory requirements applicable to the property, including zoning and other state and local government regulations, permits and licenses. No effort has been made to determine the possible effect on the property.</p>\n<p style=\"text-align: justify;\">We take no responsibility for any events, conditions, or circumstances affecting the market that exists subsequent to the last day of our fieldwork, represented by the effective date of the appraisal.</p>\n<p style=\"text-align: justify;\">This letter is invalid as an opinion of value if detached from the report, which contains the text, exhibits, and addenda.</p>',NULL,'LT'),(43,6,'jd_atn_officer_2',' ',NULL,'LT'),(44,2,'client_addres_2',' ',NULL,'LT'),(45,16,'SWText4_3','among others.',NULL,'SW'),(46,17,'SWTit5','Analysis performed',NULL,'SW'),(47,18,'SWText5','<p style=\"text-align: justify;\">The subject is located in an area where the use is predominantly industrial and it has a specific use of industrial.</p>\n<p style=\"text-align: justify;\">The study includes the application of the sales comparison approach in order to give an opinion of the land value. The cost approach and the income capitalization approaches are applied, and also a DCF is performed. Finally an opinion of the value in use is included. The final opinion of value of the property is the result of a weighted average of the indication of values of the mentioned methodologies.</p>\n<p style=\"text-align: justify;\">The subject property is a special purpose property that was designed and built for its current use. If the property is sold for a user with the same production requirements, the \"value in use\" based on the cost approach has to be considered. some Because of the above, the present report provides also an opinion of the value in use based on the cost approach without considering any type of functional or external obsolescence. In addition, upon specific request of the client, the liquidation value was obtained, which considers the property sale under atypical conditions within a short time period and with the seller being under extreme compulsion to sell.</p>',NULL,'SW'),(48,19,'SWTit6','Extraordinary Assumption',NULL,'SW'),(49,20,'SWText6','All property information provided by the client for the subject including sizes, depictions, entitlements, utilities, plans, development costs, etc. as presented in this report are assumed to be correct and reliable.',NULL,'SW'),(50,21,'SWTit7','Hypothetical Condition',NULL,'SW'),(51,22,'SWText7','None noted',NULL,'SW'),(52,0,'SMTit1','Property appraised:',NULL,'SM'),(53,1,'SMText1','industrial warehouse, adapted for refrigerated and frozen storage, comprised of: refrigerated and frozen storage chambers, maneuver yard, parking yard, administrative offices and loading docks. ',NULL,'SM'),(55,2,'SMTit2','Current use:',NULL,'SM'),(56,3,'SMText2','',NULL,'SM'),(57,4,'SMTit3','Purpose of the appraisal:',NULL,'SM'),(58,5,'SMText3','To give an opinion of the market value, the value in use and the liquidation value of the subject property.',NULL,'SM'),(59,6,'SMTit4','Intended use of appraisal:',NULL,'SM'),(60,7,'SMText4','Loan purposes',NULL,'SM'),(61,8,'SMTit5','Current use:',NULL,'SM'),(62,9,'SMText5','',NULL,'SM'),(63,10,'SMTit6','Highest and best use of the land as if vacant:',NULL,'SM'),(64,11,'SMText6','',NULL,'SM'),(65,12,'SMTit7','Highest and best use of the land with improvements:',NULL,'SM'),(66,13,'SMText7','',NULL,'SM'),(67,14,'SMvc1_1','As is Market Value',NULL,'SM'),(68,15,'SMvc1_2','Fee Simple Estate',NULL,'SM'),(69,16,'SMvc1_3','',NULL,'SM'),(70,17,'SMvc1_4','',NULL,'SM'),(71,18,'SMvc2_1','Value in Use',NULL,'SM'),(72,19,'SMvc2_2','Fee Simple Estate',NULL,'SM'),(73,20,'SMvc2_3','',NULL,'SM'),(74,21,'SMvc2_4','',NULL,'SM'),(75,22,'SMvc3_1','Liquidation Value',NULL,'SM'),(76,23,'SMvc3_2','Fee Simple Estate',NULL,'SM'),(77,24,'SMvc3_3','',NULL,'SM'),(78,25,'SMvc3_4','',NULL,'SM'),(79,26,'SMvc4_1','',NULL,'SM'),(80,27,'SMvc4_2','',NULL,'SM'),(81,28,'SMvc4_3','',NULL,'SM'),(82,29,'SMvc4_4','',NULL,'SM'),(83,30,'SMvc5_1','',NULL,'SM'),(84,31,'SMvc5_2','',NULL,'SM'),(85,32,'SMvc5_3','',NULL,'SM'),(86,33,'SMvc5_4','',NULL,'SM'),(87,34,'SM_ch_vc1','',NULL,'SM'),(88,35,'SM_ch_vc2','',NULL,'SM'),(89,36,'SM_ch_vc3','',NULL,'SM'),(90,37,'SM_ch_vc4','',NULL,'SM'),(91,38,'SM_ch_vc5','',NULL,'SM');
/*!40000 ALTER TABLE `leyendas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `relevant_ch`
--

DROP TABLE IF EXISTS `relevant_ch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relevant_ch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `campo` varchar(30) COLLATE latin1_spanish_ci NOT NULL,
  `v_1` double NOT NULL DEFAULT '0',
  `v_2` double NOT NULL DEFAULT '0',
  `v_3` double NOT NULL DEFAULT '0',
  `v_4` double NOT NULL DEFAULT '0',
  `v_5` double NOT NULL DEFAULT '0',
  `id_in` int(11) DEFAULT NULL,
  `tipo_comp` varchar(3) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=137 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relevant_ch`
--

LOCK TABLES `relevant_ch` WRITE;
/*!40000 ALTER TABLE `relevant_ch` DISABLE KEYS */;
INSERT INTO `relevant_ch` VALUES (1,'rc1','1) Property rights conveyed',0,0,0,0,0,NULL,'CLS'),(2,'rc2','2) Financing terms',0,0,0,0,0,NULL,'CLS'),(3,'rc3','3) Conditions of sale',0,0,0,0,0,NULL,'CLS'),(4,'rc4','4) Market Conditions',0,0,0,0,0,NULL,'CLS'),(5,'rc5','5) Location',-10,-5,0,5,10,NULL,'CLS'),(6,'rc6_0','Size (m2) (ft2)',90,0,0,0,0,NULL,'CLS'),(7,'rc6_1','Frontage',-10,-5,0,5,10,NULL,'CLS'),(8,'rc6_2','Access',-10,-5,0,5,10,NULL,'CLS'),(9,'rc6_3','Shape',-10,-5,0,5,10,NULL,'CLS'),(10,'rc6_4','Utilities',-10,-5,0,5,10,NULL,'CLS'),(11,'rc6_5','Topography',-10,-5,0,5,10,NULL,'CLS'),(12,'rc6_6','Depth radio',-10,-5,0,5,10,NULL,'CLS'),(13,'rc6_7','Flood hazard',-10,-5,0,5,10,NULL,'CLS'),(14,'rc6_8','Easements',-10,-5,0,5,10,NULL,'CLS'),(15,'rc6_9','Other 1',-10,-5,0,5,10,NULL,'CLS'),(16,'rc6_10','Other 2',-10,-5,0,5,10,NULL,'CLS'),(17,'rc7','7) Use (zoning)',-10,-5,0,5,10,NULL,'CLS'),(18,'rc8','8) Listing / Sale',-10,0,0,0,0,NULL,'CLS'),(19,'rc1','1) Property rights conveyed',0,0,0,0,0,NULL,'CRL'),(20,'rc2','2) Financing terms',0,0,0,0,0,NULL,'CRL'),(25,'rc3','3) Conditions of sale',0,0,0,0,0,NULL,'CRL'),(26,'rc4','4) Financing terms',0,0,0,0,0,NULL,'CRL'),(27,'rc5','5) Location',-15,-10,0,10,15,NULL,'CRL'),(28,'rc6_0','Size (m2) (ft2)',90,0,0,5,10,NULL,'CRL'),(29,'rc6_1','Condition',-10,-5,0,5,10,NULL,'CRL'),(30,'rc6_2','Functional Utility',-10,-5,0,5,10,NULL,'CRL'),(31,'rc6_3','Construction Quality',-10,-5,0,5,10,NULL,'CRL'),(32,'rc6_4','Obsolescence',-10,-5,0,5,10,NULL,'CRL'),(33,'rc6_5','Age and Maintenance',-10,-5,0,5,10,NULL,'CRL'),(34,'rc6_9','Other 1',-10,-5,0,5,10,NULL,'CRL'),(35,'rc6_10','Other 2',-10,-5,0,5,10,NULL,'CRL'),(36,'rc7','7) Zoning',-10,-5,0,5,10,NULL,'CRL'),(37,'rc8','8) Listing / Real Rent',-10,0,0,5,10,NULL,'CRL'),(38,'rc1','1) Property rights conveyed',0,0,0,0,0,NULL,'CRS'),(39,'rc2','2) Financing terms',0,0,0,0,0,NULL,'CRS'),(40,'rc3','3) Conditions of sale',0,0,0,0,0,NULL,'CRS'),(41,'rc4','4) Financing terms',0,0,0,0,0,NULL,'CRS'),(42,'rc5','5) Location',-15,-10,0,10,15,NULL,'CRS'),(43,'rc6_0','Size (m2) (ft2)',90,0,0,5,10,NULL,'CRS'),(44,'rc6_1','Condition',-10,-5,0,5,10,NULL,'CRS'),(45,'rc6_2','Functional Utility',-10,-5,0,5,10,NULL,'CRS'),(46,'rc6_3','Construction Quality',-10,-5,0,5,10,NULL,'CRS'),(47,'rc6_4','Obsolescence',-10,-5,0,5,10,NULL,'CRS'),(48,'rc6_5','Age and Maintenance',-10,-5,0,5,10,NULL,'CRS'),(49,'rc6_9','Other 1',-10,-5,0,5,10,NULL,'CRS'),(50,'rc6_10','Other 2',-10,-5,0,5,10,NULL,'CRS'),(51,'rc7','7) Zoning',-10,-5,0,5,10,NULL,'CRS'),(52,'rc8','8) Listing / Real Rent',-10,0,0,5,10,NULL,'CRS'),(89,'rc1','1) Property rights conveyed',0,0,0,0,0,70778,'CLS'),(90,'rc2','2) Financing terms',0,0,0,0,0,70778,'CLS'),(91,'rc3','3) Conditions of sale',0,0,0,0,0,70778,'CLS'),(92,'rc4','4) Market Conditions',0,0,0,0,0,70778,'CLS'),(93,'rc5','5) Location',-10,-5,0,5,10,70778,'CLS'),(94,'rc6_0','Size (m2) (ft2)',90,0,0,0,0,70778,'CLS'),(95,'rc6_1','Frontage',-10,-5,0,5,10,70778,'CLS'),(96,'rc6_2','Access',-10,-5,0,5,10,70778,'CLS'),(97,'rc6_3','Shape',-10,-5,0,5,10,70778,'CLS'),(98,'rc6_4','Utilities',-10,-5,0,5,10,70778,'CLS'),(99,'rc6_5','Topography',-10,-5,0,5,10,70778,'CLS'),(100,'rc6_6','Depth radio',-10,-5,0,5,10,70778,'CLS'),(101,'rc6_7','Flood hazard ',-10,-5,0,5,10,70778,'CLS'),(102,'rc6_8','Easements',-10,-5,0,5,10,70778,'CLS'),(103,'rc6_9','Other 1',-10,-5,0,5,10,70778,'CLS'),(104,'rc6_10','Other 2',-10,-5,0,5,10,70778,'CLS'),(105,'rc7','7) Use (zoning)',-10,-5,0,5,10,70778,'CLS'),(106,'rc8','8) Listing / Sale',-15,-10,0,0,0,70778,'CLS'),(122,'rc1','1) Property rights conveyed',0,0,0,0,0,70778,'CRL'),(123,'rc2','2) Financing terms',0,0,0,0,0,70778,'CRL'),(124,'rc3','3) Conditions of sale',0,0,0,0,0,70778,'CRL'),(125,'rc4','4) Market Conditions',0,0,0,0,0,70778,'CRL'),(126,'rc5','5) Location',-15,-10,0,10,15,70778,'CRL'),(127,'rc6_0','Size (m2) (ft2)',95,0,0,0,0,70778,'CRL'),(128,'rc6_1','Frontage',-10,-5,0,5,10,70778,'CRL'),(129,'rc6_2','Access',-10,-5,0,5,10,70778,'CRL'),(130,'rc6_3','Shape',-10,-5,0,5,10,70778,'CRL'),(131,'rc6_4','Utilities',-10,-5,0,5,10,70778,'CRL'),(132,'rc6_5','Topography',-10,-5,0,5,10,70778,'CRL'),(133,'rc6_9','Other 1',-10,-5,0,5,10,70778,'CRL'),(134,'rc6_10','Other 2',-10,-5,0,5,10,70778,'CRL'),(135,'rc7','Use (zoning)',-10,-5,0,5,10,70778,'CRL'),(136,'rc8','Listing / Sale',-5,0,0,0,0,70778,'CRL');
/*!40000 ALTER TABLE `relevant_ch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `scopeofwork_in`
--

DROP TABLE IF EXISTS `scopeofwork_in`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `scopeofwork_in` (
  `id_in` int(11) NOT NULL,
  `SWparrafo1` longtext COLLATE latin1_spanish_ci,
  `SWTit1` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `SWText1` mediumtext COLLATE latin1_spanish_ci,
  `SWTit2` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `SWText2_1` mediumtext COLLATE latin1_spanish_ci,
  `SWText2_2` mediumtext COLLATE latin1_spanish_ci,
  `providedBy` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `SWText2_3` mediumtext COLLATE latin1_spanish_ci,
  `SWTit3` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `SWText3_1` text COLLATE latin1_spanish_ci,
  `dateInspection` date DEFAULT NULL,
  `inspector` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `SWText3_2` mediumtext COLLATE latin1_spanish_ci,
  `SWTit4` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `SWText4_1` text COLLATE latin1_spanish_ci,
  `SWText4_2` longtext COLLATE latin1_spanish_ci,
  `SWText4_3` mediumtext COLLATE latin1_spanish_ci,
  `SWTit5` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `SWText5` longtext COLLATE latin1_spanish_ci,
  `SWTit6` varchar(60) COLLATE latin1_spanish_ci NOT NULL,
  `SWText6` mediumtext COLLATE latin1_spanish_ci NOT NULL,
  `SWTit7` varchar(60) COLLATE latin1_spanish_ci NOT NULL,
  `SWText7` mediumtext COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`id_in`),
  KEY `id_in` (`id_in`),
  CONSTRAINT `scope_in` FOREIGN KEY (`id_in`) REFERENCES `inmuebles` (`id_in`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `scopeofwork_in`
--

LOCK TABLES `scopeofwork_in` WRITE;
/*!40000 ALTER TABLE `scopeofwork_in` DISABLE KEYS */;
INSERT INTO `scopeofwork_in` VALUES (31259,'<p style=\"text-align: justify;\">The scope of work refers to the manner and extent to which the identification, inspection, investigation, data collection and analysis were carried out, pertaining to the purpose and use of this report.</p>\n<p style=\"text-align: justify;\">In order to reach the opinion of the market value of the subject property, the following was carried out:</p>','Analysis of the neighborhood','Includes an analysis of the neighborhood, city, and region in general; More specifically, those areas include the subject neighborhood, defined as','Identification of the property','The subject property is located on',', and was identified through physical and electronic documentation provided by ','Juan Perez','which consisted of: property layouts, property tax bill, and property deeds, also the property was located on internet by an aerial photograph. ','Inspection of the property','The property was inspected on ','2014-11-21','Oscar Alonso','and the inspection consisted on a site tour, however, we get permission to access to the areas indicated in the property plan (II.- Subject Property)','Investigation performed','The market research was carried out using information provided to us directly by real estate brokers such as','Edgar Parra, Jaime R','among others.','Analysis performed','<p style=\"text-align: justify;\">The subject is located in an area where the use is predominantly industrial and it has a specific use of industrial.</p>\n<p style=\"text-align: justify;\">The study includes the application of the sales comparison approach in order to give an opinion of the land value. The cost approach and the income capitalization approaches are applied, and also a DCF is performed. Finally an opinion of the value in use is included. The final opinion of value of the property is the result of a weighted average of the indication of values of the mentioned methodologies.</p>\n<p style=\"text-align: justify;\">The subject property is a special purpose property that was designed and built for its current use. If the property is sold for a user with the same production requirements, the \"value in use\" based on the cost approach has to be considered. some Because of the above, the present report provides also an opinion of the value in use based on the cost approach without considering any type of functional or external obsolescence. In addition, upon specific request of the client, the liquidation value was obtained, which considers the property sale under atypical conditions within a short time period and with the seller being under extreme compulsion to sell.</p>','Extraordinary Assumption','All property information provided by the client for the subject including sizes, depictions, entitlements, utilities, plans, development costs, etc. as presented in this report are assumed to be correct and reliable.','Hypothetical Condition','None noted'),(70778,'<p style=\"text-align: justify;\">The scope of work refers to the manner and extent to which the identification, inspection, investigation, data collection and analysis were carried out, pertaining to the purpose and use of this report.</p>\n<p style=\"text-align: justify;\">In order to reach the opinion of the market value of the subject property, the following was carried out:</p>','Analysis of the neighborhood','Includes an analysis of the neighborhood, city, and region in general; More specifically, those areas include the subject neighborhood, defined as','Identification of the property','The subject property is located on',', and was identified through physical and electronic documentation provided by ','Mr. Miguel Angel Cruz, Maintenance','which consisted of: property layouts, property tax bill, and property deeds, also the property was located on internet by an aerial photograph. ','Inspection of the property','The property was inspected on ','2016-06-01','Oscar Alonso','and the inspection consisted on a site tour, however, we get permission to access to the areas indicated in the property plan (II.- Subject Property)','Investigation performed','The market research was carried out using information provided to us directly by real estate brokers such as','Alfa Nevado Inmobiliaria, Bufete Logistico Inmobiliario, Century 21 Vilchis / Hugo Hernández, JLL DatabasE, Realty World Key House, Alfa Nevado Inmobiliaria, Bufete Logistico Inmobiliario, Century 21 Vilchis / Hugo Hernández, JLL DatabasE, Realty World Key House, Vicarther Inmobiliaria','among others.','Analysis performed','<p style=\"text-align: justify;\">The subject is located in an area where the use is predominantly industrial and it has a specific use of industrial.</p>\n<p style=\"text-align: justify;\">The study includes the application of the sales comparison approach in order to give an opinion of the land value. The cost approach and the income capitalization approaches are applied, and also a DCF is performed. Finally an opinion of the value in use is included. The final opinion of value of the property is the result of a weighted average of the indication of values of the mentioned methodologies.</p>\n<p style=\"text-align: justify;\">The subject property is a special purpose property that was designed and built for its current use. If the property is sold for a user with the same production requirements, the \"value in use\" based on the cost approach has to be considered. some Because of the above, the present report provides also an opinion of the value in use based on the cost approach without considering any type of functional or external obsolescence. In addition, upon specific request of the client, the liquidation value was obtained, which considers the property sale under atypical conditions within a short time period and with the seller being under extreme compulsion to sell.</p>','Extraordinary Assumption','All property information provided by the client for the subject including sizes, depictions, entitlements, utilities, plans, development costs, etc. as presented in this report are assumed to be correct and reliable.','Hypothetical Condition','None noted'),(85503,'<p style=\"text-align: justify;\">The scope of work refers to the manner and extent to which the identification, inspection, investigation, data collection and analysis were carried out, pertaining to the purpose and use of this report.</p>\n<p style=\"text-align: justify;\">In order to reach the opinion of the market value of the subject property, the following was carried out:</p>','Analysis of the neighborhood','Includes an analysis of the neighborhood, city, and region in general; More specifically, those areas include the subject neighborhood, defined as','Identification of the property','The subject property is located on',', and was identified through physical and electronic documentation provided by ','Ms. Patricia Perez','which consisted of: property layouts, property tax bill, and property deeds, also the property was located on internet by an aerial photograph. ','Inspection of the property','The property was inspected on ','2016-09-27','Oscar Alonso','and the inspection consisted on a site tour, we get  indicated in the property plan (II.- Subject Property)','Investigation performed','The market research was carried out using information provided to us directly by real estate brokers such as','Century XXI','among others.','Analysis performed','<p class=\"MsoNormal\"><span style=\"font-family: \'Arial\',sans-serif;\">In valuing the subject property, the sales comparison approach was used to obtain the price of the terrain as well as to obtain prices of similar properties for sale.&nbsp; These comparable properties were researched and abstracted through brokers&rsquo; information. Also, a personal inspection of the areas where those properties are located was performed. </span></p>\n<p class=\"MsoNormal\"><span style=\"font-family: \'Arial\',sans-serif;\">The subject is located in an area where the use is predominantly Industrial; the subject has specific use \"Heavy Industry\" (Industria Pesada T-3). The study will include the cost approach and the income approach, and the opinion of value will be a weighted average of both approaches.</span></p>\n<p class=\"MsoNormal\"><span style=\"font-family: \'Arial\',sans-serif;\">The subject to be valued is a typical special purpose property that was specially designed and constructed for its current use. If the property is sold for a user with the same production requirements, the \"Value in Use\" based on the Cost Approach should be considered. On the other hand for a typical purchaser of the property most of the improvements would not have value contribution.</span></p>\n<p class=\"MsoNormal\"><span style=\"font-family: \'Arial\',sans-serif;\">Because of the above, the present report provides the Value in Use following the Cost Approach without considering any type of functional or external obsolescence and the Value in Exchange considering the Income Approach and the Cost Approach affected by obsolescence.</span></p>\n<p class=\"MsoNormal\"><span style=\"font-family: \'Arial\',sans-serif;\">The Cost Approach consists of a set of procedures through which a value indication is derived for the fee simple interest in a property by estimating the current cost to construct a reproduction of (or replacement for) the existing structure, including an entrepreneurial incentive, deducting depreciation from the total cost, and adding the estimated land value.</span></p>\n<p class=\"MsoNormal\"><span style=\"font-family: \'Arial\',sans-serif;\">The Income approach is based on the Economic Principle of Anticipation, the Income Approach presents a value obtained in terms of its ability to generate income. Income-producing properties are typically purchased as an investment and, from the point of view of the investor, the income-generating capacity of a property is the critical element that affects the value.</span></p>\n<p class=\"MsoNormal\"><span style=\"font-family: \'Arial\',sans-serif;\">In addition, at the express request of our client, the Orderly Disposition Value was obtained, which considers the property sale under atypical conditions in a considerably shorter time that the one corresponding to the market value.</span></p>\n<p>&nbsp;</p>\n<p class=\"MsoNormal\"><span style=\"font-family: \'Arial\',sans-serif;\">We assume that the information provided by the client or otherwise obtained from our investigation was adequate from which to undertake the appraisal assignment and produce reliable and credible values conclusions.</span></p>','Extraordinary Assumption','All property information provided by the client for the subject including sizes, depictions, entitlements, utilities, plans, development costs, etc. as presented in this report are assumed to be correct and reliable.','Hypothetical Condition','None noted');
/*!40000 ALTER TABLE `scopeofwork_in` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status_in`
--

DROP TABLE IF EXISTS `status_in`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `status_in` (
  `id_status` int(11) NOT NULL,
  `descripcion` varchar(30) COLLATE latin1_spanish_ci DEFAULT NULL,
  UNIQUE KEY `id_status_2` (`id_status`),
  KEY `id_status` (`id_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status_in`
--

LOCK TABLES `status_in` WRITE;
/*!40000 ALTER TABLE `status_in` DISABLE KEYS */;
INSERT INTO `status_in` VALUES (1,'In Progress'),(2,'Draft'),(3,'Rejected'),(4,'Approved JLL'),(5,'Approved Client');
/*!40000 ALTER TABLE `status_in` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `summary_in`
--

DROP TABLE IF EXISTS `summary_in`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `summary_in` (
  `id_in` int(11) NOT NULL,
  `SMTit1` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `SMText1` mediumtext COLLATE latin1_spanish_ci,
  `SMTit2` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `SMText2` mediumtext COLLATE latin1_spanish_ci,
  `SMTit3` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `SMText3` mediumtext COLLATE latin1_spanish_ci,
  `SMTit4` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `SMText4` mediumtext COLLATE latin1_spanish_ci,
  `SMTit5` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `SMText5` mediumtext COLLATE latin1_spanish_ci,
  `SMTit6` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `SMText6` mediumtext COLLATE latin1_spanish_ci,
  `SMTit7` varchar(60) COLLATE latin1_spanish_ci DEFAULT NULL,
  `SMText7` mediumtext COLLATE latin1_spanish_ci,
  `SMvc1_1` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `SMvc1_2` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `SMvc1_3` date DEFAULT NULL,
  `SMvc1_4` double DEFAULT NULL,
  `SMvc2_1` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `SMvc2_2` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `SMvc2_3` date DEFAULT NULL,
  `SMvc2_4` double DEFAULT NULL,
  `SMvc3_1` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `SMvc3_2` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `SMvc3_3` date DEFAULT NULL,
  `SMvc3_4` double DEFAULT NULL,
  `SMvc4_1` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `SMvc4_2` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `SMvc4_3` date DEFAULT NULL,
  `SMvc4_4` double DEFAULT NULL,
  `SMvc5_1` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `SMvc5_2` varchar(40) COLLATE latin1_spanish_ci DEFAULT NULL,
  `SMvc5_3` date DEFAULT NULL,
  `SMvc5_4` double DEFAULT NULL,
  `SM_ch_vc1` varchar(2) COLLATE latin1_spanish_ci DEFAULT NULL,
  `SM_ch_vc2` varchar(2) COLLATE latin1_spanish_ci DEFAULT NULL,
  `SM_ch_vc3` varchar(2) COLLATE latin1_spanish_ci DEFAULT NULL,
  `SM_ch_vc4` varchar(2) COLLATE latin1_spanish_ci DEFAULT NULL,
  `SM_ch_vc5` varchar(2) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id_in`),
  CONSTRAINT `summary_in_ibfk_1` FOREIGN KEY (`id_in`) REFERENCES `inmuebles` (`id_in`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `summary_in`
--

LOCK TABLES `summary_in` WRITE;
/*!40000 ALTER TABLE `summary_in` DISABLE KEYS */;
INSERT INTO `summary_in` VALUES (31259,'Property appraised:','industrial warehouse, adapted for refrigerated and frozen storage, comprised of: refrigerated and frozen storage chambers, maneuver yard, parking yard, administrative offices and loading docks. ','Current use:','Industrial','Purpose of the appraisal:','To give an opinion of the market value, the value in use and the liquidation value of the subject property.','Intended use of appraisal:','Loan purposes','Current use:','Industrial','Highest and best use of the land as if vacant:','Current use ( industrial)','Highest and best use of the land with improvements:','Current use (industiral)','As is Market Value','Fee Simple Estate',NULL,0,'Value in Use','Fee Simple Estate',NULL,0,'Liquidation Value','Fee Simple Estate',NULL,0,'','',NULL,0,'','',NULL,0,'','','','0','0'),(70778,'Property appraised:','industrial warehouse, adapted for refrigerated and frozen storage, comprised of: refrigerated and frozen storage chambers, maneuver yard, parking yard, administrative offices and loading docks. ','Current use:','sd','Purpose of the appraisal:','To give an opinion of the market value, the value in use and the liquidation value of the subject property.','Intended use of appraisal:','Loan purposes','Current use:','industrial','Highest and best use of the land as if vacant:','industrial','Highest and best use of the land with improvements:','industrial use','As is Market Value','Fee Simple Estate',NULL,0,'Value in Use','Fee Simple Estate',NULL,0,'Liquidation Value','Fee Simple Estate',NULL,0,'','',NULL,0,'','',NULL,0,'','','','0','0'),(85503,'Property appraised:','Property appraised: Production Plant with rentable area of 21,241.45 m2; 228,641.06 ft2  comprised of 3 Storage bays, production plant, maneuver yard, parking yard, Silos, Administrative offices and loading docks.','Current use:','Industrial complex','Purpose of the appraisal:','To give an opinion of the market value, the value in use and the liquidation value of the subject property.','Intended use of appraisal:','Account Monitoring Purpuses','Current use:','Industrial Complex','Highest and best use of the land as if vacant:','Industrial','Highest and best use of the land with improvements:','Industrial','As is Market Value','Fee Simple Estate',NULL,0,'Value in Use','Fee Simple Estate',NULL,0,'Liquidation Value','Fee Simple Estate',NULL,0,'','',NULL,0,'','',NULL,0,'','','','0','0');
/*!40000 ALTER TABLE `summary_in` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type_in`
--

DROP TABLE IF EXISTS `type_in`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `type_in` (
  `id_type` int(11) NOT NULL,
  `descripcion` varchar(30) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_in`
--

LOCK TABLES `type_in` WRITE;
/*!40000 ALTER TABLE `type_in` DISABLE KEYS */;
INSERT INTO `type_in` VALUES (1,'Industrial'),(2,'Commercial');
/*!40000 ALTER TABLE `type_in` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `nombre` varchar(30) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `apellidos` varchar(50) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `pwd` varchar(10) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `correo` varchar(50) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `tipo` varchar(2) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `puesto` varchar(60) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `telefono` int(11) DEFAULT NULL,
  `titulo` varchar(80) DEFAULT NULL,
  `foto` varchar(20) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `fecha_alta` datetime DEFAULT NULL,
  PRIMARY KEY (`correo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES ('Alfredo','Giorgana De la Concha','4lfr3d0','alfredo.giorgana@am.jll.com','1','National Director',59808798,'MS, MAI, ASA, MRICS','tll_30468.png',NULL),('Celia','Miranda Piña','c3l14','celia.miranda@am.jll.com','2','VALUER',59808854,NULL,'tll_90163.png','2016-02-16 02:24:21'),('Daniel','Algazi Cohen','d4n13l','daniel.algazi@am.jll.com','1','SENIOR VALUER',59808856,'MBA','tll_16601.png','2016-02-18 18:30:48'),('Edu','Mussil','ney','eduardo.ms@senni.com.mx','1','TI',NULL,NULL,'tll_32810.jpg',NULL),('Héctor','Bonilla Carmona','h3ct0r','hector.bonilla@am.jll.com','2','SENIOR VALUER',59808847,'EVI','tll_30273.png',NULL),('Irina','Martinez Miranda','1r1n4','irina.martinez@am.jll.com','2','SENIOR VALUER',59808853,'ARQ','tll_30345.png',NULL),('José Luis','Gonzalez Rodriguez','j0s3','jose.gonzalez@am.jll.com','2','VALUER',59808043,NULL,'logojll.png',NULL),('Luis Angel','Figueroa Rodriguez','4ng3l','luisangel.figueroa@am.jll.com','2','VALUER',59808849,NULL,'logojll.png',NULL),('Octavio','González Rodríguez','Pedregal57','octavio.gonzalez@am.jll.com','2','Valuador',59808848,'MBA','logojll.png','2016-07-05 11:54:30'),('Oscar','Alonso Martínez','0sc4r','oscar.alonso@am.jll.com','2','SENIOR VALUER',59808850,'EVI','tll_30378.png','2016-02-18 18:32:07');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-09-30 21:38:35
