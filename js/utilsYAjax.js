// JavaScript Document

function submitForm(forma) 
{
	$("#"+forma).validationEngine();	
	if($("#"+forma).validationEngine('validate'))
	   $("#"+forma).submit();		
}

function submitFormComp(forma) 
{
	$("#"+forma).validationEngine();
	if($("#"+forma).validationEngine('validate'))
        {  $("#price_mx").prop('disabled', false);
	   $("#"+forma).submit();
       }
}

function submitFiltros(forma,registrosPagina,baseURL) 
{	
	$("#"+forma).validationEngine();        
	if($("#"+forma).validationEngine('validate'))
	  paginarInAX(1,registrosPagina,baseURL);		
}


function resetFiltros()
{
    $("#f1").val('');
    $("#f2").val('');
    $("#f3").val('');
    $("#f4").val('');
    $("#f5").val('');
    $("input[name=status][value=0]").attr('checked', 'checked');
}

function resetCompFiltros()
{
    $("#f1").val('');
    $("#f2Ini").val('');
    $("#f2Fin").val('');
    $("#f3").val('');
    $("#f4").val('');
    $("#f5").val('');
    $("#f6Ini").val('');
    $("#f6Fin").val('');
    $("input[name=tipoCompRadio][value=CLS]").attr('checked', 'checked');
}

function resetCompFiltrosReport(tipo)
{
    $("#f1"+tipo).val('');
    $("#f2Ini"+tipo).val('');
    $("#f2Fin"+tipo).val('');
    $("#f3"+tipo).val('');
    $("#f4"+tipo).val('');
    $("#f5"+tipo).val('');
    $("#f6Ini"+tipo).val('');
    $("#f6Fin"+tipo).val('');
}

function submitCompFiltros(forma,registrosPagina,baseURL,tipoComp,selectComp,id_in)
{
	$("#"+forma).validationEngine();
	if($("#"+forma).validationEngine('validate'))
        { 
            if(id_in === null)
               paginarCompAX(1,registrosPagina,baseURL,tipoComp,selectComp,id_in,null,null); 
           else
               paginarCompAX(1,registrosPagina,baseURL,tipoComp,selectComp,id_in,comparablesElegidos,comparablesElegidosBorrados); 
        }
}

function submitInAX(baseURL,forma,id_in,accion,tipo)
{
    $("#confirm"+tipo).removeClass("msjOk").removeClass("msjErr").addClass("msjconfirm").html("").show();
    $("#"+forma).validationEngine();
    if($("#"+forma).validationEngine('validate'))
    {   
        var reportParts = [];
        $("input[name='report']:checked" ).each(function() { reportParts.push($(this).val()); });
        $("input[name='ann_in']:checked" ).each(function() { reportParts.push($(this).val()); });
        $("input[name='ap_in']:checked"  ).each(function() { reportParts.push($(this).val()); });
        
        saveReportPartsAX(id_in,baseURL,reportParts);
        
        $.ajax({data      : $("#"+forma).serialize(),
                url       : baseURL+'inmueble/guardar/'+id_in+'/'+accion+'/'+tipo+'/'+$('input[name=status]:checked').val()+'/'+$('input[name=idiom]:checked').val(),
                type      : 'post',
                dataType  : 'json',
                beforeSend: function () { $("#confirm"+tipo).removeClass("msjOk").addClass("msjconfirm");
                                          $("#confirm"+tipo).html("<img src=\""+baseURL+"images/spin.png\"> Guardando seccion, espere por favor...");},
                success   : function (response) {   if ('session' in response) 
                                                      {
                                                          jAlert(response['msj'],"HERMES PROJECT VALUATION REPORT", function (ans) { window.location.href = baseURL; });
                                                          return false;
                                                      }
                                                    $("#confirm"+tipo).removeClass("msjconfirm").addClass("msjOk");
                                                    $("#confirm"+tipo).html("<img src=\""+baseURL+"images/check.png\"> Seccion guardada exitosamente");
                                                    $("#confirm"+tipo).slideUp(12800);
                                                }, 
                error     : function(xhr, textStatus, error)
                                { jAlert("Ha ocurrido un error al guardar la seccion: " + xhr.statusText + " " + textStatus+" "+error,"HERMES PROJECT VALUATION REPORT"); 
                                  $("#confirm"+tipo).removeClass("msjconfirm").removeClass("msjOk").addClass("msjErr").html("<img src=\""+baseURL+"images/close.png\">Ocurrio un problema al guardar la seccion, intentelo mas tarde");}
               });
    }
}

function submitAPV_VI_AX(baseURL,forma,id_in,accion,tipo,subseccion)
{
    $("#"+forma).validationEngine();
    if($("#"+forma).validationEngine('validate'))
    {
        var reportParts = [];
        $("input[name='report']:checked" ).each(function() { reportParts.push($(this).val()); });
        $("input[name='ann_in']:checked" ).each(function() { reportParts.push($(this).val()); });
        $("input[name='ap_in']:checked"  ).each(function() { reportParts.push($(this).val()); });
        
        saveReportPartsAX(id_in,baseURL,reportParts);
 
        $.ajax({ 
                data      : $("#"+forma).serialize(),
                url       : baseURL+'inmueble/guardar/'+id_in+'/'+accion+'/'+tipo+'/'+$('input[name=status]:checked').val()+'/'+$('input[name=idiom]:checked').val(),
                type      : 'post',
                dataType  : 'json',
                beforeSend: function () {},
                success   : function (response) {
                                          if ('session' in response) 
                                            {
                                                jAlert(response['msj'],"HERMES PROJECT VALUATION REPORT", function (ans) { window.location.href = baseURL; });
                                                return false;
                                            }  
                                          cargaAPInAX(baseURL,tipo,id_in,true,subseccion);                                           
                                         }, 
                error     : function(xhr, textStatus, error)
                                { jAlert("Ha ocurrido un error al guardar la seccion: " + xhr.statusText + " " + textStatus+" "+error,"HERMES PROJECT VALUATION REPORT"); 
                                  $("#confirm"+tipo).removeClass("msjconfirm").removeClass("msjOk").addClass("msjErr").html("<img src=\""+baseURL+"images/close.png\">Ocurrio un problema al guardar la seccion, intentelo mas tarde");}
               });
    }
}

function submitAnnexInAX(baseURL,forma,id_in,accion,tipo)
{    
    $("#confirm"+tipo).removeClass("msjOk").removeClass("msjErr").addClass("msjconfirm").html("").show();
    if($('#annexs'+tipo+' select').length === 0 )
    { $("#confirm"+tipo).removeClass("msjconfirm").removeClass("msjOk").addClass("msjErr").html("<img src=\""+baseURL+"images/close.png\"> Favor de seleccionar un comparable antes de guardar esta seccion "); 
      $("#confirm"+tipo).slideUp(6400);
    }
    else
    {
        $("#"+forma).validationEngine();
        if($("#"+forma).validationEngine('validate'))
        {
            var reportParts = [];
            $("input[name='report']:checked" ).each(function() { reportParts.push($(this).val()); });
            $("input[name='ann_in']:checked" ).each(function() { reportParts.push($(this).val()); });
            $("input[name='ap_in']:checked"  ).each(function() { reportParts.push($(this).val()); });
        
            saveReportPartsAX(id_in,baseURL,reportParts);
 
            $.ajax({ 
                    data      : $("#"+forma).serialize(),
                    url       : baseURL+'inmueble/guardar/'+id_in+'/'+accion+'/'+tipo+'/'+$('input[name=status]:checked').val()+'/'+$('input[name=idiom]:checked').val(),
                    type      : 'post',
                    dataType  : 'json',
                    beforeSend: function () { $("#confirm"+tipo).removeClass("msjOk").addClass("msjconfirm");
                                              $("#confirm"+tipo).html("<img src=\""+baseURL+"images/spin.png\"> Guardando seccion, espere por favor...");},
                    success   : function (response) { 
                                              if ('session' in response) 
                                                {
                                                    jAlert(response['msj'],"HERMES PROJECT VALUATION REPORT", function (ans) { window.location.href = baseURL; });
                                                    return false;
                                                }
                                              $("#confirm"+tipo).removeClass("msjconfirm").addClass("msjOk");
                                              $("#confirm"+tipo).html("<img src=\""+baseURL+"images/check.png\"> Seccion guardada exitosamente");
                                              $("#confirm"+tipo).slideUp(12800);                                              
                                            }, 
                    error     : function(xhr, textStatus, error)
                                    { jAlert("Ha ocurrido un error al guardar la seccion: " + xhr.statusText + " " + textStatus+" "+error,"HERMES PROJECT VALUATION REPORT"); 
                                      $("#confirm"+tipo).removeClass("msjconfirm").removeClass("msjOk").addClass("msjErr").html("<img src=\""+baseURL+"images/close.png\">Ocurrio un problema al guardar la seccion, intentelo mas tarde");}
                   });
        }
    }    
}

function submitAnnexA2InAX(baseURL,forma,id_in,accion,tipo,dialogAdjus)
{
    $("#confirm"+tipo).removeClass("msjOk").removeClass("msjErr").addClass("msjconfirm").html("").show();
    if($('#annexs'+tipo+' input').length === 0 )
    { 
      $("#confirm"+tipo).removeClass("msjconfirm").removeClass("msjOk").addClass("msjErr").html("<img src=\""+baseURL+"images/close.png\"> Favor de seleccionar un comparable antes de guardar esta seccion "); 
      $("#confirm"+tipo).slideUp(6400);
    }
    else
    {
        $("#"+forma).validationEngine();
        if($("#"+forma).validationEngine('validate'))
        {
            var reportParts = [];
            $("input[name='report']:checked" ).each(function() { reportParts.push($(this).val()); });
            $("input[name='ann_in']:checked" ).each(function() { reportParts.push($(this).val()); });
            $("input[name='ap_in']:checked"  ).each(function() { reportParts.push($(this).val()); });
        
           saveReportPartsAX(id_in,baseURL,reportParts);
 
            $.ajax({ 
                    data      : $("#"+forma).serialize(),
                    url       : baseURL+'inmueble/guardar/'+id_in+'/'+accion+'/'+tipo+'/'+$('input[name=status]:checked').val()+'/'+$('input[name=idiom]:checked').val(),
                    type      : 'post',
                    dataType  : 'json',
                    beforeSend: function () { $("#confirm"+tipo).removeClass("msjOk").addClass("msjconfirm");
                                              $("#confirm"+tipo).html("<img src=\""+baseURL+"images/spin.png\"> Guardando seccion, espere por favor...");},
                    success   : function (response) 
                    { 
                      if ('session' in response) 
                        {
                            jAlert(response['msj'],"HERMES PROJECT VALUATION REPORT", function (ans) { window.location.href = baseURL; });
                            return false;
                        }
                      $("#confirm"+tipo).removeClass("msjconfirm").addClass("msjOk");
                      $("#confirm"+tipo).html("<img src=\""+baseURL+"images/check.png\"> Seccion guardada exitosamente");
                      $("#confirm"+tipo).slideUp(12800);                                          
                      dialogAdjus.dialog( "close" );
                    }, 
                    error     : function(xhr, textStatus, error)
                                    { jAlert("Ha ocurrido un error al guardar la seccion: " + xhr.statusText + " " + textStatus+" "+error,"HERMES PROJECT VALUATION REPORT"); 
                                      $("#confirm"+tipo).removeClass("msjconfirm").removeClass("msjOk").addClass("msjErr").html("<img src=\""+baseURL+"images/close.png\">Ocurrio un problema al guardar la seccion, intentelo mas tarde");}
                   });
        }
    }
}

function saveReportPartsAX(id_in,baseURL,reportParts)
{        
    var param = {"id_in" : id_in, "reportParts" : reportParts};

    $.ajax({ url      : baseURL+'inmueble/saveReportPartsAX/',
             type     : 'post',
             data     : param,
             dataType : 'json',                
             success  : function (response) {},
             error    : function (err)      { jAlert("Ha ocurrido un error al saveReportPartsAX: " + err.status + " " + err.statusText,"HERMES PROJECT VALUATION REPORT"); }
            });	
}

function saveReportPartsANPPLAX(id_in,baseURL)
{   
    var reportParts = [];
    $("input[name='report']:checked" ).each(function() { reportParts.push($(this).val()); });
    $("input[name='ann_in']:checked" ).each(function() { reportParts.push($(this).val()); });
    $("input[name='ap_in']:checked"  ).each(function() { reportParts.push($(this).val()); });
        
    $("#confirmANPPL").removeClass("msjOk").removeClass("msjErr").addClass("msjconfirm").html("").show();
    var param = {"id_in" : id_in, "reportParts" : reportParts};

    $.ajax({ url      : baseURL+'inmueble/saveReportPartsAX/',
             type     : 'post',
             data     : param,
             dataType : 'json',
             beforeSend: function () { $("#confirmANPPL").removeClass("msjOk").addClass("msjconfirm");
                                       $("#confirmANPPL").html("<img src=\""+baseURL+"images/spin.png\"> Guardando seccion, espere por favor...");},
             success  : function (response) {$("#confirmANPPL").removeClass("msjconfirm").addClass("msjOk");
                                             $("#confirmANPPL").html("<img src=\""+baseURL+"images/check.png\"> Seccion guardada exitosamente");
                                             $("#confirmANPPL").slideUp(12800); },
             error    : function (err)      { jAlert("Ha ocurrido un error al saveReportPartsAX: " + err.status + " " + err.statusText,"HERMES PROJECT VALUATION REPORT"); }
            });	
}

/**********************
 * Carga Seccion INI
 **********************/
function cargaSeccionInAX(baseURL,seccion,id_in)
{
 if(seccion!="FP" && seccion!="R" && seccion!="AP" && seccion!="AN" )
  { var idiom = $('input[name=idiom]:checked').val();
    $.ajax({
            data      :  {"seccion":seccion, "id_in":id_in,"idiom":idiom},
            url       :  baseURL+'inmueble/cargaSeccionInAX/',
            type      : 'post',
            dataType  : 'json',
            beforeSend: function () {  },
            success   : function (response) 
                        {
                         if ('session' in response) 
                            {
                                jAlert(response['msj'],"HERMES PROJECT VALUATION REPORT", function (ans) { window.location.href = baseURL; });
                                return false;
                            }
                         tinymce.remove();
                         switch(seccion)
                            {
                                case "LT":                                    
                                    var checkboxDef = '';
                                    var def         = response['definiciones'];                                    
                                    (response['seccionData']['client']        == "Client")        ? $("#client"       ).attr("placeholder",response['seccionData']['client'])        : $("#client"       ).val(response['seccionData']['client']);
                                    (response['seccionData']['client_addres'] == "Client Address")? $("#client_addres").attr("placeholder",response['seccionData']['client_addres']) : $("#client_addres").val(response['seccionData']['client_addres']);
                                    
                                    (isEmpty(response['seccionData']['client_addres_2']))? $("#client_addres_2").attr("placeholder",response['seccionData']['client_addres_2']) : $("#client_addres_2").val(response['seccionData']['client_addres_2']);
                                    
                                    (response['seccionData']['atn_officer']    == "Attn:")               ? $("#atn_officer"   ).attr("placeholder",response['seccionData']['atn_officer'])    : $("#atn_officer"   ).val(response['seccionData']['atn_officer']);
                                    (response['seccionData']['jd_atn_officer'] == "Job Description Attn")? $("#jd_atn_officer").attr("placeholder",response['seccionData']['jd_atn_officer']) : $("#jd_atn_officer").val(response['seccionData']['jd_atn_officer']);
                                    
                                    (isEmpty(response['seccionData']['jd_atn_officer_2']))? $("#jd_atn_officer_2").attr("placeholder",response['seccionData']['jd_atn_officer_2']) : $("#jd_atn_officer_2").val(response['seccionData']['jd_atn_officer_2']);
                                    
                                    if(response['seccionData']['client_logo'] == "No Logo" || isEmpty(response['seccionData']['client_logo']))
                                        { $("#clientLogo"   ).val(""); 
                                          $("#divClientLogo").html("No Logo");}
                                    else    
                                        {$("#clientLogo"   ).val(response['seccionData']['client_logo']);
                                         $("#divClientLogo").html("<img src='"+baseURL+"gallery/"+$("#id_in").val()+"/"+response['seccionData']['client_logo']+"'><br><br>"+
                                                                  "<img class=\"imgCL\" title=\"Borrar Logotipo del cliente\" name='"+response['seccionData']['client_logo']+"'" +
                                                                  " id=\""+response['seccionData']['client_logo']+"\" src=\""+baseURL+"/images/close.png\"><br>");
                                
                                        $('.imgCL').addClass('pointer');
                                        $('.imgCL').click(function(i) { var img = $(this);
                                                                        jConfirm("¿Borrar Logotipo del Cliente?","HERMES PROJECT VALUATION REPORT", function(r) {
                                                                                   if(r) { borraImagenCargadaClientLogoAX(baseURL,img);  }
                                                                                });
                                                                      });		
                                        }
                                    
                                    $("#LTparrafo1")   .val( response['seccionData']['parrafo1']);
                                    $("#dirClient")    .html($("#calle").val()+"&nbsp;"+$("#num").val()+",&nbsp;"+$("#col").val()+",&nbsp;"+$("#mun").val()+",&nbsp;"+$("#edo").val()+",&nbsp;CP: "+$("#cp").val());
                                    $("#LTtexto1")     .val( response['seccionData']['texto1']);                                    
                                    $("#LTtexto2")     .val( response['seccionData']['texto2']);
                                    $("#LTtexto3")     .val( response['seccionData']['texto3']);
                                    $("#LTparrafo2")   .val( response['seccionData']['parrafo2']);
                                    $("#LTparrafo3")   .val( response['seccionData']['parrafo3']);
                                    $("#texto1Lbl")    .append(" "+response['seccionData']['effectiveDate']);
                                    $('#firma_prin')   .val( response['seccionData']['firma_prin']);
                                    $('#firma_sec')    .val( response['seccionData']['firma_sec']);
                                    var tituloFP = "";
                                    if (!isEmpty(response['seccionData']['tituloFP']) )
                                        {tituloFP =", "+ response['seccionData']['tituloFP']}
                                    var tituloFS = "";
                                    if (!isEmpty(response['seccionData']['tituloFS']) )
                                        {tituloFS =", "+ response['seccionData']['tituloFS']}
                                    $("#firma_prinLbl").html(empty(response['seccionData']['nombreFP'])+tituloFP+"<br>"+empty(response['seccionData']['puestoFP'])+" <br> Valuation and Consulting Services<br> JLL Mexico");
                                    $("#firma_secLbl") .html(empty(response['seccionData']['nombreFS'])+tituloFS+"<br>"+empty(response['seccionData']['puestoFS'])+" <br> Valuation and Consulting Services<br> JLL Mexico");
                                    $('#LTchbox1')     .prop( "checked", (response['seccionData']['LTchbox1'])=="1"?true:false );
                                    $('#LTchbox2')     .prop( "checked", (response['seccionData']['LTchbox2'])=="1"?true:false );
                                    $('#LTchbox3')     .prop( "checked", (response['seccionData']['LTchbox3'])=="1"?true:false );
                                    
                                    $("#AXmarketVal")   .html(response['totales'][0]);
                                    $("#AXmarketValM2") .html(response['totales'][1]);
                                    $("#AXmarketValft2").html(response['totales'][2]);
                                    
                                    $("#AX_VU_Approach") .html(response['totales'][3]);
                                    $("#AXVUApproachM2") .html(response['totales'][4]);
                                    $("#AXVUApproachft2").html(response['totales'][5]);
                                    
                                    $("#AXIliquidValLbl")   .html(response['totales'][6]);
                                    $("#AXIliquidValM2Lbl") .html(response['totales'][7]);
                                    $("#AXIliquidValft2Lbl").html(response['totales'][8]);
                                    
                                    for (var x = 0; x < def.length ; x++)
                                        { checkboxDef = checkboxDef+'<section class="col col-10"><label class="checkbox" style="text-align: justify; text-justify: inter-word"><input type="checkbox" class="defCheck" name="def[]" value="'+def[x]['id_def']+'" '+(isEmpty(def[x]['id_in'])?'':'checked')+'><i></i><strong>'+def[x]['concepto'+idiom]+'</strong><br>'+def[x]['descripcion'+idiom]+'</label></section>';
                                          if(response['tipo']=="1")
                                            checkboxDef = checkboxDef+'<section class="col col-1"><img class="borraDef" title="Borrar la definicion" value="'+def[x]['id_def']+'" src="'+baseURL+'images/close.png"/></section>'; 
                                        }
                                    $("#definiciones").append(checkboxDef);
                                    $(".borraDef").addClass('pointer');
                                    $('.borraDef').click(function(i) {var ch = $(this);
                                                                        jConfirm("¿Borrar la defininicion?","HERMES PROJECT VALUATION REPORT", function(r) {
                                                                                    if(r) { borraDefinicionAX(baseURL,ch);  }
                                                                               });                                                                    
                                                                     });
                                    tinymce.remove();
                                    tinymce.init({
                                                selector: '.richTextSmallLT',
                                                setup: function (editor) { editor.on('change', function () { editor.save(); }); },
                                                height: 200,
                                                toolbar: ' undo redo  | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist |',
                                                content_css: ['//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css','//www.tinymce.com/css/codepen.min.css']
                                              });
                                    tinymce.init({
                                                selector: '.richTextVerySmallLT',
                                                setup: function (editor) { editor.on('change', function () { editor.save(); }); },
                                                height: 80,
                                                toolbar: ' undo redo  | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist |',
                                                content_css: ['//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css','//www.tinymce.com/css/codepen.min.css']
                                              });          
                                    tinymce.init({
                                                selector: '.richTextLargeLT',
                                                setup: function (editor) { editor.on('change', function () { editor.save(); }); },
                                                height: 425,
                                                toolbar: ' undo redo  | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist |',
                                                content_css: ['//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css','//www.tinymce.com/css/codepen.min.css']
                                              });
                                    $(".val").change(function() { traeDatosValuadorAX(baseURL,this); });
                                    
                                    if (response['reportPart']==="")
                                        { $("#letterCheckBox" ).removeAttr("checked"); }
                                    else
                                        { $("#letterCheckBox").attr("checked",response['reportPart']);  }                                    
                                break;
                                case "SW":
                                    
                                    $("#SWParrafo1") .val(response['seccionData']['SWParrafo1']);
                                    $("#SWTit1")     .val(response['seccionData']['SWTit1']);
                                    $("#SWText1")    .val(response['seccionData']['SWText1']);
                                    $(".dirClientSW").html(response['seccionData']['col']);
                                    $(".dirComClientSW").html(response['seccionData']['loc']);
                                    $("#SWTit2")     .val(response['seccionData']['SWTit2']);
                                    $("#SWText2_1")  .val(response['seccionData']['SWText2_1']);
                                    $("#SWText2_2")  .val(response['seccionData']['SWText2_2']);
                                    (isEmpty(response['seccionData']['providedBy']))? $("#SWprovidedBy").attr("placeholder","Who provided the information") : $("#SWprovidedBy").val(response['seccionData']['providedBy']);
                                    $("#SWText2_3")  .val(response['seccionData']['SWText2_3']);
                                    $('#SWTit3')     .val( response['seccionData']['SWTit3']);
                                    $('#SWText3_1')  .val(response['seccionData']['SWText3_1']);
                                    (isEmpty(response['seccionData']['dateInspection']))? $("#SWdateInspection").attr("placeholder","Date Inspection") : $("#SWdateInspection").val(response['seccionData']['dateInspection']);
                                    (isEmpty(response['seccionData']['inspector']))? $("#SWinspector").attr("placeholder","Inspector") : $("#SWinspector").val(response['seccionData']['inspector']);
                                    $('#SWText3_2')  .val(response['seccionData']['SWText3_2']);
                                    $('#SWTit4')     .val(response['seccionData']['SWTit4']);
                                    $('#SWText4_1')  .val(response['seccionData']['SWText4_1']);
                                    $('#SWText4_2')  .val(response['seccionData']['SWText4_2']);
                                    (isEmpty(response['seccionData']['SWText4_2']))? $("#SWText4_2").val(response['definiciones']) : $("#SWText4_2").val(response['seccionData']['SWText4_2']);
                                    $('#SWText4_3')  .val(response['seccionData']['SWText4_3']);
                                    $('#SWTit5')     .val(response['seccionData']['SWTit5']);
                                    $('#SWText5')    .val(response['seccionData']['SWText5']);
                                    $('#SWTit6')     .val(response['seccionData']['SWTit6']);
                                    $('#SWText6')    .val(response['seccionData']['SWText6']);
                                    $('#SWTit7')     .val(response['seccionData']['SWTit7']);
                                    $('#SWText7')    .val(response['seccionData']['SWText7']);
                                                                        
                                     tinymce.init({
                                                selector: '.richTextSmallSW',
                                                setup: function (editor) { editor.on('change', function () { editor.save(); }); },
                                                height: 100,
                                                toolbar: ' undo redo  | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist |',
                                                content_css: ['//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css','//www.tinymce.com/css/codepen.min.css']
                                              });
                                    tinymce.init({
                                                selector: '.richTextMediumSW',
                                                setup: function (editor) { editor.on('change', function () { editor.save(); }); },
                                                height: 180,
                                                toolbar: ' undo redo  | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist |',
                                                content_css: ['//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css','//www.tinymce.com/css/codepen.min.css']
                                              });                                    
                                    if (response['reportPart']==="")
                                        { $("#scopeWorkCheckBox" ).removeAttr("checked"); }
                                    else
                                        { $("#scopeWorkCheckBox").attr("checked",response['reportPart']);  }          
                                break;
                                case "SM":
                                    $('#SMTit1')    .val(response['seccionData']['SMTit1']);
                                    $('#SMText1')   .val(response['seccionData']['SMText1']);                                    
                                    $('#SMTit2')    .val(response['seccionData']['SMTit2']);
                                    $('#SMText2')   .val(response['seccionData']['SMText2']);                                    
                                    $('#SMTit3')    .val(response['seccionData']['SMTit3']);
                                    $('#SMText3')   .val(response['seccionData']['SMText3']);
                                    $('#SMTit4')    .val(response['seccionData']['SMTit4']);
                                    $('#SMText4')   .val(response['seccionData']['SMText4']);                                    
                                    $('#SMTit5')    .val(response['seccionData']['SMTit5']);
                                    $('#SMText5')   .val(response['seccionData']['SMText5']);
                                    $('#SMTit6')    .val(response['seccionData']['SMTit6']);
                                    $('#SMText6')   .val(response['seccionData']['SMText6']);
                                    $('#SMTit7')    .val(response['seccionData']['SMTit7']);
                                    $('#SMText7')   .val(response['seccionData']['SMText7']);                                    
                                    $('#SMvc1_1')   .val(response['seccionData']['SMvc1_1']);
                                    $('#SMvc1_2')   .val(response['seccionData']['SMvc1_2']);
                                    $('#SMvc1_3')   .val(response['seccionData']['SMvc1_3']);
                                    $('#SMvc1_4')   .val(response['seccionData']['SMvc1_4']);
                                    $('#SMvc2_1')   .val(response['seccionData']['SMvc2_1']);
                                    $('#SMvc2_2')   .val(response['seccionData']['SMvc2_2']);
                                    $('#SMvc2_3')   .val(response['seccionData']['SMvc2_3']);
                                    $('#SMvc2_4')   .val(response['seccionData']['SMvc2_4']);
                                    $('#SMvc3_1')   .val(response['seccionData']['SMvc3_1']);
                                    $('#SMvc3_2')   .val(response['seccionData']['SMvc3_2']);
                                    $('#SMvc3_3')   .val(response['seccionData']['SMvc3_3']);
                                    $('#SMvc3_4')   .val(response['seccionData']['SMvc3_4']);
                                    $('#SMvc4_1')   .val(response['seccionData']['SMvc4_1']);
                                    $('#SMvc4_2')   .val(response['seccionData']['SMvc4_2']);
                                    $('#SMvc4_3')   .val(response['seccionData']['SMvc4_3']);
                                    $('#SMvc4_4')   .val(response['seccionData']['SMvc4_4']);
                                    $('#SMvc5_1')   .val(response['seccionData']['SMvc5_1']);
                                    $('#SMvc5_2')   .val(response['seccionData']['SMvc5_2']);
                                    $('#SMvc5_3')   .val(response['seccionData']['SMvc5_3']);
                                    $('#SMvc5_4')   .val(response['seccionData']['SMvc5_4']);
                                    $('#SM_ch_vc1') .prop( "checked", (response['seccionData']['LTchbox1'])=="1"?true:false );
                                    $('#SM_ch_vc2') .prop( "checked", (response['seccionData']['LTchbox2'])=="1"?true:false );
                                    $('#SM_ch_vc3') .prop( "checked", (response['seccionData']['LTchbox3'])=="1"?true:false );
                                    $('#SM_ch_vc4') .prop( "checked", (response['seccionData']['SM_ch_vc4'])=="1"?true:false );
                                    $('#SM_ch_vc5') .prop( "checked", (response['seccionData']['SM_ch_vc5'])=="1"?true:false );
                                    $('#SM_po')     .html(response['seccionData']['SM_po']);
                                    $('#SM_sa')     .html(response['seccionData']['SM_sa']);                                    
                                    $('#SM_ur')     .html(response['seccionData']['SM_ur']);
                                    $('#SM_er')     .html('$ '+response['seccionData']['SM_er']+' MXN/USD');
                                    $('#SM_ed')     .html(response['seccionData']['SM_ed']);
                                    
                                    $('#SM_dr')     .html(response['definiciones']['hoy']);
                                    $('#SM_ls')     .html(response['definiciones']['totLS']);
                                    $('#SM_gba')    .html(response['definiciones']['totGBA']);
                                    $('#SM_tra')    .html(response['definiciones']['totTRA']);
                                    $('#SM_lsFT')   .html(response['definiciones']['totLSFT']);
                                    $('#SM_gbaFT')  .html(response['definiciones']['totGBAFT']);
                                    $('#SM_traFT')  .html(response['definiciones']['totTRAFT']);
                                    
                                    $("#SM_marketVal")  .html(response['totales'][0]);
                                    $("#SM_VU_Approach").html(response['totales'][3]);                                    
                                    $("#SM_liquidVal")  .html(response['totales'][6]);  
                                    
                                    $("#SM_client").html(response['definiciones']['client']);
                                    $("#SW_Tit6")   .html(response['definiciones']['SWTit6']);
                                    $("#SW_Text6")  .html(response['definiciones']['SWText6']);
                                    $("#SW_Tit7")   .html(response['definiciones']['SWTit7']);
                                    $("#SW_Text7")  .html(response['definiciones']['SWText7']);
                                    
                                    $("#AI_Tit11")   .html(response['definiciones']['AITit11']);
                                    $("#AI_Text11")  .html(response['definiciones']['AIText11']);
                                    
                                    if (response['reportPart']==="")
                                        { $("#summaryCheckBox" ).removeAttr("checked"); }
                                    else
                                        { $("#summaryCheckBox").attr("checked",response['reportPart']);  }          
                                break;                                
                            }                           
                        }, 
            error     : function(err) { jAlert("Ha ocurrido un error cargaSeccionInAX en " + err.status + " " + err.statusText,"HERMES PROJECT VALUATION REPORT"); }
    });
 }
 else{ if(seccion == "AP"){ $("input[name='sky-tabs-3']").removeAttr("checked");
                            $("#divAPI"   ).html(""); 
                            $("#divAPII"  ).html(""); 
                            $("#divAPIII" ).html(""); 
                            $("#divAPIV"  ).html(""); 
                            $("#divAPV"   ).html(""); 
                            $("#divAPVI"  ).html(""); 
                            $("#divAPVII" ).html(""); 
                            $("#divAPVIII").html("");
                         }
        if(seccion == "AN"){ $("input[name='sky-tabs-2']").removeAttr("checked");
                             $("#annexsAA1").html("");
                             $("#annexsAA2").html("");
                             $("#annexsAA3").html("");
                             $("#annexsAA4").html("");
                             $("#annexsAA5").html("");
                             $("#annexsAA6").html("");
                             $('#tblPhotos').html('');
                             $('#divAAIMG' ).html('');                            
                             $('#divACA'   ).html('');
                             comparablesElegidos         = [];
                             comparablesElegidosBorrados = [];
                           }
    }
}
/**********************
 * Carga Seccion FIN
 **********************/


/**********************
 * Carga AP INI
 **********************/
function cargaAPInAX(baseURL,ap,id_in,recalcula,subseccion)
{ 
    var hash = location.hash.replace('#','');
    if(hash != ''){ location.hash = ''; }
    var idiom = $('input[name=idiom]:checked').val();   
    $.ajax({
            data      :  {"ap":ap, "id_in":id_in,"idiom":idiom},
            url       :  baseURL+'inmueble/cargaAPInAX/',
            type      : 'post',
            dataType  : 'json',
            beforeSend: function () { if (recalcula===false){
                                                            $("#divAPI"   ).html(""); 
                                                            $("#divAPII"  ).html(""); 
                                                            $("#divAPIII" ).html(""); 
                                                            $("#divAPIV"  ).html(""); 
                                                            $("#divAPV"   ).html(""); 
                                                            $("#divAPVI"  ).html(""); 
                                                            $("#divAPVII" ).html(""); 
                                                            $("#divAPVIII").html(""); 
                                                            $('#divACA'   ).html('');
                                                            }
                                    },
            success   : function (response) 
                        {
                         if ('session' in response) 
                        {
                            jAlert(response['msj'],"HERMES PROJECT VALUATION REPORT", function (ans) { window.location.href = baseURL; });
                            return false;
                        }   
                         tinymce.remove();
                         switch(ap)
                            {
                                case "API":
                                    $("#divAPI").html(response['apCore']);  
                                    subirImagenAPI(baseURL);
                                    $('.clImgAPI').addClass('pointer');                                    
                                    $('.clImgAPI').click(function(i) { var img = $(this);                                        
                                                                       jConfirm("¿Borrar imagen?","HERMES PROJECT VALUATION REPORT", function(r) {
                                                                                          if(r) {  borraImagenCargadaAPIAX(baseURL,img);  }
                                                                            });
                                                                  });
                                    if (response['reportPart']==="")
                                        { $("#apICheckBox" ).removeAttr("checked"); }
                                    else
                                        { $("#apICheckBox").attr("checked",response['reportPart']);  }          
                                break;
                                case "APII":
                                    $("#divAPII").html(response['apCore']);                                      
                                    subirImagenAPII(baseURL,"APII_1","APII");
                                    subirImagenAPII(baseURL,"APII_2","APII");
                                    subirImagenAPII(baseURL,"APII_3","APII");                                    
                                    $('.clImgAPII_1').addClass('pointer');
                                    $('.clImgAPII_2').addClass('pointer');
                                    $('.clImgAPII_3').addClass('pointer');
                                    $('.clImgAPII_1').click(function(i) { var img = $(this);jConfirm("¿Borrar imagen?","HERMES PROJECT VALUATION REPORT", function(r) { if(r) { borraImagenCargadaAPIIAX(baseURL,img,"APII_1","APII");  } }); });
                                    $('.clImgAPII_2').click(function(i) { var img = $(this);jConfirm("¿Borrar imagen?","HERMES PROJECT VALUATION REPORT", function(r) { if(r) { borraImagenCargadaAPIIAX(baseURL,img,"APII_2","APII");  } }); });
                                    $('.clImgAPII_3').click(function(i) { var img = $(this);jConfirm("¿Borrar imagen?","HERMES PROJECT VALUATION REPORT", function(r) { if(r) { borraImagenCargadaAPIIAX(baseURL,img,"APII_3","APII");  } }); });
                                    if (response['reportPart']==="")
                                        { $("#apIICheckBox" ).removeAttr("checked"); }
                                    else
                                        { $("#apIICheckBox").attr("checked",response['reportPart']);  }
                                break;
                                case "APIII":
                                    $("#divAPIII").html(response['apCore']);
                                    tinymce.init({ selector: '.richTextBigAPIII',
                                                   setup: function (editor) { editor.on('change', function () { editor.save(); }); },
                                                   height: 350,
                                                   toolbar: ' undo redo  | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist |',
                                                   content_css: ['//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css','//www.tinymce.com/css/codepen.min.css']
                                                });
                                    if (response['reportPart']==="")
                                        { $("#apIIICheckBox" ).removeAttr("checked"); }
                                    else
                                        { $("#apIIICheckBox").attr("checked",response['reportPart']);  }
                                break;                                
                                case "APIV":
                                    $("#divAPIV").html(response['apCore']);
                                    tinymce.init({ selector: '.richTextBigAPIV',
                                                   setup: function (editor) { editor.on('change', function () { editor.save(); }); },
                                                   height: 250,
                                                   toolbar: ' undo redo  | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist |',
                                                   content_css: ['//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css','//www.tinymce.com/css/codepen.min.css']
                                                });
                                    if (response['reportPart']==="")
                                        { $("#apIVCheckBox" ).removeAttr("checked"); }
                                    else
                                        { $("#apIVCheckBox").attr("checked",response['reportPart']);  }                                                
                                break;                                
                                case "APV":
                                    $("#divAPV").html(response['apCore']);
                                    if(recalcula=== true){
                                        $("#confirmRecalculate"+ap+subseccion).removeClass("msjconfirm").addClass("msjOk");
                                        $("#confirmRecalculate"+ap+subseccion).html("<img src=\""+baseURL+"images/check.png\"> Seccion recalculada exitosamente");
                                        $("#confirmRecalculate"+ap+subseccion).slideUp(12800);
                                    }                                    
                                    var er = response['er'];
                                     
                                    $('.addCPD').click(function() {
                                        var cpdId = Number($("#numCPD").val());                              
                                        cpdId++;
                                        
                                        $("#tblCPD > tbody > tr:last").before( '<tr> <td valing="center" class="leftCell">  <img src="'+baseURL+'images/close.png" title="Delete Curable physical deterioration" id="CPD'+cpdId+'" class="delCPD pointer" alt="">&nbsp;&nbsp;'+cpdId+' </td>'+
                                                                                                          '<td valing="center" class="leftCell">  <input type="text" name="CPD'+cpdId+'"    id="CPD'+cpdId+'"  value="" class="validate[required,custom[onlyLetterNumber]] text-input conborde" placeholder="Curable physical deterioration" maxlength="60" size="16"></td>'+
                                                                                                          '<td valing="center" class="centerCell"><input type="text" name="CPDUN'+cpdId+'"  id="CPDUN'+cpdId+'" value="" class="validate[required,custom[onlyLetterNumber]] text-input conborde" placeholder="Unit" maxlength="40" size="3"></td>'+
                                                                                                          '<td valing="center" class="centerCell"><input type="text" name="CPDAM'+cpdId+'"  id="CPDAM'+cpdId+'" value="" class="conborde" onchange="calculaTotCPD(\'CPD\','+cpdId+','+er+')" placeholder="0.00" maxlength="10" size="2"></td>'+
                                                                                                          '<td valing="center" class="centerCell"> <br><input type="text" name="CPDUV'+cpdId+'Mask"  id="CPDUV'+cpdId+'Mask" value="" class="pmask conborde" onchange="calculaTotCPD(\'CPD\','+cpdId+','+er+')" placeholder="0.00" maxlength="20" size="8">mxn <input type="hidden" name="CPDUV'+cpdId+'"  id="CPDUV'+cpdId+'" value="" >'+
                                                                                                          '                                        <br>$<label id="CPDUV'+cpdId+'usd"></label> usd</td>'+
                                                                                                          '<td valing="center" class="rightCell">  <br>$<label id="lblCPDTO'+cpdId+'"></label> mxn<input type="hidden" class="CPD" name="CPDTO'+cpdId+'" id="CPDTO'+cpdId+'" value=""><br>'+
                                                                                                          '                                                                   $ <label id="lblCPDTO'+cpdId+'usd"></label> usd</td>'+
                                                                                                     '</tr>'
                                                                                                   );
                                        $("#numCPD").val(cpdId);
                                        $(".pmask").maskMoney({prefix:'$',precision:2,decimal:'.', allowNegative: false, thousands:',', allowZero: true});
                                        $(".pmask").maskMoney("mask");
                                        $('.delCPD').click(function() { borraCPD(this,er); });
                                    });
                                    
                                    $(".pmask").maskMoney({prefix:'$',precision:2,decimal:'.', allowNegative: false, thousands:',', allowZero: true});
                                    $(".pmask").maskMoney("mask");
                                    $('.delCPD').click(function() { borraCPD(this,er); });
                                    
                                    tinymce.init({ selector: '.richTextAPV',
                                                   setup: function (editor) { editor.on('change', function () { editor.save(); }); },
                                                   height: 200,
                                                   toolbar: ' undo redo  | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist |',
                                                   content_css: ['//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css','//www.tinymce.com/css/codepen.min.css']
                                                });
                                    if (response['reportPart']==="")
                                        { $("#apVCheckBox" ).removeAttr("checked"); }
                                    else
                                        { $("#apVCheckBox").attr("checked",response['reportPart']);  }                                                            
                                break;
                                case "APVI":
                                    $("#divAPVI").html(response['apCore']);
                                    tinymce.init({ selector: '.richTextAPVI',
                                                   setup: function (editor) { editor.on('change', function () { editor.save(); }); },
                                                   height: 200,
                                                   toolbar: ' undo redo  | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist |',
                                                   content_css: ['//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css','//www.tinymce.com/css/codepen.min.css']
                                                });
                                    $(".pmask").maskMoney({prefix:'$',precision:2,decimal:'.', allowNegative: false, thousands:',', allowZero: true});
                                    $(".pmask").maskMoney("mask");
                                    $(".cmask").maskMoney({precision:2,decimal:'.', allowNegative: false, thousands:',', allowZero: true});
                                    $(".cmask").maskMoney("mask");
                                    if (response['reportPart']==="")
                                        { $("#apVICheckBox" ).removeAttr("checked"); }
                                    else
                                        { $("#apVICheckBox").attr("checked",response['reportPart']);  }
                                break;
                                case "APVII":
                                    $("#divAPVII" ).html(response['apCore']['coreAPVII'] );
                                    $("#divAPVIII").html(response['apCore']['coreAPVIII']);
                                    $("#divAPIX"  ).html(response['apCore']['coreAPIX']  );
                                    $("#divAPX"   ).html(response['apCore']['coreAPX']   );
                                    tinymce.init({ selector: '.richTextAPVII',
                                                   setup: function (editor) { editor.on('change', function () { editor.save(); }); },
                                                   height: 150,
                                                   toolbar: ' undo redo  | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist |',
                                                   content_css: ['//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css','//www.tinymce.com/css/codepen.min.css']
                                                });
                                    $(".pmask").maskMoney({prefix:'$',precision:2,decimal:'.', allowNegative: false, thousands:',', allowZero: true});
                                    $(".pmask").maskMoney("mask");
                                    if (response['reportPart']["APVII"]==="")
                                        { $("#apVIICheckBox" ).removeAttr("checked"); }
                                    else
                                        { $("#apVIICheckBox").attr("checked",response['reportPart']);  }
                                        
                                    if (response['reportPart']["APVIII"]==="")
                                        { $("#apVIIICheckBox" ).removeAttr("checked"); }
                                    else
                                        { $("#apVIIICheckBox").attr("checked",response['reportPart']);  }
                                    
                                    if (response['reportPart']["APIX"]==="")
                                        { $("#apIXCheckBox" ).removeAttr("checked"); }
                                    else
                                        { $("#apIXCheckBox").attr("checked",response['reportPart']);  }

                                    if (response['reportPart']["APX"]==="")
                                        { $("#apXCheckBox" ).removeAttr("checked"); }
                                    else
                                        { $("#apXCheckBox").attr("checked",response['reportPart']);  } 
                                        
                                break;
                                case "APVIII":                                    
                                    $("#divAPXI" ).html(response['apCore']['coreAPXI'] );                                    
                                    $("#divAPXII").html(response['apCore']['coreAPXII']);                                    
                                    tinymce.init({ selector: '.richTextAPVIII',
                                                   setup: function (editor) { editor.on('change', function () { editor.save(); }); },
                                                   height: 200,
                                                   toolbar: ' undo redo  | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist |',
                                                   content_css: ['//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css','//www.tinymce.com/css/codepen.min.css']
                                                });
                                    $("#AXIFecha").datepicker({dateFormat: 'M d yy'});
                                    
                                    if (response['reportPart']["APXI"]==="")
                                        { $("#apXICheckBox" ).removeAttr("checked"); }
                                    else
                                        { $("#apXICheckBox").attr("checked",response['reportPart']);  }                                     
                                        
                                    if (response['reportPart']["APXII"]==="")
                                        { $("#apXIICheckBox" ).removeAttr("checked"); }
                                    else
                                        { $("#apXIICheckBox").attr("checked",response['reportPart']);  }                                                                             
                                break;
                                case "ACA":
                                              var ft = response['ft'];
                                              var er = Number(response['er']);
                                              $('#divACA' ).html  (response['apCore']['tablaACA']);
                                              $('.addArea').click(function() {var columnId  = Number($('#tblACA > thead > tr > th:last').attr("id"));
                                                                              columnId++;
                                                                              var areaT     = '<img src="'+baseURL+'images/close.png" title="Delete Area" id="'+columnId+'" class="delArea pointer" alt=""><br><br><input type="text" name="areaT'+columnId+'" id="areaT'+columnId+'" class="validate[custom[onlyLetterNumber]] text-input conborde" onChange="asignaTypeName(this,\'Ty\','+columnId+')" value="" placeholder="Type Area" size="8" maxlength="40">'; 
                                                                              var areaL     = '<input type="text" name="areaL'+columnId+'" id="areaL'+columnId+'" class="validate[required,custom[onlyLetterNumber]] text-input conborde" onChange="asignaTypeName(this,\'Na\','+columnId+')" value="" placeholder="Name Area" size="8" maxlength="40">';
                                                                              var areaG     = '<label class="checkbox" style="text-align: justify; text-justify: inter-word"><input type="checkbox" class="sumarSub" name="gba'+columnId+'" value="GBA" checked><i></i>GBA*</label>';
                                                                              var areaR     = '<label class="checkbox" style="text-align: justify; text-justify: inter-word"><input type="checkbox" class="sumarSub" name="tra'+columnId+'" value="TRA" checked><i></i>TRA*</label>';
                                                                              var tblHeader = '<table cellspacing="0" cellpadding="0" width="100%"><tr><td class="leftCell">'+areaT+'</td></tr><tr><td class="leftCell">'+areaL+'</td></tr><tr><td>'+areaG+'</td></tr><tr><td>'+areaR+'</td></tr></table>';
                                                                              
                                                                              $('#tblACA > thead > tr > th:last').after('<th id="'+columnId+'" class="centerCell">'+ tblHeader +'</th>');
                                                                              $('#tblACA > tbody > tr:last').find("td:last").after('<td class="leftCell adprice cn" id="areaSubC'+columnId+'"><label id="lblSub'+columnId+'"> 0.00</label> m2<br><label id="lblSub'+columnId+'ft2"> 0.00</label> ft2 <input type="hidden" class="TOT"  name="sub'+columnId+'" id="sub'+columnId+'" value=""></td>'); 
                                                                              
                                                                              /*************COST APPROACH ADD TYPE INI**********/                                                                              
                                                                              $('#tblRCN > thead > tr > th:last').after("<th class='leftCell' id='areaHeDEP"+columnId+"'><label id='lblhcTyRCN"+columnId+"'>  </label><br><label id='lblhcNaRCN"+columnId+"'>  </label></th>");
                                                                              $('#tblRCN > thead > tr').prev("tr").children("th").attr("colspan",columnId);
                                                                              $('#tblRCN > tbody > tr > td:last').after('<td class="leftCell adprice" id="areaRCN'+columnId+'"><textarea rows="6" cols="12"   name="defRCN'+columnId+'"      id="defRCN'+columnId+'"      class="conborde" placeholder="Definition" maxlength="200"> </textarea><br>'+
                                                                                                                                                                            '  <input type="text"           name="defValueRCN'+columnId+'Mask" id="defValueRCN'+columnId+'Mask" class="pmask conborde" onChange="sumaRCN(this,\''+columnId+'\',\''+er+'\',true)" value="" placeholder="0.00" size="10" maxlength="20"> mxn'+
                                                                                                                                                                            '  <input type="hidden"         name="defValueRCN'+columnId+'" id="defValueRCN'+columnId+'" class="defvarcn" onChange="sumaRCN(this,\''+columnId+'\',\''+er+'\',true)" value="" placeholder="0.00" size="5" maxlength="10"><br>'+
                                                                                                                                                                            '  <input type="text"           name="fdhRCN'+columnId+'"      id="fdhRCN'+columnId+'"      class="conborde" onChange="sumaRCN(this,\''+columnId+'\',\''+er+'\',false)" value="" placeholder="0.00" size="6" maxlength="10"><br>'+
                                                                                                                                                                            '  <input type="text"           name="fcRCN'+columnId+'"       id="fcRCN'+columnId+'"       class="conborde" onChange="sumaRCN(this,\''+columnId+'\',\''+er+'\',false)" value="" placeholder="0.00" size="6" maxlength="10"><br>'+
                                                                                                                                                                            '$ <label id="lblucRCN'+columnId+'">0.0</label>mxn<input type="hidden" name="ucRCN'+columnId+'" id="ucRCN'+columnId+'" class="ucrcn" value=""> <br>'+
                                                                                                                                                                            '  <input type="text"           name="porRCN'+columnId+'"      id="porRCN'+columnId+'"      class="conborde" onChange="sumaRCN(this,\''+columnId+'\',\''+er+'\',false)" value="100" placeholder="0.00" size="4" maxlength="10">%<br>'+
                                                                                                                                                                            '<br>$ <label id="lblucRCNAD'+columnId+'">0.0</label> mxn<input type="hidden" class="ucadrcn" name="ucAdRCN'+columnId+'" id="ucAdRCN'+columnId+'"  value=""> <br>'+
                                                                                                                                                                            '<label class="cnAM">&nbsp;$ <label id="lblRcn'+columnId+'"    class="cn" >0.0 </label> mxn<br>'+
                                                                                                                                                                            '&nbsp;$ <label id="lblRcn'+columnId+'usd" class="cn" >0.0 </label> usd<input type="hidden" class="rcn" name="rcn'+columnId+'" id="rcn'+columnId+'" value=""><br></label>'+
                                                                                                                           '</td>'); 
                                                                              /*************COST APPROACH ADD TYPE END**********/
                                                                              
                                                                              /*************DEPRECIATED ADD TYPE INI **********/
                                                                              $('#tblDEP > thead > tr > th:last').after("<th class='leftCell' id='areaHeDEP"+columnId+"><label id='lblhcTyDEP"+columnId+"'>  </label><br><label id='lblhcNaDEP"+columnId+"'>  </label></th>");
                                                                              $('#tblDEP > thead > tr').prev("tr").children("th").attr("colspan",columnId);
                                                                              $('#tblDEP > tbody > tr > td:last').after('<td class="leftCell adprice" id="areDEP'+columnId+'"><input  type="text" name="efageDEP'+columnId+'" id="efageDEP'+columnId+'" class="conborde" onChange="obtenDVC('+columnId+','+er+')" value="" placeholder="0.00" size="6" maxlength="10"><br> '+
                                                                                                                                                                            ' <input type="text" name="ulDEP'+columnId+'"    id="ulDEP'+columnId+'"    class="conborde" onChange="obtenDVC('+columnId+','+er+')" value="" placeholder="0.00" size="6" maxlength="10"><br>'+
                                                                                                                                                                            ' <br><label id="lblafDEP'+columnId+'" >0.0</label> %<input type="hidden" class="rcn" name="afDEP'+columnId+'" id="afDEP'+columnId+'" value=""><br> '+
                                                                                                                                                                            '<label class="cnCA"> <br>$<label id="lblDvcDEP'+columnId+'" class="cn" >0.00</label> mxn<br>&nbsp;$<label id="lblDvcDEP'+columnId+'usd" class="cn" >0.00</label> usd<input type="hidden" name="dvc'+columnId+'" id="dvc'+columnId+'" value=""><br></label>'+
                                                                                                                                                                            ' </td>');
                                                                              /*************DEPRECIATED ADD TYPE END **********/
                                                                              
                                                                              
                                                                              $('#tblACA > tbody > tr').each(function(){ var rowId  = Number($(this).attr("id"));
                                                                                                                         if (rowId > 0 )
                                                                                                                           { $(this).find('td:last-child').after('<td class="leftCell areaDato'+columnId+'"><input type="text"   name="az'+columnId+rowId+'Mask" id="az'+columnId+rowId+'Mask" class="cmask conborde" onChange="sumaTotCA(this,'+ft+','+columnId+','+rowId+','+er+')" value="" placeholder="0.00" size="6" maxlength="20"> m2\n\
                                                                                                                                                                                                            <input type="hidden" name="az'+columnId+rowId+'"     id="az'+columnId+rowId+'"     class="GBA TRA col'+columnId+' row'+rowId+'"></td>'); }
                                                                                                                       });                                                                              
                                                                                                                       
                                                                              $('#numColumn').val(columnId);
                                                                              $('.sumarSub' ).click(function() { sumaGBATRA(this,ft);   });
                                                                              $('.delArea'  ).click(function() { borraArea(this,ft,er); });
                                                                              $(".pmask").maskMoney({prefix:'$',precision:2,decimal:'.', allowNegative: false, thousands:',', allowZero: true});
                                                                              $(".pmask").maskMoney("mask");
                                                                              $(".cmask").maskMoney({precision:2,decimal:'.', allowNegative: false, thousands:',', allowZero: true});
                                                                              $(".cmask").maskMoney("mask");
                                                                             });
                                              $('.addZone').click(function() {var rowId   = Number($('#tblACA > tbody > tr:last').prev("tr").attr("id"));
                                                                              rowId++;
                                                                              var columns = "";
                                                                              var zona    = "";
                                                                              $('#tblACA > thead > tr > th').each(function(){var columnId = Number($(this).attr("id"));                         
                                                                                                                             var clasesSum = "";
                                                                                                                             $(this).find('input:checkbox').each(function(){ clasesSum = ( String($(this).attr("checked")) === "checked")?clasesSum+" "+$(this).val():clasesSum+""; });
                                                                                                                             if (columnId !== 0)                                                                                                                                                               
                                                                                                                                { columns = columns + '<td class="leftCell areaDato'+columnId+'"><input type="text"   name="az'+columnId+rowId+'Mask" id="az'+columnId+rowId+'Mask" class="cmask conborde" onChange="sumaTotCA(this,'+ft+','+columnId+','+rowId+','+er+')"  value="" placeholder="0.00" size="6" maxlength="20"> m2\n\
                                                                                                                                                                                                 <input type="hidden" name="az'+columnId+rowId+'"     id="az'+columnId+rowId+'"     class="'+clasesSum+' col'+columnId+' row'+rowId+'" value=""></td>';}
                                                                                                                            });
                                                                              zona = '<img src="'+baseURL+'images/close.png" title="Delete Zone" id="'+rowId+'" class="delZone pointer" alt="">&nbsp;&nbsp;'+rowId+'. <input type="text" name="zona'+rowId+'" id="zona'+rowId+'" value="" class="validate[required,custom[onlyLetterNumber]] text-input conborde" placeholder="Zone" maxlength="40" size="6">';
                                                                              $('#tblACA > tbody > tr:last').prev("tr").after('<tr id="'+rowId+'"><td class="centerCell">'+zona+'</td>'+columns+'</tr>');                                                                                                                                                              
                                                                              $('#numRow').val(rowId);
                                                                              $('#SubTotRow').append('<label id="lblSubTotRow'+rowId+'" class="subTotRow"></label> m2<input type="hidden" name="subTotRow'+rowId+'" id="subTotRow'+rowId+'" value="'+rowId+'"><br><br>');
                                                                              $('.delZone').click(function() { borraZone(this,ft,er); });
                                                                              $(".pmask").maskMoney({prefix:'$',precision:2,decimal:'.', allowNegative: false, thousands:',', allowZero: true});
                                                                              $(".pmask").maskMoney("mask");
                                                                              $(".cmask").maskMoney({precision:2,decimal:'.', allowNegative: false, thousands:',', allowZero: true});
                                                                              $(".cmask").maskMoney("mask");
                                                                             });                                                                                                                    
                                              $('.addAE').click(function() {var aeId = Number($("#numAE").val());
                                                                            aeId++;
                                                                            $("#tblAE > tbody > tr:last").before('<tr>   <td valing="center" class="leftCell"   ><img src="'+baseURL+'images/close.png" title="Delete ACCESORY EQUIPMENT" id="AE'+aeId+'" class="delAECW pointer" alt="">&nbsp;&nbsp;'+aeId+'.   <input type="text" name="AE'+aeId+'"   id="AE'+aeId+'"   value=""  class="validate[required,custom[onlyLetterNumber]] text-input conborde" placeholder="Accesory Equipment" maxlength="60" size="15"></td>'
                                                                                                                 +'      <td valing="center" class="centerCell"  >            <input type="text" name="AEAM'+aeId+'" id="AEAM'+aeId+'" value=""  class="conborde" onchange="calculaTotAECW(\'AE\','+aeId+','+er+')"      placeholder="0.00" maxlength="10" size="3"></td>'
                                                                                                                 +'      <td valing="center" class="centerCell"  >            <input type="text" name="AEUN'+aeId+'" id="AEUN'+aeId+'" value=""  class="validate[custom[onlyLetterNumber]] text-input conborde" placeholder="[Unit]" maxlength="40" size="4"></td>'
                                                                                                                 +'      <td valing="center" class="centerCell"  >      <br>  <input type="text" name="AEUV'+aeId+'Mask" id="AEUV'+aeId+'Mask" value=""  class="pmask conborde" onchange="calculaTotAECW(\'AE\','+aeId+','+er+')" placeholder="0.00" maxlength="20" size="10"> mxn'
                                                                                                                 +'                                                           <input type="hidden" name="AEUV'+aeId+'" id="AEUV'+aeId+'" value=""></td>'
                                                                                                                 +'      <td valing="center" class="rightCell"  >      <br>$ <label id="lblAETO'+aeId+'">0.0</label> mxn<input type="hidden" class="AE" name="AETO'+aeId+'" id="AETO'+aeId+'" value=""><br>'
                                                                                                                 +'                                                                   $ <label id="lblAETO'+aeId+'usd">0.0</label> usd</td>'
                                                                                                                 +'      <td valing="center" class="centerCell celdaDiv" >    <input type="text" name="AEEfAgDEP'+aeId+'"  id="AEEfAgDEP'+aeId+'" value="" class="conborde" onchange="obtenDVCAECW(\'AE\','+aeId+','+er+')"    placeholder="0.00" maxlength="10" size="3"></td>'
                                                                                                                 +'      <td valing="center" class="centerCell" >             <input type="text" name="AEULDEP'+aeId+'"    id="AEULDEP'+aeId+'"   value=""    class="conborde" onchange="obtenDVCAECW(\'AE\','+aeId+','+er+')" placeholder="0.00" maxlength="10" size="3"></td>'                                                                                                                 
                                                                                                                 +'      <td valing="center" class="centerCell" >             <label id="lblAEAFDEP'+aeId+'" >0.0</label> %   <input type="hidden" name="AEAFDEP'+aeId+'" id="AEAFDEP'+aeId+'" value=""></td>'
                                                                                                                 +'      <td valing="center" class="rightCell"  >      <br>$ <label id="lblAEDVC'+aeId+'"   >0.0</label> mxn<input type="hidden" class="AEDEP" name="AEDVC'+aeId+'" id="AEDVC'+aeId+'" value=""><br>'
                                                                                                                 +'                                                                   $ <label id="lblAEDVC'+aeId+'usd">0.0</label> usd</td>'
                                                                                                                 +'</tr>'
                                                                                                                );
                                                                            
                                                                            $("#numAE").val(aeId);
                                                                            $(".pmask").maskMoney({prefix:'$',precision:2,decimal:'.', allowNegative: false, thousands:',', allowZero: true});
                                                                            $(".pmask").maskMoney("mask");
                                                                            $('.delAECW').click(function() { borraAECW(this,er); });
                                                                           });
                                              $('.addCW').click(function() {var cwId = Number($("#numCW").val());                              
                                                                            cwId++;
                                                                            $("#tblCW > tbody > tr:last").prev("tr").prev("tr").before( '<tr><td valing="center" class="leftCell"   ><img src="'+baseURL+'images/close.png" title="Delete COMPLEMENTARY WORK" id="CW'+cwId+'" class="delAECW pointer" alt="">&nbsp;&nbsp;'+cwId+'.   <input type="text" name="CW'+cwId+'"    id="CW'+cwId+'"   value="" class="validate[required,custom[onlyLetterNumber]] text-input conborde" placeholder="COMPLEMENTARY WORKS" maxlength="60" size="15"></td>'
                                                                                                                                       +'    <td valing="center" class="centerCell"  >            <input type="text" name="CWAM'+cwId+'"  id="CWAM'+cwId+'"  value="" class="conborde" onchange="calculaTotAECW(\'CW\','+cwId+','+er+')"      placeholder="0.00" maxlength="10" size="3"></td>'
                                                                                                                                       +'    <td valing="center" class="centerCell"  >            <input type="text" name="CWUN'+cwId+'"  id="CWUN'+cwId+'" value="" class="validate[custom[onlyLetterNumber]] text-input conborde" placeholder="Unit" maxlength="40" size="4"></td>'
                                                                                                                                       +'    <td valing="center" class="centerCell"  >      <br>  <input type="text" name="CWUV'+cwId+'Mask"  id="CWUV'+cwId+'Mask" value="" class="pmask conborde" onchange="calculaTotAECW(\'CW\','+cwId+','+er+')" placeholder="0.00" maxlength="20" size="10"> mxn'
                                                                                                                                       +'                                                         <input type="hidden" name="CWUV'+cwId+'"  id="CWUV'+cwId+'" value=""></td>'
                                                                                                                                       +'    <td valing="center" class="rightCell"  >      <br>$ <label id="lblCWTO'+cwId+'"   >0.0</label> mxn<input type="hidden" class="CW" name="CWTO'+cwId+'" id="CWTO'+cwId+'" value=""><br>'
                                                                                                                                       +'                                                                 $ <label id="lblCWTO'+cwId+'usd">0.0</label> usd</td>'
                                                                                                                                       +'    <td valing="center" class="centerCell celdaDiv" >    <input type="text" name="CWEfAgDEP'+cwId+'" id="CWEfAgDEP'+cwId+'" value="" class="conborde" onchange="obtenDVCAECW(\'CW\','+cwId+','+er+')" placeholder="0.00" maxlength="10" size="3"></td>'
                                                                                                                                       +'    <td valing="center" class="centerCell" >             <input type="text" name="CWULDEP'+cwId+'"   id="CWULDEP'+cwId+'"   value="" class="conborde" onchange="obtenDVCAECW(\'CW\','+cwId+','+er+')" placeholder="0.00" maxlength="10" size="3"></td>'                                                                                                                                       
                                                                                                                                       +'    <td valing="center" class="centerCell" >             <label id="lblCWAFDEP'+cwId+'" >0.0</label> %   <input type="hidden" name="CWAFDEP'+cwId+'" id="CWAFDEP'+cwId+'" value=""></td>'
                                                                                                                                       +'    <td valing="center" class="rightCell"  >      <br>$ <label id="lblCWDVC'+cwId+'"   >0.0</label> mxn<input type="hidden" class="CWDEP" name="CWDVC'+cwId+'" id="CWDVC'+cwId+'" value=""><br>'
                                                                                                                                       +'                                                                 $ <label id="lblCWDVC'+cwId+'usd">0.0</label> usd</td>'
                                                                                                                                       +'</tr>'
                                                                                                                                      );
                                                                            $("#numCW").val(cwId);
                                                                            $(".pmask").maskMoney({prefix:'$',precision:2,decimal:'.', allowNegative: false, thousands:',', allowZero: true});
                                                                            $(".pmask").maskMoney("mask");
                                                                            $('.delAECW').click(function() { borraAECW(this,er); });
                                                                           });
                                             $('.delArea' ).click(function() { borraArea(this,ft,er); });
                                             $('.delZone' ).click(function() { borraZone(this,ft,er); });
                                             $('.delAECW' ).click(function() { borraAECW(this,er);    });
                                             $('.sumarSub').click(function() { sumaGBATRA(this,ft);   });
                                             $(".pmask").maskMoney({prefix:'$',precision:2,decimal:'.', allowNegative: false, thousands:',', allowZero: true});
                                             $(".pmask").maskMoney("mask");
                                             $(".cmask").maskMoney({precision:2,decimal:'.', allowNegative: false, thousands:',', allowZero: true});
                                             $(".cmask").maskMoney("mask");
                                                 
                                   break;
                            }                           
                        }, 
            error     : function(err) { jAlert("Ha ocurrido un error cargaSeccionInAX en " + err.status + " " + err.statusText,"HERMES PROJECT VALUATION REPORT"); }
    }); 
}
/**********************
 * Carga AP FIN
 **********************/

/**********************
 * Carga ANEXO INI
 **********************/
function cargaAnexoInAX(baseURL,seccion,tipoComp,id_in)
{   var idiom = $('input[name=idiom]:checked').val();       
    $.ajax({
            data      :  {"seccion":seccion, "id_in":id_in,"idiom":idiom},
            url       :  baseURL+'inmueble/cargaAnexoInAX/',
            type      : 'post',
            dataType  : 'json',
            beforeSend: function () {  $("#annexsAA1").html("");
                                       $("#annexsAA2").html("");
                                       $("#annexsAA3").html("");
                                       $("#annexsAA4").html("");
                                       $("#annexsAA5").html("");
                                       $("#annexsAA6").html("");
                                       $('#tblPhotos').html('');
                                       $('#divAAIMG' ).html('');                                       
                                       $('#annexsLim' ).html('');                                       
                                       $('#annexsCert' ).html('');                                       
                                       comparablesElegidos         = [];
                                       comparablesElegidosBorrados = [];
                                    },
            success   : function (response) 
                        {
                            if ('session' in response) 
                            {
                                jAlert(response['msj'],"HERMES PROJECT VALUATION REPORT", function (ans) { window.location.href = baseURL; });
                                return false;
                            }
                            tinymce.remove();
                            switch(seccion)
                               {
                                   case "AA1":
                                   case "AA3":
                                   case "AA5":
                                       if(response['comparables'] === false)
                                           $("#annexs"+seccion).html("<br><center><h1>Report without comparable</h1></center>");
                                       else
                                           selectCompAX(baseURL,response['comparables'],[],tipoComp,id_in);
                                       
                                        if (response['reportPart']["AA1"]=="")
                                            { $("#AA1CheckBox" ).removeAttr("checked"); }
                                        else
                                            { $("#AA1CheckBox").attr("checked",response['reportPart']);  }

                                        if (response['reportPart']["AA3"]=="")
                                            { $("#AA1CheckBox" ).removeAttr("checked"); }
                                        else
                                            { $("#AA3CheckBox").attr("checked",response['reportPart']);  } 
                                            
                                        if (response['reportPart']["AA5"]=="")
                                            { $("#AA5CheckBox" ).removeAttr("checked"); }
                                        else
                                            { $("#AA5CheckBox").attr("checked",response['reportPart']);  }     
                                   break;
                                   case "AA2":
                                   case "AA4":
                                   case "AA6":
                                          var responseft = response['ft'];
                                          if(response['comparables'] === false)
                                             $("#annexs"+seccion).html("<br><br><center><h1>Report without comparable</h1></center>");
                                          else
                                          {    
                                            $("#annexs"+seccion).html(response['grid']);
                                            $("#compElements"+seccion).html(response['ce']);
                                            $('#indicatedValuem2').hide();
                                            $('#indicatedValueft2').hide();
                                            $(".weightInput").change(function(){  if (isNaN($(this).val()) === true)
                                                                                      { $(this).val(0);
                                                                                        jAlert("Favor de ingresar un porcentaje correcto. Ejemplo 5.00","HERMES PROJECT VALUATION REPORT");
                                                                                      }
                                                                                      else
                                                                                      {
                                                                                          $('#indicatedValuem2').hide();
                                                                                          $('#indicatedValueft2').hide();
                                                                                          
                                                                                          var composition = Number($(this).val());
                                                                                          var idComp      = $(this).attr('idComp');
                                                                                          var ap          = $(this).attr('ap');
                                                                                          var apft        = $(this).attr('apft');
                                                                                          var fc          = ap*(composition/100);
                                                                                          var ft          = responseft;
                                                                                          var fcFT        = (seccion === "AA2")?apft*fc:(fc/ft)*12;
                                                                                          var ft2AA4      = (seccion === "AA4")?" usd/sqf/year":" usd/ft2";
                                                                                          var fc2AA4      = (seccion === "AA4")?" usd/sqm/mo.":" usd/m2";
                                                                                          
                                                                                          $("#composition"+idComp+"m2").html(fc.toFixed(2));
                                                                                          $("#composition"+idComp+"ft2").html(fcFT.toFixed(2)+ft2AA4);
                                                                                          
                                                                                          var totalIVM    = 0;
                                                                                          var totalIVFT   = 0;
                                                                                          $(".sumTot").each(function(){ totalIVM  = totalIVM  + Number($(this).html()); });
                                                                                          
                                                                                          var totalPesos   = 0;
                                                                                          $(".weightInput").each(function(){ totalPesos  = totalPesos  + (isNaN($(this).val())?0:Number($(this).val())) });
                                                                                          $("#weightTot").removeClass("msjOk").removeClass("msjErr").html("").show();
                                                                                          if(totalPesos === 100)
                                                                                               $("#weightTot").addClass("msjOk").html("<img src=\""+baseURL+"images/check.png\">100%").slideUp(12800);
                                                                                           else
                                                                                               $("#weightTot").addClass("msjErr").html("<img src=\""+baseURL+"images/close.png\">"+totalPesos+"% Not 100%");
                                                                                           
                                                                                          totalIVFT  = (tipoComp === "AA2")?(totalIVM / ft):(totalIVM / ft)*12;
                                                                                          
                                                                                          $('#indicatedValuem2' ).html(totalIVM);
                                                                                          $('#indicated_value'  ).val(totalIVM);
                                                                                          $('#indicatedm2'      ).html("$ "+ $.number(totalIVM,2, '.', ',' ) + fc2AA4);
                                                                                          $('#indicatedValueft2').html(totalIVFT);
                                                                                          $('#indicatedft2'     ).html("$ "+ $.number(totalIVFT,2, '.', ',' ) + ft2AA4);
                                                                                      }
                                                                               });
                                          if (response['reportPart']["AA2"]==="")
                                            { $("#AA2CheckBox" ).removeAttr("checked"); }
                                          else
                                            { $("#AA2CheckBox").attr("checked",response['reportPart']);  } 
                                          
                                          if (response['reportPart']["AA4"]==="")
                                            { $("#AA4CheckBox" ).removeAttr("checked"); }
                                          else
                                            { $("#AA4CheckBox").attr("checked",response['reportPart']);  }   
                                            
                                          if (response['reportPart']["AA6"]==="")
                                            { $("#AA6CheckBox" ).removeAttr("checked"); }
                                          else
                                            { $("#AA6CheckBox").attr("checked",response['reportPart']);  } 
                                          }
                                   break;
                                   case "AA8":
                                              $('#tblPhotos').append(response['aaCore']['tablaFotos']);
                                              $('#divAAIMG' ).html  (response['aaCore']['tablaAAIMG']);
                                              $('#slAAMIG'  ).html  (response['aaCore']['selectAAMIMG']);
                                              $('.delPhoto' ).click (function() { var obj= $(this);
                                                                                jConfirm("¿Borrar imagen?","HERMES PROJECT VALUATION REPORT", function(r) {
                                                                                    if(r) { borraImagenCargadaAA8AX(baseURL,obj,"Photos");  }
                                                                                });
                                                                              });
                                             $('.editTit').click(function() {var id = $(this).attr("id");                                                                             
                                                                             $("#lbl"+id).html('<section class="col col-7"><label class="input"><i class="icon-append fa fa-tag"></i><input type="text" name="txt'+id+'" value="" id="txt'+id+'" placeholder="Nuevo Titulo" maxlength="40"><b class="tooltip tooltip-bottom-right">Modificar el Titulo de la imagen</b></label><label id="buttonUpPh" class="button">update</label></section>');
                                                                             $("#buttonUpPh").click(function() { updateTitlePhotoAX(baseURL,id,$("#txt"+id).val()); });
                                                                             $(this).hide();
                                                                            });
                                              if (response['reportPart']["PH"]==="")
                                                { $("#PHCheckBox" ).removeAttr("checked"); }
                                              else
                                                { $("#PHCheckBox").attr("checked",response['reportPart']);  }   

                                              if (response['reportPart']["AAA"]==="")
                                                { $("#AAACheckBox" ).removeAttr("checked"); }
                                              else
                                                { $("#AAACheckBox").attr("checked",response['reportPart']);  }                                                                             
                                   break;
                                   case "AA9":
                                             $('#annexsLim' ).html(response['aaCore']['annexsLim']);
                                             $('#annexsCert').html(response['aaCore']['annexsCert']);
                                              tinymce.init({ selector: '.richTextBigLC',
                                                   setup: function (editor) { editor.on('change', function () { editor.save(); }); },
                                                   height: 350,
                                                   toolbar: ' undo redo  | bold italic | alignleft aligncenter alignright alignjustify',
                                                   content_css: ['//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css','//www.tinymce.com/css/codepen.min.css']
                                                });
                                               if (response['reportPart']["LIM"]==="")
                                                { $("#LIMCheckBox" ).removeAttr("checked"); }
                                              else
                                                { $("#limCheckBox").attr("checked",response['reportPart']);  }   

                                              if (response['reportPart']["CERT"]==="")
                                                { $("#CERTCheckBox" ).removeAttr("checked"); }
                                              else
                                                { $("#CERTCheckBox").attr("checked",response['reportPart']);  } 
                                   break;                                             
                               }
                        }, 
            error     : function(err) { jAlert("Ha ocurrido un error cargaAnexoInAX en " + err.status + " " + err.statusText,"HERMES PROJECT VALUATION REPORT"); }
    });
}

function sumaGBATRA(object,ft)
{    
    var columnId = $(object).parent().parent().parent().parent().parent().parent().attr('id');
    var value    = $(object).val();
    var actions  = String($(object).attr('checked'));    
    if(actions === "checked")
        { $('.col'+columnId).addClass(value); }
    else
        { $('.col'+columnId).removeClass(value); }
    $("#tot"+value).html( $.number(sumaCA(value),2, '.', ',' ) );
    $("#tot"+value+"ft").html( $.number((sumaCA(value)*ft),2, '.', ',' ) );    
}

function borraArea(object,ft,er)
{
    var columnId = $(object).attr('id');
    jConfirm("¿Borrar el área definida?, se borrará tambien el enfoque de costos y depreciación","HERMES PROJECT VALUATION REPORT", function(r) {
    if(r) { 
            $("#"+columnId).remove();
            $("#areaSubC" +columnId).remove();
            $("#areaHeRCN"+columnId).remove();
            $("#areaRCN"  +columnId).remove();
            $("#areaHeDEP"+columnId).remove();
            $("#areaDEP"  +columnId).remove();
            $(".areaDato" +columnId).remove();
            var columnIdAnt = (columnId===1)?2:columnId-1;
            calculaSubtotatles(ft,columnIdAnt,"ALL",er);
          }
    });      
}

function borraZone(object,ft,er)
{
    var rowId = $(object).parent().parent().attr('id');
    jConfirm("¿Borrar la zona registrada?, este cambio afectará el enfoque de costos y depreciación","HERMES PROJECT VALUATION REPORT", function(r) {
    if(r) { 
            $(object).parent().parent().remove();
            $("#lblSubTotRow" +rowId).next().remove();
            $("#lblSubTotRow" +rowId).remove();
            $("#subTotRow"    +rowId).next().remove();
            $("#subTotRow"    +rowId).remove();
            $('#tblACA > thead > tr > th').each(function(){ if( (Number($(this).attr('id')) !== 0) && (Number($(this).attr('id')) !== -1) ){                          
                                                              var columnId =$(this).attr('id');                                                        
                                                              calculaSubtotatles(ft,(columnId),"ALL",er);
                                                          }
                                                          });            
          }
    });      
}

function borraCPD(object,er)
{
    var imgId = String($(object).attr('id'));
    var tipo  = imgId.substring(0, 3);    
    jConfirm("¿Borrar deteriodo registrado?","HERMES PROJECT VALUATION REPORT", function(r) {
    if(r) { 
            $(object).parent().parent().remove();
            
            var subTotal = sumaCA(tipo);
            $("#subTotalCPD")   .html( $.number((subTotal),2, '.', ',' )  );
            $("#subTotalCPDusd").html( $.number((subTotal/er),2, '.', ',' )  );
          }
    });
}

function borraAECW(object,er)
{
    var imgId = String($(object).attr('id'));
    var tipo  = imgId.substring(0, 2);    
    jConfirm("¿Borrar la zona registrada?, este cambio afectará el concepto de: Total replacement cost","HERMES PROJECT VALUATION REPORT", function(r) {
    if(r) { 
            $(object).parent().parent().remove();
            calculaSubTotalesAECWRCNDEP(tipo,er);
          }
    });
}

function sumaRCN(object,columnId,er,mask)
{
    var campo = 0;
    if(mask === true)
        { campo       = $(object).maskMoney('unmasked')[0];
          var id      = $(object).attr('id');
          var len     = id.length;
          var len_ext = len-3;
          id          =  id.substring(0, len_ext-1);           
          $("#"+id).val(campo);
        }
    else
        { campo = $(object).val(); }
    if( isNaN(campo) )
    {
        jAlert("El valor ingresado: "+uv+" no es valido","HERMES PROJECT VALUATION REPORT");
        $(object).val(0.00);
        $(object).focus();
    }
    else{ totalRCN(columnId,er);
          obtenDVC(columnId,er);
        }
}

function totalRCN(columnId,er)
{
    var uc = Number($("#defValueRCN"+columnId).val()) * Number($("#fdhRCN"+columnId).val()) * Number($("#fcRCN"+columnId).val());
    $("#ucRCN"+columnId).val(uc);
    $("#lblucRCN"+columnId).html($.number( (uc),2, '.', ',' ) );
    var por  = Number($("#porRCN"+columnId).val());
    var ucAd = (uc * (por/100)) ; 
    $("#ucAdRCN"+columnId).val(ucAd);
    $("#lblucRCNAD"+columnId).html($.number( (ucAd),2, '.', ',' ) );
    var areaBuild = Number($("#sub"+columnId).val());
    var rcn       = ucAd*areaBuild;
    $("#lblRcn"+columnId      ).html($.number( (rcn)                 ,2, '.', ',' ) );
    $("#lblRcn"+columnId+"usd").html($.number( (rcn/er)              ,2, '.', ',' ) );
    $("#rcn"+columnId         ).val(rcn);

    var subTotDefVaRCN = sumaCA("defvarcn");
    $("#subTotDVRCN"   ).html($.number( subTotDefVaRCN     ,2, '.', ',' ) );
    $("#subTotDVRCNusd").html($.number( (subTotDefVaRCN/er),2, '.', ',' ) );
    var subTotUCRCN    = sumaCA("ucrcn");
    $("#subTotUCRCN"   ).html($.number( subTotUCRCN     ,2, '.', ',' ) );
    $("#subTotUCRCNusd").html($.number( (subTotUCRCN/er),2, '.', ',' ) );
    var subTotUCAdRCN    = sumaCA("ucadrcn");
    $("#subTotUCAdRCN"   ).html($.number( subTotUCAdRCN     ,2, '.', ',' ) );
    $("#subTotUCAdRCNusd").html($.number( (subTotUCAdRCN/er),2, '.', ',' ) );

    var subTotRCN    = sumaCA("rcn");         
    $("#subTotRCN"   ).html($.number( subTotRCN     ,2, '.', ',' ) );         
    $("#subTotRCNusd").html($.number( (subTotRCN/er),2, '.', ',' ) );
    var totAE     = sumaCA("AE");
    var totCW     = sumaCA("CW");         
    $("#subTotalRC"   ).html($.number((totAE+totCW+subTotRCN)   ,2, '.', ',' ) );
    $("#subTotalRCusd").html($.number((totAE+totCW+subTotRCN)/er,2, '.', ',' ) );
}


function sumaTotCA(object,ft,columnId,rowId,er)
{
    var campo = $(object).maskMoney('unmasked')[0];
     
    if (isNaN(campo) )
    {
        jAlert("El valor ingresado: "+campo+" no es valido","HERMES PROJECT VALUATION REPORT");
        $(object).val(0.00);
    }
    else{ var campoMask = $(object).attr('id');
          var len       = campoMask.length;
          var len_ext   = len-3;
          campoMask     = campoMask.substring(0, len_ext-1);           
          $("#"+campoMask).val(campo); 
          calculaSubtotatles(ft,columnId,rowId,er); 
         }
}


function calculaSubtotatles(ft,columnId,rowId,er)
{
    var totGBA = sumaCA("GBA");
    $("#totGBA"   ).html( $.number( totGBA    ,2, '.', ',' ) );
    $("#totGBAft" ).html( $.number((totGBA*ft),2, '.', ',' ) );
    $("#txttotGBA").val(totGBA);

    var totTRA = sumaCA("TRA");
    $("#totTRA"   ).html( $.number( totTRA    ,2, '.', ',' ) );
    $("#totTRAft" ).html( $.number((totTRA*ft),2, '.', ',' ) );
    $("#txttotTRA").val(totTRA);                  

    var subArea = sumaCA("col"+columnId);
    $("#lblSub"+columnId ).html( $.number(subArea ,2, '.', ',' ) );
    $("#lblSub"+columnId+"ft2").html( $.number(subArea*ft ,2, '.', ',' ) );
    $("#sub"+columnId    ).val(subArea);

    var subZone = 0;
    if(rowId == "ALL")
         { $('#tblACA > tbody > tr').each(function(){ if( (Number($(this).attr('id')) !== 0) && (Number($(this).attr('id')) !== -1) ){                          
                                                        var zoneId =$(this).attr('id');
                                                        subZone = sumaCA("row" + zoneId);                                                        
                                                        $("#lblSubTotRow"+ zoneId ).html( $.number(subZone ,2, '.', ',' ) );         
                                                        $("#subTotRow"   + zoneId ).val(subZone);
                                                      }
                                                    });
         }
    else {  subZone = sumaCA("row"+rowId);
            $("#lblSubTotRow"+rowId ).html( $.number(subZone ,2, '.', ',' ) );         
            $("#subTotRow"+rowId    ).val(subZone);
         }  

    var totCA = sumaCA("TOT");         
    $("#totCA"   ).html( $.number( totCA     ,2, '.', ',' ) );
    $("#totCAft" ).html( $.number((totCA*ft) ,2, '.', ',' ) );
    $("#txttotCA").val(totCA);

    totalRCN(columnId,er);
    var subTotDep = sumaCA("rcnDEP");
    $("#totDEP"   ).html($.number(subTotDep   ,2, '.', ',' ));
    $("#totDEPusd").html($.number(subTotDep/er,2, '.', ',' ));
    calculaSubTotalesAECWRCNDEP('AE',er);
    calculaSubTotalesAECWRCNDEP('CW',er);    
}

function calculaTotAECW(tipo,id,usd)
{
    var amount    = $("#"+tipo+"AM"+id).val(); 
    var unitPrice = $("#"+tipo+"UV"+id+"Mask").maskMoney('unmasked')[0];
    var campo     = $("#"+tipo+"UV"+id+"Mask").attr('id');

    if (isNaN(amount)){  jAlert("El valor ingresado: "+amount+" no es valido","HERMES PROJECT VALUATION REPORT");
                         $("#"+tipo+"AM"+id).val(0.00); }
    else{ if (isNaN(unitPrice)){  jAlert("El valor ingresado: "+unitPrice+" no es valido","HERMES PROJECT VALUATION REPORT");
                                  $("#"+tipo+"UV"+id).val(0.00); }
          else { var len       = campo.length;
                 var len_ext   = len-3;
                 var campoMask =  campo.substring(0, len_ext-1);           
                 $("#"+campoMask).val(unitPrice);
                 
                 var total   = amount * unitPrice;
                 $("#"+tipo+"TO"+id).val(total);
                 $("#lbl"+tipo+"TO"+id)      .html( $.number(total,          2, '.', ',' ) );
                 $("#lbl"+tipo+"TO"+id+"usd").html( $.number((total/usd),    2, '.', ',' )  );
                 $("#"+tipo+"UV"+id+"usd")   .html( $.number((unitPrice/usd),2, '.', ',' )  );                 
                                  
                 obtenDVCAECW(tipo,id,usd);
               }
        }
}

function calculaTotCPD(tipo,id,usd)
{
    var amount    = $("#"+tipo+"AM"+id).val(); 
    var unitPrice = $("#"+tipo+"UV"+id+"Mask").maskMoney('unmasked')[0];

    if (isNaN(amount)){  jAlert("El valor ingresado: "+amount+" no es valido","HERMES PROJECT VALUATION REPORT");
                         $("#"+tipo+"AM"+id).val(0.00); }
    else{ if (isNaN(unitPrice)){  jAlert("El valor ingresado: "+unitPrice+" no es valido","HERMES PROJECT VALUATION REPORT");
                                  $("#"+tipo+"UV"+id+"Mask").val(0.00); }
          else { $("#"+tipo+"UV"+id).val(unitPrice);
                 var total   = amount * unitPrice;
                 $("#"+tipo+"TO"+id).val(total);
                 $("#lbl"+tipo+"TO"+id)      .html( $.number(total,          2, '.', ',' ) );
                 $("#lbl"+tipo+"TO"+id+"usd").html( $.number((total/usd),    2, '.', ',' )  );
                 $("#"+tipo+"UV"+id+"usd")   .html( $.number((unitPrice/usd),2, '.', ',' )  );
                 
                 var subTotal = sumaCA(tipo);
                 $("#subTotalCPD")   .html( $.number((subTotal),2, '.', ',' )  );
                 $("#subTotalCPDusd").html( $.number((subTotal/usd),2, '.', ',' )  );
               }
        }
}

function sumaCA(tipo)
{
    var subtotal = 0;
    $('.'+tipo).each(function(){subtotal= subtotal + Number($(this).val()); });
    
    return subtotal;
}

function asignaTypeName(object,campo,columnId)
{
    $("#lblhc"+campo+"RCN"+columnId).html($(object).val());
    $("#lblhc"+campo+"DEP"+columnId).html($(object).val());
}


function obtenDVC(columnId,er)
{
    
    var efage = $("#efageDEP"+columnId).val();  //useful_life
    var ul    = $("#ulDEP"   +columnId).val();  //age factor


 
    if (isNaN(efage) || isNaN(ul) )
    { jAlert("El valor ingresado para Effective Age: "+efage+" o el valor ingresado para Useful Life: "+ul+", alguno NO es valido","HERMES PROJECT VALUATION REPORT");  }
    else{ 
          efage   = Number(efage);          
          ul      = Number(ul); 

          
         
          
          var divul = (ul===0)?1:ul;
          var af       =  efage / divul;// formula anterior ((0.100*ul) + ((0.900)*(ul-efage)))/divul;
          af = parseFloat(af).toFixed(4);
          //alert ("af:" + af);
          //alert(af);
          $("#lblafDEP"+columnId).html($.number(af*100,2, '.', ',' ));
          $("#afDEP"+columnId   ).val(af);
          
          var dvc = af * Number($("#rcn"+columnId).val());
          $("#lblDvcDEP"+columnId      ).html($.number(dvc,2, '.', ',' ));
          $("#lblDvcDEP"+columnId+"usd").html($.number(dvc/er,2, '.', ',' ));
          $("#dvc"+columnId            ).val (dvc);
          
          var totDep   = sumaCA("rcnDEP");          
          $("#totDEP"   ).html($.number(totDep,2, '.', ',' ));
          $("#totDEPusd").html($.number(totDep/er,2, '.', ',' ));
        }
}

function obtenDVCAECW(tipo,columnId,er)
{
    var efage = $("#"+tipo+"EfAgDEP" +columnId).val();
    var ul    = $("#"+tipo+"ULDEP"   +columnId).val();
 
    if (isNaN(efage) || isNaN(ul) )
    { jAlert("El valor ingresado para Effective Age: "+efage+" o el valor ingresado para Useful Life: "+ul+", alguno NO es valido","HERMES PROJECT VALUATION REPORT");  }
    else{ 
          efage   = Number(efage);
          ul      = Number(ul);          
          
          var divul = (ul===0)?1:ul;
          var af    = efage / divul;/// formula anterior ((0.100*ul) + ((0.900)*(ul-efage)))/divul;
          $("#lbl"+tipo+"AFDEP"+columnId).html($.number(af*100,2, '.', ',' ));
          $("#"+tipo+"AFDEP"+columnId   ).val(af);
          
          var dvc = af * Number($("#"+tipo+"TO"+columnId).val());
          $("#lbl"+tipo+"DVC"+columnId      ).html($.number(dvc,2, '.', ',' ));
          $("#lbl"+tipo+"DVC"+columnId+"usd").html($.number(dvc/er,2, '.', ',' ));
          $("#"+tipo+"DVC"+columnId         ).val (dvc);
          
          calculaSubTotalesAECWRCNDEP(tipo,er);
        }
}

function calculaSubTotalesAECWRCNDEP(tipo,er)
{
    var totAECW = sumaCA(tipo+"DEP");
    $("#subTotal"+tipo+"DEP"   ).html($.number(totAECW,2, '.', ',' ));
    $("#subTotal"+tipo+"DEPusd").html($.number(totAECW/er,2, '.', ',' ));

    var totAE = sumaCA("AEDEP");
    var totcCW= sumaCA("CWDEP");
    $("#subTotalAEWCDEP"   ).html($.number((totAE + totcCW),2, '.', ',' ));
    $("#subTotalAEWCDEPusd").html($.number((totAE + totcCW)/er,2, '.', ',' ));

    var totrcnDEP = sumaCA("rcnDEP");
    $("#subTotalRCDEP"   ).html($.number((totAE + totcCW + totrcnDEP),2, '.', ',' ));
    $("#subTotalRCDEPusd").html($.number((totAE + totcCW + totrcnDEP)/er,2, '.', ',' ));
    
    var totAE   = sumaCA("AE");
    var totCW   = sumaCA("CW");                         
    $("#subTotalAE"   ).html($.number(totAE    , 2, '.', ',' ) );
    $("#subTotalAEusd").html($.number(totAE/er, 2, '.', ',' ) );
    $("#subTotalCW"   ).html($.number(totCW    , 2, '.', ',' ) );
    $("#subTotalCWusd").html($.number(totCW/er, 2, '.', ',' ) );
    var totAECW = 0;
    var totRCN  = sumaCA("rcn");
    totAECW = (totAE+totCW);
    $("#subTotalAEWC"   ).html($.number(totAECW    , 2, '.', ',' ) );
    $("#subTotalAEWCusd").html($.number(totAECW/er, 2, '.', ',' ) );
    totAECW = totAECW + totRCN;
    $("#subTotalRC"   ).html($.number(totAECW    , 2, '.', ',' ) );
    $("#subTotalRCusd").html($.number(totAECW/er, 2, '.', ',' ) );
}

function updateTitlePhotoAX(baseURL,id,titulo)
{
    var param = {"id" : id, "titulo": titulo};
    $.ajax({ 
            data    : param,               
            url     : baseURL+'inmueble/updateTitlePhotoAX/',
            type    : 'post',
            dataType: 'json',
            beforeSend: function () { },
            success   : function (response) {  
                                                if ('session' in response) 
                                                {
                                                    jAlert(response['msj'],"HERMES PROJECT VALUATION REPORT", function (ans) { window.location.href = baseURL; });
                                                    return false;
                                                }
                                                $("#lbl"+id).html(titulo);
                                                $("#"+id).show();
                                            }, 
            error: function(err) { jAlert("Ha ocurrido un error al agregar Definicion AX: " + err.status + " " + err.statusText,"HERMES PROJECT VALUATION REPORT"); }
    });
}

function salvarAA2(baseURL,id_in,tipoComp)
{
    var compValue       = [];
    var compId          = [];
    var annex           = (tipoComp==="CLS")?"2":(tipoComp==="CRL")?"4":"6";
    var indicated_value = $("#indicated_value").val();
    $(".weightInput").each(function(){ compValue.push($(this).val() );
                                       compId.push($(this).attr('idcomp') );
                                     });
    $("#confirmAA"+annex+"1").removeClass("msjOk").removeClass("msjErr").addClass("msjconfirm").html("").show();
    var param = {"compId" : compId, "compValue": compValue, "id_in" : id_in, "tipoComp":tipoComp, "indicated_value":indicated_value };
    $.ajax({ 
            data    : param,               
            url     : baseURL+'inmueble/salvarAA2AX/',
            type    : 'post',
            dataType: 'json',
            beforeSend: function () { $("#confirmAA"+annex+"1").removeClass("msjOk").addClass("msjconfirm");
                                          $("#confirmAA"+annex+"1").html("<img src=\""+baseURL+"images/spin.png\"> Guardando seccion, espere por favor...");},
            success   : function (response) 
            {
                if ('session' in response) 
                {
                   jAlert(response['msj'],"HERMES PROJECT VALUATION REPORT", function (ans) { window.location.href = baseURL; });
                    return false;
                }
                $("#confirmAA"+annex+"1").removeClass("msjconfirm").addClass("msjOk");
                $("#confirmAA"+annex+"1").html("<img src=\""+baseURL+"images/check.png\"> Seccion guardada exitosamente");
                $("#confirmAA"+annex+"1").slideUp(12800);
            }, 
            error: function(err) { jAlert("Ha ocurrido un error al agregar Definicion AX: " + err.status + " " + err.statusText,"HERMES PROJECT VALUATION REPORT"); }
    });
    
}

/**********************
 * Carga ANEXO FIN
 **********************/

/**********************
 * LETTER INI
 **********************/
function agregaDefAX(baseURL, def, desc)
{    	    
    var param = {"def" : def, "desc" : desc };
    $.ajax({ 
            data    : param,               
            url     : baseURL+'inmueble/agregaDefAX/',
            type    : 'post',
            dataType: 'json',                
            success : function (response) 
            {	var checkboxDef = '';
                
                if ('session' in response) 
                {
                    jAlert(response['msj'],"HERMES PROJECT VALUATION REPORT", function (ans) { window.location.href = baseURL; });
                    return false;
                }
                
                if(response['tipo']=="1")
                    checkboxDef = '<section class="col col-1"><img class="borraDef" title="Borrar la definicion" value="'+response['id_def']+'" src="'+baseURL+'images/close.png"/></section>';
                
                $("#definiciones").append('<section class="col col-10"><label class="checkbox" style="text-align: justify; text-justify: inter-word"><input type="checkbox" class="defCheck" name="def[]" value="'+response['id_def']+'"><i></i><strong>'+def+'</strong><br>'+desc+'</label></section>'+checkboxDef); 
                $("#newDef"  ).fadeOut("slow").html('');
                $(".borraDef").addClass('pointer');
                $('.borraDef').click(function() {var ch = $(this);
                                                   jConfirm("¿Borrar la nueva defininicion?","HERMES PROJECT VALUATION REPORT", function(r) {
                                                                if(r) { borraDefinicionAX(baseURL,ch);  }
                                                          });
                                                 });
            }, 
            error: function(err) { jAlert("Ha ocurrido un error al agregar Definicion AX: " + err.status + " " + err.statusText,"HERMES PROJECT VALUATION REPORT"); }
    });
}

function borraDefinicionAX(baseURL,object)
{
    var param = {"id_def" : $(object).attr('value')};
    $.ajax({ 
            data    : param,               
            url     : baseURL+'inmueble/borraDefinicionAX/',
            type    : 'post',
            dataType: 'json',                
            success : function (response)
            {
                if ('session' in response) 
                {
                    jAlert(response['msj'],"HERMES PROJECT VALUATION REPORT", function (ans) { window.location.href = baseURL; });
                    return false;
                }
               $(object).parent().prev().remove();
               $(object).parent().remove();                
            }, 
            error: function(err) { jAlert("Ha ocurrido un error al agregar Definicion AX: " + err.status + " " + err.statusText,"HERMES PROJECT VALUATION REPORT"); }
    });
}

/**********************
 * LETTER FIN
 **********************/

function paginarInAX(currentPage,registrosPagina,baseURL)
{    
    var param = {"pagina" : currentPage,
                 "f1"     : $("#f1").val(),
                 "f2"     : $("#f2").val(),
                 "f3"     : $("#f3").val(),
                 "f4"     : $("#f4").val(),
                 "f5"     : $("#f5").val(),
                 "status" : $('input[name=status]:checked').val()
                };

    $.ajax({            
             url      : baseURL+'inmueble/paginarAX/',
             type     : 'post',
             data     : param,
             dataType : 'json',
             beforeSend: function () 
                {$("#spinPaginar").html("<img src=\""+baseURL+"images/spin.png\"> Espere por favor...");
                 $("#inmueblesTable tbody").html("");},
             success:  function (response) 
             {	
               $("#spinPaginar").html("");
               var columna = '';
               var conteo    = 0;
               var registros = 0;                              
               
               if (response == false)
               {
                   columna = '<tr><td colspan="6" align="center"><br><strong>No results</strong></td></tr>';
                   $('#inmueblesTable > tbody:last-child').append(columna);
                }
               else if ('session' in response) 
                        {
                            jAlert(response['msj'],"HERMES PROJECT VALUATION REPORT", function (ans) { window.location.href = baseURL; });
                            return false;
                        } 
               else
               { 
                   var fotoPortada = '';
                   var id          = response['offset'];                    
                   conteo          = response['conteo'];
                   registros       = response['registros'];
                                      
                   $("#linksPaginar").pagination({
                        items       : conteo,
                        itemsOnPage : registrosPagina,
                        cssStyle    : 'light-theme',
                        currentPage : currentPage,
                        onPageClick : function(pageNumber){paginarInAX(pageNumber,registrosPagina,baseURL);}
                    });
                   
                   for (var x = 0; x <= registros.length ; x++)
                   {
                    fotoPortada=(registros[x]['fotoPortada'] == "logoJLL.jpg") ? baseURL+response['dirFoto']+registros[x]['fotoPortada'] : baseURL+response['dirFoto']+registros[x]['id_in']+"/"+registros[x]['fotoPortada'];
                    id = id +1;
                    columna = '<tr>';
                    columna = columna + '<td align="center" width="15px">'+id+'</td>';
                    columna = columna + '<td align="center" width="20px"><img class="img-circle" width="45" heigth="45" src="'+fotoPortada+'"></td>';
                    columna = columna + '<td align="center">'+empty(registros[x]['preparedFor'])+'</td>';                    
                    columna = columna + '<td align="left">'+empty(registros[x]['calle'])+' '+empty(registros[x]['num'])+' '+empty(registros[x]['col'])+'<br>'+empty(registros[x]['mun'])+' '+empty(registros[x]['edo'])+' CP '+empty(registros[x]['cp'])+'</td>';
                    columna = columna + '<td align="center">'+empty(registros[x]['repNum'])+'</td>';
                    columna = columna + '<td align="center">'+empty(registros[x]['effectiveDate'])+'</td>';
                    columna = columna + '<td align="center">'+registros[x]['status']+'</td>';
                    columna = columna + '<td align="center">'+registros[x]['creadoPor']+'</td>';
                    columna = columna + '<td align="center"><!--<a href="'+baseURL+'inmueble/consulta/'+registros[x]['id_in']+'"><img title="Ver detalle" src="'+baseURL+'images/detail.png"></a>--> &nbsp;&nbsp;&nbsp;&nbsp;';
                    columna = columna + '                   <a href="'+baseURL+'inmueble/forma/E/'+registros[x]['id_in']+'"><img title="Modificar Inmueble" src="'+baseURL+'images/edit.png"></a></td>';
                    columna = columna + '</tr>';
                    
                    $('#inmueblesTable > tbody:last-child').append(columna);
                    
                    columna = '';
                   }                                      
                }                
             }, 
             error: function(err) { jAlert("Ha ocurrido un error al paginar los inmueblesAX: " + err.status + " " + err.statusText,"HERMES PROJECT VALUATION REPORT"); }
             });       
}

function paginarUsrAX(currentPage,registrosPagina,baseURL)
{
    var param = {"pagina" : currentPage };
      
    $.ajax({            
             url      : baseURL+'usuario/paginarAX/',
             type     : 'post',
             data     : param,
             dataType : 'json',
             beforeSend: function () 
                {$("#spinPaginar").html("<img src=\""+baseURL+"images/spin.png\"> Espere por favor...");
                 $("#userTable tbody").html("");},
             success:  function (response) 
             { 
               $("#spinPaginar").html("");                           
               var columna = '';
               var conteo    = 0;
               var registros = 0;
               
                if (response == false)
               {
                  columna = '<tr><td colspan="6" align="center"><br><strong>No results</strong></td></tr>';
                   $('#userTable > tbody:last-child').append(columna);
                }
               else if ('session' in response) 
                {
                    jAlert(response['msj'],"HERMES PROJECT VALUATION REPORT", function (ans) { window.location.href = baseURL; });
                    return false;
                } 
               else
               {                    
                   var id    = response['offset'];                    
                   conteo    = response['conteo'];
                   registros = response['registros'];
                   
                   $("#linksPaginar").pagination({
                        items       : conteo,
                        itemsOnPage : registrosPagina,
                        cssStyle    : 'light-theme',
                        currentPage : currentPage,
                        onPageClick : function(pageNumber){paginarUsrAX(pageNumber,registrosPagina,baseURL);}
                    });
                    
                   for (var x = 0; x <= registros.length ; x++)
                   {
                    var nombre = empty(registros[x]['nombre'])+' '+empty(registros[x]['apellidos']);
                    var foto   = isEmpty(registros[x]['foto'])?"images/logoJLL.jpg":response['dirFoto']+registros[x]['foto']
                    id = id +1;
                    columna = '<tr>';
                    columna = columna + '<td align="center" width="15px">'+id+'</td>';
                    columna = columna + '<td align="center" width="20px"><img title="'+nombre+'" class="img-circle" width="40" heigth="40" src="'+baseURL+foto+'"></td>';
                    columna = columna + '<td align="center">'+nombre+'</td>';
                    columna = columna + '<td align="center">'+empty(registros[x]['correo'])+'</td>';
                    columna = columna + '<td align="center">'+empty(registros[x]['puesto'])+'</td>';                    
                    columna = columna + '<td align="center">'+empty(registros[x]['tipo'])+'</td>';                    
                    columna = columna + '<td align="center"><a href="'+baseURL+'usuario/edita/'+encodeURIComponent(registros[x]['correo'])+'"><img title="Modificar Inmueble" src="'+baseURL+'images/edit.png"></a></td>';
                    columna = columna + '</tr>';
                    
                    $('#userTable > tbody:last-child').append(columna);
                    
                    columna = '';                    
                   }                                      
                }                
             }, 
             error: function(err) { jAlert("Ha ocurrido un error al paginar los inmueblesAX: " + err.status + " " + err.statusText,"HERMES PROJECT VALUATION REPORT"); }
             });       
}

function paginarCompAX(currentPage,registrosPagina,baseURL,tipoComp,selectComp,id_in,comparablesElegidos,comparablesElegidosBorrados)
{  
    var tipoTabla="";
    var headerT = "";
  
    if(selectComp===true)
      { tipoTabla=tipoComp; }
    else
      {  if(tipoComp==="CRL")
              headerT = "<tr><th></th><th></th><th>Type of property</th><th>Location</th><th>Construction m2</th><th>Monthly Rent MXN</th><th>Unitary Rent</th><th>Closing/Listing Date</th><th>Created By</th><th>Actions</th></tr>";
         else
              headerT = "<tr><th></th><th></th><th>Type of property</th><th>Location</th><th>"+((tipoComp==="CLS")?"Land m2":"Construction m2")+"</th><th>Price MXN</th><th>Unit Sale Price</th><th>Closing/Listing Date</th><th>Created By</th><th>Actions</th></tr>";
       }
       
    var param = {"pagina"        : currentPage, 
                 "tipoComp"      : tipoComp,
                 "f1"            : $("#f1"+tipoTabla).val(),
                 "f2Ini"         : $("#f2Ini"+tipoTabla).val(),
                 "f2Fin"         : $("#f2Fin"+tipoTabla).val(),
                 "f3"            : $("#f3"+tipoTabla).val(),
                 "f4"            : $("#f4"+tipoTabla).val(),
                 "f5"            : $("#f5"+tipoTabla).val(),
                 "f6Ini"         : $("#f6Ini"+tipoTabla).val(),
                 "f6Fin"         : $("#f6Fin"+tipoTabla).val(),
                 "id_in"         : id_in,
                 registrosPagina :registrosPagina
                };        
     
    $.ajax({            
             url      : baseURL+'comparables/paginarAX/',
             type     : 'post',
             data     : param,
             dataType : 'json',
             beforeSend: function () 
                {$("#spinPaginar"+tipoTabla).html("<img src=\""+baseURL+"images/spin.png\"> Espere por favor...");
                 $("#compTable"+tipoTabla+" tbody").html("");},
             success:  function (response) 
             {                
               $("#spinPaginar"+tipoTabla).html("");                           
               var columna = '';
               var conteo    = 0;
               var registros = 0;
               
               if (response === false)
               {
                   columna = '<tr><td colspan="10" align="center"><br><strong>No results</strong></td></tr>';
                   $('#compTable'+tipoTabla+' > tbody:last-child').append(columna);
                }
                else if ('session' in response) 
                {
                    jAlert(response['msj'],"HERMES PROJECT VALUATION REPORT", function (ans) { window.location.href = baseURL; });
                    return false;
                }
               else
               { 
                   var id      = response['offset'];                    
                   conteo      = response['conteo'];
                   registros   = response['registros'];                                    
                   
                   $('#compTable > thead').html(headerT);
                     
                   $("#linksPaginar"+tipoTabla).pagination({
                        items       : conteo,
                        itemsOnPage : registrosPagina,
                        cssStyle    : 'light-theme',
                        currentPage : currentPage,
                        onPageClick : function(pageNumber){ paginarCompAX(pageNumber,registrosPagina,baseURL,tipoComp,selectComp,id_in,comparablesElegidos,comparablesElegidosBorrados); }
                    });
                     
                   for (var x = 0; x <= registros.length ; x++)
                   {
                    var pathFoto = baseURL+response['dirFoto']+registros[x]['foto'];  
                    id = id +1;
                    columna = '<tr>';
                    var c1 = (tipoComp==="CLS")?registros[x]['land_m2']:registros[x]['construction'];
                    columna = columna + '<td align="center" width="15px">'+id+'</td>';
                    columna = columna + '<td align="center" width="20px"><img class="img-circle" width="60" heigth="60" src="'+pathFoto+'"></td>';
                    columna = columna + '<td align="center">'+empty(registros[x]['type_property'])+'</td>';
                    columna = columna + '<td align="left">'  +empty(registros[x]['calle'])+' '+empty(registros[x]['num'])+' '+empty(registros[x]['col'])+'<br>'+empty(registros[x]['mun'])+' '+empty(registros[x]['edo'])+' CP '+empty(registros[x]['cp'])+'</td>';
                    columna = columna + '<td align="center">'+$.number(empty(c1) ,2, '.', ',' )+'</td>';
                    columna = columna + '<td align="center">$'+$.number(empty(registros[x]['price_mx']),2, '.', ',' )+'</td>';
                    columna = columna + '<td align="center">$'+$.number(empty(registros[x]['unit_value_mx']),2, '.', ',' )+' mxn/m2</td>';
                    columna = columna + '<td align="center">'+empty(registros[x]['closing_listing_date'])+'</td>';
                    columna = columna + '<td align="center">'+empty(registros[x]['creadoPor'])+'</td>';
                    
                    if(selectComp===true)
                        {   var sc  = comparableElegido(registros[x]['id_comp'],comparablesElegidos,comparablesElegidosBorrados,registros[x]['selectComp']);
                            columna = columna + '<td align="center"><input type="checkbox" class="'+x+'compCheck'+tipoComp+'" '+sc+' name="compIn'+tipoComp+'" onClick="clickComparable(this,comparablesElegidos,comparablesElegidosBorrados)" value="'+registros[x]['id_comp']+'"></td>'; 
                        }
                    else
                        {   var botonDelete = isEmpty(registros[x]['trabajoAsignado'])?'&nbsp;<img title="Borrar Comparables de un  Inmueble" class="pointer borraComp" id="'+registros[x]['id_comp']+'" src="'+baseURL+'images/erase2.png">':"";
                                columna     = columna + '<td align="left"><a href="'+baseURL+'comparables/forma/'+tipoComp+'/E/'+registros[x]['id_comp']+'"><img title="Modificar Comparable de un Inmueble" src="'+baseURL+'images/edit.png"></a>'+botonDelete+' </td>'; 
                        }
                        
                    columna = columna + '</tr>';
                    
                    $('#compTable'+tipoTabla+' > tbody:last-child').append(columna);
                    $('.borraComp').click(function() { var id =$(this).attr("id");
                                                      jConfirm("¿Borrar Comparable y toda su informacion?","HERMES PROJECT VALUATION REPORT", function(r) {
                                                             if(r) { window.location.href = baseURL+'comparables/borra/'+id+'/'+tipoComp;   }
                                                     });                
                                       }); 
                    columna = '';
                   }
                   
                }
             }, 
             error: function(err) { jAlert("Ha ocurrido un error al paginar los comparablesAX: " + err.status + " " + err.statusText,"HERMES PROJECT VALUATION REPORT"); }
             });       
}

/**************************** SELECCION COMPARABLES INI  ***************************/
function clickComparable(objeto, comparablesElegidos,comparablesElegidosBorrados)
{
    var valor  = $(objeto).val();
    var action = $(objeto).attr("checked"); 
    
    if(action === "checked")
        {   eliminaValor(comparablesElegidosBorrados,valor);
            if(exiteValor(comparablesElegidos,valor)===false)
                { comparablesElegidos.push(valor); }
        }
    else{ 
          eliminaValor(comparablesElegidos,valor);
          if(exiteValor(comparablesElegidosBorrados,valor)===false)
                { comparablesElegidosBorrados.push(valor); }        
        }        
}

function eliminaValor(arreglo,valor)
{
   for(var i=0;i<arreglo.length;i++)
        { if(arreglo[i] === valor)
            { arreglo.splice(i, 1); }
        }
  return arreglo;
}
function exiteValor(arreglo,valor)
{
    var resp = false;
    for(var i=0;i<arreglo.length;i++)
        { if(arreglo[i]===valor)
            { resp =true; }
        }
  return resp;
}

function comparableElegido(comp,comparablesElegidos,comparablesElegidosBorrados,compGuardados)
{ 
    if (empty(compGuardados) )
        { 
           if(exiteValor(comparablesElegidos,comp)===false & exiteValor(comparablesElegidosBorrados,comp)===false)
            { comparablesElegidos.push(comp); }
          return exiteValor(comparablesElegidosBorrados,comp)===true?'':"checked='checked'"; }
    else{        
          return exiteValor(comparablesElegidos,comp)===true?"checked='checked'":""; 
        }
}

function selectCompAX(baseURL,selecComp,comparablesElegidosBorrados,tipoComp,id_in)
{  var anexo = (tipoComp==="CLS")?"AA1":(tipoComp==="CRL")?"AA3":"AA5";   
    $.ajax({
            data      : {"selecComp" : selecComp, "tipoComp" : tipoComp, "id_in":id_in},
            url       : baseURL+'inmueble/seleccioncompAX/',
            type      : 'post',
            dataType  : 'json',
            beforeSend: function () {$("#annexs"+anexo).html("<br><img src=\""+baseURL+"images/spin.png\"> Generando comparables, espere por favor...");},
            success   : function (response) 
                        {
                         if ('session' in response) 
                            {
                                jAlert(response['msj'],"HERMES PROJECT VALUATION REPORT", function (ans) { window.location.href = baseURL; });
                                return false;
                            }
                         $("#annexs"+anexo).html("");
                         $("#annexs"+anexo).html(response['tablaANEXO']);
                         $(".rc8").prop('disabled', 'disabled');
                        },
            error     : function(err) { jAlert("Ha ocurrido un error al selectCompAX en " + err.status + " " + err.statusText,"HERMES PROJECT VALUATION REPORT"); }
    });
}
/**************************** SELECCION COMPARABLES FIN    ***************************/


function empty(e) {
            switch(e) {
                case "":                 
                case null:
                case false:
                case typeof this == "undefined":
                    return "";
                        default : return e;
            }
        }
        
function emptyExtraAA(e) {
            switch(String(e)) {
                case "":
                case "undefined":
                case null:
                case false:
                case typeof this == "undefined":
                    return "A.- X ";
                default : return e;
            }
        }        
        
function isEmpty(e) {
                    switch(e) {
                        case "":
                        case "null":
                        case "0":
                        case 0:
                        case null:
                        case false:
                        case "undefined":
                        case typeof this === "undefined":
                            return true;
                        default : return false;
                    }
                }          

/*******************************************************  
 * Fotos AA8 INI *
 * *****************************************************/
function getSettingsImage(hrefF,id_in,tipoGaleria)
{
    var settings = {url         : hrefF+"inmueble/agregaImagenAA8AX",
                    dragDrop	: true,
                    fileName	: "myfile",
                    allowedTypes: "jpg,png,gif,pdf",
                    returnType	: "json",		
                    onSuccess 	: function(files,data,xhr)
                    { 
                      $(".ajax-file-upload-green"       ).html("Guardar Imagen");
                      $(".ajax-file-upload-red"         ).html("Cancelar Imagen");
                      $(".ajax-file-upload-green"       ).attr("id", "btnGuardarArchivo"+tipoGaleria );
                      if( (tipoGaleria === "Photos") || ($("#selectAAIMG").val() === "34") )
                      {
                       var leyenda     = (tipoGaleria === "Photos")?"Fotografia":"Seccion";
                       var titExtraAA  = ($("#selectAAIMG").val() === "34")?emptyExtraAA($("#titAAIMG34").html()):"";
                       var campoTitulo = '<section class="col col-10"><label class="label">Titulo de la '+leyenda+'</label>'+
                                         '    <label class="input"><i class="icon-append fa fa-tag"></i><input type="text" name="phototitulo" value="'+titExtraAA+'" id="phototitulo" placeholder="Titulo de la '+leyenda+'" maxlength="40">'+
                                         '        <b class="tooltip tooltip-bottom-right">Titulo de la '+leyenda+' acerca del reporte</b></label>'+
                                         '</section>';                       
                       $(campoTitulo).insertBefore("#btnGuardarArchivo"+tipoGaleria);                       
                      }
                      $("#btnGuardarArchivo"+tipoGaleria).click(function(){if(tipoGaleria==="AAIMG")
                                                                            {
                                                                                if($("#selectAAIMG").val() === "0")
                                                                                    { jAlert("Favor de seleccionar el tipo de anexo");
                                                                                    $("#selectAAIMG").focus();                                                                                     
                                                                                    $.post(hrefF+"inmueble/borraImagenAA8AX",{op:"delete",name:data},function() {});
                                                                                }
                                                                                else                                                                                    
                                                                                    renombraImagenAA8AX(hrefF, data, id_in, tipoGaleria,$("#selectAAIMG").val(),$("#phototitulo").val());
                                                                            }
                                                                            else                                                                                
                                                                                renombraImagenAA8AX(hrefF, data, id_in, tipoGaleria,$("#phototitulo").val(),null); 
                                                                          });
                    },
                    showDelete:true,
                    deleteCallback: function(data,pd)
                    {
                        for(var i=0;i<data.length;i++)
                        {
                            $.post(hrefF+"inmueble/borraImagenAX",{op:"delete",name:data[i]},
                            function(resp, textStatus, jqXHR)
                                    { $("#status"+tipoGaleria).html("<div>Imagen Borrada</div>"); });
                        }      
                        pd.statusbar.hide(); //You choice to hide/not.
                    }
                };
   return  settings;            
}

function subirImagenAA8(hrefF)
{
    $("#fileUploaderPhotoAA8").uploadFile(getSettingsImage(hrefF,$("#id_in").val(),"Photos"));
    $("#fileUploaderAAIMG"   ).uploadFile(getSettingsImage(hrefF,$("#id_in").val(),"AAIMG"));
}

function renombraImagenAA8AX(baseURL,nombreArchivo,id_in,tipoGaleria,phototitulo,extraAA)
{ 
    var nombreArchivoStr = String(nombreArchivo);
    var extensionStr	 = String(nombreArchivo);
    var len 		 = nombreArchivoStr.length;
    var len_ext 	 = len-3;
    var tg               = "";    
    if(tipoGaleria === "Photos")
        tg = "PH";
    else
        tg = "AAIM";        
    
    var param = {"nombreArchivo" : nombreArchivoStr.substring(0, len_ext-1),
		 "extension"	 : extensionStr.substring(len_ext,len), 
		 "id_in"	 : id_in,
                 "tipoGaleria"   : tipoGaleria,
                 "phototitulo"   : phototitulo,
                 "extraAA"       : extraAA
                };            
        $.ajax({ 
		data	: param,               
                url	: baseURL+'inmueble/renombraImagenAA8AX/',
                type	: 'post',
		dataType: 'json',                
                beforeSend: function () {$("#status"+tg).html("").removeClass("msjOk").addClass("msjconfirm");
                                         $("#status"+tg).show();
                                         $("#status"+tg).html("<img src=\""+baseURL+"images/upload.jpg\">&nbsp;&nbsp;<img src=\""+baseURL+"images/spin.png\"> Subiendo Imagen..., espere por favor...");
                                       },
                success : function (response)
				{   
                                    if ('session' in response) 
                                    {
                                        jAlert(response['msj'],"HERMES PROJECT VALUATION REPORT", function (ans) { window.location.href = baseURL; });
                                        return false;
                                    }
                                    
                                    $("#status"+tg).html("").addClass("msjOk");
                                    $("#status"+tg).html("<img src=\""+baseURL+"images/check.png\"> Imagen guardada exitosamente");
                                    $("#status"+tg).slideUp(12800);
                                    
                                    var anchoPhotos= (tipoGaleria === "Photos")?"width='430'":"";
                                    var	iconImage="<a href=\""+baseURL+response['nombreHermes']+"\" target=\"_blank\">"
                                                      +"<img title=\"Ver Photo\" "+anchoPhotos+" src=\""+baseURL+response['nombreHermes']+"\"></a><br>";
                                        iconImage = (tipoGaleria==="Photos")?iconImage+"<label id='lbl"+response['idPhoto']+"'>"+phototitulo+"</label>&nbsp;&nbsp;&nbsp;<img class='editTit pointer' id='"+response['idPhoto']+"' name='"+response['phototitulo']+"' title='Editar Titulo' src='"+baseURL+"images/edit.png'><br>":iconImage+"<br>";
                                        iconImage = iconImage+"<img class=\"delPhoto pointer\" title=\"Borrar Imagen\" id='"+response['idPhoto']+"' src=\""+baseURL+"images/close.png\"><br><br>";
                                    
                                    if(tipoGaleria === "Photos")
                                    {                                        
                                        var tds = $('#tbl'+tipoGaleria+' tr:last').children('td').length;

                                        if(tds < 3)
                                            $('#tbl'+tipoGaleria+' tr:last > td:last').after('<td align="center">'+iconImage+'</td>');
                                        else
                                            $('#tbl'+tipoGaleria+' tr:last').after('<tr><td></td><td align="center">'+iconImage+'</td></tr>');
                                        
                                        $('.editTit').click(function() {var id = $(this).attr("id");                                                                        
                                                                        $("#lbl"+id).html('<section class="col col-7"><label class="input"><i class="icon-append fa fa-tag"></i><input type="text" name="txt'+id+'" value="" id="txt'+id+'" placeholder="Nuevo Titulo" maxlength="40"><b class="tooltip tooltip-bottom-right">Modificar el Titulo de la imagen</b></label><label id="buttonUpPh" class="button">update</label></section>');
                                                                        $("#buttonUpPh").click(function() { updateTitlePhotoAX(baseURL,id,$("#txt"+id).val()); });

                                                                        $(this).hide();
                                                                       });                                        
                                    }
                                    else
                                    {   
                                        if ($('#tblAAIMG'+phototitulo).length > 0)
                                        {   if(phototitulo === "34")
                                               $('#titAAIMG'+phototitulo).html(extraAA.toUpperCase());
                                            $('#tblAAIMG'+phototitulo).append('<center><div class="row">'+iconImage+'</div></center>'); }
                                        else
                                        {   var tituloAnexo = (phototitulo === "34")?extraAA.toUpperCase():""+response['tituloAnexo'];
                                            var AAIMG=  '<fieldset>'+
                                                       '     <div class="row"> <h1 id="titAAIMG'+phototitulo+'">'+tituloAnexo+'</h1> </div>'+
                                                       '     <div class="row">'+
                                                       '     <section id="tblAAIMG'+phototitulo+'" class="col col-9">'+
                                                       '         <center>'+
                                                       '             <div class="row">'+iconImage+'</div>'+
                                                       '         <center>'+
                                                       '    </section>'+
                                                       '     </div>'+
                                                       '</fieldset>';
                                            $('#divAAIMG').append(AAIMG);
                                        }
                                    }
                                    $('.delPhoto').click(function() {  var ob = $(this);
                                                                       jConfirm("¿Borrar imagen?","HERMES PROJECT VALUATION REPORT", function(r) {
                                                                        if(r) { borraImagenCargadaAA8AX(baseURL,ob,tipoGaleria);  }
                                                                        });
                                                                    });                                    
                }, 
		error: function(err) { jAlert("Ha ocurrido un error al renombraImagenAA8AX: " + err.status + " " + err.statusText,"HERMES PROJECT VALUATION REPORT"); }
        });			
}

function borraImagenCargadaAA8AX(baseURL,objectImagen,tipoGaleria)
{    
	var id_photo = String($(objectImagen).attr('id'));
        
        var param = {"id_photo" : id_photo };
        $.ajax({ 
		data	: param,               
                url     : baseURL+'inmueble/borraImagenCargadaAA8AX/',
                type	: 'post',
		dataType: 'json',
                success : function (response)  
                                { 
                                    if ('session' in response) 
                                    {
                                        jAlert(response['msj'],"HERMES PROJECT VALUATION REPORT", function (ans) { window.location.href = baseURL; });
                                        return false;
                                    }
                                    $(objectImagen).parent().remove(); 
                                }, 
		error   : function(err)
				{ jAlert("Ha ocurrido un error borraImagenCargadaImagenAA8AX: " + err.status + " " + err.statusText,"HERMES PROJECT VALUATION REPORT"); }
        });			
}
/********************************************************* 
 * Fotos AA8 FIN 
 * ********************************8**********************/

/**********************
 * Foto Profile INI
 *********************/

function subirImagenProfile(hrefF)
{
var settings = {
                    url		: hrefF+"usuario/agregaImagenProfileAX",
                    dragDrop	: true,
                    fileName	: "myfile",
                    allowedTypes    : "jpg,png,gif,pdf",
                    returnType	: "json",		
                    onSuccess 	: function(files,data,xhr)
                    { 
                        
                      $(".ajax-file-upload-green").html("Guardar Foto Perfil");
                      $(".ajax-file-upload-red").html("Cancelar Foto Perfil");

                      $(".ajax-file-upload-green").attr( "id", "btnGuardarImagenPF" );	  
                      $("#btnGuardarImagenPF").click(function(){ $("#statusPF").html("<div><img src=\""+hrefF+"images/upload.jpg\"> Loading image...</div>");
                                                                 renombraImagenProfileAX(hrefF, data); 
                                                               });
                    },
                    showDelete:true,
                    deleteCallback: function(data,pd)
                    {
                        for(var i=0;i<data.length;i++)
                        {
                                $.post(hrefF+"usuario/borraImagenProfileAX",{op:"delete",name:data[i]},
                                function(resp, textStatus, jqXHR)
                                        { $("#statusPF").html("<div>Imagen del Perfil cancelada</div>"); });
                        }      
                        pd.statusbar.hide(); //You choice to hide/not.
                    }
                };
	
$("#fileUploaderFotoPerfil").uploadFile(settings);

}

function renombraImagenProfileAX(baseURL,nombreArchivo)		
{    	
    var nombreArchivoStr = String(nombreArchivo);
    var extensionStr	 = String(nombreArchivo);
    var len 		 = nombreArchivoStr.length;
    var len_ext 	 = len-3;     
    
    var param = {"nombreArchivo" : nombreArchivoStr.substring(0, len_ext-1),
		 "extension"	 : extensionStr.substring(len_ext,len)
                };
        $.ajax({ 
		data	: param,               
                url	: baseURL+'usuario/renombraImagenProfileAX/',
                type	: 'post',
		dataType: 'json',                
                success : function (response) 
				{   $("#statusPF").html("");
                                    if ('session' in response) 
                                    {
                                       jAlert(response['msj'],"HERMES PROJECT VALUATION REPORT", function (ans) { window.location.href = baseURL; });
                                        return false;
                                    }
                                    var	iconImage="<img class=\"img-circle\" width=\"55\" heigth=\"55\" src=\""+baseURL+response['nombreHermes']+"\"><br><br>"
						 +"<img class=\"imgP\" title=\"Borrar Imagen\" name='"+response['hImg']+"'" 
						 +" id=\""+response['hImg']+"\" src=\""+baseURL+"/images/close.png\"><br>";                                                                          
                                                                        
                                    $('#fotoPerfil').html(iconImage);
                                    $('#foto').val(response['hImg']);
                                                                                                         
                                    $('.imgP').addClass('pointer');
                                    $('.imgP').click(function(i) { var img = $(this);
                                                                  jConfirm("¿Borrar imagen?","HERMES PROJECT VALUATION REPORT", function(r) {
                                                                                   if(r) { borraImagenCargadaProfileAX(baseURL,img);  }
                                                                        });
                                                                 });		
		
                }, 
		error: function(err){ jAlert("Ha ocurrido un error al renombraAdjuntoAX: " + err.status + " " + err.statusText,"HERMES PROJECT VALUATION REPORT"); }
        });			
}


function borraImagenCargadaProfileAX(baseURL,objectImagen)
{    	        
	var nombreArchivoStr = String($(objectImagen).attr('id'));
        var extensionStr     = String($(objectImagen).attr('id'));
        var len 	     = nombreArchivoStr.length;        
        var len_ext 	     = len-3; 
                  
        var param = {"nombreArchivo" : nombreArchivoStr.substring(0, len_ext-1),
                     "extension"     : extensionStr.substring(len_ext,len)                     
                    };       
        $.ajax({ 
		data	: param,               
                url     : baseURL+'usuario/borraImagenProfileCargadaAX/',
                type	: 'post',
		dataType: 'json',
                success : function (response)  
                                {
                                    if ('session' in response) 
                                    {
                                        jAlert(response['msj'],"HERMES PROJECT VALUATION REPORT", function (ans) { window.location.href = baseURL; });
                                        return false;
                                    }
                                    var	iconImage="<img class=\"img-circle\" width=\"55\" heigth=\"55\" src=\""+baseURL+response['dirFoto']+"/logojll.png\">";
                                    $(objectImagen).parent().html('');
                                    $('#fotoPerfil').html(iconImage);
                                }, 
		error   : function(err)
				{ jAlert("Ha ocurrido un error borraImagenCargadaAX: " + err.status + " " + err.statusText,"HERMES PROJECT VALUATION REPORT"); }
        });			
}
/**********************
 * Foto Profile FIN
 **********************/


/*******************
 * Foto Portada INI
 *******************/
function subirImagenFotoPortada(hrefF)
{
var settings = 
{
	url		: hrefF+"inmueble/agregaImagenFotoPortadaAX",
	dragDrop	: true,
	fileName	: "myfile",
	allowedTypes    : "jpg,png,gif",
	returnType	: "json",		
	onSuccess 	: function(files,data,xhr)
	{
	  $(".ajax-file-upload-green").html("Guardar Imagen");
	  $(".ajax-file-upload-red").html("Cancelar Imagen");
		
	  $(".ajax-file-upload-green").attr( "id", "btnGuardarImagenPf" );	  
	  $("#btnGuardarImagenPf").click(function(){ $("#statusFotoPortada").html("<div><img src=\""+hrefF+"images/upload.jpg\"> Loading image...</div>");                                                    
                                                     renombraImagenFotoPortadaAX(hrefF, data); 
                                                   });
	},
	showDelete:true,
	deleteCallback: function(data,pd)
	{
		for(var i=0;i<data.length;i++)
		{
			$.post(hrefF+"inmueble/borraImagenAX",{op:"delete",name:data[i]},
			function(resp, textStatus, jqXHR)
				{ $("#statusFotoPortada").html("<div>Imagen Borrada</div>"); });
		}      
		pd.statusbar.hide(); //You choice to hide/not.
	}
};

$("#mulitplefileuploaderFotoPortada").uploadFile(settings);

}

function renombraImagenFotoPortadaAX(baseURL,nombreArchivo)		
{    	
    var nombreArchivoStr = String(nombreArchivo);
    var extensionStr	 = String(nombreArchivo);
    var len 		 = nombreArchivoStr.length;
    var len_ext 	 = len-3;     
    
    var param = {"nombreArchivo" : nombreArchivoStr.substring(0, len_ext-1),
		 "extension"	 : extensionStr.substring(len_ext,len),
                 "id_in"         : $("#id_in").val()
                };
    $.ajax({ 
            data    : param,               
            url     : baseURL+'inmueble/renombraImagenFotoPortadaAX/',
            type    : 'post',
            dataType: 'json',                
            success : function (response) 
                            {	$("#statusFotoPortada").html("");
                                
                                if ('session' in response) 
                                {
                                    jAlert(response['msj'],"HERMES PROJECT VALUATION REPORT", function (ans) { window.location.href = baseURL; });
                                    return false;
                                }
                                var iconImage="<img width=\"412\" height=\"418\" src=\""+baseURL+response['nombreHermes']+"\"><br><br>"
                                             +"<img class=\"imgFP\" title=\"Borrar Foto de Portada\" name='"+response['hImg']+"'" 
                                             +" id=\""+response['hImg']+"\" src=\""+baseURL+"/images/close.png\"><br>";                                                                          

                                $('#divFotoPortada').html(iconImage);
                                $('#fotoPortada').val(response['hImg']);

                                $('.imgFP').addClass('pointer');
                                $('.imgFP').click(function(i) { var img = $(this);
                                                                jConfirm("¿Borrar Foto de Portada?","HERMES PROJECT VALUATION REPORT", function(r) {
                                                                                   if(r) { borraImagenCargadaFotoPortadaAX(baseURL,img);  }
                                                                        });
                                                              });		

            }, 
            error   : function(err) { jAlert("Ha ocurrido un error al renombraAdjuntoAX: " + err.status + " " + err.statusText,"HERMES PROJECT VALUATION REPORT"); }
    });			
}


function borraImagenCargadaFotoPortadaAX(baseURL,objectImagen)
{    	        
	var nombreArchivoStr = String($(objectImagen).attr('id'));
        var extensionStr     = String($(objectImagen).attr('id'));
        var len 	     = nombreArchivoStr.length;        
        var len_ext 	     = len-3; 
                  
        var param = {"nombreArchivo" : nombreArchivoStr.substring(0, len_ext-1),
                     "extension"     : extensionStr.substring(len_ext,len),
                     "id_in"         : $("#id_in").val()
                    };       
        $.ajax({ 
		data	: param,               
                url     : baseURL+'inmueble/borraImagenCargadaFotoPortadaAX/',
                type	: 'post',
		dataType: 'json',
                success : function (response)  
                            {
                                if ('session' in response) 
                                {
                                    jAlert(response['msj'],"HERMES PROJECT VALUATION REPORT", function (ans) { window.location.href = baseURL; });
                                    return false;
                                }
                                var	iconImage="<center><img src=\""+baseURL+response['dirFoto']+"logoJLL.jpg\" title=\"Imagen de la Portada del Reporte de Valuacion\"></center>";
                                $(objectImagen).parent().html('');
                                $('#divFotoPortada').html(iconImage);
                                $('#fotoPortada').val('logoJLL.jpg');                                
                            }, 
		error   : function(err)
                            { jAlert("Ha ocurrido un error borraImagenCargadaFotoPortadaAX: " + err.status + " " + err.statusText,"HERMES PROJECT VALUATION REPORT"); }
        });			
}
/**********************
 * Foto Portada FIN
 **********************/

/*******************
 * Foto Client Logo INI
 *******************/
function subirImagenClientLogo(hrefF)
{
var settings = 
{
	url		: hrefF+"inmueble/agregaImagenClientLogoAX",
	dragDrop	: true,
	fileName	: "myfile",
	allowedTypes    : "jpg,png,gif,pdf",
	returnType	: "json",		
	onSuccess 	: function(files,data,xhr)
	{        
	  $(".ajax-file-upload-green").html("Guardar Logo");
	  $(".ajax-file-upload-red"  ).html("Cancelar Logo");
		
	  $(".ajax-file-upload-green").attr( "id", "btnGuardarImagenCL" );
	  $("#btnGuardarImagenCL").click(function(){ $("#statusClientLogo").html("<div><img src=\""+hrefF+"images/upload.jpg\"> Loading image...</div>");
                                                     renombraImagenClientLogoAX(hrefF, data); 
                                                   });
	},
	showDelete:true,
	deleteCallback: function(data,pd)
	{
		for(var i=0;i<data.length;i++)
		{
			$.post(hrefF+"inmueble/borraImagenAX",{op:"delete",name:data[i]},
			function(resp, textStatus, jqXHR)
				{ $("#statusClientLogo").html("<div>Logo Borrado</div>"); });
		}      
		pd.statusbar.hide(); //You choice to hide/not.
	}
};
	
$("#fileUpLoaderClientLogo").uploadFile(settings);

}

function renombraImagenClientLogoAX(baseURL,nombreArchivo)		
{    	
    var nombreArchivoStr = String(nombreArchivo);
    var extensionStr	 = String(nombreArchivo);
    var len 		 = nombreArchivoStr.length;
    var len_ext 	 = len-3;     
    
    var param = {"nombreArchivo" : nombreArchivoStr.substring(0, len_ext-1),
		 "extension"	 : extensionStr.substring(len_ext,len),
                 "id_in"         : $("#id_in").val()
                };
    $.ajax({ 
            data    : param,               
            url     : baseURL+'inmueble/renombraImagenClientLogoAX/',
            type    : 'post',
            dataType: 'json',                
            success : function (response) 
                            {	$("#statusClientLogo").html("");
                                if ('session' in response) 
                                {
                                   jAlert(response['msj'],"HERMES PROJECT VALUATION REPORT", function (ans) { window.location.href = baseURL; });
                                    return false;
                                }
                                var iconImage="<img src=\""+baseURL+response['nombreHermes']+"\"><br><br>"
                                             +"<img class=\"imgCL\" title=\"Borrar Logotipo del cliente\" name='"+response['hImg']+"'" 
                                             +" id=\""+response['hImg']+"\" src=\""+baseURL+"/images/close.png\"><br>";                                                                          

                                $('#divClientLogo').html(iconImage);
                                $('#clientLogo').val(response['hImg']);

                                $('.imgCL').addClass('pointer');
                                $('.imgCL').click(function(i) { var img = $(this);
                                                                jConfirm("¿Borrar Logotipo del Cliente?","HERMES PROJECT VALUATION REPORT", function(r) {
                                                                                   if(r) { borraImagenCargadaClientLogoAX(baseURL,img);  }
                                                                        });
                                                              });
            }, 
            error   : function(err) { jAlert("Ha ocurrido un error al renombraAdjuntoAX: " + err.status + " " + err.statusText,"HERMES PROJECT VALUATION REPORT"); }
    });			
}


function borraImagenCargadaClientLogoAX(baseURL,objectImagen)
{    	        
	var nombreArchivoStr = String($(objectImagen).attr('id'));
        var extensionStr     = String($(objectImagen).attr('id'));
        var len 	     = nombreArchivoStr.length;
        var len_ext 	     = len-3;
        var param            = {"nombreArchivo" : nombreArchivoStr.substring(0, len_ext-1),
                                "extension"     : extensionStr.substring(len_ext,len),
                                "id_in"         : $("#id_in").val()
                               };
        $.ajax({ 
		data	: param,               
                url     : baseURL+'inmueble/borraImagenCargadaClientLogoAX/',
                type	: 'post',
		dataType: 'json',
                success : function (response)  
                            {   
                                if ('session' in response) 
                                {
                                   jAlert(response['msj'],"HERMES PROJECT VALUATION REPORT", function (ans) { window.location.href = baseURL; });
                                    return false;
                                }
                                $(objectImagen).parent().html('');
                                $('#divClientLogo').html('No Logo');
                                $('#clientLogo').val('');
                            }, 
		error   : function(err)
                            { jAlert("Ha ocurrido un error borraImagenCargadaFotoPortadaAX: " + err.status + " " + err.statusText,"HERMES PROJECT VALUATION REPORT"); }
        });			
}
/**********************
 * Foto Client Logo FIN
 **********************/


/*******************
 * Foto Comparable INI
 *******************/
function subirImagenFotoComp(hrefF)
{
var settings = 
{
	url		: hrefF+"comparables/agregaImagenCompAX",
	dragDrop	: true,
	fileName	: "myfile",
	allowedTypes    : "jpg,png,gif,pdf",
	returnType	: "json",		
	onSuccess 	: function(files,data,xhr)
	{        
	  $(".ajax-file-upload-green").html("Guardar Imagen");
	  $(".ajax-file-upload-red").html("Cancelar Imagen");
		
	  $(".ajax-file-upload-green").attr( "id", "btnGuardarImagenComp" );	  
	  $("#btnGuardarImagenComp").click(function(){ $("#statusComp").html("<div><img src=\""+hrefF+"images/upload.jpg\"> Loading image...</div>");
                                                       renombraImagenFotoCompAX(hrefF, data); 
                                                     });
	},
	showDelete:true,
	deleteCallback: function(data,pd)
	{
		for(var i=0;i<data.length;i++)
		{
			$.post(hrefF+"comparables/borraImagenCompAX",{op:"delete",name:data[i]},
			function(resp, textStatus, jqXHR)
				{ $("#statusComp").html("<div>Imagen Borrada</div>"); });
		}      
		pd.statusbar.hide(); //You choice to hide/not.
	}
}
	
$("#fileUploaderFotoComp").uploadFile(settings);

}

function renombraImagenFotoCompAX(baseURL,nombreArchivo)		
{    	
    var nombreArchivoStr = String(nombreArchivo);
    var extensionStr	 = String(nombreArchivo);
    var len 		 = nombreArchivoStr.length;
    var len_ext 	 = len-3;     
    
    var param = {"nombreArchivo" : nombreArchivoStr.substring(0, len_ext-1),
		 "extension"	 : extensionStr.substring(len_ext,len),
                 "id_comp"         : $("#id_comp").val()
                };
    $.ajax({ 
            data	: param,               
            url	: baseURL+'comparables/renombraImagenCompAX/',
            type	: 'post',
            dataType: 'json',                
            success : function (response) 
                            {	$("#statusComp").html("");
                                if ('session' in response) 
                                {
                                    jAlert(response['msj'],"HERMES PROJECT VALUATION REPORT", function (ans) { window.location.href = baseURL; });
                                    return false;
                                }
                                var iconImage="<img src=\""+baseURL+response['nombreHermes']+"\"><br><br>"
                                             +"<img class=\"imgCP\" title=\"Borrar Foto Comparable\" name='"+response['id_comp']+"'" 
                                             +" id=\""+response['hImg']+"\" src=\""+baseURL+"/images/close.png\"><br>";                                                                          

                                $('#FotoComp').html(iconImage);
                                $('#foto').val(response['id_comp']+'/'+response['hImg']);

                                $('.imgCP').addClass('pointer');
                                $('.imgCP').click(function(i) {   var objectImagen = $(this);
                                                                  jConfirm("¿Borrar Foto del Comparable?","HERMES PROJECT VALUATION REPORT", function(r) {
                                                                                   if(r) { borraImagenCargadaFotoCompAX(baseURL,objectImagen);  }
                                                                        });
                                                              });
            }, 
            error   : function(err) { jAlert("Ha ocurrido un error al renombraAdjuntoAX: " + err.status + " " + err.statusText,"HERMES PROJECT VALUATION REPORT"); }
    });			
}


function borraImagenCargadaFotoCompAX(baseURL,objectImagen)
{    	        
	var nombreArchivoStr = String($(objectImagen).attr('id'));
        var extensionStr     = String($(objectImagen).attr('id'));
        var len 	     = nombreArchivoStr.length;        
        var len_ext 	     = len-3; 
        var tmp              = nombreArchivoStr.substring(0, len_ext-1);
        var tmp2             = tmp.split("/",2);
        var id_comp          = tmp2[0];
        var nombreArchivo    = tmp2[1];
        
        var param = {"nombreArchivo" : nombreArchivo,
                     "id_comp"       : id_comp,
                     "extension"     : extensionStr.substring(len_ext,len)        
                    };       
        $.ajax({ 
		data	: param,               
                url     : baseURL+'comparables/borraImagenCompCargadaAX/',
                type	: 'post',
		dataType: 'json',
                success : function (response)  
                            {
                                if ('session' in response) 
                                {
                                   jAlert(response['msj'],"HERMES PROJECT VALUATION REPORT", function (ans) { window.location.href = baseURL; });
                                    return false;
                                }
                                var	iconImage="<img src=\""+baseURL+response['dirFoto']+"jll_in.jpg\"  class=\"img-circle\">";
                                $(objectImagen).parent().html('');
                                $('#FotoComp').html(iconImage);
                                $('#foto').val('jll_in.jpg');                                
                            }, 
		error   : function(err)
                            { jAlert("Ha ocurrido un error borraImagenCargadaFotoPortadaAX: " + err.status + " " + err.statusText,"HERMES PROJECT VALUATION REPORT"); }
        });			
}
/**********************
 * Foto Comparable FIN
 **********************/


/*******************
 * Foto API INI
 *******************/
function subirImagenAPI(hrefF)
{
var settings = 
{
	url		: hrefF+"inmueble/agregaImagenAPIAX",
	dragDrop	: true,
	fileName	: "myfile",
	allowedTypes    : "jpg,png,gif",
	returnType	: "json",		
	onSuccess 	: function(files,data,xhr)
	{        
	  $(".ajax-file-upload-green").html("Guardar Imagen");
	  $(".ajax-file-upload-red").html("Cancelar Imagen");
		
	  $(".ajax-file-upload-green").attr( "id", "btnGuardarImagenAPI" );	  
	  $("#btnGuardarImagenAPI").click(function(){ $("#statusAPI").html("<div><img src=\""+hrefF+"images/upload.jpg\"> Loading image...</div>");
                                                      renombraImagenAPIAX(hrefF, data); });
	},
	showDelete:true,
	deleteCallback: function(data,pd)
	{
		for(var i=0;i<data.length;i++)
		{
			$.post(hrefF+"inmueble/borraImagenAX",{op:"delete",name:data[i]},
			function(resp, textStatus, jqXHR)
				{ $("#statusAPI").html("<div>Imagen Borrada</div>"); });
		}      
		pd.statusbar.hide(); //You choice to hide/not.
	}
};
	
$("#fileUploaderAPI").uploadFile(settings);

}

function renombraImagenAPIAX(baseURL,nombreArchivo)		
{    	
    var nombreArchivoStr = String(nombreArchivo);
    var extensionStr	 = String(nombreArchivo);
    var len 		 = nombreArchivoStr.length;
    var len_ext 	 = len-3;     
    
    var param = {"nombreArchivo" : nombreArchivoStr.substring(0, len_ext-1),
		 "extension"	 : extensionStr.substring(len_ext,len),
                 "id_in"         : $("#id_in").val()
                };
    $.ajax({ 
            data    : param,               
            url     : baseURL+'inmueble/renombraImagenAPIAX/',
            type    : 'post',
            dataType: 'json',                
            success : function (response) 
                            {	$("#statusAPI").html("");
                                if ('session' in response) 
                                {
                                   jAlert(response['msj'],"HERMES PROJECT VALUATION REPORT", function (ans) { window.location.href = baseURL; });
                                    return false;
                                }
                                var iconImage="<img src=\""+baseURL+response['nombreHermes']+"\"><br><br>"
                                             +"<img class=\"clImgAPI\" title=\"Borrar imagen\" name='"+response['hImg']+"'" 
                                             +" id=\""+response['hImg']+"\" src=\""+baseURL+"/images/close.png\"><br>";                                                                          

                                $('#divImgAPI').html(iconImage);
                                $('#imgAPI').val(response['hImg']);

                                $('.clImgAPI').addClass('pointer');
                                $('.clImgAPI').click(function(i) { var img = $(this);
                                                                jConfirm("¿Borrar imagen?","HERMES PROJECT VALUATION REPORT", function(r) {
                                                                                   if(r) { borraImagenCargadaAPIAX(baseURL,img);  }
                                                                        });
                                                              });		

            }, 
            error   : function(err) { jAlert("Ha ocurrido un error al renombraAdjuntoAX: " + err.status + " " + err.statusText,"HERMES PROJECT VALUATION REPORT"); }
    });			
}


function borraImagenCargadaAPIAX(baseURL,objectImagen)
{
	var nombreArchivoStr = String($(objectImagen).attr('id'));
        var extensionStr     = String($(objectImagen).attr('id'));
        var len 	     = nombreArchivoStr.length;
        var len_ext 	     = len-3; 
                  
        var param = {"nombreArchivo" : nombreArchivoStr.substring(0, len_ext-1),
                     "extension"     : extensionStr.substring(len_ext,len),
                     "id_in"         : $("#id_in").val()
                    };                    
        $.ajax({ 
		data	: param,               
                url     : baseURL+'inmueble/borraImagenCargadaAPIAX/',
                type	: 'post',
		dataType: 'json',
                success : function (response)  
                            {   
                                if ('session' in response) 
                                {
                                    jAlert(response['msj'],"HERMES PROJECT VALUATION REPORT", function (ans) { window.location.href = baseURL; });
                                    return false;
                                }
                                $(objectImagen).parent().html('');                             
                                $('#imgAPI').val('');
                            }, 
		error   : function(err)
                            { jAlert("Ha ocurrido un error borraImagenCargadaAPIAX: " + err.status + " " + err.statusText,"HERMES PROJECT VALUATION REPORT"); }
        });			
}
/**********************
 * Foto API FIN
 **********************/


/*******************
 * Foto APII INI
 *******************/
function subirImagenAPII(hrefF,tipo,urlFuncion)
{
var settings = 
{
	url		: hrefF+"inmueble/agregaImagen"+urlFuncion+"AX",
	dragDrop	: true,
	fileName	: "myfile",
	allowedTypes    : "jpg,png,gif",
	returnType	: "json",		
	onSuccess 	: function(files,data,xhr)
	{        
	  $(".ajax-file-upload-green").html("Guardar Imagen");
	  $(".ajax-file-upload-red").html("Cancelar Imagen");
		
	  $(".ajax-file-upload-green").attr( "id", "btnGuardarImagenAP"+tipo );	  
	  $("#btnGuardarImagenAP"+tipo).click(function(){ $("#status"+tipo).html("<div><img src=\""+hrefF+"images/upload.jpg\"> Loading image...</div>");
                                                          renombraImagenAPIIAX(hrefF,tipo,urlFuncion, data); 
                                                        });
	},
	showDelete:true,
	deleteCallback: function(data,pd)
	{
		for(var i=0;i<data.length;i++)
		{
			$.post(hrefF+"inmueble/borraImagenAX",{op:"delete",name:data[i]},
			function(resp, textStatus, jqXHR)
				{ $("#status"+tipo).html("<div>Imagen Borrada</div>"); });
		}      
		pd.statusbar.hide(); //You choice to hide/not.
	}
};
	
$("#fileUploader"+tipo).uploadFile(settings);

}

function renombraImagenAPIIAX(baseURL,tipo,urlFuncion,nombreArchivo)		
{    	
    var nombreArchivoStr = String(nombreArchivo);
    var extensionStr	 = String(nombreArchivo);
    var len 		 = nombreArchivoStr.length;
    var len_ext 	 = len-3;     
    
    var param = {"nombreArchivo" : nombreArchivoStr.substring(0, len_ext-1),
		 "extension"	 : extensionStr.substring(len_ext,len),
                 "id_in"         : $("#id_in").val(),
                 "tipo"         : tipo
                };
    $.ajax({ 
            data    : param,               
            url     : baseURL+'inmueble/renombraImagen'+urlFuncion+'AX/',
            type    : 'post',
            dataType: 'json',                
            success : function (response) 
                            {	$("#status"+tipo).html("");
                                if ('session' in response) 
                                {
                                   jAlert(response['msj'],"HERMES PROJECT VALUATION REPORT", function (ans) { window.location.href = baseURL; });
                                    return false;
                                }
                                var leyenda = response['tipo']==="APII_3"?"Property Plans":"Subject's Location";
                                var iconImage="<img src=\""+baseURL+response['nombreHermes']+"\"><br><br>"
                                             +"<img class=\"clImg"+tipo+"\" title=\"Borrar imagen\" name='"+response['hImg']+"'" 
                                             +" id=\""+response['hImg']+"\" src=\""+baseURL+"/images/close.png\"><br> "+leyenda+" <br>";

                                $('#divImg'+tipo).html(iconImage);
                                $('#img'+tipo).val(response['hImg']);

                                $('.clImg'+tipo).addClass('pointer');
                                $('.clImg'+tipo).click(function(i) { var img = $(this);
                                                                jConfirm("¿Borrar imagen?","HERMES PROJECT VALUATION REPORT", function(r) {
                                                                                   if(r) { borraImagenCargadaAPIIAX(baseURL,img,tipo,urlFuncion);  }
                                                                        });
                                                              });		

            }, 
            error   : function(err) { jAlert("Ha ocurrido un error al renombraAdjuntoAX: " + err.status + " " + err.statusText,"HERMES PROJECT VALUATION REPORT"); }
    });			
}


function borraImagenCargadaAPIIAX(baseURL,objectImagen,tipo,urlFuncion)
{    	        
	var nombreArchivoStr = String($(objectImagen).attr('id'));
        var extensionStr     = String($(objectImagen).attr('id'));
        var len 	     = nombreArchivoStr.length;        
        var len_ext 	     = len-3; 
                  
        var param = {"nombreArchivo" : nombreArchivoStr.substring(0, len_ext-1),
                     "extension"     : extensionStr.substring(len_ext,len),
                     "id_in"         : $("#id_in").val(),
                     "tipo"          : tipo
                    };       
        $.ajax({ 
		data	: param,               
                url     : baseURL+'inmueble/borraImagenCargada'+urlFuncion+'AX/',
                type	: 'post',
		dataType: 'json',
                success : function (response)  
                            {   
                                if ('session' in response) 
                                {
                                    jAlert(response['msj'],"HERMES PROJECT VALUATION REPORT", function (ans) { window.location.href = baseURL; });
                                    return false;
                                }
                                $(objectImagen).parent().html('');                                
                                $('#img'+tipo).val('');
                            }, 
		error   : function(err)
                            { jAlert("Ha ocurrido un error borraImagenCargadaAPIIAX: " + err.status + " " + err.statusText,"HERMES PROJECT VALUATION REPORT"); }
        });			
}
/**********************
 * Foto APII FIN
 **********************/


function recuperarPwd()
{
	
    $("#closePWD" ).position({        
                                my: "right-8 top",
                                at: "right-8 top",
                                of: "#recuperarPwd"
                            });
    $('#closePWD').addClass('pointer');
    $('#closePWD').click(function()
          {
            $("#leyendaPWD").html("");
            $("#userRec").val("");
            $('#recuperarPwd').hide('Fold');
            $("#recuperarPwd" ).position({        
                                            my: "left0 top0",
                                            at: "left0 top0",
                                            of: "#recuperarPwd"
                                        });
            $("#closePWD" ).position({        
                                        my: "right-8 top",
                                        at: "right-8 top",
                                        of: "#recuperarPwd"
                                    });
          });	
    $('#recuperarPwd').show('Drop');
}

function recuperarPwdAX(baseURL,forma)
{
  $("#"+forma).validationEngine();
  if($("#"+forma).validationEngine('validate'))
  {
    var correo = $("#email").val();
    var param  = {"correo" : correo};    
    $.ajax({ 
            data      : param,               
            url       : baseURL+'gestion/recuperarPwdAX/',
            type      : 'post',
            dataType  : 'json',
            beforeSend: function () {$("#leyendaPWD").html("<br><img src=\""+baseURL+"images/spin.png\"> Validando Cuenta, espere por favor...");},
            success   :  function (response) 
                        {
                            $("#leyendaPWD").html("");						
                            if(response['existe']== true)
                            {
                                $("#leyendaPWD").html("La contaseña ha sido enviada a la cuenta de correo: "+correo);
                                $("#usuario").val(correo);
                            }
                            else
                                $("#leyendaPWD").html("No se encuentra registrada la cuenta de correo: "+correo);
                        }, 
            error     : function(err) {alert("Ha ocurrido un error recuperarPwdAX: " + err.status + " " + err.statusText);}
    });
  }  
}



function exportaInmuebleAX(id_in,baseURL,tipo,report,ann_in,ap_in,fecha_name,repNum,idiom)
{
        var tag     = "";
        var leyenda = "";
        var logo    = "";
                
        leyenda = "documento generado";
        logo    = "WordLogo.png";
        tag     = "generaDOC";

	var param = {"id_in" : id_in, "tipoExp" : tipo, "report" : report, "ann_in" : ann_in, "ap_in": ap_in,"fecha_name":fecha_name,"repNum":repNum,"idiom":idiom};
        $.ajax({                
                url       : baseURL+'inmueble/exportaInmuebleAX/',
                type	  : 'post',
                data	  : param,
                dataType  : 'json',
                beforeSend: function () 
				{$("#"+tag).html("<img src=\""+baseURL+"images/spin.png\"> Generando Archivo, espere por favor...");},
                success:  function (response) 
                          {	                              
                                if ('session' in response) 
                                {
                                    jAlert(response['msj'],"HERMES PROJECT VALUATION REPORT", function (ans) { window.location.href = baseURL; });
                                    return false;
                                }
                                if(response['expFile']=="No information")
                                {$("#"+tag).html("No information to generate report");}
                                else
                                {
                                    var	iconF="<a href=\""+baseURL+"gestion/download/"+response['dir']+response['id_in']+"/"+response['expFile']+ "\" target=\"_blank\">"
                                                      +"<img title=\"Ver "+leyenda+" con nombre "+response['expFile']+"\" "
                                                      +"  src=\""+baseURL+"images/"+logo+"\" width='40px' height='40px'><br>"+response['expFile']+"</a><br><br>"
                                                      +"&nbsp;&nbsp;&nbsp;&nbsp;<img class=\"bcExpForm\" title=\"Borrar archivo de exportación\""
                                                      +"name=\""+response['id_in']+"\"  id=\""+response['expFile']+"\" src=\""+baseURL+"/images/erase.png\">";

                                    $("#"+tag).html(iconF);

                                    $('.bcExpForm').addClass('pointer');
                                    $('.bcExpForm').click(function(i) { var objectImagen =$(this);                                                                    
                                                                        jConfirm("¿Borrar Archivo generado?","HERMES PROJECT VALUATION REPORT", function(r) {
                                                                                       if(r) { borraExpFormAX(baseURL,objectImagen);  }
                                                                                });
                                                                       });
                                }
                          },
		error: function(err)
                         { jAlert("Ha ocurrido un error al exportaInmuebleAX: " + err.status + " " + err.statusText,"HERMES PROJECT VALUATION REPORT"); 
                         $("#"+tag).addClass("msjErr").html("<img src=\""+baseURL+"images/close.png\">Ocurrio un problema al generar reporte, intentelo mas tarde");}
        });	
}



function exportaComparablesAX(tipo_c,f1,f3,f2Ini,f2Fin,f4,f5,f6Ini,f6Fin,baseURL)
{
        var tag     = "";
        var leyenda = "";
        var logo    = "";
                
        leyenda = "documento generado";
        logo    = "ExcelLogo.png";
        tag     = "generaDOC";

	var param = {"tipo_c" : tipo_c,"f1":f1,"f3":f3,"f2Ini":f2Ini,"f2Fin":f2Fin,"f4":f4,"f5":f5,"f6Ini":f6Ini,"f6Fin":f6Fin};
        $.ajax({                
                url       : baseURL+'comparables/exportaExcelAX/',
                type	  : 'post',
                data	  : param,
                dataType  : 'json',
                beforeSend: function () 
				{$("#"+tag).html("<img src=\""+baseURL+"images/spin.png\"> Generando Archivo, espere por favor...");},
                success:  function (response) 
                          {	                              
                                if ('session' in response) 
                                {
                                    jAlert(response['msj'],"HERMES PROJECT VALUATION REPORT", function (ans) { window.location.href = baseURL; });
                                    return false;
                                }
                                if(response['expFile']=="No information")
                                {$("#"+tag).html("No information to generate report");}
                                else
                                {
                                    var	iconF="<br><a href=\""+baseURL+"gestion/downloadExcel/"+response['dir']+"/"+response['expFile']+ "\" target=\"_blank\">"
                                                      +"<img title=\"Ver "+leyenda+" con nombre "+response['expFile']+"\" "
                                                      +"  src=\""+baseURL+"images/"+logo+"\" width='40px' height='40px'><br>"+response['expFile']+"</a><br><br>"
                                                      +"&nbsp;&nbsp;&nbsp;&nbsp;<img class=\"bcExpExcel\" title=\"Borrar archivo de exportación\""
                                                      +"name=\""+response['id_in']+"\"  id=\""+response['expFile']+"\" src=\""+baseURL+"/images/erase.png\">";

                                    $("#"+tag).html(iconF);
                                    
                                    $('.bcExpExcel').addClass('pointer');
                                    $('.bcExpExcel').click(function(i) { var objectImagen =$(this);                                                                    
                                                                        jConfirm("¿Borrar Archivo generado?","HERMES PROJECT VALUATION REPORT", function(r) {
                                                                                       if(r) { $(objectImagen).parent().html('');  }
                                                                                });
                                                                       });
                                }
                          },
		error: function(err)
                         { jAlert("Ha ocurrido un error al exportaInmuebleAX: " + err.status + " " + err.statusText,"HERMES PROJECT VALUATION REPORT"); 
                         $("#"+tag).addClass("msjErr").html("<img src=\""+baseURL+"images/close.png\">Ocurrio un problema al generar reporte, intentelo mas tarde");}
        });	
}

function borraExpFormAX(baseURL,objectImagen)
{    	        
    var nombreArchivoStr = String($(objectImagen).attr('id'));
    var extensionStr     = String($(objectImagen).attr('id'));
    var len              = nombreArchivoStr.length;        
    var len_ext 	 = len-4; 

    var param = {"nombreArchivo" : nombreArchivoStr.substring(0, len_ext-1),
                 "extension"     : extensionStr.substring(len_ext,len),
                 "id_in"         : $(objectImagen).attr('name')
                };       
    $.ajax({ 
            data    : param,               
            url     : baseURL+'inmueble/borraExpFormAX/',
            type    : 'post',
            dataType: 'json',
            success : function (response) { 
                                            if ('session' in response) 
                                            {
                                               jAlert(response['msj'],"HERMES PROJECT VALUATION REPORT", function (ans) { window.location.href = baseURL; });
                                                return false;
                                            }
                                            $(objectImagen).parent().html(''); 
                                          }, 
            error   : function(err) { jAlert("Ha ocurrido un error borraImagenCargadaAX: " + err.status + " " + err.statusText,"HERMES PROJECT VALUATION REPORT"); }
    });
}


function validateDuplicateAX(baseURL,field)
{
    $.ajax({
            data      :  {"correo" : $(field).val()},
            url       :  baseURL+'gestion/validaCampoDuplicadoAX/',
            type      : 'post',
            dataType  : 'json',
            beforeSend: function () { $("#msjCorreo").html(""); },
            success   : function (response) 
                        {
                            if (response['duplicado'] == true)
                            { $("#msjCorreo").html("El correo "+response['correo']+" ya se encuentra a signado a "+response['nombre']+". Favor de ingresar otra cuenta de correo "); 
                              $(field).val('');
                            }
                            else
                            { $("#msjCorreo").html(""); }
                        }, 
            error     : function(err) { jAlert("Ha ocurrido un error al validad duplicidad en " + err.status + " " + err.statusText,"HERMES PROJECT VALUATION REPORT"); }
    });    
}



function traeDatosValuadorAX(baseURL,field)
{ 
    $.ajax({
            data      :  {"correo" : $(field).val()},
            url       :  baseURL+'gestion/traeDatosValuadorAX/',
            type      : 'post',
            dataType  : 'json',
            beforeSend: function () { $("#"+$(field).attr('id')+"Lbl").html(""); },
            success   : function (response) 
                        { 
                            if ('session' in response) 
                            {
                                jAlert(response['msj'],"HERMES PROJECT VALUATION REPORT", function (ans) { window.location.href = baseURL; });
                                return false;
                            }
                            var titulo = "";
                            if (!isEmpty(response[0]['titulo']) )
                                {titulo =", "+ response[0]['titulo']}
                            $("#"+$(field).attr('id')+"Lbl").html(empty(response[0]['nombre'])+" "+empty(response[0]['apellidos'])+titulo+"<br>"+empty(response[0]['puesto'])+" <br>Valuation and Consulting Services<br> JLL Mexico");
                        }, 
            error     : function(err) { jAlert("Ha ocurrido un error al traeDatosValuadorAX en " + err.status + " " + err.statusText,"HERMES PROJECT VALUATION REPORT"); }
    });    
}


function updateERAX(baseURL,campo,id,tipoComp,tipo)
{
    if (isNaN(campo) || isEmpty(campo))
    {
        jAlert("El valor ingresado: "+campo+" como Exchane Rate no es valido","HERMES PROJECT VALUATION REPORT");
        $("#newER").val('');
    }
    else{
        $.ajax({
                data      :  {"er" : campo, "id":id, "tipo":tipo},
                url       :  baseURL+'gestion/updateERAX/',
                type      : 'post',
                dataType  : 'json',            
                success   : function (response) 
                            {
                                if ('session' in response) 
                                {
                                    jAlert(response['msj'],"HERMES PROJECT VALUATION REPORT", function (ans) { window.location.href = baseURL; });
                                    return false;
                                }
                                $("#erDiv").html('Exchange rate $'+campo+'&nbsp;&nbsp;<img title="Update Exchange Rate" id="updateER" width="15" height="15" src="'+baseURL+'images/edit.png"><br><label style="font-size:10px;">'+response['date']+'</label>');
                                $("#updateER").addClass('pointer');
                                $("#updateER").click(function() { 
                                                                  $("#erDiv").html('Exchange rate $<label class="input"><i class="icon-append fa fa-usd"></i><input type="text" name="newER" value=""  id="newER" placeholder="New Exchange Rate" maxlength="8"><b class="tooltip tooltip-bottom-right">Actualizar Tipo de Cambio</b></label>'+                                                      
                                                                                  '<label id="buttonUpER" class="button">update</label></section>');                                                                  
                                                                  $("#buttonUpER").click(function() { updateERAX(baseURL,$("#newER").val(),id,tipoComp,tipo); });
                                                              });
                                er = campo;                                                              
                                $("#exchange_rate").val(er);
                                if(tipo === "comp")
                                {                                    
                                    var price  = Number($("#price_mx").val());  
                                    var usd    = price/er;                                          
                                    var land   = (tipoComp==="CLS")?Number($("#land_m2").val()) : Number($("#construction").val());
                                    var uv     = land!==0?price/land:0;
                                    var uv_usd = (tipoComp==="CRL")?((uv/er)*response['ftyear']):(tipoComp==="CRS")?( (uv/er)/ft):(uv/er);                                    
                                    
                                    $("#price_mx").val(price);
                                    $("#price_usdLbl").html('$'+$.number(usd ,2, '.', ',' ) + ' usd');                                      
                                    $("#price_usd").val(usd.toFixed(2));

                                    $("#unit_value_mxLbl").html((tipoComp==="CRL")?'$'+$.number(uv,2,'.',',' )+" mxn / m2 / mo<br>$"+$.number((uv/er),2,'.',',' )+" usd / m2 / mo" : "$"+$.number(uv,2,'.',',' )+" mxn / m2");
                                    $("#unit_value_mx").val(uv.toFixed(2));

                                    $("#unit_value_usdLbl").html((tipoComp==="CRL")?'$'+$.number(uv_usd ,2, '.', ',' )+" usd / ft2 / year" : (tipoComp==="CRS")?'$'+$.number(uv_usd,2, '.', ',' )+" usd / ft2":'$'+$.number(uv_usd ,2, '.', ',' )+" usd / m2");
                                    $("#unit_value_usd").val(uv_usd.toFixed(2));
                                }
                                else
                                { cargaAnexoInAX(baseURL,"AA1","CLS",id); }
                            }, 
                error     : function(err) { jAlert("Ha ocurrido un error al updateERAX en " + err.status + " " + err.statusText,"HERMES PROJECT VALUATION REPORT"); }
            });
    }
}

function rcExtraField(object)
{   $(object).parent().next().remove();
    $(object).parent().next().remove();
    if($(object).val()==5 || $(object).val()==6 || $(object).val()==9 || $(object).val()==10  )
        { $(object).parent().parent().after().append('<br><label class="input"><i class="icon-append fa fa-text-width"></i><input type="text" name="'+$(object).attr('id')+'_adjus" id="'+$(object).attr('id')+'_adjus" class="validate[required,custom[onlyLetterNumber]] text-input" value="" placeholder="Value" maxlength="40"><b class="tooltip tooltip-bottom-right">Value</b></label>');  }
}

function validateSelect(object,campo)
{
    var select = $(object).attr('select');
    var valor  = $(object).val();
    
    if (isEmpty(valor)==false) 
        { $("#"+select).attr('class','validate[custom[requiredInFunction]]');
           $("."+campo)         .each(function(){ $(this).html(valor); });
           $("."+campo+"Input") .each(function(){ $(this).val(valor); });
           $("."+campo+"Select").addClass('validate[custom[requiredInFunction]]').removeAttr("disabled");
        }
    else
        { var leyendaOther= (campo==="rc6_9")?"1":"2";
          $("#"+select).removeClass('validate[custom[requiredInFunction]]'); 
          $("."+campo)         .each(function(){ $(this).html("Other "+leyendaOther); });
          $("."+campo+"Input") .each(function(){ $(this).val(null); });
          $("."+campo+"Select").val(0).removeClass('validate[custom[requiredInFunction]]').prop("disabled", true );
        }
}

function nomenclaturaRepNum(baseURL,id_in,fn)
{    
    var origen      = $("#origen_name"     ).val();
    var concepto    = $("#concepto_name"   ).val();
    var clave       = $("#clave_ano_name"  ).val();    

    $.ajax({
            data      :  {"origen_name" : origen, "concepto_name":concepto, "clave_ano_name":clave, "id_in":id_in},
            url       :  baseURL+'inmueble/generaConsecutivoAX/',
            type      : 'post',
            dataType  : 'json',            
            success   : function (response) 
                        {
                        origen          = ($("#origen_name"    ).val()==="35")?"":"M-";                        
                        var consecutivo = response;                        
                        var repNumCap   = quitaCaracteresEspeciales( $("#repNumCap").val() );    
                        var nomen       = origen + concepto + "-" + clave + "-" + consecutivo + "_"+ repNumCap;
                        
                        $("#appraisal_num"     ).val (origen + concepto + "-" + clave + "-" + consecutivo);
                        $("#lblrepNum"         ).html(nomen);
                        $("#repNum"            ).val (nomen);
                        $("#consecutivoNameLbl").html(consecutivo);
                        $("#consecutivo_name"  ).val (consecutivo);
                        
                        return nomen;
                        
                        }, 
            error     : function(err) { jAlert("Ha ocurrido un error al nomenclaturaRepNum en " + err.status + " " + err.statusText,"HERMES PROJECT VALUATION REPORT"); }
    });
}

function quitaCaracteresEspeciales(entrada)
{    
    return String( entrada ).toUpperCase().replace(/\?/g, '').replace(/ /g, '_').replace(/Ñ/g, 'N').replace(/Á/g, 'A').replace(/É/g, 'E').replace(/Í/g, 'I').replace(/Ó/g, 'O').replace(/Ú/g, 'U').replace(/,/g, '_').replace(/`/g, "").replace(/#/g, "").replace(/\"/g, '').replace(/\\/g, '').replace(/\'/g, '').replace(/\%/g, '').replace(/</g, '').replace(/>/g, "");
}

function LandToft(object, ft)
{
   var id   = $(object).attr('id');
   var land = $(object).maskMoney('unmasked')[0];
 
    if ( isNaN(land) )
         { jAlert("El valor ingresado: "+land+" NO es un numero valido","HERMES PROJECT VALUATION REPORT");
         $(object).val('');}
    else{   var len     = id.length;
            var len_ext = len-3;
            id  =  id.substring(0, len_ext-1);           
            $("#"+id).val(land);
           
            land   = Number(land);
            $("#"+id+"Ft").html($.number(land*ft,2, '.', ',' ));
            
            if(id ==="taxBill" | id ==="deed")
            {                
                var dif  = Number($("#deed").val()) - Number($("#taxBill").val());
                
                $("#landDif").html($.number(dif,2, '.', ',' ));
                $("#landDifFt").html($.number(dif*ft,2, '.', ',' ));
            }
        }     
}


function calculaPorCFO(tipo, object,rcn,er,inc)
{
   var num = $(object).maskMoney('unmasked')[0];
 
    if ( isNaN(num) )
        { jAlert("El valor ingresado: "+num+" NO es un numero valido","HERMES PROJECT VALUATION REPORT");
           $(object).val('');
        }
    else{  var campo   = String($(object).attr('id'));
           var len     = campo.length;
           var len_ext = len-3;
           var campoN  =  campo.substring(0, len_ext-1);           
           $("#"+campoN).val(num);
           num   = Number(num)/er;
           var por = (isEmpty(rcn)===true?0:num/rcn);           
           $("#"+tipo+"Lbl").html($.number(por*100,2, '.', ',' ));
           $("#"+tipo).val(por);           
           $("#"+tipo+"usd").html($.number(num,2, '.', ',' ));
           
           var subTotal    = sumaCA("cfo"+inc)/er;           
           var subTotalPor = (isEmpty(rcn)===true?0:subTotal/rcn);
                     
           $("#AVtotCFO"+inc+"Por").html($.number(subTotalPor*100,2, '.', ',' ));
           $("#AVtotCFO"+inc      ).html($.number(subTotal*er,2, '.', ',' ));
           $("#AVtotCFO"+inc+"usd").html($.number(subTotal   ,2, '.', ',' ));
        }
}



function calculaPGI(tipo, campo, object,er,iv,ra)
{
    var mxn = $(object).maskMoney('unmasked')[0];

    if ( isNaN(mxn) )
         { jAlert("El valor ingresado: "+mxn+" NO es un numero valido","HERMES PROJECT VALUATION REPORT");
           $(object).val('');
         }
    else 
    if ( isEmpty($(object).parent().prev().prev().children().val())===true )
        { jAlert("Es necesario ingresar el concepto correspondiente a la columna: 'Expense reimbursement revenue'","HERMES PROJECT VALUATION REPORT");
          $(object).focus();}
    else{  var campoMk = String($(object).attr('id'));
           var len     = campoMk.length;
           var len_ext = len-3;
           var campoN  =  campoMk.substring(0, len_ext-1);           
           $("#"+campoN).val(mxn);
           mxn                   = Number(mxn);
           var usd               = Number(mxn)/er;
           var mxnMonto          = (tipo === "Mon")? mxn*12 : mxn/12;
           var usdMonto          = (mxnMonto)/er;
           var campoPorCalcular  = (tipo === "Mon")? "Year" : "Mon";
           
           $("#AVIpgi_"+campo+"_"+tipo+"_usd").html($.number(usd,2, '.', ',' ));
           $("#AVIpgi_"+campo+"_"+campoPorCalcular+"_mxn").val(mxnMonto);
           $("#AVIpgi_"+campo+"_"+campoPorCalcular+"_usdLbl").html($.number(usdMonto,2, '.', ',' ));
           $("#AVIpgi_"+campo+"_"+campoPorCalcular+"_mxnLbl").html($.number(mxnMonto,2, '.', ',' ));
           
           var serr  = Number($("#AVIpgi_1_Year_mxn").val()) + Number($("#AVIpgi_2_Year_mxn").val()) + Number($("#AVIpgi_3_Year_mxn").val()) + Number($("#AVIpgi_4_Year_mxn").val()) ;
           $("#AVIpgi_serr_mxnLbl").html($.number(serr   ,2, '.', ',' ));
           $("#AVIpgi_serr_usdLbl").html($.number(serr/er,2, '.', ',' ));
           
           var tpgio = serr/er + ((iv * 12) * ra);
           $("#AVIpgi_tpgio_mxnLbl").html($.number(tpgio*er   ,2, '.', ',' ));
           $("#AVIpgi_tpgio_usdLbl").html($.number(tpgio,2, '.', ',' ));
           
           calculamvcl($("#AVIpgi_mvcl_por"),er,tpgio*er);
        }
}
function calculamvcl(object,er,tpgio)
{
    var mvclPor = $(object).val();

    if ( isNaN(mvclPor) )
         { jAlert("El valor ingresado: "+mvclPor+" NO es un numero valido","HERMES PROJECT VALUATION REPORT");
           $(object).val('');
         }
    else{  mvclPor = Number(mvclPor)/100;
           var mvcl = mvclPor*tpgio;
           $("#AVIpgi_mvcl_mxnLbl").html($.number(mvcl   ,2, '.', ',' ));
           $("#AVIpgi_mvcl_usdLbl").html($.number(mvcl/er,2, '.', ',' ));
           
           var egi = tpgio - mvcl;
           $("#AVIpgi_egi_mxnLbl").html($.number(egi   ,2, '.', ',' ));
           $("#AVIpgi_egi_usdLbl").html($.number(egi/er,2, '.', ',' ));
        }
}
calculaOE_Por(this,'2',18.6457,1336583.44224)
function calculaOE_Por(object,campo,er,egi)
{
    var oePor = $(object).val();

    if ( isNaN(oePor) )
         { jAlert("El valor ingresado: "+oePor+" NO es un numero valido","HERMES PROJECT VALUATION REPORT");
           $(object).val('');
         }
    else{  oePor = Number(oePor)/100;
           var eoNum = oePor * egi;
           $("#AVIoe_"+campo+"_num2").val(eoNum);
           $("#AVIoe_"+campo+"_num2Lbl"   ).html($.number(eoNum   ,2, '.', ',' ));
           $("#AVIoe_"+campo+"_num2Lblusd").html($.number(eoNum/er,2, '.', ',' ));
           
           calculaTotalesOE(er,egi);
        }
}

function calculaOE_Num(object,campo,er,egi)
{
    var oeNum = $(object).maskMoney('unmasked')[0];

    if ( isNaN(oeNum) )
         { jAlert("El valor ingresado: "+oeNum+" NO es un numero valido","HERMES PROJECT VALUATION REPORT");
           $(object).val('');
         }
    else{  var campoMk = String($(object).attr('id'));          
           var len     = campoMk.length;
           var len_ext = len-3;
           var campoN  =  campoMk.substring(0, len_ext-1);
           
           $("#"+campoN).val(oeNum);
           oeNum = Number(oeNum);
           var oePor = (oeNum / egi)*100;
           $("#AVIoe_"+campo+"_num1"   ).val(oePor);           
           $("#AVIoe_"+campo+"_num1Lbl").html($.number(oePor,2, '.', ',' ));
           $("#AVIoe_"+campo+"_num2usd").html($.number(oeNum/er,2, '.', ',' ));
           
           calculaTotalesOE(er,egi);
        }
}

function calculaTotalesOE(er,egi)
{
    
   var sePor = sumaCA("oeNum1");
   var seNum = sumaCA("oeNum2");
   $("#AVIoe_sePor"   ).html($.number(sePor   ,2, '.', ',' ));
   $("#AVIoe_seNum"   ).html($.number(seNum   ,2, '.', ',' ));
   $("#AVIoe_seNumusd").html($.number(seNum/er,2, '.', ',' ));
   
   var unf   = (Number($("#AVIoe_unf").val())/100) * seNum;
   $("#AVIoe_unfLbl"   ).html($.number(unf   ,2, '.', ',' ));
   $("#AVIoe_unfLblusd").html($.number(unf/er,2, '.', ',' ));
                                    
   var totExpPor = sePor + ( (unf/egi)*100 );
   var totExpNum = seNum + unf;
   $("#totExpPor"      ).html($.number(totExpPor   ,2, '.', ',' ));
   $("#AVIoe_totExp"   ).html($.number(totExpNum   ,2, '.', ',' ));
   $("#AVIoe_totExpusd").html($.number(totExpNum/er,2, '.', ',' ));
   $("#AVI_totExp"     ).val(totExpPor);
 
   var noi  = egi - totExpNum;    
   $("#AVIoe_noi"   ).html($.number(noi   ,2, '.', ',' ));
   $("#AVIoe_noiusd").html($.number(noi/er,2, '.', ',' ));
   $("#AVI_noi"     ).val(noi);
}

function calculaOE_unf(object,er,egi)
{
    var unf = $(object).val();

    if ( isNaN(unf) )
         { jAlert("El valor ingresado: "+unf+" NO es un numero valido","HERMES PROJECT VALUATION REPORT");
           $(object).val('');
         }
    else{ calculaTotalesOE(er,egi); }
}


function calculaSourcePor(object,campo,url)
{
    var sourcePor = $(object).val();

    if ( isNaN(sourcePor) )
         { jAlert("El valor ingresado: "+sourcePor+" NO es un numero valido","HERMES PROJECT VALUATION REPORT");
           $(object).val('');
         }
    else{  
           var por1 = Number($("#AVIsource_"+campo+"_ran1").val());
           var por2 = Number($("#AVIsource_"+campo+"_ran2").val());
           //var av = ( por2 === 0)?por1:(por1 + por2)/2;
           var av = Number($("#AVIsource_"+campo+"_av").val());
           //$("#AVIsource_"+campo+"_av").html($.number(av,2, '.', ',' ));
           
           var avTot  = 0;
           var numTot = 0;           
           
           for(var i=1;i<5;i++)
           {   
               if ($("#AVIsource_"+i+"_Txt").val() != "")
               {
                   avTot  = avTot +Number($("#AVIsource_"+i+"_av").val());
                   numTot = numTot + 1;                   
                }
           }    

           //alert("avTot:" + avTot);                             
           var averageTot = avTot / numTot; 
           $("#avTot"    ).html($.number(averageTot,2, '.', ',' ));           
           $("#avgTot"   ).val(averageTot);
           $("#avgTotLbl").html($.number(averageTot,2, '.', ',' ));
           
           calcularCapRate($('#AVIcapR_por1'),url);
        }
}

/*

function calculaSourcePor(object,campo,url)
{
    var sourcePor = $(object).val();

    if ( isNaN(sourcePor) )
         { jAlert("El valor ingresado: "+sourcePor+" NO es un numero valido","HERMES PROJECT VALUATION REPORT");
           $(object).val('');
         }
    else{  
           var por1 = Number($("#AVIsource_"+campo+"_ran1").val());
           var por2 = Number($("#AVIsource_"+campo+"_ran2").val());
           var av = ( por2 === 0)?por1:(por1 + por2)/2;
           $("#AVIsource_"+campo+"_av").html($.number(av,2, '.', ',' ));
           
           var avTot  = 0;
           var numTot = 0;           
           
           for(var i=1;i<5;i++)
           {   
               if ($("#AVIsource_"+i+"_Txt").val() != "")
               {
                   avTot  = avTot +Number($("#AVIsource_"+i+"_av").html());
                   numTot = numTot + 1;                   
               }
           }                                 
           var averageTot = avTot / numTot;           
           $("#avTot"    ).html($.number(averageTot,2, '.', ',' ));           
           $("#avgTot"   ).val(averageTot);
           $("#avgTotLbl").html($.number(averageTot,2, '.', ',' ));
           
           calcularCapRate($('#AVIcapR_por1'),url);
        }
}


*/


function calcularICR(object,url)
{
    var sourcePor = $(object).val();

    if ( isNaN(sourcePor) )
         { jAlert("El valor ingresado: "+sourcePor+" NO es un numero valido","HERMES PROJECT VALUATION REPORT");
           $(object).val('');
         }
    else{  
        var AVImir   = Number($("#AVImir").val())/100;
        $("#AVImirLvlC").html($.number($("#AVImir").val(),2, '.', ',' ));
        
        var AVImt    = Number($("#AVImt").val());
        $("#AVImtLvlC").html($.number($("#AVImt").val(),2, '.', ',' ));
        
        var AVIlvr   = Number($("#AVIlvr").val())/100;
        $("#AVIlvrLvlC").html($.number($("#AVIlvr").val(),2, '.', ',' ));
        $("#lvrLbl"    ).html($.number($("#AVIlvr").val(),2, '.', ',' ));
        
        var AVIedr   = Number($("#AVIedr").val()/100);
        var edr      = 1-AVIlvr;
        $("#AVIedrLbl").html($.number(edr*100   ,2, '.', ',' ));
        $("#edrLbl"   ).html($.number(AVIedr*100,2, '.', ',' ));                
        
        var morCo1 = (AVImir/12);
        var morCo2 = Math.pow( 1/(1+(AVImir/12)),(AVImt*12) );        
        var morCo  = ( morCo1 / (1 - morCo2 ) ) * 12;
        
        $("#morCo"   ).html($.number(morCo*100,2, '.', ',' ));
        $("#morCoLbl").html($.number(morCo*100,2, '.', ',' ));
        $("#AVImorCo").val(morCo*100);
        $("#AVImorCoLvlC").html($.number(morCo*100,2, '.', ',' ));
        
        var morCoTot = AVIlvr * morCo;
        $("#morCoLblFin").html($.number(morCoTot*100,2, '.', ',' ));
        
        var edrTot   = AVIedr * edr;
        $("#edrLblFin").html($.number(edrTot*100,2, '.', ',' ));
        
        var icr      = morCoTot + edrTot;
        $("#icrLbl").html($.number(icr*100,2, '.', ',' ));
        $("#icrLbl2").html($.number(icr*100,2, '.', ',' ));
        $("#AVIicr").val(icr*100);        
        
        calcularCapRate($('#AVIcapR_por1'),url);
    }    
}


function calcularDCR(object,url)
{ 
    var dcr = $(object).val();
    if ( isNaN(dcr) )
         { jAlert("El valor ingresado: "+dcr+" NO es un numero valido","HERMES PROJECT VALUATION REPORT");
           $(object).val('');
         }
    else{
          var  dcr = (Number(dcr)) * (Number($("#AVIlvr").val())/100) * (Number($("#AVImorCo").val())*100);
          $("#dcrFin").html($.number(dcr/100,2, '.', ',' ));
          $("#dcrLbl").html($.number(dcr/100,2, '.', ',' ));
          $("#dcr").val(dcr/100);
          
          calcularCapRate($('#AVIcapR_por1'),url);
        }
}


function calcularCapRate(object,url)
{ 
    var cr = $(object).val();
    if ( isNaN(cr) )
         { jAlert("El valor ingresado: "+cr+" NO es un numero valido","HERMES PROJECT VALUATION REPORT");
           $(object).val('');
         }
    else{
          var AVIcapR_por1 = (Number($("#AVIcapR_por1").val())/100);
          var AVIcapR_por2 = (Number($("#AVIcapR_por2").val())/100);
          var AVIcapR_por3 = (Number($("#AVIcapR_por3").val())/100);
          var avgTot       = Number($("#avgTot").val());
          var icr          = Number($("#AVIicr").val());
          var dcr          = Number($("#dcr").val());
          
          var capRate      = (AVIcapR_por1 * avgTot) + (AVIcapR_por2 * icr) + (AVIcapR_por3 * dcr);
          
          $("#capRateTot" ).html("<br>"+$.number(capRate,2, '.', ',' ));
          $("#AVIcap_rate").val(capRate);
          
          var prorrateoCR  = (AVIcapR_por1+AVIcapR_por2+AVIcapR_por3)*100;
          var valProrrateo = ( prorrateoCR === 100)?"<img src=\""+url+"images/check.png\">&nbsp;&nbsp;":"<img src=\""+url+"images/close.png\">&nbsp;&nbsp;";
          var classVal     = ((prorrateoCR === 100)?"msjOkCentro":"msjErrCentro");
          $("#capRSum").removeClass("msjOkCentro msjErrCentro").addClass(classVal);          
          $("#capRSum").html("<br>"+valProrrateo+$.number(prorrateoCR,2, '.', ',' )+' %');
        }
}


function calcularExLand(object,iv, plans, ft)
{ 
    var campoMk = String($(object).attr('id'));
    var farGen =  (campoMk==="AVIfAreaMask")?$(object).maskMoney('unmasked')[0]:$(object).val();
    if ( isNaN(farGen) )
         { jAlert("El valor ingresado: "+farGen+" NO es un numero valido","HERMES PROJECT VALUATION REPORT");
           $(farGen).val('');
         }
    else{             
           var len     = campoMk.length;
           var len_ext = len-3;
           var campoN  =  campoMk.substring(0, len_ext-1);           
           $("#"+campoN).val(farGen);
        
          var far          = Number($("#AVIfar"  ).val());
          var mf           = Number($("#AVImf"   ).val());
          var fArea        = Number($("#AVIfArea").val());
          $("#AVIfAreaft").html($.number(fArea*ft,2, '.', ',' ));
          
          var tls          = far * fArea;
          $("#tls"  ).html($.number(tls,2, '.', ',' ));
          $("#tlsft").html($.number(tls*ft,2, '.', ',' ));
          
          var exl          = (plans-tls)>0?(plans-tls):0;
          $("#exl"  ).html($.number(exl,2, '.', ',' ));
          $("#exlft").html($.number(exl*ft,2, '.', ',' ));
          
          var uvel         = iv * mf;
          $("#uvel"  ).html($.number(uvel,2, '.', ',' ));
          $("#uvelft").html($.number(uvel*ft,2, '.', ',' ));
          
          var vel          = uvel * exl;
          $("#vel"  ).html($.number(vel,2, '.', ',' ));
          $("#velft").html($.number(vel*ft,2, '.', ',' ));
          
          var ivudica = Number($("#AVIivudica").val());
          var AVIivudicael = ivudica + vel;
          $("#ivudicaelLbl").html($.number(AVIivudicael,2, '.', ',' ));
          $("#AVIivudicael").val(AVIivudicael);
        }
}

function calcularivusca(ra, baseURL)
{
    var uav    = Number($("#AVIIuavMask").maskMoney('unmasked')[0]);
    var ivusca = uav * ra;
    $("#AVIIuav"      ).val(uav);
    $("#AVIIivuscaLbl").html($.number(ivusca,2, '.', ',' ));
    $("#AVIIivusca"   ).val(ivusca);
    
    traeFormatoRedondeadoAX(ivusca, 0, baseURL,"AVIIivuscaRoundLbl");
    
}

function traeFormatoRedondeadoAX(cantidad, tipo, baseURL, idTagRedondeo)
{    
    var param = {"cantidad" : cantidad,
                 "tipo"     : tipo
                };

    $.ajax({ url       : baseURL+'gestion/traeFormatoRedondeadoAX/',
             type      : 'post',
             data      : param,
             dataType  : 'json',
             beforeSend: function ()         {  },
             success   : function (response) { $("#"+idTagRedondeo).html(response);	}, 
             error     : function(err)       { jAlert("Ha ocurrido un error al paginar los inmueblesAX: " + err.status + " " + err.statusText,"HERMES PROJECT VALUATION REPORT");} });       
}


function calcularMarketValue(object, url, idTagRedondeo, mask)
{     
    var mv =  (mask===true)?$(object).maskMoney('unmasked')[0]:$(object).val();    
    if ( isNaN(mv) )
         { jAlert("El valor ingresado: "+mv+" NO es un numero valido","HERMES PROJECT VALUATION REPORT");
           $(object).val(0);
         }
    else{ 
          if (mask===true)
            { $("#AXnum3").val(mv);
              $("#AXTit3Sum").html($.number(mv,2, '.', ',' ));
              var dcfUV = mv / Number($("#totTRA").val());
              $("#AXTit3SumUV").html($.number(dcfUV,2, '.', ',' ));
            }
            
          var ponderacionMV1 = (Number($("#AXpor1").val())/100) * Number($("#AXnum1").val());
          var ponderacionMV2 = (Number($("#AXpor2").val())/100) * Number($("#AXnum2").val());
          var ponderacionMV3 = (Number($("#AXpor3").val())/100) * Number($("#AXnum3").val());
          var ponderacionMV5 = (Number($("#AXpor5").val())/100) * Number($("#AXnum5").val());
          var AXmarketVal    = ponderacionMV1 + ponderacionMV2 + ponderacionMV3 + ponderacionMV5;
          
          traeFormatoRedondeadoAX(AXmarketVal, 0, url, idTagRedondeo);
          
          var totPor         = Number($("#AXpor1").val()) + Number($("#AXpor2").val()) + Number($("#AXpor3").val()) + Number($("#AXpor5").val());
          var valtotPor      =  (totPor === 100)?"<img src='"+url+"images/check.png'>&nbsp;&nbsp;":"<img src=\""+url+"images/close.png\">&nbsp;&nbsp;";
          var classVal       = ((totPor === 100)?"msjOkCentro":"msjErrCentro");
    
          $("#totPorMV").removeClass("msjOkCentro msjErrCentro").addClass(classVal);          
          $("#totPorMV").html("<br>"+valtotPor+$.number(totPor,2, '.', ',' )+' %');
          
          $("#ponderacionMV1" ).html($.number(ponderacionMV1,2, '.', ',' ));
          $("#AX_C_Approach"  ).val (ponderacionMV1);
          $("#ponderacionMV2" ).html($.number(ponderacionMV2,2, '.', ',' ));
          $("#AX_IC_Approach" ).val (ponderacionMV2);
          $("#ponderacionMV3" ).html($.number(ponderacionMV3,2, '.', ',' ));
          $("#AX_DCF_Approach").val (ponderacionMV3);
          $("#ponderacionMV5" ).html($.number(ponderacionMV5,2, '.', ',' ));
          $("#AX_SC_Approach" ).val (ponderacionMV5);
          $("#AXmarketValLbl" ).html($.number(AXmarketVal,2, '.', ',' ));
          $("#AXmarketValField"    ).val (AXmarketVal);
        }
}


function calcularVis(object, cavweo,url,idTagRedondeo)
{     
    var mv =  $(object).val();
    if ( isNaN(mv) )
         { jAlert("El valor ingresado: "+mv+" NO es un numero valido","HERMES PROJECT VALUATION REPORT");
           $(object).val(0);
         }
    else{ 
          var ponderacionMV4 = (Number(mv)/100) * cavweo;
          
          traeFormatoRedondeadoAX(ponderacionMV4, 0, url, idTagRedondeo);
          $("#AX_VU_Approach").val(ponderacionMV4);
          $("#ponderacionMV4").html($.number(ponderacionMV4,2, '.', ',' ));          
          
        }
}


function sumarRate(object,url,idTagRedondeo,marketVal,totTRA,ft)
{     
    var rate =  $(object).val();
    if ( isNaN(rate) )
         { jAlert("El valor ingresado: "+rate+" NO es un numero valido","HERMES PROJECT VALUATION REPORT");
           $(object).val(0);
         }
    else{ 
          var totRate = sumaCA('sumRate');
          
          traeFormatoRedondeadoAXLV(totRate, 1, url, idTagRedondeo,marketVal,totTRA,ft);
          
          $("#totRiskPLbl").html($.number(totRate,2, '.', ',' )+" %");
          $("#discRateLbl").html($.number(totRate,2, '.', ',' )+" %");
          $("#ocdrLbl" ).html($.number(totRate,2, '.', ',' ));
    
          
        }
}

function traeFormatoRedondeadoAXLV(cantidad, tipo, baseURL, idTagRedondeo, marketVal,totTRA,ft)
{    
    var param = {"cantidad" : cantidad,
                 "tipo"     : tipo
                };

    $.ajax({ url       : baseURL+'gestion/traeFormatoRedondeadoAX/',
             type      : 'post',
             data      : param,
             dataType  : 'json',
             beforeSend: function ()         {  },
             success   : function (response) { $("#"+idTagRedondeo).html($.number(response,1, '.', ',' )+" %");
                                               calculaLiquidFactor(marketVal,response,baseURL,totTRA,ft)
                                             }, 
             error     : function(err)       { jAlert("Ha ocurrido un error al paginar los inmueblesAX: " + err.status + " " + err.statusText,"HERMES PROJECT VALUATION REPORT");} });       
}

function calculaLiquidFactor(marketVal,redondearHermesTotRate,url,totTRA,ft)
{    
    var difDR       = redondearHermesTotRate - Number($("#AXIRate1").val());
    //var ocdr        = difDR + Number($("#AXIRate9").val());
    var ocdr = Number($("#ocdrLbl").html());
    
    $("#difDRLbl").html($.number(difDR,2, '.', ',' ));
    //$("#ocdrLbl" ).html($.number(ocdr ,2, '.', ',' ));
    
    var ocdrMonth   = ocdr / 12 ;
    $("#ocdrMonthLbl").html($.number(ocdrMonth,2, '.', ',' ));
    
    var mt          = Number($("#mt").val());
    var AXISaleTerm = Number($("#AXISaleTerm").val());
    var restaMT     = (mt- AXISaleTerm);
    
    restaMT = restaMT<0?(restaMT*-1):restaMT;
    $("#AXISaleTermLbl").html($("#AXISaleTerm").val());
    $("#restaMT").html(restaMT);
    //alert("term:"+AXISaleTerm);
    /*console.log("AXiSaleTerm:"+AXISaleTerm);
    console.log("ocdrMonth:"+ocdrMonth);
    console.log("mt:"+mt);
    console.log("marketVal:"+marketVal);*/
    //var oppCos      = (isEmpty(AXISaleTerm))?0:marketVal - ( marketVal / Math.pow((1 + (ocdrMonth / 100)), restaMT ) );    
    var oppCos      = (isEmpty(AXISaleTerm))?0:(ocdrMonth/100) * (mt - AXISaleTerm) * marketVal;
   

    $("#oppCosLbl").html($.number(oppCos,2, '.', ',' ));
    
    var rate6Month  = Number($("#AXIRate6").val()) / 12;
    $("#AXIRate6Lbl"  ).html($.number($("#AXIRate6").val(),2, '.', ',' ));
    $("#rate6MonthLbl").html($.number(rate6Month,2, '.', ',' ));
 
    //var manCost     = (rate6Month/100) * AXISaleTerm * marketVal;
    var manCost     = (rate6Month/100) * (mt-AXISaleTerm) * marketVal;
    $("#manCostLbl").html($.number(manCost,2, '.', ',' ));
    
    var saleCost    = (Number($("#AXIRate7").val())/100) * marketVal;
    $("#AXIRate7Lbl").html($.number($("#AXIRate7").val(),2, '.', ',' ));
    $("#saleCostLbl").html($.number(saleCost,2, '.', ',' ));
    
    var rate8Month  = Number($("#AXIRate8").val()) / 12;
    $("#AXIRate8Lbl").html($.number($("#AXIRate8").val(),2, '.', ',' ));
    $("#rate8MonthLbl").html($.number(rate8Month,2, '.', ',' ));
    
    //var properTax   = (rate8Month/100) * AXISaleTerm * marketVal;
    var properTax   = (rate8Month/100) * (mt-AXISaleTerm) * marketVal;
    $("#properTaxLbl").html($.number(properTax,2, '.', ',' ));
    
    var liquidVal   = marketVal - oppCos - manCost - saleCost - properTax;
    
    $("#liquidValLblTb").html($.number(liquidVal,2, '.', ',' ));
    $("#liquidValLbl"  ).html($.number(liquidVal,2, '.', ',' ));    
    
    //var liquidFactor= (isEmpty(marketVal))?0:(marketVal - liquidVal) / marketVal; 
    var liquidFactor= (isEmpty(marketVal))?0:(1-(marketVal - liquidVal) / marketVal); 
     
    $("#liquidFactorLbl").html($.number(liquidFactor*100,2, '.', ',' ));
    $("#AXIliquidVal"   ).val(liquidVal);
    traeFormatoRedondeadoAX(liquidVal, 0, url, 'liquidValLblRH');    
    
    var AXIliquidValM2  = liquidVal / totTRA;
    var AXIliquidValft2 = AXIliquidValM2 / ft;    
    $("#AXIliquidValM2Lbl").html($.number(AXIliquidValM2,2, '.', ',' ));    
    $("#AXIliquidValft2Lbl").html($.number(AXIliquidValft2,2, '.', ',' ));    
    traeFormatoRedondeadoAX(liquidVal, 0, url, 'AXIliquidValLbl');
    
    var liquidValMXM      = liquidVal * Number($("#erLiqVal").val()) ;
    var AXIliquidValMXMM2 = liquidValMXM / totTRA;
    $("#AXIliquidValMXMM2Lbl").html($.number(AXIliquidValMXMM2,2, '.', ',' ));        
    traeFormatoRedondeadoAX(liquidValMXM, 0, url, 'AXIliquidValMXMLbl');
}



function recalculaAPV_VI(baseURL,tipoAccion,ap,id_in,subseccion)
{
    $("#confirmRecalculate"+ap+subseccion).addClass("msjconfirm").html("<img src=\""+baseURL+"images/spin.png\"> Recalculando, espere por favor...").show();
    submitAPV_VI_AX(baseURL,'form'+ap,id_in,tipoAccion,ap,subseccion);    
}



function cargaAreaBuild(url, anexo, tipoComp, id_in)
{
    //$("input[name='sky-tabs-2']").trigger( "click", [ anexo, tipoComp, id_in] );
    
    alert($("#sky-tab2-9 > ul > .sky-tab-content-9").html());
}